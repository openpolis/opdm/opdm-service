#!/bin/bash

# data di oggi
DATE=$(date +%Y%m%d)

curl https://cronitor.link/I2bGKr/run -m 10 || true

# backup db to archive on postgres container
echo Generating backup archive
/usr/bin/docker exec -eDATE="$DATE" -upostgres opdm-service_postgres bash -c "cd ~/data; pg_dump opdm > opdm_$DATE.sql; gzip opdm_$DATE.sql; exit"

# copy archive from container on local machine
echo Copy archive from container
/usr/bin/docker cp "opdm-service_postgres:/var/lib/postgresql/data/opdm_$DATE.sql.gz" .

# store backup from local  machine to aws S3
echo Store archive to s3
/usr/bin/aws --profile s3 s3 cp "opdm_$DATE.sql.gz s3://opdm-service-data/"

# monthly dump
DAY=$(date +%d)
if [[ $DAY = 01 ]]; then
  echo Copying monthly dump
  /usr/bin/aws --profile s3 s3 cp "s3://opdm-service-data/opdm_$DATE.sql.gz" s3://opdm-service-data/monthly/
fi

# removing daily backups older than a week
echo Removing backups older than a week
for (( i=10; i>=7; i-- ));
do
    echo Removing "s3://opdm-service-data/opdm_$(date -d "-$i days" +"%Y%m%d").sql.gz"
    /usr/bin/aws/ --profile s3 s3 rm "s3://opdm-service-data/opdm_$(date -d "-$i days" +"%Y%m%d").sql.gz"
done;

# remove archive locally
rm "opdm_$DATE.sql.gz"

# remove archive on docker-container
/usr/bin/docker exec -eDATE="$DATE" -upostgres opdm-service_postgres bash -c "rm /var/lib/postgresql/data/opdm_$DATE.sql.gz"

curl https://cronitor.link/I2bGKr/complete -m 10 || true

