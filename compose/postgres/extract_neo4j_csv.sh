echo csv_extraction
cat extract_neo4j_csv.sql | psql -Uopdm opdm

echo csv_transformation
rm *_header.csv
for F in $(ls -1 *.csv | sed -e 's/\.csv$//')
do
  echo $F
  head -n 1 $F.csv > ${F}_header.csv
  tail -n +2 $F.csv > ${F}_body.csv
  mv ${F}_body.csv $F.csv
done

