drop function if exists date_2_ts(text);

create or replace function date_2_ts(d text) returns bigint
    language plpgsql
as
$$
BEGIN
RETURN date_part('epoch', to_timestamp(d, 'YYYY-MM-DD'));
END;
$$;


-- areas.csv
CREATE TEMP VIEW av as
SELECT
  'AREA:' || a.id::text as "id:ID(Area-ID)",
  a.id as "opdm_id:INT",
  CASE
    WHEN a.istat_classification = 'COM' THEN 'Comune di ' || a.name || ' (' || ap.identifier || ')'
    WHEN a.istat_classification = 'PROV' THEN 'Provincia di ' || a.name
    WHEN a.istat_classification = 'CM' THEN 'Città metropolitana di ' || a.name
    WHEN a.istat_classification = 'REG' THEN 'Regione ' || a.name
    WHEN a.istat_classification = 'RIP' THEN 'Ripartizione ' || a.name
    ELSE a.name
  END as "name",
  CASE
    WHEN a.istat_classification = 'COM' THEN 'Comune'
    WHEN a.istat_classification = 'PROV' THEN 'Provincia'
    WHEN a.istat_classification = 'CM' THEN 'Città metropolitana'
    WHEN a.istat_classification = 'REG' THEN 'Regione'
    WHEN a.istat_classification = 'RIP' THEN 'Ripartizione'
    ELSE ''
  END as "classification",
  a.inhabitants as "inhabitants:int",
  CASE WHEN a.end_date > current_date::text or a.end_date is null THEN 'true'
       ELSE 'false'
  END as "is_current:BOOLEAN",
  st_y(st_centroid(a.geometry)::geometry) as "lat:float",
  st_x(st_centroid(a.geometry)::geometry) as "long:float",
  'Area'::text as ":LABEL"
FROM popolo_area as a
    left join popolo_area ap on a.parent_id = ap.id
;
\COPY (SELECT * FROM av) TO 'nodes_areas.csv' WITH CSV HEADER


-- organizations.csv (with labels)
CREATE TEMP VIEW ov as
SELECT
  'ORG:' || o.id::text as "id:ID(Organization-ID)",
  o.id as "opdm_id:INT",
  o.name,
  c.descr as label,
  o.classification,
  date_part('years', age(to_timestamp(date_2_ts(o.founding_date)))) as "age:int",
  CASE WHEN o.dissolution_date > current_date::text or o.dissolution_date is null THEN 'true'
       ELSE 'false'
  END as "is_current:BOOLEAN",
  CASE
    WHEN oe.revenue is null THEN null
    WHEN oe.revenue between 0 and 250000 THEN 1
    WHEN oe.revenue between 250001 and 2000000 THEN 2
    WHEN oe.revenue between 2000001 and 10000000 THEN 3
    WHEN oe.revenue between 10000001 and 50000000 THEN 4
    ELSE 5
  END as "revenue_class:int",
  CASE
    WHEN oe.employees is null THEN null
    WHEN oe.employees between 0 and 2 THEN 1
    WHEN oe.employees between 3 and 10 THEN 2
    WHEN oe.employees between 11 and 50 THEN 3
    WHEN oe.employees between 51 and 250 THEN 4
    ELSE 5
  END as "employees_class:int",
  'Organizzazione'::text AS ":LABEL"
FROM popolo_organization as o
LEFT JOIN popolo_identifier i on i.object_id=o.id and i.content_type_id=28 and i.scheme='CF'
LEFT JOIN atoka_organizationeconomics as oe on oe.organization_id = o.id
LEFT JOIN popolo_classificationrel as cr on cr.object_id = o.id and cr.content_type_id=28
LEFT JOIN popolo_classification c on cr.classification_id=c.id
WHERE c.scheme = 'OPDM_ORGANIZATION_LABEL'
UNION
SELECT
  'ORG:' || o.id::text as "id:ID(Organization-ID)",
  o.id as "opdm_id:INT",
  o.name,
  null as label,
  o.classification,
  date_part('years', age(to_timestamp(date_2_ts(o.founding_date)))) as "age:int",
  CASE WHEN o.dissolution_date > current_date::text or o.dissolution_date is null THEN 'true'
       ELSE 'false'
  END as "is_current:BOOLEAN",
  CASE
    WHEN oe.revenue is null THEN null
    WHEN oe.revenue between 0 and 250000 THEN 1
    WHEN oe.revenue between 250001 and 2000000 THEN 2
    WHEN oe.revenue between 2000001 and 10000000 THEN 3
    WHEN oe.revenue between 10000001 and 50000000 THEN 4
    ELSE 5
  END as "revenue_class:int",
  CASE
    WHEN oe.employees is null THEN null
    WHEN oe.employees between 0 and 2 THEN 1
    WHEN oe.employees between 3 and 10 THEN 2
    WHEN oe.employees between 11 and 50 THEN 3
    WHEN oe.employees between 51 and 250 THEN 4
    ELSE 5
  END as "employees_class:int",
  'Organizzazione'::text AS ":LABEL"
FROM popolo_organization as o
LEFT JOIN popolo_identifier i on i.object_id=o.id and i.content_type_id=28 and i.scheme='CF'
LEFT JOIN atoka_organizationeconomics as oe on oe.organization_id = o.id
WHERE o.id NOT IN
      (SELECT o.id
       FROM popolo_organization as o
                LEFT JOIN popolo_classificationrel as cr on cr.object_id = o.id and cr.content_type_id = 28
                LEFT JOIN popolo_classification as c on c.id = cr.classification_id
       WHERE c.scheme = 'OPDM_ORGANIZATION_LABEL'
      )
;
\COPY (SELECT * FROM ov) TO 'nodes_organizations.csv' WITH CSV HEADER

-- persons.csv (with labels)
CREATE TEMP VIEW pv as
SELECT
  'PERSON:' || p.id::text as "id:ID(Person-ID)",
  p.id as "opdm_id:INT",
  p.name, p.image,
  p.gender,
  date_part('years', age(to_timestamp(date_2_ts(p.birth_date)))) as "age:int",
  p.birth_location as birth_location_str,
  string_agg(c.descr, ', ' order by c.descr) AS "labels",
  CASE WHEN p.death_date is null THEN 'true'
       ELSE 'false'
  END as "is_alive:BOOLEAN",
  'Persona'::text AS ":LABEL"
FROM popolo_person p
  LEFT JOIN popolo_classificationrel cr on cr.object_id=p.id and cr.content_type_id=33
  LEFT JOIN popolo_classification c on cr.classification_id=c.id
WHERE c.scheme = 'OPDM_PERSON_LABEL'
GROUP BY "opdm_id:INT",
  p.name, p.image, p.gender, "age:int",
  birth_location_str,
  "is_alive:BOOLEAN"
UNION
SELECT
  'PERSON:' || p.id::text as "id:ID(Person-ID)",
  p.id as "opdm_id:INT",
  p.name, p.image,
  p.gender,
  date_part('years', age(to_timestamp(date_2_ts(p.birth_date)))) as "age:int",
  p.birth_location as birth_location_str,
  null as labels,
  CASE WHEN p.death_date is null THEN 'true'
       ELSE 'false'
  END as "is_alive:BOOLEAN",
  'Persona'::text AS ":LABEL"
FROM popolo_person p
WHERE p.id NOT IN
      (select p.id
       from popolo_person as p
                LEFT JOIN popolo_classificationrel as cr on cr.object_id = p.id and cr.content_type_id = 33
                LEFT JOIN popolo_classification as c on c.id = cr.classification_id
       WHERE c.scheme = 'OPDM_PERSON_LABEL'
      )
;
\COPY (SELECT * FROM pv) TO 'nodes_persons.csv' WITH CSV HEADER


-- areas_parents.csv
CREATE TEMP VIEW apv as
SELECT
  md5(id::text || ':' || parent_id::text)::uuid as "id",
  'AREA:' || id::text as ":START_ID(Area-ID)",
  'AREA:' || parent_id::text as ":END_ID(Area-ID)",
  'PARTE_DI'::text as ":TYPE"
from popolo_area
where parent_id is not null
;
\COPY (SELECT * FROM apv) TO 'rels_areas_parents.csv' WITH CSV HEADER


-- organizations_parents.csv
CREATE TEMP VIEW opv as
SELECT
  md5(id::text || ':' || parent_id::text)::uuid as "id",
  'ORG:' || id::text as ":START_ID(Organization-ID)",
  'ORG:' || parent_id::text as ":END_ID(Organization-ID)",
  'PARTE_DI'::text as ":TYPE"
from popolo_organization
where parent_id is not null
;
\COPY (SELECT * FROM opv) TO 'rels_organizations_parents.csv' WITH CSV HEADER


-- organizations_areas_parents.csv
CREATE TEMP VIEW oapv as
SELECT
  md5(id::text || ':' || area_id::text)::uuid as "id",
  'ORG:' || id::text as ":START_ID(Organization-ID)",
  'AREA:' || area_id ::text as ":END_ID(Area-ID)",
  'ATTIVA_IN'::text as ":TYPE"
from popolo_organization
where area_id is not null
;
\COPY (SELECT * FROM oapv) TO 'rels_organizations_areas_parents.csv' WITH CSV HEADER


-- memberships.csv
CREATE TEMP VIEW mv as
SELECT
  md5(person_id::text || ':' || organization_id::text || ':' || label || ':' || coalesce(m.start_date, 'NULL')::text)::uuid as "id",
  'PERSON:' || person_id::text as ":START_ID(Person-ID)",
  'ORG:' || o.id::text as ":END_ID(Organization-ID)",
  replace(m.role, lower(c.descr), '') as role,
  m.label,
  string_agg(mc.descr, ', ' order by mc.descr) AS "topics",
  m.start_date as "start_date",
  m.end_date as "end_date",
  CASE WHEN m.end_date > current_date::text or m.end_date is null THEN 'true'
       ELSE 'false'
  END as "is_current:BOOLEAN",
  'INCARICO_IN'::text as ":TYPE"
FROM popolo_membership as m
LEFT JOIN popolo_organization as o on o.id=m.organization_id
LEFT JOIN popolo_classificationrel as cr on cr.object_id = o.id and cr.content_type_id=28
LEFT JOIN popolo_classification c on cr.classification_id=c.id
LEFT JOIN popolo_classificationrel as mcr on mcr.object_id = m.id and mcr.content_type_id=27
LEFT JOIN popolo_classification as mc on mcr.classification_id=mc.id
WHERE c.scheme='FORMA_GIURIDICA_OP'
GROUP BY m.id, o.id, c.descr
;
\COPY (SELECT * FROM mv) TO 'rels_memberships.csv' WITH CSV HEADER


-- personal_ownerships.csv
CREATE TEMP VIEW pov as
SELECT
  md5(owner_person_id::text || ':' || owned_organization_id::text || ':' || coalesce(start_date, 'NULL')::text)::uuid as "id",
  'PERSON:' || owner_person_id::text as ":START_ID(Person-ID)",
  'ORG:' || owned_organization_id::text as ":END_ID(Organization-ID)",
  percentage as "percentage:float",
  start_date as "start_date",
  end_date as "end_date",
  CASE WHEN end_date > current_date::text or end_date is null THEN 'true'
       ELSE 'false'
  END as "is_current:boolean",
  'DETIENE_QUOTA_DI'::text as ":TYPE"
FROM popolo_ownership
WHERE owner_organization_id is null
;
\COPY (SELECT * FROM pov) TO 'rels_personal_ownerships.csv' WITH CSV HEADER


-- organization_ownerships.csv
CREATE TEMP VIEW oov as
SELECT
  md5(owner_organization_id::text || ':' || owned_organization_id::text || ':' || coalesce(start_date, 'NULL'))::uuid as "id",
  'ORG:' || owner_organization_id::text as ":START_ID(Organization-ID)",
  'ORG:' || owned_organization_id::text as ":END_ID(Organization-ID)",
  percentage as "percentage:float",
  start_date as "start_date",
  end_date as "end_date",
  end_reason,
  CASE WHEN end_date > current_date::text or end_date is null THEN 'true'
       ELSE 'false'
  END as "is_current:BOOLEAN",
  'DETIENE_QUOTA_DI'::text as ":TYPE"
FROM popolo_ownership
WHERE owner_person_id is null
;
\COPY (SELECT * FROM oov) TO 'rels_organization_ownerships.csv' WITH CSV HEADER

-- appointments.csv
CREATE TEMP VIEW app as
SELECT
  md5(am.person_id::text || ':' || pm.person_id::text || ':' || pm.label || ':' || coalesce(pm.start_date, 'NULL')::text)::uuid as "id",
  'PERSON:' || am.person_id::text as ":START_ID(Person-ID)",
  'PERSON:' || pm.person_id::text as ":END_ID(Person-ID)",
  pm.role,
  pm.label,
  pm.start_date as "start_date",
  pm.end_date as "end_date",
  pm.end_reason,
  CASE WHEN pm.end_date > current_date::text or pm.end_date is null THEN 'true'
       ELSE 'false'
  END as "is_current:BOOLEAN",
  'HA_NOMINATO'::text as ":TYPE"
from popolo_membership pm, popolo_membership am
where pm.appointed_by_id is not null and pm.appointed_by_id=am.id
;
\COPY (SELECT * FROM app) TO 'rels_appointments.csv' WITH CSV HEADER

-- topics.csv
CREATE TEMP VIEW topics as
SELECT
    'TOPIC:' || t.id::text as "id:ID(Topic-ID)",
    t.id as "opdm_id:INT",
    t.descr as name,
    'Tema'::text AS ":LABEL"
FROM popolo_classification as t
WHERE t.scheme='OPDM_TOPIC_TAG'
;
\COPY (SELECT * FROM topics) TO 'nodes_topics.csv' WITH CSV HEADER

-- organizations_topics.csv
CREATE TEMP VIEW orgs_topics as
SELECT DISTINCT
    md5(ot.object_id::text || ':' || ot.classification_id::text )::uuid as "id",
    'ORG:' || ot.object_id::text as ":START_ID(Organization-ID)",
    'TOPIC:' || ot.classification_id::text as ":END_ID(Topic-ID)",
    'SI_OCCUPA_DI'::text as ":TYPE"
FROM popolo_classificationrel as ot
LEFT JOIN popolo_classification t on t.id=ot.classification_id
WHERE t.scheme='OPDM_TOPIC_TAG' AND ot.content_type_id = 28
;
\COPY (SELECT * FROM orgs_topics) TO 'rels_organizations_topics.csv' WITH CSV HEADER

-- organizations_relationships.csv
CREATE TEMP VIEW org_rels as
SELECT
  md5(source_organization_id::text || ':' || dest_organization_id::text || ':' || c.descr || ':' || coalesce(r.start_date, 'NULL')::text)::uuid as "id",
  'ORG:' || r.source_organization_id::text as ":START_ID(Organization-ID)",
  'ORG:' || r.dest_organization_id::text as ":END_ID(Organization-ID)",
  r.weight as "weight:int",
  c.descr as "classification",
  r.start_date,
  r.end_date,
  CASE WHEN r.end_date > current_date::text or r.end_date is null THEN 'true'
       ELSE 'false'
  END as "is_current:BOOLEAN",
  'IN_RELAZIONE_DI'::text as ":TYPE"
FROM popolo_organizationrelationship r
LEFT JOIN popolo_classification c on r.classification_id=c.id
WHERE c.scheme='OP_TIPO_RELAZIONE_ORG'
;\COPY (SELECT * FROM org_rels) TO 'rels_organizations_relations.csv' WITH CSV HEADER
