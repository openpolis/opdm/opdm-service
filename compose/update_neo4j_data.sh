if [[ -f /root/.linkurious_env ]]; then
    source /root/.linkurious_env
    echo LINKURIOUS env variables read
else
    echo "No variables set. ERROR."
    exit 1
fi

if [ -z ${NEO4J_USER} ]; then
  echo "NEO4J_USER must be set. ERROR."
  exit 1
fi

if [[ -z ${NEO4J_PASSWORD} ]]; then
    echo "NEO4J_PASSWORD must be set. ERROR."
    exit 1
fi

if [[ -z ${LINKURIOUS_USER} ]]; then
    echo "LINKURIOUS_USER must be set. ERROR."
    exit 1
fi

if [[ -z ${LINKURIOUS_PASSWORD} ]]; then
    echo "LINKURIOUS_PASSWORD must be set. ERROR."
    exit 1
fi

if [[ -z ${LINKURIOUS_SOURCE_KEY} ]]; then
    echo "LINKURIOUS_SOURCE_KEY must be set. ERROR."
    exit 1
fi

if [[ -z ${LINKURIOUS_REINDEX} ]]; then
  echo "Setting LINKURIOUS_REINDEX to true."
  export LINKURIOUS_REINDEX=true
fi

if [[ -z ${NEO4J_DOCKER_VOLUME_PATH} ]]; then
  echo "Setting NEO4J_DOCKER_VOLUME_PATH to default /var/lib/... "
  export NEO4J_DOCKER_VOLUME_PATH=/var/lib/docker/volumes/opdmservice_neo4j_data/_data/databases
fi

echo " "
echo " ---------------------------------------"
echo "| Starting update procedure using:      "
echo "| using:                                "
echo "| LK_USER: ${LINKURIOUS_USER}           "
echo "| LK_PASSWORD: ${LINKURIOUS_PASSWORD}   "
echo "| N4J_USER: ${NEO4J_USER}               "
echo "| N4J_PASSWORD: ${NEO4J_PASSWORD}       "
echo "| SOURCE_KEY: ${LINKURIOUS_SOURCE_KEY}  "
echo "| REINDEX: ${LINKURIOUS_REINDEX}        "
echo "| NEO4J_DOCKER_VOLUME_PATH: ${NEO4J_DOCKER_VOLUME_PATH} "
echo " ---------------------------------------"

# csv_extraction, csv_split & csv_transfer
echo " "
echo " -----------------------------"
echo "| Fetching data from postgres |"
echo " -----------------------------"
docker exec -t opdm-service_postgres bash -c "cd /var/lib/postgresql/neo4j_import && ./extract_neo4j_csv.sh"

# neo4j_import_data
echo " "
echo " -------------------------------------"
echo "| Importing data into new neo4j graph |"
echo " -------------------------------------"
docker exec -t -e HEAP_SIZE=2G \
  opdm-service_neo4j neo4j-admin import --database=graph_new.db --multiline-fields=true \
    --nodes="import/nodes_areas_header.csv,import/nodes_areas.csv" \
    --nodes="import/nodes_persons_header.csv,import/nodes_persons.csv" \
    --nodes="import/nodes_organizations_header.csv,import/nodes_organizations.csv" \
    --nodes="import/nodes_topics_header.csv,import/nodes_topics.csv" \
    --relationships="import/rels_memberships_header.csv,import/rels_memberships.csv" \
    --relationships="import/rels_areas_parents_header.csv,import/rels_areas_parents.csv" \
    --relationships="import/rels_organizations_parents_header.csv,import/rels_organizations_parents.csv" \
    --relationships="import/rels_organizations_areas_parents_header.csv,import/rels_organizations_areas_parents.csv" \
    --relationships="import/rels_personal_ownerships_header.csv,import/rels_personal_ownerships.csv" \
    --relationships="import/rels_organization_ownerships_header.csv,import/rels_organization_ownerships.csv" \
    --relationships="import/rels_appointments_header.csv,import/rels_appointments.csv" \
    --relationships="import/rels_organizations_topics_header.csv,import/rels_organizations_topics.csv" \
    --relationships="import/rels_organizations_relations_header.csv,import/rels_organizations_relations.csv"


# neo4j_swap_graph_data
pushd $NEO4J_DOCKER_VOLUME_PATH || exit
rm -rf graph_old.db
chown -R 101:101 graph_new.db
mv graph.db graph_old.db
mv graph_new.db graph.db
popd || exit

# neo4j_restart
echo " "
echo " --------------------------------------"
echo "| Restarting neo4j ...                 |"
echo " --------------------------------------"
docker restart opdm-service_neo4j


if [ $LINKURIOUS_REINDEX == true ]; then

  # linkurious_reconnect
  curl -s -c cookies -X POST -d "usernameOrEmail=${LINKURIOUS_USER}" -d "password=${LINKURIOUS_PASSWORD}" "https://linkurious.openpolis.io/api/auth/login" > /dev/null
  echo " "
  echo " -------------------------------------------"
  echo "| Reconnecting linkurious to the datasource |"
  echo " -------------------------------------------"
  export CONFIG_INDEX=$(curl -s -b cookies "https://linkurious.openpolis.io/api/admin/sources" | jq ".[] | select(.key == \"${LINKURIOUS_SOURCE_KEY}\") | .configIndex")
  curl -s -X POST -b cookies "https://linkurious.openpolis.io/api/admin/source/${CONFIG_INDEX}/connect"
  echo "Waiting for connection ..."
  status=$(curl -s -b cookies "https://linkurious.openpolis.io/api/admin/sources" | jq ".[] | select(.key == \"${LINKURIOUS_SOURCE_KEY}\") | .state")
  until [ $status == "\"ready\"" ] || [ $status == "\"needReindex\"" ]
  do
      echo $status
      sleep 5;
      status=$(curl -s -b cookies "https://linkurious.openpolis.io/api/admin/sources" | jq ".[] | select(.key == \"${LINKURIOUS_SOURCE_KEY}\") | .state")
  done
  echo "... reconnected!"

  # rebuild neo4j alternative indexes
  docker exec -it opdm-service_neo4j cypher-shell --username ${NEO4J_USER} --password ${NEO4J_PASSWORD} \
	  "call db.index.fulltext.createNodeIndex('myAlternativeNodeIdIndex', ['Area', 'Organizzazione', 'Persona', 'Tema'], ['id'], {analyzer: 'keyword'})"
  echo "Generating myAlternativeNodeIdIndex"
  sleep 5

  docker exec -it opdm-service_neo4j cypher-shell --username ${NEO4J_USER} --password ${NEO4J_PASSWORD} \
	  "call db.index.fulltext.createRelationshipIndex('myAlternativeEdgeIdIndex', ['ATTIVA_IN', 'DETIENE_QUOTA_DI', 'HA_NOMINATO', 'INCARICO_IN', 'PARTE_DI', 'SI_OCCUPA_DI', 'IN_RELAZIONE_DI'], ['id'], {analyzer: 'keyword'})"
  echo "Generating myAlternativeEdgeIdIndex"
  sleep 5



  # linkurious_reindex
  echo " "
  echo " ----------------------------"
  echo "| Reindexing linkurious data |"
  echo " ----------------------------"
  curl -s -X POST -b cookies "https://linkurious.openpolis.io/api/${LINKURIOUS_SOURCE_KEY}/search/index"
  while [ $(curl -s -b cookies "https://linkurious.openpolis.io/api/${LINKURIOUS_SOURCE_KEY}/search/status" | jq .indexing) == "\"ongoing\"" ]
  do
      curl -s -b cookies "https://linkurious.openpolis.io/api/${LINKURIOUS_SOURCE_KEY}/search/status" | jq '"\(.indexing_status) - \(.indexing_progress)"'
      sleep 30;
  done
  rm cookies
fi

echo " "
echo " ------------"
echo "|   Ready!   |"
echo " ------------"
