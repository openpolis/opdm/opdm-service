
# OPDM Changelog

All notable changes to the data in OPDM will be documented here.

The format is lousely based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)

## 2021-08-31
- parliamentary memberships, groups, committeestaken from sparql endpoints

## 2020-04-10

- administrative history of an organ from MININT:
  - memberships' end date and reason
  - electoral lists

## 2019-02-12

- public organization's ownerships (and owned organizations) from ATOKA
- memberships into organizations imported from ATOKA
   
## 2019-02-08

- minister's dipartimenti and equivalent organizations


## 2019-01-09

- person's other_names synced from similarities 

