# Openpolis DataManager Service

The Openpolis DataManager service fetch interesting data around 
the world, mixes and matches it and provides it back to all 
those in need of it.

See `project/` path for the source code of this project.

See `docs/` for documentation.


## Development

Development is performed on local workstations, without requiring
Docker.

This allows developers to develop
against *local* versions of the `django-popolo` and `opdm-etl`
packages.

Environment variables are read from from the `.env` file.
This file is not present in the repository,
and it should be generated starting from `.env.sample`.
Variables in the linux environent at execution time override those
read from `.env`.

A running postgres database is required somewhere, as well as a running 
redis-server (to store sessions).
Connections to these servers should be defined in the `.env` file, as
`DATABASE_URL` and `REDIS_URL` values.

To start developing, clone this repository, then:

    cd config
    
    # generate (and modify) the .env file
    cp .env.sample .env
    
    # install and configure pyenv

    # install python 3.6.x version, using pyenv
    pyenv install 3.6.5

    # define python 3.6.5 as local, in the directory
    pyenv local 3.6.5

    # create and activate a virtualenv
    virtualenv venv
    source venv/bin/activate
    
    # install requirements
    pip install pip-tools
    pip install --upgrade pip
    pip-compile
    pip-sync requirements.txt

    # when developing them, re-install packages from local dirs
    pip uninstall django-popolo && pip install -e /Users/gu/Workspace/django-popolo
    pip uninstall opdm-etl && pip install -e /Users/gu/Workspace/opdm-etl
    pip uninstall op-task-manager-project && pip install -e /Users/gu/Workspace/op-task-manager-project
    
    # install other useful packages for devlopers (docs-writing, ipython, jupyter ...)
    pip install Sphinx sphinx_rtd_theme ipython jupyter safety 

    # create database
    createdb -Upostgres opdm

    python manage.py migrate
    python manage.py createsuperuser
    python manage.py compilemessages -l it 

    # now the server can be run
    python manage.py runserver
    
    
### Restore development session

`tmuxinator` can be used to restore a development session. 
The `.tmuxinator.yml` file contains all information needed to start
the session.


## Deploy

The stack is defined in `docker-compose.yml` file.
It uses:

  - a django app (web)
  - a frontend web server (nginx)
  - a storage RDBMS (postgres)
  - a cache system, at least for sessions (redis)


### Deploy on a docker-machine server

Environment variables need to be defined:

    export POSTGRES_DB=opdm
    export POSTGRES_USER=opdm
    export POSTGRES_PASS=opdm_pass

This can be done using the dmctl utility:

    dm ls # list of all docker machines 
    dmctl set op-staging-1 ls # env setup for the op-staging-1 machine

The web image must be built

    docker build --compress -t openpolis/opdm/opdm-service:latest # build image


To deploy on a local or on a remote machine (using `docker-machine`):

    dc up -d # start all services as daemon on the remote machine 


dc stands for `docker-compose` here, a proper installation should
automatically provide you the shortcut.

To restart all services, after a change in `docker-compose.yml`:

    dc up -d 

**IMPORTANT**: The web app image need to be rebuilt whenever there are changes
in the source code.


To enter a shell in the container of the web service:

    dc exec web bash

This can be done also to enter the other containers, but it shouldn't
be necessary.

To stop all services and remove all containers:

    dc down

Postgresql data are **persisted** under a docker `volume`, and
**won't be lost** after a shut down of all services.

Note: On OSX the volumes are under the inner hosting machine.
Volumes can be inspected with the `docker volume` command.

See [Docker Compose Documentation](https://docs.docker.com/compose/),
to see other commands.

The source of inspiration for *dockerizing* this django app was:
https://www.capside.com/labs/deploying-full-django-stack-with-docker-compose/


### CI/CD Integration with GitLab

When a push to the gitlab.depp.it repository is performed:

- tests [^1] are performed on the runner
- if tests succed and branch is `develop`, deploy latest revision to [`staging`][opdm-service environments] using docker compose
- if tests succed and branch is `master`, deploy latest revision to [`production`][opdm-service environments] using docker compose


## Testing

To tests the service:

    python manage.py test --settings=config.settings.test

To extract a [``coverage``][coverage.py] report:

    coverage run manage.py test --settings=config.settings.test
    coverage report


## License and Authors

See LICENSE.txt for the license this software is released under.
See authors in CONTRIBUTORS.txt



[^1]: Unittests,
    [flake8](http://flake8.pycqa.org/) for common errors and PEP8 compliancy,  
    [safety](https://pyup.io/safety/) for security checks by pyup

[opdm-service environments]: https://gitlab.depp.it/openpolis/opdm/opdm-service/environments
[coverage.py]: https://coverage.readthedocs.io
