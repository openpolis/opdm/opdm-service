import hashlib
import json

from django.db import models
from django.db.models import JSONField
from django.utils.translation import ugettext_lazy as _
from popolo.behaviors.models import GenericRelatable, Timestampable


class AKA(GenericRelatable, Timestampable, models.Model):
    """AKA (Also Known As)
    Used to store the search that produced the similarities, and
    the context they were produced in, so that it can be re-imported when managed.

    """
    search_params = JSONField(
        _("search parameters"),
        help_text=_("The search parameters, as dictionary")
    )

    n_similarities = models.PositiveSmallIntegerField(
        _("n of similarities"),
        blank=True,
        null=True,
    )

    loader_context = JSONField(
        _("loader context"),
        blank=True,
        null=True,
        help_text=_("a JSON representation of the item and the context it was being imported into")
    )

    is_resolved = models.BooleanField(
        _("is associated"),
        default=False,
        help_text=_("Whether the aka has been resolved (associated to an object)")
    )

    hashed = models.CharField(
        max_length=64, unique=True,
        default='',
        null=True, blank=True,
        help_text=_("Hash used to enforce uniqueness of json fields, and avoid postgres error on index too big")
    )

    data_source_url = models.URLField(
        _("url"),
        max_length=350,
        blank=True, null=True,
        help_text=_("The data source URL")
    )

    class Meta:
        verbose_name = _("AKA similarity")
        verbose_name_plural = _("AKA similarities")

    def __str__(self):
        return "{0} similarities for {1}".format(
            self.n_similarities, self.search_params
        )

    def save(self, **kwargs):
        text = json.dumps(self.search_params) + json.dumps(self.loader_context)
        self.hashed = hashlib.sha256(text.encode('utf-8')).hexdigest()

        super(AKA, self).save(**kwargs)
