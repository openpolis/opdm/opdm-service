from django.contrib import admin

from project.akas.models import AKA


class AKAAdmin(admin.ModelAdmin):
    model = AKA
    list_display = ('search_params', 'is_resolved')
    search_fields = ('search_params', )


admin.site.register(AKA, AKAAdmin)
