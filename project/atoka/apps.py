from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AtokaConfig(AppConfig):

    name = 'project.atoka'
    verbose_name = _("Atoka")
