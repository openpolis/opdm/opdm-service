from unittest.mock import MagicMock

from ooetl import ETL
from popolo.tests.factories import OrganizationFactory

from project.tasks.tests.etl import SolrETLTest
from atokaconn import AtokaConn
from project.atoka.etl.extractors import AtokaOwnershipsExtractor, AtokaEconomicsExtractor, AtokaPeopleExtractor
from project.atoka.etl.transformations import AtokaOrganizationEconomicsTransformation
from project.atoka.etl.transformations.persons_memberships \
    import AtokaPersonsMembershipsOwnershipsFromOrganizationsTransformation
from project.atoka.tests.etl.atoka_mocks import (
    get_companies_tax_ids_batch, get_companies_atoka_ids_batch,
    get_roles_atoka_ids_batch, get_companies_economics_extractor, get_companies_for_economics_transformation,
    get_companies_for_memberships_transformation, get_people_tax_ids_batch)
from project.tasks.etl.extractors import ListExtractor
from project.tasks.etl.loaders import DummyLoader


class AtokaETLTest(SolrETLTest):
    """Tests the Atoka extractions and transformation classes
    """

    def test_ownerships_extractor(self):
        """Test extraction of ownerships works correctly
        """

        # mock atoka_conn method for cciaa or govTypes results
        AtokaConn.get_companies_from_tax_ids = MagicMock(
            side_effect=get_companies_tax_ids_batch,
            status_code=200,
            ok=True
        )

        AtokaConn.get_companies_from_atoka_ids = MagicMock(
            side_effect=get_companies_atoka_ids_batch,
            status_code=200,
            ok=True
        )

        AtokaConn.get_roles_from_atoka_ids = MagicMock(
            side_effect=get_roles_atoka_ids_batch,
            status_code=200,
            ok=True
        )

        res = AtokaOwnershipsExtractor(
            ['02438750586', '00008010803', '00031500945', '00031730948', '00033120437', '00034670943']
        ).extract()['results']

        self.assertEqual(type(res), list)
        self.assertEqual(len(res), 5)

        c = res[next(i for i, v in enumerate(res) if v['tax_id'] == '00008010803')]
        self.assertEqual(c['atoka_id'], 'b248111d6667')
        self.assertEqual(len(c['other_atoka_ids']), 0)
        self.assertEqual(c['name'], "COMUNE DI CINQUEFRONDI")
        self.assertEqual(c['legal_form'], 'Soggetto non iscritto al Registro Imprese')
        self.assertEqual(c['rea'], None)
        self.assertEqual(c['cciaa'], None)
        self.assertEqual('shares_owned' in c, True)
        self.assertEqual(len(c['shares_owned']), 1)
        sho = c['shares_owned'][0]
        self.assertEqual(sho['percentage'], 12.5)
        self.assertEqual(sho['atoka_id'], 'c17fceac95f3')
        self.assertEqual(sho['last_updated'], "2005-05-20")

        self.assertEqual(sho['founded'], "2004-07-14")
        self.assertEqual(sho['name'], "CONSORZIO FORESTALE ASPRO-SERRE")
        self.assertEqual(sho['rea'], "159715")
        self.assertEqual(sho['cciaa'], "RC")
        self.assertEqual(sho['tax_id'], "91009990804")
        self.assertEqual(sho['legal_form'], "Consorzio")

        self.assertEqual('roles' in sho, True)

    def test_memberships_transformation(self):
        """Test that the structure of the extracted information matches requirements

        Memberships are distributed among persons
        """
        o1 = OrganizationFactory.create(
            name="COMUNE DI CINQUEFRONDI",
            identifier="00008010803",
            classification="Soggetto non iscritto al Registro Imprese"
        )
        o1.add_identifier(scheme='CF', identifier='00008010803')

        o2 = OrganizationFactory.create(
            name="CONSORZIO FORESTALE ASPRO-SERRE",
            identifier="91009990804",
            classification="Consorzio"
        )
        o2.add_identifier(scheme='CF', identifier='91009990804')

        etl = ETL(
            extractor=ListExtractor(get_companies_for_memberships_transformation()),
            transformation=AtokaPersonsMembershipsOwnershipsFromOrganizationsTransformation(),
            loader=DummyLoader(),
            log_level=0,
            # source="http://api.atoka.it"
            )
        etl.extract().transform()

        res = etl.processed_data
        self.assertGreater(len(res), 0)

        c = res[next(i for i, v in enumerate(res) if v['family_name'] == 'Valenzise')]
        self.assertEqual(len(c['memberships']), 1)

        c = res[next(i for i, v in enumerate(res) if v['family_name'] == 'Nicolaci')]
        self.assertEqual(len(c['memberships']), 2)

    def test_economics_extractor(self):
        """Test that the structure of the extracted information matches requirements
        """

        # mock atoka_conn method for cciaa or govTypes results
        AtokaConn.get_items_from_ids = MagicMock(
            side_effect=get_companies_economics_extractor,
            status_code=200,
            ok=True
        )

        # extract info
        res = AtokaEconomicsExtractor(
            ['6da785b3adf2', '38e098baa0f9']
        ).extract()['results']

        self.assertEqual(len(res), 2)

        # test complete economics info
        c = res[next(i for i, v in enumerate(res) if v['id'] == '38e098baa0f9')]
        self.assertEqual(c['id'], '38e098baa0f9')
        self.assertEqual(c['name'], 'DEPP SRL')
        ce = c['economics']
        self.assertEqual('balanceSheets' in ce, True)
        self.assertEqual(len(ce['balanceSheets']) > 1, True)
        self.assertEqual('employees' in ce, True)
        self.assertEqual(len(ce['employees']) > 1, True)
        etl = ETL(
            extractor=ListExtractor(get_companies_for_memberships_transformation()),
            transformation=AtokaOrganizationEconomicsTransformation(),
            loader=DummyLoader(),
            log_level=0,
            # source="http://api.atoka.it"
            )
        etl.extract().transform()

    def test_economics_transformation(self):
        """Test that the structure of the extracted information matches requirements
        """
        etl = ETL(
            extractor=ListExtractor(get_companies_for_economics_transformation()),
            transformation=AtokaOrganizationEconomicsTransformation(),
            loader=DummyLoader(),
            log_level=0,
            # source="http://api.atoka.it"
            )
        etl.extract().transform()
        # test complete economics info
        res = etl.processed_data
        c = res[next(i for i, v in enumerate(res) if v['tax_id'] == '09988761004')]
        self.assertEqual(c['atoka_id'], '38e098baa0f9')
        self.assertEqual(c['name'], 'DEPP SRL')
        self.assertEqual(c['is_public'], False)
        self.assertEqual(c['capital_stock'], 10000)
        self.assertEqual(c['revenue'], 471000)
        self.assertEqual(c['mol'], 29000)
        self.assertEqual(c['employees'], 9)
        self.assertGreaterEqual(len(c['historical_values']), 4)
        self.assertEqual(c['historical_values'][0]['year'], 2017)

        sh = c['historical_values'][0]
        self.assertEqual(sh['assets'], 172000)
        self.assertEqual(sh['costs'], 442000)
        self.assertEqual(sh['ebitda'], 25000)
        self.assertEqual(sh['mol'], 29000)
        self.assertEqual(sh['production'], 471000)
        self.assertEqual(sh['profit'], 12000)
        self.assertEqual(sh['staff_costs'], 157000)
        self.assertEqual(sh['purchases'], 0)
        self.assertEqual(sh['employees'], 7)
        self.assertEqual(sh['services_and_tp_goods_charges'], 285000)
        self.assertEqual(sh['net_financial_position'], 7000)

        # test restricted economics info
        c = res[next(i for i, v in enumerate(res) if v['tax_id'] == '02241890223')]
        self.assertEqual(c['atoka_id'], '6da785b3adf2')
        self.assertEqual(c['name'], 'SPAZIODATI S.R.L.')
        self.assertEqual(c['is_public'], False)
        self.assertEqual(c['capital_stock'], 21638)
        self.assertGreaterEqual(len(c['historical_values']), 4)
        self.assertEqual(c['historical_values'][0]['year'], 2017)

        sh = c['historical_values'][0]
        self.assertEqual(sh.get('assets', None), None)
        self.assertEqual(sh.get('costs', None), None)
        self.assertEqual(sh['employees'], 26)

    def _test_people_extractor(self):
        """Test extraction of people works correctly
        """

        #
        # TODO: setup mock functions for all atoka calls before adding this to the test suite
        #

        # mock atoka_conn method for cciaa or govTypes results
        AtokaConn.get_people_from_tax_ids = MagicMock(
            side_effect=get_people_tax_ids_batch,
            status_code=200,
            ok=True
        )

        AtokaConn.get_companies_from_atoka_ids = MagicMock(
            side_effect=get_companies_atoka_ids_batch,
            status_code=200,
            ok=True
        )

        AtokaConn.get_roles_from_atoka_ids = MagicMock(
            side_effect=get_roles_atoka_ids_batch,
            status_code=200,
            ok=True
        )

        tax_ids = ['BFFSFN83P06F205V', 'BNSLRT61T26C076F']

        res = AtokaPeopleExtractor(tax_ids, ).extract()['results']

        self.assertEqual(type(res), dict)
        self.assertEqual(len(res.keys()), 3)

        # Buffagni has no properties or memberships in companies
        # thus it's not extracted (it's filtered)
        res_people = res['people']
        self.assertNotIn('BFFSFN83P06F205V', res['people'])

        c = res_people['BNSLRT61T26C076F']
        self.assertEqual(c['name'], "Alberto Bonisoli")
        self.assertEqual(len(c['organizations']), 1)
        o = next(iter(c['organizations'].values()))
        self.assertEqual(len(o['shares_owned']), 1)

        self.assertEqual('last_updated' in o['shares_owned'][0], True)
        self.assertEqual('percentage' in o['shares_owned'][0], True)

        res_cpeople = res['c_people']
        self.assertEqual(len(res_cpeople), 2)

        # TODO: this company seems to have unknwon revenue according to Atoka
        res = AtokaPeopleExtractor(tax_ids, min_revenue=100000).extract()['results']
        c = res['people']['pNqgRhMES6xW2rhZa3']
        self.assertEqual(len(c['organizations']), 1)
