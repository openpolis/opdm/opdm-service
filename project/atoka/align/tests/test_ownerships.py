from datetime import datetime
import logging

import django.test
from ooetl import ETL
from popolo.tests.factories import OrganizationFactory, PersonFactory

from project.atoka.align.management.commands.align_atoka_close_ownerships import TransformShareholdersIDs, \
    PersonalOwnershipsCloser, TransformSharesOwnedIDs, OrganizationOwnershipsCloser
from project.atoka.models import OrganizationEconomics
from project.tasks.etl.extractors import ListExtractor
from project.tasks.etl.loaders import DummyLoader


from faker import Faker
fake = Faker("it_IT")
Faker.seed(0)


class ClosePersonalOwnership(django.test.TestCase):

    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.atoka.align.management.commands"
        )
        logger.handlers = []

    @staticmethod
    def prepare_data():
        """Prepare data in OPDM test DB"""
        org = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        org.add_identifier(
            fake.pystr(max_chars=20), 'ATOKA_ID'
        )
        org.add_classification(
            scheme='FORMA_GIURIDICA_OP', code='1301', descr=org.classification
        )

        p1 = PersonFactory.create()
        p1.add_identifier(fake.ssn(), 'CF')
        p1.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        org.add_owner(
            p1,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        return org, p1

    def test_transformations_transforms_ids(self):
        """ATOKA IDS are transformed correctly into OPDM ids"""

        org, p1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p1.ownerships.first().percentage,
                        "last_updated": p1.ownerships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 1)
        self.assertTrue(etl.processed_data[0]['organization_id'] == org.id)
        self.assertTrue('shareholders' in etl.processed_data[0])
        self.assertTrue(len(etl.processed_data[0]['shareholders']) == 1)
        self.assertTrue('last_updated' in etl.processed_data[0]['shareholders'][0])
        self.assertTrue(
            etl.processed_data[0]['shareholders'][0]['last_updated'] ==
            atoka_extraction[0]['shareholders_people'][0]['last_updated']
        )
        self.assertTrue(etl.processed_data[0]['shareholders'][0]['person_id'] == p1.id)

    def test_transformation_skips_orgs_with_no_known_persons(self):

        org, p1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": fake.first_name(),
                            "family_name": fake.last_name(),
                            "gender": 'M',
                            "birth_date": datetime.strftime(fake.date_between("-60y", "-25y"), "%Y-%m-%d"),
                            "birth_location": fake.city(),
                            "tax_id": fake.ssn(),
                            "atoka_id": fake.pystr(max_chars=20)
                        },
                        "percentage": fake.pyfloat(
                            min_value=0, max_value=100
                        ),
                        "last_updated": datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 0)

    def test_transformation_skips_unknown_person(self):

        org, p1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p1.ownerships.first().percentage,
                        "last_updated": p1.ownerships.first().start_date
                    },
                    {
                        "person": {
                            "given_name": fake.first_name(),
                            "family_name": fake.last_name(),
                            "gender": 'M',
                            "birth_date": datetime.strftime(fake.date_between("-60y", "-25y"), "%Y-%m-%d"),
                            "birth_location": fake.city(),
                            "tax_id": fake.ssn(),
                            "atoka_id": fake.pystr(max_chars=20)
                        },
                        "percentage": fake.pyfloat(
                            min_value=0, max_value=100
                        ),
                        "last_updated": datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 1)
        self.assertTrue('shareholders' in etl.processed_data[0])
        self.assertTrue(len(etl.processed_data[0]['shareholders']) == 1)

    def test_close_ownership(self):

        org, p1 = self.prepare_data()

        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        org.add_owner(
            p2,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p1.ownerships.first().percentage,
                        "last_updated": p1.ownerships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=PersonalOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships_as_owned.count() == 2)
        self.assertTrue(org.ownerships_as_owned.filter(end_date__isnull=False).count() == 1)

    def test_close_ownership_dry_run(self):

        org, p1 = self.prepare_data()

        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        org.add_owner(
            p2,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p1.ownerships.first().percentage,
                        "last_updated": p1.ownerships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=PersonalOwnershipsCloser(dry_run=True),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships_as_owned.count() == 2)
        self.assertTrue(org.ownerships_as_owned.filter(end_date__isnull=False).count() == 0)

    def test_no_ownerships_to_update(self):
        """
        ATOKA contains the same data of OPDM
        No ownerships are closed.

        :return:
        """
        org, p1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p1.ownerships.first().percentage,
                        "last_updated": p1.ownerships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=PersonalOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships_as_owned.filter(owner_person=p1).count() == 1)
        self.assertTrue(org.ownerships_as_owned.filter(owner_person=p1, end_date__isnull=False).count() == 0)

    def test_swap_ownership_with_non_existing_in_opdm(self):
        """
        in OPDM org is owned by p1 and p2,
        ATOKA indicates that the org is owned by p1 and p3

        Since p3 is not in OPDM, the ATOKA record is filtered out,
        thus p2's ownership is closed (it's in OPDM, but not in ATOKA).

        NOTE: that's a problem, so the close_ownerships task
        must always be launched after importing new ownerships).

        :return:
        """
        org, p1 = self.prepare_data()

        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        org.add_owner(
            p2,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p1.ownerships.first().percentage,
                        "last_updated": p1.ownerships.first().start_date
                    },
                    {
                        "person": {
                            "given_name": fake.first_name(),
                            "family_name": fake.last_name(),
                            "gender": 'M',
                            "birth_date": datetime.strftime(fake.date_between("-60y", "-25y"), "%Y-%m-%d"),
                            "birth_location": fake.city(),
                            "tax_id": fake.ssn(),
                            "atoka_id": fake.pystr(max_chars=20)
                        },
                        "percentage": fake.pyfloat(
                            min_value=1, max_value=100
                        ),
                        "last_updated": datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=PersonalOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships_as_owned.count() == 2)
        self.assertTrue(org.ownerships_as_owned.filter(end_date__isnull=False).count() == 1)

    def test_swap_ownership_with_existing_in_opdm_older_than_6m(self):
        """
        in OPDM, org is owned by p1 and p2,
        ATOKA indicates that org is owned by p2

        Since p1 is not in ATOKA p1's ownership is closed,
        since the change happened more thant 6 months ago,
        p1 ownerhips end_date is set to today's date

        :return:
        """
        org, p1 = self.prepare_data()

        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        org.add_owner(
            p2,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p2.given_name,
                            "family_name": p2.family_name,
                            "gender": p2.gender,
                            "birth_date": p2.birth_date,
                            "birth_location": p2.birth_location,
                            "tax_id": p2.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p2.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p2.ownerships.first().percentage,
                        "last_updated": p2.ownerships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=PersonalOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships_as_owned.count() == 2)
        self.assertTrue(org.ownerships_as_owned.filter(end_date__isnull=False).count() == 1)

        o1 = org.ownerships_as_owned.get(owner_person=p1)
        o2 = org.ownerships_as_owned.get(owner_person=p2)
        self.assertTrue(
            o1.end_date != o2.start_date
        )
        self.assertTrue(
            o1.end_date == datetime.strftime(datetime.now(), '%Y-%m-%d')
        )
        self.assertTrue(
            "New (changed) ownership found" not in o1.end_reason
        )

    def test_swap_ownership_with_existing_in_opdm_before_6m(self):
        """
        in OPDM, org is owned by p1 and p2,
        ATOKA indicates that org is owned by p2

        Since p1 is not in ATOKA p1's ownership is closed,

        The change happened less than 6 months ago,
        p1 ownerhips end_date is set to p2 ownerships' start_date

        :return:
        """
        org, p1 = self.prepare_data()

        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        org.add_owner(
            p2,
            start_date=datetime.strftime(fake.date_between("-5M", "-1M"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p2.given_name,
                            "family_name": p2.family_name,
                            "gender": p2.gender,
                            "birth_date": p2.birth_date,
                            "birth_location": p2.birth_location,
                            "tax_id": p2.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p2.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p2.ownerships.first().percentage,
                        "last_updated": p2.ownerships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=PersonalOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships_as_owned.count() == 2)
        self.assertTrue(org.ownerships_as_owned.filter(end_date__isnull=False).count() == 1)

        o1 = org.ownerships_as_owned.get(owner_person=p1)
        o2 = org.ownerships_as_owned.get(owner_person=p2)
        self.assertTrue(
            o1.end_date == o2.start_date,
            f"o1.end_date: {o1.end_date}, o2.start_date: {o2.start_date}"
        )
        self.assertTrue(
            "New (changed) ownership found" in o1.end_reason
        )

    def test_end_date_before_start_date(self):
        """
        ATOKA wants to set an end_date conflicting with an existing start_date
        No ownerships are closed.

        :return:
        """
        org, p1 = self.prepare_data()

        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        org.add_owner(
            p2,
            start_date=datetime.strftime(fake.date_between("-6M", "-1M"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        o1 = org.ownerships_as_owned.get(owner_person=p1)
        o1.start_date = datetime.strftime(fake.date_between("-3M", "-1M"), "%Y-%m-%d")
        o1.save()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shareholders_people": [
                    {
                        "person": {
                            "given_name": p2.given_name,
                            "family_name": p2.family_name,
                            "gender": p2.gender,
                            "birth_date": p2.birth_date,
                            "birth_location": p2.birth_location,
                            "tax_id": p2.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p2.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "percentage": p2.ownerships.first().percentage,
                        "last_updated": p1.ownerships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformShareholdersIDs(),
            loader=PersonalOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships_as_owned.count() == 2)
        self.assertTrue(org.ownerships_as_owned.filter(end_date__isnull=False).count() == 0)


class CloseOrganizationOwnership(django.test.TestCase):

    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.atoka.align.management.commands"
        )
        logger.handlers = []

    @staticmethod
    def prepare_data():
        """Prepare data in OPDM test DB"""
        org = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        org.add_identifier(
            fake.pystr(max_chars=20), 'ATOKA_ID'
        )
        org.add_classification(
            scheme='FORMA_GIURIDICA_OP', code='1301', descr=org.classification
        )

        ow1 = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        ow1.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        ow1.add_owner(
            org,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        return org, ow1

    def test_transformation_transforms_ids(self):
        """ATOKA IDS are transformed correctly into OPDM ids"""

        org, ow1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow1.name,
                        "atoka_id": ow1.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow1).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow1).first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 1)
        self.assertTrue(etl.processed_data[0]['organization_id'] == org.id)
        self.assertTrue('shares_owned' in etl.processed_data[0])
        self.assertTrue(len(etl.processed_data[0]['shares_owned']) == 1)
        self.assertTrue(etl.processed_data[0]['shares_owned'][0]['organization_id'] == ow1.id)
        self.assertTrue('last_updated' in etl.processed_data[0]['shares_owned'][0])
        self.assertTrue(
            etl.processed_data[0]['shares_owned'][0]['last_updated'] ==
            atoka_extraction[0]['shares_owned'][0]['last_updated']
        )

    def test_transformation_skips_orgs_with_no_known_persons(self):

        org, ow1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": fake.company(),
                        "atoka_id": fake.pystr(max_chars=20),
                        "percentage": fake.pyfloat(
                            min_value=0, max_value=100
                        ),
                        "last_updated": datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 0)

    def test_transformation_skips_unknown_person(self):

        org, ow1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow1.name,
                        "atoka_id": ow1.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow1).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow1).first().start_date
                    },
                    {
                        "name": fake.company(),
                        "atoka_id": fake.pystr(max_chars=20),
                        "percentage": fake.pyfloat(
                            min_value=0, max_value=100
                        ),
                        "last_updated": datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 1)
        self.assertTrue('shares_owned' in etl.processed_data[0])
        self.assertTrue(len(etl.processed_data[0]['shares_owned']) == 1)
        self.assertTrue(etl.processed_data[0]['shares_owned'][0]['organization_id'] == ow1.id)

    def test_close_ownership(self):

        org, ow1 = self.prepare_data()

        ow2 = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        ow2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        ow2.add_owner(
            org,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow1.name,
                        "atoka_id": ow1.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow1).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow1).first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=OrganizationOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships.count() == 2)
        self.assertTrue(org.ownerships.filter(end_date__isnull=False).count() == 1)

    def test_close_ownership_dry_run(self):

        org, ow1 = self.prepare_data()

        ow2 = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        ow2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        ow2.add_owner(
            org,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow1.name,
                        "atoka_id": ow1.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow1).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow1).first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=OrganizationOwnershipsCloser(dry_run=True),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships.count() == 2)
        self.assertTrue(org.ownerships.filter(end_date__isnull=False).count() == 0)

    def test_no_ownerships_to_update(self):
        """
        ATOKA contains the same data of OPDM
        No ownerships are closed.

        :return:
        """
        org, ow1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow1.name,
                        "atoka_id": ow1.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow1).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow1).first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=OrganizationOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships.filter(owned_organization=ow1).count() == 1)
        self.assertTrue(org.ownerships_as_owned.filter(owned_organization=ow1, end_date__isnull=False).count() == 0)

    def test_do_not_close_ownership_for_public_company(self):
        """A company listed in the public stock exchange cannot be trusted as to ownerships"""

        org, ow1 = self.prepare_data()

        OrganizationEconomics.objects.create(
            organization=org,
            is_public=True
        )

        ow2 = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        ow2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        ow2.add_owner(
            org,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow1.name,
                        "atoka_id": ow1.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow1).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow1).first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=OrganizationOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships.count() == 2)
        self.assertFalse(org.ownerships.filter(end_date__isnull=False).count() == 0)

    def test_swap_ownership_with_non_existing_in_opdm(self):
        """
        OPDM owns ow1 and ow2,
        ATOKA indicates that org now owns ow1 and ow3

        Since ow3 is not in OPDM, the ATOKA record is filtered out,
        thus ow2's ownership is closed (it's in OPDM, but not in ATOKA).

        NOTE: that's a problem, so the close_ownerships task
        must always be launched after importing new ownerships).

        :return:
        """
        org, ow1 = self.prepare_data()

        ow2 = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        ow2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        ow2.add_owner(
            org,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow1.name,
                        "atoka_id": ow1.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow1).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow1).first().start_date
                    },
                    {
                        "name": fake.company(),
                        "atoka_id": fake.pystr(max_chars=20),
                        "percentage": fake.pyfloat(
                            min_value=0, max_value=100
                        ),
                        "last_updated": datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d"),
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=OrganizationOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships.filter(end_date__isnull=True).count() == 1)
        self.assertTrue(org.ownerships.get(owned_organization=ow2).end_date is not None)

    def test_swap_ownership_with_existing_in_opdm_older_than_6m(self):
        """
        in OPDM, org owns ow1 and ow2,
        ATOKA indicates that org owns ow2.

        Since ow1 is not in ATOKA ow1's ownership is closed.

        The change happened more than 6 months ago,
        so ow1 ownerhips end_date is set to today's date

        :return:
        """
        org, ow1 = self.prepare_data()

        ow2 = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        ow2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        ow2.add_owner(
            org,
            start_date=datetime.strftime(fake.date_between("-1y", "-6M"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow2.name,
                        "atoka_id": ow2.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow2).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow2).first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=OrganizationOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships.count() == 2)
        o1 = org.ownerships.get(owned_organization=ow1)
        o2 = org.ownerships.get(owned_organization=ow2)
        self.assertTrue(
            o1.end_date != o2.start_date
        )
        self.assertTrue(
            o1.end_date == datetime.strftime(datetime.now(), '%Y-%m-%d')
        )
        self.assertTrue(
            "New (changed) ownership found" not in o1.end_reason
        )

    # def test_swap_ownership_with_existing_in_opdm_before_6m(self):
    #     """
    #     in OPDM, org owns ow1 and ow2,
    #     ATOKA indicates that org owns ow2.
    #
    #     Since ow1 is not in ATOKA ow1's ownership is closed.
    #
    #     The change happened less than 6 months ago,
    #     so ow1 ownerhips end_date is set ow2's start_date.
    #
    #     :return:
    #     """
    #     org, ow1 = self.prepare_data()
    #
    #     ow2 = OrganizationFactory.create(
    #         identifier=fake.pystr(max_chars=20),
    #         classification=fake.sentence(nb_words=3)
    #     )
    #     ow2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
    #
    #     ow2.add_owner(
    #         org,
    #         start_date=datetime.strftime(fake.date_between("-6M", "-1M"), "%Y-%m-%d"),
    #         percentage=fake.pyfloat(min_value=1, max_value=100)
    #     )
    #
    #     atoka_extraction = [
    #         {
    #             "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
    #             "other_atoka_ids": [],
    #             "name": org.name,
    #             "legal_form": org.classification,
    #             "shares_owned": [
    #                 {
    #                     "name": ow2.name,
    #                     "atoka_id": ow2.identifiers.get(scheme='ATOKA_ID').identifier,
    #                     "percentage": org.ownerships.filter(owned_organization=ow2).first().percentage,
    #                     "last_updated": org.ownerships.filter(owned_organization=ow2).first().start_date
    #                 },
    #             ]
    #         },
    #     ]
    #
    #     etl = ETL(
    #         extractor=ListExtractor(atoka_extraction),
    #         transformation=TransformSharesOwnedIDs(),
    #         loader=OrganizationOwnershipsCloser(),
    #         log_level=0,
    #     )
    #     etl.etl()
    #
    #     self.assertTrue(org.ownerships.count() == 2)
    #     o1 = org.ownerships.get(owned_organization=ow1)
    #     o2 = org.ownerships.get(owned_organization=ow2)
    #     self.assertTrue(
    #         o1.end_date == o2.start_date
    #     )
    #     self.assertTrue(
    #         "New (changed) ownership found" in o1.end_reason
    #     )

    def test_end_date_before_start_date(self):
        """
        ATOKA wants to set a end_date conflicting with an existing start_date
        No ownerships are closed.

        :return:
        """
        org, ow1 = self.prepare_data()

        o1 = org.ownerships.get(owned_organization=ow1)
        o1.start_date = datetime.strftime(fake.date_between("-3M", "-1M"), "%Y-%m-%d")
        o1.save()

        ow2 = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        ow2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        ow2.add_owner(
            org,
            start_date=datetime.strftime(fake.date_between("-5M", "-4M"), "%Y-%m-%d"),
            percentage=fake.pyfloat(min_value=1, max_value=100)
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "shares_owned": [
                    {
                        "name": ow2.name,
                        "atoka_id": ow2.identifiers.get(scheme='ATOKA_ID').identifier,
                        "percentage": org.ownerships.filter(owned_organization=ow2).first().percentage,
                        "last_updated": org.ownerships.filter(owned_organization=ow2).first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformSharesOwnedIDs(),
            loader=OrganizationOwnershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.ownerships.count() == 2)
        self.assertTrue(org.ownerships.filter(end_date__isnull=False).count() == 0)
