from datetime import datetime
import logging

import django.test
from ooetl import ETL
from popolo.tests.factories import OrganizationFactory, PersonFactory, PostFactory, RoleTypeFactory

from project.atoka.align.management.commands.align_atoka_close_memberships import TransformIDs, MembershipsCloser
from project.tasks.etl.extractors import ListExtractor
from project.tasks.etl.loaders import DummyLoader

from faker import Faker
fake = Faker("it_IT")
Faker.seed(0)


class CloseMemberships(django.test.TestCase):

    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.atoka.align.management.commands"
        )
        logger.handlers = []

    @staticmethod
    def prepare_data():
        """Prepare data in OPDM test DB"""
        org = OrganizationFactory.create(
            identifier=fake.pystr(max_chars=20),
            classification=fake.sentence(nb_words=3)
        )
        org.add_identifier(
            fake.pystr(max_chars=20), 'ATOKA_ID'
        )
        org.add_classification(
            scheme='FORMA_GIURIDICA_OP', code='1301', descr=org.classification
        )

        role_type = RoleTypeFactory.create(label='Sindaco supplente')

        post1 = PostFactory.create(role=role_type.label, organization=org)

        p1 = PersonFactory.create()
        p1.add_identifier(fake.ssn(), 'CF')
        p1.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')

        p1.add_role(
            post=post1,
            label=f"{post1.label}",
            role=post1.role,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d")
        )

        return role_type, org, p1, post1

    def test_transformation_transforms_ids(self):
        """ATOKA IDS are transformed correctly into OPDM ids"""

        role_type, org, p1, post1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 1)
        self.assertTrue(len(etl.processed_data[0]['memberships']) == 1)
        self.assertTrue(etl.processed_data[0]['organization_id'] == org.id)

    def test_transformation_skips_orgs_with_no_known_persons(self):
        role_type, org, p1, post1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": fake.first_name(),
                            "family_name": fake.last_name(),
                            "gender": 'M',
                            "birth_date": datetime.strftime(fake.date_between("-50y", "-18y"), "%Y-%m-%d"),
                            "birth_location": fake.city(),
                            "tax_id": fake.ssn(),
                            "atoka_id": fake.pystr(max_chars=20)
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 0)

    def test_transformation_skips_unknown_role_type(self):

        role_type, org, p1, post1 = self.prepare_data()

        post2 = PostFactory.create(role=role_type.label, organization=org)
        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        p2.add_role(
            post=post2,
            label=f"{post2.label}  {org.classification}",
            role=post2.role,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d")
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                    {
                        "person": {
                            "given_name": p2.given_name,
                            "family_name": p2.family_name,
                            "gender": p2.gender,
                            "birth_date": p2.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p2.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p2.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": 'trapezista subacqueo',
                        "start_date": p2.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 1)
        self.assertTrue(len(etl.processed_data[0]['memberships']) == 1)

    def test_transform_ids_skips_unknown_person(self):

        role_type, org, p1, post1 = self.prepare_data()

        post2 = PostFactory.create(role=role_type.label, organization=org)
        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        p2.add_role(
            post=post2,
            label=f"{post2.label}  {org.classification}",
            role=post2.role,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d")
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                    {
                        "person": {
                            "given_name": fake.first_name(),
                            "family_name": fake.last_name(),
                            "gender": 'M',
                            "birth_date": datetime.strftime(fake.date_between("-50y", "-18y"), "%Y-%m-%d"),
                            "birth_location": fake.city(),
                            "tax_id": fake.ssn(),
                            "atoka_id": fake.pystr(max_chars=20)
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=DummyLoader(),
            log_level=0,
        )
        etl.extract().transform()

        self.assertTrue(len(etl.processed_data) == 1)
        self.assertTrue(len(etl.processed_data[0]['memberships']) == 1)

    def test_close_membership(self):

        role_type, org, p1, post1 = self.prepare_data()

        post2 = PostFactory.create(role=role_type.label, organization=org)
        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        p2.add_role(
            post=post2,
            label=f"{post2.label}  {org.classification}",
            role=post2.role,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d")
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=MembershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.memberships.count() == 2)
        self.assertTrue(org.memberships.filter(end_date__isnull=False).count() == 1)

    def test_close_membership_dry_run(self):
        """
        The membership should be closed, but it's not (dry-run)
        :return:
        """

        role_type, org, p1, post1 = self.prepare_data()

        post2 = PostFactory.create(role=role_type.label, organization=org)
        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        p2.add_role(
            post=post2,
            label=f"{post2.label}  {org.classification}",
            role=post2.role,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d")
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=MembershipsCloser(dry_run=True),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.memberships.count() == 2)
        self.assertTrue(org.memberships.filter(end_date__isnull=False).count() == 0)

    def test_swap_roles_with_non_existing_in_opdm(self):
        """
        OPDM has p1 and p2 in post1 and post2,
        ATOKA indicates that the post is now assigned to p3

        Since p3 is not in OPDM, the ATOKA record is filtered out,
        thus p2's membership is closed (it's in OPDM, but not in ATOKA).

        NOTE: that's a problem, so the close_memberships task
        must always be launched after importing new memberships).

        :return:
        """
        role_type, org, p1, post1 = self.prepare_data()

        post2 = PostFactory.create(role=role_type.label, organization=org)
        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        p2.add_role(
            post=post2,
            label=f"{post2.label}  {org.classification}",
            role=post2.role,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d")
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                    {
                        "person": {
                            "given_name": fake.first_name(),
                            "family_name": fake.last_name(),
                            "gender": 'M',
                            "birth_date": datetime.strftime(fake.date_between("-50y", "-20y"), "%Y-%m-%d"),
                            "birth_location": fake.city(),
                            "tax_id": fake.ssn(),
                            "atoka_id": fake.pystr(max_chars=20)
                        },
                        "label": post1.role,
                        "start_date": datetime.strftime(fake.date_between("-1y", "-6M"), "%Y-%m-%d")
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=MembershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.memberships.filter(end_date__isnull=True).count() == 1)
        self.assertTrue(org.memberships.get(person=p2).end_date is not None)

    def test_swap_roles_with_existing_in_opdm_older_than_6m(self):
        """
        OPDM has both p1 and p2 in post1,
        ATOKA indicates that the post is now assigned to p2
        The change is older than 6 months from now.

        :return:
        """
        role_type, org, p1, post1 = self.prepare_data()

        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        p2.add_role(
            post=post1,
            label=f"{post1.label}  {org.classification}",
            role=post1.role,
            start_date=datetime.strftime(fake.date_between("-1y", "-6M"), "%Y-%m-%d")
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p2.given_name,
                            "family_name": p2.family_name,
                            "gender": p2.gender,
                            "birth_date": p2.birth_date,
                            "birth_location": p2.birth_location,
                            "tax_id": p2.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p2.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p2.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=MembershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.memberships.count() == 2)
        m1 = org.memberships.get(person=p1)
        m2 = org.memberships.get(person=p2)
        self.assertTrue(
            m1.end_date != m2.start_date
        )
        self.assertTrue(
            m1.end_date == datetime.strftime(datetime.now(), '%Y-%m-%d')
        )
        self.assertTrue(
            "New membership with same role found." not in m1.end_reason
        )

    # def test_swap_roles_with_existing_in_opdm_before_6m(self):
    #     """
    #     OPDM has both p1 and p2 in post1,
    #     ATOKA indicates that the post is now assigned to p2
    #     The change happened before 6 months from now.
    #
    #     :return:
    #     """
    #     role_type, org, p1, post1 = self.prepare_data()
    #
    #     p2 = PersonFactory.create()
    #     p2.add_identifier(fake.ssn(), 'CF')
    #     p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
    #     p2.add_role(
    #         post=post1,
    #         label=f"{post1.label}  {org.classification}",
    #         role=post1.role,
    #         start_date=datetime.strftime(fake.date_between("-6M", "-1M"), "%Y-%m-%d")
    #     )
    #
    #     atoka_extraction = [
    #         {
    #             "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
    #             "other_atoka_ids": [],
    #             "name": org.name,
    #             "legal_form": org.classification,
    #             "roles": [
    #                 {
    #                     "person": {
    #                         "given_name": p2.given_name,
    #                         "family_name": p2.family_name,
    #                         "gender": p2.gender,
    #                         "birth_date": p2.birth_date,
    #                         "birth_location": p2.birth_location,
    #                         "tax_id": p2.identifiers.get(scheme='CF').identifier,
    #                         "atoka_id": p2.identifiers.get(scheme='ATOKA_ID').identifier
    #                     },
    #                     "label": post1.role,
    #                     "start_date": p2.memberships.first().start_date
    #                 },
    #             ]
    #         },
    #     ]
    #
    #     etl = ETL(
    #         extractor=ListExtractor(atoka_extraction),
    #         transformation=TransformIDs(),
    #         loader=MembershipsCloser(),
    #         log_level=0,
    #     )
    #     etl.etl()
    #
    #     self.assertTrue(org.memberships.count() == 2)
    #     m1 = org.memberships.get(person=p1)
    #     m2 = org.memberships.get(person=p2)
    #     self.assertTrue(
    #         m1.end_date == m2.start_date
    #     )
    #     self.assertTrue(
    #         "New membership with same role found." in m1.end_reason
    #     )
    #
    def test_missing_org(self):
        """
        ATOKA returns data on a missing atoka id (should not be possible)

        :return:
        """
        role_type, org, p1, post1 = self.prepare_data()

        post2 = PostFactory.create(role=role_type.label, organization=org)
        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        p2.add_role(
            post=post2,
            label=f"{post2.label}  {org.classification}",
            role=post2.role,
            start_date=datetime.strftime(fake.date_between("-5y", "-1y"), "%Y-%m-%d")
        )

        atoka_extraction = [
            {
                "atoka_id": fake.pystr(max_chars=20),
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=MembershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.memberships.count() == 2)
        self.assertTrue(org.memberships.filter(end_date__isnull=True).count() == 2)

    def test_no_memberships_to_update(self):
        """
        ATOKA contains the same data of OPDM
        No memberships are closed.

        :return:
        """
        role_type, org, p1, post1 = self.prepare_data()

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p1.given_name,
                            "family_name": p1.family_name,
                            "gender": p1.gender,
                            "birth_date": p1.birth_date,
                            "birth_location": p1.birth_location,
                            "tax_id": p1.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p1.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p1.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=MembershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.memberships.filter(person=p1).count() == 1)
        self.assertTrue(org.memberships.filter(person=p1, end_date__isnull=False).count() == 0)

    def test_end_date_before_start_date(self):
        """
        ATOKA wants to set a end_date conflicting with an existing start_date
        No memberships are closed.

        :return:
        """
        role_type, org, p1, post1 = self.prepare_data()

        m1 = p1.memberships.first()
        m1.start_date = datetime.strftime(fake.date_between("-3M", "-1M"), "%Y-%m-%d")
        m1.save()

        p2 = PersonFactory.create()
        p2.add_identifier(fake.ssn(), 'CF')
        p2.add_identifier(fake.pystr(max_chars=20), 'ATOKA_ID')
        p2.add_role(
            post=post1,
            label=f"{post1.label}  {org.classification}",
            role=post1.role,
            start_date=datetime.strftime(fake.date_between("-5M", "-4M"), "%Y-%m-%d")
        )

        atoka_extraction = [
            {
                "atoka_id": org.identifiers.get(scheme='ATOKA_ID').identifier,
                "other_atoka_ids": [],
                "name": org.name,
                "legal_form": org.classification,
                "roles": [
                    {
                        "person": {
                            "given_name": p2.given_name,
                            "family_name": p2.family_name,
                            "gender": p2.gender,
                            "birth_date": p2.birth_date,
                            "birth_location": p2.birth_location,
                            "tax_id": p2.identifiers.get(scheme='CF').identifier,
                            "atoka_id": p2.identifiers.get(scheme='ATOKA_ID').identifier
                        },
                        "label": post1.role,
                        "start_date": p2.memberships.first().start_date
                    },
                ]
            },
        ]

        etl = ETL(
            extractor=ListExtractor(atoka_extraction),
            transformation=TransformIDs(),
            loader=MembershipsCloser(),
            log_level=0,
        )
        etl.etl()

        self.assertTrue(org.memberships.count() == 2)
        self.assertTrue(
            org.memberships.filter(end_date__isnull=False).count() == 0,
            org.memberships.filter(end_date__isnull=False).count()
        )
