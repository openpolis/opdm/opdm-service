from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class AlignAtokaConfig(AppConfig):

    name = 'project.atoka.align'
    verbose_name = _("Align Atoka")
