import json
import time

from django.db.models import Q
from taskmanager.management.base import LoggingBaseCommand

from django.conf import settings
from popolo.models import Organization

from project.atoka.etl.extractors import AtokaOrganizationsCompleteExtractor
from project.core import batch_generator
from atokaconn import AtokaConn


class Command(LoggingBaseCommand):
    help = "Starting from organizations in OPDM, extract memberships and ownerships for each, from ATOKA's API " \
        "and store them as an array into a local JSON file"

    atoka_conn = AtokaConn(
        service_url=settings.ATOKA_API_ENDPOINT,
        version=settings.ATOKA_API_VERSION,
        key=settings.ATOKA_API_KEY
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--offset",
            dest="offset", type=int,
            default=0,
            help="Start processing from offset (useful during development)",
        )
        parser.add_argument(
            "--limit",
            dest="limit", type=int,
            help="Stop processing after limit (useful during development)",
        )
        parser.add_argument(
            "--selected-ids",
            nargs='*', metavar='ATOKA_ID',
            dest="selected_ids",
            help="Only consider these atoka ids (used for debugging)",
        )
        parser.add_argument(
            "--json-file",
            dest="jsonfile",
            default="./data/atoka/extract_align_atoka.json",
            help="Complete path to json file where output will be stored"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        batch_size = options['batch_size']
        offset = options['offset']
        limit = options.get('limit', None)
        jsonfile = options['jsonfile']
        selected_ids = options.get('selected_ids')
        self.logger.info("Start extraction procedure")

        orgs_qs = Organization.objects.filter(
            identifiers__scheme='ATOKA_ID'
        ).exclude(
            classifications__classification__scheme='LIVELLO_PARTECIPAZIONE_OP',
            classifications__classification__code='0'
        )

        orgs_owned_by_persons = set(
            orgs_qs.filter(
                Q(ownerships_as_owned__end_date__isnull=True) &
                Q(ownerships_as_owned__owner_person__identifiers__scheme='ATOKA_ID')
            ).values_list('identifiers__identifier', flat=True).distinct()
        )

        orgs_with_memberships = set(
            orgs_qs.filter(
                Q(memberships__end_date__isnull=True) &
                Q(memberships__person__identifiers__scheme='ATOKA_ID')
            ).values_list('identifiers__identifier', flat=True).distinct()
        )

        orgs_with_ownerships = set(
            orgs_qs.filter(
                Q(ownerships__end_date__isnull=True) &
                Q(ownerships__owner_organization__identifiers__scheme='ATOKA_ID')
            ).values_list('identifiers__identifier', flat=True).distinct()
        )

        orgs_owned_by_orgs = set(
            orgs_qs.filter(
                Q(ownerships_as_owned__end_date__isnull=True) &
                Q(ownerships_as_owned__owner_organization__identifiers__scheme='ATOKA_ID')
            ).values_list('identifiers__identifier', flat=True).distinct()
        )

        orgs_ids = orgs_owned_by_persons | orgs_with_memberships | orgs_with_ownerships | orgs_owned_by_orgs

        if selected_ids:
            orgs_ids = orgs_ids & selected_ids

        n_organizations = len(orgs_ids)

        self.logger.info(
            f"Fetching {n_organizations} organizations (offset={offset}, limit={limit})"
        )

        # generate batches of batch_size, to query atoka's endpoint
        # flatten iterators of cfs
        ids_batches = batch_generator(batch_size, iter(orgs_ids))

        atoka_records = []
        atoka_people_requests = 0
        atoka_org_requests = 0
        people_ids = []
        org_ids = []

        counter = 0
        for ids_batch in ids_batches:
            # implement offset
            if counter >= offset:
                atoka_extractor = AtokaOrganizationsCompleteExtractor(
                    ids_batch, ids_type='atoka_id', shares_details=False
                )
                atoka_extractor.logger = self.logger

                counter += len(ids_batch)

                atoka_res = atoka_extractor.extract()

                # update works as an extension, as there are no duplicates among the atoka ids
                atoka_records.extend(atoka_res['results'])

                atoka_people_requests += atoka_res['meta']['atoka_requests']['people']
                atoka_org_requests += atoka_res['meta']['atoka_requests']['companies']

                people_ids.extend(atoka_res['meta']['ids']['people'])
                people_ids = list(set(people_ids))

                org_ids.extend(atoka_res['meta']['ids']['companies'])
                org_ids = list(set(org_ids))

                self.logger.info(
                    f"{counter} ids, {len(org_ids)} companies, {len(people_ids)} people"
                )
            else:
                self.logger.info("skipping {0} ids untill {1}".format(
                    len(ids_batch), offset
                    )
                )

            if limit and counter >= limit:
                break

            time.sleep(1.)

        self.logger.info(
            f"crediti spesi con atoka: {atoka_org_requests} organizations, {atoka_people_requests} people"
        )

        # produce the json file
        self.logger.info("Dati scritti in {0}".format(jsonfile))
        with open(jsonfile, "w") as f:
            json.dump(atoka_records, f)

        self.logger.info("End")
