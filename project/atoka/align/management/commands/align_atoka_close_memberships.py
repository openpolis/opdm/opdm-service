# -*- coding: utf-8 -*-
import re
from datetime import datetime, timedelta
from ooetl import ETL
from popolo.models import Person, Organization, Membership
from ooetl.transformations import Transformation

from project.atoka.etl.transformations import AtokaMembershipsTransformation
from project.tasks.etl.extractors import JsonArrayExtractor
from project.tasks.etl.loaders import PopoloLoader
from taskmanager.management.base import LoggingBaseCommand


class MembershipsCloser(PopoloLoader):
    """A loader that actually sets end_date for given memberships"""

    def __init__(self, **kwargs):
        self.dry_run = kwargs.pop("dry_run", False)
        super(MembershipsCloser, self).__init__(**kwargs)

    @staticmethod
    def key_getter(membership, classification=None):
        role = membership['role'].strip()
        if classification is not None:
            role = membership['role'].replace(classification.lower(), '').strip()
        return membership['person_id'], role

    def load_item(self, item, **kwargs):
        try:
            org = Organization.objects.get(id=item['organization_id'])
            self.etl.logger.debug(f"Verifying memberships for org {org.name} ({org.id})")
        except Organization.DoesNotExist:  # pragma: no cover
            self.etl.logger.error(
                f"Could not find org item['organization_id'], with id {item['organization_id']} in OPDM"
            )
        else:
            opdm_memberships = list(org.memberships.current().values('person_id', 'label', 'role', 'start_date'))
            atoka_memberships = item['memberships']

            opdm_keys_set = {self.key_getter(v, org.classification) for v in opdm_memberships}
            atoka_keys_set = {self.key_getter(v) for v in atoka_memberships}

            removes_keys_set = opdm_keys_set - atoka_keys_set
            if removes_keys_set:
                for r in filter(lambda x: self.key_getter(x, org.classification) in removes_keys_set, opdm_memberships):
                    try:
                        m = org.memberships.get(**r)
                    except Membership.DoesNotExist:  # pragma: no cover (already filtered out by transformation)
                        self.etl.logger.error(
                            f"  - could not find membership for org, parameters: {r}."
                        )
                    else:
                        # compute last start date for memberships of the same role within that org
                        last_start_date = sorted(
                            (
                                am.get('start_date', '')
                                for am in atoka_memberships
                                if am.get('role', None) == m.role.replace(org.classification.lower(), '').strip()
                            ),
                            reverse=True
                        )
                        if len(last_start_date) > 0:
                            last_start_date = last_start_date[0]
                        else:  # pragma: no cover
                            last_start_date = None
                        six_months_ago = (datetime.now() - timedelta(days=182)).strftime('%Y-%m-%d')

                        if last_start_date and last_start_date > six_months_ago:
                            end_date = last_start_date
                            end_reason = "Record not found in Atoka. New membership with same role found."
                        else:
                            end_date = datetime.strftime(datetime.now(), '%Y-%m-%d')
                            end_reason = "Record not found in Atoka."

                        self.etl.logger.info(
                            f"Closing membership {m}, setting end_date to: {end_date} reason: {end_reason}"
                        )
                        m.end_date = end_date
                        m.end_reason = end_reason
                        if m.end_date <= m.start_date:
                            self.logger.warning(f"End date must follow start_date for {m}. Not closing.")
                        else:
                            if not self.dry_run:
                                m.save()
            else:
                self.etl.logger.debug(
                    "  - no memberships to close found for org."
                )


class TransformIDs(Transformation):
    """A Transformation that transform info in extracted json file
    - assign opdm ids starting from atoka ids
    - remove roles not in opdm
    """
    def __init__(self, **kwargs):
        self.log_step = kwargs.pop("log_step", 10)
        super(TransformIDs, self).__init__(**kwargs)

    def transform(self):
        """ Transform a list of dicts parsed from the JSON file,
        """
        self.logger.info("start of transform")
        od = self.etl.original_data

        organizations_atoka_id_dict = dict(
            Organization.objects.filter(
                identifiers__scheme='ATOKA_ID'
            ).values_list(
                'identifiers__identifier', 'id'
            )
        )

        persons_atoka_id_dict = dict(
            Person.objects.filter(
                identifiers__scheme='ATOKA_ID'
            ).values_list(
                'identifiers__identifier', 'id'
            )
        )

        res = []
        counter = 1
        for o in od:
            try:
                organization_id = organizations_atoka_id_dict[o['atoka_id']]
            except KeyError:
                self.logger.error(f"Organization with ATOKA_ID {o['atoka_id']} could not be found in OPDM. Skipping.")
                continue
            memberships = []
            for r in o.get('roles', []):

                person = r['person']
                role_name = re.sub(r"\s+", " ", r['label'])
                role_type = AtokaMembershipsTransformation.atoka_role_2_opdm_roletype(role_name)
                if role_type is None:
                    if role_name.lower() not in AtokaMembershipsTransformation.atoka_roles_skip_list:
                        self.logger.warning(
                            "Role <{0}> is neither among the allowed roles, nor in the skip list."
                            "Skipping it.".format(role_name)
                        )
                    continue
                try:
                    person_id = persons_atoka_id_dict[person['atoka_id']]
                except KeyError:
                    continue

                memberships.append({
                    'role': role_type,
                    'start_date': r['start_date'],
                    'person_id': person_id,
                    'person_name': f"{person['given_name']} {person['family_name']}, "
                    f"{person.get('birth_date', '-')}, {person.get('birth_location', '-')})",
                })

            if len(memberships):
                res.append({
                    'organization_id': organization_id,
                    'name': o['name'],
                    'memberships': memberships
                })
            counter += 1
            if counter % self.log_step == 0:  # pragma: no cover
                self.logger.info(f"{counter}/{len(od)}")

        self.etl.processed_data = res

        self.logger.info("end of transformation")


class Command(LoggingBaseCommand):  # pragma: no cover
    help = "Close memberships present in OPDM and not present in ATOKA"
    source_url = None
    log_step = None
    dry_run = None

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_url", help="Source of the JSON file (http[s]:// or file:///)"
        )
        parser.add_argument(
            "--log-step",
            dest="log_step",
            type=int,
            default=50,
            help="Number of steps to log process completion to stdout. Defaults to 500.",
        )
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action="store_true",
            help="Perform queries and log actions, DO NOT save on DB"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.source_url = options["source_url"]
        self.log_step = options["log_step"]
        self.dry_run = options["dry_run"]
        self.logger.info("Start of procedure")

        ETL(
            extractor=JsonArrayExtractor(self.source_url),
            loader=MembershipsCloser(
                log_step=self.log_step,
                dry_run=self.dry_run
            ),
            transformation=TransformIDs(log_step=self.log_step),
            logger=self.logger,
            log_level=self.logger.level,
            end_reason='record missing from ATOKA source',
        )()

        self.logger.info("End of procedure")
