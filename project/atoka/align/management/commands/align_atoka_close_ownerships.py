# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from decimal import ROUND_HALF_DOWN, Context, setcontext

from django.db.models import F
from ooetl import ETL
from popolo.models import Person, Organization, Ownership
from ooetl.transformations import Transformation

from project.tasks.etl.extractors import JsonArrayExtractor
from project.tasks.etl.loaders import PopoloLoader
from taskmanager.management.base import LoggingBaseCommand


class OwnershipsCloser(PopoloLoader):
    """Base loader for Ownerships closer"""

    def __init__(self, **kwargs):
        self.dry_run = kwargs.pop("dry_run", False)
        super(OwnershipsCloser, self).__init__(**kwargs)

        # avoid rounding problems ?
        setcontext(Context(prec=4, rounding=ROUND_HALF_DOWN))


class PersonalOwnershipsCloser(OwnershipsCloser):
    """A loader that actually sets end_date for given personal ownerships"""

    @staticmethod
    def key_getter(ownership):
        return ownership['person_id'], round(float(ownership['percentage']), 2)

    def load_item(self, item, **kwargs):
        try:
            organization = Organization.objects.get(id=item['organization_id'])
            self.etl.logger.debug(f"Verifying ownerships for organization {organization.name} ({organization.id})")
        except Organization.DoesNotExist:  # pragma: no cover
            self.etl.logger.error(f"Could not find organization with id {item['organization_id']} in OPDM")
        else:
            opdm_owners = list(
                organization.ownerships_as_owned.current().
                filter(owner_person__isnull=False).
                annotate(person_id=F('owner_person_id')).
                values('person_id', 'percentage', 'start_date')
            )
            atoka_owners = item['shareholders']

            opdm_keys_set = {self.key_getter(v) for v in opdm_owners}
            atoka_keys_set = {self.key_getter(v) for v in atoka_owners}

            removes_keys_set = opdm_keys_set - atoka_keys_set
            if removes_keys_set:
                for owner in filter(lambda x: self.key_getter(x) in removes_keys_set, opdm_owners):
                    try:
                        owner['owner_person_id'] = owner.pop('person_id')
                        o = organization.ownerships_as_owned.get(**owner)
                    except Ownership.DoesNotExist:  # pragma: no cover
                        self.etl.logger.error(
                            f"  - could not find ownership for person, parameters: {owner}."
                        )
                    else:
                        # compute last start date for org's ownerships of the same owned organization
                        last_start_date = sorted(
                            (
                                ao.get('last_updated', '')
                                for ao in atoka_owners
                            ),
                            reverse=True
                        )
                        if len(last_start_date) > 0:
                            last_start_date = last_start_date[0]
                        else:  # pragma: no cover
                            last_start_date = None
                        six_months_ago = (datetime.now() - timedelta(days=182)).strftime('%Y-%m-%d')

                        if last_start_date and last_start_date > six_months_ago:
                            end_date = last_start_date
                            end_reason = "Record not found in Atoka. New (changed) ownership found."
                        else:
                            end_date = datetime.strftime(datetime.now(), '%Y-%m-%d')
                            end_reason = "Record not found in Atoka."

                        self.etl.logger.info(
                            f"Closing ownership {o}, setting end_date to: {end_date} reason: {end_reason}"
                        )
                        o.end_date = end_date
                        o.end_reason = end_reason
                        if o.start_date and o.end_date <= o.start_date:
                            self.logger.warning(f"End date must follow start_date for {o}. Not closing.")
                        else:
                            if not self.dry_run:
                                o.save()
            else:
                self.etl.logger.debug(
                    "  - no ownerships to close found for org"
                )


class OrganizationOwnershipsCloser(OwnershipsCloser):
    """A loader that actually sets end_date for given organization ownerships"""

    @staticmethod
    def key_getter(ownership):
        return ownership['organization_id'], round(float(ownership['percentage']), 2)

    def load_item(self, item, **kwargs):
        try:
            org = Organization.objects.get(id=item['organization_id'])
            self.etl.logger.debug(f"Verifying ownerships for org {org.name} ({org.id})")

            # try:
            #     if org.economics.is_public:
            #         self.etl.logger.info(
            #             f"Org {org.name} ({org.id}) is quoted at Milano stock exchange "
            #             f"and ownerships will not be verified."
            #         )
            #         return
            # except OrganizationEconomics.DoesNotExist:
            #     pass

        except Organization.DoesNotExist:  # pragma: no cover
            self.etl.logger.error(f"Could not find org with id {item['owner_id']} in OPDM")
        else:
            opdm_ownerships = list(
                org.ownerships.current().
                annotate(organization_id=F('owned_organization_id')).
                values('organization_id', 'percentage', 'start_date')
            )
            atoka_ownerships = item['shares_owned']

            opdm_keys_set = {self.key_getter(v) for v in opdm_ownerships}
            atoka_keys_set = {self.key_getter(v) for v in atoka_ownerships}

            removes_keys_set = opdm_keys_set - atoka_keys_set
            if removes_keys_set:
                for r in filter(lambda x: self.key_getter(x) in removes_keys_set, opdm_ownerships):
                    try:
                        r['owned_organization_id'] = r.pop('organization_id')
                        o = org.ownerships.current().get(**r)
                    except Ownership.DoesNotExist:  # pragma: no cover (already filtered out by transformation)
                        self.etl.logger.error(
                            f"  - could not find ownership for org, parameters: {r}."
                        )
                    else:
                        # compute last start date for org's ownerships
                        last_start_date = sorted(
                            (
                                ao.get('last_updated', '')
                                for ao in atoka_ownerships
                            ),
                            reverse=True
                        )
                        if len(last_start_date) > 0:
                            last_start_date = last_start_date[0]
                        else:  # pragma: no cover
                            last_start_date = None
                        six_months_ago = (datetime.now() - timedelta(days=182)).strftime('%Y-%m-%d')

                        if last_start_date and last_start_date > six_months_ago:
                            end_date = last_start_date
                            end_reason = "Record not found in Atoka. New (changed) ownership found."
                        else:
                            end_date = datetime.strftime(datetime.now(), '%Y-%m-%d')
                            end_reason = "Record not found in Atoka."

                        self.etl.logger.info(
                            f"Closing ownership {o}, setting end_date to: {end_date} reason: {end_reason}"
                        )
                        o.end_date = end_date
                        o.end_reason = end_reason
                        if o.start_date and o.end_date <= o.start_date:
                            self.logger.warning(f"End date must follow start_date for {o}. Not closing.")
                        else:
                            if not self.dry_run:
                                o.save()
            else:
                self.etl.logger.debug(
                    "  - no ownerships to close found for org."
                )


class TransformIDs(Transformation):  # pragma: no cover
    """A Transformation that transform info in extracted json file:
    - assign opdm ids starting from atoka ids
    - remove ownerships not in opdm
    """
    def __init__(self, **kwargs):
        self.log_step = kwargs.pop("log_step", 10)
        self.orgs_dict = dict(
            Organization.objects.filter(
                identifiers__scheme='ATOKA_ID'
            ).values_list(
                'identifiers__identifier', 'id'
            )
        )
        self.persons_dict = dict(
            Person.objects.filter(
                identifiers__scheme='ATOKA_ID'
            ).values_list(
                'identifiers__identifier', 'id'
            )
        )
        super(TransformIDs, self).__init__(**kwargs)

    def transform(self):
        self.logger.error("This is the base class")


class TransformShareholdersIDs(TransformIDs):
    """A Transformation that transform shareholders ids into opdm persons ids:
    """

    def transform(self):
        """ Transform a list of dicts parsed from the JSON file,
        """
        self.logger.info("start of transform")
        od = self.etl.original_data

        res = []
        counter = 1
        for o in od:
            try:
                organization_id = self.orgs_dict[o['atoka_id']]
            except KeyError:
                self.logger.warning(f"Could not find organization with atoka_id {o['atoka_id']}")
                continue

            shareholders = []
            for s in o.get('shareholders_people', []):

                person = s['person']
                try:
                    person_id = self.persons_dict[person['atoka_id']]
                except KeyError:
                    self.logger.warning(f"Could not find person with atoka_id {person['atoka_id']}")
                    continue

                shareholders.append({
                    'percentage': s['percentage'],
                    'last_updated': s.get('last_updated', None),
                    'person_id': person_id,
                    'person_name': f"{person['given_name']} {person['family_name']}, "
                    f"{person.get('birth_date', '-')}, {person.get('birth_location', '-')})",
                })

            if len(shareholders):
                res.append({
                    'organization_id': organization_id,
                    'name': o['name'],
                    'shareholders': shareholders
                })

            counter += 1
            if counter % self.log_step == 0:  # pragma: no cover
                self.logger.info(f"{counter}/{len(od)}")

        self.etl.processed_data = res

        self.logger.info("end of transformation")


class TransformSharesOwnedIDs(TransformIDs):
    """A Transformation that transform shares_owned ids into opdm persons ids:
    """

    def transform(self):
        """ Transform a list of dicts parsed from the JSON file,
        """
        self.logger.info("start of transform")
        od = self.etl.original_data

        res = []
        counter = 1
        for o in od:
            try:
                organization_id = self.orgs_dict[o['atoka_id']]
            except KeyError:
                self.logger.warning(f"Could not find organization with atoka_id {o['atoka_id']}")
                continue
            shares_owned = []
            for s in o.get('shares_owned', []):

                try:
                    owned_id = self.orgs_dict[s['atoka_id']]
                except KeyError:
                    self.logger.warning(f"Could not find organization with atoka_id {s['atoka_id']}")
                    continue

                shares_owned.append({
                    'organization_id': owned_id,
                    'organization_name': s['name'],
                    'percentage': s['percentage'],
                    'last_updated': s.get('last_updated', None),
                })

            if len(shares_owned):
                res.append({
                    'organization_id': organization_id,
                    'name': o['name'],
                    'shares_owned': shares_owned
                })

            counter += 1
            if counter % self.log_step == 0:  # pragma: no cover
                self.logger.info(f"{counter}/{len(od)}")

        self.etl.processed_data = res

        self.logger.info("end of transformation")


class Command(LoggingBaseCommand):  # pragma: no cover
    help = "Close ownerships present in OPDM and not present in ATOKA"
    source_url = None
    log_step = None
    dry_run = None
    ownership_type = None

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_url", help="Source of the JSON file (http[s]:// or file:///)"
        )
        parser.add_argument(
            "--log-step",
            dest="log_step",
            type=int,
            default=50,
            help="Number of steps to log process completion to stdout. Defaults to 500.",
        )
        parser.add_argument(
            "--ownership-type",
            dest="ownership_type",
            default='personal',
            help="Type of ownerhip to close: personal|organization.",
        )
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action="store_true",
            help="Perform queries and log actions, DO NOT save on DB"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.source_url = options["source_url"]
        self.log_step = options["log_step"]
        self.ownership_type = options['ownership_type']
        self.dry_run = options["dry_run"]
        self.logger.info("Start of procedure")

        if 'person' in self.ownership_type.lower():
            transformation_class = TransformShareholdersIDs
            loader_class = PersonalOwnershipsCloser
        else:
            transformation_class = TransformSharesOwnedIDs
            loader_class = OrganizationOwnershipsCloser

        ETL(
            extractor=JsonArrayExtractor(self.source_url),
            transformation=transformation_class(log_step=self.log_step),
            loader=loader_class(
                log_step=self.log_step,
                dry_run=self.dry_run
            ),
            logger=self.logger,
            log_level=self.logger.level,
            end_reason='record missing from ATOKA source',
        )()

        self.logger.info("End of procedure")
