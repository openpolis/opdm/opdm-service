# -*- coding: utf-8 -*-
import os

from django.core import management
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """This management tasks is a metacommand that compares entities in ATOKA with entities in OPDM
    in order to close memberships and ownerships that are not present any longer in ATOKA and still are
    in OPDM.

    The task performs these suubtasks:
    - extraction from ATOKA of all details (base, roles, shares), for organizations we have in OPDM, having ATOKA_ID and
      with members/shares/shareholders having an ATOKA_ID
    - closing of memberships still in OPDM, but not any longer in ATOKA
    - closing of personal and organization wnerships still in OPDM, but not any longer in ATOKA

    It should be used called by:

    .. code::python

        python manage.py align_atoka_all \
          --data-path=./data/atoka/ \
          --batch-size=10 \
          --verbosity=2

    """

    help = "Metacommand that executes etl procedures to cleanup OPDM data " \
        "comparing it with ATOKA (closing memberships" \
        "and ownerships no more existing)"
    verbosity = None
    batch_size = None
    data_path = None
    active = True
    dry_run = False
    use_atoka_cache = False
    filename_suffix = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--data-path",
            dest="data_path",
            default="./data/atoka",
            help="Complete path to json files"
        )
        parser.add_argument(
            "--use-atoka-cache",
            dest="use_atoka_cache",
            action='store_true',
            help="Use extract_organizations[SUFFIX].json, without fetching it anew from ATOKA."
        )
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action="store_true",
            help="Perform queries and log actions, DO NOT save on DB"
        )

    def section_log(self, title):
        self.logger.info(" ")
        self.logger.info(title)
        self.logger.info("*" * len(title))

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.batch_size = options['batch_size']
        self.data_path = options['data_path']
        self.use_atoka_cache = options['use_atoka_cache']
        self.dry_run = options['dry_run']

        self.logger.info("Start overall procedure")

        self.verbosity = options.get("verbosity", 1)

        #
        # Clean-up
        #

        self.section_log("script_compute_missing_cfs")
        management.call_command(
            'script_compute_missing_cfs',
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("script_consolidate_persons")
        management.call_command(
            'script_consolidate_persons',
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        # merge duplicate organization, merging data
        self.section_log("script_merge_duplicate_organizations")
        management.call_command(
            'script_merge_duplicate_organizations',
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        #
        # Persons
        #

        self.filename_suffix = "_align_atoka"
        json_file = os.path.join(self.data_path, f'extract{self.filename_suffix}.json')

        if not self.use_atoka_cache:
            if os.path.exists(json_file):
                os.unlink(json_file)

        if not os.path.exists(json_file):
            self.section_log(
                f"align_atoka_extract_data => {json_file}"
            )
            management.call_command(
                'align_atoka_extract_data',
                batch_size=50,
                json_file=json_file,
                verbosity=self.verbosity,
                stdout=self.stdout
            )

        #
        # Memberships
        #

        self.section_log(
            f"align_atoka_close_memberships <= {json_file}"
        )
        management.call_command(
            'align_atoka_close_memberships',
            json_file,
            verbosity=self.verbosity,
            dry_run=self.dry_run,
            stdout=self.stdout
        )

        #
        # Ownerships
        #

        # Personal ownerships
        self.section_log(
            f"align_atoka_close_ownerships --ownership-type='personal' <= {json_file}"
        )
        management.call_command(
            'align_atoka_close_ownerships',
            json_file,
            ownership_type='personal',
            verbosity=self.verbosity,
            dry_run=self.dry_run,
            stdout=self.stdout
        )

        # Organization ownerships
        self.section_log(
            f"align_atoka_close_ownerships --ownership-type='organization' <= {json_file}"
        )
        management.call_command(
            'align_atoka_close_ownerships',
            json_file,
            ownership_type='organization',
            verbosity=self.verbosity,
            dry_run=self.dry_run,
            stdout=self.stdout
        )
