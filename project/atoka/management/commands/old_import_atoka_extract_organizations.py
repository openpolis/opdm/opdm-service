# -*- coding: utf-8 -*-
import itertools
import json

from popolo.models import Organization
from taskmanager.management.base import LoggingBaseCommand

from project.core import batch_generator
from project.atoka.etl.extractors import AtokaOwnershipsExtractor


class Command(LoggingBaseCommand):
    help = "Starting from organizations in OPDM, extract info from ATOKA's API " \
        "and store them as an array into a local JSON file"

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=25,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--offset",
            dest="offset", type=int,
            default=0,
            help="Start processing",
        )
        parser.add_argument(
            "--classifications",
            nargs='*', metavar='CLASS',
            dest="classifications", type=int,
            help="Only process specified classifications, by ID "
            "(by id, ex: Ministero, Consiglio Reg., Città Metrop.: 498 133 279)",
        )
        parser.add_argument(
            "--tax-ids",
            nargs='*', metavar='TAX_ID',
            dest="tax_ids",
            help="Only consider these tax_ids (used for debugging)",
        )
        parser.add_argument(
            "--json-file",
            dest="jsonfile",
            default="./data/atoka/extract_organizations.json",
            help="Complete path to json file"
        )
        parser.add_argument(
            "--shares-level",
            dest="shares_level",
            type=int,
            default=0,
            help="Level of the public share, starting from public institution = 0"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        batch_size = options['batch_size']
        offset = options['offset']
        jsonfile = options['jsonfile']
        classifications = options.get('classifications', [])
        tax_ids = options.get('tax_ids')
        shares_level = options['shares_level']

        self.logger.info("Start procedure")

        if shares_level < 0 or shares_level > 2:
            raise Exception("--shares-level must be between 0 and 2")

        organizations_qs = Organization.objects.current()

        organizations_qs = organizations_qs.filter(
            classifications__classification__scheme='LIVELLO_PARTECIPAZIONE_OP',
            classifications__classification__code=str(shares_level)
        )

        if classifications:
            organizations_qs = organizations_qs.filter(
                classifications__classification_id__in=classifications
            )

        if tax_ids:
            organizations_qs = organizations_qs.filter(
                identifier__in=tax_ids
            )

        organizations_qs = organizations_qs.exclude(identifier__isnull=True)

        # group organizations by classification counting occurrences
        organizations_groups = [
            {
                'descr': x['classifications__classification__descr'],
                'id': x['classifications__classification_id'],
                'n': 0
            }
            for x in list(organizations_qs.values(
                'classifications__classification__descr',
                'classifications__classification_id'
            ).distinct())
        ]
        for group in organizations_groups:
            group['n'] = organizations_qs.filter(classifications__classification_id=group['id']).count()
        organizations_groups = sorted(organizations_groups, key=lambda x: x['n'] * -1)

        atoka_records = []
        atoka_companies_requests = 0
        atoka_people_requests = 0
        people_ids = []
        owned_ids = []
        counter = 0
        for organizations_group in organizations_groups:
            self.logger.info('processing {0} organizations classified as {1}'.format(
                organizations_group['n'], organizations_group['descr']
            ))

            organization_group_qs = organizations_qs.filter(
                classifications__classification_id=organizations_group['id']
            )

            # extract iterators for identifiers and multiple cfs
            cfs = organization_group_qs.values_list('identifier', flat=True).distinct().iterator()
            multiple_cfs = (o.split(",") for o in organization_group_qs.filter(
                    identifiers__scheme='ALTRI_CF_ATOKA'
                ).distinct().values_list('identifiers__identifier', flat=True).iterator())

            # generate batches of batch_size, to query atoka's endpoint
            # flatten iterators of cfs
            batches = batch_generator(
                batch_size, itertools.chain(
                    *multiple_cfs, cfs
                )
            )

            group_counter = 0
            for tax_ids_batch in batches:
                # extract atoka_ownerships into a list in memory,
                # in order to use it in the two following ETL procedures

                # implement offset
                if counter >= offset:

                    tax_ids_batch = list(set(tax_ids_batch))
                    atoka_extractor = AtokaOwnershipsExtractor(tax_ids_batch)
                    atoka_extractor.logger = self.logger

                    atoka_res = atoka_extractor.extract()
                    atoka_records.extend(atoka_res['results'])

                    atoka_companies_requests += atoka_res['meta']['atoka_requests']['companies']
                    atoka_people_requests += atoka_res['meta']['atoka_requests']['people']

                    people_ids.extend(atoka_res['meta']['ids']['people'])
                    people_ids = list(set(people_ids))

                    owned_ids.extend(atoka_res['meta']['ids']['companies'])
                    owned_ids = list(set(owned_ids))

                    group_counter += len(tax_ids_batch)

                    self.logger.info("[{0}]: {1} tax_ids, {2} partecipate, {3} persone --------".format(
                        organizations_group['descr'], group_counter, len(owned_ids), len(people_ids)
                    ))
                else:
                    self.logger.info("[{0}]: skipping {1} tax_ids".format(
                        organizations_group['descr'], len(tax_ids_batch)
                    ))

                self.logger.debug("")

                counter += len(tax_ids_batch)

        self.logger.info(
            "crediti spesi con atoka: {0} companies, {1} people".format(
                atoka_companies_requests, atoka_people_requests
            )
        )

        # produce the json file
        self.logger.info("Dati scritti in {0}".format(jsonfile))
        with open(jsonfile, "w") as f:
            json.dump(atoka_records, f)
