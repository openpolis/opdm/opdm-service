# -*- coding: utf-8 -*-
import os

from django.core import management
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Metacommand that executes all etl procedures, for a given shares level, starting from OPDM organizations"
    verbosity = None
    batch_size = None
    data_path = None
    shares_level = None
    use_atoka_cache = False
    clear_diff_cache = False
    filename_suffix = None
    classifications = []
    tax_ids = []

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--data-path",
            dest="data_path",
            default="./data/atoka",
            help="Complete path to json files"
        )
        parser.add_argument(
            "--shares-level",
            dest="shares_level",
            type=int,
            default=0,
            help="Level of the public share, starting from public institution = 0"
        )
        parser.add_argument(
            "--tax-ids",
            nargs='*', metavar='TAX_ID',
            dest="tax_ids",
            help="Only consider these tax_ids (used for debugging)",
        )
        parser.add_argument(
            "--classifications",
            nargs='*', metavar='CLASS',
            dest="classifications", type=int,
            help="Only process specified classifications, by ID "
            "(by id, ex: Ministero, Consiglio Reg., Città Metrop.: 498 133 279)",
        )
        parser.add_argument(
            "--use-atoka-cache",
            dest="use_atoka_cache",
            action='store_true',
            help="Use extract_organizations[SUFFIX].json, without fetching it anew from ATOKA."
        )
        parser.add_argument(
            "--clear-diff-cache",
            dest="clear_diff_cache",
            action='store_true',
            help="Clear diff cache, load will process all records, not just new ones."
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.batch_size = options['batch_size']
        self.data_path = options['data_path']
        self.shares_level = options['shares_level']
        self.tax_ids = options.get('tax_ids') or []
        self.classifications = options.get('classifications') or []
        self.use_atoka_cache = options['use_atoka_cache']
        self.clear_diff_cache = options['clear_diff_cache']

        if len(self.tax_ids) or len(self.classifications) or self.shares_level:
            import hashlib
            m = hashlib.sha256()
            if len(self.tax_ids):
                m.update(bytes(str(self.tax_ids), 'utf8'))
            if len(self.classifications):
                m.update(bytes(str(self.classifications), 'utf8'))
            if self.shares_level:
                m.update(bytes(str(self.shares_level), 'utf8'))
            self.filename_suffix = f"_{m.hexdigest()[:8]}"
        else:
            self.filename_suffix = ""

        self.logger.info("Start overall procedure")

        if self.shares_level < 0 or self.shares_level > 2:
            raise Exception("--shares-level must be between 0 and 2")

        self.verbosity = options.get("verbosity", 1)

        if not self.use_atoka_cache:
            if os.path.exists(os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json')):
                os.unlink(os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json'))

        if not os.path.exists(os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json')):
            self.extract_json()

        self.transform(['organizations'])
        self.load_organizations()
        self.transform(['ownerships', 'persons_memberships'])
        self.load_ownerships()
        self.load_persons_and_memberships()

        self.logger.info("End overall procedure")

        return self.filename_suffix

    def extract_json(self):
        self.logger.info("Extract json")
        management.call_command(
            'import_atoka_extract_organizations',
            verbosity=self.verbosity,
            json_file=os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json'),
            batch_size=self.batch_size,
            shares_level=self.shares_level,
            tax_ids=self.tax_ids,
            classifications=self.classifications,
            stdout=self.stdout
        )

    def transform(self, contexts=None):
        if contexts is None:
            contexts = ['organizations', 'ownerships', 'persons_memberships']
        self.logger.info(
            f"Transform organizations, ownerships, persons_memberships, using suffix: {self.filename_suffix}"
        )
        management.call_command(
            'import_atoka_transform_organizations_json',
            verbosity=self.verbosity,
            json_source_file=os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json'),
            filename_suffix=self.filename_suffix,
            json_output_path=self.data_path,
            contexts=contexts,
            stdout=self.stdout
        )

    def load_organizations(self):
        self.logger.info("Load organizations")
        management.call_command(
            'import_orgs_from_json',
            os.path.join(self.data_path, f'organizations_from_organizations{self.filename_suffix}.json'),
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            lookup_strategy='mixed_current',
            update_strategy='keep_old_overwrite_cf',
            log_step=100,
            stdout=self.stdout
        )

    def load_ownerships(self):
        self.logger.info("Load ownerships")
        management.call_command(
            'import_ownerships_from_json',
            os.path.join(self.data_path, f'ownerships_from_organizations{self.filename_suffix}.json'),
            original_source='http://api.atoka.io',
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            close_missing=True,
            lookup_strategy='mixed_current',
            log_step=100,
            stdout=self.stdout
        )

    def load_persons_and_memberships(self):
        self.logger.info("Load persons and memberships")
        management.call_command(
            'import_persons_from_json',
            os.path.join(self.data_path, f'persons_memberships_from_organizations{self.filename_suffix}.json'),
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            close_missing=True,
            log_step=100, context='atoka',
            use_dummy_transformation=True,
            stdout=self.stdout
        )
