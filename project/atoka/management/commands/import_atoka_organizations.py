# -*- coding: utf-8 -*-
import os

import pandas as pd
from django.core import management
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Starting from ids, classifications or partecipation level of organizations already in OPDM, " \
        "extracts all related data from Atoka (members, ownerships, owners)"
    verbosity = None
    batch_size = None
    data_path = None
    use_atoka_cache = False
    clear_diff_cache = None
    filename_suffix = None
    shares_level = None
    classifications = []
    overwrite_part_level = None
    ids = []
    ids_type = None
    ids_source = None
    opdm_context = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--data-path",
            dest="data_path",
            default="./data/atoka",
            help="Complete path to json files"
        )
        parser.add_argument(
            "--ids-type",
            dest="ids_type",
            default="tax_id",
            help="Type of id field to lookup in ATOKA (tax_id, atoka_id)"
        )
        parser.add_argument(
            "--ids",
            nargs='*', metavar='ID',
            dest="ids",
            help="Only consider these ids (used for debugging)",
        )
        parser.add_argument(
            "--ids-source",
            dest="ids_source",
            help="File from where ids may be read (relative, absolute path, or URL)"
        )
        parser.add_argument(
            "--shares-level",
            dest="shares_level",
            type=int,
            help="The level of shares we're starting the fetch from. "
            "(0-institutions, 1-direct ownership, 2-indirect ownership, ...)"
        )
        parser.add_argument(
            "--classifications",
            nargs='*', metavar='CLASS',
            dest="classifications", type=int,
            help="Only process specified classifications, by ID "
            "(by id, ex: Ministero, Consiglio Reg., Città Metrop.: 498 133 279)",
        )
        parser.add_argument(
            "--opdm-context",
            dest="opdm_context",
            default="OPDM",
            help="OP_CONTESTO classification value to add to all loaded items"
        )
        parser.add_argument(
            "--use-atoka-cache",
            dest="use_atoka_cache",
            action='store_true',
            help="Use extract_organizations[SUFFIX].json, without fetching it anew from ATOKA."
        )
        parser.add_argument(
            "--clear-diff-cache",
            dest="clear_diff_cache",
            action='store_true',
            help="Clear diff cache, load will process all records, not just new ones."
        )
        parser.add_argument(
            "--overwrite-part-level",
            dest="overwrite_part_level",
            action="store_true",
            help="Assign partecipation level to orgs already having it; "
            "useful to assign INTERESSE_PUBBLICO_OP classifications"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.batch_size = options['batch_size']
        self.data_path = options['data_path']
        self.ids = options.get('ids') or []
        self.ids_type = options['ids_type']
        self.ids_source = options.get('ids_source', None)
        self.shares_level = options.get('shares_level', None)
        self.opdm_context = options.get('opdm_context', None)
        self.classifications = options.get('classifications') or []
        self.use_atoka_cache = options['use_atoka_cache']
        self.clear_diff_cache = options['clear_diff_cache']
        self.overwrite_part_level = options['overwrite_part_level']

        if self.ids_source:
            self.ids = list(pd.read_csv(self.ids_source, dtype=object)['id'])

        if (
            len(self.ids) or len(self.classifications) or
            self.shares_level is not None
        ):
            import hashlib
            m = hashlib.sha256()
            m.update(bytes("pl", 'utf8'))
            if len(self.ids):
                m.update(bytes(str(self.ids), 'utf8'))
            if len(self.classifications):
                m.update(bytes(str(self.classifications), 'utf8'))
            if self.shares_level is not None:
                m.update(bytes(str(self.shares_level), 'utf8'))
            if self.opdm_context is not None:
                m.update(bytes(str(self.opdm_context), 'utf8'))
            self.filename_suffix = f"_{m.hexdigest()[:8]}"
        else:
            self.logger.error(
                "At least one of these arguments should be passed: ids, ids-source, classifications, shares-level"
            )
            exit(1)

        self.logger.info("Start procedure")

        self.verbosity = options.get("verbosity", 1)

        self.correct_organizations_identifiers()

        # assign partecipation level to organizations at level share_level
        # this is needed before identifying orgs to start from
        if self.shares_level:
            self.assign_partecipation_level(self.shares_level, overwrite=self.overwrite_part_level)

        if not self.use_atoka_cache:
            if os.path.exists(os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json')):
                os.unlink(os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json'))

        if not os.path.exists(os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json')):
            self.extract_json()

        self.transform(['organizations'])
        self.load_organizations()
        if self.shares_level and self.shares_level == 0:
            self.transform(['organizations_ownerships', ])
            self.load_organizations_ownerships()
        else:  # if self.shares_level is None or > 0
            self.transform(['organizations_ownerships', 'persons_memberships_ownerships'])
            self.load_organizations_ownerships()
            self.load_persons_memberships_ownerships()

        # assign next partecipation level to organizations,
        # using ownerships info just loaded in opdm
        if self.shares_level:
            self.assign_partecipation_level(self.shares_level + 1)

        self.logger.info(f"End overall procedure - suffix: {self.filename_suffix}")

        return self.filename_suffix

    def correct_organizations_identifiers(self):
        self.logger.info("Updating organizations main identifiers with their CF")
        management.call_command(
            'script_correct_organizations_identifiers',
            verbosity=self.verbosity,
            stdout=self.stdout
        )

    def assign_partecipation_level(self, level, overwrite=False):
        self.logger.info(f"Assigning partecipation level to organizations ({self.shares_level})")
        management.call_command(
            'script_assign_partecipation_level',
            verbosity=self.verbosity,
            threshold=0.25,
            shares_level=level,
            overwrite=overwrite,
            stdout=self.stdout
        )

    def extract_json(self):
        self.logger.info("Extract json")
        management.call_command(
            'import_atoka_extract_organizations',
            verbosity=self.verbosity,
            json_file=os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json'),
            batch_size=self.batch_size,
            classifications=self.classifications,
            ids=self.ids,
            ids_type=self.ids_type,
            shares_level=self.shares_level,
            stdout=self.stdout
        )

    def transform(self, contexts=None):
        if contexts is None:
            contexts = ['organizations', 'organizations_ownerships', 'persons_memberships_ownerships']
        self.logger.info(
            f"Transform organizations, using suffix: {self.filename_suffix}"
        )
        management.call_command(
            'import_atoka_transform_organizations_json',
            verbosity=self.verbosity,
            json_source_file=os.path.join(self.data_path, f'extract_organizations{self.filename_suffix}.json'),
            filename_suffix=self.filename_suffix,
            json_output_path=self.data_path,
            contexts=contexts,
            stdout=self.stdout
        )

    def load_organizations(self):
        self.logger.info("Load organizations")
        management.call_command(
            'import_orgs_from_json',
            os.path.join(self.data_path, f'organizations_from_organizations{self.filename_suffix}.json'),
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            lookup_strategy='mixed_current',
            update_strategy='keep_old_overwrite_cf',
            opdm_context=self.opdm_context,
            private_company_verification=True,
            log_step=100,
            stdout=self.stdout
        )

    def load_organizations_ownerships(self):
        self.logger.info("Load organization ownerships")
        management.call_command(
            'import_ownerships_from_json',
            os.path.join(self.data_path, f'organizations_ownerships_from_organizations{self.filename_suffix}.json'),
            original_source='https://api.atoka.io',
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            lookup_strategy='mixed_current',
            private_company_verification=True,
            log_step=100,
            stdout=self.stdout
        )

    def load_persons_memberships_ownerships(self):
        self.logger.info("Load persons, memberships and ownerships")
        management.call_command(
            'import_persons_from_json',
            os.path.join(
                self.data_path, f'persons_memberships_ownerships_from_organizations{self.filename_suffix}.json'
            ),
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            log_step=100, context='atoka',
            use_dummy_transformation=True,
            stdout=self.stdout
        )
