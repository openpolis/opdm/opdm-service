# -*- coding: utf-8 -*-
import os

from django.core import management
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Metacommand that executes all etl procedures, to fetch data starting from OPDM persons"
    verbosity = None
    batch_size = None
    data_path = None
    shares_level = None
    tax_ids = []
    opdm_ids_file = None
    classifications = []
    min_revenue = None
    min_shares_percentage = None
    min_employees = None
    active = True
    use_atoka_cache = False
    clear_diff_cache = False
    filename_suffix = None

    def add_arguments(self, parser):
        parser.add_argument(
            nargs='*', metavar='CLASS',
            dest="classifications", type=int,
            help="Only process specified classifications "
            "(by id, ex: Ministero, Consiglio Reg., Città Metrop.: 498 133 279)",
        )
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of persons processed at once",
        )
        parser.add_argument(
            "--data-path",
            dest="data_path",
            default="./data/atoka",
            help="Complete path to json files"
        )
        parser.add_argument(
            '--active',
            dest='active',
            action='store_true',
            help="Whether to consider only currently active roles, or all. Defaults to all."
        )
        parser.add_argument(
            "--min-revenue",
            dest="min_revenue", type=int,
            default=100000,
            help="Only consider organizations having a known revenue greater or equal to this."
            "Goes together with --min-employees, with boolean OR logic.",
        )
        parser.add_argument(
            "--min-employees",
            dest="min_employees", type=int,
            default=10,
            help="Only consider organizations having a known n. of employees greater or equal to this."
            "Goes together with --min-revenue, with boolean OR logic.",
        )
        parser.add_argument(
            "--min-shares-percentage",
            dest="min_shares_percentage", type=float,
            default=1.0,
            help="Only consider owners with a shares ratio greater or equal than this.",
        )
        parser.add_argument(
            "--tax-ids",
            nargs='*', metavar='TAX_ID',
            dest="tax_ids",
            help="Only consider these tax_ids (used for debugging)",
        )
        parser.add_argument(
            "--opdm-ids-file",
            dest="opdm_ids_file",
            help="Only consider persons whose OPDM ids are found in the given text file "
            "(path or URL to file, one ID per line)",
        )
        parser.add_argument(
            "--use-atoka-cache",
            dest="use_atoka_cache",
            action='store_true',
            help="Use extract_organizations[SUFFIX].json, without fetching it anew from ATOKA."
        )
        parser.add_argument(
            "--clear-diff-cache",
            dest="clear_diff_cache",
            action='store_true',
            help="Clear diff cache, load will process all records, not just new ones."
        )

    def section_log(self, title):
        self.logger.info(" ")
        self.logger.info(title)
        self.logger.info("*" * len(title))

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.batch_size = options['batch_size']
        self.data_path = options['data_path']
        self.tax_ids = options.get('tax_ids') or []
        self.opdm_ids_file = options.get('opdm_ids_file', None)
        self.classifications = options.get('classifications')
        self.min_revenue = options.get('min_revenue')
        self.min_employees = options.get('min_employees')
        self.min_shares_percentage = options.get('min_shares_percentage')
        self.active = options.get("active")
        self.use_atoka_cache = options['use_atoka_cache']
        self.clear_diff_cache = options['clear_diff_cache']

        if len(self.tax_ids) or self.shares_level or len(self.classifications) or self.opdm_ids_file:
            import hashlib
            m = hashlib.sha256()
            if len(self.tax_ids):
                m.update(bytes(str(self.tax_ids), 'utf8'))
            if self.opdm_ids_file:
                m.update(bytes(str(self.opdm_ids_file), 'utf8'))
            if len(self.classifications):
                m.update(bytes(str(self.classifications), 'utf8'))
            if self.shares_level:
                m.update(bytes(str(self.shares_level), 'utf8'))
            self.filename_suffix = f"_{m.hexdigest()[:8]}"
        else:
            self.filename_suffix = ""

        if self.min_revenue < 0:
            raise Exception("min_revenue must be a number greater than 0")

        if self.min_employees < 0:
            raise Exception("min_employees must be a number greater than 0")

        if self.min_shares_percentage < 0:
            raise Exception("min_shares_percentage must be a number greater than 0")

        self.logger.info("Start overall procedure")

        self.verbosity = options.get("verbosity", 1)

        if not self.use_atoka_cache:
            if os.path.exists(os.path.join(self.data_path, f'extract_persons{self.filename_suffix}.json')):
                os.unlink(os.path.join(self.data_path, f'extract_persons{self.filename_suffix}.json'))

        if not os.path.exists(os.path.join(self.data_path, f'extract_persons{self.filename_suffix}.json')):
            self.extract_json()

        # organizations need to be imported, in order for the successive transformations to work
        self.transform(['organizations'])
        self.load_organizations()
        self.transform(['persons_memberships_ownerships', ])
        self.load_persons_memberships_ownerships()
        self.transform(['organizations_ownerships', ])
        self.load_organizations_ownerships()

        self.logger.info(f"End overall procedure - suffix: {self.filename_suffix}")

        return self.filename_suffix

    def extract_json(self):
        self.logger.info("Extract json")
        management.call_command(
            'import_atoka_extract_persons',
            *self.classifications,
            verbosity=self.verbosity,
            json_file=os.path.join(self.data_path, f'extract_persons{self.filename_suffix}.json'),
            batch_size=self.batch_size,
            active=self.active,
            min_revenue=self.min_revenue, min_employees=self.min_employees,
            min_shares_percentage=self.min_shares_percentage,
            tax_ids=self.tax_ids,
            opdm_ids_file=self.opdm_ids_file,
            stdout=self.stdout
        )

    def transform(self, contexts: list):
        if contexts is None:
            contexts = ['organizations', 'organizations_ownerships', 'persons_memberships_ownerships']
        self.section_log("Transform {0}.".format(",".join(contexts)))
        management.call_command(
            'import_atoka_transform_persons_json',
            verbosity=self.verbosity,
            json_source_file=os.path.join(self.data_path, f'extract_persons{self.filename_suffix}.json'),
            filename_suffix=self.filename_suffix,
            json_output_path=self.data_path,
            contexts=contexts,
            stdout=self.stdout
        )

    def load_organizations(self):
        self.section_log("Load organizations")
        management.call_command(
            'import_orgs_from_json',
            os.path.join(self.data_path, f'organizations_from_persons{self.filename_suffix}.json'),
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            lookup_strategy='mixed_current',
            private_company_verification=True,
            log_step=100,
            stdout=self.stdout
        )

    def load_organizations_ownerships(self):
        self.section_log("Load organizations ownerships")
        management.call_command(
            'import_ownerships_from_json',
            os.path.join(
                self.data_path,
                f'organizations_ownerships_from_persons{self.filename_suffix}.json'
            ),
            original_source='https://api.atoka.io',
            owner_type="organization",
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            private_company_verification=True,
            lookup_strategy='identifier_current',
            log_step=100,
            stdout=self.stdout
        )

    def load_persons_memberships_ownerships(self):
        self.section_log("Load persons, with memberships and ownerships")
        management.call_command(
            'import_persons_from_json',
            os.path.join(
                self.data_path,
                f'persons_memberships_ownerships_from_persons{self.filename_suffix}.json'
            ),
            verbosity=self.verbosity,
            clear_cache=self.clear_diff_cache,
            close_missing=True,
            log_step=100, context='atoka',
            use_dummy_transformation=True,
            stdout=self.stdout
        )
