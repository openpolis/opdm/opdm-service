# -*- coding: utf-8 -*-
from django.core import management
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """This management tasks is a metacommand that executes all tasks to sync data with the ATOKA source

    It should be used when upgrading, and called by:

    .. code::python

    python manage.py import_atoka_meta --batch-size 100 --data-path ./data/atoka
    """

    help = "Metacommand that executes etl procedures to sync data from ATOKA"
    verbosity = None
    batch_size = None
    data_path = None
    use_atoka_cache = None
    clear_diff_cache = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--data-path",
            dest="data_path",
            default="./data/atoka",
            help="Complete path to json files"
        )
        parser.add_argument(
            "--use-atoka-cache",
            dest="use_atoka_cache",
            action='store_true',
            help="Use extract_organizations[SUFFIX].json, without fetching it anew from ATOKA."
        )
        parser.add_argument(
            "--clear-diff-cache",
            dest="clear_diff_cache",
            action='store_true',
            help="Clear diff cache, load will process all records, not just new ones."
        )

    def section_log(self, title):
        self.logger.info(" ")
        self.logger.info(title)
        self.logger.info("*" * len(title))

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.batch_size = options['batch_size']
        self.data_path = options['data_path']
        self.use_atoka_cache = options['use_atoka_cache']
        self.clear_diff_cache = options['clear_diff_cache']

        self.logger.info("Start overall procedure")

        self.verbosity = options.get("verbosity", 1)

        self.section_log("import_atoka_organizations --shares-level=0 --clear-diff-cache")
        management.call_command(
            'import_atoka_organizations',
            shares_level=0,
            clear_diff_cache=self.clear_diff_cache,
            use_atoka_cache=self.use_atoka_cache,
            data_path=self.data_path,
            batch_size=self.batch_size,
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("import_atoka_organizations --shares-level=1 --clear-diff-cache")
        management.call_command(
            'import_atoka_organizations',
            shares_level=1,
            clear_diff_cache=self.clear_diff_cache,
            use_atoka_cache=self.use_atoka_cache,
            data_path=self.data_path,
            batch_size=self.batch_size,
            # classifications=[144, ],
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("script_process_akas --aka_lo=90 --context-filter=atoka")
        management.call_command(
            'script_process_akas',
            aka_lo=90,
            context_filter="atoka",
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("import_atoka_organizations --shares-level=2 --clear-diff-cache")
        management.call_command(
            'import_atoka_organizations',
            shares_level=2,
            clear_diff_cache=self.clear_diff_cache,
            use_atoka_cache=self.use_atoka_cache,
            data_path=self.data_path,
            batch_size=self.batch_size,
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("script_process_akas --aka_lo=90 --context-filter=atoka")
        management.call_command(
            'script_process_akas',
            aka_lo=90,
            context_filter="atoka",
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("import_atoka_persons 402 ... 133 1175  --min-revenue=100000 --active --clear-diff-cache")
        classifications = [
            402, 156, 2382, 836, 1222, 126, 1220, 87, 69, 15, 29, 360, 515, 1218,
            110, 167, 43, 422, 1197, 836, 777, 119, 1223, 826, 498, 83, 1204, 1205,
            133, 1175
        ]
        management.call_command(
            'import_atoka_persons',
            *classifications,
            min_revenue=100000,
            active=True,
            batch_size=10,
            clear_diff_cache=self.clear_diff_cache,
            use_atoka_cache=self.use_atoka_cache,
            data_path=self.data_path,
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("script_process_akas --aka_lo=90 --context-filter=atoka")
        management.call_command(
            'script_process_akas',
            aka_lo=90,
            context_filter="atoka",
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("import_atoka_organizations_economics --clear-diff-cache")
        management.call_command(
            'import_atoka_organizations_economics',
            clear_diff_cache=True,
            data_path=self.data_path,
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("script_compute_missing_cfs")
        management.call_command(
            'script_compute_missing_cfs',
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("script_consolidate_persons")
        management.call_command(
            'script_consolidate_persons',
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        # merge duplicate organization, merging data
        self.section_log("script_merge_duplicate_organizations")
        management.call_command(
            'script_merge_duplicate_organizations',
            verbosity=self.verbosity,
            stdout=self.stdout
        )
