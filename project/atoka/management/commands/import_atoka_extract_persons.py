import json
import time

from taskmanager.management.base import LoggingBaseCommand
from popolo.models import Person

from project.core import batch_generator
from atokaconn import AtokaConn
from django.conf import settings
from project.atoka.etl.extractors import AtokaPeopleExtractor


class Command(LoggingBaseCommand):
    help = "Starting from persons in OPDM, extract info from ATOKA's API " \
        "and store them as an array into a local JSON file"

    atoka_conn = AtokaConn(
        service_url=settings.ATOKA_API_ENDPOINT,
        version=settings.ATOKA_API_VERSION,
        key=settings.ATOKA_API_KEY,
    )

    def add_arguments(self, parser):
        parser.add_argument(
            nargs='*', metavar='CLASS',
            dest="classifications", type=int,
            help="Only process specified classifications "
            "(by id, ex: Ministero, Consiglio Reg., Città Metrop.: 498 133 279)",
        )

        parser.add_argument(
            '--active',
            dest='active',
            action='store_true',
            help="Whether to consider only currently active roles, or all. Defaults to all."
        )

        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=25,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--offset",
            dest="offset", type=int,
            default=0,
            help="Start processing from offset (useful during development)",
        )
        parser.add_argument(
            "--tax-ids",
            nargs='*', metavar='TAX_ID',
            dest="tax_ids",
            help="Only consider these tax_ids (used for debugging)",
        )
        parser.add_argument(
            "--opdm-ids-file",
            dest="opdm_ids_file",
            help="Only consider persons whose OPDM ids are found in the given text file "
            "(path or URL to file, one ID per line)",
        )
        parser.add_argument(
            "--json-file",
            dest="jsonfile",
            default="./data/atoka/extract_persons.json",
            help="Complete path to json file"
        )
        parser.add_argument(
            "--min-revenue",
            dest="min_revenue", type=int,
            default=100000,
            help="Only consider organizations having a known revenue greater or equal to this."
            "Goes together with --min-employees, with boolean OR logic.",
        )
        parser.add_argument(
            "--min-employees",
            dest="min_employees", type=int,
            default=10,
            help="Only consider organizations having a known n. of employees greater or equal to this."
            "Goes together with --min-revenue, with boolean OR logic.",
        )
        parser.add_argument(
            "--min-shares-percentage",
            dest="min_shares_percentage", type=float,
            default=1.0,
            help="Only consider owners with a shares ratio greater or equal than this.",
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        batch_size = options['batch_size']
        offset = options['offset']
        jsonfile = options['jsonfile']
        classifications = options.get('classifications', [])
        tax_ids = options.get('tax_ids')
        opdm_ids_file = options.get('opdm_ids_file', None)
        min_revenue = options.get('min_revenue')
        min_employees = options.get('min_employees')
        min_shares_percentage = options.get('min_shares_percentage')

        opdm_ids = []

        if not min_revenue or min_revenue < 0:
            raise Exception("min_revenue must be a number greater than 0")

        if not min_shares_percentage or min_shares_percentage < 0:
            raise Exception("min_shares_percentage must be a number greater than 0")

        self.logger.info("Start extraction procedure")

        persons_qs = Person.objects.all()

        filters = {}

        # filter out past roles, if required
        if options['active']:
            filters.update({
                'memberships__end_date__isnull': True
            })

        if opdm_ids_file:
            if 'http://' in opdm_ids_file or 'https://' in opdm_ids_file:
                import requests
                res = requests.get(opdm_ids_file)
                if res.status_code == 200:
                    content = res.text
                else:
                    raise Exception(f"Could not fetch {opdm_ids_file}. {res.reason}.")
            else:
                try:
                    with open(opdm_ids_file, 'r') as f:
                        content = f.read()
                except Exception as e:
                    raise Exception(f"While reading {opdm_ids_file}. {e}.")

            # strip first row and convert into int
            opdm_ids = [int(x) for x in content.strip().split("\n")[1:]]

        if opdm_ids:
            filters.update({
                'id__in': opdm_ids
            })
        else:
            # fetch classifications for contexts
            if classifications:
                filters.update({
                    'memberships__organization__classifications__classification_id__in': classifications
                })

            if tax_ids:
                filters.update({
                    'identifiers__scheme': 'CF',
                    'identifiers__identifier__in': tax_ids
                })

        persons_qs = persons_qs.filter(**filters)
        n_persons = persons_qs.count()

        self.logger.info(
            "Fetching {0} persons with{1}roles in {2} classifications.".format(
                n_persons,
                " current " if options['active'] else " ",
                len(classifications) if classifications else "all"
            )
        )

        cfs = persons_qs.filter(
            identifiers__scheme='CF'
        ).distinct().values_list('identifiers__identifier', flat=True).iterator()

        # generate batches of batch_size, to query atoka's endpoint
        # flatten iterators of cfs
        batches = batch_generator(batch_size, cfs)

        atoka_records = {'people': {}, 'c_people': {}, 'c_organizations': {}}
        atoka_people_requests = 0
        atoka_companies_requests = 0
        people_ids = []
        organizations_ids = []
        c_people_ids = []
        c_organizations_ids = []

        counter = 0
        for tax_ids_batch in batches:
            # implement offset
            if counter >= offset:
                atoka_extractor = AtokaPeopleExtractor(
                    tax_ids_batch,
                    min_revenue=min_revenue,
                    min_employees=min_employees,
                    min_shares_percentage=min_shares_percentage
                )
                atoka_extractor.logger = self.logger

                counter += len(tax_ids_batch)

                atoka_res = atoka_extractor.extract()

                # complex extension of results, avoid duplications
                people = atoka_res['results']['people']
                for pid, person in people.items():
                    if pid not in atoka_records['people']:
                        atoka_records['people'][pid] = person
                    else:
                        for org in person['organizations']:
                            if org not in atoka_records['people'][pid]['organizations']:
                                atoka_records['people'][pid]['organizations'][org['id']] = org

                c_people = atoka_res['results']['c_people']
                for pid, person in c_people.items():
                    if pid not in atoka_records['c_people']:
                        atoka_records['c_people'][pid] = person
                    else:
                        atoka_records['c_people'][pid]['roles'].extend(person['roles'])
                        atoka_records['c_people'][pid]['shares_owned'].extend(person['shares_owned'])

                c_organizations = atoka_res['results']['c_organizations']
                for oid, organization in c_organizations.items():
                    if oid not in atoka_records['c_organizations']:
                        atoka_records['c_organizations'][oid] = organization
                    else:
                        atoka_records['c_organizations'][oid]['shares_owned'].extend(organization['shares_owned'])
                        atoka_records['c_organizations'][oid]['shareholders'].extend(organization['shareholders'])

                atoka_people_requests += atoka_res['meta']['atoka_requests']['people']
                atoka_companies_requests += atoka_res['meta']['atoka_requests']['companies']

                people_ids.extend(atoka_res['meta']['ids']['people'])
                people_ids = list(set(people_ids))

                organizations_ids.extend(atoka_res['meta']['ids']['organizations'])
                organizations_ids = list(set(organizations_ids))

                c_people_ids.extend(atoka_res['meta']['ids']['c_people'])
                c_people_ids = list(set(c_people_ids))

                c_organizations_ids.extend(atoka_res['meta']['ids']['c_organizations'])
                c_organizations_ids = list(set(c_organizations_ids))

                self.logger.info(
                    "{0} tax_ids, {1} people, {2} organizations, "
                    "{3} conn. people, {4} conn. organizations --------".format(
                        counter, len(people_ids), len(organizations_ids),
                        len(c_people_ids), len(c_organizations_ids)
                    )
                )
            else:
                self.logger.info("skipping {0} tax_ids untill {1}".format(
                    len(tax_ids_batch), offset
                    )
                )

            time.sleep(1.)

        self.logger.info(
            "crediti spesi con atoka: {0} people, {1} companies".format(
                atoka_people_requests, atoka_companies_requests
            )
        )

        # produce the json file
        self.logger.info("Dati scritti in {0}".format(jsonfile))
        with open(jsonfile, "w") as f:
            json.dump(atoka_records, f)

        self.logger.info("End")
