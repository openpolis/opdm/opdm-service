# -*- coding: utf-8 -*-
import os

from ooetl import ETL
from ooetl.loaders import JsonLoader

from project.atoka.etl.transformations import AtokaOrganizationEconomicsTransformation
from project.atoka.etl.transformations.organizations import AtokaOrganizationsFromOrganizationsTransformation
from project.atoka.etl.transformations.ownerships import AtokaOwnershipsFromOrganizationsTransformation
from project.atoka.etl.transformations.persons_memberships \
    import AtokaPersonsMembershipsOwnershipsFromOrganizationsTransformation
from project.tasks.etl.extractors import JsonArrayExtractor, ListExtractor
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Transform information from ATOKA, loading them from a JSON file, producing files for successive purposes"

    def add_arguments(self, parser):
        parser.add_argument(
            "--json-source-file",
            dest="json_src",
            default="./data/atoka/extract_organizations.json",
            help="Complete path to source file with atoka info"
        )
        parser.add_argument(
            "--json-output-path",
            dest="json_out_path",
            default="./data/atoka",
            help="Path to directory where emitted json files will be stored"
        )
        parser.add_argument(
            "--filename-suffix",
            dest="filename_suffix",
            default="",
            help="Suffix used to differentiate cache"
        )
        parser.add_argument(
            "--contexts",
            dest="contexts",
            metavar='CONTEXT',
            nargs='*',
            default=[
                'organizations', 'organization_economics',
                'organizations_ownerships', 'persons_memberships_ownerships'
            ],
            help="Context allows to select the transformation class to apply",
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        json_src = options['json_src']
        json_out_path = options['json_out_path']
        filename_suffix = options['filename_suffix']
        contexts = options['contexts']

        self.logger.info("Start transformation procedure")

        atoka_records = JsonArrayExtractor(json_src).extract()

        if 'organizations' in contexts:
            json_filename = f"organizations_from_organizations{filename_suffix}.json"
            self.logger.info("organizations")

            ETL(
                extractor=ListExtractor(atoka_records),
                transformation=AtokaOrganizationsFromOrganizationsTransformation(),
                loader=JsonLoader(os.path.join(json_out_path, json_filename)),
                log_level=self.logger.level,
                # source="http://api.atoka.it"
            )()
            self.logger.info(
                "Dati scritti in {0}".format(os.path.join(json_out_path, json_filename))
            )

        if 'organizations_economics' in contexts:
            self.logger.info("organizations_economics")
            json_filename = f"organizations_economics_from_organizations{filename_suffix}.json"
            ETL(
                extractor=ListExtractor(atoka_records),
                transformation=AtokaOrganizationEconomicsTransformation(),
                loader=JsonLoader(os.path.join(json_out_path, json_filename)),
                log_level=self.logger.level,
                # source="http://api.atoka.it"
            )()
            self.logger.info(
                "Dati scritti in {0}".format(os.path.join(json_out_path, json_filename))
            )

        if 'organizations_ownerships' in contexts:
            self.logger.info("ownerships")
            json_filename = f"organizations_ownerships_from_organizations{filename_suffix}.json"
            ETL(
                extractor=ListExtractor(atoka_records),
                transformation=AtokaOwnershipsFromOrganizationsTransformation(),
                loader=JsonLoader(os.path.join(json_out_path, json_filename)),
                log_level=self.logger.level,
                # source="http://api.atoka.it"
            )()
            self.logger.info(
                "Dati scritti in {0}".format(os.path.join(json_out_path, json_filename))
            )

        # update or create memberships taken from atoka_records
        if 'persons_memberships_ownerships' in contexts:
            self.logger.info("persons_memberships_ownerships")
            json_filename = f"persons_memberships_ownerships_from_organizations{filename_suffix}.json"
            ETL(
                extractor=ListExtractor(atoka_records),
                transformation=AtokaPersonsMembershipsOwnershipsFromOrganizationsTransformation(),
                loader=JsonLoader(os.path.join(json_out_path, json_filename)),
                log_level=self.logger.level,
                # source="http://api.atoka.it"
            )()
            self.logger.info(
                "Dati scritti in {0}".format(os.path.join(json_out_path, json_filename))
            )

        self.logger.info("Stop transformation procedure")
