# -*- coding: utf-8 -*-
import logging

from taskmanager.management.base import LoggingBaseCommand

from project.atoka.etl.loaders import OrganizationEconomicsLoader
from project.tasks.etl.composites import JsonDiffCompositeETL
from project.tasks.etl.extractors import JsonArrayExtractor
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Import Organizations economics details from a remote or local json source"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_url", help="Source of the JSON file (http[s]:// or file:///)"
        )
        parser.add_argument(
            "--update-strategy",
            dest="update_strategy",
            default="overwrite",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to keep_old",
        )
        parser.add_argument(
            "--identifier-scheme",
            dest="identifier_scheme",
            default='ATOKA_ID',
            help="Which scheme to use with identifier/mixed lookup strategy",
        )
        parser.add_argument(
            "--log-step",
            dest="log_step",
            type=int,
            default=500,
            help="Number of steps to log process completion to stdout. Defaults to 500.",
        )
        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.handle_cache(*args, **options)

        update_strategy = options["update_strategy"]
        identifier_scheme = options["identifier_scheme"]
        source_url = options["source_url"]
        filename = source_url[source_url.rfind("/")+1:]
        log_step = options["log_step"]

        # define the instance and invoke the etl() method through __call__()
        self.logger.info("Starting loading procedure")
        self.logger.info("Reading json file from: {0}".format(source_url))
        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(source_url),
            loader=OrganizationEconomicsLoader(
                update_strategy=update_strategy,
                identifier_scheme=identifier_scheme,
                log_step=log_step
            ),
            key_getter=lambda x: x['atoka_id'],
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename=filename,
            clear_cache=self.clear_cache,
            log_level=self.logger.level,
            source=source_url
        )()
        self.logger.info("End loading procedure")
