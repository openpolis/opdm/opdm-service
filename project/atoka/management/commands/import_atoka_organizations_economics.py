# -*- coding: utf-8 -*-
import os

import pandas as pd
from django.core import management
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Metacommand that executes all etl procedures to fetch aconomics details for organizations with an ATOKA id"
    verbosity = None
    batch_size = None
    data_path = None
    shares_level = None
    clear_cache = False
    tax_ids = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--data-path",
            dest="data_path",
            default="./data/atoka",
            help="Complete path to json files"
        )
        parser.add_argument(
            "--use-atoka-cache",
            dest="use_atoka_cache",
            action='store_true',
            help="Use extract_organizations_economics.json not fetching it anew from atoka"
        )
        parser.add_argument(
            "--clear-diff-cache",
            dest="clear_diff_cache",
            action='store_true',
            help="Clear diff cache, load will process all records, not just new ones."
        )
        parser.add_argument(
            "--ids-source",
            dest="ids_source",
            help="File from where ids may be read (relative, absolute path, or URL)"
        )
        parser.add_argument(
            "--ids-type",
            dest="ids_type",
            default="tax_id",
            help="Type of id field to lookup in ATOKA (tax_id, atoka_id)"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.batch_size = options['batch_size']
        self.data_path = options['data_path']
        self.use_atoka_cache = options['use_atoka_cache']
        self.clear_diff_cache = options['clear_diff_cache']
        self.ids = options.get('ids') or []
        self.ids_type = options['ids_type']
        self.verbosity = options['verbosity']
        ids_source = options.get('ids_source', None)

        if ids_source:
            self.ids = list(pd.read_csv(ids_source, dtype=object)['id'])

        import hashlib
        m = hashlib.sha256()
        m.update(bytes("pl", 'utf8'))
        if len(self.ids):
            m.update(bytes(str(self.ids), 'utf8'))
        self.filename_suffix = f"_{m.hexdigest()[:8]}"

        self.json_file_path = os.path.join(
            self.data_path, f'extract_organizations_economics{self.filename_suffix}.json'
        )
        if not self.use_atoka_cache:
            if os.path.exists(self.json_file_path):
                os.unlink(self.json_file_path)

        if not os.path.exists(self.json_file_path):
            self.extract_json()

        self.logger.info("Start overall procedure")

        self.verbosity = options.get("verbosity", 1)

        self.transform()
        self.load_economics()

        self.logger.info("End overall procedure")

    def extract_json(self):
        self.logger.info("Extract json")
        management.call_command(
            'import_atoka_extract_organizations_economics',
            verbosity=self.verbosity,
            json_file=self.json_file_path,
            ids=self.ids,
            ids_type=self.ids_type,
            batch_size=self.batch_size,
            stdout=self.stdout
        )

    def transform(self):
        self.logger.info("Transform organizations_economics")
        management.call_command(
            'import_atoka_transform_organizations_json',
            verbosity=self.verbosity,
            json_source_file=self.json_file_path,
            json_output_path=self.data_path,
            contexts=['organizations_economics'],
            filename_suffix=self.filename_suffix,
            stdout=self.stdout
        )

    def load_economics(self):
        self.logger.info("Load organizations_economics")
        management.call_command(
            'import_org_economics_from_json',
            os.path.join(self.data_path, f"organizations_economics_from_organizations{self.filename_suffix}.json"),
            clear_cache=self.clear_diff_cache,
            verbosity=self.verbosity,
            log_step=100,
            stdout=self.stdout
        )
