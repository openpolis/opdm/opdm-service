# -*- coding: utf-8 -*-
import json

from popolo.models import Organization
from taskmanager.management.base import LoggingBaseCommand

from project.core import batch_generator
from project.atoka.etl.extractors import AtokaEconomicsExtractor


class Command(LoggingBaseCommand):
    help = "Extract economics info from ATOKA's API and store them as an array into a local JSON file"

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--offset",
            dest="offset", type=int,
            default=0,
            help="Start processing",
        )
        parser.add_argument(
            "--json-file",
            dest="jsonfile",
            help="Complete path to json file"
        )
        parser.add_argument(
            "--ids-type",
            dest="ids_type",
            default="tax_id",
            help="Type of id field to lookup in ATOKA (tax_id, atoka_id)"
        )
        parser.add_argument(
            "--ids",
            nargs='*', metavar='ID',
            dest="ids",
            help="Only consider these ids (used for debugging)",
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        batch_size = options['batch_size']
        offset = options['offset']
        jsonfile = options['jsonfile']
        ids = options.get('ids') or []
        ids_type = options['ids_type']

        self.logger.info("Start procedure")

        organizations_values = Organization.objects.none()
        organizations_qs = Organization.objects.current().exclude(
            classifications__classification__scheme='FORMA_GIURIDICA_OP',
            classifications__classification__descr__in=['Comune', 'Comunità montana o isolana', 'Provincia', 'Regione']
        )
        if ids:
            if ids_type == "tax_id":
                organizations_qs = organizations_qs.exclude(identifier__isnull=True)
                organizations_values = organizations_qs.filter(
                    identifier__in=ids
                ).values_list('identifier', flat=True).distinct()

            if ids_type == "atoka_id":
                organizations_values = organizations_qs.filter(
                    identifiers__scheme='ATOKA_ID',
                    identifiers__identifier__in=ids
                ).filter(identifiers__scheme='ATOKA_ID').values_list(
                    'identifiers__identifier', flat=True
                ).distinct()
        else:
            organizations_values = organizations_qs.filter(identifiers__scheme='ATOKA_ID').values_list(
                'identifiers__identifier', flat=True
            ).distinct()

        atoka_records = []
        atoka_companies_requests = 0
        atoka_people_requests = 0
        counter = 0

        # generate batches of batch_size, to query atoka's endpoint
        total_count = organizations_values.count()
        batches = batch_generator(
            batch_size, organizations_values.iterator()
        )

        for _ids_batch in batches:
            # extract economics information for organizations from ATOKA

            # implement offset
            if counter >= offset:

                atoka_extractor = AtokaEconomicsExtractor(_ids_batch)
                atoka_extractor.logger = self.logger

                atoka_res = atoka_extractor.extract(ids_field_name='taxIds')
                atoka_records.extend(atoka_res['results'])

                atoka_companies_requests += atoka_res['meta']['atoka_requests']['companies']

                counter += len(_ids_batch)

                self.logger.info("{0}/{1}".format(counter, total_count))
            else:
                counter += len(_ids_batch)
                self.logger.info("skipping {0} ids".format(
                    len(_ids_batch)
                ))

            self.logger.debug("")

        self.logger.info(
            "crediti spesi con atoka: {0} companies, {1} people".format(
                atoka_companies_requests, atoka_people_requests
            )
        )

        # produce the json file
        self.logger.info("Dati scritti in {0}".format(jsonfile))
        with open(jsonfile, "w") as f:
            json.dump(atoka_records, f)
