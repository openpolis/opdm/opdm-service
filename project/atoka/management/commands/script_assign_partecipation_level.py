# -*- coding: utf-8 -*-
from django.db.models import Sum
from popolo.models import Classification, Organization
from taskmanager.management.base import LoggingBaseCommand

from project.atoka.models import OrganizationEconomics


class Command(LoggingBaseCommand):
    help = "Compute and update the partecipation level, the percentage of public partecipation " \
        "and a public interest for given organizations"
    pi = None
    no_pi = None
    threshold = None
    shares_level = None
    overwrite = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--threshold",
            dest="threshold", type=float,
            default=.25,
            help="Threshold for public control",
        )
        parser.add_argument(
            "--shares-level",
            dest="shares_level",
            type=int,
            default=0,
            help="Level of the public share, starting from public institution = 0"
        )
        parser.add_argument(
            "--overwrite",
            dest="overwrite",
            action="store_true",
            help="Assign level to orgs already having it; useful to assign INTERESSE_PUBBLICO_OP classifications"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.shares_level = options['shares_level']
        self.threshold = options['threshold']
        self.overwrite = options['overwrite']
        self.pi = Classification.objects.get(scheme='INTERESSE_PUBBLICO_OP', code='1')
        self.no_pi = Classification.objects.get(scheme='INTERESSE_PUBBLICO_OP', code='0')

        self.logger.info("Start of procedure")

        if self.shares_level < 0 or self.shares_level > 3:
            raise Exception("--shares-level must be between 0 and 3")

        cs = [
            Classification.objects.get(scheme='LIVELLO_PARTECIPAZIONE_OP', code='0'),
            Classification.objects.get(scheme='LIVELLO_PARTECIPAZIONE_OP', code='1'),
            Classification.objects.get(scheme='LIVELLO_PARTECIPAZIONE_OP', code='2'),
            Classification.objects.get(scheme='LIVELLO_PARTECIPAZIONE_OP', code='3')
        ]

        if self.shares_level > 0:

            previous_cs = cs[self.shares_level-1]
            c = cs[self.shares_level]

            # extract all orgs owned by orgs of previous level
            # excluding those already having correct level
            orgs = Organization.objects.filter(
                ownerships_as_owned__isnull=False
            ).filter(
                ownerships_as_owned__owner_organization__classifications__classification=previous_cs
            ).distinct()
            self.assign_partecipation_level(orgs, c, overwrite=self.overwrite)

        else:
            # for Level 0 organizations (institutions)

            # This excluded all private organizations, but has been suberseeded,
            # it's here to keep the list of classifications for private orgs
            #
            orgs = Organization.objects.filter(
                classifications__classification__scheme='FORMA_GIURIDICA_OP'
            ).current().exclude(
                classifications__classification_id__in=[
                    11, 20, 24, 29, 48, 69, 83, 295, 321, 346, 403, 621, 941, 730, 1182, 1183, 1184, 1185, 1186, 1187,
                    1188, 1190, 1189, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202,
                    1620, 1630, 2740
                ]
            ).filter(identifier__isnull=False)

            self.assign_partecipation_level(orgs, cs[0], overwrite=self.overwrite)

        self.logger.info("End of procedure")

    def assign_partecipation_level(self, orgs, c, overwrite=False):
        """Assign LIVELLO_PARTECIPAZIONE_OP [c] classification to org not already having it among [orgs]

        :param orgs: queryset of orgs to check
        :param c: classification level (object)
        :param overwrite: overwrite values in orgs already having it
        :return:
        """
        if not overwrite:
            orgs = orgs.exclude(classifications__classification=c)
        n_orgs = orgs.count()
        self.logger.info(f"  processing {n_orgs} organisations")
        for n, org in enumerate(orgs, start=1):
            current_level_cl_rel = org.classifications.filter(
                classification__scheme='LIVELLO_PARTECIPAZIONE_OP'
            ).first()
            if current_level_cl_rel is None or int(current_level_cl_rel.classification.code) < int(c.code):
                org.add_classification_rel(c)

            if c.code == '0':
                org.add_classification_rel(self.pi)
            if c.code == '1':
                classification_rel = self.compute_public_interest(org, self.threshold, level=1)
                if classification_rel:
                    org.add_classification_rel(classification_rel)

            if n % 100 == 0:
                self.logger.info("  processed {0}/{1}".format(n, n_orgs))

    def compute_public_interest(self, org, threshold, level):
        """Compute classification to organizations, whether they are of public interest or not

        :param org: the organization to compute the classification for
        :param threshold: sums of ownerships over this will mark the orgs as of public interest
                          a value of 0 assigns the PI flag o all orgs
        :param level: LIVELLO_PARTECIPAZIONE_OP ('1', '2')
        :return: INTERESSE_PUBBLICO_OP Classification
        """
        if level == 1:
            org_sum = org.ownerships_as_owned.filter(
                owner_organization__classifications__classification=self.pi
            ).aggregate(Sum('percentage'))['percentage__sum']
            if org_sum:
                if org_sum >= threshold:
                    e, _ = OrganizationEconomics.objects.update_or_create(
                        organization=org,
                        defaults={
                            'pub_part_percentage': org_sum
                        }
                    )
                    return self.pi
                else:
                    return self.no_pi
        elif level == 2:
            raise Exception("Level 2 public interest computation is compundend and has yet to be implemented")
        else:
            raise Exception("Level can only take values between 0 and 2")
