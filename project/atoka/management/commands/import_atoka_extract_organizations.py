# -*- coding: utf-8 -*-
import itertools
import json
import time

from popolo.models import Organization, Classification

from atokaconn import AtokaException
from taskmanager.management.base import LoggingBaseCommand

from project.core import batch_generator
from project.atoka.etl.extractors import AtokaOrganizationsCompleteExtractor


class Command(LoggingBaseCommand):
    help = "Starting from institutions in OPDM, extract organizations' basic info from ATOKA's API " \
        "and store them as an array into a local JSON file, for later processing"

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=25,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--offset",
            dest="offset", type=int,
            default=0,
            help="Start processing",
        )
        parser.add_argument(
            "--classifications",
            nargs='*', metavar='CLASS',
            dest="classifications", type=int,
            help="Only process specified classifications, by ID "
            "(by id, ex: Ministero, Consiglio Reg., Città Metrop.: 498 133 279)",
        )
        parser.add_argument(
            "--ids-type",
            dest="ids_type",
            default="tax_id",
            help="Type of id field to lookup in ATOKA (tax_id, atoka_id)"
        )
        parser.add_argument(
            "--ids",
            nargs='*', metavar='ID',
            dest="ids",
            help="Only consider these ids (used for debugging)",
        )
        parser.add_argument(
            "--shares-level",
            dest="shares_level",
            type=int,
            default=0,
            help="The level of shares we're starting the fetch from. "
            "(0-institutions, 1-direct ownership, 2-indirect ownership, ...)"
        )
        parser.add_argument(
            "--json-file",
            dest="jsonfile",
            default="./data/atoka/extract_organizations.json",
            help="Complete path to json file"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        batch_size = options['batch_size']
        offset = options['offset']
        jsonfile = options['jsonfile']
        classifications = options.get('classifications', [])
        ids = options.get('ids') or []
        ids_type = options['ids_type']
        shares_level = options['shares_level']

        self.logger.info("Start procedure")

        organizations_qs = Organization.objects.current()

        if not ids and shares_level is not None:
            organizations_qs = organizations_qs.filter(
                classifications__classification__scheme='LIVELLO_PARTECIPAZIONE_OP',
                classifications__classification__code=shares_level
            )

        if classifications:
            organizations_qs = organizations_qs.filter(
                classifications__classification_id__in=classifications
            )

        if ids_type == "tax_id":
            organizations_qs = organizations_qs.exclude(identifier__isnull=True)
            if ids:
                organizations_qs = organizations_qs.filter(
                    identifier__in=ids
                )

        if ids_type == "atoka_id" and ids:
            organizations_qs = organizations_qs.filter(
                identifiers__scheme='ATOKA_ID',
                identifiers__identifier__in=ids
            )

        # group organizations by classification counting occurrences
        organizations_groups = []
        for classification in organizations_qs.exclude(classification__isnull=True).\
                values_list('classification', flat=True).distinct():
            try:
                c = Classification.objects.get(
                    scheme='FORMA_GIURIDICA_OP', descr=classification
                )
            except Classification.DoesNotExist:
                self.logger.error(
                    f"Couldn't find classification {classification} among FORMA_GIURIDICA_OP classifications"
                )
            else:
                organizations_groups.append(
                    {
                        'descr': classification,
                        'id': c.id,
                        'n': organizations_qs.filter(classifications__classification_id=c.id).count()
                    }
                )

        # non-classified organizations
        if organizations_qs.filter(classification__isnull=True).count():
            organizations_groups.append(
                {
                    'descr': 'NC',
                    'id': 0,
                    'n': organizations_qs.filter(classification__isnull=True).count()
                }
            )

        organizations_groups = sorted(organizations_groups, key=lambda x: x['n'] * -1)
        atoka_records = []
        atoka_companies_requests = 0
        atoka_people_requests = 0
        people_ids = []
        owned_ids = []
        counter = 0
        for organizations_group in organizations_groups:
            self.logger.info('processing {0} organizations classified as {1}'.format(
                organizations_group['n'], organizations_group['descr']
            ))

            if organizations_group['id']:
                organization_group_qs = organizations_qs.filter(
                    classifications__classification_id=organizations_group['id']
                )
            else:
                organization_group_qs = organizations_qs.filter(classification__isnull=True)

            if ids_type == 'tax_id':
                # extract iterators for identifiers and multiple cfs
                cfs = organization_group_qs.values_list('identifier', flat=True).distinct().iterator()
                multiple_cfs = (o.split(",") for o in organization_group_qs.filter(
                        identifiers__scheme='ALTRI_CF_ATOKA'
                    ).distinct().values_list('identifiers__identifier', flat=True).iterator())

                # generate batches of batch_size, to query atoka's endpoint
                # flatten iterators of cfs
                batches = batch_generator(
                    batch_size, itertools.chain(
                        *multiple_cfs, cfs
                    )
                )
            elif ids_type == 'atoka_id':
                # extract iterators for atoka_ids and multiple atoka_ids
                atokas = organization_group_qs\
                    .filter(identifiers__scheme='ATOKA_ID')\
                    .values_list('identifiers__identifier', flat=True)\
                    .distinct().iterator()
                multiple_atokas = map(
                    lambda x: x.split("/")[-1],
                    organization_group_qs
                    .filter(links__link__note='Altra company in ATOKA con stesso CF')
                    .values_list('links__link__url', flat=True))

                # generate batches of batch_size, to query atoka's endpoint
                # flatten iterators of atoka_ids
                batches = batch_generator(
                    batch_size, itertools.chain(
                        *multiple_atokas, atokas
                    )
                )
            else:
                raise AtokaException("Argument ids_type must take on the value 'tax_id' or 'atoka_id'.")

            group_counter = 0
            for ids_batch in batches:
                # extract atoka_ownerships into a list in memory,
                # in order to use it in the two following ETL procedures

                # implement offset
                if counter >= offset:

                    ids_batch = list(set(ids_batch))
                    atoka_extractor = AtokaOrganizationsCompleteExtractor(ids_batch, ids_type=ids_type)
                    atoka_extractor.logger = self.logger

                    atoka_res = atoka_extractor.extract()
                    atoka_records.extend(atoka_res['results'])

                    atoka_companies_requests += atoka_res['meta']['atoka_requests']['companies']
                    atoka_people_requests += atoka_res['meta']['atoka_requests']['people']

                    people_ids.extend(atoka_res['meta']['ids']['people'])
                    people_ids = list(set(people_ids))

                    owned_ids.extend(atoka_res['meta']['ids']['companies'])
                    owned_ids = list(set(owned_ids))

                    group_counter += len(ids_batch)

                    self.logger.info(
                        f"[{organizations_group['descr']}]: {group_counter} ids, "
                        f"{len(atoka_records)} valid organizations, "
                        f"{len(owned_ids)} shares_owned/shareholders, "
                        f"{len(people_ids)} people --------")
                else:
                    self.logger.info(f"[{organizations_group['descr']}]: skipping {len(ids_batch)} tax_ids")

                self.logger.debug("")

                counter += len(ids_batch)

                time.sleep(2.)

        self.logger.info(
            "crediti spesi con atoka: {0} companies, {1} people".format(
                atoka_companies_requests, atoka_people_requests
            )
        )

        # produce the json file
        self.logger.info("Dati scritti in {0}".format(jsonfile))
        with open(jsonfile, "w") as f:
            json.dump(atoka_records, f)
