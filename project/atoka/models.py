from datetime import datetime

from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

from popolo.models import Organization


current_year = datetime.now().year


class OrganizationEconomics(models.Model):
    """Keeps economics latest indicator for an Organization

    It is related 1-1 with a popolo Organization instance"""
    organization = models.OneToOneField(
        Organization,
        on_delete=models.CASCADE,
        primary_key=True,
        related_name='economics'
    )
    employees = models.PositiveIntegerField(
        blank=True, null=True,
        help_text=_("Latest number of employees")
    )
    revenue = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly revenue")
    )
    revenue_trend = models.FloatField(
        blank=True, null=True,
        validators=[MinValueValidator(0.), MaxValueValidator(100.)],
        help_text=_("Latest trend in revenues")
    )
    capital_stock = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest capital stock")
    )
    assets = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly assets")
    )
    costs = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly costs")
    )
    ebitda = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly ebitda")
    )
    mol = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly mol")
    )
    net_financial_position = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly net financial position")
    )
    production = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly production")
    )
    profit = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly profit")
    )
    purchases = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly purchases")
    )
    raw_materials_variation = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly raw materials variation")
    )
    services_and_tp_goods_charges = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly service and tp goods charges")
    )
    staff_costs = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Latest yearly staff costs")
    )
    is_public = models.BooleanField(
        default=False,
        help_text=_("If the organization is listed in the publick stock exchange")
    )
    pub_part_percentage = models.PositiveIntegerField(
        blank=True, null=True,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        help_text=_("Percentage of shares held or referrable to public institutions")
    )


class OrganizationEconomicsHistorical(models.Model):
    """Keeps economics historical indicators for an Organization

    It also contains data for the latest year.
    """

    organization = models.ForeignKey(
        OrganizationEconomics,
        on_delete=models.CASCADE,
        related_name='historical_values'
    )
    year = models.PositiveIntegerField(
        validators=[MinValueValidator(1980), MaxValueValidator(current_year)],
        help_text=_("Year of validity of economics indicator")
    )
    employees = models.PositiveIntegerField(
        blank=True, null=True,
        help_text=_("Current year's number of employees")
    )
    revenue = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Revenue for current year")
    )
    revenue_trend = models.FloatField(
        blank=True, null=True,
        validators=[MinValueValidator(0.), MaxValueValidator(100.)],
        help_text=_("Current year's revenue trend with respect to previous year")
    )
    capital_stock = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's capital stock")
    )
    assets = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's assets")
    )
    costs = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's costs")
    )
    ebitda = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's ebitda")
    )
    mol = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's mol")
    )
    net_financial_position = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's net financial position")
    )
    production = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's production")
    )
    profit = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's profit")
    )
    purchases = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's purchases")
    )
    raw_materials_variation = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's raw materials variation")
    )
    services_and_tp_goods_charges = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's service and tp goods charges")
    )
    staff_costs = models.BigIntegerField(
        blank=True, null=True,
        help_text=_("Current year's staff costs")
    )
