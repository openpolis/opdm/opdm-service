import itertools
from typing import Type, Union

from ooetl.extractors import Extractor
from django.conf import settings
from atokaconn import AtokaConn, AtokaException


class AtokaOrganizationsCompleteExtractor(Extractor):
    """:class:`Extractor` for extractions of organizations' information out of the Atoka API
    Produces a json file containing a list of dicts.
    For each dict:
      - org
        |- atoka_id
        |- tax_id
        |- ...
        |- roles[]
           |- atoka_id
           |- cf
           |- family_name, given_name, birth_date, birth_location
           |- role_label
           |- ...
        |- shares_owned[]
           |- atoka_id
           |- tax_id
           |- ...
        |- shareholders[]
           |- atoka_id
           |- tax_id
           |- ...

    Uses methods in the `AtokaConn` class, to extract information from the ATOKA api.

    Can start from a batch of tax_ids or atoka_ids, depending on the invocation context
    """

    def __init__(self, ids_batch: list, ids_type: str = 'tax_id', shares_details: bool = True, **kwargs):
        """Create a new instance of the extractor, to extract info from a batch of companies.

        Args:
            ids_batch: a list of ids for companies lookup in Atoka
            ids_type: the type of id to lookup in ATOKA (tax_id | atoka_id)
            shares_details: whether to extract shares companies details, can be set to false when extracting
                            data for atoka.align scripts, in order to reduce the number of requests to atoka's api

        Returns:
            instance of a :class:`AtokaOrganizationsCompleteExtractor`
        """
        self.ids_batch = ids_batch
        self.ids_type = ids_type
        self.shares_details = shares_details
        self.atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
            logger=self.logger
        )
        self.atoka_companies_requests = 0
        self.atoka_people_requests = 0
        self.min_shares_percentage = kwargs.get('min_shares_percentage', 1.)

        if self.ids_type.lower() not in ['tax_id', 'atoka_id']:
            raise AtokaException("ids_type attribute must take on 'tax_id' ot 'atoka_id'")

        super().__init__()

    def extract(self, **kwargs) -> dict:
        """Extract meaningful information from Atoka.

        Given a list of ids in self.ids_batch, queries Atoka API, in order to retrieve details on the companies.

        The returned dict is structured like this:
        {
            'meta': {
                'atoka_requests': {
                    'companies': int,
                    'people': int
                },
                'ids': {
                    'companies': list,
                    'people': list
                }
            },
            'results': list
        }

        Each element in the `results` list represents an **organization**, and has major identifiers and classifications
        Owned organizations are embedded in the `shares_owned` list.
        Owners are emberdded in the `shareholders` list.
        Roles within the organization, with person details and identifiers are in `roles`.

        The `results` list has the following structure:
        [
            {
                'atoka_id': owner_atoka_id,
                'tax_id': owner_tax_id,
                'other_atoka_ids': [ ... ],
                'name': owner_name,
                'legal_form': owner_atoka_legal_form_level2,
                'rea': owner_rea_id,
                'cciaa': owner_cciaa,
                'full_address': owner_full_address,
                'roles': [
                    {
                        'person': {
                            'family_name': familyName,
                            'given_name': givenName,
                            'gender': gender,
                            'birth_date': birthDate,
                            'birth_location': birthPlace,
                            'name': name,
                            'atoka_id':id,
                            'tax_id': taxId,
                        },
                        'label': role_name,
                        'start_date': role_since
                    },
                    ...
                ],
                'shares_owned': [
                    {
                        'name': owned_name,
                        'founded': owned_founded,
                        'atoka_id': owned_atoka_id,
                        'tax_id': owned_tax_id,
                        'rea': owned_rea_id,
                        'cciaa': owned_cciaa,
                        'full_address': owned_full_address,
                        'legal_form': owned_atoka_legal_form_level2,
                        'ateco': owned_ateco,
                        'percentage': ratio * 100,
                        'last_updated': share_last_updated,
                        'public': is_public
                    },
                    ...
                ],
                'shareholders': [
                    {
                        'name': org_name,
                        'founded': org_founded,
                        'atoka_id': org_atoka_id,
                        'tax_id': org_tax_id,
                        'rea': org_rea_id,
                        'cciaa': org_cciaa,
                        'full_address': org_full_address,
                        'percentage': ratio * 100,
                        'last_updated': share_last_updated,
                        'public': is_public
                    },
                    ...
                ],
            },
            ...
        ]

        :return: a dict of results and meta (counters) information
        """

        # fetch all companies among the list
        # will need batch_size=1 here, because shares may contain many results
        # and the limit is 50
        # fetching companies with all active status, since some of the non-active seems to hold
        # important ownership information on active ones
        res_doubles = {}
        if self.ids_type.lower() == 'tax_id':
            res_tot = self.atoka_conn.get_companies_from_tax_ids(
                self.ids_batch, packages='base,shares', active='*', batch_size=1
            )
            # remove obfuscated from res_tot
            res_tot = [r for r in res_tot if not r.get('obfuscated', False)]
            # return multiple results for single tax_ids, if any
            for r in res_tot:
                if r['base']['taxId'] not in res_doubles:
                    res_doubles[r['base']['taxId']] = []
                res_doubles[r['base']['taxId']].append(r['id'])
            res_doubles = {
                k: v
                for k, v in res_doubles.items() if len(v) > 1
            }
        elif self.ids_type.lower() == 'atoka_id':
            res_tot = self.atoka_conn.get_companies_from_atoka_ids(
                self.ids_batch, packages='base,shares', active='*', batch_size=1
            )
            # remove obfuscated from res_tot
            res_tot = [r for r in res_tot if not r.get('obfuscated', False)]
        else:
            raise AtokaException("The tax_id attribute can only take on one of the values: tax_id, atoka_id")

        self.atoka_companies_requests += len(res_tot)

        self.logger.debug(
            f"- da {len(self.ids_batch)} identificatori di tipo {self.ids_type}, "
            f"ricevuti da Atoka dettagli e quote per {len(res_tot)} organizzazioni "
            "(ci possono essere doppioni)"
        )

        res = self.Results(self, res_tot, res_doubles)

        self.logger.debug("adding people with roles in fetched orgs")
        people_atoka_ids = res.add_roles()

        self.logger.debug("adding orgs partially owned by fetched orgs")
        shares_owned_atoka_ids = res.add_shares_owned()

        self.logger.debug("adding orgs owning part of fetched orgs")
        shareholders_companies_atoka_ids = res.add_shareholders_companies()

        self.logger.debug("adding persons owning part of fetched orgs")
        shareholders_people_atoka_ids = res.add_shareholders_people()

        # builds dict to return, with metadata and results
        results = []
        for tax_id, result in res.res_dict.items():
            result['tax_id'] = tax_id
            results.append(result)

        return {
            'meta': {
                'atoka_requests': {
                    'companies': self.atoka_companies_requests,
                    'people': self.atoka_people_requests
                },
                'ids': {
                    'companies': list(res.atoka_tax_id_map.keys()) +
                    shares_owned_atoka_ids +
                    shareholders_companies_atoka_ids,
                    'people': people_atoka_ids + shareholders_people_atoka_ids
                }
            },
            'results': results
        }

    class Results(object):
        """Class that contains the results produced by the extractor"""

        def __init__(self, extractor, res_tot, res_doubles):
            # transform the results into a dict,
            # merging results from multiple records corresponding to the same tax_id
            self.res_dict = {}
            self.res_tot = res_tot
            self.extractor = extractor

            for r in self.res_tot:
                if r['base']['taxId'] not in self.res_dict:
                    r_dict = {
                        'atoka_id': r['id'],
                        'other_atoka_ids': [
                            atoka_id for atoka_id in res_doubles[r['base']['taxId']] if atoka_id != r['id']
                        ] if r['base']['taxId'] in res_doubles else [],
                        'name': r['name'],
                        'rea': r['base'].get('rea', None),
                        'cciaa': r['base'].get('cciaa', None),
                        'full_address': r.get('fullAddress', None),
                        'founded': r['base'].get('founded', None),
                        'ateco': r['base'].get('ateco', []),
                        'roles': [],
                        'shares_owned': [],
                        'shareholders': [],
                        'shareholders_people': [],
                    }
                    legal_forms = [
                        x['name'] for x in r['base'].get('legalForms', []) if x['level'] == 2
                    ]
                    if legal_forms:
                        r_dict['legal_form'] = legal_forms[0]

                else:
                    r_dict = self.res_dict[r['base']['taxId']]

                self.res_dict[r['base']['taxId']] = r_dict

            self.atoka_tax_id_map = {v['atoka_id']: k for k, v in self.res_dict.items()}
            self.atoka_tax_id_map.update(
                {atoka_id: k for k, v in self.res_dict.items() for atoka_id in v['other_atoka_ids']}
            )

        def add_shares(self, share_type) -> list:
            """Add shares owned or sharesholders details to res_dict

            :param share_type:
            :return:
            """
            if share_type not in ['shares_owned', 'shareholders']:
                raise AtokaException(
                    "share_type parameter can take one of these values: 'shares_owned', 'shareholders'."
                )
            if share_type == 'shares_owned':
                atoka_key = 'sharesOwned'
                label = 'partecipate'
            else:
                atoka_key = share_type
                label = share_type

            # add extracted info (non-detailed) to res_dict, and compute aotka_ids in the process
            atoka_ids = []
            for r in self.res_tot:
                if 'shares' in r:
                    r_dict = self.res_dict[r['base']['taxId']]
                    r_dict[share_type].extend(
                        {
                            'name': sho['name'],
                            'last_updated': sho['lastUpdate'],
                            'atoka_id': sho['id'],
                            'percentage': sho.get('ratio', 0.) * 100.
                        }
                        for sho in filter(
                            lambda x:
                                x.get('active', False) is True and
                                x.get('typeOfRight', None) == 'proprietà',
                            r['shares'].get(atoka_key, [])
                        )
                    )
                    atoka_ids.extend(x['atoka_id'] for x in r_dict[share_type])
            # remove doubles
            atoka_ids = list(set(atoka_ids))

            if self.extractor.shares_details:

                # collect companies details from Atoka
                orgs = self.extractor.atoka_conn.get_companies_from_atoka_ids(
                    atoka_ids, packages='base', active='true', batch_size=10
                )

                # remove obfuscated from orgs
                orgs = [r for r in orgs if not r.get('obfuscated', False)]

                self.extractor.atoka_companies_requests += len(orgs)
                self.extractor.logger.debug(f"- ricevuti dettagli per {len(orgs)} {label}")

                # put companies details info into a dict indexed with atoka_id
                orgs_dict = {
                    r['id']: {
                        'name': r['name'],
                        'cciaa': r['base'].get('cciaa', None),
                        'rea': r['base'].get('rea', None),
                        'tax_id': r['base'].get('taxId', None),
                        'vat': r['base'].get('vat', None),
                        'founded': r['base'].get('founded', None),
                        'full_address': r.get('fullAddress', None),
                        'legal_form': [x['name'] for x in r['base']['legalForms'] if x['level'] == 2][0],
                        'ateco': r['base'].get('ateco', []),
                    }
                    for r in orgs
                }

                # upgrade res_dict values with companies details values
                for tax_id, org in self.res_dict.items():
                    for o in org[share_type]:
                        o_details = orgs_dict.get(o['atoka_id'], None)
                        if o_details:
                            for f in [
                                'name', 'cciaa', 'rea', 'ateco', 'tax_id', 'vat',
                                'founded', 'legal_form', 'full_address'
                            ]:
                                if f in o_details:
                                    o[f] = o_details[f]

                        else:
                            self.extractor.logger.warning(
                                "! organizzazione {0} richiesta ad atoka, "
                                "ma non presente nei risultati".format(o['atoka_id'])
                            )

            return atoka_ids

        def add_shares_owned(self) -> list:
            """Add shares and their details to res_dict structure"""
            return self.add_shares('shares_owned')

        def add_shareholders_companies(self) -> list:
            """Add shareholders and their details to res_dict structure"""
            return self.add_shares('shareholders')

        @staticmethod
        def extract_birth_place(base: dict) -> Type[Union[str, None]]:
            """

            :param base:
            :return:
            """
            if 'birthPlace' not in base:
                return None
            if 'municipality' in base['birthPlace']:
                return "{0} ({1})".format(
                  base['birthPlace']['municipality'],
                  base['birthPlace']['provinceCode']
                )
            else:
                return "{0} ({1})".format(base['birthPlace']['state'], base['birthPlace']['stateCode'])

        def add_roles(self) -> list:
            """Add persons with memberships in companies in the focus

            :return: list of unique person ids extracted
            """
            # extract all atoka_ids of companies in the focus
            companies_ids = list(
                itertools.chain.from_iterable(
                    [r['atoka_id'], ] + r['other_atoka_ids']
                    for r in self.res_dict.values()
                )
            )

            # extract all people's atoka_ids from res_owned elements and returns flat list, removing duplicates
            people = self.extractor.atoka_conn.get_roles_from_atoka_ids(
                companies_ids, packages='base,companies',
                companiesRolesOfficial='true', companiesRoles=self.extractor.atoka_conn.allowed_roles
            )
            # remove obfuscated from people
            people = [r for r in people if not r.get('obfuscated', False)]

            self.extractor.atoka_people_requests += len(people)
            ids = []
            people_dict = {}
            for person in people:
                if (
                    'base' in person and
                    'gender' in person['base'] and
                    'familyName' in person['base'] and
                    'givenName' in person['base'] and
                    person['id'] not in ids
                ):
                    ids.append(person['id'])
                    people_dict[person['id']] = {
                        'given_name': person['base']['givenName'],
                        'family_name': person['base']['familyName'],
                        'gender': person['base']['gender'],
                        'birth_date': person['base'].get('birthDate', None),
                        'birth_location': self.extract_birth_place(person['base']),
                        'tax_id': person['base'].get('taxId', None),
                        'companies': [
                            {
                                'id': x['id'],
                                'roles': [
                                    xr for xr in x['roles']
                                    if 'official' in xr and xr['official'] is True and 'name' in xr
                                ]
                            }
                            for x in person['companies']['items'] if x['id'] in companies_ids
                        ]
                    }
            self.extractor.logger.debug(
                f"- ricevuti dettagli per {len(people)} persone ({len(ids)} distinte)"
            )

            atoka_tax_id_map = {v['atoka_id']: k for k, v in self.res_dict.items()}
            atoka_tax_id_map.update(
                {atoka_id: k for k, v in self.res_dict.items() for atoka_id in v['other_atoka_ids']}
            )

            # upgrade owned_orgs_dict with roles, from people_dict
            for atoka_id, person in people_dict.items():
                person['atoka_id'] = atoka_id
                for company in person.pop('companies', []):
                    org = self.res_dict.get(atoka_tax_id_map[company['id']])
                    if org:
                        if 'roles' not in org:
                            org['roles'] = []
                        org['roles'].extend([
                            {
                                'person': person,
                                'label': org_role['name'],
                                'start_date': org_role.get('since', None)
                            }
                            for org_role in company['roles']
                        ])
                    else:
                        self.extractor.logger.warning(
                            "! azienda {0} richiesta ad atoka nel calcolo dei ruoli per {1}, "
                            "ma non presente nei risultati".format(company['id'], person['atoka_id'])
                        )

            return ids

        def add_shareholders_people(self) -> list:
            """Add persons that are direct shareholders of companies in the focus (personal ownerships)

            :return: list of unique person ids extracted
            """
            # extract all atoka_ids of companies in the focus
            companies_ids = list(
                itertools.chain.from_iterable(
                    [r['atoka_id'], ] + r['other_atoka_ids']
                    for r in self.res_dict.values()
                )
            )

            # extract all people's atoka_ids from res_owned elements and returns flat list, removing duplicates
            people = self.extractor.atoka_conn.get_items_from_ids(
                    companies_ids, 'people', ids_field_name='sharesOwnedIds', packages='base,shares',
                    sharesOwnedRatioMin=str(self.extractor.min_shares_percentage/100.0)
            )
            # remove obfuscated from people
            people = [r for r in people if not r.get('obfuscated', False)]

            self.extractor.atoka_people_requests += len(people)
            people_ids = []
            people_dict = {}
            for person in people:
                if (
                    'base' in person and
                    'gender' in person['base'] and
                    'familyName' in person['base'] and
                    'givenName' in person['base']
                ):
                    if person['id'] not in people_ids:
                        people_ids.append(person['id'])
                        people_dict[person['id']] = {
                            'given_name': person['base']['givenName'],
                            'family_name': person['base']['familyName'],
                            'gender': person['base']['gender'],
                            'birth_date': person['base'].get('birthDate', None),
                            'birth_location': self.extract_birth_place(person['base']),
                            'tax_id': person['base'].get('taxId', None),
                            'shares_owned': []
                        }
                    people_dict[person['id']]['shares_owned'].extend(
                        {
                            'organization_id': s['id'],
                            'percentage': s.get('ratio', 0.) * 100.,
                            'last_updated': s.get('lastUpdate', None),
                        } for s in person['shares']['sharesOwned']
                        if s['id'] in list(self.atoka_tax_id_map.keys()) and 'shares' in person
                    )

            self.extractor.logger.debug(
                f"- ricevuti dettagli per {len(people)} persone ({len(people_ids)} distinte)"
            )

            # upgrade owned_orgs_dict with roles, from people_dict
            for atoka_id, person in people_dict.items():
                person['atoka_id'] = atoka_id
                for company in person.pop('shares_owned', []):
                    org = self.res_dict.get(self.atoka_tax_id_map[company['organization_id']])
                    if org:
                        org['shareholders_people'].append(
                            {
                                'person': person,
                                'percentage': company['percentage'],
                                'last_updated': company.get('last_updated', None)
                            }
                        )
                    else:
                        self.extractor.logger.warning(
                            "! azienda {0} richiesta ad atoka nel calcolo dei ruoli per {1}, "
                            "ma non presente nei risultati".format(company['organization_id'], person['atoka_id'])
                        )

            return people_ids


class AtokaPeopleExtractor(Extractor):
    """:class:`Extractor` for extractions of people information out of the Atoka API

    Uses methods in the `AtokaConn` class, to extract information from atoka api.
    """

    def __init__(self, batch: list, **kwargs):
        """Create a new instance of the extractor, to extract info from a batch of people.

        Args:
            batch: a list of tax_ids for people lookup in Atoka
            kwargs: other parameters: min_revenue, min_shares_percenage

        Returns:
            instance of a :class:`AtokaPeopleExtractor`
        """
        self.batch = batch
        self.min_revenue = kwargs.get('min_revenue', 0)
        self.min_employees = kwargs.get('min_employees', 0)
        self.min_shares_percentage = kwargs.get('min_shares_percentage', 0.)
        super().__init__()

    def extract(self, **kwargs) -> dict:
        """Extract meaningful information from Atoka.

        Given a list of tax_ids in self.batch, queries Atoka API, in order to retrieve details.

        people are returned as a list in the results.
        Each element in the list represents a **person**, and has major identifiers, classifications,
        shares of companies owned and roles in companies.

        ownerships and roles are embedded in `shares_owned` and `roles` properties.

        Each element of `shares_owned` contains anagraphical details of the organization, and the last revenue.
        Each element of `roles` contains anagraphical details of the organizations.

        [
            { ... }
        ]

        :return: a list with ownerhip and embedded details, with roles
        """

        tax_ids = self.batch
        atoka_companies_requests = 0
        atoka_people_requests = 0
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
            logger=self.logger
        )

        # the final result will be a 3-segemnts dictionary
        res_dict = {
            'people': {},
            'c_people': {},
            'c_organizations': {}
        }

        ##
        # people
        ##

        # fetch all people among the list
        # will need batch_size=1 here, because shares may contain many results
        # and the limit is 50
        # fetchin companies with all active status, since some of the non-active seems to hold
        # important ownership information on active ones
        res_tot = atoka_conn.get_people_from_tax_ids(
            tax_ids, packages='base,shares,companies', batch_size=1
        )
        # remove obfuscated from res_tot
        res_tot = [r for r in res_tot if not r.get('obfuscated', False)]

        atoka_people_requests += len(res_tot)

        self.logger.info(
            "- da {0} tax_ids, ricevuti da Atoka dettagli e quote per {1} persone "
            "(ci possono essere doppioni)".format(
                len(tax_ids), len(res_tot)
            )
        )

        # merge results from multiple records corresponding to the same tax_id
        for r in res_tot:
            if r['base']['taxId'] not in res_dict:
                r_dict = {
                    'atoka_id': r['id'],
                    'name': r['name'],
                    'organizations': {},
                }
            else:
                r_dict = res_dict[r['base']['taxId']]

            if 'shares' in r and 'sharesOwned' in r['shares']:
                for sho in filter(
                    lambda x:
                        x.get('active', False) is True and
                        x.get('typeOfRight', None) == 'proprietà' and
                        x.get('ratio', 0.) * 100. >= self.min_shares_percentage,
                        r['shares']['sharesOwned']
                ):
                    if sho['id'] not in r_dict['organizations']:
                        r_dict['organizations'][sho['id']] = {
                            'name': sho['name'],
                            'base': {},
                            'shares_owned': [],
                            'roles': []
                        }
                    r_dict['organizations'][sho['id']]['shares_owned'].append(
                        {
                            'percentage': sho.get('ratio', 0.) * 100.,
                            'last_updated': sho.get('lastUpdate', None),
                        }
                    )

            if 'companies' in r and r['companies']['count'] > 0:
                for c in r['companies']['items']:
                    if c['id'] not in r_dict['organizations']:
                        r_dict['organizations'][c['id']] = {
                            'name': c['name'],
                            'base': {},
                            'shares_owned': [],
                            'roles': []
                        }
                    r_dict['organizations'][c['id']]['roles'].extend(
                        list(filter(
                            lambda x: x.get('official', False) is True,
                            c.get('roles', [])
                        ))
                    )

            # do not add persons with no roles or shares in known organizations
            if len(r_dict['organizations'].keys()) == 0:
                continue

            res_dict['people'][r['base']['taxId']] = r_dict

        # extract all atoka_ids from organizations elements and returns flat list
        # then apply list(set(x)) to remove duplicates, if any
        extracted_orgs_ids = list(set(list(itertools.chain.from_iterable([
            r['organizations'].keys()
            for r in res_dict['people'].values()
        ]))))

        # fetch more details for organizations connected to the person
        # tax_id and active status are added here to partial info fetched by the previous call
        # constraints on min_employees and min_revenue are added
        # organizations with unknown employees and revenue are NOT fetched
        extracted_orgs_1 = atoka_conn.get_companies_from_atoka_ids(
            extracted_orgs_ids, packages='base,shares,people,economics',
            active='true', batch_size=10,
            revenueMin=str(self.min_revenue)
        )
        atoka_companies_requests += len(extracted_orgs_1)

        extracted_orgs_2 = atoka_conn.get_companies_from_atoka_ids(
            extracted_orgs_ids, packages='base,shares,people,economics',
            active='true', batch_size=10,
            employeesMin=str(self.min_employees)
        )
        atoka_companies_requests += len(extracted_orgs_2)

        # rebuild connected_orgs_ids using only organizations fetched from the previous
        # as a UNION of the 2 criterion, simulating the OR boolean logic
        extracted_orgs = extracted_orgs_1 + extracted_orgs_2
        extracted_orgs_ids = list(set(
            x['id'] for x in extracted_orgs
        ))

        # add 'base' to companies and shares_owned
        extracted_orgs_dict = {
            i['id']: i['base'] for i in extracted_orgs if i['id'] in extracted_orgs_ids
        }

        # re-write persons['organizations'] removing the ones not in connected_orgs_dict
        # and remove persons with no organizations
        persons_id_to_remove = []
        for _id, person in res_dict['people'].items():
            for org_id, org in person.get('organizations', {}).items():
                if org_id in extracted_orgs_ids:
                    org['base'] = extracted_orgs_dict[org_id]
            # only consider organizations having a base (filtered above)
            person['organizations'] = {
                k: v for k, v in person['organizations'].items()
                if v['base'] != {}
            }

            if len(person['organizations'].keys()) == 0:
                persons_id_to_remove.append(_id)

        for pid in persons_id_to_remove:
            del res_dict['people'][pid]

        people_ids = [
            r['atoka_id'] for r in res_dict['people'].values()
        ]

        ##
        # connected people
        ##

        # fetch other people with memberships in the extracted organizations
        # filtering only people not already under scrutiny (not in people_ids)
        connected_people_by_roles = list(filter(
            lambda x: x['id'] not in people_ids,
            atoka_conn.get_items_from_ids(
                extracted_orgs_ids, 'people', ids_field_name='companies', packages='base,companies',
                companiesRolesOfficial='true', companiesRoles=atoka_conn.allowed_roles
            )
        ))

        atoka_people_requests += len(connected_people_by_roles)

        for person in connected_people_by_roles:
            pid = person.pop('id')
            if pid not in res_dict['c_people']:
                res_dict['c_people'][pid] = {
                    'base': person['base'],
                    'name': person['name'],
                    'roles': [],
                    'shares_owned': [],
                }
            res_dict['c_people'][pid]['roles'].extend([
                {
                    'organization_id': c['id'],
                    'name': r.get('name', 'n.d.'),
                    'since': r.get('since', None)
                }
                for c in person['companies']['items']
                for r in c['roles']
                if c['id'] in extracted_orgs_ids and r.get('official', False)
            ])

        # fetch other people owning the connected organizations
        # only considering shares of above the specified threshold
        connected_people_by_shares = list(filter(
            lambda x: x['id'] not in people_ids,
            atoka_conn.get_items_from_ids(
                extracted_orgs_ids, 'people', ids_field_name='sharesOwnedIds', packages='base,shares',
                sharesOwnedRatioMin=str(self.min_shares_percentage/100.0)
            )
        ))
        atoka_people_requests += len(connected_people_by_shares)

        for person in connected_people_by_shares:
            pid = person.pop('id')
            if pid not in res_dict['c_people']:
                res_dict['c_people'][pid] = {
                    'base': person['base'],
                    'name': person['name'],
                    'roles': [],
                    'shares_owned': [],
                }
            res_dict['c_people'][pid]['shares_owned'].extend([
                {
                    'organization_id': s['id'],
                    'percentage': s.get('ratio', 0.) * 100.,
                    'last_updated': s.get('lastUpdate', None),
                } for s in person['shares']['sharesOwned']
                if s['id'] in extracted_orgs_ids and 'shares' in person
            ])

        ##
        # connected organizations
        ##

        # extract atoka_ids of shareholders of extracted_organizations
        connected_shareholders_orgs_ids = list(set(list(itertools.chain.from_iterable([
            [x['id'] for x in r['shares']['shareholders'] if 'shares' in r and x['company']]
            for r in extracted_orgs if 'shares' in r and 'shareholders' in r['shares']
        ]))))

        # extract atoka_ids of sharesOwned by extracted_organizations
        connected_sharesowned_orgs_ids = list(set(list(itertools.chain.from_iterable([
            [x['id'] for x in r['shares']['sharesOwned'] if 'shares' in r and x['active']]
            for r in extracted_orgs if 'shares' in r and 'sharesOwned' in r['shares']
        ]))))

        # fetch details of shareholders of connected orgs (just to grab the tax_id)
        # filters on shares percentage
        # put it into shares_owned, as the point of view is that of the owned organization
        connected_orgs = atoka_conn.get_companies_from_atoka_ids(
            connected_shareholders_orgs_ids + connected_sharesowned_orgs_ids,
            packages='base,shares',
            active='true', batch_size=10
        )
        # TODO: filter out organizations already fetched
        atoka_companies_requests += len(connected_orgs)

        for org in connected_orgs:
            oid = org.pop('id')
            if oid not in res_dict['c_organizations']:
                res_dict['c_organizations'][oid] = {
                    'base': org['base'],
                    'name': org['name'],
                    'shares_owned': [],
                    'shareholders': [],
                }
            res_dict['c_organizations'][oid]['shares_owned'].extend([
                {
                    'organization_id': o['id'],
                    'percentage': o.get('ratio', 0.) * 100.,
                    'last_updated':o.get('lastUpdate', None),
                } for o in org.get('shares', {}).get('sharesOwned', [])
                if (
                    o['id'] in extracted_orgs_ids and
                    o.get('active', False) and
                    o.get('ratio', 0.) * 100. >= self.min_shares_percentage
                )
            ])
            res_dict['c_organizations'][oid]['shareholders'].extend([
                {
                    'organization_id': o['id'],
                    'percentage': o.get('ratio', 0.) * 100.,
                    'last_updated':o.get('lastUpdate', None),
                } for o in org.get('shares', {}).get('shareholders', [])
                if (
                    o['id'] in extracted_orgs_ids and
                    o.get('company', False) and
                    o.get('ratio', 0.) * 100. >= self.min_shares_percentage
                )
            ])
            # remove organisations with no valuable shares (owning or owned)
            if len(
                res_dict['c_organizations'][oid]['shares_owned'] + res_dict['c_organizations'][oid]['shareholders']
            ) == 0:
                del res_dict['c_organizations'][oid]

        # returns meta and results (as dict)
        return {
            'meta': {
                'atoka_requests': {
                    'people': atoka_people_requests,
                    'companies': atoka_companies_requests
                },
                'ids': {
                    'people': people_ids,
                    'organizations': extracted_orgs_ids,
                    'c_people': res_dict['c_people'].keys(),
                    'c_organizations': res_dict['c_organizations'].keys()
                }
            },
            'results': res_dict
        }


class AtokaOwnershipsExtractor(Extractor):
    """:class:`Extractor` for extractions of ownerships information out of the Atoka API

    This class is deprecated, use AtokaOrganizationsCompleteExtractor in its stead.

    Uses methods in the `AtokaConn` class, to extract information from atoka api.
    """

    def __init__(self, batch: list):
        """Create a new instance of the extractor, to extract info from a batch of companies.

        Args:
            batch: a list of tax_ids for companies lookup in Atoka

        Returns:
            instance of a :class:`AtokaOwnershipsExtractor`
        """
        self.batch = batch
        super().__init__()

    def extract(self, **kwargs) -> dict:
        """Extract meaningful information from Atoka.

        Given a list of tax_ids in self.batch, queries Atoka API, in order to retrieve owned shares and roles.

        Ownerships are returned as a list in the results.
        Each element in the list represents an **owner**, and has major identifiers, classifications and
        **owned organizations**.
        Owned organizations are embedded in the `shares_owned` list.
        Each element of `shares_owned` contains identifiers, classifications and
        **people having roles** in the organization.

        [
            {
                'atoka_id': owner_atoka_id,
                'tax_id': owner_tax_id,
                'other_atoka_ids': [ ... ],
                'name': owner_name,
                'legal_form': owner_atoka_legal_form_level2,
                'rea': owner_rea_id,
                'cciaa': owner_cciaa,
                'full_address': owner_full_address,
                'shares_owned': [
                    {
                        'name': owned_name,
                        'founded': owned_founded,
                        'atoka_id': owned_atoka_id,
                        'tax_id': owned_tax_id,
                        'rea': owned_rea_id,
                        'cciaa': owned_cciaa,
                        'full_address': owned_full_address,
                        'legal_form': owned_atoka_legal_form_level2,
                        'ateco': owned_ateco,
                        'percentage': ratio * 100,
                        'last_updated': share_last_updated,
                        'roles': [
                            {
                                'person': {
                                    'family_name': familyName,
                                    'given_name': givenName,
                                    'gender': gender,
                                    'birth_date': birthDate,
                                    'birth_location': birthPlace,
                                    'name': name,
                                    'atoka_id':id,
                                    'tax_id': taxId,
                                },
                                'label': role_name,
                                'start_date': role_since
                            },
                            ...
                        ]
                    },
                    ...
                ]
            },
            ...
        ]

        :return: a list with ownerhip and embedded details, with roles
        """

        tax_ids = self.batch
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
            logger=self.logger
        )
        atoka_companies_requests = 0
        atoka_people_requests = 0

        # fetch all companies among the list having govType values set
        # will need batch_size=1 here, because shares may contain many results
        # and the limit is 50
        # fetchin companies with all active status, since some of the non-active seems to hold
        # important ownership information on active ones
        res_tot = atoka_conn.get_companies_from_tax_ids(
            tax_ids, packages='base,shares', active='*', batch_size=1
        )
        res_tot = [r for r in res_tot if not r.get('obfuscated', False)]
        atoka_companies_requests += len(res_tot)

        self.logger.info(
            "- da {0} tax_ids, ricevuti da Atoka dettagli e quote per {1} organizzazioni "
            "(ci possono essere doppioni)".format(
                len(tax_ids), len(res_tot)
            )
        )

        # return multiple results for single tax_ids, if any
        res_doubles = {}
        for r in res_tot:
            if r['base']['taxId'] not in res_doubles:
                res_doubles[r['base']['taxId']] = []
            res_doubles[r['base']['taxId']].append(r['id'])
        res_doubles = {
            k: v
            for k, v in res_doubles.items() if len(v) > 1
        }

        # remove owners with no shares, or shares to non-active companies
        def check_owner_has_shares_in_active_companies(owner_org):
            if 'shares' in owner_org and 'sharesOwned' in owner_org['shares']:
                properties_to_active = list(filter(
                    lambda x: x['active'] is True and x['typeOfRight'] == 'proprietà' and 'ratio' in x,
                    owner_org['shares']['sharesOwned']
                ))
                if len(properties_to_active):
                    return True
            return False

        res_tot = list(
            sorted(
                filter(check_owner_has_shares_in_active_companies, res_tot),
                key=lambda x: x['base']['taxId']
            )
        )
        self.logger.debug(
            "- {0} istituzioni hanno partecipazioni in aziende attive".format(
                len(res_tot)
            )
        )

        # transform the results into a dict,
        # merging results from multiple records corresponding to the same tax_id
        res_dict = {}

        for r in res_tot:
            if r['base']['taxId'] not in res_dict:
                r_dict = {
                    'atoka_id': r['id'],
                    'other_atoka_ids': [
                        atoka_id for atoka_id in res_doubles[r['base']['taxId']] if atoka_id != r['id']
                    ] if r['base']['taxId'] in res_doubles else [],
                    'name': r['name'],
                    'legal_form': [x['name'] for x in r['base']['legalForms'] if x['level'] == 2][0],
                    'rea': r['base'].get('rea', None),
                    'cciaa': r['base'].get('cciaa', None),
                    'full_address': r.get('fullAddress', None),
                    'shares_owned': [],
                }
            else:
                r_dict = res_dict[r['base']['taxId']]

            r_dict['shares_owned'].extend(
                {
                    'name': sho['name'],
                    'last_updated': sho['lastUpdate'],
                    'atoka_id': sho['id'],
                    'percentage': sho.get('ratio', 0.) * 100.
                }
                for sho in filter(
                    lambda x:
                        x.get('active', False) is True and
                        x.get('typeOfRight', None) == 'proprietà',
                    r['shares'].get('sharesOwned', [])
                )
                if 'shares' in r
            )

            res_dict[r['base']['taxId']] = r_dict

        # extract all atoka_ids from shares_owned elements and returns flat list
        # then apply list(set(x)) to remove duplicates, if any
        owned_atoka_ids = list(set(list(itertools.chain.from_iterable([
            [x['atoka_id'] for x in r['shares_owned']]
            for r in res_dict.values()
        ]))))

        owned_orgs = atoka_conn.get_companies_from_atoka_ids(
            owned_atoka_ids, packages='base,shares', active='true', batch_size=10
        )

        # build the list of all atoka_ids of owners at level shares_level
        # used while building list of shareholders, and exclude main owner
        atoka_ids = [
            r['atoka_id'] for r in res_dict.values()
        ] + list(
            itertools.chain.from_iterable(
                [r['other_atoka_ids'] for r in res_dict.values()]
            )
        )

        owned_orgs_dict = {}
        for r in owned_orgs:
            shareholders = [
                {
                    'name': sho['name'],
                    'last_updated': sho['lastUpdate'],
                    'atoka_id': sho['id'],
                    'percentage': sho.get('ratio', 0.) * 100.
                }
                for sho in filter(
                    lambda x:
                        x.get('active', False) is True and
                        x.get('typeOfRight', None) == 'proprietà',
                    r['shares'].get('shareholders', [])
                )
                if 'shares' in r and sho['id'] not in atoka_ids
            ]

            owned_orgs_dict[r['id']] = {
                'name': r['name'],
                'cciaa': r['base'].get('cciaa', None),
                'rea': r['base'].get('rea', None),
                'tax_id': r['base'].get('taxId', None),
                'vat': r['base'].get('vat', None),
                'founded': r['base'].get('founded', None),
                'full_address': r.get('fullAddress', None),
                'legal_form': [x['name'] for x in r['base']['legalForms'] if x['level'] == 2][0],
                'ateco': r['base'].get('ateco', []),
                'shareholders': shareholders
            }

        # extract all atoka_ids from shareholders elements and returns flat list
        # then apply list(set(x)) to remove duplicates, if any
        shareholders_atoka_ids = list(set(list(itertools.chain.from_iterable([
            [x['atoka_id'] for x in r['shareholders']]
            for r in owned_orgs_dict.values()
        ]))))

        shareholders_orgs = atoka_conn.get_companies_from_atoka_ids(
            shareholders_atoka_ids, packages='base', active='true', batch_size=10
        )

        shareholders_orgs_dict = {
            r['id']: {
                'name': r['name'],
                'cciaa': r['base'].get('cciaa', None),
                'rea': r['base'].get('rea', None),
                'tax_id': r['base'].get('taxId', None),
                'vat': r['base'].get('vat', None),
                'founded': r['base'].get('founded', None),
                'full_address': r.get('fullAddress', None),
                'legal_form': [x['name'] for x in r['base']['legalForms'] if x['level'] == 2][0],
                'ateco': r['base'].get('ateco', [])
            } for r in shareholders_orgs
        }

        atoka_companies_requests += len(owned_orgs) + len(shareholders_orgs)
        self.logger.info("- ricevuti dettagli per {0} partecipate".format(len(owned_orgs) + len(shareholders_orgs)))

        # extract all people's atoka_ids from res_owned elements and returns flat list, removing duplicates
        people = atoka_conn.get_roles_from_atoka_ids(
            owned_atoka_ids, packages='base,companies',
            companiesRolesOfficial='true', companiesRoles=atoka_conn.allowed_roles
        )
        atoka_people_requests += len(people)

        def extract_birth_place(base: dict) -> Type[Union[str, None]]:
            """

            :param base:
            :return:
            """
            if 'birthPlace' not in base:
                return None
            if 'municipality' in base['birthPlace']:
                return "{0} ({1})".format(
                  base['birthPlace']['municipality'],
                  base['birthPlace']['provinceCode']
                )
            else:
                return "{0} ({1})".format(base['birthPlace']['state'], base['birthPlace']['stateCode'])

        people_ids = []
        people_dict = {}
        for person in people:
            if (
                'base' in person and
                'gender' in person['base'] and
                'familyName' in person['base'] and
                'givenName' in person['base'] and
                person['id'] not in people_ids
            ):
                people_ids.append(person['id'])
                people_dict[person['id']] = {
                    'given_name': person['base']['givenName'],
                    'family_name': person['base']['familyName'],
                    'gender': person['base']['gender'],
                    'birth_date': person['base'].get('birthDate', None),
                    'birth_location': extract_birth_place(person['base']),
                    'tax_id': person['base'].get('taxId', None),
                    'companies': [
                        {
                            'id': x['id'],
                            'roles': [
                                xr for xr in x['roles']
                                if 'official' in xr and xr['official'] is True and 'name' in xr
                            ]
                        }
                        for x in person['companies']['items'] if x['id'] in owned_atoka_ids
                    ]
                }
        self.logger.info(
            "- ricevuti dettagli per {0} persone ({1} distinte)".format(
                len(people), len(people_ids)
            )
        )

        # upgrade owned_orgs_dict with roles, from people_dict
        for atoka_id, person in people_dict.items():
            person['atoka_id'] = atoka_id
            for company in person.pop('companies', []):
                org = owned_orgs_dict.get(company['id'])
                if org:
                    if 'roles' not in org:
                        org['roles'] = []
                    org['roles'].extend([
                        {
                            'person': person,
                            'label': org_role['name'],
                            'start_date': org_role.get('since', None)
                        }
                        for org_role in company['roles']
                    ])
                else:
                    self.logger.warning(
                        "! azienda {0} richiesta ad atoka nel calcolo dei ruoli per {1}, "
                        "ma non presente nei risultati".format(company['id'], person['atoka_id'])
                    )

        # upgrade res_dict values with details values
        for tax_id, org in res_dict.items():
            for owned in org['shares_owned']:
                owned_details = owned_orgs_dict.get(owned['atoka_id'], None)
                if owned_details:
                    for f in [
                        'name', 'cciaa', 'rea', 'ateco', 'tax_id', 'vat',
                        'founded', 'legal_form', 'full_address', 'roles', 'shareholders'
                    ]:
                        if f in owned_details:
                            owned[f] = owned_details[f]

                    if 'shareholders' in owned_details:
                        for shareholder in owned_details['shareholders']:
                            shareholder_details = shareholders_orgs_dict.get(shareholder['atoka_id'], None)
                            if shareholder_details:
                                for f in [
                                    'name', 'cciaa', 'rea', 'ateco', 'tax_id', 'vat',
                                    'founded', 'legal_form', 'full_address'
                                ]:
                                    if f in shareholder_details:
                                        shareholder[f] = shareholder_details[f]

                else:
                    self.logger.warning(
                        "! organizzazione {0} richiesta ad atoka, "
                        "ma non presente nei risultati".format(owned['atoka_id'])
                    )

        # returns a list
        results = []
        for tax_id, result in res_dict.items():
            result['tax_id'] = tax_id
            results.append(result)

        return {
            'meta': {
                'atoka_requests': {
                    'people': atoka_people_requests,
                    'companies': atoka_companies_requests
                },
                'ids': {
                    'people': people_ids,
                    'companies': owned_atoka_ids
                }
            },
            'results': results
        }


class AtokaEconomicsExtractor(Extractor):
    """:class:`Extractor` for extractions of economics information out of the Atoka API

    Uses methods in the `AtokaConn` class, to extract information from atoka api.
    """

    def __init__(self, batch: list):
        """Create a new instance of the extractor, to extract info from a batch of companies.

        Args:
            batch: a list of tax_ids for companies lookup in Atoka

        Returns:
            instance of a :class:`AtokaEconomicsExtractor`
        """
        self.batch = batch
        super().__init__()

    def extract(self, **kwargs) -> dict:
        """Extract meaningful information from Atoka.

        Given a list of tax_ids in self.batch, queries Atoka API, in order to retrieve ecomomics details.

        Results are returned as a dict.

        {
            'meta': {
                'atoka_requests': {
                    'companies': atoka_companies_requests
                },
            },
            'results': results
        }

        Each element in the `result` list contains all organization economics details,
        and the name and all major identifiers, are shown for clarity.
        The sections found in atoka's `economics` package are extracted

        [
            {
                'id': atoka_id,
                'tax_id': tax_id,
                'other_atoka_ids': [],
                'name': name,
                'economics': {
                    'public': false,
                    'capitalStock': { 'value': 10000 },
                    'balanceSheets': [
                        {
                            'year': 2018,
                            'latest': True,
                            'revenue': 1000000,
                            'revenuteTrend': 0.24,
                            ...
                        },
                        ...
                    ],
                    'employees': [
                        {
                            'year': 2018,
                            'latest': True,
                            'value': 81
                        },
                        ...
                    ]
                }
            },
            ...
        ]

        :return: a list of organization id and names, with economics detail
        """

        ids = self.batch
        ids_field_name = kwargs.get('ids_field_name', 'ids')
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
            logger=self.logger
        )

        atoka_companies_requests = 0

        # fetch all companies among the list having govType values set
        # will need batch_size=1 here, because shares may contain many results
        # and the limit is 50
        try:
            res_tot = atoka_conn.get_items_from_ids(
                ids, 'companies', ids_field_name=ids_field_name, packages='base,economics', active='true', batch_size=1
            )
            res_tot = [r for r in res_tot if not r.get('obfuscated', False)]

            atoka_companies_requests += len(res_tot)

            self.logger.info(
                f"- da {len(ids)} {ids_field_name}, ricevuti da Atoka dettagli per "
                f"{len(res_tot)} istituzioni (ci possono essere doppioni)"
            )
        except Exception as e:
            self.logger.warning(f"During extraction: {e}")
            res_tot = []

        results = []
        for r in res_tot:
            tax_id = r['base'].get('taxId', None)
            if tax_id is None:
                self.logger.warning("Could not find taxId for result with id: {0}. Skipping.".format(r['id']))
                continue
            r.pop('base')
            r_dict = r
            r_dict['tax_id'] = tax_id
            results.append(r_dict)

        return {
            'meta': {
                'atoka_requests': {
                    'companies': atoka_companies_requests
                },
            },
            'results': results
        }
