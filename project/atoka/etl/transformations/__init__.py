import typing
from operator import itemgetter
from typing import Union

from ooetl.transformations import Transformation


def get_cf(d: dict) -> Union[str, None]:
    """Returns the CF string from a dictionary structured like this:
    {
        'identifiers': [
            { 'scheme': 'CF', 'identifier': '123456' },
            { 'scheme': 'ATOKA_ID', 'identifier': '654321' },
            ...
        ]
    }

    Returns None if it can't find the CF

    :param d: a dictionary with `identifiers` list of dict
    :return: the CF string or None if not found
    """
    if d is None:
        return None

    try:
        return next(filter(lambda x: x['scheme'] == 'CF', d.get('identifiers', [])))['identifier']
    except StopIteration:
        return None


class AtokaOrganizationsTransformation(Transformation):
    """Base class for transformations that transform information extracted from Atoka,
    into a data structure useable in PopoloOrgLoader.
    """

    @staticmethod
    def filter_rows(od):
        # return filter(lambda x:x['family_name'] == 'Forteleoni', od)
        return od

    @staticmethod
    def atoka2opdm_legal_form(atoka_legal_form: str) -> int:
        """Mapping between atoka legal forms and opdm FORMA_GIURIDICA_OP classification

        :param atoka_legal_form:
        :return:
        """
        if atoka_legal_form:
            return {
                "altre forme": 1221,
                "associazione": 403,
                "associazione impresa": 1194,
                "associazione in partecipazione": 403,
                "azienda autonoma statale": 144,
                "azienda municipale": 144,
                "azienda provinciale": 144,
                "azienda regionale": 144,
                "azienda speciale": 144,
                "azienda speciale di cui al dlgs 267/2000": 144,
                "azienda speciale di ente locale": 144,
                "comunione ereditaria": 1190,
                "consorzio": 321,
                "consorzio con attività esterna": 321,
                "consorzio di cui al dlgs 267/2000": 321,
                "consorzio fidi": 321,
                "consorzio intercomunale": 150,
                "consorzio municipale": 150,
                "consorzio senza attività esterna": 321,
                "contratto di rete dotato di soggettività giuridica": 1221,
                "cooperativa sociale": 1192,
                "ente": 1221,
                "ente diritto pubblico": 422,
                "ente ecclesiastico": 1197,
                "ente ecclesiastico civilmente riconosciuto": 1197,
                "ente impresa": 621,
                "ente morale": 621,
                "ente pubblico commerciale": 87,
                "ente pubblico economico": 422,
                "ente sociale": 87,
                "fondazione": 83,
                "fondazione impresa": 83,
                "gruppo europeo di interesse economico": 1195,
                "impresa familiare": 1183,
                "impresa individuale": 1183,
                "istituto religioso": 1197,
                "mutua assicurazione": 1193,
                "non precisata": 1221,
                "piccola società cooperativa": 941,
                "piccola società cooperativa a responsabilità limitata": 941,
                "società a responsabilità limitata": 24,
                "società a responsabilità limitata a capitale ridotto": 24,
                "società a responsabilità limitata con unico socio": 295,
                "società a responsabilità limitata semplificata": 24,
                "società consortile": 321,
                "società consortile a responsabilità limitata": 321,
                "società consortile cooperativa a responsabilità limitata": 321,
                "società consortile in accomandita semplice": 321,
                "società consortile in nome collettivo": 321,
                "società consortile per azioni": 321,
                "società cooperativa": 941,
                "società cooperativa a responsabilita illimitata": 941,
                "società cooperativa a responsabilita limitata": 941,
                "società cooperativa consortile": 321,
                "società cooperativa europea": 1195,
                "società costituita in base a leggi di altro stato": 1202,
                "società di fatto": 1190,
                "società di mutuo soccorso": 1198,
                "società europea": 1195,
                "società in accomandita per azioni": 1191,
                "società in accomandita semplice": 1188,
                "società in nome collettivo": 1187,
                "società irregolare": 1190,
                "società per azioni": 11,
                "società per azioni con socio unico": 11,
                "società semplice": 1186,
                "società tra professionisti": 1189,
                "soggetto non iscritto al registro imprese": 1221,
            }.get(atoka_legal_form.lower(), None)

    @staticmethod
    def get_index(d: dict) -> Union[tuple, None]:
        """Return unique hashable index for organization

        :param d: dict containing organization
        :return: tuple
        """
        cf = get_cf(d)
        if cf is None and d is not None:
            # deprecated
            cf = d.get("identifier", None)
        return cf

    def transform(self, **kwargs):
        pass


class AtokaOwnershipsTransformation(Transformation):
    """Base class for transformations that transform ownerships extracted from Atoka,
    into a data structure useable in PopoloOwnershipOrgLoader.
    """

    @staticmethod
    def filter_rows(od):
        # return filter(lambda x:x['family_name'] == 'Forteleoni', od)
        return od

    @staticmethod
    def get_index(d: dict) -> tuple:
        """Return unique hashable index for ownership dict

        :param d: dict containing ownership details
        :return: tuple
        """
        return get_cf(d["owning_org"]), get_cf(d["owned_org"])

    def transform(self, **kwargs):
        pass


class AtokaMembershipsTransformation(Transformation):
    """Base class for transformations that transform roles extracted from Atoka,
    into a data structure useable in PopoloPersonWithMembershipsLoader.
    """

    @staticmethod
    def filter_rows(od):
        # return filter(lambda x:x['family_name'] == 'Forteleoni', od)
        return od

    atoka_roles_skip_list = [
            "board member",
            "board members",
            "delegato",
            "direttore o manager",
            "membro comitato direttivo",
            "membro comitato esecutivo",
            "membro consiglio direttivo",
            "membro del comitato di controllo sulla gestione",
            "office manager",
            "vice direttore generale",
            "administration",
            "presidenza",
            "n.d.",
            "fondatore",
            "responsabile direzione",
            "preposto al commercio ingrosso settore alimentare",
            "coordinatore",
            "membro comitato di sorveglianza",
            "direzione vendite",
            "",
            " ",
            "-",
            "- project manager animal area",
            "- project manager human area",
            "- research and development",
            "- technical manager",
            "– amministrazione comunale di conversano",
            "– amministrazione comunale di noicattaro",
            "– amministrazione comunale di rutigliano",
            "(chief commericial officer, radar processing engineer)",
            "(chief technical officer, microwave engineer)",
            "accounting",
            "affari generali",
            "agente di assicurazioni",
            "amministrazione",
            "assegnista",
            "assessore",
            "associate professor",
            "auditor",
            "co-founder",
            "commissario giudiziale",
            "commissario giudiziario",
            "commissario straordinario",
            "comunicazione",
            "consigliere di gestione",
            "consigliere di sorveglianza",
            "consiglio direttivo",
            "controllo",
            "councilors",
            "counselor",
            "cto",
            "delegato al ritiro capitale versato",
            "delegato di cui art. 2 legge 25/8/91 n.287",
            "developer",
            "dipendente",
            "director",
            "direttore",
            "direttore amministrativo",
            "direttore artistico",
            "direttore commerciale",
            "direttore di esercizio",
            "direttore incaricato",
            "direttore responsabile",
            "direttore sanitario",
            "direttore scientifico",
            "direttore tecnico",
            "direzione",
            "direzione controllo qualità",
            "dirigente",
            "docente a contratto",
            "dottore",
            "dottore in economia aziendale - management and consulting",
            "esperta maestra del gusto e",
            "fabbro",
            "formazione",
            "full professor",
            "guida",
            "imprenditore",
            "imprenditrice",
            "ingegnere",
            "ingegnere chimico",
            "ingegnere libero professionista",
            "it director",
            "legale",
            "management",
            "marketing, comunicazione e promozione commerciale",
            "medico radiologo",
            "preposto",
            "preposto agenti rappresentanti di commercio",
            "preposto alla gestione tecnica ai sensi d.m. 37/2008",
            "preposto alla gestione tecnica ai sensi del d.m. 274/97",
            "presidente comitato direttivo",
            "presidente comitato esecutivo",
            "presidente consiglio direttivo",
            "presidente del comitato di controllo sulla gestione",
            "presidente del consiglio di gestione",
            "presidente provinciale",
            "procuratore",
            "procuratore ad negotia",
            "procuratore generale",
            "procuratore speciale",
            "professor",
            "professore associato",
            "professore ordinario",
            "professore ordinario, presidente",
            "professore, amministratore delegato",
            "progettazione",
            "progettazione, tecnica",
            "psicologa - psicoterapeuta",
            "r&d manager, coo",
            "rappresentante",
            "rappresentante comune obbligazionisti",
            "rappresentante comune patrimoni/finanziamenti",
            "research scientist",
            "responsabile",
            "responsabile abbanoa spa",
            "responsabile aciclubprato srl",
            "responsabile actv spa",
            "responsabile aet2000 ooo1",
            "responsabile affari generali e legali - ufficio protocollo",
            "responsabile agenzia",
            "responsabile agenzia area nolana - centrale unica di committenza",
            "responsabile agid_00",
            "responsabile ama direzione generale",
            "responsabile amministrativo",
            "responsabile amministrazione",
            "responsabile amministrazione - fatturazione elettronica",
            "responsabile amministrazione e direzione",
            "responsabile aoo agid",
            "responsabile aoo ato me 3",
            "responsabile aoo_aspo",
            "responsabile area amministrazione",
            "responsabile area staff e ad",
            "responsabile area tecnica gestionale",
            "responsabile asmenet scarl",
            "responsabile asp_serviziallapersona",
            "responsabile aspm servizi ambientali",
            "responsabile avm spa",
            "responsabile capannori servizi srl - sede centrale",
            "responsabile capriservizi srl unipersonale",
            "responsabile casirate gas 2 srl",
            "responsabile castiglione 2014 azienda speciale",
            "responsabile covigas srl",
            "responsabile del settore",
            "responsabile dell ufficio",
            "responsabile della protezione del dati personali (dpo)",
            "responsabile della qualità",
            "responsabile direzione amministrativa",
            "responsabile eco center spa",
            "responsabile ecologia e ambiente spa in liquidazion",
            "responsabile editoriale",
            "responsabile emiliambiente spa (area unica)",
            "responsabile farmacia comunale",
            "responsabile farmacie comunali pomezia s.p.a.",
            "responsabile gal valli gesso vermenagna pesio scarl",
            "responsabile galpiceno",
            "responsabile gestione verde pubblico",
            "responsabile gestori di pubblici servizi",
            "responsabile impianti",
            "responsabile l'ora srl-fattura elettronica",
            "responsabile laziocrea pagamenti farmacie ssr",
            "responsabile laziocrea pagamenti fornitori ssr",
            "responsabile montecatini parcheggi e servizi",
            "responsabile paolo ricci servizi",
            "responsabile parking nola",
            "responsabile patrimonio del trentino spa",
            "responsabile piccole manutenzioni",
            "responsabile presidenza",
            "responsabile protocollo",
            "responsabile protocollo amnu s.p.a.",
            "responsabile protocollo pec",
            "responsabile registro protocollo sede",
            "responsabile s.a.p.na. s.p.a.",
            "responsabile scientifico",
            "responsabile sede amministrativa",
            "responsabile sede gal terre del sesia",
            "responsabile sede legale",
            "responsabile sede operativa",
            "responsabile segreteria",
            "responsabile senza categoria",
            "responsabile servizi cimiteriali",
            "responsabile servizio protocollo",
            "responsabile servizio trasporto scolastico e noleggio con conducente",
            "responsabile societa' acqua procida s.a.p. srl",
            "responsabile tecnico",
            "responsabile terre del sesia",
            "responsabile trattamento",
            "responsabile uff_fatturazione",
            "responsabile uffcio ztl",
            "responsabile ufficio amministrativo",
            "responsabile ufficio amministrativo esu srl",
            "responsabile ufficio fatturazione elettronica pa",
            "responsabile ufficio liquidazione",
            "responsabile ufficio per la fatturazione elettronica",
            "responsabile ufficio per la transizione al digitale",
            "responsabile ufficio protocollo-01",
            "responsabile valbe servizi s.p.a. - impianto depurazione mariano comense",
            "responsabile volsca ambiente e servizi spa",
            "responsabile, amministrativo, cfo",
            "revisore legale",
            "ricerca",
            "ricerca e sviluppo",
            "ricercatore universitario",
            "ricercatrice",
            "servizi generali",
            "sindaco",
            "sindaco protempore",
            "specialist",
            "staff",
            "studio legale",
            "tech",
            "tecnico",
            "tecnico dell ufficio",
            "tecnico e commerciale",
            "tecnico scientifico",
            "ufficio legale",
            "vice direttore e responsabile servizi manutenzione & logistica",
            "direttore",
            "direttore incaricato",
        ]

    @staticmethod
    def atoka_role_2_opdm_roletype(atoka_role: str) -> str:
        """Mapping between atoka roles in organizations and opdm RoleType names"""
        if atoka_role:
            return {
                "titolare": "Titolare",
                "titolare firmatario": "Titolare",
                "amministratore unico": "Amministratore unico",
                "consigliere": "Consigliere",
                "socio amministratore": "Amministratore",
                "socio accomandante": "Socio accomandante",
                "socio": "Socio",
                "socio accomandatario": "Socio accomandatario",
                "presidente consiglio amministrazione": "Presidente del consiglio di amministrazione",
                "socio unico": "Socio",
                "amministratore": "Amministratore",
                "sindaco effettivo": "Sindaco effettivo",
                "sindaco effettivo:": "Sindaco effettivo",
                "vice presidente consiglio amministrazione": "Vice Presidente del consiglio di amministrazione",
                "amministratore delegato": "Amministratore delegato",
                "liquidatore": "Liquidatore",
                "sindaco supplente": "Sindaco supplente",
                "socio di societa' in nome collettivo": "Socio",
                "consigliere delegato": "Consigliere",
                "presidente": "Presidente",
                "curatore fallimentare": "Curatore fallimentare",
                "presidente del collegio sindacale": "Presidente del collegio sindacale",
                "vice presidente": "Vice Presidente",
                "legale rappresentante": "Legale rappresentante",
                "revisore dei conti": "Revisore dei conti",
                "legale rappresentante di societa'": "Legale rappresentante",
                "institore": "Institore",
                "direttore generale": "Direttore generale",
                "amm.": "Amministratore",
                "amministratore giudiziario": "Amministratore",
                "amministratore provvisorio": "Amministratore",
                "amministratore straordinario": "Amministratore",
                "leader": "Amministratore",
                "– vice presidente, amministratore delegato": "Amministratore delegato",
                "amministratore delegato e presidente cda": "Amministratore delegato",
                "amministratore delegato, amministrazione": "Amministratore delegato",
                "ceo": "Amministratore delegato",
                "chief excecutive officer": "Amministratore delegato",
                "chief executive officer": "Amministratore delegato",
                "consigliere amministratore delegato": "Amministratore delegato",
                "presidenete e amministratore delegato": "Amministratore delegato",
                "presidente - amministratore delegato": "Amministratore delegato",
                "presidente amministratore delegato": "Amministratore delegato",
                "presidente e ad": "Amministratore delegato",
                "presidente e amministratore delegato": "Amministratore delegato",
                "amministratore unico /  direttore generale": "Amministratore unico",
                "amministratore unico pro-tempore": "Amministratore unico",
                "amministratore unico, direttore generale, responsabile della prevenzione": "Amministratore unico",
                "amministratore unico, dottore commercialista": "Amministratore unico",
                "amministratore unico, legale rappresentante": "Amministratore unico",
                "rappresentante legale      amministratore unico": "Amministratore unico",
                "rappresentante legale - amministratore unico": "Amministratore unico",
                "–consigliere": "Consigliere",
                ", consigliere –": "Consigliere",
                "consigliere di amministrazione": "Consigliere",
                "consigliere cda": "Consigliere",
                "consiglieri": "Consigliere",
                "consiglieri consiglio d'amministrazione": "Consigliere",
                "consiglio di amministrazione": "Consigliere",
                "direttore finanziario, consigliere": "Consigliere",
                "general manager": "Direttore generale",
                "direttore generale tecnoborsa scpa": "Direttore generale",
                "direttore operations": "Direttore generale",
                "direzione generale": "Direttore generale",
                "direzione operativa": "Direttore generale",
                "responsabile direzione generale": "Direttore generale",
                "preposto della sede secondaria": "Institore",
                "legale rappresentante - consigliere anziano": "Legale rappresentante",
                "legale rappresentante e liquidatore": "Legale rappresentante",
                "legale rappresentante, management": "Legale rappresentante",
                "rappresentante legale": "Legale rappresentante",
                "commissario liquidatore": "Liquidatore",
                "liquidatore - legale rappresentante": "Liquidatore",
                "liquidatore giudiziario": "Liquidatore",
                "liquidatore unico": "Liquidatore",
                "presidente collegio di liquidazione": "Liquidatore",
                "presidente del collegio dei liquidatori": "Liquidatore",
                "rappresentante legale liquidatore": "Liquidatore",
                "- president": "Presidente",
                "– presidente": "Presidente",
                "– soggetti privati (rutigliano) - presidente": "Presidente",
                "amm., pres.": "Presidente",
                "chairman": "Presidente",
                "chairperson": "Presidente",
                "consigliere delegato, presidente": "Presidente",
                "imprenditore, presidente": "Presidente",
                "ingegnere chimico, ricercatore chimico, president": "Presidente",
                "presidente:": "Presidente",
                "president": "Presidente",
                "presidente di 1000 miglia s.r.l.": "Presidente",
                "presidente e": "Presidente",
                "presidente e legale rappresentante": "Presidente",
                "presidente pro tempore": "Presidente",
                "presidente, assicuratrice": "Presidente",
                "presidente, legale": "Presidente",
                "presidente, vice presidente": "Presidente",
                "presidente, vicepresidente": "Presidente",
                "responsabile, presidente": "Presidente",
                "sindaco, presidente": "Presidente",
                "chairman of the board of auditors": "Presidente del collegio sindacale",
                "presidente collegio sindacale": "Presidente del collegio sindacale",
                ", presidente del cda –": "Presidente del consiglio di amministrazione",
                "presidente c.d.a.": "Presidente del consiglio di amministrazione",
                "presidente c.d.a. e legale rappresentante": "Presidente del consiglio di amministrazione",
                "presidente c.d.a. legale rappresentante": "Presidente del consiglio di amministrazione",
                "presidente cda": "Presidente del consiglio di amministrazione",
                "presidente cda – referente topografia/fotogrammetria":
                    "Presidente del consiglio di amministrazione",
                "presidente cda e rappresentante legale": "Presidente del consiglio di amministrazione",
                "presidente consiglio di amm.ne - legale rappresent": "Presidente del consiglio di amministrazione",
                "presidente consiglio di amministrazione": "Presidente del consiglio di amministrazione",
                "presidente consiglio di amministrazione, vicepresidente":
                    "Presidente del consiglio di amministrazione",
                "presidente del c.d.a.": "Presidente del consiglio di amministrazione",
                "presidente del cda": "Presidente del consiglio di amministrazione",
                "presidente del consiglio di amministrazi": "Presidente del consiglio di amministrazione",
                "presidente del consiglio di amministrazione": "Presidente del consiglio di amministrazione",
                "permanent auditor": "Revisore dei conti",
                "presidente collegio dei revisori": "Revisore dei conti",
                "presidente dei revisori dei conti": "Revisore dei conti",
                "regular auditor": "Revisore dei conti",
                "revisore contabile": "Revisore dei conti",
                "revisore unico": "Revisore dei conti",
                "revisore unico società per azioni": "Revisore dei conti",
                "sidaco effettivo": "Sindaco effettivo",
                "sindaco effettivo: ": "Sindaco effettivo",
                "standing auditor": "Sindaco effettivo",
                "alternate auditor": "Sindaco supplente",
                "sindaco suppl.": "Sindaco supplente",
                "supplementary auditor": "Sindaco supplente",
                "socio fondatore": "Socio",
                "titolare, responsabile della trasparenza": "Titolare",
                "– vice presidente": "Vice Presidente",
                ", vice presidente vicario –": "Vice Presidente",
                "(vice-chair and chief operative officer, radar system engineer)": "Vice Presidente",
                "executive vp": "Vice Presidente",
                "vice president": "Vice Presidente",
                "vice presidente vicario": "Vice Presidente",
                "vice presidente, consigliere": "Vice Presidente",
                "vice presidente, vicepresidente": "Vice Presidente",
                "vicepresidente": "Vice Presidente",
                "vice presidente cda": "Vice Presidente del consiglio di amministrazione",
            }.get(atoka_role.lower(), None)

    def transform(self, **kwargs):
        pass


class AtokaOrganizationEconomicsTransformation(Transformation):
    """Transform economics information extracted from Atoka, into a data structure useable in a PopoloLoader.

    Used to create or upgrade OrganizationEconomic details found in atoka.

    Original information from ATOKA
    ------------------------------------------

    {
        'id': atoka_id,
        'tax_id': tax_id,
        'name': name,
        'economics': {
            'public': false,
            'capitalStock': { 'value': 10000 },
            'balanceSheets': [
                {
                    'year': 2018,
                    'latest': True,
                    'revenue': 1000000,
                    'revenuteTrend': 0.24,
                    ...
                },
                ...
            ],
            'employees': [
                {
                    'year': 2018,
                    'latest': True,
                    'value': 81
                },
                ...
            ]
        }
    },

    Information as needed by loader
    ------------------------------------

    {
        "atoka_id": "7058f762c20c",
        "tax_id": "00040450074",
        "name": "AUTOPORTO VALLE D'AOSTA - S.P.A.",
        "is_public": false,
        "revenue": 2545000,
        "revenue_trend": 0.0063,
        "capital_stock": 31270000,
        "assets": null,
        "costs": null,
        "ebitda": null,
        "mol": null,
        "net_financial_position": null,
        "production": null,
        "profit": null,
        "purchases": null,
        "raw_materials_variation": null,
        "services_and_tp_goods_charges": null,
        "staff_costs": null,
        "employees": 13
        "historical_values": [
          {
            "year": 2018,
            "employees": 13,
          },
          {
            "year": 2017,
            "capital_stock": 31270000,
            "revenue": 2545000,
            "revenue_trend": 0.0063,
            "employees": 15,
          },
          {
            "year": 2016,
            "capital_stock": 31270000,
            "revenue": 2529000,
            "revenue_trend": -0.016,
            "employees": 15,
          },
          {
            "year": 2015,
            "capital_stock": 31270000,
            "revenue": 2570000,
            "revenue_trend": -0.0019,
          }
        ],
    }
    """

    @staticmethod
    def filter_rows(od):
        # filter out results with no economics section
        return filter(lambda x: 'economics' in x, od)

    def transform(self):
        """ Transform a list of dicts extracted from the ATOKA API,

        :return: the ETL instance (to chain methods)
        """
        self.logger.debug("start of transform")
        self.logger.debug("  get a copy of the original dataframe")
        od = self.etl.original_data

        # apply subclass-specific filters (metro/provinces separation)
        od = list(self.filter_rows(od))
        self.logger.debug(
            "- {0} organizzzioni hanno dettagli economici".format(
                len(list(od))
            )
        )

        def normalize_field_names(_item: dict) -> dict:
            """Normalize field names from camelCase to _"""
            norm = {
                'revenueTrend': 'revenue_trend',
                'capitalStock': 'capital_stock',
                'netFinancialPosition': 'net_financial_position',
                'rawMaterialsVariation': 'raw_materials_variation',
                'servicesAndTPGoodsCharges': 'services_and_tp_goods_charges',
                'staffCosts': 'staff_costs'
            }

            return {norm.get(k, k): v for k, v in _item.items()}

        def fetch_one_per_year(items: list, key_field: str = 'date', max_items: int = 3) -> list:
            """Fetch only one value per year, after sorting by dates"""
            try:
                sorted_items = sorted(items, key=itemgetter(key_field), reverse=True)
            except KeyError:
                return [items[0]]

            fetched_items = []
            for _item in sorted_items:
                if _item['year'] not in [i['year'] for i in fetched_items]:
                    fetched_items.append(normalize_field_names(_item))
            return fetched_items[:min(len(fetched_items), max_items)]

        def fetch_latest(items: list) -> typing.Union[dict, None]:
            """Fetch first items containing 'latest', should be one

            :param items:
            :return:
            """
            try:
                return normalize_field_names(next(filter(lambda x: x['latest'] is True, items)))
            except StopIteration:
                if len(items) == 1:
                    return normalize_field_names(items[0])
                else:
                    return {}

        def org_from_item(i: dict) -> dict:
            r_dict = {'atoka_id': i['id']}
            for k in ['tax_id', 'name']:
                r_dict[k] = i[k]

            r_dict['is_public'] = i['economics'].get('public', False)
            historical_values = []
            if 'balanceSheets' in i['economics']:
                # copy historical values
                balance_sheets = fetch_one_per_year(i['economics']['balanceSheets'], max_items=4)
                for sheet in balance_sheets:
                    historical_values.append({
                        k: v for k, v in sheet.items() if k not in ['latest', 'revenueTrendLabel']
                    })

                # fetch latest values in balanceSheets and move under r_dict (as latest)
                for norm_field in [
                    'revenue', 'revenue_trend', 'capital_stock',
                    'assets', 'costs', 'ebitda', 'mol', 'net_financial_position',
                    'production', 'profit', 'purchases', 'raw_materials_variation',
                    'services_and_tp_goods_charges', 'staff_costs'
                ]:
                    r_dict['{0}'.format(norm_field)] = fetch_latest(balance_sheets).get(norm_field, None)

                # correct capital_stock, if found under economics
                if i['economics'].get('capitalStock', None):
                    r_dict['capital_stock'] = i['economics']['capitalStock']['value']

            if 'employees' in i['economics']:
                # distribute employees through historical_values historical values
                employees = fetch_one_per_year(i['economics']['employees'])
                for employee in employees:
                    year = employee.get('year', None)
                    hist = next((hv for hv in historical_values if hv["year"] == year), None)
                    if hist is None:
                        historical_values.append({'year': year, 'employees': employee['value']})
                    else:
                        hist['employees'] = employee['value']

                # fetch latest value
                r_dict['employees'] = fetch_latest(i['economics']['employees']).get('value', None)

            r_dict['historical_values'] = historical_values
            return r_dict

        # store processed data into the Transformation instance
        # uses a set to avoid duplications
        self.etl.processed_data = []
        unique_set = set()
        for item in od:
            org = org_from_item(item)
            index = org['atoka_id']
            if index and index not in unique_set:
                self.etl.processed_data.append(org)
                unique_set.add(index)
