from project.atoka.etl.transformations import AtokaOwnershipsTransformation


class AtokaOwnershipsFromOrganizationsTransformation(AtokaOwnershipsTransformation):
    """Transform ownerships extracted from Atoka, into a data structure useable in PopoloOwnershipOrgLoader.

    Original ownerships information from ATOKA
    ------------------------------------------

    {
        'atoka_id': owner_atoka_id,
        'tax_id': owner_tax_id,
        'other_atoka_ids': [ ... ],
        'name': owner_name,
        'legal_form': owner_atoka_legal_form_level2,
        'rea': owner_rea_id,
        'cciaa': owner_cciaa,
        'shares_owned': [
            {
                'name': owned_name,
                'founded': owned_founded,
                'atoka_id': owned_atoka_id,
                'tax_id': owned_tax_id,
                'rea': owned_rea_id,
                'cciaa': owned_cciaa,
                'legal_form': owned_atoka_legal_form_level2,
                'ateco': owned_ateco,
                'percentage': ratio * 100,
                'last_updated': share_last_updated
            },
            ...
        ],
        'shareholders': [
            {
                'name': owner_name,
                'founded': owner_founded,
                'atoka_id': owner_atoka_id,
                'tax_id': owner_tax_id,
                'rea': owner_rea_id,
                'cciaa': owner_cciaa,
                'legal_form': owner_atoka_legal_form_level2,
                'ateco': owner_ateco,
                'percentage': ratio * 100,
                'last_updated': share_last_updated
            },
            ...
        ],
    },
    ...


    Organization item as needed by loader
    ------------------------------------

    processed_item = {
        "owning_org": {
            "name": owning_org_name,
            "identifiers": [{
                "scheme": "cf",
                "identifier": owning_org_identifier (CF)
            }]
        },
        "owned_org": {
            "name": owned_org_name,
            "identifiers": [{
                "scheme": "cf",
                "identifier": owned_org_identifier (CF)
            }]
        },
        "percentage": ownership_percentage,
        "start_date": ownership_start_date or None,
        "end_date": ownership_end_date or None,
        "end_reason": ownership_end_reason or None
        "sources": [{
            "url": "https://api.atoka.io",
            "note": "ATOKA API"
        }]
    }

    """

    def transform(self):
        """ Transform a list of dicts extracted from the ATOKA API,

        :return: the ETL instance (to chain methods)
        """
        self.logger.debug("start of transform")
        self.logger.debug("  get a copy of the original dataframe")
        od = self.etl.original_data

        # apply subclass-specific filters (metro/provinces separation)
        od = self.filter_rows(od)

        def ownerships_from_item(i: dict) -> list:
            _ownerships = []
            for owned in i.get('shares_owned', []):
                owning_org_tax_id = i.get("tax_id", None)
                owned_org_tax_id = owned.get("tax_id", None)
                if owning_org_tax_id and owned_org_tax_id:
                    _ownership = {
                        "owning_org": {
                            "name": i.get("name", None),
                            "identifiers": [{
                                'scheme': 'CF',
                                'identifier': owning_org_tax_id,
                            }]
                        },
                        "owned_org": {
                            "name": owned.get("name", None),
                            "identifiers": [{
                                'scheme': 'CF',
                                'identifier': owned_org_tax_id,
                            }]
                        },
                        "percentage": owned["percentage"],
                        "start_date": owned.get("last_updated", None),
                        "sources": [{
                            "url": "https://api.atoka.io",
                            "note": "ATOKA API"
                        }]

                    }
                    _ownerships.append(_ownership)

                for shareholder in owned.get('shareholders', []):
                    owning_org_tax_id = shareholder.get("tax_id", None)
                    owned_org_tax_id = owned.get("tax_id", None)
                    if owning_org_tax_id and owned_org_tax_id:
                        _ownership = {
                            "owning_org": {
                                "name": shareholder.get("name", None),
                                "identifiers": [{
                                    'scheme': 'CF',
                                    'identifier': owning_org_tax_id,
                                }]
                            },
                            "owned_org": {
                                "name": owned.get("name", None),
                                "identifiers": [{
                                    'scheme': 'CF',
                                    'identifier': owned_org_tax_id,
                                }]
                            },
                            "percentage": shareholder["percentage"],
                            "start_date": shareholder.get("last_updated", None),
                            "sources": [{
                                "url": "https://api.atoka.io",
                                "note": "ATOKA API"
                            }]

                        }
                        _ownerships.append(_ownership)

            return _ownerships

        # store processed data into the Transformation instance
        # uses a set to avoid duplications
        self.etl.processed_data = []
        unique_set = set()
        for item in od:
            for ownership in ownerships_from_item(item):
                index = self.get_index(ownership)
                if index not in unique_set:
                    self.etl.processed_data.append(ownership)
                    unique_set.add(index)


class AtokaOrgOwnershipsFromPersonsTransformation(AtokaOwnershipsTransformation):
    """Transform organization's ownerships extracted from Atoka,
    into a data structure useable in PopoloOwnershipOrgLoader.

    Original ownerships information from ATOKA
    ------------------------------------------------------------------

    people[owner_people_id].organization:

    {
      "base": {
        "active": true,
        ...
        "taxId": "00477160303",
        "vat": "00477160303"
      },
      "name": "COMUNE DI PASIAN DI PRATO",
      "shares_owned": [
        {
          "organization_id": "e5a94a28fc35",
          "percentage": 1.67,
          "last_updated": "2016-12-27"
        }
      ],
    }

    c_organizations[owner_org_id].shares_owned:
    [
        {
            "organization_id": "f1ba4d92bf1d",
            "percentage": 0,
            "last_updated": "2017-07-21"
        },
        ...
    ]
    c_organizations[owned_org_id].shareholders:
    [
        {
            "organization_id": "14af948ae335",
            "percentage": 11.50,
            "last_updated: "2018-01-13"
        },
        ...
    ]


    Organization item as needed by loader
    ------------------------------------

    processed_item = {
        "owning_org": {
            "name": owning_org_name,
            "identifiers": [{
                "scheme": "cf",
                "identifier": owning_org_identifier (CF)
            }]
        },
        "owned_org": {
            "name": owned_org_name,
            "identifiers": [{
                "scheme": "cf",
                "identifier": owned_org_identifier (CF)
            }]
        },
        "percentage": ownership_percentage,
        "start_date": ownership_start_date or None,
        "end_date": ownership_end_date or None,
        "end_reason": ownership_end_reason or None
        "sources": [{
            "url": "https://api.atoka.io",
            "note": "ATOKA API"
        }]
    }

    """

    def transform(self):
        """ Transform a list of dicts extracted from the ATOKA API,

        :return: the ETL instance (to chain methods)
        """
        self.logger.debug("start of transform")
        self.logger.debug("  get a copy of the original dataframe")

        # extract organizations from `people` dictionaries,
        # to reference them later as owning_org or owned_org
        organizations = {}
        for person in self.etl.original_data['people'].values():
            for org_atoka_id, org_dict in person['organizations'].items():
                if org_atoka_id not in organizations:
                    organizations[org_atoka_id] = org_dict

        # only read org ownerships from c_organizations
        # all other ownerships are persons' ownerships
        c_organizations = []
        for c_org_atoka_id, c_org in self.etl.original_data['c_organizations'].items():
            c_org['atoka_id'] = c_org_atoka_id
            c_organizations.append(c_org)
        od = c_organizations

        # apply subclass-specific filters (metro/provinces separation)
        od = self.filter_rows(od)

        def ownerships_from_item(i: dict) -> list:
            _ownerships = []
            for owned in i.get('shares_owned', []):
                owner_org_tax_id = i["base"].get("taxId", None)
                if owned["organization_id"] in organizations:
                    owned_org = organizations[owned["organization_id"]]
                    owned_org_tax_id = owned_org["base"].get("taxId", None)
                    if owner_org_tax_id and owned_org_tax_id:

                        _ownership = {
                            "owning_org": {
                                "name": i.get("name", None),
                                "identifiers": [{
                                    'scheme': 'CF',
                                    'identifier': owner_org_tax_id,
                                }]
                            },
                            "owned_org": {
                                "name": owned_org.get("name", None),
                                "identifiers": [{
                                    'scheme': 'CF',
                                    'identifier': owned_org_tax_id,
                                }]
                            },
                            "percentage": owned["percentage"],
                            "start_date": owned.get("last_updated", None),
                            "sources": [{
                                "url": "https://api.atoka.io",
                                "note": "ATOKA API"
                            }]

                        }
                        _ownerships.append(_ownership)
            for owner in i.get('shareholders', []):
                owned_org_tax_id = i["base"].get("taxId", None)
                if owner["organization_id"] in organizations:
                    owner_org = organizations[owner["organization_id"]]
                    owner_org_tax_id = owner_org["base"].get("taxId", None)
                    if owned_org_tax_id and owner_org_tax_id:
                        _ownership = {
                            "owning_org": {
                                "name": owner_org.get("name", None),
                                "identifiers": [{
                                    'scheme': 'CF',
                                    'identifier': owner_org_tax_id,
                                }]
                            },
                            "owned_org": {
                                "name": i.get("name", None),
                                "identifiers": [{
                                    'scheme': 'CF',
                                    'identifier': owned_org_tax_id,
                                }]
                            },
                            "percentage": owner["percentage"],
                            "start_date": owner.get("last_updated", None),
                            "sources": [{
                                "url": "https://api.atoka.io",
                                "note": "ATOKA API"
                            }]

                        }
                        _ownerships.append(_ownership)

            return _ownerships

        # store processed data into the Transformation instance
        # uses a set to avoid duplications
        self.etl.processed_data = []
        unique_set = set()
        for item in od:
            for ownership in ownerships_from_item(item):
                index = self.get_index(ownership)
                if index not in unique_set:
                    self.etl.processed_data.append(ownership)
                    unique_set.add(index)
