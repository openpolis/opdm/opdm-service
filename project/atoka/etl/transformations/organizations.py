from popolo.models import Classification

from project.atoka.etl.transformations import AtokaOrganizationsTransformation


class AtokaOrganizationsFromOrganizationsTransformation(AtokaOrganizationsTransformation):
    """Transform information extracted from Atoka, into a data structure useable in PopoloOrgLoader.

    Used to create or upgrade owner and owned organizations found in atoka.

    Original ownerships information from ATOKA
    ------------------------------------------

    {
        'atoka_id': owner_atoka_id,
        'tax_id': owner_tax_id,
        'other_atoka_ids': [ ... ],
        'name': owner_name,
        'legal_form': owner_atoka_legal_form_level2,
        'rea': owner_rea_id,
        'cciaa': owner_cciaa,
        'shares_owned': [
            {
                'name': owned_name,
                'founded': owned_founded,
                'atoka_id': owned_atoka_id,
                'tax_id': owned_tax_id,
                'rea': owned_rea_id,
                'cciaa': owned_cciaa,
                'legal_form': owned_atoka_legal_form_level2,
                'ateco': owned_ateco,
                'percentage': ratio * 100,
                'last_updated': share_last_updated,
            },
            ...
        ],
        'shareholders': [
            {
                'name': owned_name,
                'founded': owned_founded,
                'atoka_id': owned_atoka_id,
                'tax_id': owned_tax_id,
                'rea': owned_rea_id,
                'cciaa': owned_cciaa,
                'legal_form': owned_atoka_legal_form_level2,
                'ateco': owned_ateco,
                'percentage': ratio * 100,
                'last_updated': share_last_updated
            },
            ...
        ]
    },
    ...

    Organization item as needed by loader
    ------------------------------------

    "item": {
        "name": org_name,
        "classification": org_legal_form,
        "founding_date": org_founded,
        "identifiers": [
            {
                "scheme": "CF",
                "identifier": org_identifier (CF)
            },
            {
                "scheme": "ATOKA_ID",
                "identifier": org_atoka_id
            },
            {
                "scheme": "REA",
                "identifier": "{org_cia}-{org_rea}"
            }
        ],
        "classifications": [
            {
                "scheme": "FORMA_GIURIDICA_OP",
                "descr": org_legal_form
            }
        ],
        "sources": [{
            "url": "https://api.atoka.io",
            "note": "ATOKA API"
        }],
        "links": [{
            "url": "https://api.atoka.io/v2/companies/org_atoka_id,
            "note": "Altra company in ATOKA con stesso CF"
        },...],
    },

    """
    def transform(self):
        """ Transform a list of dicts extracted from the ATOKA API,

        :return: the ETL instance (to chain methods)
        """
        self.logger.debug("start of transform")
        self.logger.debug("  get a copy of the original dataframe")
        od = self.etl.original_data

        # apply subclass-specific filters (metro/provinces separation)
        od = self.filter_rows(od)

        classifications_dict = {
            c['id']: c['descr']
            for c in Classification.objects.filter(scheme='FORMA_GIURIDICA_OP').values('id', 'descr')
        }

        def org_from_item(i: dict) -> dict:
            identifier = i.get("tax_id", None)
            if identifier:
                o = {
                    "name": i["name"],
                    "founding_date": i.get("founded", None),
                    "classifications": [],
                    "contact_details": [],
                    "identifiers": [
                        {
                            "scheme": "CF",
                            "identifier": identifier
                        },
                        {
                            "scheme": "ATOKA_ID",
                            "identifier": i["atoka_id"]
                        }
                    ],
                    "sources": [{
                        "url": "https://api.atoka.io",
                        "note": "ATOKA API"
                    }],
                    "links": [],
                }
                classification_id = self.atoka2opdm_legal_form(i.get("legal_form", None))
                if classification_id:
                    o["classifications"].append({
                        "classification": classification_id
                    })
                    o["classification"] = classifications_dict[classification_id]

                if "rea" in i and i['rea'] and "cciaa" in i and i['cciaa']:
                    o["identifiers"].append({
                        "scheme": "CCIAA-REA",
                        "identifier": "{0}-{1}".format(i["cciaa"], i["rea"])
                    })
                if "vat" in i and i['vat']:
                    o["identifiers"].append({
                        "scheme": "VAT",
                        "identifier": i["vat"]
                    })
                if "ateco" in i and len(i['ateco']) > 0:
                    for ateco_class in i['ateco']:
                        o["classifications"].append({
                            "scheme": "ATOKA_ATECO",
                            "code": "{0}-{1}".format(ateco_class['rootCode'], ateco_class['code']),
                            "descr": ateco_class['description']
                        })
                if "other_atoka_ids" in i and len(i["other_atoka_ids"]):
                    for loc in i["other_atoka_ids"]:
                        o["links"].append({
                            "url": "https://api.atoka.io/v2/companies/{0}".format(loc),
                            "note": "Altra company in ATOKA con stesso CF"
                        })
                if "full_address" in i and i["full_address"]:
                    o["contact_details"].append({
                        "label": "Indirizzo della sede legale",
                        "contact_type": "MAIL",
                        "value": i["full_address"],
                        "note": "Indirizzo da Atoka"
                    })
                return o

        # store processed data into the Transformation instance
        # uses a set to avoid duplications
        self.etl.processed_data = []
        unique_set = set()
        for owning_item in od:
            owning_org = org_from_item(owning_item)
            index = self.get_index(owning_org)
            if index and index not in unique_set:
                self.etl.processed_data.append(owning_org)
                unique_set.add(index)
            for item in owning_item.get("shares_owned", []):
                org = org_from_item(item)
                index = self.get_index(org)
                if index and index not in unique_set:
                    self.etl.processed_data.append(org)
                    unique_set.add(index)
            for item in owning_item.get("shareholders", []):
                org = org_from_item(item)
                index = self.get_index(org)
                if index and index not in unique_set:
                    self.etl.processed_data.append(org)
                    unique_set.add(index)


class AtokaOrganizationsFromPersonsTransformation(AtokaOrganizationsTransformation):
    """Transform information extracted from Atoka, into a data structure useable in PopoloOrgLoader.

    Used to create or upgrade owner and owned organizations found in atoka.

    Original ownerships information from ATOKA (starting from persons)
    ------------------------------------------

    {
        "people": {
            "p6riP9Obo13RzlOJDC": {
                "atoka_id": "p6riP9Obo13RzlOJDC",
                "name": "Aurelia Bubisutti",
                "organizations": {
                    "e5a94a28fc35": {
                        "name": "CAFC S.P.A.",
                        "base": {
                            "active": true,
                            ...
                        }
                    }
                }
            },
            ...
        },
        "c_organizations": {
            "077be869749f": {
                "base": {
                    "active": true,
                    ...
                },
                "name": "COMUNE DI PASIAN DI PRATO",
                "shares_owned": [
                    {
                        "organization_id": "e5a94a28fc35",
                        "percentage": 1.67,
                        "last_updated": "2016-12-27"
                    }
                ]
                "shareholders": [
                    {
                        "organization_id": "14af948ae335",
                        "percentage": 11.50,
                        "last_updated": "2018-01-13"
                    }
                ]
            },
            ...
        }
    }

    Organization item as needed by loader
    ------------------------------------

    "item": {
        "name": org_name,
        "classification": org_legal_form,
        "founding_date": org_founded,
        "identifiers": [
            {
                "scheme": "CF",
                "identifier": org_CF
            },
            {
                "scheme": "ATOKA_ID",
                "identifier": org_atoka_id
            },
            {
                "scheme": "REA",
                "identifier": "{org_cia}-{org_rea}"
            }
        ],
        "classifications": [
            {
                "scheme": "FORMA_GIURIDICA_OP",
                "descr": org_legal_form
            }
        ],
        "sources": [{
            "url": "https://api.atoka.io",
            "note": "ATOKA API"
        }],
        "links": [{
            "url": "https://api.atoka.io/v2/companies/org_atoka_id,
            "note": "Altra company in ATOKA con stesso CF"
        },...],
    },

    """

    def transform(self):
        """ Transform a list of dicts extracted from the ATOKA API,

        :return: the ETL instance (to chain methods)
        """
        self.logger.debug("start of transform")
        self.logger.debug("  get a copy of the original dataframe")

        # extract organizations from `people` and `c_organizations` dictionaries in original_data
        organizations = []
        for person in self.etl.original_data['people'].values():
            for org_atoka_id, org_dict in person['organizations'].items():
                org_dict['atoka_id'] = org_atoka_id
                organizations.append(org_dict)
        for c_org_atoka_id, c_org in self.etl.original_data['c_organizations'].items():
            c_org['atoka_id'] = c_org_atoka_id
            organizations.append(c_org)

        od = organizations

        # apply filter, if defined (debugging purposes)
        od = self.filter_rows(od)

        classifications_dict = {
            c['id']: c['descr']
            for c in Classification.objects.filter(scheme='FORMA_GIURIDICA_OP').values('id', 'descr')
        }

        def org_from_item(i: dict) -> dict:
            base = i.get('base')
            identifier = base.get("taxId", None)
            if identifier:
                o = {
                    "name": i["name"],
                    "founding_date": base.get("founded", None),
                    "classifications": [],
                    "contact_details": [],
                    "identifiers": [
                        {
                            "scheme": "CF",
                            "identifier": identifier
                        },
                        {
                            "scheme": "ATOKA_ID",
                            "identifier": i["atoka_id"]
                        }
                    ],
                    "sources": [{
                        "url": "https://api.atoka.io",
                        "note": "ATOKA API"
                    }],
                    "links": [],
                }
                legal_forms = base.get("legalForms", [])
                if len(legal_forms):
                    legal_form = next(filter(lambda x: x['level'] == 2, legal_forms)).get("name", None)
                    classification_id = self.atoka2opdm_legal_form(legal_form)
                    if classification_id:
                        o["classifications"].append({
                            "classification": classification_id
                        })
                        o["classification"] = classifications_dict[classification_id]

                if "rea" in base and base['rea'] and "cciaa" in base and base['cciaa']:
                    o["identifiers"].append({
                        "scheme": "CCIAA-REA",
                        "identifier": "{0}-{1}".format(base["cciaa"], base["rea"])
                    })
                if "vat" in base and base['vat']:
                    o["identifiers"].append({
                        "scheme": "VAT",
                        "identifier": base["vat"]
                    })
                if "ateco" in base and len(base['ateco']) > 0:
                    for ateco_class in base['ateco']:
                        o["classifications"].append({
                            "scheme": "ATOKA_ATECO",
                            "code": "{0}-{1}".format(ateco_class['rootCode'], ateco_class['code']),
                            "descr": ateco_class['description']
                        })
                if (
                    "registeredAddress" in base and
                    "fullAddress" in base["registeredAddress"] and
                    base["registeredAddress"]["fullAddress"]
                ):
                    o["contact_details"].append({
                        "label": "Indirizzo della sede legale",
                        "contact_type": "MAIL",
                        "value": base["registeredAddress"]["fullAddress"],
                        "note": "Indirizzo da Atoka"
                    })
                return o

        # store processed data into the Transformation instance
        # uses a set to avoid duplications
        self.etl.processed_data = []
        unique_set = set()
        for org_item in od:
            org = org_from_item(org_item)
            index = self.get_index(org)
            if index and index not in unique_set:
                self.etl.processed_data.append(org)
                unique_set.add(index)
