import re
from collections import defaultdict

from popolo.models import Organization, Person

from project.core.dicts_functions import get_org_by_tax_id_dict
from project.atoka.etl.transformations import AtokaMembershipsTransformation


class PersonDict(dict):
    """dict extension used to store unique information for a person

    Has the same behaviour of a dict, plus:
    - the memberships_index attribute (a set)
    - the merge() method
    """

    def __init__(self):
        super().__init__()
        self.memberships_index = set()
        self.ownerships_index = set()

    def merge(self, _p):
        """
        Merge information contained in pm within this PersonDict instance
        Anagraphical data are only added once,
        Memberships are merged so that there is no repetition

        :param _pm: the person_memberships dictionary containing info to be merged
        :return:
        """

        # add anagraphical data if not present yet
        if 'given_name' not in self:
            for k, v in _p.items():
                if k != 'memberships' and k != 'ownerships':
                    self[k] = v

        # merge memberships data
        for m in _p.get('memberships', []):
            m_index = (m["label"], m["organization_id"], m["start_date"])
            if m_index not in self.memberships_index:
                self.memberships_index.add(m_index)
                if 'memberships' not in self:
                    self['memberships'] = []
                self['memberships'].append(m)

        # merge ownerships data
        for o in _p.get('ownerships', []):
            o_index = (o["organization_id"], o.get("start_date", None))
            if o_index not in self.ownerships_index:
                self.ownerships_index.add(o_index)
                if 'ownerships' not in self:
                    self['ownerships'] = []
                self['ownerships'].append(o)


class AtokaPersonsMembershipsOwnershipsFromOrganizationsTransformation(AtokaMembershipsTransformation):
    """Transform roles extracted from Atoka, into a data structure useable in PopoloPersonWithMembershipsLoader.

    Original memberships information from ATOKA
    ------------------------------------------

    {
        'atoka_id': owner_atoka_id,
        'tax_id': owner_tax_id,
        'other_atoka_ids': [ ... ],
        'name': owner_name,
        'legal_form': owner_atoka_legal_form_level2,
        'rea': owner_rea_id,
        'cciaa': owner_cciaa,
        'shares_owned': [],
        'shareholders': [],
        'roles': [
            {
                'person': {
                    'given_name': person_given_name,
                    'family_name': person_family_name,
                    'birth_date': person_birth_date,
                    'birth_location': person_birth_location (city (prov),
                    'gender': person_gender,
                    'tax_id': person_tax_id,
                    'atoka_id': person_atoka_id
                },
                'label': role_label,
                'start_date': role_start_date
            },
            ...
        ],
        'shareholders_people': [
            {
                'person': {
                    'given_name': person_given_name,
                    'family_name': person_family_name,
                    'birth_date': person_birth_date,
                    'birth_location': person_birth_location (city (prov),
                    'gender': person_gender,
                    'tax_id': person_tax_id,
                    'atoka_id': person_atoka_id
                },
                'percentage': perc,
                'last_updated': last_updated
            },
            ...
        ],
    },
    ...


    Membership item as needed by loader
    ------------------------------------

    processed_item =   {
        "given_name": "Antonino",
        "family_name": "PAPALDO",
        "birth_date": "1899-04-19",
        "birth_location": "Pedara (CT)"
        "gender": "M",
        "tax_id": role["tax_id"],
        "vat": role["vat"],
        "sources": [
            {
                "url": "https://api.atoka.io",
                "note": "ATOKA API"
            },
            ...
        ],
        "memberships": [
            {
                "label": "Giudice della Corte Costituzionale",
                "role": "Giudice costituzionale",
                "start_date": "1953-03-19"
                "end_date": "1967-12-15",
                "organization_id": 24022,
                "sources": [
                    {
                        "url": "https://api.atoka.io",
                        "note": "ATOKA API"
                    }
                ],
            },
            ...
        ],
        "ownerships": [
            {
                "organization_id": 24033,
                "percentage": 0.276,
                "start_date": "2015-09-23",
                "sources": [
                    {
                        "url": "https://api.atoka.io",
                        "note": "ATOKA API"
                    }
                ],
            },
            ...
        ],
    }

    The only information not being there are:
    - the organization_id (in terms of OPDM),
    - the role pointing to an existing OPDM RoleType

    We need a dict of all organizations with their tax_id within the ETL process,
    and a map of all RoleTypes,
    then a mapping between the labels used in ATOKAs roles and the RoleTypes in OPDM.
    """

    @classmethod
    def memberships_from_item(cls, i: dict, orgs_dict: dict, logger) -> list:
        _memberships = []
        org_tax_id = i.get("tax_id", None)
        roles = i.get("roles", [])
        if org_tax_id and roles:
            try:
                org = orgs_dict[org_tax_id]
            except KeyError:
                logger.error(f"Could not find {org_tax_id} among the organizations. Skipping it.")
            else:

                for role in i.get('roles', []):
                    # validation criteria, to avoid errors and unwanted roles
                    role_type = cls.atoka_role_2_opdm_roletype(role["label"])
                    if role_type is None:
                        if role["label"].lower() not in cls.atoka_roles_skip_list:
                            logger.warning(
                                f"Role {role['label']} is neither among the allowed roles, nor in the skip list."
                                "Skipping it."
                            )
                        continue
                    if org["classification"] is None:
                        logger.error(
                            f"Org {org['id']} has no legal form. "
                            "Can not get a RoleType for roles regarding it. "
                            "Skipping role."
                        )
                        continue

                    person_role = {
                        "given_name": role["person"]["given_name"],
                        "family_name": role["person"]["family_name"],
                        "gender": role["person"]["gender"],
                        "birth_date": role["person"]["birth_date"],
                        "birth_location": role["person"]["birth_location"],
                        "identifiers": [
                            {
                                "scheme": "CF",
                                "identifier": role["person"]["tax_id"]
                            },
                            {
                                "scheme": "ATOKA_ID",
                                "identifier": role["person"]["atoka_id"]
                            },
                        ],
                        "sources": [
                            {
                                "url": "https://api.atoka.io",
                                "note": "ATOKA API"
                            }
                        ],
                        "memberships": [
                            {
                                "label": "{0} {1}".format(
                                    role_type,
                                    org["name"]
                                ),
                                "role": "{0} {1}".format(
                                    role_type,
                                    org["classification"].lower()
                                ),
                                "start_date": role["start_date"],
                                "organization_id": org["id"],
                                "sources": [
                                  {
                                    "url": "https://api.atoka.io",
                                    "note": "ATOKA API"
                                  }
                                ],
                            }
                        ],
                    }
                    _memberships.append(person_role)

        return _memberships

    @classmethod
    def ownerships_from_item(cls, i: dict, orgs_dict: dict, logger) -> list:
        _ownerships = []
        org_tax_id = i.get("tax_id", None)
        shareholders = i.get("shareholders_people", [])
        if org_tax_id and shareholders:
            try:
                org = orgs_dict[org_tax_id]
            except KeyError:
                logger.error(f"Could not find {org_tax_id} among the organizations. Skipping it.")
            else:

                for shareholder in i.get('shareholders_people', []):
                    # validation criteria, to avoid errors and unwanted roles
                    person = {
                        "given_name": shareholder["person"]["given_name"],
                        "family_name": shareholder["person"]["family_name"],
                        "gender": shareholder["person"]["gender"],
                        "birth_date": shareholder["person"]["birth_date"],
                        "birth_location": shareholder["person"]["birth_location"],
                        "identifiers": [
                            {
                                "scheme": "CF",
                                "identifier": shareholder["person"]["tax_id"]
                            },
                            {
                                "scheme": "ATOKA_ID",
                                "identifier": shareholder["person"]["atoka_id"]
                            },
                        ],
                        "sources": [
                            {
                                "url": "https://api.atoka.io",
                                "note": "ATOKA API"
                            }
                        ],
                        "ownerships": [
                            {
                                "percentage": shareholder["percentage"],
                                "start_date": shareholder.get("last_updated", None),
                                "organization_id": org["id"],
                                "sources": [
                                  {
                                    "url": "https://api.atoka.io",
                                    "note": "ATOKA API"
                                  }
                                ],
                            }
                        ],
                    }
                    _ownerships.append(person)

        return _ownerships

    def transform(self):
        """ Transform a list of dicts extracted from the ATOKA API,

        :return: the ETL instance (to chain methods)
        """
        self.logger.debug("start of transform")
        self.logger.debug("  get a copy of the original dataframe")
        od = self.etl.original_data

        od = self.filter_rows(od)

        orgs_tax_ids = []
        for item in od:
            tax_id = item.get("tax_id", None)
            if tax_id:
                orgs_tax_ids.append(tax_id)
            else:
                self.logger.error(
                    f"Could not find 'tax_id' in org with "
                    f"name: {item.get('name', '-')}, atoka_id: {item.get('atoka_id', '-')}."
                )
        organizations_dict = get_org_by_tax_id_dict(orgs_tax_ids)

        # store processed data into the Transformation instance
        persons = defaultdict(PersonDict)
        for item in od:
            persons_memberships = self.memberships_from_item(item, organizations_dict, self.logger)
            for pm in persons_memberships:
                p_index = (
                    pm["given_name"],
                    pm["family_name"],
                    pm["birth_date"],
                    pm["birth_location"],
                )
                persons[p_index].merge(pm)
            persons_ownerships = self.ownerships_from_item(item, organizations_dict, self.logger)
            for po in persons_ownerships:
                p_index = (
                    po["given_name"],
                    po["family_name"],
                    po["birth_date"],
                    po["birth_location"],
                )
                persons[p_index].merge(po)

        self.etl.processed_data = list(persons.values())


class AtokaPersonsMembershipsOwnershipsFromPersonsTransformation(AtokaMembershipsTransformation):
    """Transform persons, with related memberships and ownerhips extracted from Atoka,
    into a data structure useable in PopoloPersonWithMembershipsLoader.

    Original memberships information from ATOKA
    ------------------------------------------
    .people[owner_id].organizations[owned_id]
    {
      "atoka_id": "pJTNQJpGjyiAG5vKFy",
      "name": "Roberto Bagnasco",
      "organizations": {
        "02e2c544c38e": {
          "name": "UNIONE FARMACISTI LIGURI SPA",
          "base": {
            "active": true,
            ...
            "startup": false,
            "taxId": "03795140106",
          },
          "name": "CAFC S.P.A.",
          "shares_owned": [
            {
              "percentage": 50,
              "last_updated": "2007-02-23"
            }
          ],
          "roles": [
            {
              "name": "consigliere",
              "official": true,
              "since": "2017-06-07"
            }
          ]
        }
      }
      ...
    }

    .c_people[owner_id]
    {
      "base": {
        "birthDate": "1986-10-02",
        "birthPlace": {
          "macroregion": "Nord-ovest",
          "municipality": "Genova",
          "province": "Genova",
          "provinceCode": "GE",
          "region": "Liguria",
          "state": "Italia",
          "stateCode": "IT"
        },
        "familyName": "Massardo",
        "gender": "M",
        "givenName": "Davide",
        "taxId": "MSSDVD86R02D969X"
      },
      "name": "Davide Massardo",
      "shares_owned": [
        {
          "organization_id": "963e2f4f3851",
          "percentage": 50,
          "last_updated": "2007-02-23"
        },
        {
          "organization_id": "1daf1bc9a56c",
          "percentage": 35,
          "last_updated": "2016-05-12"
        }
      ],
      "roles": [
        {
          "organization_id": "02e2c544c38e",
          "name": "consigliere",
          "since": "2016-05-12"
        }
      ],
    }


    Membership item as needed by loader
    ------------------------------------

    processed_item =   {
        "given_name": "Davide",
        "family_name": "Massardo",
        "birth_date": "1986-10-02",
        "birth_location": "Genova (GE)"
        "gender": "M",
        "tax_id": "MSSDVD86R02D969X",
        "sources": [
          {
            "url": "https://api.atoka.io",
            "note": "ATOKA API"
          },
          ...
        ],
        "memberships": [
          {
            "label": "Membro del Consiglio di Amministrazione di CAFC S.P.A",
            "role": "Membro del Consiglio di amministrazione",
            "start_date": "2017-06-07"
            "end_date": null,
            "organization_id": 24022,
            "sources": [
              {
                "url": "https://api.atoka.io",
                "note": "ATOKA API"
              }
            ],
          },
          ...
        ],
        "ownerships": [
          {
            "organization_id": 20847,
            "percentage": 50,
            "sources": [
              {
                "url": "https://api.atoka.io",
                "note": "ATOKA API"
              }
            ]
          },
          ...
        ]
    }

    """

    @staticmethod
    def get_index(d: dict) -> tuple:
        """Return unique hashable index for personal ownership dict

        :param d: dict containing ownership details
        :return: tuple
        """
        return d["owning_person"]["identifier"], d["owned_org"]["identifier"]

    def transform(self):
        """ Transform a list of dicts extracted from the ATOKA API,

        :return: the ETL instance (to chain methods)
        """
        self.logger.debug("start of transform")
        self.logger.debug("  get a copy of the original dataframe")

        # dictionary containing all persons' details for which information are gathered
        # info are merged to avoid duplications
        persons_details = defaultdict(PersonDict)

        od = self.etl.original_data

        # apply subclass-specific filters (metro/provinces separation)
        od = self.filter_rows(od)

        # extract organizations details from DB, into a dict to avoid query during the loop
        # the key here is the ATOKA_ID
        organizations_in_opdm_atoka_ids = []
        for person_tax_id, person in od['people'].items():
            organizations_in_opdm_atoka_ids.extend(person['organizations'].keys())
        organizations_in_opdm = Organization.objects.filter(
            identifiers__scheme='ATOKA_ID',
            identifiers__identifier__in=organizations_in_opdm_atoka_ids
        ).values(
            "id", "identifiers__identifier", "identifier", "name", "classification"
        )
        organizations_in_opdm_dict = {
            organization.pop("identifiers__identifier"): organization
            for organization in organizations_in_opdm
        }
        del organizations_in_opdm, organizations_in_opdm_atoka_ids

        # extract people details from DB, into a dict to avoid query during the loop
        # the key here is the CF
        people_tax_ids = list(od['people'].keys())
        people_in_opdm = Person.objects.filter(
            identifiers__scheme='CF', identifiers__identifier__in=people_tax_ids,
        ).values(
            "id", "identifiers__identifier",
            "family_name", "given_name", "gender", "birth_date", "birth_location",
        )
        people_in_opdm_dict = {
            person.pop("identifiers__identifier"): person
            for person in people_in_opdm
        }
        del people_in_opdm, people_tax_ids

        # extract memberships from .people[].organizations[].roles
        # and ownerships from .people[].organizations[].shares_owned
        # for people and organizations already known in opdm
        for person_tax_id, person in od['people'].items():

            person_opdm = people_in_opdm_dict.get(person_tax_id, None)

            if person_opdm is None:
                self.logger.error(
                    f"Could not find person with tax_id:{person_tax_id} in opdm. Skipping transformation."
                )
                continue

            person_details = {
                "given_name": person_opdm["given_name"],
                "family_name": person_opdm["family_name"],
                "gender": person_opdm["gender"],
                "birth_date": person_opdm["birth_date"],
                "birth_location": person_opdm["birth_location"],
                "identifiers": [
                    {
                        "scheme": "CF",
                        "identifier": person_tax_id
                    },
                    {
                        "scheme": "ATOKA_ID",
                        "identifier": person["atoka_id"]
                    },
                ],
                "sources": [
                    {
                        "url": "https://api.atoka.io",
                        "note": "ATOKA API"
                    }
                ],
                "memberships": [],
                "ownerships": []
            }

            p_index = (
                person_details["given_name"],
                person_details["family_name"],
                person_details["birth_date"],
                person_details["birth_location"],
            )

            for org_atoka_id, org_dict in person['organizations'].items():

                # fetch opdm organizations (must have been already loaded)
                org_opdm = organizations_in_opdm_dict.get(org_atoka_id, None)

                if org_opdm is None:
                    org_opdm = Organization.objects.filter(
                        links__link__url=f"https://api.atoka.io/v2/companies/{org_atoka_id}"
                    ).values(
                       "id", "identifiers__identifier", "identifier", "name", "classification"
                    ).first()
                    if org_opdm is None:
                        # skip orgs not in opdm, log at debug level, not to clutter things
                        self.logger.debug(
                            f"Could not find org with atoka_id: {org_atoka_id} in opdm. Skipping transformation."
                        )
                        continue

                # roles => memberships
                for role in org_dict.get('roles', []):

                    # validation criteria, to avoid errors and unwanted roles
                    role_name = re.sub(r"\s+", " ", role["name"])
                    role_type = AtokaMembershipsTransformation.atoka_role_2_opdm_roletype(role_name)
                    if role_type is None:
                        if role_name.lower() not in AtokaMembershipsTransformation.atoka_roles_skip_list:
                            self.logger.warning(
                                "Role <{0}> is neither among the allowed roles, nor in the skip list."
                                "Skipping it.".format(role_name)
                            )
                        continue
                    if org_opdm["classification"] is None:
                        self.logger.error(
                            "Org with opdm id: <{0}> has no legal form. "
                            "Can not get a RoleType for roles regarding it. "
                            "Skipping role.".format(org_opdm["id"])
                        )
                        continue

                    person_details['memberships'].append({
                        "label": "{0} {1}".format(
                            role_type,
                            org_opdm["name"]
                        ),
                        "role": "{0} {1}".format(
                            role_type,
                            org_opdm["classification"].lower()
                        ),
                        "start_date": role.get("since", None),
                        "organization_id": org_opdm["id"],
                        "sources": [
                          {
                            "url": "https://api.atoka.io",
                            "note": "ATOKA API"
                          }
                        ],
                    })

                # shares_owned => ownerships
                for share in org_dict.get('shares_owned', []):

                    person_details['ownerships'].append({
                        "organization_id": org_opdm['id'],
                        "percentage": share["percentage"],
                        "start_date": share.get("last_updated", None),
                        "sources": [{
                            "url": "https://api.atoka.io",
                            "note": "ATOKA API"
                        }]
                    })

                persons_details[p_index].merge(person_details)

        # extract memberships from .c_people[].roles
        # and ownerships from .c_people[].shares_owned
        for person_atoka_id, person in od['c_people'].items():
            person_base = person["base"]
            if person_base.get("birthPlace", None):
                if "provinceCode" in person_base["birthPlace"]:
                    birth_location = "{0} ({1})".format(
                        person_base["birthPlace"]["municipality"],
                        person_base["birthPlace"]["provinceCode"]
                    )
                else:
                    if "municipality" in person_base["birthPlace"]:
                        birth_location = person_base["birthPlace"]["municipality"]
                    else:
                        birth_location = person_base["birthPlace"]["state"]
            else:
                birth_location = None

            person_details = {
                "given_name": person_base.get("givenName", "-"),
                "family_name": person_base.get("familyName", "-"),
                "gender": person_base.get("gender", None),
                "birth_date": person_base.get("birthDate", None),
                "birth_location": birth_location,
                "identifiers": [
                    {
                        "scheme": "CF",
                        "identifier": person_base.get("taxId", None)
                    },
                    {
                        "scheme": "ATOKA_ID",
                        "identifier": person_atoka_id
                    },
                ],
                "sources": [
                    {
                        "url": "https://api.atoka.io",
                        "note": "ATOKA API"
                    }
                ],
                "memberships": [],
                "ownerships": []
            }

            p_index = (
                person_details["given_name"],
                person_details["family_name"],
                person_details["birth_date"],
                person_details["birth_location"],
            )

            # roles => memberships
            for role in person.get('roles', []):
                org_opdm = organizations_in_opdm_dict.get(role["organization_id"], None)
                if org_opdm is None:
                    org_opdm = Organization.objects.filter(
                        links__link__url=f"https://api.atoka.io/v2/companies/{role['organization_id']}"
                    ).values(
                       "id", "identifiers__identifier", "identifier", "name", "classification"
                    ).first()
                    if org_opdm is None:
                        self.logger.error(
                            f"Could not find org with atoka_id: "
                            f"{role['organization_id']} in opdm. Skipping transformation."
                        )
                        continue

                # validation criteria, to avoid errors and unwanted roles
                role_name = re.sub(r"\s+", " ", role["name"])
                role_type = AtokaMembershipsTransformation.atoka_role_2_opdm_roletype(role_name)
                if role_type is None:

                    if role_name.lower() not in AtokaMembershipsTransformation.atoka_roles_skip_list:
                        self.logger.warning(
                            "Role <{0}> is neither among the allowed roles, nor in the skip list."
                            "Skipping it.".format(role_name)
                        )
                    continue
                if org_opdm["classification"] is None:
                    self.logger.error(
                        "Org wih opdm id: <{0}> has no legal form. "
                        "Can not get a RoleType for roles regarding it. "
                        "Skipping role.".format(org_opdm["id"])
                    )
                    continue

                person_details['memberships'].append({
                    "label": "{0} {1}".format(
                        role_type,
                        org_opdm["name"]
                    ),
                    "role": "{0} {1}".format(
                        role_type,
                        org_opdm["classification"].lower()
                    ),
                    "start_date": role.get("since", None),
                    "organization_id": org_opdm["id"],
                    "sources": [{
                        "url": "https://api.atoka.io",
                        "note": "ATOKA API"
                    }]
                })

            # shares_owned => ownerships
            for share in person.get('shares_owned', []):
                org_opdm = organizations_in_opdm_dict.get(share["organization_id"], None)
                if org_opdm is None:
                    org_opdm = Organization.objects.filter(
                        links__link__url=f"https://api.atoka.io/v2/companies/{share['organization_id']}"
                    ).values(
                       "id", "identifiers__identifier", "identifier", "name", "classification"
                    ).first()
                    if org_opdm is None:
                        self.logger.error(
                            f"Could not find org with atoka_id: "
                            f"{share['organization_id']} in opdm. Skipping transformation."
                        )
                        continue
                person_details['ownerships'].append({
                    "organization_id": org_opdm['id'],
                    "percentage": share["percentage"],
                    "start_date": share.get("last_updated", None),
                    "sources": [{
                        "url": "https://api.atoka.io",
                        "note": "ATOKA API"
                    }]
                })

            persons_details[p_index].merge(person_details)

        self.etl.processed_data = [pm for pm in persons_details.values()]
