from popolo.models import Organization

from project.atoka.models import OrganizationEconomics
from project.tasks.etl.loaders import PopoloLoader


class OrganizationEconomicsLoader(PopoloLoader):
    """:class:`Loader` that stores  data in ``atoka.OrganizationEconomics`` instances

    The generic `load` method can still be overridden in subclasses,
    should peculiar necessities arise (as bulk loading).
    """
    areas_dict = None

    def __init__(self, **kwargs):
        """Create a new instance of the loader

        Args:
            lookup_strategy: anagraphical, identifier, mixed select the lookup strategy to use
            identifier_scheme: the scheme to use whith the mixer/identifier lookup strategies (OP_ID, CF, ...)
            update_strategy: how to update the Organization
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
        """
        super(OrganizationEconomicsLoader, self).__init__(**kwargs)
        self.identifier_scheme = kwargs.get('identifier_scheme', 'ATOKA_ID')
        self.update_strategy = kwargs.get('update_strategy', 'overwrite')

    def load_item(self, item, **kwargs):
        """load Organization into the Popolo models
        lookup an organization, with strategy defined in self.lookup_strategy
        invoke update_or_create_from_item (anagraphical data plus identifiers, contacts, ...)

        :param item: the item to be loaded
        :return:
        """

        _id = item.pop(self.identifier_scheme.lower())
        historical_values = item.pop('historical_values', [])

        try:
            org = Organization.objects.get(
                identifiers__scheme=self.identifier_scheme,
                identifiers__identifier=_id,
            )
        except Organization.DoesNotExist:
            self.logger.error("Could not find organization with {0}:{1} ({2})".format(
                self.identifier_scheme, _id, item['name'])
            )
            return

        # update or create logic that use defined update_strategy
        defaults = {
            k.replace('latest_', ''): v for k, v in item.items() if k not in ['tax_id', 'name']
        }
        try:
            o, created = OrganizationEconomics.objects.get_or_create(
                organization=org,
                defaults=defaults
            )
            if created:
                self.logger.debug("Economics details created for org with {0}:{1}".format(self.identifier_scheme, _id))
            else:
                for k, v in defaults.items():
                    if self.update_strategy == "overwrite" or not getattr(o, k):
                        setattr(o, k, v)
                o.save()

        except Exception as e:
            self.logger.error("{0} when loading organization {1}".format(e, item))
            return

        for hv in historical_values:
            year = hv.get('year', None)

            # skip insertion for info not labeled with a year
            if year is None:
                continue

            # update or create logic that use defined update_strategy
            hv_defaults = {
                k: v for k, v in hv.items() if k not in ['currency', 'date', 'year']
            }
            oh, created = o.historical_values.get_or_create(
                year=year,
                defaults=hv_defaults
            )
            if created:
                self.logger.debug(
                    "Historical economics details created for org with {0}:{1}".format(self.identifier_scheme, _id)
                )
            else:
                for k, v in hv_defaults.items():
                    if self.update_strategy == "overwrite" or not getattr(o, k):
                        setattr(o, k, v)
                o.save()
