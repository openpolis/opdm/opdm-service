from typing import Dict

from django.conf import settings


def main_settings(*_) -> Dict:
    """This processor adds some useful informations about this project to the Context."""

    return {
        "project_name": settings.PROJECT_NAME,
        "project_version": settings.VERSION,
        "project_git_rev": settings.GIT_REVISION,
        "is_production": settings.CONTEXT == "production",
    }
