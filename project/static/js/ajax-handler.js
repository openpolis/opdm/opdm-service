document.addEventListener('DOMContentLoaded', function () {

    // Variabile per tenere traccia dell'ultima riga che ha attivato il popup
    var lastTriggeredLookup = null;

    function updateInstances() {
        var contentType = this;
        lastTriggeredLookup = 'id_' + contentType.parentNode.parentNode.id + '-subject_id';
        var input = document.getElementById(lastTriggeredLookup);
        input.value = '';

        fetch('/get-instances/?content_type=' + contentType.value)
            .then(response => response.json())
            .then(data => {

                var lookupLink = document.getElementById('lookup_id_' + lastTriggeredLookup);

                // inputSubj.value = '';
                if (lookupLink) {
                    lookupLink.href = '/admin/' + data.app_label + '/' + data.model + '/?_to_field=id';
                }
            })
            .catch(error => {
                var lookupLink = document.getElementById('lookup_id_' + lastTriggeredLookup);
                if (lookupLink) {
                    lookupLink.href = "javascript:void(0)";
                }
                // console.error('Errore nella richiesta fetch:', error);
            });
    }

    var container = document.getElementById('subjects-group');
    container.addEventListener('change', function (event) {
        var target = event.target;
        if (target && target.matches('.content-type-subject')) {
            updateInstances.call(target);
        }
    });

    container.addEventListener('click', function (event) {
        var target = event.target;
        if (target && target.classList.contains('related-lookup') && target.className === "content-type-subject") {
            lastTriggeredLookup = 'id_' + target.parentNode.parentNode.id + '-subject_id';
        }
    });

    var originalDismissPopup = window.dismissRelatedLookupPopup;
    window.dismissRelatedLookupPopup = function (win, chosenId) {
        if (lastTriggeredLookup) {
            // console.log(lastTriggeredLookup);
            var object_id_field = document.getElementById(lastTriggeredLookup);
            if (object_id_field) {
                object_id_field.value = chosenId;
                win.close();
            }
            lastTriggeredLookup = null;
        }

        // Assicurati di chiamare la funzione originale per chiudere il popup
        originalDismissPopup(win, chosenId);
    };
});
