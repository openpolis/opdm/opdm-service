from django import forms
from django.contrib.contenttypes.models import ContentType

from project.opp.home.models import EventSubject


class ContentTypeSelector(forms.Select):
    add_id_index = True
    class Media:
        js = ('js/ajax-handler.js',)

class InstanceSelector(forms.TextInput):
    def render(self, name, value, attrs=None, renderer=None):
        attrs_id = attrs['id']
        output = super().render(name, value, attrs, renderer)
        # Aggiungi il link HTML dopo l'output dell'input
        lookup_link = f'<a href="javascript:void(0)" style="cursor: default;" class="related-lookup" id="lookup_id_{attrs_id}" title="Lookup"></a>'
        return output + lookup_link

class GenericForeignKeySelectorForm(forms.ModelForm):

    subject_ct = forms.ModelChoiceField(
        queryset=ContentType.objects.filter(
            model__in=['person','organization', 'voting',
                       'bill', 'govdecree', 'govbill', 'implementingdecree']
        ),
        widget=ContentTypeSelector(attrs={'class': 'content-type-subject',}),
    )
    subject_id = forms.IntegerField(widget=InstanceSelector)


    class Meta:
        model = EventSubject
        fields = ['subject_ct', 'subject_id',]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # Verifica se l'oggetto è in modalità di sola lettura
        if self.instance and self.instance.pk:
            for field_name in self.fields:
                if field_name not in ['subject_id']:
                    self.fields[field_name].widget.attrs['readonly'] = True
                    self.fields[field_name].widget.attrs['disabled'] = True
