from django.contrib import admin

from project.opp.home.forms import GenericForeignKeySelectorForm
from project.opp.home.models import Event, Link, EventSubject


class EventSubjectInline(admin.TabularInline):
    def has_change_permission(self, request, obj=None):
        return False
    def has_add_permission(self, request, obj):
        return True
    def has_delete_permission(self, request, obj=None):
        return True

    def object_id_info(self, instance):
        if instance.object_id and instance.object_ct:
            try:
                model_class = instance.object_ct.model_class()
                obj = model_class.objects.get(pk=instance.object_id)
                return f'{obj}'  # Aggiungi i campi specifici del tuo modello
            except model_class.DoesNotExist:
                return "Object not found"
        return "N/A"

    object_id_info.short_description = 'Object Info'

    def subject_id_info(self, instance):
        if instance.subject_id and instance.subject_ct:
            try:
                model_class = instance.subject_ct.model_class()
                obj = model_class.objects.get(pk=instance.subject_id)
                return f'{obj}'  # Aggiungi i campi specifici del tuo modello
            except model_class.DoesNotExist:
                return "Object not found"
        return "N/A"


    subject_id_info.short_description = 'Subject Info'
    fields = ('subject_id_info', 'subject_ct', 'subject_id',)
    readonly_fields = ('subject_id_info', )
    # raw_id_fields = ("subject",)
    model = EventSubject
    form = GenericForeignKeySelectorForm
    extra = 1  # Number of empty forms to display


@admin.register(Event)
class EventsAdmin(admin.ModelAdmin):
    raw_id_fields = ('date',)
    date_hierarchy = 'date__date'
    inlines = [EventSubjectInline]
    # def ref_object(self, obj):
    #     return obj.ref_object

    def get_readonly_fields(self, request, obj=None):
        return ['ref_object', ]
    fields = ('date', 'is_key_event', 'title', 'description','branch', 'category')
    list_filter = ('is_key_event',
                   'branch',
                   'category'

                   )
    search_fields = ('date__date', 'title')
    list_display = ('date', 'title', 'category', 'is_key_event', 'branch')



@admin.register(Link)
class LinkAdmin(admin.ModelAdmin):
    search_fields = ('url', 'title',)

    list_filter = (
        'date',
    )
    fieldsets = (
        ('Principale', {
            'fields': (
                'url',
                'title',
                'description',
                'image',
                'date'
            ),
            'classes': (
                'baton-tabs-init',
            ),
        }),
    )
    list_display = ('title', 'date',)

    class Media:
        css = {
            "all": ("css/admin_overrides.css",)
        }


# @admin.register(EventCategory)
# class EventCategoryAdmin(admin.ModelAdmin):
#     pass
