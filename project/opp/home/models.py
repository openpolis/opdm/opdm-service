from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils.safestring import mark_safe
from popolo.behaviors.models import Timestampable
from popolo.models import Organization, KeyEvent, Classification
import datetime
from ckeditor.fields import RichTextField

from project.calendars.models import CalendarDaily

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


# class EventCategory(Timestampable, models.Model):
#     is_key_event = models.BooleanField()
#     description = models.CharField(max_length=256)
#     code = models.CharField(max_length=10)


class Event(models.Model):

    date = models.ForeignKey(
        to=CalendarDaily,
        related_name='opp_events',
        on_delete=models.PROTECT)

    is_key_event = models.BooleanField(default=False)
    title = RichTextField(
    )
    description = RichTextField(null=True, blank=True)
    category = models.ForeignKey(
        Classification,
        related_name="opp_events",
        limit_choices_to={"scheme": 'OPP_EVENTS_LOG'},
        on_delete=models.CASCADE,
        help_text="Categoria"
    )
    sub_category = models.TextField(blank=True, null=True)

    branch = models.CharField(
        choices=[('C', 'Camera'), ('S', 'Senato')],
        blank=True, null=True,
        max_length=1

    )

    class Meta:
        ordering = ('-date', '-id')



    def __str__(self):
        return mark_safe(self.title)


class EventSubject(models.Model):

    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="subjects")
    subject_ct = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name="opp_events_subjects",
                                   blank=True, null=True,
                                   limit_choices_to={'app_label__in': [
                                       'popolo',
                                       'votes',
                                       'acts',
                                       'gov',
                                       'parl',
                                   ]})
    subject_id = models.PositiveIntegerField(blank=True, null=True)
    subject = GenericForeignKey('subject_ct', 'subject_id')


class Link(Timestampable, models.Model):
    """
    A URL, with a title for referring to linked entities
    """

    class Meta:
        verbose_name_plural = "Collegamenti"
        unique_together = ("url", "title")

    url = models.URLField(verbose_name="url", max_length=350, help_text="La URL del link")

    title = models.CharField(
        max_length=256, help_text="Una breve descrizione del link", blank=True, null=True
    )

    description = RichTextField(null=True, blank=True)

    date = models.DateTimeField(
        blank=True, null=True,
        verbose_name="Orario di publicazione",
        help_text="Data e ora di pubblicazione"
    )
    image = models.URLField(
        blank=True, null=True,
        max_length=350, help_text="La URL dell'immagine"
    )

    def __str__(self) -> str:
        return f"{self.title}"
