from django.apps import AppConfig


class HomeConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'project.opp.home'
    def ready(self):
        # Implicitly connect a signal handlers decorated with @receiver.
        from project.opp.home import signals  # noqa
