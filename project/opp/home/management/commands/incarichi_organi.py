from django.contrib.contenttypes.models import ContentType
from django.db.models import Count, Q, F
from django.contrib.postgres.aggregates import ArrayAgg
from popolo.models import KeyEvent, Membership as OPDMMembership, Classification

from project.calendars.models import CalendarDaily
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand


today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')

def previous_role(membership):
    check_date = datetime.datetime.strptime(membership.start_date, '%Y-%m-%d') - datetime.timedelta(days=1)
    previous_memb = OPDMMembership.objects.filter(
        (Q(end_date=check_date.strftime('%Y-%m-%d')) | Q(end_date=membership.start_date)),
        person=membership.person, organization=membership.organization).first()
    return previous_memb

def check_previous_role_comp(membership):
    previous_memb = previous_role(membership)
    if previous_memb and previous_memb.role != membership.role:
        if not 'componente' in membership.role.lower():
            return previous_memb
        else:
            return OPDMMembership.objects.none()
    return OPDMMembership.objects.none()

def check_previous_role(membership):
    previous_memb = previous_role(membership)
    if previous_memb and previous_memb.role != membership.role:
        if not 'componente' in membership.role.lower():
            return True, True, membership.id
        else:
            return True, False, membership.id
    return False, False, False


def check_previous_role_ends(membership):
    check_date = datetime.datetime.strptime(membership.end_date, '%Y-%m-%d') + datetime.timedelta(days=1)
    next_memb = OPDMMembership.objects.filter((Q(start_date=check_date.strftime('%Y-%m-%d')) | Q(start_date=membership.end_date)),
                                         person=membership.person, organization=membership.organization).first()
    if next_memb and next_memb.role != membership.role:
        if not 'componente' in membership.role.lower():
            return True, True, membership.id
        else:
            return True, False, membership.id

    return False, False, membership.id

def get_routings(membership):
    branch=None
    routing_name = '#'
    if membership.organization.parent and 'Senato' in membership.organization.parent.name:
        branch = 'S'
        routing_name = 'senato'
    elif membership.organization.parent and 'Camera' in membership.organization.parent.name:
        branch = 'C'
        routing_name = 'camera'
    if 'consiglio di presidenza' in membership.organization.name.lower() or 'ufficio di presidenza' in membership.organization.name.lower() \
        or 'ufficio provvisorio di presidenza' in membership.organization.name.lower():
        organo_routing = f'{routing_name}/presidenza'
    elif ('commissione' in membership.organization.name.lower()
          or 'comitato' in membership.organization.name.lower()
          or membership.organization.name.lower().startswith('giunta')) and branch:
        organo_routing = f'{routing_name}/commissioni_e_giunte'
    elif (membership.organization.parent and membership.organization.parent.name == 'Parlamento Italiano' and (
        'comitato' in membership.organization.name.lower()
        or 'commissione' in membership.organization.name.lower())
          or membership.organization.name.lower().startswith('delegazione')
          or membership.organization.name.lower().startswith('assemblea parlamentare dell\'unione')

    ):
        organo_routing = 'bicamerali/commissioni'
    else:
        organo_routing = '#'
    return (routing_name, organo_routing, branch)

def create_event_single_membership(membership):
    organo = membership.organization.name.split('Senato')[0].split('Camera')[0].strip()

    routing, organo_routing, branch = get_routings(membership)

    other_roles = OPDMMembership.objects.filter(organization=membership.organization, start_date=membership.start_date, person=membership.person).exclude(role__icontains='Componente')
    as_entering_role = ''
    if other_roles.first():
        as_entering_role = f" come {other_roles.first().role.split(' ')[0].lower()}"

    title = f"<a href='/persone/{membership.person.slug}'>{membership.person.name}</a> " \
            f"è entrat{'a' if membership.person.gender == 'F' else 'o'} a far parte dell'organo " \
            f"<a href='/organi_parlamentari/{organo_routing}'>{organo}</a>{as_entering_role}"
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio ruolo organo',
                                                                   code=8)
    event, created = Event.objects.get_or_create(
        category=classification,
        subjects__subject_ct=ContentType.objects.get_for_model(membership.person),
        subjects__subject_id=membership.person.id,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{membership.role.split(' ')[0]}-{membership.organization.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': None,
            'branch': branch
        }
    )
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.organization.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.organization))
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.person.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.person))

def create_event_list_membership(memberships):
    membership = memberships.first()
    organo = membership.organization.name.split('Senato')[0].split('Camera')[0].strip()
    routing, organo_routing, branch = get_routings(membership)


    persons_ref = []
    for x in memberships:
        other_roles = OPDMMembership.objects.filter(organization=x.organization,
                                                    start_date=x.start_date, person=x.person).exclude(
            role__icontains='Componente')
        as_entering_role = ''
        if other_roles.first():
            as_entering_role = f" (come {other_roles.first().role.split(' ')[0].lower()})"
        persons_ref.append(f"<a href='/persone/{x.person.slug}'>{x.person.name}</a>{as_entering_role}")

    persons_ref = ', '.join(persons_ref)
    title = persons_ref + f" sono entrati a far parte dell'organo " \
            f"<a href='/organi_parlamentari/{organo_routing}'>{organo}</a>"
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio ruolo organo',
                                                                   code=8)
    event, created = Event.objects.update_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{membership.organization.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': None,
            'branch': branch
        }
    )
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.organization.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.organization))
    for i in memberships:
        EventSubject.objects.get_or_create(event=event,
                                           subject_id=i.person.id,
                                           subject_ct=ContentType.objects.get_for_model(i.person))

def create_event_list_distinct_membership(memberships):
    membership = memberships.first()
    # check_date = datetime.datetime.strptime(membership.start_date, '%Y-%m-%d') - datetime.timedelta(days=1)
    # organo = membership.organization.name.split('Senato')[0].split('Camera')[0].strip()

    branch = None
    # routing, organo_routing = get_routings(membership)

    persons_ref = []
    organi_list = []
    for x in memberships:
        other_roles = OPDMMembership.objects.filter(organization=x.organization,
                                                    start_date=x.start_date, person=x.person).exclude(
            role__icontains='Componente')
        as_entering_role = ''
        if other_roles.first():
            as_entering_role = f" (come {other_roles.first().role.split(' ')[0].lower()})"
        organo = x.organization.name.split('Senato')[0].split('Camera')[0].strip()
        routing, organo_routing, branch = get_routings(x)
        persons_ref.append(f"<a href='/persone/{x.person.slug}'>{x.person.name}</a> in "
                            f"<a href='/organi_parlamentari/{organo_routing}'>{organo}</a>{as_entering_role}")
        organi_list.append(organo)
    title = f"{memberships.count()} persone sono entrate a far parte di organi parlamentari ({membership.organization.classification.lower()})."

    description = ', '.join(persons_ref)
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio ruolo organo',
                                                                   code=8)
    event, created = Event.objects.update_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{','.join(organi_list)}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': description,
            'branch': branch
        }
    )

    for i in memberships:
        EventSubject.objects.get_or_create(event=event,
                                           subject_id=i.person.id,
                                           subject_ct=ContentType.objects.get_for_model(i.person))

        EventSubject.objects.get_or_create(event=event,
                                           subject_id=i.organization.id,
                                           subject_ct=ContentType.objects.get_for_model(i.organization))

def create_event_single_role_membership(membership):
    organo = membership.organization.name.split('Senato')[0].split('Camera')[0].strip()

    branch = None
    routing, organo_routing, branch = get_routings(membership)

    title = f"<a href='/persone/{membership.person.slug}'>{membership.person.name}</a> " \
            f"è diventat{'a' if membership.person.gender == 'F' else 'o'} {membership.role.split(' ')[0].lower().strip()} in " \
            f"<a href='/organi_parlamentari/{organo_routing}'>{organo}</a>"
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio ruolo organo',
                                                                   code=8)

    event, created = Event.objects.update_or_create(
        category=classification,
        subjects__subject_ct=ContentType.objects.get_for_model(membership.person),
        subjects__subject_id=membership.person.id,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{membership.id}-{membership.organization.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': None,
            'branch': branch
        }
    )
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.organization.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.organization))
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.person.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.person))

def create_event_list_role_membership(memberships):

    def _description_detail(membership):
        prev_role = check_previous_role_comp(membership)
        if prev_role:
            return f"da {prev_role.role.split(' ')[0].lower().strip()} a {membership.role.split(' ')[0].lower().strip()}"

        return f"come {membership.role.split(' ')[0].lower().strip()}"
    membership = memberships.first()
    organo = membership.organization.name.split('Senato')[0].split('Camera')[0].strip()

    routing, organo_routing, branch = get_routings(membership)
    title = f"{memberships.count()} nuovi ruoli all\'interno dell\'organo " \
            f"<a href='/organi_parlamentari/{organo_routing}'>{organo}</a>."

    description = ', '.join([f"<a href='/persone/{x.person.slug}'>{x.person.name}</a> {_description_detail(x)}" for x in memberships])

    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio ruolo organo',
                                                                   code=8)

    event, created = Event.objects.update_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{membership.organization.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': description,
            'branch': branch
        }
    )
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.organization.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.organization))

    for i in memberships:
        EventSubject.objects.get_or_create(event=event,
                                           subject_id=i.person.id,
                                           subject_ct=ContentType.objects.get_for_model(i.person))


def create_event_single_role_membership_ends(membership):
    if membership.end_date == membership.organization.end_date:
        return

    organo = membership.organization.name.split('Senato')[0].split('Camera')[0].strip()

    branch = None
    routing, organo_routing, branch = get_routings(membership)

    title = f"<a href='/persone/{membership.person.slug}'>{membership.person.name}</a> " \
            f"non fa più parte dell\'organo " \
            f"<a href='/organi_parlamentari/{organo_routing}'>{organo}</a>"
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio ruolo organo',
                                                                   code=8)

    event, created = Event.objects.update_or_create(
        category=classification,
        subjects__subject_ct=ContentType.objects.get_for_model(membership.person),
        subjects__subject_id=membership.person.id,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{membership.id}-{membership.organization.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': None,
            'branch': branch
        }
    )
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.organization.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.organization))
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.person.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.person))


def create_event_list_role_membership_ends(memberships):

    membership = memberships.first()
    organization = membership.organization
    if membership.end_date == organization.end_date:
        return
    organo = membership.organization.name.split('Senato')[0].split('Camera')[0].strip()

    routing, organo_routing, branch = get_routings(membership)
    title = f"{memberships.count()} persone non fanno più parte dell\'organo " \
            f"<a href='/organi_parlamentari/{organo_routing}'>{organo}</a>."

    description = ', '.join([f"<a href='/persone/{x.person.slug}'>{x.person.name}</a>" for x in memberships])

    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Fine ruolo organo',
                                                                   code=9)

    event, created = Event.objects.update_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{membership.organization.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': description,
            'branch': branch
        }
    )
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.organization.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.organization))

    for i in memberships:
        EventSubject.objects.get_or_create(event=event,
                                           subject_id=i.person.id,
                                           subject_ct=ContentType.objects.get_for_model(i.person))



class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)

        memberships = OPDMMembership.objects.exclude(
            organization__classification__icontains='Ministero') \
            .exclude(
            organization__classification__icontains='Presidenza del Consiglio') \
            .exclude(
            role__in=['Deputato', 'Senatore', 'Membro gruppo politico in assemblea elettiva']) \
            .exclude(
            role__istartswith='Capogruppo') \
            .filter(
            organization__classification__in=[
                'Giunta parlamentare',
                'Commissione/comitato bicamerale parlamentare',
                'Organo di presidenza parlamentare',
                'Commissione permanente parlamentare',
                'Commissione speciale/straordinaria parlamentare',
                'Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza',
                'Commissione d\'inchiesta parlamentare monocamerale/bicamerale',
                'Delegazione parlamentare presso assemblea internazionale']) \
            .filter(start_date__gte=ke.start_date, start_date__lte=(ke.end_date or today)).exclude(start_date=F('end_date'))

        memb_list = []
        memb_list_to_exclude = []
        for i in memberships:
            existing, to_include, id = check_previous_role(i)
            if existing and to_include:
                memb_list.append(id)
            elif existing and not to_include:
                memb_list_to_exclude.append(id)
        memberships_agg_roles = memberships.filter(id__in=memb_list).annotate(org_class=F('organization__classification')).values(
            'start_date', 'organization', 'org_class').annotate(c=Count('*'), ids=ArrayAgg('id'))
        for i in memberships_agg_roles:
            if i.get('c') == 1:
                create_event_single_role_membership(membership=memberships.get(id__in=i['ids']))
            else:
                # pass
                create_event_list_role_membership(memberships=memberships.filter(id__in=i['ids']))

        memberships_detail = memberships.exclude(id__in=memb_list+memb_list_to_exclude).annotate(org_parent=F('organization__parent'),org_class=F('organization__classification'))
        memberships_agg = memberships_detail.values('start_date', 'org_class', 'org_parent').annotate(cnt_org=Count('organization', distinct=True),c=Count('*'), ids=ArrayAgg('id'),
                                                                                                                                                                                      ids_org=ArrayAgg('organization_id', distinct=True))
        memberships_agg_detail = memberships_detail.values('start_date', 'organization', 'org_class').annotate(c=Count('*'), ids=ArrayAgg('id'))
        for i in memberships_agg:
            if i.get('c') == 1:
                create_event_single_membership(membership=memberships_detail.get(id__in=i['ids']))
            elif i.get('cnt_org') == 1:
                # pass
                create_event_list_membership(memberships=memberships_detail.filter(id__in=i['ids']))
            elif i.get('cnt_org') != i.get('c'):
                for org in i.get('ids_org'):
                    j = memberships_agg_detail.get(organization_id=org,start_date=i.get('start_date'))
                    if j.get('c') == 1:
                        create_event_single_membership(membership=memberships_detail.get(id__in=j['ids']))
                    else:
                        create_event_list_membership(memberships=memberships_detail.filter(id__in=j['ids']))

            elif  i.get('cnt_org') == i.get('c'):
                create_event_list_distinct_membership(memberships=memberships_detail.filter(id__in=i['ids']))

        memberships_ends = memberships.filter(end_date__isnull=False)

        memb_list_ends = []
        memb_list_to_exclude_ends = []
        for i in memberships_ends:
            existing, to_include, id = check_previous_role_ends(i)
            if not existing:
                memb_list_ends.append(id)
            else:
                memb_list_to_exclude_ends.append(i)

        memberships_agg_roles_ends = memberships_ends.filter(id__in=memb_list_ends).annotate(org_class=F('organization__classification')).values(
            'end_date', 'organization', 'org_class').annotate(c=Count('*'), ids=ArrayAgg('id'))

        for i in memberships_agg_roles_ends:
            if i.get('c') == 1:
                create_event_single_role_membership_ends(membership=memberships_ends.get(id__in=i['ids']))
            else:
                # pass
                create_event_list_role_membership_ends(memberships=memberships_ends.filter(id__in=i['ids']))
