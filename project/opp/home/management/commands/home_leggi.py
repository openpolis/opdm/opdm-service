from django.contrib.contenttypes.models import ContentType
from django.db.models import F, Min
from popolo.models import Classification

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand


today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


def parse_days(days):
    import math
    days_p = days
    res = []
    if (years := math.floor(days_p / 365)):
        if years > 1:
            res.append(f"{years} anni")
        else:
            res.append(f"{years} anno")
    if (months := math.floor((days_p % 365) / 30)):
        if months > 1:
            res.append(f"{months} mesi")
        else:
            res.append(f"{months} mese")
    if len(res) == 0:
        return f"{days_p} giorni"

    elif len(res) == 1:
        return ' '.join(res)

    else:
        all_but_last = ' '.join(res[:-1])
        last = res[-1]
        return ' e '.join([all_but_last, last])

class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        bills = Bill.objects.by_legislature(leg)
        bills_law = bills.filter(steps__phase__phase__in=['Approvato definitivamente legge', 'Approvato definitivamente non ancora pubblicato'])\
            .exclude(steps__phase__phase__in=['Approvato con modificazioni'])\
            .annotate(min_status_date=Min(F('steps__status_date')))\
            .values('id', 'min_status_date').distinct().order_by('min_status_date')
        cnt_gov=0
        cnt_parl=0
        cnt_minotauro=0

        for j in bills_law:
            i = Bill.objects.get(id=j['id'])
            if i.initiative == Bill.GOVERNMENT:
                cnt_gov+=1
            else:
                cnt_parl+=1
            voting_final = i.votings.filter(is_final=True).first()
            description = ''


            title = f"È stata approvata la legge " \
                    f"<a href='/attivita_legislativa/disegni_di_legge/{i.slug}'>{i.public_title or i.title}</a>" \
                    f" di iniziativa {i.get_initiative_display().lower()},"

            if i.is_law:
                title += f" pubblicata in Gazzetta Ufficiale il {i.law_publication_date.strftime('%d/%m/%Y')}."
            else:
                title += f" da pubblicare ancora in Gazzetta Ufficiale."
            if i.type == Bill.DL_CONVERSION:
                dl = GovDecree.objects.get(id=i.parliament_iter['detail'][0]['previous_codes'][0]['id'])
                description += f"È la conversione del decreto legge <a href='attivita_legislativa/decreti_legge/{dl.slug}'>{dl.title}</a>"
                if dl.is_minotaurus:
                    cnt_minotauro += 1

                    description += f", cosidetto <a href='/glossario/'>'minotauro'</a>, sono {cnt_minotauro} i decreti dello stesso tipo" \
                                   f" approvati da questo Governo"
                description += '.<br>'

            tot = cnt_gov+cnt_parl
            gov_media = round(100*cnt_gov/tot, 2)
            gov_media = "{:.2f}".format(gov_media).replace('.', ',')
            parl_media = round(100*cnt_parl/tot, 2)
            parl_media = "{:.2f}".format(parl_media).replace('.', ',')
            if voting_final:
                description += (f"\n<a target='_blank' href='votazioni/{voting_final.identifier}'>Votazione finale</a> approvata con "
                               f"<strong>{voting_final.n_margin} voti di scarto</strong>.")
            elif i.is_final_by_commission:
                description += (
                    f"\nAtto approvato in <strong>commissione deliberante</strong>.")
            elif i.is_final_by_hands_voting:
                description += (
                    f"\nVotazione finale avvenuta per <strong>alzata di mano</strong>.")
            description +=f"\n Sono così <strong>{tot}</strong> le leggi approvate in questa legislatura di cui" \
                          f" <strong>{cnt_gov}</strong> di iniziativa governativa, pari al <strong>{gov_media}%</strong> " \
                          f"e <strong>{cnt_parl}</strong> di iniziativa parlamentare, pari al <strong>{parl_media}%</strong> "
            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='Approvazione legge',
                                                                           code=10)
            event, created = Event.objects.update_or_create(
                category=classification,

                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                date=CalendarDaily.objects.get(date=j['min_status_date']),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': None
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))

            if voting_final:
                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=voting_final.id,
                                                   subject_ct=ContentType.objects.get_for_model(voting_final))
