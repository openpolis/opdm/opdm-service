import pandas as pd
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import F, Value, Min, Q, DateField, Window, Count
from django.db.models.functions import Coalesce, RowNumber
from popolo.models import KeyEvent, Membership as OPDMMembership, Classification, Person

from project.calendars.models import CalendarDaily
from project.opp.home.models import Event, EventSubject
from project.opp.parl.models import Assembly
import datetime
from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.models import Membership, MembershipGroup, Group

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')

def get_role(role):
    if 'segretario d\'aula' in role.lower():
        return 'segretario d\'aula'
    else:
        return role.split(' ')[0].lower()

def create_event_single_membership(membership):
    if 'Senato' in membership.organization.parent.name:
        branch = 'S'
        routing_name = 'senato'
    else:
        branch = 'C'
        routing_name = 'camera'
    role = membership.role.split('gruppo')[0].strip()
    group = Group.objects.get(organization=membership.organization.id)

    title = f"<a href='/persone/{membership.person.slug}'>{membership.person.name}</a> " \
            f"è diventat{'a' if membership.person.gender == 'F' else 'o'} {role.lower()} del gruppo " \
            f"<a href='/organi_parlamentari/{routing_name}/gruppi_parlamentari/{group.acronym}'>{group.name_compact}</a>."
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio ruolo nel gruppo',
                                                                   code=7)
    event, created = Event.objects.get_or_create(
        category=classification,
        subjects__subject_ct=ContentType.objects.get_for_model(membership.person),
        subjects__subject_id=membership.person.id,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{membership.role.split(' ')[0]}-{group.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': None,
            'branch': branch
        }
    )

    EventSubject.objects.get_or_create(event=event,
                                       subject_id=group.id,
                                       subject_ct=ContentType.objects.get_for_model(group))
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.person.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.person))
def create_event_list_membership(memberships):
    membership = memberships.first()
    if 'Senato' in membership.organization.parent.name:
        branch = 'S'
        routing_name = 'senato'
    else:
        branch = 'C'
        routing_name = 'camera'
    # role = membership.role.split('gruppo')[0].strip()
    group = Group.objects.get(organization=membership.organization.id)

    title = f"{memberships.count()} nuovi ruoli all\'interno del gruppo " \
            f"<a href='/organi_parlamentari/{routing_name}/gruppi_parlamentari/{group.acronym}'>{group.name_compact}</a>."
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio ruolo nel gruppo',
                                                                   code=7)

    description = ', '.join([f"<a href='/persone/{x.person.slug}'>{x.person.name}</a> come {get_role(x.role)}" for x in memberships.order_by('person__family_name')])
    event, created = Event.objects.get_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=f"{get_role(membership.role)}-{group.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': description,
            'branch': branch
        }
    )

    EventSubject.objects.get_or_create(event=event,
                                       subject_id=group.id,
                                       subject_ct=ContentType.objects.get_for_model(group))
    for i in memberships:
        EventSubject.objects.get_or_create(event=event,
                                           subject_id=i.person.id,
                                           subject_ct=ContentType.objects.get_for_model(i.person))

def create_closing_event_single_membership(membership):
    if 'Senato' in membership.organization.parent.name:
        branch = 'S'
        routing_name = 'senato'
    else:
        branch = 'C'
        routing_name = 'camera'
    role = membership.role.split('gruppo')[0].strip()
    group = Group.objects.get(organization=membership.organization.id)

    title = f"<a href='/persone/{membership.person.slug}'>{membership.person.name}</a> " \
            f"ha cessato di essere {role.lower()} di " \
            f"<a href='/organi_parlamentari/{routing_name}/gruppi_parlamentari/{group.acronym}'>{group.name_compact}</a>."
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Fine ruolo nel gruppo',
                                                                   code=6)
    event, created = Event.objects.get_or_create(
        category=classification,
        subjects__subject_ct=ContentType.objects.get_for_model(membership.person),
        subjects__subject_id=membership.person.id,
        date=CalendarDaily.objects.get(date=membership.end_date),
        sub_category=f"{membership.role.split(' ')[0]}-{group.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': None,
            'branch': branch
        }
    )

    EventSubject.objects.get_or_create(event=event,
                                       subject_id=group.id,
                                       subject_ct=ContentType.objects.get_for_model(group))
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.person.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.person))
def create_closing_event_list_membership(memberships):
    membership = memberships.first()
    if 'Senato' in membership.organization.parent.name:
        branch = 'S'
        routing_name = 'senato'
    else:
        branch = 'C'
        routing_name = 'camera'
    # role = membership.role.split('gruppo')[0].strip()
    group = Group.objects.get(organization=membership.organization.id)

    title = f"{memberships.count()} ruoli cessati all\'interno del gruppo " \
            f"<a href='/organi_parlamentari/{routing_name}/gruppi_parlamentari/{group.acronym}'>{group.name_compact}</a>."
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Fine ruolo nel gruppo',
                                                                   code=6)

    description = ', '.join([f"<a href='/persone/{x.person.slug}'>{x.person.name}</a> come {get_role(x.role)}" for x in memberships.order_by('person__family_name')])
    event, created = Event.objects.get_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=membership.end_date),
        sub_category=f"{get_role(membership.role)}-{group.id}",
        defaults={
            'is_key_event': False,
            'title': title,
            'description': description,
            'branch': branch
        }
    )

    EventSubject.objects.get_or_create(event=event,
                                       subject_id=group.id,
                                       subject_ct=ContentType.objects.get_for_model(group))
    for i in memberships:
        EventSubject.objects.get_or_create(event=event,
                                           subject_id=i.person.id,
                                           subject_ct=ContentType.objects.get_for_model(i.person))

class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        new_date = datetime.datetime.strptime(ke.start_date, '%Y-%m-%d') + datetime.timedelta(days=15)
        new_date = new_date.strftime('%Y-%m-%d')
        membrigruppi = MembershipGroup.objects.filter(group__legislature=ke).exclude(group__acronym='Nessun gruppo')
        membrigruppi = membrigruppi.annotate(
            row_number=Window(expression=RowNumber(), partition_by=[F("membership")], order_by=[F("start_date")]))
        df = pd.DataFrame(membrigruppi.annotate(chamber=F('membership__opdm_membership__organization__name'))
                          .values('row_number', 'id', 'start_date', 'chamber'))

        df = df[df['row_number'] > 1]
        df['person'] = df['id'].apply(lambda x: MembershipGroup.objects.get(id=x).membership.opdm_membership.person.id)
        df['to_group'] = df['id'].apply(lambda x: MembershipGroup.objects.get(id=x).group.id)
        df['from_group'] = df['id'].apply(lambda x: MembershipGroup.objects.get(id=x).previous_group.group.id)
        df_senato = df[df['chamber'].str.contains('Senato')]
        df_camera = df[df['chamber'].str.contains('Camera')]

        df_senato = df_senato.sort_values(by='start_date').reset_index()
        df_camera = df_camera.sort_values(by='start_date').reset_index()
        ## Cambio gruppo
        agg_func = lambda x: (x.count(), list(x))
        df_senato_group_by = df_senato.groupby(by=['start_date', 'from_group', 'to_group'])['person'].agg(agg_func)
        cnt_cambi_senato = 0
        for index, row in pd.DataFrame(df_senato_group_by).iterrows():
            start_date, from_group_id, to_group_id = index
            from_group = Group.objects.get(id=from_group_id)
            to_group = Group.objects.get(id=to_group_id)

            cnt, persons = row['person']
            persons = Person.objects.filter(id__in=persons)
            title = ', '.join([f"<a href='/persone/{x.slug}'>{x.name}</a>" for x in persons.order_by('family_name')])
            if cnt == 1:
                title += f" è entrat{'a' if persons[0].gender == 'F' else 'o'} " \
                       f"a far parte del gruppo " \
                        f"<a target='_blank' href='/organi_parlamentari/senato/gruppi_parlamentari/{to_group.acronym}'>{to_group.name_compact}</a> " \
                       f"proveniente dal gruppo <a target='_blank' href='/organi_parlamentari/senato/gruppi_parlamentari/{from_group.acronym}'>{from_group.name_compact}</a"
                cnt_cambi_senato += cnt
                description = f"È il <a target='_blank' href='/organi_parlamentari/senato/gruppi_parlamentari'>{cnt_cambi_senato}°</a> cambio di gruppo in questa legislatura al Senato."
            else:
                title += f" sono entrati " \
                         f"a far parte del gruppo " \
                         f"<a target='_blank' href='/organi_parlamentari/senato/gruppi_parlamentari/{to_group.acronym}'>{to_group.name_compact}</a> " \
                         f"provenienti dal gruppo <a target='_blank' href='/organi_parlamentari/senato/gruppi_parlamentari/{from_group.acronym}'>{from_group.name_compact}</a>"
                cnt_cambi_senato += cnt
                description = f"Sono così <a target='_blank' href='/organi_parlamentari/senato/gruppi_parlamentari'>{cnt_cambi_senato}</a> i cambi di gruppo in questa legislatura al Senato."

            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='Cambio di gruppo',
                                                                           code=5)
            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=start_date),
                sub_category=f"{from_group.id}-{to_group.id}",
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': 'S'
                }
            )
            for i in persons:
                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=i.id,
                                                   subject_ct=ContentType.objects.get_for_model(i))

            for i in [to_group, from_group]:
                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=i.id,
                                                   subject_ct=ContentType.objects.get_for_model(i))


        df_camera_group_by = df_camera.groupby(by=['start_date', 'from_group', 'to_group'])['person'].agg(agg_func)
        cnt_cambi_camera = 0
        for index, row in pd.DataFrame(df_camera_group_by).iterrows():
            start_date, from_group_id, to_group_id = index
            from_group = Group.objects.get(id=from_group_id)
            to_group = Group.objects.get(id=to_group_id)

            cnt, persons = row['person']
            persons = Person.objects.filter(id__in=persons)

            title = ', '.join([f"<a href='/persone/{x.slug}'>{x.name}</a>" for x in persons.order_by('family_name')])
            if cnt == 1:
                title += f" è entrat{'a' if persons[0].gender == 'F' else 'o'} " \
                         f"a far parte del gruppo " \
                         f"<a target='_blank' href='/organi_parlamentari/camera/gruppi_parlamentari/{to_group.acronym}'>{to_group.name_compact}</a> " \
                         f"proveniente dal gruppo <a target='_blank' href='/organi_parlamentari/camera/gruppi_parlamentari/{from_group.acronym}'>{from_group.name_compact}</a"
                cnt_cambi_camera += cnt
                description = f"È il <a target='_blank' href='/organi_parlamentari/camera/gruppi_parlamentari'>{cnt_cambi_camera}°</a> cambio di gruppo in questa legislatura alla Camera."
            else:
                title += f" sono entrati " \
                         f"a far parte del gruppo " \
                         f"<a target='_blank' href='/organi_parlamentari/camera/gruppi_parlamentari/{to_group.acronym}'>{to_group.name_compact}</a> " \
                         f"provenienti dal gruppo <a target='_blank' href='/organi_parlamentari/camera/gruppi_parlamentari/{from_group.acronym}'>{from_group.name_compact}</a>"
                cnt_cambi_camera += cnt
                description = f"Sono così <a target='_blank' href='/organi_parlamentari/camera/gruppi_parlamentari'>{cnt_cambi_camera}</a> i cambi di gruppo in questa legislatura alla Camera."

            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='Cambio di gruppo',
                                                                           code=5)
            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=start_date),
                sub_category=f"{from_group.id}-{to_group.id}",
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': 'C'
                }
            )
            for i in persons:
                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=i.id,
                                                   subject_ct=ContentType.objects.get_for_model(i))
            for i in [to_group, from_group]:
                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=i.id,
                                                   subject_ct=ContentType.objects.get_for_model(i))

        group_roles = OPDMMembership.objects.filter(organization__classification='Gruppo politico in assemblea elettiva',
                                      organization__parent__key_events__key_event=ke)\
            .exclude(role='Membro gruppo politico in assemblea elettiva')
        ## Caso fine ruolo gruppo
        group_roles_agg = group_roles.filter(end_date__isnull=False).values('end_date', 'organization').annotate(c=Count('*'), ids=ArrayAgg('id')).order_by('-end_date')
        for i in group_roles_agg:
            if i.get('c') == 1:
                create_closing_event_single_membership(membership=group_roles.get(id__in=i['ids']))
            else:
                create_closing_event_list_membership(memberships=group_roles.filter(id__in=i['ids']))
        ## Cambio ruolo gruppo inizio
        group_roles_agg = group_roles.values('start_date', 'organization').annotate(c=Count('*'),ids=ArrayAgg('id'))
        for i in group_roles_agg:
            if i.get('c') == 1:
                create_event_single_membership(membership=group_roles.get(id__in=i['ids']))
            else:
                create_event_list_membership(memberships=group_roles.filter(id__in=i['ids']))

        membrigruppi_no_group = MembershipGroup.objects.filter(group__legislature=ke).filter(
            group__acronym__in=['Nessun gruppo'])
        for i in membrigruppi_no_group:
            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='Cambio di gruppo',
                                                                           code=5)
            new_date = i.membership.opdm_membership.start_date
            new_date = datetime.datetime.strptime(new_date, '%Y-%m-%d') + datetime.timedelta(days=15)
            new_date = new_date.strftime('%Y-%m-%d')

            if i.start_date > new_date:
                if i.membership.branch=='S':
                    aula = 'senato'
                else:
                    aula = 'camera'
                title = f"<a href='/persone/{i.membership.opdm_membership.person.slug}'>{i.membership.opdm_membership.person.name}</a> è uscit{'a' if  i.membership.opdm_membership.person.gender == 'F' else 'o'} " \
                         f" dal gruppo <a target='_blank' href='/organi_parlamentari/{aula}/gruppi_parlamentari/{i.previous_group.group.acronym}'>{i.previous_group.group.name_compact}</a> senza scegliere ancora un altro gruppo a cui aderire."

                event, created = Event.objects.get_or_create(
                    category=classification,
                    date=CalendarDaily.objects.get(date=i.start_date),
                    sub_category=f"Uscita gruppo senza altro gruppo",
                    defaults={
                        'is_key_event': True,
                        'title': title,
                        'description': None,
                        'branch': 'C'
                    }
                )
