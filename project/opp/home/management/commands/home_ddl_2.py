from django.contrib.contenttypes.models import ContentType
from django.db.models import Q, Max, F
from popolo.models import Classification, KeyEvent

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree, GovBill, IterPhase, Iter
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand
import math

from project.opp.parl.models import Legislature
from project.opp.utils import get_identifier_dlgs, get_type_act, TodayDate

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        ''' Gestione casi approvazione da un ramo'''

        leg = kwargs['legislatura']
        statuses = IterPhase.objects.filter(
            phase__in=['Approvato con modificazioni', 'Approvato'])

        testi_unificati = IterPhase.objects.filter(
            phase__in=['Approvato in testo unificato'])
        bill_testi_unificati = Iter.objects.by_legislature(leg).filter(phase__in=testi_unificati, bill__votings__isnull=False)
        bill_testi_unificati = Iter.objects.filter(id__in = list(set([x.id for x in bill_testi_unificati])))

        iter_bills_unificati = Iter.objects.filter(id__in = [x['max_id'] for x in
                                                             bill_testi_unificati.values('status_date',
                                                                                         'bill__original_title').annotate(max_id=Max(F('id')))])
        iters = Iter.objects.by_legislature(leg).filter(Q(phase__in=statuses)|Q(id__in=iter_bills_unificati.values('id')))

        for iter in iters:
            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='DDL approved',
                                                                           code=24)
            title = f"Il disegno di legge " \
                    f"<a target='_blank' href='/attivita_legislativa/disegni_di_legge/{iter.bill.slug}'>{iter.bill.public_title or iter.bill.title}</a> " \
                    f"è stato {iter.phase.phase.lower()}."
            voting_final = iter.bill.votings.filter(is_final=True).first()
            description = ''
            if voting_final:
                description += (f"<a target='_blank' href='votazioni/{voting_final.identifier}'>Votazione finale</a> approvata con "
                               f"<strong>{voting_final.n_margin} voti di scarto</strong>.\n")

            if iter.bill.type == Bill.COSTITUTIONAL:
                description += ("Per diventare legge è richiesta una procedura aggravata con un doppio voto da "
                                "parte di entrambe le camere ad almeno 3 mesi di distanza l’uno dall’altro.")
                iter_list = iter.bill.parliament_iter['detail']
                approvals = [x for x in iter_list if 'approvato' in x['label'].lower()  and x['date'] <=iter.status_date ]
                description += (f"\nQuesta è l'approvazione numero {len(approvals)}.")
            else:
                description += f"Per diventare legge dovrà essere approvato anche "
                if iter.bill.branch == 'S':
                    description += f" alla Camera."
                else:
                    description += f" al Senato."
            event, created = Event.objects.update_or_create(
                category=classification,
                subjects__subject_ct=ContentType.objects.get_for_model(iter.bill),
                subjects__subject_id=iter.bill.id,
                date=CalendarDaily.objects.get(date=iter.status_date),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': iter.bill.branch
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=iter.bill.id,
                                               subject_ct=ContentType.objects.get_for_model(iter.bill))
            if voting_final:
                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=voting_final.id,
                                                   subject_ct=ContentType.objects.get_for_model(voting_final))
