

from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import Count, Q
from popolo.models import Classification, KeyEvent, Organization

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree, ImplementingDecree
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand
import math

from project.opp.votes.models import Voting, Group

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')

class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']

        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)

        orgs = Organization.objects.filter(classification__in=[
                'Commissione/comitato bicamerale parlamentare',
                'Commissione permanente parlamentare',
                'Commissione speciale/straordinaria parlamentare',
                'Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza',
                'Commissione d\'inchiesta parlamentare monocamerale/bicamerale',
                'Delegazione parlamentare presso assemblea internazionale']) \
            .filter(start_date__gte=ke.start_date, start_date__lte=(ke.end_date or today))


        org_ended = orgs.filter(end_date__isnull=False)

        classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                       descr='Commissione sciolta',
                                                                       code=20)
        for i in org_ended:
            if i.parent and 'Senato' in i.parent.name:
                parent_name = 'Senato'
                branch = 'S'
            elif i.parent and 'Camera' in i.parent.name:
                parent_name = 'Camera'
                branch = 'C'
            else:
                parent_name = 'Bicamerale'
                branch = None
            title = f"La {i.name} si è sciolta ({parent_name})."
            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=i.end_date),
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': '',
                    'branch': branch
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))

        org_started = orgs

        classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                       descr='Commissione formata',
                                                                       code=21)
        for i in org_started:
            if i.parent and 'Senato' in i.parent.name:
                parent_name = 'Senato'
                branch = 'S'
            elif i.parent and 'Camera' in i.parent.name:
                parent_name = 'Camera'
                branch = 'C'
            else:
                parent_name = 'Bicamerale'
                branch = None
            title = f"La {i.name} si è costituita."
            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=i.start_date),
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': '',
                    'branch': branch
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))
