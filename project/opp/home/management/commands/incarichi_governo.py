from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import F, Value, Min, Q, DateField, Count, Func
from django.db.models.functions import Coalesce
from popolo.models import KeyEvent, Membership as OPDMMembership, Classification

from project.calendars.models import CalendarDaily
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand

from project.opp.utils import parse_days

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')

class ExtractFirstWord(Func):
    function = 'SPLIT_PART'
    template = "%(function)s(%(expressions)s, ' ', 1)"


def create_event_single_membership(ke, membership):
    govmemberships_leg_before = OPDMMembership.objects \
        .filter(organization__classification='Governo della Repubblica').filter(
        start_date__lt=ke.start_date).exclude(role__icontains='Segretario Generale')
    title = f"È stat{'a' if membership.person.gender == 'F' else 'o'} nominat{'a' if membership.person.gender == 'F' else 'o'}" \
            f" <a target='_blank' href='/persone/{membership.person.slug}'>{membership.person.name}</a>"
    title += f" come {membership.label}."
    description = f""
    if govmemberships_leg_before.filter(person=membership.person):
        roles = govmemberships_leg_before.filter(person=membership.person).values('organization').distinct().order_by(
            'organization')
        description += f"Aveva già fatto parte di {roles.count()} " \
                       f"Govern{'o' if roles.count() == 1 else 'i'} nella storia della Repubblica."
    else:
        description += f"È la prima volta che entra a far di un Governo della Repubblica."
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio incarico governo',
                                                                   code=4)
    event, created = Event.objects.get_or_create(
        category=classification,
        subjects__subject_ct=ContentType.objects.get_for_model(membership.person),
        subjects__subject_id=membership.person.id,
        date=CalendarDaily.objects.get(date=membership.start_date),
        sub_category=membership.role.split(' ')[0],
        defaults={
            'is_key_event': True,
            'title': title,
            'description': description,
            'branch': None
        }
    )
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=membership.person.id,
                                       subject_ct=ContentType.objects.get_for_model(membership.person))

def create_event_list_membership(ke, memberships, source_row):
    person_ref = [f" <a target='_blank' href='/persone/{x.person.slug}'>{x.person.name}</a>" for x in memberships.order_by('person__family_name')]
    title = f"Sono state nominate {memberships.count()} persone nel ruolo di {source_row['role_master'].lower()}."
    description = ', '.join(person_ref)
    description += f" sono stati nominati nel ruolo di {source_row['role_master'].lower().strip()}."
    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Inizio incarico governo',
                                                                   code=4)
    event, created = Event.objects.get_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=source_row['start_date']),
        sub_category=source_row.get('role_master'),
        defaults={
            'is_key_event': True,
            'title': title,
            'description': description,
            'branch': None
        }
    )
    for i in memberships:
        EventSubject.objects.get_or_create(event=event,
                                           subject_id=i.person.id,
                                           subject_ct=ContentType.objects.get_for_model(i.person))


class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)

        govmemberships_leg = OPDMMembership.objects\
            .filter(
            Q(organization__classification__icontains='ministero') |
            Q(organization__classification__icontains='presidenza del consiglio')).annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))).filter(
                start_date__gte=ke.start_date, end_date_coalesce__lte=(ke.end_date or today_strf)
            ).exclude(role__icontains='Segretario Generale')
        ## Persona cessa incarico governo

        govmemberships_leg_before = OPDMMembership.objects \
            .filter(organization__classification='Governo della Repubblica').filter(
            start_date__lt=ke.start_date).exclude(role__icontains='Segretario Generale')
        fine_incarichi = govmemberships_leg.filter(end_date__isnull=False)
        for i in fine_incarichi:
            if i.person.end_date == i.end_date:
                title = f"È decedut{'a' if i.person.gender=='F' else 'o' } <a target='_blank' href='/persone/{i.person.slug}'>{i.person.name}</a> e decadate da ruolo di {i.role.split(' ')[0].lower()}."
            else:
                title = f"{i.end_reason.title()} di <a target='_blank' href='/persone/{i.person.slug}'>{i.person.name}</a>"
                title += f" da {i.role.split(' ')[0].lower()}."

            start_date = datetime.datetime.strptime(i.start_date, '%Y-%m-%d')
            end_date = datetime.datetime.strptime(i.end_date, '%Y-%m-%d')
            gov = i.label.split('del ')[1]
            gov_name = f"{' '.join(gov.split(' ')[2:])} {gov.split(' ')[0]}"
            description = (f"Ha esercitato il ruolo di {i.label.split('del ')[0].strip()} del governo "
                           f"{gov_name}"
                           f" per {parse_days((end_date-start_date).days)}.")
            if govmemberships_leg_before.filter(person=i.person):
                roles = govmemberships_leg_before.filter(person=i.person).values('organization').distinct().order_by('organization')
                description += f"<br>Aveva già fatto parte di {roles.count()} govern{ 'i' if roles.count()>1 else 'o'} nella storia della Repubblica."
            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='Fine incarico governo',
                                                                           code=3)
            event, created = Event.objects.get_or_create(
                category=classification,
                subjects__subject_ct=ContentType.objects.get_for_model(i.person),
                subjects__subject_id=i.person.id,
                date=CalendarDaily.objects.get(date=i.end_date),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': None
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.person.id,
                                               subject_ct=ContentType.objects.get_for_model(i.person))
        ## Persona inizia incarico governo

        inizio_incarichi = govmemberships_leg
        dates = inizio_incarichi.annotate(role_master=ExtractFirstWord(F('role'))).values('start_date', 'role_master').annotate(
            c=Count('*'), ids=ArrayAgg('id'))
        for i in dates:
            if i.get('c')==1:
                create_event_single_membership(ke=ke,membership=inizio_incarichi.get(id__in=i['ids']))
            else:
                create_event_list_membership(ke=ke, memberships=inizio_incarichi.filter(id__in=i['ids']), source_row=i)
