from django.contrib.contenttypes.models import ContentType
from popolo.models import Classification, KeyEvent

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree, GovBill
from project.opp.gov.models import Government
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand
import math

from project.opp.utils import get_identifier_dlgs, get_type_act



class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']

        bills = Bill.objects.by_legislature(leg)

        bills_gov_no_conv = bills.filter(initiative=Bill.GOVERNMENT).exclude(type=Bill.DL_CONVERSION).exclude(is_ratification=True).order_by('date_presenting')
        list_bills_gov_no_conv_processed = []
        for i in bills_gov_no_conv:
            if not i.is_origin()[0]:
                continue
            list_bills_gov_no_conv_processed.append(i.id)


            gov = Government.objects.get(organization_id=i.gov_at_presenting_date.organization.id)
            title = f"Il governo {gov.name} ha presentato il disegno di legge " \
                    f"<a target='_blank' href='/attivita_legislativa/disegni_di_legge/{i.slug}'>{i.public_title or i.title}</a>."


            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='DDL gov',
                                                                           code=15)

            num_gov = bills_gov_no_conv.filter(id__in=list_bills_gov_no_conv_processed)
            description = f"""Ad esclusione di quelli di conversione di decreti legge e ratifiche, il Governo ha presentato
            {num_gov.count()} disegni di legge."""

            event, created = Event.objects.get_or_create(
                category=classification,
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                date=CalendarDaily.objects.get(date=i.date_presenting),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': i.branch
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.gov_at_presenting_date.id,
                                               subject_ct=ContentType.objects.get_for_model(i.gov_at_presenting_date))
