from django.contrib.contenttypes.models import ContentType
from popolo.models import KeyEvent, Classification

from project.calendars.models import CalendarDaily
from project.opp.acts.models import GovSitting
from project.opp.gov.models import Government
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


def parse_days(days):
    import math
    days_p = days
    res = []
    if (years := math.floor(days_p / 365)):
        if years > 1:
            res.append(f"{years} anni")
        else:
            res.append(f"{years} anno")
    if (months := math.floor((days_p % 365) / 30)):
        if months > 1:
            res.append(f"{months} mesi")
        else:
            res.append(f"{months} mese")
    if len(res) == 0:
        return f"{days_p} giorni"

    elif len(res) == 1:
        return ' '.join(res)

    else:
        all_but_last = ' '.join(res[:-1])
        last = res[-1]
        return ' e '.join([all_but_last, last])

class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        govs = Government.get_governments_by_leg(leg=leg)
        gov_sittings = GovSitting.objects.filter(government__in=govs.values('organization'))
        classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                       descr='Sedute CdM',
                                                                       code=11)
        for i in gov_sittings:
            title = f"Si è svolta la <a href='{i.url}'>Seduta n. {i.number} del Consiglio dei ministri</a>"
            event, created = Event.objects.get_or_create(
                category=classification,
                # object_id=i.id,
                # object_ct = ContentType.objects.get_for_model(i),
                # event_description='Seduta CDM',
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                date=CalendarDaily.objects.get(date=i.starting_datetime.date()),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': None,
                    # 'category': Event.ORGANIZATION,
                    'branch': None
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))

