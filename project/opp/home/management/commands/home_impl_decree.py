

from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import Count
from popolo.models import Classification, KeyEvent

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree, ImplementingDecree
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand
import math

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


def generate_multi_case_impl_decree(i):
    impl_decrees = ImplementingDecree.objects.filter(id__in=i.get('ids'))
    total_resources = sum([(x.get_resources.get('total') or 0) for x in impl_decrees])
    title = f"Sono stati adottati {i.get('c')} decreti attuativi."
    if total_resources:
        total_resources = "{:,}".format(round(total_resources)).replace(',', '.')
        title += f" sbloccando {total_resources} €."


    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Decreto Attuativo adottato',
                                                                   code=16)

    acts_ref = [f" <a target='_blank' href='/attivita_legislativa/decreti_attuativi/{x.id}'>{x.public_title or x.title}</a>" for x in
                  impl_decrees]
    description = ", ".join(acts_ref)

    event, created = Event.objects.get_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=i.get('adoption_date')),
        sub_category='implementing_decrees',
        defaults={
            'is_key_event': True,
            'title': title,
            'description': description,
            'branch': None
        }
    )
    for j in impl_decrees:
        EventSubject.objects.get_or_create(event=event,
                                           subject_id=j.id,
                                           subject_ct=ContentType.objects.get_for_model(j))


def generate_single_case_impl_decree(i):
    impl_decree = ImplementingDecree.objects.get(id__in=i.get('ids'))
    total_resources = impl_decree.get_resources['total'] or 0
    title = f"Il decreto attuativo <a target='_blank' href='/attivita_legislativa/decreti_attuativi/{impl_decree.id}'>{impl_decree.public_title or impl_decree.title}</a>" \
            f" è stato adottato."
    description = ""
    if total_resources:
        total_resources = "{:,}".format(round(total_resources)).replace(',', '.')
        description = f"Questo decreto sblocca {total_resources} €."


    classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                   descr='Decreto Attuativo adottato',
                                                                   code=16)

    event, created = Event.objects.get_or_create(
        category=classification,
        date=CalendarDaily.objects.get(date=i.get('adoption_date')),
        subjects__subject_ct=ContentType.objects.get_for_model(impl_decree),
        subjects__subject_id=impl_decree.id,
        sub_category='implementing_decrees',
        defaults={
            'is_key_event': True,
            'title': title,
            'description': description,
            'branch': None
        }
    )
    EventSubject.objects.get_or_create(event=event,
                                       subject_id=impl_decree.id,
                                       subject_ct=ContentType.objects.get_for_model(impl_decree))


class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        impl_decrees = ImplementingDecree.objects.by_legislature(leg).filter(is_adopted=True)

        impl_decrees_aggr = impl_decrees.values('adoption_date').annotate(c=Count('*'), ids=ArrayAgg('id'))
        for i in impl_decrees_aggr:
            if i.get('c') == 1:
                generate_single_case_impl_decree(i)
            else:
                generate_multi_case_impl_decree(i)
