

from django.contrib.contenttypes.models import ContentType
from popolo.models import Classification, KeyEvent, Organization, Membership as OPDMMembership

from project.calendars.models import CalendarDaily
from project.opp.gov.models import Government
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')

def parse_days(days):
    import math
    days_p = days
    res = []
    if (years := math.floor(days_p / 365)):
        if years > 1:
            res.append(f"{years} anni")
        else:
            res.append(f"{years} anno")
    if (months := math.floor((days_p % 365) / 30)):
        if months > 1:
            res.append(f"{months} mesi")
        else:
            res.append(f"{months} mese")
    if len(res) == 0:
        return f"{days_p} giorni"

    elif len(res) == 1:
        return ' '.join(res)

    else:
        all_but_last = ' '.join(res[:-1])
        last = res[-1]
        return ' e '.join([all_but_last, last])

class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']

        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)

        govs = Government.get_governments_by_leg(leg=leg)

        classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                       descr='Inizio governo',
                                                                       code=22)
        # govs_ended = govs.filter(organization__end_date__isnull=False)
        for i in govs:
            govmemberships_leg_before = OPDMMembership.objects \
                .filter(organization__classification='Governo della Repubblica').filter(
                start_date__lt=ke.start_date).exclude(role__icontains='Segretario Generale')



            pres_cons = i.organization.memberships.get(role__istartswith='Presidente del Consiglio')
            roles = govmemberships_leg_before.filter(person=pres_cons.person).values('organization').distinct().order_by(
                'organization')
            title = f"{pres_cons.person.name} è il nuovo Presidente del Consiglio."
            gov_before=Organization.objects.filter(classification='Governo della Repubblica', start_date__lt=i.organization.start_date).count()
            description = f"Nasce così il {i.organization.name}."#che è il {gov_before+1}° Governo della storia della Repubblica."
            description += f"<br>Aveva già fatto parte di {roles.count()} Governi nella storia della Repubblica."
            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=i.organization.start_date),
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': None
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))

        govs = govs.filter(organization__end_date__isnull=False)

        classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                       descr='Fine governo',
                                                                       code=23)
        for i in govs:
            pres_cons = i.organization.memberships.get(role__istartswith='Presidente del Consiglio')
            title = f"Il presidente del Consiglio {pres_cons.person.name} ha rassegnato le dimissioni."
            start_date = datetime.datetime.strptime(i.organization.start_date, '%Y-%m-%d').date()
            end_date = datetime.datetime.strptime(i.organization.end_date, '%Y-%m-%d').date()

            description = f"Finisce così il {i.organization.name} dopo {parse_days((end_date-start_date).days)}."

            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=i.organization.end_date),
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': None
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))
