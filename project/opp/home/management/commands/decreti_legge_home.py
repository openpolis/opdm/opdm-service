from django.contrib.contenttypes.models import ContentType
from django.db.models import F, Value, Min, Q, DateField
from django.db.models.functions import Coalesce
from popolo.models import KeyEvent, Membership as OPDMMembership

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree
from project.opp.home.models import Event
from project.opp.parl.models import Assembly
import datetime
from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.models import Membership

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)

        govdecrees = GovDecree.objects.by_legislature(leg).order_by('publication_gu_date')

        for i in govdecrees:

            title = f"Il {i.sitting.government.__str__()} ha emanato il decreto legge " \
                    f"<a href='/attivita_legislativa/decreti_legge/{i.slug}'>{i.title}</a> " \
                    f"che dovrà essere convertito in legge dal Parlamento entro il " \
                    f"{i.date_expiring.strftime('%d/%m/%Y')}."
            gov_decrees_by_gov = GovDecree.objects.filter(sitting__government=i.sitting.government).filter(publication_gu_date__lte=i.publication_gu_date)
            start_gov = datetime.datetime.strptime(i.sitting.government.start_date, '%Y-%m-%d').date()
            proxy_months  = max(1, round(((i.publication_gu_date - start_gov).days)/30, 1))

            description = f"Per questo Governo è il decreto legge numero {gov_decrees_by_gov.count()}, con una media mensile" \
                          f" di {round(gov_decrees_by_gov.count()/proxy_months, 2)} decreti legge."
            Event.objects.get_or_create(
                object_id=i.id,
                object_ct = ContentType.objects.get_for_model(i),
                event_description='DL emanato',
                date=CalendarDaily.objects.get(date=i.publication_gu_date),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'category': Event.LAW,
                    'branch': None
                }
            )
            if i.status().get('status') == 'e':
                title = f"Il DL " \
                        f"<a href='/attivita_legislativa/decreti_legge/{i.slug}'>{i.title}</a> " \
                        f"è decaduto."
                        # f"{i.date_expiring.strftime('%d/%m/%Y')}."
                description = f"Era stato pubblicato il {i.publication_gu_date.strftime('%d/%m/%Y')} e non è stato convertito in legge dal Parlamento entro i sessanta giorni previsti dall’art. 77 della Costituzione."
                Event.objects.get_or_create(
                    object_id=i.id,
                    object_ct=ContentType.objects.get_for_model(i),
                    event_description='DL decaduto',
                    date=CalendarDaily.objects.get(date=i.publication_gu_date),
                    defaults={
                        'is_key_event': True,
                        'title': title,
                        'description': description,
                        'category': Event.LAW,
                        'branch': None
                    }
                )

