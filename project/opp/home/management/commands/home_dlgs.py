

from django.contrib.contenttypes.models import ContentType
from popolo.models import Classification, KeyEvent

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree, GovBill
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand
import math

from project.opp.utils import get_identifier_dlgs, get_type_act

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        gov_bills = GovBill.objects.by_legislature(leg)
        list_gov_bills_processed = []
        for i in gov_bills:
            list_gov_bills_processed.append(i.id)

            title = f"Il {i.decision_sitting.government.name} ha emanato il decreto legislativo " \
                    f"<a href='/attivita_legislativa/decreti_legislativi/{i.slug}'>{i.public_title or i.title}</a>."


            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='DLGS Emanato',
                                                                           code=14)

            gov_bills_before = gov_bills.filter(decision_sitting__starting_datetime__lt=i.decision_sitting.starting_datetime)
            delegation_laws = i.delegation_law
            delegation_laws_list = []
            description=''
            if delegation_laws:
                for del_law in delegation_laws:
                    identifier = get_identifier_dlgs(del_law, leg)
                    type_act = get_type_act(del_law, leg)
                    if type_act == 'prev_act':
                        dl_law_string = f"{identifier}"
                    else:
                        dl_law_string = f"<a href='/attivita_legislativa/disegni_di_legge/{del_law.slug}'>{identifier}</a>"
                    delegation_laws_list.append(dl_law_string)
                delegation_laws_formatted = ', '.join(delegation_laws_list)
                description += f"""Deriva dalla legge delega {delegation_laws_formatted}.<br>"""
            num_gov = gov_bills_before.count()+1
            if gov_bills.filter(decision_sitting__starting_datetime=i.decision_sitting.starting_datetime).exclude(id=i.id).filter(id__in=list_gov_bills_processed):
                num_gov += gov_bills.filter(decision_sitting__starting_datetime=i.decision_sitting.starting_datetime).exclude(id=i.id).filter(id__in=list_gov_bills_processed).count()
            description += f"""È il {num_gov}° decreto legislativo emanato in questa legislatura."""

            event, created = Event.objects.get_or_create(
                category=classification,
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                date=CalendarDaily.objects.get(date=i.decision_sitting.starting_datetime.date()),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': None
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))


            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.decision_sitting.government.id,
                                               subject_ct=ContentType.objects.get_for_model(i.decision_sitting.government))
