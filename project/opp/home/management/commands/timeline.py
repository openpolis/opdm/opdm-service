import base64
import json

import requests
from taskmanager.management.base import LoggingBaseCommand
from django.core.management import call_command
import environ
import time
import logging

from project.opp.parl.models import Legislature


env = environ.Env()


class Command(LoggingBaseCommand):
    logger = logging.getLogger(f"project.{__name__}")
    help = "Update data for OPENPARLAMENTO for ACTS data."
    log_levels = {
        0: logging.ERROR,
        1: logging.WARNING,
        2: logging.INFO,
        3: logging.DEBUG,
    }

    def add_arguments(self, parser):
        """Add arguments to the command."""
        parser.add_argument(
            "--leg",
            type=int,
            choices=Legislature.get_list_legislature(),
            default=Legislature.get_list_legislature()[-1],
            dest='legislatura',
            required=True,
            help="Legislature number"
        )

    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.
        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        self.logger.info(f"----- Starting {command_name} -----")
        start_time = time.time()
        call_command(command_name, *args, **options, stdout=self.stdout)
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

    def update_data(self, name, legislatura, verbosity=3):
        """Helper function to update data and log progress"""
        self.logger.info(f"Update {name}")
        self.call_command(name, legislatura=legislatura, verbosity=verbosity)

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        self.logger.setLevel(self.log_levels.get(options["verbosity"]))
        legislatura = options['legislatura']
        self.logger.info(f"Timeline legislatura {legislatura}")

        update_commands = ['home_impl_decree', 'incarichi_parlamento', 'incarichi_governo',
                           'incarichi_gruppo', 'incarichi_organi', 'home_votes',
                           'home_leggi', 'home_sedute_cdm', 'home_dl', 'home_dlgs',
                           'home_ddl', 'home_gruppi', 'home_commissioni', 'home_governi', 'home_ddl_2']
        for command in update_commands:
            self.update_data(command, legislatura)
        username = env('USERNAME_PROD_OPP')
        password = env('PWD_PROD_OPP')

        # Codifica le credenziali in base64
        credentials = base64.b64encode(f'{username}:{password}'.encode('utf-8')).decode('utf-8')


        data = {
            'urlsToDelete': [f"/"],
            'token': env('CACHE_INVALIDATE_TOKEN'),
        }  # TODO da cambiare parametro url
        response = requests.post('https://staging.new.openparlamento.it/api/delete-cache',
                     headers={'Content-Type': 'application/json',
                              'Authorization': f'Basic {credentials}'
                              },
                     data=json.dumps(data))
        response = requests.post('https://parlamento19.openpolis.it/api/delete-cache',
                     headers={'Content-Type': 'application/json',
                              'Authorization': f'Basic {credentials}'},
                     data=json.dumps(data))

