from django.contrib.contenttypes.models import ContentType
from django.db.models import F, Value, Min, Q, DateField
from django.db.models.functions import Coalesce
from popolo.models import KeyEvent, Membership as OPDMMembership

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree, GovBill
from project.opp.home.models import Event
from project.opp.parl.models import Assembly
import datetime
from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.models import Membership

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)

        govbills = GovBill.objects.by_legislature(leg)
        cnt=0
        for i in govbills:
            cnt+=1
            title = f"È stato emanato il decreto legislativo <a href='/attivita_legislativa/decreti_legislativi/{i.slug}'>{i.title}</a>"
            description = f"È derivato da "
            leggi_derivate= str(', '.join([f"<a href='/attivita_legislativa/disegni_di_legge/{x.slug}'>{x.title}</a>" for x in i.delegation_law]))
            description += leggi_derivate
            description += f"<br>È il dlgs numero {cnt} emanato in questa legislatura."

            Event.objects.get_or_create(
                object_id=i.id,
                object_ct = ContentType.objects.get_for_model(i),
                event_description='DLGS emanato',
                date=CalendarDaily.objects.get(date=i.bill_date),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'category': Event.LAW,
                    'branch': None
                }
            )


