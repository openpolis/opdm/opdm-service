from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import Count, Q
from popolo.models import Classification, KeyEvent

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree, ImplementingDecree
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand
import math

from project.opp.votes.models import Voting

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')

class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        votings = (Voting.objects.by_legislature(leg).filter(Q(Q(is_key_vote=True) & ~Q(is_final=True))|
                                                                 # Q(is_final=True)|
                                                                 Q(is_confidence=True)|
                                                                 Q(type__in=[Voting.PREJUDICIAL, Voting.SUSPENSIVE]))
                   .order_by('sitting__date'))


        classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                       descr='Voto',
                                                                       code=17)
        cnt_gov_confidence = {}
        for i in votings:
            if not i.members_votes.all():
                continue
            description = []
            bills = i.bills.all()
            if i.type  == Voting.PREJUDICIAL:
                type_vote = 'questione pregiudiziale'
            elif i.type == Voting.SUSPENSIVE:
                type_vote = 'questione sospensiva'
            else:
                type_vote = 'votazione'
            title = f"Con {i.n_margin} voti di scarto è stata {i.outcome[:-1].lower()}a la {type_vote} " \
                    f"<a target='_blank' href='votazioni/{i.identifier}'>{i.public_title or i.title_compact}</a>."
            if i.is_final:
                if i.sitting.branch == 'C':
                    origin_branch = 'alla Camera'
                    destination_branch = 'al Senato'
                else:
                    origin_branch = 'al Senato'
                    destination_branch = 'alla Camera'
                title += f"<br>Si tratta di un voto finale."
                if bills.count()>1:
                    bills_ref = ', '.join(
                        [f"<a target='_blank' href='/attivita_legislativa/disegni_di_legge/{x.slug}'>{x.title}</a>" for
                         x in bills])

                    if any(['definitivamente' in x.last_status().phase.phase for x in i.bills.all()]):
                        description.append(f"Il voto approva definitivamente gli atti {bills_ref}.")
                    else:
                        description.append(f"Il voto approva gli atti {bills_ref} {origin_branch}, l'esame passa {destination_branch}.")

                elif bills.count()==1:
                    if any(['definitivamente' in x.last_status().phase.phase for x in i.bills.all()]):
                        description.append(f"Il voto approva definitivamente l'atto <a target='_blank' href='/attivita_legislativa/disegni_di_legge/{bills.first().slug}'>{bills.first().title}</a>.")
                    else:
                        description.append(f"Il voto approva l'atto <a target='_blank' href='/attivita_legislativa/disegni_di_legge/{bills.first().slug}'>{bills.first().title}</a> {origin_branch}, l'esame passa {destination_branch}.")

            gov = i.get_gov

            if i.is_confidence and i.type!=Voting.MOTION:
                title += f"<br>Si tratta di un voto di fiducia."
                cnt_gov_confidence[gov.id] = cnt_gov_confidence.get(gov.id, 0) + 1
                description.append(f"Salgono così a {cnt_gov_confidence.get(gov.id)} i voti di fiducia del {gov.organization.name}")

            description = '<br>'.join(description)

            event, created = Event.objects.update_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=i.sitting.date),
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': i.sitting.branch
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))


            for j in bills:
                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=j.id,
                                                   subject_ct=ContentType.objects.get_for_model(j))
