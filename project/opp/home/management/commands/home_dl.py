

from django.contrib.contenttypes.models import ContentType
from popolo.models import Classification, KeyEvent

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand
import math

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


def parse_days(days):
    import math
    days_p = days
    res = []
    if (years := math.floor(days_p / 365)):
        if years > 1:
            res.append(f"{years} anni")
        else:
            res.append(f"{years} anno")
    if (months := math.floor((days_p % 365) / 30)):
        if months > 1:
            res.append(f"{months} mesi")
        else:
            res.append(f"{months} mese")
    if len(res) == 0:
        return f"{days_p} giorni"

    elif len(res) == 1:
        return ' '.join(res)

    else:
        all_but_last = ' '.join(res[:-1])
        last = res[-1]
        return ' e '.join([all_but_last, last])

class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        gov_decrees = GovDecree.objects.by_legislature(leg)
        list_gov_decrees_processed = []
        for i in gov_decrees:
            list_gov_decrees_processed.append(i.id)

            title = f"Il {i.sitting.government.name} ha emanato il decreto legge " \
                    f"<a href='/attivita_legislativa/decreti_legge/{i.slug}'>{i.public_title or i.title}</a>" \
                    f" che dovrà essere convertito in legge dal Parlamento entro il {i.date_expiring.strftime('%d/%m/%Y')}."

            start_gov = i.sitting.government.start_date

            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='DL Emanato',
                                                                           code=12)

            gov_decrees_before = GovDecree.objects.filter(publication_gu_date__lt=i.publication_gu_date,
                                                          publication_gu_date__gte=i.publication_gu_date- datetime.timedelta(days=60))

            decreti_before_status = [x.status_at_date(i.publication_gu_date) for x in gov_decrees_before]
            decreti_in_corso = [x for x in decreti_before_status if x['status'] == 'c']

            gov_decrees_before_gov = GovDecree.objects.filter(sitting__government=i.sitting.government, publication_gu_date__lt=i.publication_gu_date)
            months = math.ceil((i.publication_gu_date - datetime.datetime.strptime(start_gov, '%Y-%m-%d').date()).days/30)
            num_gov = gov_decrees_before_gov.count()+1
            if GovDecree.objects.filter(publication_gu_date=i.publication_gu_date).exclude(id=i.id).filter(id__in=list_gov_decrees_processed):
                num_gov += 1
            media = round((gov_decrees_before_gov.count()+1)/months, 1)
            media = "{:.1f}".format(media).replace('.', ',')
            description = f"""Ci sono altri {len(decreti_in_corso)} decreti legge in corso di conversione.
        Per questo Governo è il {num_gov}° decreto legge,
        con una media mensile di {media} decreti legge."""

            event, created = Event.objects.get_or_create(
                category=classification,
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                date=CalendarDaily.objects.get(date=i.publication_gu_date),
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': None
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))


            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.sitting.government.id,
                                               subject_ct=ContentType.objects.get_for_model(i.sitting.government))


        # dl decaduto
        for i in gov_decrees:
            if i.sitting.government.start_date < ke.start_date:
                continue
            status = i.status()
            if status['status'] == 'e':
                title = f"Il DL del {i.sitting.government.name} " \
                        f"<a href='/attivita_legislativa/decreti_legge/{i.slug}'>{i.public_title or i.title}</a>" \
                        f" è decaduto."

                gov_decrees_before = GovDecree.objects.filter(sitting__government=i.sitting.government,
                                                              publication_gu_date__lt=i.publication_gu_date,
                                                              )

                decreti_before_status = [x.status_at_date(datetime.datetime.strptime(status['date'], '%Y-%m-%d').date()) for x in gov_decrees_before]
                decreti_decaduti= [x for x in decreti_before_status if x['status'] == 'e']

                description = f"""Era stato pubblicato il {i.publication_gu_date.strftime('%d/%m/%Y')} e non è stato convertito in legge dal Parlamento
                entro i sessanta giorni previsti dall’art. 77 della Costituzione.
                Sono {len(decreti_decaduti)} i decreti legge decaduti di questo governo."""
                classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                               descr='DL decaduto',
                                                                               code=13)
                event, created = Event.objects.get_or_create(
                    category=classification,
                    subjects__subject_ct=ContentType.objects.get_for_model(i),
                    subjects__subject_id=i.id,
                    date=CalendarDaily.objects.get(date=status['date']),
                    defaults={
                        'is_key_event': True,
                        'title': title,
                        'description': description,
                        'branch': None
                    }
                )
                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=i.id,
                                                   subject_ct=ContentType.objects.get_for_model(i))

                EventSubject.objects.get_or_create(event=event,
                                                   subject_id=i.sitting.government.id,
                                                   subject_ct=ContentType.objects.get_for_model(i.sitting.government))
