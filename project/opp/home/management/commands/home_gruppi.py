

from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.aggregates import ArrayAgg
from django.db.models import Count, Q
from popolo.models import Classification, KeyEvent

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill, GovDecree, ImplementingDecree
from project.opp.home.models import Event, EventSubject
import datetime
from taskmanager.management.base import LoggingBaseCommand
import math

from project.opp.votes.models import Voting, Group

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')

class Command(LoggingBaseCommand):


    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        leg = kwargs['legislatura']


        groups = Group.objects.by_legislature(leg).filter(organization__end_date__isnull=False)

        classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                       descr='Gruppo dissolto',
                                                                       code=18)
        for i in groups:
            title = f"Il gruppo {i.name_compact}, composto da {i.members_at_date(i.organization.end_date)}, si è sciolto."
            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=i.organization.end_date),
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': '',
                    'branch': i.branch[0]
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))

        groups = Group.objects.by_legislature(leg)

        classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                       descr='Gruppo formato',
                                                                       code=19)
        for i in groups:
            title = f"Si è formato il nuovo gruppo {i.name_compact}, a cui hanno aderito {i.members_at_date(i.organization.start_date).count()} parlamentari."
            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=i.organization.start_date),
                subjects__subject_ct=ContentType.objects.get_for_model(i),
                subjects__subject_id=i.id,
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': '',
                    'branch': i.branch[0]
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.id,
                                               subject_ct=ContentType.objects.get_for_model(i))
