from project.opp.utils import TodayDate
from django.contrib.contenttypes.models import ContentType
from django.db.models import F, Min, Value
from django.db.models.functions import Coalesce

from popolo.models import Classification
from taskmanager.management.base import LoggingBaseCommand

from project.calendars.models import CalendarDaily
from project.opp.home.models import Event, EventSubject
from project.opp.parl.models import Assembly, Legislature
from project.opp.votes.models import Membership


class Command(LoggingBaseCommand):

    def add_arguments(self, parser):
        parser.add_argument(
            "--leg",
            type=int,
            choices=Legislature.get_list_legislature(),
            default=Legislature.get_list_legislature()[-1],
            dest='legislatura',
        )

    def handle(self, *args, **kwargs):
        today = TodayDate()
        leg = kwargs['legislatura']
        ke = Legislature.get_legislature_by_number(int(leg))

        orgs = Assembly.objects \
            .annotate(end_date_coalesce=Coalesce(F('organization__end_date'), Value(today.today_formatted)),
                      start_date_coalesce=Coalesce(F('organization__start_date'), Value(today.today_formatted))) \
            .filter(
            start_date_coalesce__gte=ke.start_date,
            end_date_coalesce__lte=(ke.end_date or today.today_formatted))

        ## Persona cessa incarico parlamentare
        memberships_leg = Membership.objects.by_legislature(leg)
        fine_incarichi = memberships_leg.filter(opdm_membership__end_date__isnull=False)
        for i in fine_incarichi:

            if i.opdm_membership.person.end_date == i.opdm_membership.end_date:
                title = f"Decesso di <a target='_blank' " \
                        f"href='/persone/{i.opdm_membership.person.slug}'>{i.opdm_membership.person.name}</a>."
            else:
                title = f"{i.opdm_membership.end_reason.title()} <a target='_blank' " \
                        f"href='/persone/{i.opdm_membership.person.slug}'>{i.opdm_membership.person.name}</a>."

            description = f"""Il suo mandato in questa legislatura è durato {i.parse_days_in_related_parliament}."""
            cnt_leg = int(i.other_election_parl.count())
            if cnt_leg > 0:
                if cnt_leg == 1:
                    description += f"<br>Era già stat{'a' if i.opdm_membership.person.gender == 'F' else 'o'} " \
                                   f"elett{'a' if i.opdm_membership.person.gender == 'F' else 'o'} in Parlamento in un'altra legislatura"
                else:
                    description += f"<br>Era già stat{'a' if i.opdm_membership.person.gender == 'F' else 'o'} " \
                                   f"elett{'a' if i.opdm_membership.person.gender == 'F' else 'o'} in Parlamento in altre {cnt_leg} legislature"
                description += f" per una permanenza totale di {i.parse_days_in_parliament}."

            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='Fine incarico parlamentare',
                                                                           code=2)

            event, created = Event.objects.get_or_create(

                # event_description='Fine incarico parlamentare',
                category=classification,
                date=CalendarDaily.objects.get(date=i.opdm_membership.end_date),
                subjects__subject_ct=ContentType.objects.get_for_model(i.opdm_membership.person),
                subjects__subject_id=i.opdm_membership.person.id,
                defaults={
                    'is_key_event': True,
                    'title': title,
                    'description': description,
                    'branch': i.branch
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.opdm_membership.person.id,
                                               subject_ct=ContentType.objects.get_for_model(i.opdm_membership.person))

        ## Persona inizia incarico parlamentare
        memberships_leg = Membership.objects.by_legislature(leg)
        inizio_incarichi = memberships_leg.filter(
            opdm_membership__start_date__gt=orgs.aggregate(min=Min('organization__start_date'))['min'])
        for i in inizio_incarichi:
            description = ''
            role = i.opdm_membership.role.lower()
            if role == 'senatore' and i.opdm_membership.person.gender == 'F':
                role = 'senatrice'
            elif role == 'deputato' and i.opdm_membership.person.gender == 'F':
                role = 'deputata'
            title = f"È diventat{'a' if i.opdm_membership.person.gender == 'F' else 'o'} " \
                    f"{role} <a target='_blank' href='/persone/{i.opdm_membership.person.slug}'>{i.opdm_membership.person.name}</a>." \
                    f" (Subentra a <a target='_blank'  href='/persone/{i.replaced_member.opdm_membership.person.slug}'>{i.replaced_member.opdm_membership.person.name}</a>)."
            cnt_leg = int(i.other_election_parl.count())
            if cnt_leg == 0:
                description += f"È la prima volta che entra in Parlamento."
            elif cnt_leg == 1:
                description += f"Era già stat{'a' if i.opdm_membership.person.gender == 'F' else 'o'} elett{'a' if i.opdm_membership.person.gender == 'F' else 'o'} in Parlamento in un'altra legislatura"
                description += f" per una permanenza totale di {i.parse_days_in_parliament}."
            else:
                description += f"<br>Era già stat{'a' if i.opdm_membership.person.gender == 'F' else 'o'} elett{'a' if i.opdm_membership.person.gender == 'F' else 'o'} in Parlamento in altre {cnt_leg} legislature"
                description += f" per una permanenza totale di {i.parse_days_in_parliament}."
            classification, created = Classification.objects.get_or_create(scheme='OPP_EVENTS_LOG',
                                                                           descr='Inizio incarico parlamentare',
                                                                           code=1)
            event, created = Event.objects.get_or_create(
                category=classification,
                date=CalendarDaily.objects.get(date=i.opdm_membership.start_date),
                title=title,
                defaults={
                    'is_key_event': True,
                    'description': description,
                    'branch': i.branch
                }
            )
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.opdm_membership.person.id,
                                               subject_ct=ContentType.objects.get_for_model(i.opdm_membership.person))

        ## Inizio legislatura
        inizio_incarichi = memberships_leg.filter(
                    opdm_membership__start_date=orgs.aggregate(min=Min('organization__start_date'))['min'])

        camera = inizio_incarichi.filter(opdm_membership__organization__name__icontains='camera')
        event, created = Event.objects.get_or_create(
            category=classification,
            date=CalendarDaily.objects.get(date=orgs.aggregate(min=Min('organization__start_date'))['min']),
            branch= "C",
            defaults={
                'is_key_event': True,
                'title': f"Eletti {camera.count()} deputate e deputati.",
                'description': '',

            }
        )
        for i in camera:
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.opdm_membership.person.id,
                                               subject_ct=ContentType.objects.get_for_model(i.opdm_membership.person))
        senato = inizio_incarichi.filter(opdm_membership__organization__name__icontains='senato', opdm_membership__role='Senatore')
        event, created = Event.objects.get_or_create(
            category=classification,
            date=CalendarDaily.objects.get(date=orgs.aggregate(min=Min('organization__start_date'))['min']),
            branch= "S",
            defaults={
                'is_key_event': True,
                'title': f"Eletti {senato.count()} senatori e senatrici.",
                'description': '',

            }
        )
        for i in senato:
            EventSubject.objects.get_or_create(event=event,
                                               subject_id=i.opdm_membership.person.id,
                                               subject_ct=ContentType.objects.get_for_model(i.opdm_membership.person))
