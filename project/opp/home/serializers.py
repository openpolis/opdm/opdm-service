from django.db.models import Q
from rest_framework import serializers

from project.calendars.models import CalendarDaily
from project.opp.home.filtersets import get_list_classification_manager
from project.opp.home.models import Event, Link


class EventDetailSerializer(serializers.ModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    # date = serializers.DateField(source="date.date")
    branch = serializers.CharField()

    class Meta:
        """Define serializer Meta."""

        model = Event
        ref_name = "Event"
        fields = ("id", "is_key_event", "title", "description", "branch")


class EventListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    opp_events = serializers.SerializerMethodField()

    def get_opp_events(self, obj):
        request = self.context.get("request")
        is_key_event_value = request.GET.get("is_key_event", None)
        categories = request.GET.getlist("event_category", None)
        branch = request.GET.get("branch", None)
        search = request.GET.get("search", None)
        res = obj.opp_events.all()
        if is_key_event_value in ['True', 'False']:
             res = res.filter(is_key_event=is_key_event_value)
        else:
            res = res.all()
        if categories:
            if 'voto' in categories:
                res = res.filter(Q(category__in=get_list_classification_manager(categories))|
                                 Q(description__icontains='votazion'))
            else:
                res = res.filter(category__in=get_list_classification_manager(categories))
        else:
            res = res.all()
        if branch:
             res = res.filter(branch=branch)
        else:
            res = res.all()
        if search:
             res = res.filter(Q(title__icontains=search)|Q(description__icontains=search))
        else:
            res = res.all()
        return EventDetailSerializer(res, many=True).data

    class Meta:
        model = CalendarDaily
        ref_name = "Event"
        fields = ( "date",  "opp_events")


class LinkDetailSerializer(serializers.ModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    # date = serializers.DateField(source="date.date")


    class Meta:
        """Define serializer Meta."""

        model = Link
        ref_name = "Link"
        fields = "__all__"
