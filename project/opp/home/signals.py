import asyncio
import base64
import json
import threading

import aiohttp
from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver
from project.opp.home.models import Link
from datetime import datetime
import environ
import requests
import pytz
import time

env = environ.Env()


def get_publication_dt_img_url_from_link(link, logger=None) -> tuple:
    """Get publication datetime and image url from a Link instance.


    :param link: The instance of Link to start from
    :param logger:
    :return: a 2-tuple with publication_dt, img_url
    """
    # retrive the slug, fetch the article data from WP REST API
    slug = link.url.replace('https://www.openpolis.it/', '').replace('/', '')
    r = requests.get(
        f"https://www.openpolis.it/wp-json/wp/v2/posts?slug={slug}"
    )
    try:
        if r.status_code == 200:
            article_json = r.json()[0]
        else:
            if logger:
                logger.warning(f"Could not fetch post for article: {link.url}. Skipping.")
            raise Exception("Article not found")
    except Exception:
        if logger:
            logger.warning(f"Could not fetch post for article: {link.url}. Skipping.")
        raise Exception("Article not found")

    # retrieve pubblication datetime
    local_tz = pytz.timezone("Europe/Rome")
    publication_dt = local_tz.localize(datetime.strptime(article_json['date'], "%Y-%m-%dT%H:%M:%S"))

    # retrieve img_url through a request to featured_media
    try:
        featured_media = article_json['_links']['wp:featuredmedia'][0]['href']
        r = requests.get(featured_media)
        if r.status_code == 200:
            img_url = r.json()['media_details']['sizes']['medium_large']['source_url']
        else:
            if logger:
                logger.warning(f"Could not fimg_url for article: {link.url}. Skipping.")
            img_url = None
    except Exception as e:
        img_url = None
        if logger:
            logger.warning(f"Error {e} while fetching img_url for article: {link.url}.")

    title, description = '', ''
    if article_json.get('title'):
        title = article_json.get('title').get('rendered')
        description = article_json.get('excerpt').get('rendered')
    return publication_dt, img_url, title, description


@receiver([post_save], sender=Link)
def compute_publication_dt_img_url(sender, **kwargs):  # noqa
    link = kwargs['instance']

    try:
        publication_dt, img_url, title, description = get_publication_dt_img_url_from_link(link)
    except Exception as e:
        return

    # update article with retrieved data
    link.image = img_url
    link.date = publication_dt
    if not link.title:
        link.title=title
    if not link.description:
        link.description=description

    post_save.disconnect(receiver=compute_publication_dt_img_url, sender=Link)
    link.save()
    post_save.connect(receiver=compute_publication_dt_img_url, sender=Link)



async def async_http_request():
    data = {
        'urlsToDelete': [f"/"],
        'token': env('CACHE_INVALIDATE_TOKEN')
    }#TODO da cambiare parametro url
    username = env('USERNAME_PROD_OPP')
    password = env('PWD_PROD_OPP')

    # Codifica le credenziali in base64
    credentials = base64.b64encode(f'{username}:{password}'.encode('utf-8')).decode('utf-8')
    await asyncio.sleep(2)
    async with aiohttp.ClientSession() as session:
        async with session.post('https://parlamento19.openpolis.it/api/delete-cache',
                                headers={'Content-Type': 'application/json',
                                         'Authorization': f'Basic {credentials}'},
                                data=json.dumps(data)) as response:
            await response.text()
    await asyncio.sleep(1)
    async with aiohttp.ClientSession() as session:
        await session.get('https://parlamento19.openpolis.it/')
        print(4)

def run_async_function():
    loop = asyncio.new_event_loop()
    asyncio.set_event_loop(loop)
    loop.run_until_complete(async_http_request())
    loop.close()


@receiver([post_save, post_delete], sender=Link)
def delete_home_cache_fe(sender, **kwargs):
    thread = threading.Thread(target=run_async_function)
    thread.start()
    print('a')

