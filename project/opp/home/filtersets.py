import ast

from django.db.models.functions import Coalesce
from django.utils.translation import ugettext as _
from django.db.models import F, OuterRef, Subquery, Value, Q
from django_filters import ChoiceFilter, CharFilter, ModelChoiceFilter, MultipleChoiceFilter, ModelMultipleChoiceFilter, \
    filters
from django_filters.rest_framework import (
    FilterSet, BooleanFilter)
from popolo.models import Organization, KeyEvent, Classification

import datetime

from project.calendars.models import CalendarDaily
from project.opp.gov.models import Government
from project.opp.home.models import Event
from project.opp.votes.models import Voting

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


def get_list_classification_manager(case_list):
    base = Classification.objects.filter(scheme='OPP_EVENTS_LOG')
    res = Classification.objects.none()
    for case in case_list:
        if case == 'gov':
            res = res.union(base.filter(Q(descr__icontains='gov')|Q(descr='Sedute CdM')))
        if case == 'groups':
            res = res.union(base.filter(Q(descr__icontains='gruppo')))
        if case == 'organs':
            res = res.union(base.filter(Q(descr__icontains='organo')))
        if case == 'comm':
            res = res.union(base.filter(Q(descr__icontains='commissione')))
        if case == 'ddl':
            res = res.union(base.filter(Q(descr__startswith='DDL ')))
        if case == 'dl':
            res = res.union(base.filter(Q(descr__startswith='DL ')))
        if case == 'dlgs':
            res = res.union(base.filter(Q(descr__startswith='DLGS ')))
        if case == 'dact':
            res = res.union(base.filter(Q(descr__startswith='Decreto Attuativo ')))
        if case == 'inc_parl':
            res = res.union(base.filter(Q(descr__icontains='incarico parlamentare')))
        if case == 'law':
            res = res.union(base.filter(Q(descr__icontains='Approvazione legge')))
        if case == 'voto':
            res = res.union(base.filter(Q(descr__startswith='Voto')))
    return base.filter(id__in=res.values('id'))


def get_years(request=None):
    if request is None:
        return []

    leg = request.parser_context['kwargs'].get('legislature', None)
    legislature_identifier = f"ITL_{leg}"
    e = KeyEvent.objects.get(identifier=legislature_identifier)
    qs = Event.objects.filter(date__date__gte=e.start_date, date__date__lte=(e.end_date or today)).annotate(
        y=F("date__year")
    ).order_by('y')

    return qs.values_list('y', 'y').distinct()


class EventFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)
        self.filters['year'].extra['choices'] = [
            (str(y[0]), str(y[1])) for y in get_years(request=self.request)
        ]

        self.filters['month'].extra['choices'] = [
            (str(x), str(x)) for x in range(1,13)
        ]
    year = ChoiceFilter(
        method="year_filter",
        label=_("Year"),
        empty_label=_("All years"),
        help_text=_("Filter votings based on the year of the event date"),
    )

    @staticmethod
    def year_filter(queryset, name, value):
        if value:
            return queryset.filter(date__year=value)
        else:
            return queryset

    month = ChoiceFilter(
        method="month_filter",
        label=_("Month"),
        empty_label=_("All months"),
        help_text=_("Filter votings based on the month of the event date"),
    )

    @staticmethod
    def month_filter(queryset, name, value):
        if value:
            return queryset.filter(date__month=value)
        else:
            return queryset


    is_key_event = ChoiceFilter(
        method="is_key_event_filter",
        label=_("Is key event"),
        choices=[(True, 'Yes'), (False, 'No')],
        help_text=_("Filter events based on whether they are key events"),
    )

    @staticmethod
    def is_key_event_filter(queryset, name, value):
        if value == 'null':
            q = queryset.annotate(b=F("opp_events__is_key_event")).filter(b__isnull=True)
            return q
        elif value:
            q = queryset.annotate(b=F("opp_events__is_key_event")).filter(b=value)
            return q

        else:
            q = queryset.all()
            return q


    event_category = MultipleChoiceFilter(
        label="Categoria evento",
        choices=[('gov', 'Governo'),
                 ('groups', 'Gruppi'),
                 ('organs', 'Organi'),
                 ('comm', 'Commissioni'),
                 ('dl', 'Decreti legge'),
                 ('dlgs', 'Decreti legislativi'),
                 ('ddl', 'Disegni di legge'),
                 ('dact', 'Decreti attuativi'),
                 ('voto', 'Votazioni'),
                 ('inc_parl', 'Incarichi parlamentari'),
                 ('law', 'Approvazione legge'),
                 ],
        help_text=_("Filter bills based on type"),
        method="category_filter",
    )

    @staticmethod
    def category_filter(queryset, name, value):
        if value == 'null':
            q = queryset.annotate(b=F("opp_events__category")).filter(b__isnull=True)
            return queryset.filter(id__in=q.values('id')).distinct()
        elif value:
            if 'voto' in value:
                q = queryset.annotate(b=F("opp_events__category_id")).filter(Q(
                    b__in=get_list_classification_manager(value).values('id'))|Q(opp_events__description__icontains='votazion'))
            else:
                q = queryset.annotate(b=F("opp_events__category_id")).filter(b__in=get_list_classification_manager(value).values('id'))
            return queryset.filter(id__in=q.values('id')).distinct()

        else:
            q = queryset.all()
            return q

    branch = ChoiceFilter(
        method="branch_filter",
        label="Ramo",
        choices= Event.branch.field.choices,
        help_text=_("Filter events based on branch"),
    )

    @staticmethod
    def branch_filter(queryset, name, value):
        if value == 'null':
            q = queryset.annotate(b=F("opp_events__branch")).filter(b__isnull=True)
            return q
        elif value:
            q = queryset.annotate(b=F("opp_events__branch")).filter(b=value)
            return q

        else:
            q = queryset.all()
            return q

    class Meta:
        model = CalendarDaily
        fields = []

