from statistics import mean

import pandas as pd
from django.contrib.contenttypes.models import ContentType
from django.core.cache import cache
from django.db.models import F
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.views import APIView

from project.opp.api_opp_v1.utils import NumbersParliament
from project.opp.gov.models import Government
from project.opp.gov.serializers import PoliticalPartiesHomeListSerializer
from project.opp.home.models import Link
from project.opp.home.serializers import LinkDetailSerializer
from project.opp.votes.models import MemberVote

def get_instances_for_content_type(request):
    content_type_id = request.GET.get('content_type')
    content_type = ContentType.objects.get(id=content_type_id)
    # model = content_type.model_class()
    # instances = model.objects.all().values('id', 'model')  # Adjust 'name' field
    return JsonResponse({
        'id': content_type.id,
        'model': content_type.model,
        'app_label': content_type.app_label
    })


class HomeView(APIView):
    throttle_classes = []

    def get(self, request, legislature):


        '''
        APPROFONDIMENTI
        '''
        news = Link.objects.filter(
        ).order_by('-date')[:5]
        cache_key = f"legislature_activity_{legislature}"
        cached_results = cache.get(cache_key)

        if cached_results:
            numbers = cached_results
        else:
            numbers = NumbersParliament(legislature).get_response()
            cache.set(cache_key, numbers, timeout=None)

        g = Government.objects.all().order_by('-organization__start_date')[0]
        majority_cohesion = round(mean([x['cohesion_rate'] for x in g.majority_cohesion]), 1)
        minority_cohesion = round(mean([x['cohesion_rate'] for x in g.opposition_cohesion]), 1)

        return Response({
            'news': LinkDetailSerializer(news, many=True).data,
            'who_makes_laws': numbers.get('who_makes_laws'),
            'majority_minority_cohesion':{
                'majority':{
                    'cohesion_rate': majority_cohesion
                } ,
                'minority': {
                    'cohesion_rate': minority_cohesion
                }
            },
            'votings_absence': numbers.get('votings_absence'),
            'parties_power': PoliticalPartiesHomeListSerializer(g.political_parties).data

        })
