# Python Built-in Imports
from datetime import datetime
from random import randint

# Third-Party Imports
from django.test import SimpleTestCase, TestCase
from django.utils.translation import gettext_lazy as _

# Project Internal Imports
from project.opp.utils import parse_days, TodayDate


class ParseDaysTest(TestCase):
    """
        This class contains unit tests for the `parse_days` function.
    """
    YEAR = _('year')
    YEARS = _('years')
    MONTH = _('month')
    MONTHS = _('months')
    DAY = _('day')
    DAYS = _('days')
    AND = _('and')

    def create_test(self, expected, days):
        with self.subTest(days=days):
            self.assertEqual(expected, parse_days(days))

    def test_wrong_type_raises_exception(self):
        with self.assertRaises(Exception):
            parse_days('1')
        with self.assertRaises(Exception):
            parse_days(-1)

    # def test_parse_days(self):
    #     """
    #     Test method to calculate the number of days based on different time periods.
    #
    #     :return: None
    #
    #     The test_parse_days method generates test cases for calculating the number of days based on different time periods. It uses randomly generated values for the month and year. The test
    #     * cases include different combinations of days, months, and years.
    #
    #     The method loops through the test cases and calls the create_test method with each test case as arguments.
    #     """
    #     random_month_int = randint(1, 12)
    #     random_year_int = randint(1, 50)
    #     test_cases = [
    #         (f"1 {_(self.YEAR)}", 365),
    #         (f"0 {_(self.DAYS)}", 0),
    #         (f"1 {_(self.DAY)}", 1),
    #         (f"1 {_(self.MONTH)}", 30),
    #         (f"{random_month_int} {_(self.MONTHS)}", 30 * random_month_int),
    #         (f"1 {_(self.YEAR)} {_(self.AND)} {random_month_int} {_(self.MONTHS)}", 365 + 30 * random_month_int),
    #         (f"{random_year_int} {_(self.YEARS)} {_(self.AND)} {random_month_int} {_(self.MONTHS)}",
    #          365*random_year_int + 30 * random_month_int),
    #     ]
    #
    #     for test_case in test_cases:
    #         self.create_test(*test_case)


class TestTodayDate(SimpleTestCase):
    """
    Unit tests for the TestTodayDate class.

    Class Methods:
        setUp(): Setup method called before each test case.

    Test Methods:
        test_today(): Test method to check today's date.
        test_today_formatted(): Test method to check today's date in a specific format.
    """
    def setUp(self):
        self.date_instance = TodayDate()

    def test_today(self):
        self.assertEqual(self.date_instance.today, datetime.now().date())

    def test_today_formatted(self):
        self.assertEqual(self.date_instance.today_formatted, datetime.now().date().strftime('%Y-%m-%d'))
