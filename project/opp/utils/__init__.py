import datetime
import math

from django.utils.translation import gettext_lazy as _

from project.opp.acts.models import GovBill, Bill


def parse_days(days: int) -> str:
    """Convert number of days into string format, assuming year is 365 days and month is 30 days.

    Keyword arguments:
        days (int) -- number of days, it has to be integer

    Returns:
        string format -- <x> <year> and <n> <month>

    Examples:
        1 -> 1 day
        30 -> 1 month
        365 -> 1 year
        366 -> 1 year
    """
    res: list = []

    if days<0:
        raise Exception("Insert a number >= 0")

    if days == 1:
        return f"{days} " + _('giorno')

    if (years := math.floor(days / 365)):
        if years > 1:
            res.append(f"{years} " + 'anni')
        else:
            res.append(f"{years} " + 'anno')
    if (months := math.floor((days % 365) / 30)):
        if months > 1:
            res.append(f"{months} " + "mesi")
        else:
            res.append(f"{months} " + "mese")

    if len(res) == 0:
        return f"{days} " + 'giorni'
    elif len(res) == 1:
        return ' '.join(res)
    else:
        all_but_last = ' '.join(res[:-1])
        last = res[-1]
        return ' e '.join([all_but_last, last])


class TodayDate:
    """
    A class that provides the current date and time.

    Attributes:
        today (datetime.datetime): The current date and time.
        today_formatted (str): The formatted current date in 'YYYY-MM-DD' format.
    """
    @property
    def today(self):
        return datetime.datetime.now().date()
    @property
    def today_formatted(self):
        return self.today.strftime("%Y-%m-%d")


def get_identifier_dlgs(act: [GovBill, Bill], leg):
    if act._meta.model_name == 'bill':
        check_consitency_leg = Bill.objects.by_legislature(leg).filter(id=act.id)
        if not check_consitency_leg.count():
            return f"{act.identifier} ({act.assembly.name.split(' - ')[-1].split(' della')[0]})"
    return act.identifier


def get_type_act(act: [GovBill, Bill], leg):
    if act._meta.model_name == 'bill':
        check_consitency_leg = Bill.objects.by_legislature(leg).filter(id=act.id)
        if check_consitency_leg.count():
            return 'bill'
        else:
            return 'prev_act'

    elif act._meta.model_name == 'govbill':
        check_consitency_leg = GovBill.objects.by_legislature(leg).filter(id=act.id)
        if check_consitency_leg.count():
            return 'dlgs'
        else:
            return 'prev_act'
    elif act._meta.model_name == 'govdecree':
        return 'decree'
    elif act._meta.model_name == 'implementingdecree':
        return 'datt'


def gender_transf(role):
    if role == 'Ministro':
        role = 'Ministra'
    elif role.lower().strip() == 'viceministro':
        role = 'Viceministra'
    elif role == 'Sottosegretario':
        role = 'Sottosegretaria'
    return role
