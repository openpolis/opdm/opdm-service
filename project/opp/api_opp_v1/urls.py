from django.urls import path, include
from django.views.generic import RedirectView
from rest_framework import routers

from .views import VotingViewSet, BillViewSet, MembershipViewSet, \
    LegislaturesListView, vote_types, vm_cohesion_rates, LegislatureAPIRootView, GovernmentViewSet, \
    ParliamentOrgsViewSet, ParliamentOrgsDetailViewSet, GovDecreeViewSet, GovBillViewSet, ImplementingDecreeViewSet, \
    PersonsViewSet, ActsView, MemberVotesViewSet, BillSignerViewSet, GroupViewSet, PPIndexViewSet, GroupVotesViewSet, \
    LegislatureActivity, TopicViewSet, EventViewSet
from ..home.views import HomeView


class Router(routers.DefaultRouter):
    APIRootView = LegislatureAPIRootView
    root_view_name = 'legislature-detail'


router = Router()
router.register(r'votings', VotingViewSet, basename='voting')
router.register(r'memberships', MembershipViewSet, basename='membership')
router.register(r'memberships_votes', MemberVotesViewSet, basename='membership-votes')
router.register(r'bill_signer', BillSignerViewSet, basename='billsigner')
router.register(r'bills', BillViewSet, basename='bill')
router.register(r'govdecrees', GovDecreeViewSet, basename='govdecree')
router.register(r'govbills', GovBillViewSet, basename='govbill')
router.register(r'implementingdecrees', ImplementingDecreeViewSet, basename='implementingdecree')
router.register(r'governments', GovernmentViewSet, basename='government')
router.register(r'parl_assemblies', ParliamentOrgsViewSet, basename='parliamentorg')
router.register(r'parl_assemblies/senato/presidency', ParliamentOrgsDetailViewSet,
                basename='parliamentorg-senato-presidency')
router.register(r'parl_assemblies/senato/commission_councils', ParliamentOrgsDetailViewSet,
                basename='parliamentorg-senato-commision_councils')
router.register(r'parl_assemblies/senato/groups', GroupViewSet,
                basename='senato-groups')
router.register(r'parl_assemblies/camera/presidency', ParliamentOrgsDetailViewSet,
                basename='parliamentorg-camera-presidency')
router.register(r'parl_assemblies/camera/commission_councils',
                ParliamentOrgsDetailViewSet,
                basename='parliamentorg-camera-commision_councils')
router.register(r'parl_assemblies/camera/groups', GroupViewSet, basename='camera-groups')
router.register(r'groups_votes', GroupVotesViewSet, basename='groups-votes')
router.register(r'parl_assemblies/bichambers', ParliamentOrgsViewSet, basename='parliamentorg-bichambers')
router.register(r'persons/pp_index', PPIndexViewSet, basename='pp_index')
router.register(r'persons', PersonsViewSet, basename='person')
router.register(r'faqs', TopicViewSet, basename='topic')
router.register(r'events', EventViewSet, basename='event')


patterns = [
    path("", LegislaturesListView.as_view(), name='legislature-list', ),
    path("vote-types/", vote_types, name='vote-types'),
    path("cohesion-rates-ranges/", vm_cohesion_rates, name='cohesion-rates-ranges'),

    path("<int:legislature>/acts/", ActsView.as_view(), name='act'),
    path("<int:legislature>/legislative_activity/", LegislatureActivity.as_view(), name='legislature_activity'),
    path("<int:legislature>/", include([path("home/", HomeView.as_view(), name='home'),
                                       *router.urls]
                                       )),
]


urlpatterns = [
    path("", RedirectView.as_view(pattern_name='openparlamento_v1:legislature-list'), name='openparlamento-index'),
    path("v1/", include((patterns, "opp.api_opp_v1"), namespace="openparlamento_v1")),
]
