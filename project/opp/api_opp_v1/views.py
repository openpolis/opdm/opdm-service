from collections import OrderedDict

from django.db.models import IntegerField, F, Q, Value, Max
from django.db.models import fields
from django.db.models.functions import Cast, Substr, Coalesce, Lower, Replace
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.core.cache import caches
cache = caches['default']
db_cache = caches['db']
from rest_framework import views, viewsets
from django_filters import rest_framework as extra_filters
from rest_framework import filters
from rest_framework.decorators import action, api_view

from rest_framework.response import Response
from rest_framework.routers import APIRootView

from project.api_v1.filters import FiltersInListOnlySchema
from project.calendars.models import CalendarDaily
from project.core import ResultsWithVotationsCodelistPagination, ResultsWithMembershipsCodelistPagination, \
    ResultsWithGovDecreCodelistPagination, ResultsWithBillCodelistPagination, ResultsWithGovBillCodelistPagination, \
    ResultsWithImplementingDecreeCodelistPagination, ResultsWithPersonsCodelistPagination, \
    ResultsWithGroupVoteCodelistPagination, ResultsWithEventsCodelistPagination
from popolo.models import Organization, KeyEvent
from rest_framework.views import APIView

from project.opp.api_opp_v1.utils import NumbersParliament
from project.opp.home.filtersets import EventFilterSet
from project.opp.home.serializers import EventListSerializer, EventDetailSerializer
from project.opp.metrics.filtersets import PersonsFilterSet
from project.opp.metrics.models import PersonsOPP
from project.opp.metrics.ordering import PersonsOrdering
from project.opp.texts.models import Topic
from project.opp.texts.serializers import TopicListSerializer, TopicDetailSerializer
from project.opp.votes.models import Voting, Membership, MemberVote, \
    Group, GroupVote  # , MemberMajority, MembershipGroup, GroupMajority
from project.opp.acts.models import Bill, GovDecree, GovBill, ImplementingDecree, BillSigner
from project.opp.votes.serializers import VotingDetailSerializer, VotingListSerializer, \
    MembershipListSerializer, MembershipDetailSerializer, MemberVoteDetailSerializer, MemberVoteListSerializer, \
    GroupVoteDetailSerializer, GroupVoteListSerializer
# ,GroupVoteSerializer, MemberVoteSerializer
from project.opp.gov.models import Government
from project.opp.acts.serializers import BillDetailSerializer, BillListSerializer, GovDecreeListSerializer, \
    GovDecreeDetailSerializer, GovBillListSerializer, GovBillDetailSerializer, ImplementingDecreeListSerializer, \
    ImplementingDecreeDetailSerializer, BillSignerListSerializer, BillSignerDetailSerializer, GroupListSerializer, \
    GroupDetailSerializer
from project.opp.gov.serializers import GovernmentListSerializer, GovernmentDetailSerializer
from project.opp.votes import filtersets as filtersets_votes
from project.opp.acts.filtersets import BillFilterSet, GovDecreeFilterSet, GovBillFilterSet, ImplementingDecreeFilterSet
from project.opp.parl.serializers import ParliamentOrgListSerializer, ParliamentOrgDetailSerializer, \
    BiChamberOrgDetailSerializer, PresidencyDetailSerializer, CommissionCouncilOrgDetailSerializer, \
    GroupParliamentOrgDetailSerializer
from project.opp.metrics.serializers import PersonsListSerializer, PersonsDetailSerializer, PPIndexListSerializer
from project.opp.parl.models import Assembly
import datetime

@api_view()
def vote_types(request, *args, **kwargs):
    """All vote types"""
    return Response(dict(filtersets_votes.get_vote_types(descriptive=True)))


@api_view()
def vm_cohesion_rates(request, *args, **kwargs):
    return Response(dict(filtersets_votes.get_vm_cohesion_rates(descriptive=True)))


class LegislaturesListView(views.APIView):
    """A list of all available legislatures in Openparlamento"""

    throttle_classes = []

    @staticmethod
    def get(*args, **kwargs) -> Response:
        legislatures_list = KeyEvent.objects.filter(
            event_type='ITL',
            start_date__gte='2007-01-01'
        ).annotate(
            n=Cast(Substr("identifier", 5), output_field=IntegerField())
        ).order_by('-start_date').values(
            'name', 'start_date', 'end_date', 'identifier', "n"
        )

        for leg in legislatures_list:
            leg['url'] = args[0].build_absolute_uri(
                reverse(
                    'openparlamento_v1:legislature-detail',
                    kwargs={
                        'legislature': leg['identifier'][4:]
                    }
                )
            )

        return Response(
            legislatures_list
        )


class VotingViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all available votings, paged, filtered, sorted, ....
    retrieve:Returns a single voting selected by `id`.
    """
    lookup_field = 'pk2'
    throttle_classes = []

    def retrieve(self, request, *args, **kwargs):

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        instance = self.get_object()
        serializer = self.get_serializer(instance)

        cache_key = f'voting_detail:{instance.id}'
        cached_results = db_cache.get(cache_key)

        if cached_results:
            return Response(cached_results)
        else:
            db_cache.set(cache_key, serializer.data, timeout=None)
            return Response(serializer.data)

    ord_fields = (("date", "Data"),
                  ("majority_cohesion_rate", "Compattezza maggioranza"),
                  ("n_rebels", "Ribelli"),
                  ("identifier", "Identificativo"),
                  ("n_margin", "Margine"),
                  )
    ordering_fields = set((x[0] for x in ord_fields))

    ordering = ("-date", "-identifier")
    search_fields = ("original_title", "public_title", "description_title")
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = filtersets_votes.VotingFilterSet
    serializer_class = VotingDetailSerializer
    pagination_class = ResultsWithVotationsCodelistPagination

    schema = FiltersInListOnlySchema()

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return VotingListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return VotingDetailSerializer
        else:
            return super().get_serializer_class()


    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return Voting.objects.none()

        leg = self.kwargs['legislature']
        return Voting.objects.by_legislature(leg).exclude(majority_cohesion_rate__isnull=True). \
            annotate(date=F('sitting__date'), pk2=F('identifier')).order_by('-sitting__date', '-number')


class LegislatureAPIRootView(APIRootView):
    """Here comes the Sun! **blah**, *bla*, [openpolis](www.openpolis.it)"""

    throttle_classes = []

    def get_view_name(self):
        if hasattr(self, 'kwargs'):
            return f"Legislature {self.kwargs['legislature']}"
        else:
            return "Legislature"


class MembershipOrdering(filters.OrderingFilter):
    # The URL query parameter used for the ordering.

    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)

        if ordering:
            return queryset.order_by(*(list(ordering)+['surname',]))

        return queryset



class MembershipViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all available memberships, paged, filtered, sorted ....
    retrieve:Returns a single membership selected by `id`.
    """
    lookup_field = 'pk2'
    throttle_classes = []

    ord_fields = (
        # ("n_rebels", "Ribellione"),
        #           ("start_date_membership", "Subentrato il"),
        ("perc_fidelity", "Indice di affidabilità"),
        ("n_present", "Presenze"),
        ("n_absent", "Assenze"),
        ("n_mission", "Missioni"),
        ("days_in_parliament", "Anni in Parlamento"),
        ("surname", "Cognome"),
        ("pp_ordering", "Indice di forza")
    )
    ordering_fields = set((x[0] for x in ord_fields))
    ordering = ("-opdm_membership__start_date",)
    search_fields = ("opdm_membership__person__name",)
    filter_backends = (
        filters.SearchFilter,
        MembershipOrdering,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = filtersets_votes.MembershipFilterSet
    serializer_class = MembershipDetailSerializer
    pagination_class = ResultsWithMembershipsCodelistPagination
    schema = FiltersInListOnlySchema()

    def list(self, request, *args, **kwargs):
        if cache.get(f'membership_list_{self.request._request.get_raw_uri()}'):

            return  Response(OrderedDict(cache.get(f'membership_list_{self.request._request.get_raw_uri()}')))
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            # data = serializer.data
            # cache.set(f'pp_index_{request.__str__()}', {'res': data, 'page':page}, timeout=60 * 60)
            return self.get_paginated_response(
                serializer.data
            )

        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data
        cache.set(f'membership_list_{request.__str__()}', data, timeout=60*60)
        return Response(data)


    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return MembershipListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return MembershipDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return Membership.objects.none()

        leg = self.kwargs['legislature']
        return Membership.objects.by_legislature(leg).select_related('opdm_membership',
                                                                     'opdm_membership__person',
                                                                     'opdm_membership__area',
                                                                     'opdm_membership__organization') \
            .prefetch_related('membership_groups') \
            .annotate(start_date_membership=F('opdm_membership__start_date'),
                      surname=Lower(F('opdm_membership__person__family_name')),
                      pk2=Lower(F('opdm_membership__person__slug'))).order_by('id')
            # .filter(is_active=True)

    @action(detail=False)
    def codelists(self, request, *args, **kwargs):
        """ All memberships\' codelists """

        return Response(
            {
                'election_macro_areas': dict(filtersets_votes.get_election_macro_areas(self.request)),
                'election_areas': dict(filtersets_votes.get_election_areas(self.request)),
                'gender_types': dict(filtersets_votes.GENDER_TYPES),
                'role_types': dict(filtersets_votes.ROLE_TYPES),
            }
        )


class MemberVotesViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all available memberships, paged, filtered, sorted ....
    retrieve:Returns a single membership selected by `id`.
    """
    throttle_classes = []
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = filtersets_votes.MemberVoteFilterSet
    # pagination_class = ResultsWithMembershipsCodelistPagination
    schema = FiltersInListOnlySchema()

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return MemberVoteListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return MemberVoteDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return MemberVote.objects.none()

        leg = self.kwargs['legislature']
        return MemberVote.objects.by_legislature(leg)

    # @action(detail=False)
    # def codelists(self, request, *args, **kwargs):
    #     """ All memberships\' codelists """
    #
    #     return Response(
    #         {
    #             'election_macro_areas': dict(filtersets_votes.get_election_macro_areas(self.request)),
    #             'election_areas': dict(filtersets_votes.get_election_areas(self.request)),
    #             'gender_types': dict(filtersets_votes.GENDER_TYPES),
    #             'role_types': dict(filtersets_votes.ROLE_TYPES),
    #         }
    #     )


class GroupVotesViewSet(viewsets.ReadOnlyModelViewSet):
    throttle_classes = []
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    ordering_fields = ('date',)
    filter_class = filtersets_votes.GroupVoteFilterSet
    pagination_class = ResultsWithGroupVoteCodelistPagination
    schema = FiltersInListOnlySchema()

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return GroupVoteListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return GroupVoteDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return GroupVote.objects.none()

        leg = self.kwargs['legislature']
        return GroupVote.objects.by_legislature(leg).annotate(date=F('voting__sitting__date'))


class BillSignerViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all available memberships, paged, filtered, sorted ....
    retrieve:Returns a single membership selected by `id`.
    """
    throttle_classes = []
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = filtersets_votes.BillSignerFilterSet
    # pagination_class = ResultsWithMembershipsCodelistPagination
    schema = FiltersInListOnlySchema()

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return BillSignerListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return BillSignerDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return BillSigner.objects.none()

        leg = self.kwargs['legislature']
        return BillSigner.objects.by_legislature(leg).filter(bill__origin=True)


class BillViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all bills, paged, filtered, sorted ....
    retrieve:Returns a single bill selected by `id`.
    """

    class BillSearchFilter(filters.SearchFilter):
        def filter_queryset(self, request, queryset, view):
            search_fields = self.get_search_fields(view, request)
            search_terms = self.get_search_terms(request)
            is_digit_bill = False
            if len(search_terms)==1 and search_terms[0][0].isdigit():
                is_digit_bill = True
                search_terms = [f'C.{search_terms[0]}', f'S.{search_terms[0]}']
            if not search_fields or not search_terms:
                return queryset

            orm_lookups = [
                self.construct_search(str(search_field))
                for search_field in search_fields
            ]

            base = queryset
            conditions = []

            if is_digit_bill:
                orm_lookups = ['identifier__iexact', 'previous_codes__contains_exact']
                for search_term in search_terms:
                    queries = [
                        filters.models.Q(**{orm_lookup: search_term})
                        for orm_lookup in orm_lookups
                    ]
                    if not '-' in search_term:
                        queries += [filters.models.Q(**{"identifier__istartswith":f"{search_term}-"}),
                                    filters.models.Q(**{"previous_codes__contains": [f"{search_term}-"]})]
                    conditions.append(filters.reduce(filters.operator.or_, queries))
                queryset = queryset.filter(filters.reduce(filters.operator.or_, conditions))
            else:
                for search_term in search_terms:
                    queries = [
                        filters.models.Q(**{orm_lookup: search_term})
                        for orm_lookup in orm_lookups
                    ]
                    conditions.append(filters.reduce(filters.operator.or_, queries))
                queryset = queryset.filter(filters.reduce(filters.operator.and_, conditions))
            if self.must_call_distinct(queryset, search_fields):
                # Filtering against a many-to-many field requires us to
                # call queryset.distinct() in order to avoid duplicate items
                # in the resulting queryset.
                # We try to avoid this if possible, for performance reasons.
                queryset = filters.distinct(queryset, base)
            return queryset
        lookup_prefixes = {
            '^': 'istartswith',
            '=': 'iexact',
            '@': 'search',
            '$': 'iregex',
            '#': 'contains_exact'
        }
    throttle_classes = []
    lookup_field = 'pk2'
    search_fields = ("=identifier", "#previous_codes", "original_title", "descriptive_title", "public_title")
    ord_fields = (("law_publication_date", "Data legge"),
                  ("last_status_date", "Data status"))
    ordering_fields = set((x[0] for x in ord_fields))
    ordering = ('-last_status_date',)
    filter_backends = (
        BillSearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = BillFilterSet
    schema = FiltersInListOnlySchema()
    pagination_class = ResultsWithBillCodelistPagination


    def get_object(self):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        queryset = self.filter_queryset(self.get_queryset_all())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return BillListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return BillDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset_all(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return Bill.objects.none()

        leg = self.kwargs['legislature']
        return Bill.objects.by_legislature(leg).annotate(
            pk2=Replace(Replace(F('identifier'), Value('.'), Value('_')),Value('/'), Value('-')),
            last_status_date=Max(F('steps__status_date')))

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return Bill.objects.none()

        leg = self.kwargs['legislature']
        return Bill.get_last_bill(leg).annotate(
            pk2=Replace(F('identifier'), Value('.'), Value('_')),
            last_status_date=Max(F('steps__status_date')))


    @action(detail=False, )
    def aggregate(self, request, pk=None, **kwargs):
        """ Mostra i progetti correlati"""
        return Response(Bill.get_api_aggregate())


class GovDecreeViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all bills, paged, filtered, sorted ....
    retrieve:Returns a single bill selected by `id`.
    """
    throttle_classes = []
    lookup_field = 'pk2'
    search_fields = ("original_title", "descriptive_title", "identifier")
    ord_fields = (("publication_gu_date", "Data legge"),)
    ordering_fields = set((x[0] for x in ord_fields))
    ordering = ('-publication_gu_date',)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = GovDecreeFilterSet
    schema = FiltersInListOnlySchema()
    pagination_class = ResultsWithGovDecreCodelistPagination

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return GovDecreeListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return GovDecreeDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        if self.request is None:
            return GovDecree.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        start_date = datetime.datetime.strptime(e.start_date, '%Y-%m-%d')
        start_date = start_date.strftime('%Y-%m-%d')
        return GovDecree.objects.by_date(sd=start_date,
                                         ed=(e.end_date or today_strf)).annotate(pk2=F('identifier'))

    @action(detail=False, )
    def aggregate(self, request, pk=None, **kwargs):
        """ Mostra i progetti correlati"""
        return Response(GovDecree.get_api_aggregate())


class GovBillViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all bills, paged, filtered, sorted ....
    retrieve:Returns a single bill selected by `id`.
    """
    throttle_classes = []
    lookup_field = "pk2"
    search_fields = ("original_title", "descriptive_title", "identifier")
    ord_fields = (("bill_date", "Data legge"),)
    ordering_fields = set((x[0] for x in ord_fields))
    ordering = ('-bill_date',)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = GovBillFilterSet
    schema = FiltersInListOnlySchema()
    pagination_class = ResultsWithGovBillCodelistPagination

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return GovBillListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return GovBillDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        if self.request is None:
            return GovBill.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        return GovBill.objects.by_date(sd=e.start_date,
                                       ed=(e.end_date or today_strf)).annotate(pk2=F('identifier'))

    @action(detail=False, )
    def aggregate(self, request, pk=None, **kwargs):
        """ Mostra i progetti correlati"""
        return Response(GovBill.get_api_aggregate())


class GroupViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all bills, paged, filtered, sorted ....
    retrieve:Returns a single bill selected by `id`.
    """
    throttle_classes = []
    lookup_field = 'pk2'
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    schema = FiltersInListOnlySchema()

    def get_object(self):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        queryset = self.filter_queryset(self.get_queryset())

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        obj = queryset.filter(Q(pk2=self.kwargs[lookup_url_kwarg])|Q(organization__other_names__name=self.kwargs[lookup_url_kwarg])).distinct().values('id')
        obj = queryset.get(id__in=obj)
        self.check_object_permissions(self.request, obj)

        return obj

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        cache_key = f'group_api:{instance.id}'
        cached_results = cache.get(cache_key)

        if cached_results:
            return Response(cached_results)
        else:
            cache.set(cache_key, serializer.data, timeout=None)
            return Response(serializer.data)

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return GroupListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return GroupDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return Group.objects.none()

        leg = self.kwargs['legislature']
        groups = Group.objects.by_legislature(leg=leg)
        chamber = self.basename
        if 'senato' in chamber:
            groups = groups.filter(organization__parent__name__icontains='senato')
        elif 'camera' in chamber:
            groups = groups.filter(organization__parent__name__icontains='camera')
        return groups.annotate(pk2=F('acronym'))

    @action(detail=False, )
    def aggregate(self, request, pk=None, **kwargs):
        branch = self.basename.split('-')[0]
        instance = Assembly.objects.get(organization__parent__name__icontains=branch,
                                        organization__key_events__key_event__identifier=f"ITL_{kwargs.get('legislature')}")
        serializer = GroupParliamentOrgDetailSerializer(instance)

        # serializer.context = super().get_serializer_context()
        serializer.context.update({"request": self.request})

        cache_key = f'parl_org_detail:{branch}_groups'
        cached_results = cache.get(cache_key)
        if cached_results:
            return Response(cached_results)
        else:
            cache.set(cache_key, serializer.data, timeout=None)
            return Response(serializer.data)


class ImplementingDecreeViewSet(viewsets.ReadOnlyModelViewSet):
    """
    list:    Returns all bills, paged, filtered, sorted ....
    retrieve:Returns a single bill selected by `id`.
    """
    throttle_classes = []
    ord_fields = (("adoption_date", "Data di adozione"),)
    ordering_fields = set((x[0] for x in ord_fields))
    search_fields = ("identifier", "original_title", "theme")
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = ImplementingDecreeFilterSet
    schema = FiltersInListOnlySchema()
    pagination_class = ResultsWithImplementingDecreeCodelistPagination

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return ImplementingDecreeListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return ImplementingDecreeDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return ImplementingDecree.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        govs = Government.get_governments_by_leg(leg)
        ids_gov = [x.organization.id for x in govs]
        return ImplementingDecree.objects.filter(is_valid=True).annotate(leg=Value(leg)).filter(
        (Q(is_adopted=False)|(Q(is_adopted=True) & Q(adoption_date__gte=ke.start_date))) | Q(government__in=ids_gov)
        ).annotate(date_ref=Coalesce(
            Coalesce(F('adoption_date'), F('deadline_date')), Cast(F('government__start_date'), fields.DateField()),
                                     output_field=fields.DateField())
                   ).order_by('-date_ref')

    @action(detail=False, )
    def aggregate(self, request, pk=None, **kwargs):
        """ Mostra i progetti correlati"""
        return Response(ImplementingDecree.get_api_aggregate())


class GovernmentViewSet(viewsets.ReadOnlyModelViewSet):
    throttle_classes = []
    lookup_field = 'pk2'

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        cache_key = f'government_detail:{instance.id}'
        cached_results = cache.get(cache_key)

        if cached_results:
            return Response(cached_results)
        else:
            cache.set(cache_key, serializer.data, timeout=None)
            return Response(serializer.data)

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return GovernmentListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return GovernmentDetailSerializer
        else:
            return super().get_serializer_class()

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        if self.request is None:
            return Organization.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        return Government.objects \
            .filter(organization__start_date__gte=e.start_date,
                    organization__start_date__lt=(e.end_date or today_strf)) \
            .annotate(pk2=F('organization__slug')) \
            .order_by('-organization__start_date')


class ParliamentOrgsViewSet(viewsets.ReadOnlyModelViewSet):
    throttle_classes = []

    def get_object(self):
        legislature = self.request.parser_context['kwargs']['legislature']
        pk = self.request.parser_context['kwargs']['pk']
        if pk == 'bichambers':
            self.request.parser_context['kwargs']['pk'] \
                = Assembly.objects.get(organization__name='Parlamento Italiano').id
            return Assembly.objects.get(organization__name='Parlamento Italiano')

        elif pk in ['camera', 'senato']:
            self.request.parser_context['kwargs']['pk'] \
                = Assembly.objects.get(organization__name__icontains=pk,
                                       organization__identifier__endswith=legislature).id
            return Assembly.objects.get(organization__name__icontains=pk,
                                        organization__identifier__endswith=legislature)

        return super(ParliamentOrgsViewSet, self).get_object()

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        cache_key = f"parliament_orgs_detail:{instance.id}_{request.parser_context['kwargs']['legislature']}"
        cached_results = cache.get(cache_key)

        if cached_results:
            return Response(cached_results)
        else:
            cache.set(cache_key, serializer.data, timeout=None)
            return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        serializer = self.get_serializer(queryset, many=True)
        result_dict = {}
        for obj in serializer.data:
            url = request.build_absolute_uri().rstrip('/')
            url = f"{url}/{obj['name'].lower()}/"
            result_dict[obj['name']] = url

        return Response(result_dict)

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return ParliamentOrgListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            if self.get_queryset().get(id=self.kwargs['pk']).organization.name == 'Parlamento Italiano':
                return BiChamberOrgDetailSerializer
            else:
                return ParliamentOrgDetailSerializer
        else:
            return super().get_serializer_class()

    def get_serializer_context(self):
        # Pass the `request` object to the serializer
        return {'request': self.request}

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        if self.request is None:
            return Organization.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        return Assembly.objects \
            .filter(Q(organization__name__icontains='Parlamento Italiano')
                    | Q(organization__start_date__gte=e.start_date,
                        organization__start_date__lt=(e.end_date or today_strf))) \
            .order_by('-organization__start_date')

    # @action(detail=False, )
    # def numbers(self, request, pk=None, **kwargs):
    #     # """ Mostra i progetti correlati"""
    #     leg = kwargs['legislature']
    #     numbers = NumbersParliament(leg).get_response()
    #
    #     return Response(numbers)


class LegislatureActivity(APIView):
    throttle_classes = []

    def get(self, request, legislature):
        cache_key = f"legislature_activity_{legislature}"
        cached_results = cache.get(cache_key)

        if cached_results:
            return Response(cached_results)
        else:
            numbers = NumbersParliament(legislature).get_response()
            cache.set(cache_key, numbers, timeout=None)
            return Response(numbers)


class ParliamentOrgsDetailViewSet(viewsets.ReadOnlyModelViewSet):
    throttle_classes = []

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        cache_key = f"parliament_orgs_detail:{instance.id}_{request.parser_context['kwargs']['legislature']}"
        cached_results = cache.get(cache_key)

        if cached_results:
            return Response(cached_results)
        else:
            cache.set(cache_key, serializer.data, timeout=None)
            return Response(serializer.data)

    def list(self, request, *args, **kwargs):
        branch = str(request.parser_context['request']).split('/')[-3]
        object_name = str(self.request).split('/')[-2]
        cache_key = f'parl_org_detail:{branch}_{object_name}'
        cached_results = cache.get(cache_key)

        if cached_results:
            return Response(cached_results)
        else:
            queryset = self.filter_queryset(self.get_queryset())

            queryset = queryset.get(organization__name__icontains=branch)
            serializer = self.get_serializer(queryset)
            cache.set(cache_key, serializer.data, timeout=None)
            return Response(serializer.data)

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            if str(self.request).split('/')[-2] == 'presidency':
                return PresidencyDetailSerializer
            elif str(self.request).split('/')[-2] == 'commission_councils':
                return CommissionCouncilOrgDetailSerializer
            elif str(self.request).split('/')[-2] == 'groups':
                return GroupParliamentOrgDetailSerializer

        elif hasattr(self, 'action') and self.action == 'retrieve':
            if self.get_queryset().get(id=self.kwargs['pk']).organization.name == 'Parlamento Italiano':
                return BiChamberOrgDetailSerializer
            else:
                return ParliamentOrgDetailSerializer
        else:
            return super().get_serializer_class()

    def get_serializer_context(self):
        # Pass the `request` object to the serializer
        return {'request': self.request}

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        if self.request is None:
            return Organization.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        return Assembly.objects \
            .filter(Q(organization__name__icontains='Parlamento Italiano')
                    | Q(organization__start_date__gte=e.start_date,
                        organization__start_date__lt=(e.end_date or today_strf))) \
            .order_by('-organization__start_date')




class PersonSearch(filters.SearchFilter):
    def filter_queryset(self, request, queryset, view):
        search_fields = self.get_search_fields(view, request)
        search_terms = self.get_search_terms(request)

        if not search_fields or not search_terms:
            return queryset

        orm_lookups = [
            self.construct_search(str(search_field))
            for search_field in search_fields
        ]

        base = queryset
        conditions = []
        for search_term in search_terms:
            queries = [
                filters.models.Q(**{orm_lookup: search_term})
                for orm_lookup in orm_lookups
            ]
            conditions.append(filters.reduce(filters.operator.or_, queries))
        queryset = queryset.filter(filters.reduce(filters.operator.and_, conditions))

        if self.must_call_distinct(queryset, search_fields):
            # Filtering against a many-to-many field requires us to
            # call queryset.distinct() in order to avoid duplicate items
            # in the resulting queryset.
            # We try to avoid this if possible, for performance reasons.
            queryset = filters.distinct(queryset, base)
        return queryset
    def construct_search(self, field_name):
        lookup = self.lookup_prefixes.get(field_name[0])
        if lookup:
            field_name = field_name[1:]
        else:
            lookup = 'icontains'
        return filters.LOOKUP_SEP.join([field_name, lookup])
    def get_search_terms(self, request):
        """
        Search terms are set by a ?search=... query parameter,
        and may be comma and/or whitespace delimited.
        """
        params = request.query_params.get(self.search_param, '')
        params = params.replace('\x00', '')  # strip null characters
        params = params.replace(',', ' ')
        return [params]


class PPIndexViewSet(viewsets.ReadOnlyModelViewSet):
    throttle_classes = []
    lookup_field = 'pk2'
    filter_backends = (
        PersonSearch,
        extra_filters.DjangoFilterBackend,
        PersonsOrdering
    )
    search_fields = ("^name", "^family_name", "^given_name")
    ordering_fields = ("family_name", "pp",)
    filter_class = PersonsFilterSet
    schema = FiltersInListOnlySchema()
    pagination_class = ResultsWithPersonsCodelistPagination

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return PPIndexListSerializer

        elif hasattr(self, 'action') and self.action == 'retrieve':
            return PersonsDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        # Pass the `request` object to the serializer
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def list(self, request, *args, **kwargs):
        if cache.get(f'pp_index_{self.request._request.get_raw_uri()}'):

            return  Response(OrderedDict(cache.get(f'pp_index_{self.request._request.get_raw_uri()}')))
        queryset = self.filter_queryset(self.get_queryset())
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            # data = serializer.data
            # cache.set(f'pp_index_{request.__str__()}', {'res': data, 'page':page}, timeout=60 * 60)
            return self.get_paginated_response(
                serializer.data
            )

        serializer = self.get_serializer(queryset, many=True)
        data = serializer.data
        cache.set(f'pp_index_{request.__str__()}', data, timeout=None)
        return Response(data)

    def get_object(self):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        queryset = PersonsOPP.objects.all_and_removed()

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return PersonsOPP.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        return PersonsOPP.objects \
            .all(sd=e.start_date,
                 ed=e.end_date) \
            .annotate(leg=Value(f"{leg}"),
                      pk2=Lower(F('slug')))

    @action(detail=True, )
    def carreer_positions(self, request, pk=None, **kwargs):
        from project.opp.votes.serializers import CareerPositionDetailSerializer
        queryset = self.get_object().get_carreer_positions(leg='19').order_by('-start_date')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = CareerPositionDetailSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = CareerPositionDetailSerializer(queryset, many=True)
        return Response(serializer.data)


class PersonsViewSet(viewsets.ReadOnlyModelViewSet):
    throttle_classes = []
    lookup_field = 'pk2'

    filter_backends = (
        PersonSearch,
        # filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
        PersonsOrdering
    )
    search_fields = ("^name", '^family_name', '^given_name', '^other_names__name')
    ordering_fields = ("family_name", "pp",)
    filter_class = PersonsFilterSet
    schema = FiltersInListOnlySchema()
    pagination_class = ResultsWithPersonsCodelistPagination

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        cache_key = f'person_opp_:{instance.id}'
        cached_results = cache.get(cache_key)

        if cached_results:
            return Response(cached_results)
        else:
            cache.set(cache_key,serializer.data, timeout=None)
            return Response(cache.get(cache_key))

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return PersonsListSerializer

        elif hasattr(self, 'action') and self.action == 'retrieve':
            return PersonsDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        # Pass the `request` object to the serializer
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def list(self, request, *args, **kwargs):

        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(
                serializer.data
            )

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_object(self):
        """
        Returns the object the view is displaying.

        You may want to override this if you need to provide non-standard
        queryset lookups.  Eg if objects are referenced using multiple
        keyword arguments in the url conf.
        """
        queryset = PersonsOPP.objects.all_and_removed()

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return PersonsOPP.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        return PersonsOPP.objects \
            .all_and_removed() \
            .annotate(leg=Value(f"{leg}"),
                      pk2=Lower(F('slug')))

    @action(detail=True, )
    def carreer_positions(self, request, pk=None, **kwargs):
        from project.opp.votes.serializers import CareerPositionDetailSerializer
        queryset = self.get_object().get_carreer_positions(leg='19').order_by('-start_date')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = CareerPositionDetailSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = CareerPositionDetailSerializer(queryset, many=True)
        return Response(serializer.data)


class TopicViewSet(viewsets.ReadOnlyModelViewSet):
    throttle_classes = []

    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )

    schema = FiltersInListOnlySchema()

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return TopicListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return TopicDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        if self.request is None:
            return Topic.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        return Topic.objects.all().order_by('ordering')


class EventViewSet(viewsets.ReadOnlyModelViewSet):
    throttle_classes = []
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = EventFilterSet
    search_fields = ("opp_events__title", "opp_events__description",)
    schema = FiltersInListOnlySchema()
    pagination_class = ResultsWithEventsCodelistPagination
    ordering_fields = ('date', )

    def get_serializer_class(self):
        """Return different serializer classes for list and detail view"""
        if hasattr(self, 'action') and self.action == 'list':
            return EventListSerializer
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return EventDetailSerializer
        else:
            return super().get_serializer_class()

    def get_filterset_kwargs(self):
        return {
            'legislature': self.kwargs.get('legislature', None),
        }

    def get_serializer_context(self):
        context = super().get_serializer_context()
        context.update({"request": self.request})
        return context

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_queryset(self):
        """Filter votings based on legislature identifier, passed in the URL"""

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        if self.request is None:
            return CalendarDaily.objects.none()

        leg = self.kwargs['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        return CalendarDaily.objects.filter(opp_events__isnull=False).filter(date__gte=e.start_date, date__lte=(e.end_date or today_strf)).distinct().order_by('-date')


class ActsView(APIView):
    throttle_classes = []
    def get(self, request, legislature):
        results = {
            'leg': legislature
        }
        return Response(results)
