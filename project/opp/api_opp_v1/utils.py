import statistics

from django.core.cache import cache
from popolo.models import KeyEvent, Organization
from django.db.models import F, Max

from project.opp.acts.models import Bill, GovDecree, ImplementingDecree
from project.opp.votes.models import Voting, MemberVote


def get_governmensts_by_leg(leg):
    legislature_identifier = f"ITL_{leg}"
    e = KeyEvent.objects.get(identifier=legislature_identifier)
    governi = Organization.objects.filter(
        classifications__classification__descr__icontains='governo della repubblica',
        start_date__gte=e.start_date
    ).annotate(value=F('name')).values('id', 'slug', 'value', 'start_date')
    governi_list = list(governi)
    for gov in governi_list:
        gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()
    return governi_list


class NumbersParliament:
    def __init__(self, leg):
        self.leg = leg

    def update_cache(self):
        res = self.data
        cache.set(
            f'numbers_parliament:{self.leg}',
            {'res': res},
            timeout=None
        )

    def get_response(self):
        return self.data

    @property
    def data(self):
        gov_decrees_api = GovDecree.get_api_aggregate()
        bills_law = Bill.get_bills_by_leg(self.leg).filter(id__in=Bill.get_bills_by_leg(self.leg).filter(iter_steps__phase__in=['Approvato definitivamente legge', 'Approvato definitivamente non ancora pubblicato']).exclude(iter_steps__phase__istartswith='Approvato con modificazioni').values('id').distinct())
        gov_decrees_list = GovDecree.objects.by_legislature(self.leg)
        gov_bills = bills_law.filter(initiative=Bill.GOVERNMENT)
        parl_bills = bills_law.exclude(initiative=Bill.GOVERNMENT)
        days_gov_bills = [{'id': x.id, 'days': (x.days_to_publication or 0)} for x in gov_bills if (x.days_to_publication or 0)>0]
        days_gov_bills = sorted(days_gov_bills, key=lambda d: d['days'])
        days_parl_bills = [{'id': x.id, 'days': (x.days_to_publication or 0)} for x in parl_bills if (x.days_to_publication or 0)>0]
        days_parl_bills = sorted(days_parl_bills, key=lambda d: d['days'])

        fasts_gov = days_gov_bills[:3]
        fasts_list_gov = []
        for f in fasts_gov:
            bill = Bill.objects.get(id=f['id'])
            fasts_list_gov.append(
                {"ref": bill.identifier,
                 "slug": bill.slug,
                 "title": (bill.public_title or bill.original_title),
                 "gg": f['days']},
            )

        fasts_parl = days_parl_bills[:3]
        fasts_list_parl = []
        for f in fasts_parl:
            bill = Bill.objects.get(id=f['id'])
            fasts_list_parl.append(
                {"ref": bill.identifier,
                 "slug": bill.slug,
                 "title": (bill.public_title or bill.original_title),
                 "gg": f['days']},
            )

        slows_gov = days_gov_bills[-3:][::-1]
        slows_list_gov = []
        for s in slows_gov:
            bill = Bill.objects.get(id=s['id'])
            slows_list_gov.append(
                {"ref": bill.identifier,
                 "slug": bill.slug,
                 "title": (bill.public_title or bill.original_title),
                 "gg": s['days']},
            )

        slows_parl = days_parl_bills[-3:][::-1]
        slows_list_parl = []
        for s in slows_parl:
            bill = Bill.objects.get(id=s['id'])
            slows_list_parl.append(
                {"ref": bill.identifier,
                 "slug": bill.slug,
                 "title": (bill.public_title or bill.original_title),
                 "gg": s['days']},
            )
        # decreti attuativi
        imp_api = ImplementingDecree.get_api_aggregate(self.leg)
        to_adopt = imp_api['cnt']['to_adopt']
        adopted = imp_api['cnt']['adopted']

        year_month_laws = []
        approvato_definitivamente_legge = (
            Bill.objects.by_legislature(leg='19').filter(steps__phase__phase='Approvato definitivamente legge')
            .exclude(steps__phase__phase='Approvato con modificazioni'))
        approvato_definitivamente_non_ancora_pubblicato = (
            (Bill.objects.by_legislature(leg='19').exclude(id__in=approvato_definitivamente_legge.values('id'))
             .filter(steps__phase__phase='Approvato definitivamente non ancora pubblicato'))
            .exclude(steps__phase__phase='Approvato con modificazioni'))
        approved_all = Bill.objects.filter(id__in=approvato_definitivamente_legge.union(approvato_definitivamente_non_ancora_pubblicato)
                                           .values('id')).annotate(last_status_date=Max(F('steps__status_date')))
        years = sorted(list(set(Bill.objects.by_legislature(leg='19').values_list('date_presenting__year', flat=True))))
        for year in years:
            year_month_laws.append({'year': year,
                                    'months': []})
            months = Bill.objects.by_legislature(leg='19').filter(date_presenting__year=year).values(
                'date_presenting__month').distinct('date_presenting__month') \
                .order_by('date_presenting__month').values_list('date_presenting__month', flat=True)
            for month in set(months):
                laws_y_m = approved_all.filter(last_status_date__year=year, last_status_date__month=month)
                year_month_laws[-1]['months'].append(
                    {
                        'name': month,
                        'value': laws_y_m.count()
                    }
                )
        # voti fiducia
        trust_votes = Voting.objects.by_legislature(19).filter(is_confidence=True).exclude(type=Voting.MOTION).order_by('sitting__date')
        chamber_trust_votes = trust_votes.filter(sitting__assembly__identifier__icontains='camera')
        senate_trust_votes = trust_votes.filter(sitting__assembly__identifier__icontains='senato')

        chamber_trust_votes_list = []
        for i in chamber_trust_votes:
            chamber_trust_votes_list.append({
                "date": i.sitting.date,
                "margin": abs(int(i.n_ayes - int(i.n_majority))),
                "title": (i.public_title or i.original_title),
                "slug": i.identifier})

        senate_trust_votes_list = []
        for i in senate_trust_votes:
            senate_trust_votes_list.append({
                "date": i.sitting.date,
                "margin": abs(int(i.n_ayes - int(i.n_majority))),
                "title": (i.public_title or i.original_title),
                "slug": i.identifier

            })

        trust_laws_count = [{'id': x.id, 'cnt': x.get_count_trust_votes} for x in bills_law]
        trust_laws_count = sorted(trust_laws_count, key=lambda d: d['cnt'])
        trust_laws_count_list = []
        for i in trust_laws_count[-3:][::-1]:
            bill_inst = Bill.objects.get(id=i['id'])
            trust_laws_count_list.append(
                {
                    "title": (bill_inst.public_title or bill_inst.original_title),
                    "votes": i['cnt'],
                    "bill": bill_inst.slug},
            )
        votings = MemberVote.objects.by_legislature(leg=self.leg)
        votings_senate = votings.filter(voting__sitting__assembly__name__icontains='senato')
        absent_senate = votings_senate.filter(vote__in=[MemberVote.MISSION, MemberVote.ABSENT])

        votings_chamber = votings.filter(voting__sitting__assembly__name__icontains='camera')
        absent_chamber = votings_chamber.filter(vote__in=[MemberVote.MISSION, MemberVote.ABSENT])
        res = {
            "codelists": {
                "governments": get_governmensts_by_leg(self.leg)
            },
            "gov_decrees": {
                "cnt": gov_decrees_api['cnt'],
                "days_pub_avg": gov_decrees_api['days_pub_avg'],
                "detail": gov_decrees_api['detail'],
                'detail_typology':{
                     'is_minotaurus': gov_decrees_list.filter(is_minotaurus=True).count(),
                     'is_omnibus': gov_decrees_list.filter(is_omnibus=True).count(),
                     'is_subject_to_agreements': gov_decrees_list.filter(is_subject_to_agreements=True).count(),
                 }
            },
            "avg_approval_time": {
                "distribution": [
                    {
                        "institution": "Governo",
                        "avg": round(statistics.mean([x['days'] for x in days_gov_bills if x['days']>0])),
                        "stacked_barplot": {
                            "_30": [x for x in days_gov_bills if x['days'] <= 30 and x['days']>=0].__len__(),
                            "30_60": [x for x in days_gov_bills if x['days'] > 30 and x['days'] <= 60].__len__(),
                            "60_120": [x for x in days_gov_bills if x['days'] > 60 and x['days'] <= 120].__len__(),
                            "120_": [x for x in days_gov_bills if x['days'] > 120].__len__()
                        }
                    },
                    {
                        "institution": "Parlamento",
                        "avg": round(statistics.mean([x['days'] for x in days_parl_bills if x['days']>0])),
                        "stacked_barplot": {
                            "_30": [x for x in days_parl_bills if x['days'] <= 30 and x['days']>=0].__len__(),
                            "30_60": [x for x in days_parl_bills if x['days'] > 30 and x['days'] <= 60].__len__(),
                            "60_120": [x for x in days_parl_bills if x['days'] > 60 and x['days'] <= 120].__len__(),
                            "120_": [x for x in days_parl_bills if x['days'] > 120].__len__()
                        }
                    }
                ],
                "extremes": {
                    "government": {
                        "slow":
                            slows_list_gov,
                        "fast": fasts_list_gov
                    },
                    "parliament": {
                        "slow": slows_list_parl,
                        "fast": fasts_list_parl
                    }
                }
            },
            "implementing_decrees": {
                "to_adopt": to_adopt,
                "adopted": adopted
            },
            "lawsdistribution_by_type": [
                {"name": "Conversione decreto legge", "value": bills_law.filter(type=Bill.DL_CONVERSION).count()},
                {"name": "Legge ordinaria",
                 "value": bills_law.filter(type=Bill.ORDINARY, is_ratification=False).count()},
                {"name": "Legge di bilancio", "value": bills_law.filter(type=Bill.FINANCIAL).count()},
                {"name": "Legge costituzionale", "value": bills_law.filter(type=Bill.COSTITUTIONAL).count()},
                {"name": "Legge di ratifica di accordi internazionali", "value": bills_law.filter(
                    is_ratification=True).count()}
            ],
            "who_makes_laws": {
                "government": [
                    {"name": "Iniziativa governativa", "value": gov_bills.exclude(is_ratification=True)
                    .exclude(type=Bill.DL_CONVERSION).count()},
                    {"name": "Conversione decreti legge", "value": gov_bills.exclude(is_ratification=True)
                    .filter(type=Bill.DL_CONVERSION).count()},
                    {"name": "Ratifica", "value": gov_bills.filter(is_ratification=True).count()}
                ],
                "parliament": [
                    {"name": "Iniziativa parlamentare", "value": parl_bills.exclude(is_ratification=True)
                    .filter(initiative=Bill.PARLIAMENT).count()},
                    {"name": "Regionale", "value": parl_bills.exclude(is_ratification=True)
                    .filter(initiative=Bill.REGION).count()},
                    {"name": "Popolare", "value": parl_bills.exclude(is_ratification=True)
                    .filter(initiative=Bill.POPULAR).count()},
                    {"name": "CNEL", "value": parl_bills.exclude(is_ratification=True)
                    .filter(initiative=Bill.CNEL).count()},
                    {"name": "Ratifica", "value": parl_bills.filter(is_ratification=True).count()}
                ]
            },
            "approved_laws":
                {'cnt': bills_law.count(),
                 'detail': {
                     'is_minotaurus': bills_law.filter(is_minotaurus=True).count(),
                     'is_omnibus': bills_law.filter(is_omnibus=True).count(),
                     'is_subject_to_agreements': bills_law.filter(is_subject_to_agreements=True).count(),
                     'ratification': bills_law.filter(is_ratification=True).count()
                 }},
            "historical": year_month_laws,
            "confidence_votes": {
                "chamber": chamber_trust_votes_list,
                "senate": senate_trust_votes_list
            },
            "governments": [{'date': x['start_date'], 'name': x['value']} for x in get_governmensts_by_leg(self.leg)],

            "laws_by_confidence_votes": trust_laws_count_list,
            'votings_absence': {
                'chamber': round(100*absent_chamber.count()/votings_chamber.count(), 1),
                'senate': round(100*absent_senate.count()/votings_senate.count(), 1)
            },
        }

        return res
