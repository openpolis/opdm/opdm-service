from django.apps import AppConfig


class ApiOppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'project.opp.api_opp_v1'
