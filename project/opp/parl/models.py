from typing import List

from django.db import models
from django.db.models import Value, F, fields, Sum, Window, Q, Case, When, Min, Count
from django.db.models.functions import Coalesce, Cast, Lower, Rank
from django.core.cache import cache
import pandas as pd
from popolo.models import Organization, KeyEvent
from rest_framework.exceptions import MethodNotAllowed

from project.calendars.models import CalendarDaily
from project.opp.gov.models import Government
from project.opp.votes.models import Group, MembershipGroup, MemberMajority, GroupMajority, \
    Membership  # MembershipGroup,
from project.opp.metrics.models import PositionalPower, PersonsOPP
import inspect
import re
import datetime
import logging
import math

def decode_roman_numeral(roman):
    """Calculate the numeric value of a Roman numeral (in capital letters)"""
    trans = {'I': 1, 'V': 5, 'X': 10, 'L': 50, 'C': 100, 'D': 500, 'M': 1000}
    values = [trans[r] for r in roman]
    return sum(
        val if val >= next_val else -val
        for val, next_val in zip(values[:-1], values[1:])
    ) + values[-1]


def whoami():
    frame = inspect.currentframe()
    return inspect.getframeinfo(frame).function


class LegislatureManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(event_type='ITL')


today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


def get_group(person, date):
    date = date.strftime('%Y-%m-%d')
    try:
        return MembershipGroup.objects.annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))). \
            get(membership__opdm_membership__person=person,
                start_date__lte=date,
                end_date_coalesce__gte=date
                ).group.id
    except:
        group = MembershipGroup.objects.annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))).\
            filter(membership__opdm_membership__person=person,
                start_date__lte=date).order_by('start_date').last().group
        if not group:
            return Group.objects.all_and_metagroups().get(
                legislature=Legislature.get_legislature_by_date(date),
                is_meta_group=True, acronym='Nessun gruppo').id
        return group.id


class Assembly(models.Model):
    organization = models.OneToOneField(
        Organization,
        on_delete=models.CASCADE,
        unique=True,
        limit_choices_to={'classification__in': ["Assemblea parlamentare", ]},
        related_name='opp_assembly')

    def update_cache(self, leg=None):

        logger = logging.getLogger(__name__)
        logger.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)

        if self.organization.name == 'Parlamento Italiano':
            logger.info('update_cache_bichamber_commissions')
            self.update_cache_bichamber_commissions(leg=leg)

            logger.info('update_cache_bichamber_commissions')
            self.update_cache_parliament_delegations(leg=leg)

            logger.info('update_cache_bichamber_commissions_closed')
            self.update_cache_bichamber_commissions_closed(leg=leg)
        else:
            logger.info('update_cache_positional_power')
            self.update_cache_positional_power()

            logger.info('update_cache_groups_power_presidency')
            self.update_cache_groups_power_presidency(f'{self.__class__.__name__}_{whoami()}:{self.id}')

            logger.info('update_cache_groups')
            self.update_cache_groups()

            logger.info('update_cache_commissions_councils')
            self.update_cache_commissions_councils()

            logger.info('update_cache_commissions_councils_closed')
            self.update_cache_commissions_councils_closed()

            logger.info('update_cache_others_commissions_councils')
            self.update_cache_others_commissions_councils()

            logger.info('update_cache_presidency')
            self.update_cache_presidency()

    def update_cache_positional_power(self):
        res = self._positional_power()
        cache.set(
            f'Assembly_positional_power:{self.id}',
            {'res': res},
            timeout=None
        )

    def positional_power(self):
        if not cache.get(f'Assembly_positional_power:{self.id}'):
            self.update_cache_positional_power()
        return cache.get(f'Assembly_positional_power:{self.id}').get('res')

    def _positional_power(self):
        ed = self.organization.end_date

        if self.branch() in ['Senato', 'Camera']:
            persons_values = PositionalPower.objects \
                .annotate(end_date_coalesce=Coalesce(F('end_date'), today.date())) \
                .filter(start_date__lte=(ed or today.date()),
                        end_date_coalesce__gte=(ed or today.date())) \
                .filter(
                Q(opdm_membership__organization__parent__name__icontains=self.branch()) | Q(
                    opdm_membership__organization__parent__name__icontains='Parlamento Italiano',
                    opdm_membership__person__in=Membership.objects.filter(
                        opdm_membership__organization=self.organization.id) \
                        .values_list('opdm_membership__person_id', flat=True)))



        else:
            persons_values = PositionalPower.objects \
                .annotate(end_date_coalesce=Coalesce(F('end_date'), today.date())) \
                .filter(start_date__lte=(ed or today.date()),
                        end_date_coalesce__gte=(ed or today.date())) \
                .filter(opdm_membership__organization__parent__name__icontains=self.branch())
        if not self.branch == 'Bicamerali':
            persons_values = persons_values.filter(
                opdm_membership__person__in=self.get_memberships(self.organization).values_list('person_id', flat=True))

        persons_values = persons_values.annotate(person=F('opdm_membership__person')).values('person', 'value')
        aggregated = persons_values.values('person').annotate(sum=Sum(F('value')))
        denom = sum(aggregated.values_list('sum', flat=True))
        aggregated_std = aggregated.annotate(std_value=(100 * F('sum')) / denom)
        return aggregated_std.order_by('-std_value').annotate(
            order=Window(
                expression=Rank(),
                order_by=[
                    F('std_value').desc(),
                ]))

    def branch(self):
        if self.organization.name == 'Parlamento Italiano':
            return 'Bicamerali'
        return self.organization.parent.name.split(' ')[0].title()

    @property
    def legislature(self):
        return self.organization \
            .key_events \
            .get(key_event__identifier__icontains='ITL') \
            .key_event

    @property
    def get_presidency(self):
        return self \
            .organization \
            .children \
            .filter(
            classification='Organo di presidenza parlamentare') \
            .order_by('start_date').last()

    def get_memberships(self, org):
        memberships = org.memberships.all()
        return memberships

    def get_persons(self, org):
        memberships = self.get_memberships(org)
        persons = memberships.values('person_id')
        return persons.distinct()

    def _groups_power(self, org, date, cache_hist=None):

        ed = date
        memberships = self.get_memberships(org)
        if org.classification == 'Assemblea parlamentare':
            sub_orgs = PositionalPower.objects.filter(
                opdm_membership__organization__parent__name__icontains=self.branch()) \
                .values_list(
                'opdm_membership__organization', flat=True).distinct()

            for sub_org in sub_orgs:
                memberships = memberships.union(self.get_memberships(Organization.objects.get(id=sub_org)))

        persons = self.get_persons(org)
        pp_historical = PositionalPower.objects.filter(opdm_membership_id__in=memberships.values('id')) \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), today.date()))
        pp = pp_historical \
            .filter(start_date__lte=(ed or today.date()),
                    end_date_coalesce__gte=(ed or today.date()))

        df_pp = pd.DataFrame(pp.annotate(person=F('opdm_membership__person'))
                             .values('person', 'value'))

        df_pp_historical = pd.DataFrame(pp_historical.annotate(person=F('opdm_membership__person'))
                                        .values('person', 'value', 'start_date', 'end_date_coalesce'))
        new_data = []

        # loop over each row in the original dataset
        for index, row in df_pp_historical.iterrows():
            start_date = row['start_date']
            end_date = row['end_date_coalesce']
            date_range = pd.date_range(start=start_date, end=end_date, freq='D')
            date_df = pd.DataFrame({'date': date_range})
            for column in df_pp_historical.columns:
                if column not in ['start_date', 'end_date_coalesce']:
                    date_df[column] = row[column]
            new_data.append(date_df)

        # concatenate all of the new DataFrames into a single DataFrame
        new_df = pd.concat(new_data, ignore_index=True)
        # new_df['group'] = None
        if cache_hist:
            two_weeks_before = (today - datetime.timedelta(days=14))
            date_rif = CalendarDaily.objects.get(year=two_weeks_before.year,
                                                 month=two_weeks_before.month, is_month_start=True)

            new_df = new_df[new_df['date'] >= date_rif.date.strftime('%Y-%m-%d')]
            memb_changed = MembershipGroup.objects.filter(
                Q(start_date__gte=date_rif) | Q(end_date__gte=date_rif)).annotate(
                person_id=F('membership__opdm_membership__person_id')).values('person_id').distinct().order_by(
                'person_id')
            list_to_exclude = list(memb_changed.values_list('person_id', flat=True))
            df_distinct_person = pd.DataFrame(new_df[~new_df['person'].isin(list_to_exclude)]['person'].unique(),
                                              columns=['person'])
            df_distinct_person['group'] = df_distinct_person.apply(lambda x: get_group(x['person'], today), axis=1)
            new_df = new_df.merge(df_distinct_person, how='left', on=['person'])
        if 'group' not in new_df.columns:
            new_df['group'] = None
        new_df['group'] = new_df.apply(lambda x: get_group(x['person'], x['date']) if not x['group'] else x['group'],
                                       axis=1)
        new_df['month_year'] = pd.to_datetime(new_df['date']).dt.to_period('M')
        new_df = new_df.sort_values(by='date')

        denom_pp = sum(pp.values_list('value', flat=True))

        membershipsgroups = MembershipGroup.objects.filter(membership__opdm_membership__person__in=persons). \
            annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))). \
            filter(start_date__lte=(ed or today_strf),
                   end_date_coalesce__gte=(ed or today_strf))
        df_membgroup = pd.DataFrame(membershipsgroups.annotate(person=F('membership__opdm_membership__person'))
                                    .values('person', 'group'))
        df_merge = pd.merge(df_pp, df_membgroup, how='outer', on='person')
        dict_grouped = df_merge.groupby('group')['value'].sum().to_dict()

        groups = \
            Group.objects.filter(organization__parent=self.organization).annotate(cnt=Count('membership_groups')).order_by('-cnt')
        list_groups = groups.values('id',
                                    'supports_majority',
                                    'acronym')
        for item in list_groups:
            item['id'] = Group.objects.get(id=item['id']).id
            item['name'] = Group.objects.get(id=item['id']).name
            item['acronym'] = Group.objects.get(id=item['id']).acronym_last
            item['start_date'] = Group.objects.get(id=item['id']).organization.start_date
            item['end_date'] = Group.objects.get(id=item['id']).organization.end_date
            item['color'] = Group.objects.get(id=item['id']).color
            item['slug'] = Group.objects.get(id=item['id']).slug
            item['branch'] = Group.objects.get(id=item['id']).branch.lower()
            item['pp'] = {}
            item['pp']['branch'] = Group.objects.get(id=item['id']).branch.upper()[0]
            if item['id']:
                item['pp']['pp_branch'] = round(100 * dict_grouped.get(int(item['id']), 0) / denom_pp, 1)
            else:
                item['pp']['pp_branch'] = None
            if cache_hist:
            # if cache.get(f"historical_data_{self.id}_{org.id}_{item['id']}", None):
                if Group.objects.get(id=item['id']).organization.end_date:
                    get_position = 'dissolved'
                elif item['supports_majority'] is True:
                    get_position = 'majority'
                elif item['supports_majority'] is False:
                    get_position = 'minority'
                else:
                    get_position = 'others'

                two_weeks_before = (today - datetime.timedelta(days=14))
                # historical_data = cache.get(f"historical_data_{self.id}_{org.id}_{item['id']}", None)
                historical_data = list(filter(lambda x: x['id'] == item['id'], cache_hist.get(get_position) \
                                             .get('detail')))

                if historical_data:
                    historical_data = historical_data[0].get('historical_pp')[
                                      :-CalendarDaily.objects.filter(date__gte=two_weeks_before, date__lte=today).values(
                                          'month').distinct().count()]

                    # historical_data = historical_data[
                    #                           :-CalendarDaily.objects.filter(date__gte=two_weeks_before, date__lte=today).values(
                    #                               'month').distinct().count()]
            else:
                historical_data = []
            for month_year in new_df['month_year'].unique():
                target_month_year = pd.Period(month_year, freq='M')
                filtered_df = new_df[new_df['month_year'] == target_month_year]
                res = {
                    'year': month_year.year,
                    'month': month_year.month,
                    'pp_branch': round(100 * filtered_df[filtered_df['group'] == (int(item['id']))]['value'].sum()
                                       / filtered_df['value'].sum(), 1)}
                historical_data.append(res)

            item['historical_pp'] = historical_data
            if historical_data:
                item['pp']['pp_branch'] = historical_data[-1].get('pp_branch')

            cache.set(f"historical_data_{self.id}_{org.id}_{item['id']}", item['historical_pp'], timeout=None)

        list_groups_sorted = sorted(list_groups, key=lambda d: d['pp']['pp_branch'], reverse=True)
        majority = list(filter(lambda x: x['supports_majority'] is True and not x['end_date'], list_groups_sorted))
        minority = list(filter(lambda x: x['supports_majority'] is False and not x['end_date'], list_groups_sorted))
        others = list(filter(lambda x: x['supports_majority'] is None and not x['end_date'], list_groups_sorted))
        dissolved = list(filter(lambda x: x['end_date'] is not None, list_groups_sorted))
        res = {
            'majority': {
                'pp_sum': round(sum(map(lambda x: x['pp']['pp_branch'], majority)), 2),
                'detail': majority
            },
            'minority': {
                'pp_sum': round(sum(map(lambda x: x['pp']['pp_branch'], minority)), 2),
                'detail': minority},
            'others': {
                'pp_sum': round(sum(map(lambda x: x['pp']['pp_branch'], others)), 2),
                'detail': others},
            'dissolved': {
                'pp_sum': round(sum(map(lambda x: x['pp']['pp_branch'], others)), 2),
                'detail': dissolved}

        }

        return res

    def update_cache_groups_power_presidency(self, name):
        cache_value = cache.get(name, None)
        if cache_value:
            cache_value = cache_value.get('res')
        res = self._groups_power(self.get_presidency, self.organization.end_date, cache_value)
        cache.set(
            name,
            {'res': res},
            timeout=None
        )

    @property
    def groups_power_presidency(self):
        if not cache.get(f'{self.__class__.__name__}_{whoami()}:{self.id}'):
            self.update_cache_groups_power_presidency(f'{self.__class__.__name__}_{whoami()}:{self.id}')
            return self._groups_power(self.get_presidency, self.organization.end_date)
        return cache.get(f'{self.__class__.__name__}_{whoami()}:{self.id}').get('res')

    def age_distribution(self, org):
        persons = self.get_persons(org)
        person_ages = persons.annotate(
            age=Value(today) - Cast(F('person__birth_date'), fields.DateField())
        )
        list_ages = [x['age'].days / 365.2425 for x in person_ages.values('age')]
        return {
            'avg': round((sum(list_ages) / len(list_ages)), 0),
            'distribution':
                {
                    '_35': len(list(filter(lambda x: x < 35, list_ages))),
                    '35_45': len(list(filter(lambda x: x >= 35 and x < 45, list_ages))),
                    '45_55': len(list(filter(lambda x: x >= 45 and x < 55, list_ages))),
                    '55_65': len(list(filter(lambda x: x >= 55 and x < 65, list_ages))),
                    '65_': len(list(filter(lambda x: x >= 65, list_ages)))
                }
        }

    def gender_distribution(self, org):
        persons = self.get_persons(org)
        return {
            'female': persons.filter(person__gender='F').count(),
            'male': persons.filter(person__gender='M').count()
        }

    def get_components(self, org):
        ed = org.end_date
        memberships = self.get_memberships(org).filter(end_date=org.end_date).annotate(
            category_ordering=Case(
                When(role__startswith='Presidente', then=Value(1)),
                When(role__startswith='Vicepresidente', then=Value(2)),
                When(role__startswith='Questore', then=Value(3)),
                When(role__startswith='Segretario', then=Value(4)),
                When(role__startswith='Capogruppo', then=Value(5)),
                When(role__startswith='Componente', then=Value(6)),
                default=Value(6))).order_by('category_ordering', 'person__family_name')
        memb_values = pd.DataFrame(memberships.values('person', 'person__name', 'person__image',
                                                      'person__slug', 'role'))
        memb_values['role'] = memb_values['role'].apply(lambda x: x.split(' ')[0])
        if self.organization.name == 'Parlamento Italiano':
            leg = re.findall(r'\(([XVI]*) legislatura\)', org.name)
            assemblies = Assembly.objects.filter(organization__classification='Assemblea parlamentare',
                                                 organization__name__icontains=f"Legislatura {leg[0]} ")

            first_chamber = pd.DataFrame(assemblies[0].positional_power())
            first_chamber['pp_branch_total_ordering'] = first_chamber.shape[0]
            second_chamber = pd.DataFrame(assemblies[1].positional_power())
            second_chamber['pp_branch_total_ordering'] = second_chamber.shape[0]
            merged_df = memb_values.merge(first_chamber, on='person', how='left')
            merged_df = merged_df.merge(second_chamber, on='person', how='left')
            merged_df['sum'] = merged_df[['sum_x', 'sum_y']].sum(axis=1)
            merged_df['std_value'] = merged_df[['std_value_x', 'std_value_y']].sum(axis=1)
            merged_df['order'] = merged_df[['order_x', 'order_y']].sum(axis=1)
            merged_df['pp_branch_total_ordering'] = merged_df[
                ['pp_branch_total_ordering_x', 'pp_branch_total_ordering_y']].sum(axis=1)
            merged_df.drop(columns=['sum_x', 'sum_y',
                                    'std_value_x', 'std_value_y',
                                    'order_x', 'order_y',
                                    'pp_branch_total_ordering_x', 'pp_branch_total_ordering_y'
                                    ], inplace=True)

        else:
            positional_power = pd.DataFrame(self.positional_power())
            merged_df = memb_values.merge(positional_power, on='person', how='left')
            merged_df['pp_branch_total_ordering'] = positional_power.shape[0]
        merged_df = merged_df[['person',
                               'person__name',
                               'person__image',
                               'person__slug',
                               'role',
                               'std_value',
                               'order',
                               'pp_branch_total_ordering']]
        membershipsgroups = MembershipGroup.objects.filter(
            membership__opdm_membership__person__in=merged_df['person'].to_list()). \
            annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))). \
            filter(start_date__lte=(ed or today_strf),
                   end_date_coalesce__gte=(ed or today_strf))

        df_membgroup = pd.DataFrame(membershipsgroups.annotate(person=F('membership__opdm_membership__person'))
                                    .values('person', 'group'))
        merged_df = merged_df.rename(columns={
            "person__name": "name",
            "person__image": "image",
            "person__slug": "slug",
            "std_value": "pp_branch",
            "order": "pp_branch_ordering"
        },
            errors="raise")
        merged_df = merged_df  # .sort_values(by='pp_ordering')
        df_merge = pd.merge(merged_df, df_membgroup, how='outer', on='person')
        df_merge = df_merge.rename(columns={
            "person": "id",
        },
            errors="raise")
        ids_groups = list(Group.objects.all().values_list('id', flat=True))
        df_merge = df_merge[df_merge['group'].isin(ids_groups)]
        df_merge['group'] = df_merge['group'].apply(lambda x: Group.objects.get(id=x).group_detail_parliament())
        main_branch = self.branch()[0]
        df_merge['pp'] = df_merge.apply(lambda x: {'pp_branch': x['pp_branch'],
                                                   'pp_branch_ordering': x['pp_branch_ordering'],
                                                   'pp_branch_total_ordering': x['pp_branch_total_ordering'],
                                                   'branch': main_branch if main_branch[0] != 'B' else x['group']['branch'][0].upper()}, axis=1)
        df_merge = df_merge.drop(columns=['pp_branch', 'pp_branch_ordering', 'pp_branch_total_ordering'])
        df_merge = df_merge.drop_duplicates(subset=['id'])

        return df_merge.to_dict(orient='records')

    @property
    def presidency(self):
        if not cache.get(f'presidency:{self.id}'):
            self.update_cache_presidency()
        return cache.get(f'presidency:{self.id}').get('res')

    def update_cache_presidency(self):
        res = self._presidency()
        cache.set(
            f'presidency:{self.id}',
            {'res': res},
            timeout=None
        )

    def _presidency(self):

        res = {
            'groups_power': self.groups_power_presidency,
            'age': self.age_distribution(self.get_presidency),
            'gender': self.gender_distribution(self.get_presidency),
            'components': self.get_components(self.get_presidency)
        }

        return res

    def update_cache_groups_power_commission_councils(self, org, date, name, cache_value=None):
        res = self._groups_power(org, date, cache_value)
        cache.set(
            name,
            {'res': res},
            timeout=None
        )

    def update_cache_groups_power_chamber(self, org, date, name):
        cache_value = cache.get(name, None)
        if cache_value:
            cache_value = cache_value.get('res')
        res = self._groups_power(org, date, cache_value)
        cache.set(
            name,
            {'res': res},
            timeout=None
        )

    def groups_power_commission_councils(self, org, date):
        # if not cache.get(f'{self.__class__.__name__}_groups_power_commission_councils:{org.id}'):

        cache_value = cache.get(f'{self.__class__.__name__}_groups_power_commission_councils:{org.id}', None)
        if cache_value:
            cache_value = cache_value.get('res')

        self.update_cache_groups_power_commission_councils(org,
                                                           date,
                                                           f'{self.__class__.__name__}'
                                                           f'_groups_power_commission_councils:{org.id}',
                                                           cache_value)
        # return self._groups_power(org, date)
        return cache.get(f'{self.__class__.__name__}_groups_power_commission_councils:{org.id}').get('res')

    @property
    def commissions_councils(self):
        if not cache.get(f'commissions_councils:{self.id}'):
            self.update_cache_commissions_councils()
        return cache.get(f'commissions_councils:{self.id}').get('res')

    def update_cache_commissions_councils(self):
        res = self._commissions_councils()
        cache.set(
            f'commissions_councils:{self.id}',
            {'res': res},
            timeout=None
        )

    def _commissions_councils(self):

        def get_commission_councils_components(org_id, date):
            return Organization.objects.get(id=org_id).memberships.annotate(
                end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))
            ) \
                .filter(end_date_coalesce=date)

        ed = self.organization.end_date
        orgs = self.organization.children.filter(
            (Q(classification='Commissione permanente parlamentare') & ~Q(name__icontains='Sottocommissione')) |
            Q(name__icontains='giunta')) \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
            .filter(end_date_coalesce=(ed or today_strf)) \
            .exclude(memberships__isnull=True)
        res = orgs.annotate(
            category_ordering=Case(
                When(classification='Commissione permanente parlamentare', then=Value(1)),
                # When(classification='Commissione speciale/straordinaria parlamentare', then=Value(2)),
                # When(classification='Commissione d\'inchiesta parlamentare monocamerale/bicamerale', then=Value(3)),
                When(classification__icontains='giunta', then=Value(10)),
                default=Value(5))).order_by(
            'category_ordering',
            Lower('classification')) \
            .values('id', 'name', 'classification', 'parent__name')
        res_list = list(res)

        for item in res_list:
            # print(item)
            org = Organization.objects.get(id=item['id'])
            org_members = get_commission_councils_components(item['id'], (ed or today_strf))
            item['order'] = 100
            if item['classification'] == 'Commissione permanente parlamentare':
                if item['name'].startswith('Sottocommissione'):
                    continue
                if 'senato' in item['parent__name'].lower():
                    item['order'] = int(item['name'].split(' ')[0][:-1])
                    if 'giunta' in item['name'].lower():
                        pattern = r'(.*) \('
                    else:
                        pattern = r"(\d+ª).*- (.*)\("
                    item['name'] = ' - '.join(re.findall(pattern, item['name'])[0]).strip()
                else:
                    item['order'] = decode_roman_numeral(item['name'].split(' ')[0])
                    if 'giunta' in item['name'].lower():
                        pattern = r'\((.*)\) Camera'
                        name_commission = re.findall(pattern, item['name'])[0]
                        item['name'] = f"{item['order']}ª - {name_commission.capitalize()}"
                    else:
                        pattern = r"\((.*)\) Camera"
                        name_commission = re.findall(pattern, item['name'])[0].strip()
                        item['name'] = f"{item['order']}ª - {name_commission.capitalize()}"
            item['perc_women'] = round((100 * org_members.filter(person__gender='F').count()) / org_members.count(), 1)
            persons_opp = PersonsOPP.objects.filter(id__in=org_members.values_list('person_id', flat=True))
            leg = self.legislature.identifier.split('_')[-1]
            item['margin'] = len([x for x in persons_opp if x.parliament_by_leg(leg).supports_majority]) - math.floor(
                org_members.count() / 2)
            item['groups_power'] = self.groups_power_commission_councils(org, date=self.organization.end_date)
            item['majority_first'] = item['groups_power']['majority']['detail'][0]
            item['minority_first'] = item['groups_power']['minority']['detail'][0]
            item['age'] = self.age_distribution(org)
            item['gender'] = self.gender_distribution(org)
            item['components'] = self.get_components(org)

        res_orderd = sorted(res_list, key=lambda d: d['order'])
        return res_orderd

    def groups_power_chamber(self, org, date):
        # if not cache.get(f'{self.__class__.__name__}_groups_power_chamber:{org.id}'):
        self.update_cache_groups_power_chamber(org,
                                               date,
                                               f'{self.__class__.__name__}'
                                               f'_groups_power_chamber:{org.id}')
        # return self._groups_power(org, date)
        return cache.get(f'{self.__class__.__name__}_groups_power_chamber:{org.id}').get('res')

    @property
    def groups(self):
        if not cache.get(f'groups:{self.id}'):
            self.update_cache_groups()
        return cache.get(f'groups:{self.id}').get('res')

    def update_cache_groups(self):
        from project.opp.parl.serializers import GroupParliamentOrgDetailSerializer
        serializer = GroupParliamentOrgDetailSerializer(self)
        leg = self.legislature.identifier.split("_")[1]
        serializer.context.update({"leg": leg})

        cache_value = cache.get(f'groups:{self.id}', None)
        if cache_value:
            cache_value = cache_value.get('res')
        # res = self._groups_power(self.get_presidency, self.organization.end_date, cache_value)
        res = self._groups(cache_value)
        cache.set(
            f'groups:{self.id}',
            {'res': res},
            timeout=None
        )
        cache_key = f'parl_org_detail:{self.branch()}_groups'
        cache.set(cache_key, serializer.data, timeout=None)

    def _groups(self, cache_value=None):
        gov = Government.objects.filter(organization__start_date__gte=self.organization.start_date,
                                        organization__start_date__lte=(self.organization.end_date or today)).order_by(
            'organization__start_date').last()

        membermajority = MemberMajority.objects.filter(government=gov.organization.id,
                                                       membership__opdm_membership__organization=self.organization)
        membermajority_actual = membermajority.annotate(end_date_coalesce=Coalesce(F('end_date'),
                                                                                   Value(today_strf))).filter(
            end_date_coalesce=today_strf)

        min_start_date = membermajority.aggregate(min=Min('start_date'))['min']
        groupssmajmin = GroupMajority.objects.filter(group__organization__parent=self.organization,
                                                     )

        groupsmaj = groupssmajmin.filter(value=True)
        groupsmin = groupssmajmin.filter(value=False)
        groupsothers = groupssmajmin.filter(value__isnull=True)

        maj_group = Group.objects.all_and_metagroups().get(
            is_meta_group=True,
            organization=self.organization,
            supports_majority=True)

        min_group = Group.objects.all_and_metagroups().get(
            is_meta_group=True,
            organization=self.organization,
            supports_majority=False)

        gained_majority = membermajority.filter(start_date__gt=min_start_date).filter(value=True)
        gained_majority_list = []

        for g in gained_majority:
            repl_member = False

            mg = MembershipGroup.objects.filter(start_date__gte=g.start_date, membership=g.membership).order_by(
                'start_date')
            mg2 = mg
            if mg.count() > 1:
                mg2 = MembershipGroup.objects.filter(start_date__gte=g.start_date, membership=g.membership).exclude(
                    group__acronym='Nessun gruppo')
            if mg.first():
                mg = mg.first()
                mg2 = mg2.first()
                group_in = {
                    'id': mg2.group.id,
                    'acronym': mg2.group.acronym_last,
                    'name': mg2.group.name_compact,
                    'slug': mg2.group.slug,
                    'branch': mg2.group.branch.lower(),
                    'color': mg2.group.color
                }
                if mg.start_date == mg.membership.opdm_membership.start_date:
                    repl_member = True
                    group_out = None
                else:
                    group = MembershipGroup.objects.filter(membership=mg.membership,
                                                           start_date__lt=mg.start_date).order_by(
                        'start_date').last().group
                    group_out = {
                        'id': group.id,
                        'acronym': group.acronym_last,
                        'name': group.name_compact,
                        'slug': group.slug,
                        'branch': group.branch.lower(),
                        'color': group.color
                    }
            gained_majority_list.append(
                {
                    'slug': g.membership.opdm_membership.person.slug,
                    'name': g.membership.opdm_membership.person.name,
                    'img': g.membership.opdm_membership.person.image,
                    'date': g.start_date,
                    'out': group_out,
                    'in': group_in,
                    'replaced_member': {
                        'slug': mg.membership.replaced_member.opdm_membership.person.slug,
                        'name': mg.membership.replaced_member.opdm_membership.person.name,
                        'img': mg.membership.replaced_member.opdm_membership.person.image
                    } if repl_member else None
                }
            )
        lost_majority = membermajority.filter(value=True, end_date__isnull=False)
        lost_majority_list = []

        for l in lost_majority:
            ml = MembershipGroup.objects.filter(end_date=l.end_date, membership=l.membership)
            group_in = None
            replaced_by = None
            if ml.first():
                ml = ml.first()
                group_out = {
                    'id': ml.group.id,
                    'acronym': ml.group.acronym_last,
                    'name': ml.group.name_compact,
                    'slug': ml.group.slug,
                    'branch': ml.group.branch.lower(),
                    'color': ml.group.color
                }
                if (datetime.datetime.strptime(ml.end_date, '%Y-%m-%d') + datetime.timedelta(days=1)).strftime(
                    '%Y-%m-%d') == ml.membership.opdm_membership.end_date:
                    replaced_by = getattr(ml.membership, 'replaced_membership_parliament', None)
                if ml.end_reason not in ('Decesso', 'Dimissioni'):
                    group = MembershipGroup.objects.filter(membership=ml.membership,
                                                           start_date__gt=ml.end_date).order_by(
                        'start_date').first().group
                    group_in = {
                        'id': group.id,
                        'acronym': group.acronym_last,
                        'name': group.name_compact,
                        'slug': group.slug,
                        'branch': group.branch.lower(),
                        'color': group.color
                    }
            lost_majority_list.append(
                {
                    'slug': l.membership.opdm_membership.person.slug,
                    'name': l.membership.opdm_membership.person.name,
                    'image': l.membership.opdm_membership.person.image,
                    'date': l.end_date,
                    'end_reason': l.membership.opdm_membership.end_reason,
                    'in': group_in,
                    'out': group_out,
                    'replaced_by': {
                        'slug': replaced_by.opdm_membership.person.slug,
                        'name': replaced_by.opdm_membership.person.name,
                        'img': replaced_by.opdm_membership.person.image
                    } if replaced_by else None
                }
            )

        gained_minority = membermajority.filter(start_date__gt=min_start_date).filter(value=False)
        gained_minority_list = []

        for g in gained_minority:
            repl_member = False
            mg = MembershipGroup.objects.filter(start_date=g.start_date, membership=g.membership)
            if mg.first():
                mg = mg.first()
                group_in = {
                    'id': mg.group.id,
                    'acronym': mg.group.acronym_last,
                    'name': mg.group.name_compact,
                    'slug': mg.group.slug,
                    'branch': mg.group.branch.lower(),
                    'color': mg.group.color
                }
                if mg.start_date == mg.membership.opdm_membership.start_date:
                    repl_member = True
                    group_out = None
                else:
                    group = MembershipGroup.objects.filter(membership=mg.membership,
                                                           start_date__lt=mg.start_date).order_by(
                        'start_date').last().group
                    group_out = {
                        'id': group.id,
                        'acronym': group.acronym_last,
                        'name': group.name_compact,
                        'slug': group.slug,
                        'branch': group.branch.lower(),
                        'color': group.color
                    }
            gained_minority_list.append(
                {
                    'slug': g.membership.opdm_membership.person.slug,
                    'name': g.membership.opdm_membership.person.name,
                    'img': g.membership.opdm_membership.person.image,
                    'date': g.start_date,
                    'out': group_out,
                    'in': group_in,
                    'replaced_member': {
                        'slug': mg.membership.replaced_member.opdm_membership.person.slug,
                        'name': mg.membership.replaced_member.opdm_membership.person.name,
                        'img': mg.membership.replaced_member.opdm_membership.person.image
                    } if repl_member else None
                }
            )
        lost_minority = membermajority.filter(value=False, end_date__isnull=False)
        lost_minority_list = []

        for l in lost_minority:
            ml = MembershipGroup.objects.filter(end_date=l.end_date, membership=l.membership)
            group_in = None
            replaced_by = None
            if ml.first():
                ml = ml.first()
                group_out = {
                    'id': ml.group.id,
                    'acronym': ml.group.acronym_last,
                    'name': ml.group.name_compact,
                    'slug': ml.group.slug,
                    'branch': ml.group.branch.lower(),
                    'color': ml.group.color
                }
                if (datetime.datetime.strptime(ml.end_date, '%Y-%m-%d') + datetime.timedelta(days=1)).strftime(
                    '%Y-%m-%d') == ml.membership.opdm_membership.end_date:
                    replaced_by = getattr(ml.membership, 'replaced_membership_parliament', None)
                if ml.end_reason not in ('Decesso', 'Dimissioni'):
                    group = MembershipGroup.objects.filter(membership=ml.membership,
                                                           start_date__gt=ml.end_date).order_by(
                        'start_date').first().group
                    group_in = {
                        'id': group.id,
                        'acronym': group.acronym_last,
                        'name': group.name_compact,
                        'slug': group.slug,
                        'branch': group.branch.lower(),
                        'color': group.color
                    }

            lost_minority_list.append(
                {
                    'slug': l.membership.opdm_membership.person.slug,
                    'name': l.membership.opdm_membership.person.name,
                    'img': l.membership.opdm_membership.person.image,
                    'date': l.end_date,
                    'end_reason': l.membership.opdm_membership.end_reason,
                    'in': group_in,
                    'out': group_out,
                    'replaced_by': {
                        'slug': replaced_by.opdm_membership.person.slug,
                        'name': replaced_by.opdm_membership.person.name,
                        'img': replaced_by.opdm_membership.person.image
                    } if replaced_by else None
                }
            )

        res = {
            'majority_minority': {
                'majority': {
                    'cnt': membermajority_actual.filter(value=True).count(),
                    'gained': gained_majority.count(),
                    'lost_gained_list': sorted(gained_majority_list + lost_majority_list, key=lambda x: x['date'],
                                               reverse=True),
                    'lost': lost_majority.count(),
                    # 'lost_list': lost_majority_list,
                    'groups': [x.group.group_detail_parliament() for x in groupsmaj if not x.group.organization.end_date]
                },
                'minority': {
                    'cnt': membermajority_actual.filter(value=False).count(),
                    'gained': gained_minority.count(),
                    'lost_gained_list': sorted((gained_minority_list + lost_minority_list), key=lambda x: x['date'],
                                               reverse=True),
                    'lost': lost_minority.count(),
                    # 'lost_list': lost_minority_list,
                    'groups': [x.group.group_detail_parliament() for x in groupsmin if not x.group.organization.end_date]
                },
                'others': {
                    'groups': [x.group.group_detail_parliament() for x in groupsothers if not x.group.organization.end_date]
                },

                'dissolved': {
                    'groups': [x.group.group_detail_parliament() for x in list(groupsothers) + list(groupsmin) + list(groupsmaj)  if
                                x.group.organization.end_date]
                }
            },
            'groups_power': self.groups_power_chamber(self.organization, self.organization.end_date),
            'cohesion': {
                'majority': {
                    'value': maj_group.cohesion_rate,
                    'groups': [x.group.group_cohesion_rate_detail() for x in groupsmaj if not x.group.organization.end_date]
                },
                'minority': {
                    'value': min_group.cohesion_rate,
                    'groups': [x.group.group_cohesion_rate_detail() for x in groupsmin if not x.group.organization.end_date]
                },
                'others': {
                    'groups': [x.group.group_cohesion_rate_detail() for x in groupsothers if not x.group.organization.end_date]
                }
            },
            'list_groups': [x.group.group_detail_parliament() for x in groupssmajmin]
        }
        return res

    def bichamber_commissions(self, leg=None):
        if not cache.get(f'bichamber_commissions_{leg}:{self.id}'):
            self.update_cache_bichamber_commissions(leg)
        return cache.get(f'bichamber_commissions_{leg}:{self.id}').get('res')

    def _bichamber_commissions(self, leg=None):
        if not self.organization.name == 'Parlamento Italiano':
            return MethodNotAllowed
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        orgs = Organization.objects \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
            .filter(parent=self.organization.id,
                    classification__icontains='Commissione') \
            .filter(start_date__gte=ke.start_date,
                    end_date_coalesce=(ke.end_date or today_strf)).order_by('-start_date')

        return [{
            'name': x.name,
            'typology': x.classification,
            'start_date': x.start_date,
            'perc_women': round(
                100 * self.gender_distribution(x).get('female') / (self.gender_distribution(x).get('female')
                                                                   + self.gender_distribution(x)
                                                                   .get('male')), 1),
            'gender': self.gender_distribution(x),
            'age': self.age_distribution(x),
            'components': self.get_components(x)
        } for x in orgs]

    def parliament_delegations(self, leg=None):
        if not cache.get(f'parliament_delegations_{leg}:{self.id}'):
            self.update_cache_parliament_delegations()
        return cache.get(f'parliament_delegations_{leg}:{self.id}').get('res')

    def _parliament_delegations(self, leg=None):
        if not self.organization.name == 'Parlamento Italiano':
            return MethodNotAllowed
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        orgs = Organization.objects \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
            .filter(classification='Delegazione parlamentare presso assemblea internazionale') \
            .filter(start_date__gte=ke.start_date,
                    end_date_coalesce=(ke.end_date or today_strf)).order_by('-start_date')

        return [{
            'name': x.name,
            'typology': x.classification,
            'start_date': x.start_date,
            'perc_women': round(
                100 * self.gender_distribution(x).get('female') / (self.gender_distribution(x).get('female')
                                                                   + self.gender_distribution(x)
                                                                   .get('male')), 1) if (self.gender_distribution(x).get('female')
                                                                   + self.gender_distribution(x)
                                                                   .get('male')) > 0 else 0,
            'gender': self.gender_distribution(x),
            'age': self.age_distribution(x),
            'components': self.get_components(x)
        } for x in orgs if x.memberships.count()]

    def update_cache_parliament_delegations(self, leg=None):
        res = self._parliament_delegations(leg)
        cache.set(
            f'parliament_delegations_{leg}:{self.id}',
            {'res': res},
            timeout=None
        )

    def update_cache_bichamber_commissions(self, leg=None):
        res = self._bichamber_commissions(leg)
        cache.set(
            f'bichamber_commissions_{leg}:{self.id}',
            {'res': res},
            timeout=None
        )

    @property
    def commissions_councils_closed(self):
        if not cache.get(f'commissions_councils_closed:{self.id}'):
            self.update_cache_commissions_councils_closed()
        return cache.get(f'commissions_councils_closed:{self.id}').get('res')

    def update_cache_commissions_councils_closed(self):
        res = self._commissions_councils_closed()
        cache.set(
            f'commissions_councils_closed:{self.id}',
            {'res': res},
            timeout=None
        )

    def _commissions_councils_closed(self):

        def change_name_commissions_councils(name):
            return name

        ed = self.organization.end_date
        orgs = self.organization.children.filter(Q(classification='Commissione permanente parlamentare') |
                                                 Q(name__icontains='giunta')) \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
            .filter(end_date_coalesce__lt=(ed or today_strf)) \
            .exclude(memberships__isnull=True).order_by('-end_date')

        return [{
            'name': change_name_commissions_councils(x.name),
            'typology': x.classification,
            'start_date': x.start_date,
            'end_date': x.end_date,
            'perc_women': round(
                100 * self.gender_distribution(x).get('female') / (self.gender_distribution(x).get('female')
                                                                   + self.gender_distribution(x)
                                                                   .get('male')), 1),
            'gender': self.gender_distribution(x),
            'age': self.age_distribution(x),
            'components': self.get_components(x)
        } for x in orgs]

    @property
    def others_commissions_councils(self):
        if not cache.get(f'others_commissions_councils:{self.id}'):
            self.update_cache_others_commissions_councils()
        return cache.get(f'others_commissions_councils:{self.id}').get('res')

    def update_cache_others_commissions_councils(self):
        res = self._others_commissions_councils()
        cache.set(
            f'others_commissions_councils:{self.id}',
            {'res': res},
            timeout=None
        )

    def _others_commissions_councils(self):

        def change_name_commissions_councils(name):
            return name

        ed = self.organization.end_date
        orgs = self.organization.children.filter(
            classification__in=['Commissione d\'inchiesta parlamentare monocamerale/bicamerale',
                                'Commissione speciale/straordinaria parlamentare']) \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
            .exclude(memberships__isnull=True).order_by('-end_date').filter(end_date__isnull=True)

        return [{
            'name': change_name_commissions_councils(x.name),
            'typology': x.classification,
            'start_date': x.start_date,
            'end_date': x.end_date,
            'perc_women': round(
                100 * self.gender_distribution(x).get('female') / (self.gender_distribution(x).get('female')
                                                                   + self.gender_distribution(x)
                                                                   .get('male')), 1),
            'gender': self.gender_distribution(x),
            'age': self.age_distribution(x),
            'components': self.get_components(x)
        } for x in orgs]

    def bichamber_commissions_closed(self, leg=None):
        if not cache.get(f'bichamber_commissions_closed_{leg}:{self.id}'):
            self.update_cache_bichamber_commissions_closed()
        return cache.get(f'bichamber_commissions_closed_{leg}:{self.id}').get('res')

    def update_cache_bichamber_commissions_closed(self, leg=None):
        res = self._bichamber_commissions_closed(leg)
        cache.set(
            f'bichamber_commissions_closed_{leg}:{self.id}',
            {'res': res},
            timeout=None
        )

    def _bichamber_commissions_closed(self, leg=None):

        if not self.organization.name == 'Parlamento Italiano':
            return MethodNotAllowed
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        orgs = Organization.objects \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
            .filter(parent=self.organization.id,
                    classification__icontains='Commissione') \
            .filter(start_date__gte=ke.start_date,
                    end_date_coalesce__lt=(ke.end_date or today_strf))

        return [{
            'name': x.name,
            'typology': x.classification,
            'start_date': x.start_date,
            'end_date': x.end_date,
            'perc_women': round(
                100 * self.gender_distribution(x).get('female') / (self.gender_distribution(x).get('female')
                                                                   + self.gender_distribution(x)
                                                                   .get('male')), 1),
            'gender': self.gender_distribution(x),
            'age': self.age_distribution(x),
            'components': self.get_components(x)
        } for x in orgs]

    def __str__(self):
        return self.organization.__str__()


class Legislature(KeyEvent):
    objects = LegislatureManager()

    @property
    def legislature_number(self) -> int:
        """
        :return: The legaslature number.

        """
        return int(self.identifier.split('_')[-1])

    @classmethod
    def get_legislature_by_date(cls, date):
        """
        Find the legislature for a given date.

        :param date: The date to find the legislature for.
        :type date: datetime.date

        :return: The legislature for the given date, or None if no legislature is found.
        :rtype: YourLegislatureModel or None
        """
        return cls.objects.filter(Q(start_date__lte=date), Q(end_date__gte=date) | Q(end_date=None)).first()

    @classmethod
    def get_current_legislature(cls):
        """
        Get the current legislature.

        :return: The current legislature object.
        :rtype: Legislature
        """
        return cls.get_legislature_by_date(today)

    @classmethod
    def get_list_legislature(cls) -> list:
        current_legislature = cls.get_current_legislature()
        legislature_number = current_legislature.legislature_number
        return list(range(1, legislature_number + 1))

    @classmethod
    def get_legislature_by_number(cls, number: int) -> KeyEvent:
        leg = str(number).rjust(2, '0')
        return cls.objects.get(identifier=f"ITL_{leg}")

    class Meta:
        proxy = True
        ordering = ('start_date', )
