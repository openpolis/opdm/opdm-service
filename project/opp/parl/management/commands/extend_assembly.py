from django.db.models import F, Q
from popolo.models import Organization
from project.opp.parl.models import Assembly
from taskmanager.management.base import LoggingBaseCommand
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor
import pandas as pd


def get_assemblies():
    return Organization.objects.filter(
        Q(classification__in=['Assemblea parlamentare']) |
        Q(name__icontains='Parlamento Italiano')
    ).annotate(organization_id=F('id')).values('organization_id')


class Command(LoggingBaseCommand):
    def handle(self, *args, **kwargs):
        ETL(
            extractor=DataframeExtractor(pd.DataFrame(get_assemblies())),
            loader=DjangoUpdateOrCreateLoader(django_model=Assembly))()
