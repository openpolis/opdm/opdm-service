from django.db.models import F, Value
from django.db.models.functions import Coalesce
from popolo.models import KeyEvent

from project.opp.acts.serializers import GroupDetailSerializer
from project.opp.api_opp_v1.utils import NumbersParliament
from project.opp.parl.models import Assembly
import datetime
from taskmanager.management.base import LoggingBaseCommand
from django.core.cache import cache

from project.opp.parl.serializers import GroupParliamentOrgDetailSerializer
from project.opp.votes.models import Group
import logging
from django.core.management import call_command
today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):
    help = 'Create materialized view if it does not exist'

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )
    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.

        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        # self.logger.info(f"----- Starting {command_name} -----")
        # start_time = time.time()
        call_command(command_name, *args, **options, stdout=self.stdout)
        # elapsed = time.time() - start_time
        # self.logger.info(f"----- Completed in {elapsed:.2f}s -----")
    def handle(self, *args, **kwargs):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **kwargs)
        self.logger.setLevel(
            {
                0: logging.ERROR,
                1: logging.WARNING,
                2: logging.INFO,
                3: logging.DEBUG,
            }.get(kwargs["verbosity"])
        )
        leg = kwargs['legislatura']
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)

        self.logger.info("Groups")
        groups = Group.objects.by_legislature(leg)
        for x in groups:
            x.update_cache_group_detail_parliament()

        orgs = Assembly.objects\
            .annotate(end_date_coalesce=Coalesce(F('organization__end_date'), Value(today_strf)),
                      start_date_coalesce=Coalesce(F('organization__start_date'), Value(today_strf)))\
            .filter(
                start_date_coalesce__gte=ke.start_date,
                end_date_coalesce__lte=(ke.end_date or today_strf))
        self.logger.info("Org")
        for org in orgs:
            self.logger.info(f"Update cache for {org.organization.name}")
            org.update_cache(leg)

        for assembly in ['camera', 'senato']:
            for sub_class in ['groups', 'presidency', 'commission_councils']:
                cache.delete(f"parl_org_detail:{assembly}_{sub_class}")
        parl = Assembly.objects.get(organization__name='Parlamento Italiano')
        cache.delete(f"parliament_orgs_detail:{parl.id}_{leg}")
        self.logger.info("Branches")
        for branch in ['camera', 'senato']:
            instance = Assembly.objects.get(organization__parent__name__icontains=branch,
                                            organization__key_events__key_event__identifier=f"ITL_{leg}")


            serializer = GroupParliamentOrgDetailSerializer(instance)

            # serializer.context = super().get_serializer_context()
            serializer.context.update({"leg": leg})

            cache_key = f'parl_org_detail:{branch}_groups'
            # cached_results = cache.get(cache_key)
            cache.set(cache_key, serializer.data, timeout=None)

        keys_to_delete = cache.keys('*groups_api*')
        [cache.get(x).delete() for x in keys_to_delete]
        self.logger.info("Leg activity")
        cache_key = f"legislature_activity_{leg}"
        numbers = NumbersParliament(leg).get_response()
        cache.set(cache_key, numbers, timeout=None)
        for x in groups:
            cache_key = f'group_api:{x.id}'
            x.legislature_num = leg
            serializer = GroupDetailSerializer(x)
            cache.set(cache_key, serializer.data, timeout=None)


