from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField
from project.opp.serializers import BaseSerializerOPP
from project.opp.parl.models import Assembly


class LegislatureHyperlinkedIdentityField(HyperlinkedIdentityField):

    def get_url(self, obj, view_name, request, format):
        """
        Given an object, return the URL that hyperlinks to the object,
        considering the legislature
        """
        # Unsaved objects will not yet have a valid URL.
        if hasattr(obj, 'pk') and obj.pk is None:
            return None
        if hasattr(obj, 'pk2') and obj.pk2 is not None:
            self.lookup_field = 'pk2'

        lookup_value = getattr(obj, self.lookup_field)
        kwargs = {
            'legislature': request.parser_context['kwargs']['legislature'],
            self.lookup_field: lookup_value,
        }
        return self.reverse(view_name, kwargs=kwargs, request=request, format=format)


class ParliamentOrgListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Government model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='parliamentorg-detail',
    )
    name = serializers.CharField(source='branch')

    class Meta:
        """Define serializer Meta."""

        model = Assembly
        ref_name = "Parliament Orgs"
        fields = (
            'id',
            'url',
            'name',
        )


class ParliamentOrgDetailSerializer(BaseSerializerOPP):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='parliamentorg-detail',
    )
    name = serializers.CharField(source='branch')
    ppg_base = serializers.IntegerField(default=10)
    presidency = serializers.DictField()
    commissions_councils = serializers.ListField()
    groups = serializers.DictField()

    class Meta:
        """Define serializer Meta."""

        model = Assembly
        ref_name = "Parliament Organization"
        fields = (
            'codelists',
            'id',
            'url',
            'name',
            'ppg_base',
            'presidency',
            'commissions_councils',
            'groups'
        )


class PresidencyDetailSerializer(BaseSerializerOPP):
    """A serializer for the Voting model when seen in lists."""

    presidency = serializers.DictField()

    class Meta:
        """Define serializer Meta."""

        model = Assembly
        ref_name = "Parliament Organization"
        fields = (
            'codelists',
            'presidency',
        )


class CommissionCouncilOrgDetailSerializer(BaseSerializerOPP):
    """A serializer for the Voting model when seen in lists."""
    commissions_councils = serializers.ListField()
    commissions_councils_closed = serializers.ListField()
    others_commissions_councils = serializers.ListField()

    class Meta:
        """Define serializer Meta."""

        model = Assembly
        ref_name = "Parliament Organization"
        fields = (
            'codelists',
            'commissions_councils',
            'commissions_councils_closed',
            'others_commissions_councils'
        )


class GroupParliamentOrgDetailSerializer(BaseSerializerOPP):
    """A serializer for the Voting model when seen in lists."""

    groups = serializers.DictField()

    class Meta:
        """Define serializer Meta."""

        model = Assembly
        ref_name = "Parliament Organization"
        fields = (
            'codelists',
            'groups',
        )


class BiChamberOrgDetailSerializer(BaseSerializerOPP):

    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='parliamentorg-detail',
    )
    bichambers = serializers.SerializerMethodField()
    parliament_delegations = serializers.SerializerMethodField()
    bichambers_closed = serializers.SerializerMethodField()

    def get_bichambers(self, obj):
        return obj.bichamber_commissions(leg=self.context['request'].parser_context['kwargs']['legislature'])

    def get_parliament_delegations(self, obj):
        return obj.parliament_delegations(leg=self.context['request'].parser_context['kwargs']['legislature'])

    def get_bichambers_closed(self, obj):
        return obj.bichamber_commissions_closed(leg=self.context['request'].parser_context['kwargs']['legislature'])

    class Meta:
        """Define serializer Meta."""

        model = Assembly
        ref_name = "Bichamber Parliament Organizations"
        fields = (
            'codelists',
            'id',
            'url',
            'bichambers',
            'parliament_delegations',
            'bichambers_closed'

        )
