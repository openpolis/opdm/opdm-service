from project.opp.gov.models import Government
import datetime
from taskmanager.management.base import LoggingBaseCommand
from django.core.cache import cache

from project.opp.gov.serializers import GovernmentDetailSerializer
from project.opp.parl.models import Legislature

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):
    help = 'Create materialized view if it does not exist'

    def handle(self, *args, **kwargs):
        gov = Government.objects.first()
        gov.update_cache()
        # instance = self.get_object()
        gov.legislature_num = Legislature.get_legislature_by_date(gov.start).legislature_number
        serializer = GovernmentDetailSerializer(gov)
        cache_key = f'government_detail:{gov.id}'
        cache.set(cache_key, serializer.data, timeout=None)

