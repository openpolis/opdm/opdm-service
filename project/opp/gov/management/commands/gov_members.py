from taskmanager.management.base import LoggingBaseCommand
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor
from ooetl.transformations import Transformation
from project.opp.gov.models import Member
from django.db.models import Min, Max
import datetime
import pandas as pd
from project.opp.gov.management.commands.utils import gov_memberships




class GovMembersTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self):
        Transformation.__init__(self)

    def transform(self):
        od = self.etl.original_data.copy()
        od = od.rename(columns={"start_date_gov_members": "start_date",
                                "end_date_gov_members": "end_date"}, errors="raise")
        self.etl.processed_data = od

        return self.etl


class Command(LoggingBaseCommand):
    help = 'Create materialized view if it does not exist'

    def handle(self, *args, **kwargs):
        gov_members = gov_memberships.values(
            'person_id',
            'government_id',).annotate(
            start_date_gov_members=Min('start_date_datatype'),
            end_date_gov_members=Max('end_date_datatype'),)

        ETL(
            extractor=DataframeExtractor(pd.DataFrame(gov_members)),
            transformation=GovMembersTransformation(),
            loader=DjangoUpdateOrCreateLoader(django_model=Member, fields_to_update=['start_date',
                'end_date'
            ]))()
