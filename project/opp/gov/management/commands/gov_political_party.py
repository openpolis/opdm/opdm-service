from project.opp.gov.models import Member, GovPartyMember
import datetime
from taskmanager.management.base import LoggingBaseCommand



class Command(LoggingBaseCommand):
    help = 'Create materialized view if it does not exist'

    def handle(self, *args, **kwargs):

        gov_memberships = Member.objects.filter(start_date__gte='2021-02-12')

        for gov_membership in gov_memberships:
            obj, created = GovPartyMember.objects.get_or_create(
                gov_member=gov_membership,
                start_date=gov_membership.start_date
            )
            # if not created:
            obj.end_date = gov_membership.end_date
            obj.save()
