from django.db.models import Q, OuterRef, Subquery, F, DateField, Value
from popolo.models import Membership as OPDMMembership, Organization
from django.db.models.functions import Cast
from django.db.models.functions import Coalesce
import datetime

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')

# Subquery governments
governments = Organization.objects.filter(classification='Governo della Repubblica') \
    .annotate(
    start_date_datetype=Cast(
        F('start_date'), output_field=DateField()
    ),
    end_date_coalesced=Cast(
        Coalesce(F('end_date'), Value(today_strf)), output_field=DateField()
    )
) \
    .values('id', )

# Select gov members
gov_memberships = OPDMMembership.objects.select_related('organization', 'person', ).exclude(
    role__icontains='generale').filter(
    Q(organization__classification__icontains='ministero') |
    Q(organization__classification__icontains='presidenza del consiglio')).annotate(
    start_date_datatype=Cast(
        F('start_date'), output_field=DateField()),
    end_date_datatype=Cast(
        F('end_date'), output_field=DateField()),

).annotate(
    government_id=Subquery(governments.filter(
        start_date_datetype__lte=OuterRef('start_date_datatype'),
        end_date_coalesced__gt=OuterRef('start_date_datatype')).values('id')[:1]))
