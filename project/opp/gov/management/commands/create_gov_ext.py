from taskmanager.management.base import LoggingBaseCommand
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor
from project.opp.gov.models import Government, PoliticalParty
from django.db.models import F
from popolo.models import Organization
import pandas as pd


class Command(LoggingBaseCommand):
    help = 'Create materialized view if it does not exist'

    def handle(self, *args, **kwargs):
        govs = Organization.objects.filter(classification='Governo della Repubblica').\
            annotate(organization_id=F('id')).values('organization_id')

        ETL(
            extractor=DataframeExtractor(pd.DataFrame(govs)),
            loader=DjangoUpdateOrCreateLoader(django_model=Government))()

        pol_parties = Organization.objects.filter(classification='Partito/Movimento politico').\
            annotate(organization_id=F('id')).values('organization_id')

        ETL(
            extractor=DataframeExtractor(pd.DataFrame(pol_parties)),
            loader=DjangoUpdateOrCreateLoader(django_model=PoliticalParty))()
