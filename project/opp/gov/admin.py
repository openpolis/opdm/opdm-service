from django.contrib import admin
from project.opp.gov.models import Member, GovPartyMember
from django.urls import reverse
from django.contrib.admin import SimpleListFilter
from django.utils.translation import gettext_lazy as _
from django.utils.safestring import mark_safe
from project.opp.gov.management.commands.utils import gov_memberships


class GovernmentFilter(SimpleListFilter):
    title = _('Government filter')

    parameter_name = 'Governo'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, i) for i in qs.values_list('government__name', flat=True)
                                  .distinct().order_by('-government__start_date')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(government__name=self.value())


@admin.register(Member)
class MemberAdmin(admin.ModelAdmin):

    def get_memberships_gov(self, obj):
        queryset = gov_memberships.filter(person=obj.person, government_id=obj.government.id)
        return queryset

    def table_memberships(self, obj):
        table_data = self.get_memberships_gov(obj)
        table_html = "<table>"
        for item in table_data:
            url = reverse('admin:%s_%s_change' % (item._meta.app_label, item._meta.model_name),
                          args=[item.id])
            table_html += f"<tr><td>" \
                          f"<a href={url}>{item.id}</a>" \
                          f"</td>" \
                          f"<td>{item.label}</td></tr>"
        table_html += "</table>"
        return mark_safe(table_html)

    table_memberships.allow_tags = True
    table_memberships.short_description = 'Memberships'
    raw_id_fields = ('person', 'government', )
    list_filter = (GovernmentFilter, )
    list_display = ('__str__', 'start_date', 'end_date')
    ordering = ('person__name', )
    readonly_fields = ('table_memberships', )


@admin.register(GovPartyMember)
class GovPartyMemberAdmin(admin.ModelAdmin):
    raw_id_fields = ('gov_member', 'political_party', )
    list_display = ('__str__', 'political_party')
    list_editable = ('political_party', )
    list_filter = (('political_party', admin.RelatedOnlyFieldListFilter),
                   ('gov_member__government', admin.RelatedOnlyFieldListFilter))
