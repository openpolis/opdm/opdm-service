from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField

from project.opp.serializers import BaseSerializerOPP
from project.opp.gov.models import Government, GovPartyMember


class LegislatureHyperlinkedIdentityField(HyperlinkedIdentityField):

    def get_url(self, obj, view_name, request, format):
        """
        Given an object, return the URL that hyperlinks to the object,
        considering the legislature
        """
        # Unsaved objects will not yet have a valid URL.
        if hasattr(obj, 'pk') and obj.pk is None:
            return None
        if hasattr(obj, 'pk2') and obj.pk2 is not None:
            self.lookup_field = 'pk2'

        lookup_value = getattr(obj, self.lookup_field)
        kwargs = {
            'legislature': request.parser_context['kwargs']['legislature'],
            self.lookup_field: lookup_value,
        }
        return self.reverse(view_name, kwargs=kwargs, request=request, format=format)


class GovernmentListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Government model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='government-detail',
    )

    name = serializers.CharField(source='organization.name')
    start_date = serializers.CharField(source='organization.start_date')
    end_date = serializers.CharField(source='organization.end_date')
    id = serializers.SlugField(source='organization.id')
    # organization_id = serializers.SlugField(source='organization.id')
    slug = serializers.SlugField(source='organization.slug')

    class Meta:
        """Define serializer Meta."""

        model = Government
        ref_name = "Government"
        fields = (
            'id',
            # 'organization_id',
            'slug',
            'url',
            'name',
            'start_date',
            'end_date'
        )


class GovRolesSerializers(serializers.Serializer):
    id = serializers.IntegerField()
    role = serializers.CharField()
    organization = serializers.CharField(source='org_name')


class GovRoleListSerializer(serializers.ListSerializer):
    child = GovRolesSerializers()


class GovPartyMemberDetailSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField(source='gov_member.person.name')
    image = serializers.URLField(source='gov_member.person.image')
    roles = GovRoleListSerializer()
    pp = serializers.DictField(source='ppg')
    id = serializers.SerializerMethodField()
    slug = serializers.SerializerMethodField()

    @staticmethod
    def get_id(obj):
        try:
            return obj.gov_member.person.id
        except Exception:
            return None

    @staticmethod
    def get_slug(obj):
        try:
            return obj.gov_member.person.slug
        except Exception:
            return None

    class Meta:
        model = GovPartyMember
        ref_name = "Gov Party Members"
        fields = ('id',
                  'slug',
                  'image',
                  'pp',
                  'name',
                  'roles'
                  )
        ordering = ['-ppg', ]


class GovPartyMemberListDetailSerializer(serializers.ListSerializer):
    child = GovPartyMemberDetailSerializer()

    def to_representation(self, data):
        # sort the data based on the role field
        sorted_data = sorted(data, key=lambda x: x.ppg.get('ppg', 0), reverse=True)
        return super().to_representation(sorted_data)


class PartySerializer(serializers.Serializer):

    name = serializers.CharField(source='party_name')
    members = GovPartyMemberListDetailSerializer(source='party_members')
    image = serializers.URLField(allow_null=True,
                                 allow_blank=True)
    pp = serializers.DictField()


class PartyHomeSerializer(serializers.Serializer):

    name = serializers.CharField(source='party_name')
    image = serializers.URLField(allow_null=True,
                                 allow_blank=True)
    pp = serializers.DictField()



class PoliticalPartiesListSerializer(serializers.ListSerializer):
    child = PartySerializer()

    def to_representation(self, data):
        # sort the data based on the role field
        sorted_data = list(sorted(data, key=lambda x: x['pp']['ppg'], reverse=True))
        return super().to_representation(sorted_data)


class PoliticalPartiesHomeListSerializer(serializers.ListSerializer):
    child = PartyHomeSerializer()

    def to_representation(self, data):
        # sort the data based on the role field
        sorted_data = list(sorted(data, key=lambda x: x['pp']['ppg'], reverse=True))
        return super().to_representation(sorted_data)

class ParliamentNumbers(serializers.DictField):
    n_chamber = serializers.IntegerField()
    n_total_chamber = serializers.IntegerField()
    n_extra_chamber = serializers.IntegerField()
    n_senate = serializers.IntegerField()
    n_total_senate = serializers.IntegerField()
    n_extra_senate = serializers.IntegerField()


class MajorityCohesion(serializers.ListField):
    institution = serializers.CharField()
    cohesion_rate = serializers.FloatField()
    stacked_barplot = serializers.DictField()


class GovernmentDetailSerializer(BaseSerializerOPP):
    """A serializer for the Voting model when seen in lists."""
    #
    # url = LegislatureHyperlinkedIdentityField(
    #     view_name='government-detail',
    # )
    id = serializers.IntegerField(source='organization.id')
    name = serializers.CharField()
    ppg_base = serializers.IntegerField(default=100)
    parties_power = PoliticalPartiesListSerializer(source='political_parties')
    start_date = serializers.CharField(source='start')
    end_date = serializers.CharField(source='organization.end_date')
    parliament_numbers = ParliamentNumbers(source='n_parliament')
    majority_cohesion = MajorityCohesion()
    majority_cohesion_timeline = serializers.ListField()
    parliament_votes = serializers.DictField(source='votes_members')
    confidence_votes = serializers.DictField(source='confidence_metrics')
    gov_acts = serializers.DictField()
    gov_components = serializers.DictField()
    gov_ministries = serializers.ListField(source='get_ministries')
    slug = serializers.SlugField(source='organization.slug')

    class Meta:
        """Define serializer Meta."""

        model = Government
        ref_name = "GovMembers"
        fields = (
            'codelists',
            'end_date',
            'id',
            'slug',
            # 'url',
            'name',
            'start_date',
            'ppg_base',
            'parties_power',
            'parliament_numbers',
            'majority_cohesion',
            'majority_cohesion_timeline',
            'parliament_votes',
            'confidence_votes',
            'gov_acts',
            'gov_components',
            'gov_ministries'
        )
