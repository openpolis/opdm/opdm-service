from math import ceil

import pandas as pd
from django.core.cache import cache
from django.db import models
from django.db.models import F, Q, Value, Count, Avg, ExpressionWrapper, fields, Sum, DateField
from django.db.models.functions import Coalesce, Cast
from popolo.models import Person as PersonOPDM, Organization, KeyEvent
from django.utils.translation import gettext_lazy as _

from project.opp.utils import gender_transf
from project.opp.votes.models import Group, GroupsMetricsHST, MemberMajority, MemberVote, Voting, Membership
from project.opp.metrics.models import PositionalPower
from project.opp.acts.models import GovDecree, Bill, ImplementingDecree, ImplementingDecreeOrganizations, GovBill
from project.calendars.models import CalendarDaily

import datetime

def flatten(list_of_lists):
    if len(list_of_lists) == 0:
        return list_of_lists
    if isinstance(list_of_lists[0], list):
        return flatten(list_of_lists[0]) + flatten(list_of_lists[1:])
    return list_of_lists[:1] + flatten(list_of_lists[1:])

class MemberManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_legislature(self, leg=None):
        if leg:
            today = datetime.datetime.now().date()
            today_strf = today.strftime('%Y-%m-%d')
            leg_identifier = f"ITL_{leg}"
            ke = KeyEvent.objects.get(identifier=f'{leg_identifier}')
            return super().get_queryset().annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf), output_field=DateField())).filter(
                start_date__gte=ke.start_date, end_date_coalesce__lte=(ke.end_date or today_strf)
            )
        else:
            return super().get_queryset().select_related('sitting__assembly', 'sitting')


class Member(models.Model):
    """It represents the set of people .
    This type of relationship is not necessary strict, which it means that a person that is related to political party
    does not imply is a current member of the party, but it may express the party choice during that government
    """
    objects = MemberManager()
    person = models.ForeignKey(
        to=PersonOPDM,
        on_delete=models.CASCADE,
        related_name='gov_membership'
    )

    government = models.ForeignKey(
        to=Organization,
        on_delete=models.CASCADE,
        related_name="members_gov",
        limit_choices_to={'classification__in': ["Governo della Repubblica", ]},
    )

    start_date = models.DateField(
        _("start date"),
        help_text=_("The date to which the status refers"),

    )

    end_date = models.DateField(
        _("end date"),
        blank=True, null=True,
        help_text=_("The date to which the status refers"),
    )

    dati_specifici = models.JSONField(
        blank=True,
        null=True,
    )

    class Meta:
        unique_together = ('person', 'government')

    def get_gov_memberships(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        gov_start_date = self.government.start_date
        gov_end_date = (self.government.end_date or today_strf)
        return (self.person.memberships.annotate(end_date_coalesce=Coalesce(F('end_date'),
                                                                            Value(today_strf)))
        .filter(start_date__gte=gov_start_date,
                                              end_date_coalesce__lte=gov_end_date)
        .filter(
            Q(organization__classification__icontains='ministero') |
            Q(organization__classification__icontains='presidenza del consiglio')))

    @property
    def roles(self):
        # if not cache.get(f'gov_member:{self.id}'):
        roles_org = self.get_gov_memberships() \
            .annotate(org_name=F('organization__name'),

                      ) \
            .values('id', 'role', 'org_name', 'start_date', 'end_date').distinct()
        for role_org in roles_org:
            role = role_org['role'].split(' ')[0]
            if self.person.gender == 'F':
                role = gender_transf(role)
            role_org['role'] = role
            if self.end_date:
                role_org['positional_power'] = PositionalPower \
                    .objects.get(opdm_membership=role_org.get('id'), end_date=self.end_date).value
            else:
                role_org['positional_power'] = PositionalPower \
                    .objects.get(opdm_membership=role_org.get('id'), end_date__isnull=True).value
            role_org['party_img'] = GovPartyMember.objects.get(
                gov_member=self.id).political_party.image if GovPartyMember.objects.get(
                gov_member=self.id).political_party else None
            role_org['party_name'] = GovPartyMember.objects.get(
                gov_member=self.id).political_party.name if GovPartyMember.objects.get(
                gov_member=self.id).political_party else None
        res = list(roles_org)
        cache.set(
            f'gov_member:{self.id}',
            {'res': res},
            timeout=None
        )
        return res
        # else:
        #     return cache.get(f'gov_member:{self.id}').get('res')

    def __str__(self):
        return f"{self.person} @ {self.government}"


class GovPartyMember(models.Model):
    gov_member = models.ForeignKey(
        to=Member,
        on_delete=models.CASCADE,
        related_name='political_party'
    )

    political_party = models.ForeignKey(
        to=Organization,
        on_delete=models.CASCADE,
        limit_choices_to={'classification__in': ["Partito/Movimento politico", ]},
        related_name='opp_members',
        null=True, blank=True
    )

    start_date = models.DateField(
        _("start date"),
        help_text=_("The start date to which the party membership refers"),
    )

    end_date = models.DateField(
        _("end date"),
        blank=True, null=True,
        help_text=_("The end date to which the party membership refers"),
    )

    @property
    def roles(self):
        return self.gov_member.roles

    def get_memberships_roles(self):
        return self.gov_member.get_gov_memberships()

    @property
    def ppg(self):
        gov_member = PositionalPower.positional_power_gov('19')
        df_gov_member = pd.DataFrame(gov_member)
        res = None
        if gov_member.filter(person=self.gov_member.person.id):
            memb = df_gov_member[df_gov_member['person'] == self.gov_member.person.id]
            ppg = round(memb.iloc[0]['std_value'], 2)
            PPG_branch_ordering = memb.iloc[0]['order']
            PPG_branch_total_ordering = pd.DataFrame(gov_member).shape[0]
            res = {
                'ppg': ppg,
                'ppg_branch_ordering': int(PPG_branch_ordering),
                'ppg_branch_total_ordering': PPG_branch_total_ordering,
            }
        return res

    def __str__(self):
        return f"{self.gov_member} ({self.political_party})"


class Government(models.Model):
    organization = models.OneToOneField(
        to=Organization,
        on_delete=models.CASCADE,
        unique=True,
        limit_choices_to={'classification__in': ["Governo della Repubblica"]},
        related_name='opp_gov'
    )
    @property
    def name(self):
        return f"{' '.join(self.organization.name.split(' ')[2:])} {self.organization.name.split(' ')[0]}".strip()

    @classmethod
    def get_government_by_date(cls, date):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        return cls.objects.annotate(end_date_coalesce=Coalesce(F('organization__end_date'), Value(today_strf)))\
            .get(organization__start_date__lte=date,
                 end_date_coalesce__gt=date)

    @classmethod
    def get_governments_by_leg(cls, leg=None):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        legislatura_padded = str(leg).zfill(2)
        ke = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        return cls.objects.filter(organization__start_date__gte=ke.start_date,
                                  organization__start_date__lte=(ke.end_date or today_strf))

    @property
    def get_parliament_majority(self):
        members = MemberMajority.objects.filter(government=self.organization.id)
        return members.count()

    @property
    def get_gov_members(self):

        '''It returns the current gov members if gov is in functions,
        otherwise all the members that were members until the end date of gov'''

        return Member.objects.filter(government=self.organization,
                                     end_date=self.organization.end_date)

    def update_cache(self):
        self.update_cache_political_parties()
        self.update_cache_n_parliament()
        self.update_cache_majority_cohesion()
        self.update_cache_opposition_cohesion()
        self.update_cache_votes_members()
        self.update_cache_confidence_metrics()
        self.update_cache_gov_acts()
        self.update_cache_gov_components()
        self.update_cache_gov_government_get_ministries()

    def update_cache_political_parties(self):
        res = self._political_parties()
        cache.set(
            f'gov_pol_parties:{self.id}',
            {'res': list(res)},
            timeout=None
        )

    def _political_parties(self):
        a = self.organization.to_relationships.filter(end_date__isnull=True) \
            .values('dest_organization__opp_political_party')
        b = PoliticalParty.objects.filter(id__in=a)
        res = [{'party_name': x.party_name,
                'party_members': list(x.party_members),
                'image': x.organization.image,
                'pp': {'ppg': round(sum([i.ppg.get('ppg', 0) for i in x.party_members]), 1)}} for x in b if len(list(x.party_members))>0]
        indipendents = list(GovPartyMember.objects.filter(
                        political_party__isnull=True, end_date__isnull=True
                    ))
        res.append({'party_name': 'Indipendenti',
                    'party_members': indipendents,
                    'image': None,
                    'pp': {'ppg': round(sum([x.ppg.get('ppg') for x in indipendents]),1)}})

        return res

    @property
    def political_parties(self):
        if not cache.get(f'gov_pol_parties:{self.id}'):
            self.update_cache_political_parties()
            return self._political_parties()
        return cache.get(f'gov_pol_parties:{self.id}').get('res')

    @property
    def start(self):
        sd = self.organization.start_date
        sd_date = datetime.datetime.strptime(sd, '%Y-%m-%d')
        return sd_date.strftime('%d/%m/%Y')

    def _n_parliament(self):
        members_chamber = MemberMajority.sustain_chamber(gov=self.organization)
        members_senate = MemberMajority.sustain_senate(gov=self.organization)
        n_chamber_sustain = members_chamber.filter(value=True)
        n_senate_sustain = members_senate.filter(value=True)

        res = {
            'n_chamber': n_chamber_sustain.count(),
            'n_total_chamber': members_chamber.count(),
            'n_extra_chamber': n_chamber_sustain.count() - ceil(members_chamber.count() / 2),
            'n_senate': n_senate_sustain.count(),
            'n_total_senate': members_senate.count(),
            'n_extra_senate': n_senate_sustain.count() - ceil(members_senate.count() / 2)
        }

        return res

    @property
    def n_parliament(self):
        if not cache.get(f'gov_government_n_parliament:{self.id}'):
            self.update_cache_n_parliament()
            return self._n_parliament()
        return cache.get(f'gov_government_n_parliament:{self.id}').get('res')

    def update_cache_n_parliament(self):
        res = self._n_parliament()
        cache.set(
            f'gov_government_n_parliament:{self.id}',
            {'res': res},
            timeout=None
        )

    def _majority_cohesion(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        key_event = self.organization.key_events.get(key_event__event_type='ITL').key_event
        group_maj = Group.objects.all_and_metagroups().filter(acronym='Maggioranza', legislature=key_event)
        sd_gov = self.organization.start_date
        ed_gov = (self.organization.end_date or today_strf)
        # (StartA <= EndB) and (EndA >= StartB)
        votings = Voting.objects.filter(sitting__date__gte=sd_gov,
                                        sitting__date__lt=ed_gov)
        votings_camera = votings.filter(sitting__assembly__name__icontains='camera')
        votings_senato = votings.filter(sitting__assembly__name__icontains='senato')

        hist_groups = GroupsMetricsHST.objects.filter(
            group__acronym='Maggioranza',
            group__legislature=key_event)
        hist_groups_camera = hist_groups.filter(
            group__organization__name__icontains='camera'
        ).annotate(cohesion_rate_C=F('cohesion_rate'))
        hist_groups_senato = hist_groups.filter(
            group__organization__name__icontains='senato'
        ).annotate(cohesion_rate_S=F('cohesion_rate'))
        hist_camera = pd.DataFrame(
            list(hist_groups_camera.order_by('year', 'month').values('year', 'month', 'cohesion_rate_C')))
        hist_senato = pd.DataFrame(
            list(hist_groups_senato.order_by('year', 'month').values('year', 'month', 'cohesion_rate_S')))
        hist_all = hist_camera.merge(hist_senato, on=['year', 'month'])
        timeline = []
        for year in hist_all.year.unique():
            timeline.append(
                {'year': year,
                 'values': hist_all[hist_all['year'] == year][['month', 'cohesion_rate_C', 'cohesion_rate_S']].to_dict(
                     orient='records')})

        res = [
            {
                'institution': 'Camera',
                'cohesion_rate': group_maj.get(organization__name__icontains='senato').cohesion_rate,
                'stacked_barplot': {
                    '_60': votings_camera.filter(majority_cohesion_rate__lt=60).count(),
                    '60_80': votings_camera.filter(majority_cohesion_rate__gte=60,
                                                   majority_cohesion_rate__lt=80).count(),
                    '80_100': votings_camera.filter(majority_cohesion_rate__gte=80).count()
                },
            },

            {
                'institution': 'Senato',
                'cohesion_rate': group_maj.get(organization__name__icontains='camera').cohesion_rate,
                'stacked_barplot': {
                    '_60': votings_senato.filter(majority_cohesion_rate__lt=60).count(),
                    '60_80': votings_senato.filter(majority_cohesion_rate__gte=60,
                                                   majority_cohesion_rate__lt=80).count(),
                    '80_100': votings_senato.filter(majority_cohesion_rate__gte=80).count()
                },
            }
        ]

        return (res, timeline)

    def _opposition_cohesion(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        key_event = self.organization.key_events.get(key_event__event_type='ITL').key_event
        group_opp = Group.objects.all_and_metagroups().filter(acronym='Opposizione', legislature=key_event)
        sd_gov = self.organization.start_date
        ed_gov = (self.organization.end_date or today_strf)
        # (StartA <= EndB) and (EndA >= StartB)
        votings = Voting.objects.filter(sitting__date__gte=sd_gov,
                                        sitting__date__lt=ed_gov)
        # votings_camera = votings.filter(sitting__assembly__name__icontains='camera')
        # votings_senato = votings.filter(sitting__assembly__name__icontains='senato')

        hist_groups = GroupsMetricsHST.objects.filter(
            group__acronym='Opposizione',
            group__legislature=key_event)
        hist_groups_camera = hist_groups.filter(
            group__organization__name__icontains='camera'
        ).annotate(cohesion_rate_C=F('cohesion_rate'))
        hist_groups_senato = hist_groups.filter(
            group__organization__name__icontains='senato'
        ).annotate(cohesion_rate_S=F('cohesion_rate'))
        hist_camera = pd.DataFrame(
            list(hist_groups_camera.order_by('year', 'month').values('year', 'month', 'cohesion_rate_C')))
        hist_senato = pd.DataFrame(
            list(hist_groups_senato.order_by('year', 'month').values('year', 'month', 'cohesion_rate_S')))
        hist_all = hist_camera.merge(hist_senato, on=['year', 'month'])
        timeline = []
        for year in hist_all.year.unique():
            timeline.append(
                {'year': year,
                 'values': hist_all[hist_all['year'] == year][['month', 'cohesion_rate_C', 'cohesion_rate_S']].to_dict(
                     orient='records')})

        res = [
            {
                'institution': 'Camera',
                'cohesion_rate': group_opp.get(organization__name__icontains='senato').cohesion_rate,
            },

            {
                'institution': 'Senato',
                'cohesion_rate': group_opp.get(organization__name__icontains='camera').cohesion_rate,
            }
        ]

        return (res, timeline)

    @property
    def majority_cohesion(self):
        if not cache.get(f'gov_government_majority_cohesion:{self.id}'):
            self.update_cache_majority_cohesion()
            return self._majority_cohesion()[0]
        return cache.get(f'gov_government_majority_cohesion:{self.id}').get('res')[0]

    @property
    def opposition_cohesion(self):
        if not cache.get(f'gov_government_opposition_cohesion:{self.id}'):
            self.update_cache_opposition_cohesion()
            return self._opposition_cohesion()[0]
        return cache.get(f'gov_government_opposition_cohesion:{self.id}').get('res')[0]

    @property
    def opposition_cohesion_timeline(self):
        if not cache.get(f'gov_government_opposition_cohesion:{self.id}'):
            self.update_cache_opposition_cohesion()
            return self._opposition_cohesion()[1]
        return cache.get(f'gov_government_opposition_cohesion:{self.id}').get('res')[1]

    @property
    def majority_cohesion_timeline(self):
        if not cache.get(f'gov_government_majority_cohesion:{self.id}'):
            self.update_cache_majority_cohesion()
            return self._majority_cohesion()[1]
        return cache.get(f'gov_government_majority_cohesion:{self.id}').get('res')[1]

    def update_cache_majority_cohesion(self):
        res = self._majority_cohesion()
        cache.set(
            f'gov_government_majority_cohesion:{self.id}',
            {'res': res},
            timeout=None
        )

    def update_cache_opposition_cohesion(self):
        res = self._opposition_cohesion()
        cache.set(
            f'gov_government_opposition_cohesion:{self.id}',
            {'res': res},
            timeout=None
        )


    def _votes_members(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        votings = Voting.between_dates(self.organization.start_date, (self.organization.end_date or today_strf))

        member_gov_votes = self.organization.members_gov \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today))) \
            .filter(person__memberships__membership_parliament__votes__isnull=False,
                    start_date__lte=F('person__memberships__membership_parliament__votes__voting__sitting__date'),
                    end_date_coalesce__gt=F('person__memberships__membership_parliament__votes__voting__sitting__date')
                    ) \
            .annotate(vote=F('person__memberships__membership_parliament__votes__vote'),
                      assembly=F('person__memberships__membership_parliament__votes__voting__sitting__assembly__name')) \
            .values('vote', 'assembly').annotate(cnt=Count('*'))

        others_parliaments = \
            MemberVote.objects.filter(
                voting__sitting__date__gte=self.organization.start_date,
                voting__sitting__date__lt=(self.organization.end_date or today)) \
                .exclude(membership__opdm_membership__person__in=self.organization.members_gov.values('person')) \
                .annotate(assembly=F('voting__sitting__assembly__name')) \
                .values('vote', 'assembly').annotate(cnt=Count('*'))
        df_others = pd.DataFrame(others_parliaments)
        total_others = df_others['cnt'].sum()
        present_all_others = df_others[df_others['vote'].isin(['AYE', 'NO', 'SEC', 'ABST', 'PNV', 'PRES'])]['cnt'] \
            .sum()
        # others_parliaments.prefetch_related('membership__opdm_membership__person__gov_membership')

        df = pd.DataFrame(member_gov_votes)
        df_chamber = df[df['assembly'].str.contains('Camera')]
        df_senate = df[df['assembly'].str.contains('Senato')]
        total_all = df['cnt'].sum()
        total_chamber = df_chamber['cnt'].sum()
        total_senate = df_senate['cnt'].sum()

        present_all = df[df['vote'].isin(['AYE', 'NO', 'SEC', 'ABST', 'PNV'])]['cnt'].sum()
        present_chamber = df_chamber[df_chamber['vote'].isin(['AYE', 'NO', 'SEC', 'ABST', 'PNV'])]['cnt'].sum()
        present_senate = df_senate[df_senate['vote'].isin(['AYE', 'NO', 'SEC', 'ABST', 'PNV'])]['cnt'].sum()
        absenting_chamber = df_chamber[df_chamber['vote'].isin(['ABSE'])]['cnt'].sum()
        absenting_senate = df_senate[df_senate['vote'].isin(['ABSE'])]['cnt'].sum()
        mission_chamber = df_chamber[df_chamber['vote'].isin(['MIS'])]['cnt'].sum()
        mission_senate = df_senate[df_senate['vote'].isin(['MIS'])]['cnt'].sum()
        senators = Membership.objects.filter(opdm_membership__role__icontains='Senatore', opdm_membership__end_date__isnull=True).values_list('opdm_membership__person', flat=True)
        deputies = Membership.objects.filter(opdm_membership__role__icontains='Deputato', opdm_membership__end_date__isnull=True).values_list('opdm_membership__person', flat=True)
        res = {
            'present_gov_perc': round((100 * present_all) / total_all, 1),
            'present_all_perc': round((100 * present_all_others) / total_others, 1),
            'assemblies':
                [{'name': 'Camera',
                  'n_gov': self.organization.members_gov.filter(end_date__isnull=True).filter(person__in=deputies).count(),
                  'n_distinct_voting': votings.filter(sitting__assembly__name__icontains='Camera').count(),
                  'n_voting': total_chamber,
                  'n_present': present_chamber,
                  'n_absent': absenting_chamber,
                  'n_mission': mission_chamber
                  },
                 {'name': 'Senato',
                  'n_gov': self.organization.members_gov.filter(end_date__isnull=True).filter(person__in=senators).count(),
                  'n_distinct_voting': votings.filter(sitting__assembly__name__icontains='Senato').count(),
                  'n_voting': total_senate,
                  'n_present': present_senate,
                  'n_absent': absenting_senate,
                  'n_mission': mission_senate
                  }
                 ],
        }
        return res

    @property
    def votes_members(self):
        if not cache.get(f'gov_government_votes_members:{self.id}'):
            self.update_cache_votes_members()
        return cache.get(f'gov_government_votes_members:{self.id}').get('res')

    def update_cache_votes_members(self):
        res = self._votes_members()
        cache.set(
            f'gov_government_votes_members:{self.id}',
            {'res': res},
            timeout=None
        )

    def n_months(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        sd = self.organization.start_date
        ed = (self.organization.end_date or today_strf)
        n_months = round(CalendarDaily.objects.filter(date__gte=sd, date__lte=ed).distinct().count()/30, 2)
        return n_months

    def _confidence_metrics(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        n_months = self.n_months()
        sd = self.organization.start_date
        ed = (self.organization.end_date or today_strf)
        n_confidence = (Voting.objects.filter(sitting__date__gte=sd, sitting__date__lt=ed, is_confidence=True)
                        .exclude(type=Voting.MOTION))

        # voti fiducia

        chamber_trust_votes = n_confidence.filter(sitting__assembly__identifier__icontains='camera')
        senate_trust_votes = n_confidence.filter(sitting__assembly__identifier__icontains='senato')

        chamber_trust_votes_list = []
        for i in chamber_trust_votes:
            chamber_trust_votes_list.append({
                "date": i.sitting.date,
                "margin": abs(int(i.n_ayes - int(i.n_majority))),
                "title": (i.public_title or i.original_title),
                "slug": i.identifier})

        senate_trust_votes_list = []
        for i in senate_trust_votes:
            senate_trust_votes_list.append({
                "date": i.sitting.date,
                "margin": abs(int(i.n_ayes - int(i.n_majority))),
                "title": (i.public_title or i.original_title),
                "slug": i.identifier})

        res = {
            'month_avg': round(n_confidence.count() / n_months, 2),
            'n_chamber': n_confidence.filter(sitting__assembly__name__icontains='camera').count(),
            'n_senate': n_confidence.filter(sitting__assembly__name__icontains='senato').count(),
            'historical': [
                {'name': self.organization.name.split(' ')[-1],
                 'month_avg': round(n_confidence.count() / n_months, 2)},
                {'name': 'Draghi',
                 'month_avg': 2.68},
                {'name': 'Conte II',
                 'month_avg': 2.22},
                {'name': 'Conte I',
                 'month_avg': 0.98},

            ],
            'chamber': chamber_trust_votes_list,
            'senate': senate_trust_votes_list
        }

        return res

    @property
    def confidence_metrics(self):
        if not cache.get(f'gov_government_confidence_metrics:{self.id}'):
            self.update_cache_confidence_metrics()
        return cache.get(f'gov_government_confidence_metrics:{self.id}').get('res')

    def update_cache_confidence_metrics(self):
        res = self._confidence_metrics()
        cache.set(
            f'gov_government_confidence_metrics:{self.id}',
            {'res': res},
            timeout=None
        )

    def _gov_acts(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        n_months = self.n_months()
        duration = ExpressionWrapper(F('publication_gu_date') - F('sitting__ending_datetime__date'),
                                     output_field=fields.DurationField())

        dl = GovDecree.objects.filter(sitting__government=self.organization)
        dlgs = GovBill.objects.filter(decision_sitting__government=self.organization)
        ddl = Bill.get_last_bill('19').filter(initiative='GOV',
                                  date_presenting__gte=self.organization.start_date,
                                  date_presenting__lt=(self.organization.end_date or today_strf)).exclude(
            type=Bill.DL_CONVERSION
        )
        ddl_no_rat = ddl.filter(is_ratification=False)
        # ddl_no_rat_only_origin = [x for x in ddl_no_rat if x.is_origin()[0]]
        avg_pub_days = dl.annotate(duration=duration).aggregate(avg_days=Avg(F('duration')))['avg_days']

        impl_dec = ImplementingDecree.objects.filter(is_valid=True, government=self.organization)
        impl_dec_adopted = impl_dec.filter(is_adopted=True)
        impl_dec_to_adopt = impl_dec.filter(is_adopted=False)
        govs_before = Government.objects.filter(organization__start_date__lte=self.organization.start_date).order_by(
            '-organization__start_date')[:4]

        not_law = [{
            'id': x.id,
            'appr_bef': x.last_act_approved_before(),
            'rat': x.is_ratification,
            'gov': x.initiative == Bill.GOVERNMENT} for x in ddl_no_rat.filter(is_law=False)]

        one_branch_ids = [x['id'] for x in not_law if x['appr_bef']]

        one_branch_gov = sum([int(x['appr_bef']) for x in not_law if x['gov'] == True and x['rat'] == False])
        first_phase = ddl.exclude(id__in=one_branch_ids).exclude(is_law=True)
        to_start = [x.id for x in first_phase if x.last_status().phase.phase in ['Assegnato (no esame)',
                                                                                 'Da assegnare a commissione']]
        to_start_objects = Bill.objects.filter(id__in=to_start)
        started = [x.id for x in first_phase if x.last_status().phase.phase not in ['Assegnato (no esame)',
                                                                                    'Da assegnare a commissione',
                                                                                    'Ritirato',
                                                                                    'Respinto',
                                                                                    'Restituito al governo',
                                                                                    'D-L decaduto']]
        started_objects = Bill.objects.filter(id__in=started)

        rejected = ddl.exclude(is_law=True) \
            .exclude(id__in=[x.id for x in to_start_objects] + [x.id for x in started_objects] + one_branch_ids)
        to_reject = Bill.objects.filter(id__in=[x.id for x in rejected if x.last_status().phase.phase in ['Ritirato',
                                                                                                          'Respinto',
                                                                                                          'Restituito al governo',
                                                                                                          'D-L decaduto'
                                                                                                          ]])

        res = {
            'ddl': {
                'cnt': ddl_no_rat.count(),
                'detail': {
                    'is_law': ddl_no_rat.filter(is_law=True).count(),
                    'one_branch': one_branch_gov,
                    'first_step': started_objects.exclude(is_ratification=True)
                            .filter(initiative=Bill.GOVERNMENT).count(),
                    'to_begin': to_start_objects.exclude(is_ratification=True)
                            .filter(initiative=Bill.GOVERNMENT).count(),
                    'rejected': to_reject.exclude(is_ratification=True)
                            .filter(initiative=Bill.GOVERNMENT).count(),

                },

            },
            'dlgs': {
                'cnt': dlgs.count()
            },
            'dl': {
                'cnt': dl.count(),
                'days_pub_avg': round(avg_pub_days.total_seconds() / (60 * 60 * 24), 1),
                'month_avg': round(dl.count() / n_months, 1),
                'detail': {
                    'is_law': len([x for x in dl if x.status().get('status') == 'l']),
                    'converting': len([x for x in dl if x.status().get('status') == 'c']),
                    'expired': len([x for x in dl if x.status().get('status') == 'e'])
                },
                'historical': [x.get_historical() for x in govs_before]

            },

            'ratification':
                {'cnt': ddl.filter(is_ratification=True, is_law=True).count(), },
            'implementing':
                {
                    'to_adopt':
                        {'cnt': impl_dec_to_adopt.count(),
                         'dl_cnt': impl_dec_to_adopt.filter(ending_relations__starting_ct__model='govdecree').count()},
                    'adopted':
                        {'cnt': impl_dec_adopted.count(),
                         'dl_cnt': impl_dec_adopted.filter(ending_relations__starting_ct__model='govdecree').count()}
                }
        }

        return res

    def get_historical(self):
        if self.organization.name == 'I Governo Draghi':
            return {'name': 'Draghi',
                    'month_avg': 3.07}
        elif self.organization.name == 'II Governo Conte':
            return {'name': 'Conte II',
                    'month_avg': 3.07}
        elif self.organization.name == 'I Governo Conte':
            return {'name': 'Conte I',
                    'month_avg': 1.69}

        else:
            dl = GovDecree.objects.filter(sitting__government=self.organization)
            n_months = self.n_months()
            return {'name': self.organization.name.split(' ')[-1],
                    'month_avg': round(dl.count() / n_months, 1)}

    @property
    def gov_acts(self):
        if not cache.get(f'gov_government_gov_acts:{self.id}'):
            self.update_cache_gov_acts()
        return cache.get(f'gov_government_gov_acts:{self.id}').get('res')

    def update_cache_gov_acts(self):
        res = self._gov_acts()
        cache.set(
            f'gov_government_gov_acts:{self.id}',
            {'res': res},
            timeout=None
        )

    def _gov_components(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        gov_memebers = self.get_gov_members

        person_ages = gov_memebers.select_related('person').annotate(
            age=Value(today) - Cast(F('person__birth_date'), fields.DateField())
        )

        list_ages = [x['age'].days / 365.2425 for x in person_ages.values('age')]

        res = {
            'gender': {
                'female': gov_memebers.filter(person__gender='F').count(),
                'male': gov_memebers.filter(person__gender='M').count()
            },
            'age': {
                'avg': round((sum(list_ages) / len(list_ages)), 0),
                'distribution':
                    {
                        '_35': len(list(filter(lambda x: x < 35, list_ages))),
                        '35_45': len(list(filter(lambda x: x >= 35 and x < 45, list_ages))),
                        '45_55': len(list(filter(lambda x: x >= 45 and x < 55, list_ages))),
                        '55_65': len(list(filter(lambda x: x >= 55 and x < 65, list_ages))),
                        '65_': len(list(filter(lambda x: x >= 65, list_ages)))
                    }
            },
        }
        return res

    @property
    def gov_components(self):
        if not cache.get(f'gov_government_gov_components:{self.id}'):
            self.update_cache_gov_components()
        return cache.get(f'gov_government_gov_components:{self.id}').get('res')

    def update_cache_gov_components(self):
        res = self._gov_components()
        cache.set(
            f'gov_government_gov_components:{self.id}',
            {'res': res},
            timeout=None
        )

    def _get_ministries(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        sd = self.organization.start_date
        ed = (self.organization.end_date or today_strf)
        ministries = Organization.objects.filter(
            Q(classification__icontains='Ministero') |
            Q(classification__icontains='Presidenza del consiglio')). \
            annotate(
            start_date_coalesce=Coalesce(F('start_date'), Value('1900-01-01')),
            end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))). \
            filter(start_date_coalesce__lte=ed, end_date_coalesce__gte=sd)

        res = []

        for ministry in ministries:

            memberships = GovPartyMember.objects.filter(
                gov_member__government=self.organization,
                gov_member__person__memberships__organization=ministry,
                gov_member__person__memberships__end_date__isnull=True). \
                annotate(role=F('gov_member__person__memberships__role')).distinct()
            persons = [
                {'id': x.gov_member.person.id,
                 'slug': x.gov_member.person.slug,
                 'img': x.gov_member.person.image,
                 'name': str(x.gov_member.person.name),
                 'role': gender_transf(str(x.role.split(' ')[0])) if x.gov_member.person.gender =='F' else str(x.role.split(' ')[0]),
                 'party': {
                     'name': str(x.political_party.name) if x.political_party else 'Indipendente',
                     'img': str(x.political_party.image) if x.political_party else None
                 },
                 'pp': {
                     'ppg': float(x.ppg.get('ppg', 0)),
                     'ppg_branch_ordering': int(x.ppg.get('ppg_branch_ordering', 0)),
                     'ppg_branch_total_ordering': x.ppg.get('ppg_branch_total_ordering', 0)}
                 } for x in memberships
            ]
            persons_ordered = sorted(persons, key=lambda d: d['pp']['ppg'], reverse=True)
            aggregated_data = [{'name': x['party']['name'], 'img': x['party']['img'], 'ppg': x['pp']['ppg']} for x in
                               persons_ordered]
            if len(persons_ordered) > 0:
                df = pd.DataFrame(aggregated_data)
                df = df.fillna('').groupby(['name', 'img'])['ppg'].sum()
                res.append(
                    {
                        'id': ministry.id,
                        'name': str(ministry.name),
                        'is_portfolio': bool(ministry.classification == 'Ministero'),
                        'party_most_powerful': {
                            'name': df.sort_values().index[-1][0],
                            'img': df.sort_values().index[-1][1]
                        },

                        # 'n_impl_dec': int(impl_decree_min.count()),
                        # 'resources':
                        #     {
                        #         'total': float(df.sum()),
                        #         'detail': [
                        #             {
                        #                 'year': int(x[0]),
                        #                 'value': float(x[1])
                        #             } for x in zip(df.index, df)]
                        #
                        #     },
                        'persons': persons_ordered
                    }
                )

        sort = sorted(res, key=lambda d: sum([x['pp']['ppg'] for x in d['persons']]), reverse=True)
        return [x for x in sort if len(x['persons']) > 0]

    @property
    def get_ministries(self):
        if not cache.get(f'gov_government_get_ministries:{self.id}'):
            self.update_cache_gov_government_get_ministries()
        return cache.get(f'gov_government_get_ministries:{self.id}').get('res')

    def update_cache_gov_government_get_ministries(self):
        res = self._get_ministries()
        cache.set(
            f'gov_government_get_ministries:{self.id}',
            {'res': list(res)},
            timeout=None
        )

    def __str__(self):
        return self.organization.__str__()

    class Meta:
        ordering = ('-organization__start_date',)


class PoliticalParty(models.Model):
    organization = models.OneToOneField(
        to=Organization,
        on_delete=models.CASCADE,
        unique=True,
        limit_choices_to={'classification__in': ["Partito/Movimento politico", ]},
        related_name='opp_political_party'
    )

    @property
    def party_name(self):
        return self.organization.name

    @property
    def party_members(self):
        return self.organization.opp_members.filter(end_date__isnull=True).prefetch_related('gov_member__person')

    @classmethod
    def get_indipendent_value(cls):
        denom = PositionalPower.gov_abs_metric()
        num = sum(PositionalPower.get_gov().filter(opdm_membership__person__in=GovPartyMember.objects.filter(
            political_party__isnull=True, end_date__isnull=True
        ).values('gov_member__person'), end_date__isnull=True).values_list('value', flat=True))
        return round((100 * num) / denom, 1)

    @property
    def get_positional_value(self):
        if not cache.get(f'gov_politicalparty:{self.id}'):
            denom = PositionalPower.gov_abs_metric()
            roles = flatten([x.roles for x in self.party_members])
            res = round(100 * sum([x.get('positional_power') for x in roles]) / denom, 1)
            cache.set(
                f'gov_politicalparty:{self.id}',
                {'res': res},
                timeout=None
            )
            return res
        else:
            return cache.get(f'gov_politicalparty:{self.id}').get('res')

    def __str__(self):
        return self.organization.__str__()
