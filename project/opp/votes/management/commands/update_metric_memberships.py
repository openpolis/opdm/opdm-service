from project.opp.votes.models import Membership
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        num_legislatura = options["legislatura"]
        self.logger.info(f'Starting update memberships metrics {num_legislatura}')
        legislatura_padded = str(num_legislatura).zfill(2)

        for membro in Membership.objects.by_legislature(legislatura_padded):
            metrics = ['n_present', 'n_absent', 'n_mission']
            for metric in metrics:
                value = membro.get_absolute_metrics(metric=metric)
                setattr(membro, metric, value)
                membro.save()
            membro.days_in_parliament = membro._days_in_parliament()
            membro.save()
