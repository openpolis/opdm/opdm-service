from project.opp.votes.models import GroupVote, GroupsMetricsHST, Group
from project.calendars.models import CalendarDaily
from popolo.models import KeyEvent
from taskmanager.management.base import LoggingBaseCommand
from django.db.models import Q, Sum, IntegerField
from django.db.models.functions import Cast
import datetime

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):
    """"""
    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )
        parser.add_argument(
            '--sd',
            dest="start_date",
            required=True,
            help='Start date collection votes, format YYYY-MM-DD.')

        parser.add_argument(
            '--ed',
            dest="end_date",
            required=True,
            help='End date collection votes, format YYYY-MM-DD.')

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        num_legislatura = options["legislatura"]
        sd = options['start_date']
        ed = options['end_date']
        # self.logger.info(f'Starting import for historical metrics legislatura {num_legislatura}')
        legislatura_padded = str(num_legislatura).zfill(2)
        legislatura = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        start_legislatura = legislatura.start_date
        end_legislatura = (legislatura.end_date or today_strf)

        for gruppo in Group.objects.all_and_metagroups().filter(legislature=legislatura,
                                                                organization__isnull=False):
            start_date_gruppo = gruppo.organization.start_date
            end_date_gruppo = (gruppo.organization.end_date or today_strf)
            year_months = CalendarDaily.objects\
                .filter(date__gte=max(start_date_gruppo, start_legislatura),
                        date__lte=min(end_legislatura, end_date_gruppo)) \
                .filter(date__gte=sd, date__lte=ed)\
                .values('year', 'month').distinct().order_by('year', 'month')
            for year_month in year_months:
                start_month = CalendarDaily.objects.get(
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    is_month_start=True).date
                end_month = CalendarDaily.objects.get(
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    is_month_end=True).date
                gvotes = GroupVote.objects\
                    .filter(Q(group__organization__parent__key_events__key_event=legislatura)
                            | Q(group__organization__key_events__key_event=legislatura))\
                    .distinct()\
                    .filter(group_id=gruppo,
                            voting__sitting__date__gte=start_month,
                            voting__sitting__date__lte=end_month)

                gvotes_overall = GroupVote.objects\
                    .filter(Q(group__organization__parent__key_events__key_event=legislatura)
                            | Q(group__organization__key_events__key_event=legislatura))\
                    .distinct()\
                    .filter(group_id=gruppo,
                            voting__sitting__date__lte=end_month)
                if gvotes.count() == 0:
                    continue

                abs_values = gvotes\
                    .aggregate(Sum('n_present'),
                               Sum('n_absent'),
                               Sum('n_mission'),
                               Sum('n_rebels'),
                               Sum('vote_value'),
                               n_pres=Sum(Cast('has_president', IntegerField())))

                abs_values_overall = gvotes_overall\
                    .aggregate(Sum('n_present'),
                               Sum('n_absent'),
                               Sum('n_mission'),
                               Sum('n_rebels'),
                               Sum('vote_value'),
                               n_pres=Sum(Cast('has_president', IntegerField())))
                tot_gruppo = sum({x: abs_values.get(x)
                                  for x in ['n_present__sum', 'n_absent__sum', 'n_mission__sum']}.values())
                tot_gruppo_overall = sum({x: abs_values_overall.get(x)
                                  for x in ['n_present__sum', 'n_absent__sum', 'n_mission__sum']}.values())
                GroupsMetricsHST.objects.update_or_create(
                    group=gruppo,
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    defaults={
                        'n_voting': gvotes.count(),
                        'cohesion_rate': round(100.0*abs_values
                                               .get('vote_value__sum')/(tot_gruppo), 2),
                        'cohesion_rate_overall': round(100.0 * abs_values_overall.get('vote_value__sum') / (tot_gruppo_overall), 2),
                        'perc_present': round(100.0*abs_values.get('n_present__sum')/tot_gruppo, 2),
                        'perc_absent': round(100.0*abs_values.get('n_absent__sum')/tot_gruppo, 2),
                        'perc_mission': round(100.0*abs_values.get('n_mission__sum')/tot_gruppo, 2),
                        'n_rebels': abs_values.get('n_rebels__sum')

                    }

                )
            GroupsMetricsHST.objects.all()

        # AGGREGAZIONE  MAGGIORANZA E OPPOSIZONE ALL
        for gruppo in Group.objects.all_and_metagroups().filter(legislature=legislatura,
                                                                organization__isnull=True):
            year_months = CalendarDaily.objects\
                .filter(date__gte=start_legislatura,
                        date__lte=end_legislatura)\
                .values('year', 'month').distinct().order_by('year', 'month')
            for year_month in year_months:
                start_month = CalendarDaily.objects.get(
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    is_month_start=True).date
                end_month = CalendarDaily.objects.get(
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    is_month_end=True).date

                subgroups = Group.objects.all_and_metagroups().filter(
                    legislature=legislatura,
                    is_meta_group=True,
                    organization_id__isnull=False,
                    acronym=gruppo.acronym)
                gvotes = GroupVote.objects\
                    .filter(Q(group__organization__parent__key_events__key_event=legislatura)
                            | Q(group__organization__key_events__key_event=legislatura))\
                    .distinct()\
                    .filter(group__in=subgroups,
                            voting__sitting__date__gte=start_month,
                            voting__sitting__date__lte=end_month)
                if gvotes.count() == 0:
                    continue

                abs_values = gvotes\
                    .aggregate(Sum('n_present'),
                               Sum('n_absent'),
                               Sum('n_mission'),
                               Sum('n_rebels'),
                               Sum('vote_value'))
                tot_gruppo = sum({x: abs_values.get(x)
                                  for x in ['n_present__sum', 'n_absent__sum', 'n_mission__sum']}.values())

                GroupsMetricsHST.objects.update_or_create(
                    group=gruppo,
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    defaults={
                        'n_voting': gvotes.count(),
                        'cohesion_rate': round(100.0*abs_values.get('vote_value__sum')/tot_gruppo, 2),
                        'perc_present': round(100.0*abs_values.get('n_present__sum')/tot_gruppo, 2),
                        'perc_absent': round(100.0*abs_values.get('n_absent__sum')/tot_gruppo, 2),
                        'perc_mission': round(100.0*abs_values.get('n_mission__sum')/tot_gruppo, 2),
                        'n_rebels': abs_values.get('n_rebels__sum')

                    }

                )
            GroupsMetricsHST.objects.all()
