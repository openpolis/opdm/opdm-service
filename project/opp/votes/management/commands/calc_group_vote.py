from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.models import Group, MemberVote, Voting, GroupVote, GroupMajority
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor
from ooetl.transformations import Transformation
from django.db.models import F, Value, Count, Case, When, Sum
from django.db.models.functions import Coalesce
import pandas as pd

import datetime

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


def get_votes_metagroup(voting, assemblea, columns):
    df_all = pd.DataFrame(columns=columns)
    presidente_value = voting.president.member_supports_majority_at_date(voting.sitting.date)
    for value in [True, False]:
        group = get_meta_group(assemblea, value)
        member_majority = GroupVote.get_membri_gruppo(
            group,
            voting)
        m_votes = MemberVote.objects.filter(voting=voting, membership__in=member_majority.values('membership'))
        votes_count = m_votes.values('vote').annotate(total=Count('vote'))
        df = pd.DataFrame(votes_count.values('vote', 'total'))
        df = df.pivot_table(index='vote', values='total', aggfunc='sum')
        # df.reset_index(inplace=True)
        df = df.transpose()
        df = df.reindex(
            df.columns.union(
                ['ABSE',
                 'ABST',
                 'AYE',
                 'MIS',
                 'NO',
                 'PRES',
                 'RNV',
                 'PNV',
                 'SEC'], sort=False), axis=1, fill_value=0)
        df = df.rename(columns={
            'ABSE': 'n_absent',
            'ABST': 'n_abstained',
            'AYE': 'n_ayes',
            'MIS': 'n_mission',
            'NO': 'n_nos',
            'PRES': 'PRES',
            'RNV': 'n_requiring_not_voting',
            'PNV': 'n_present_not_voting',
            'SEC': 'SEC'}, errors="continue")
        df['group'] = group
        df['voting'] = voting
        df['supports_majority'] = value
        if value == presidente_value:
            df['PRES'] = 1
        df_all = df_all.append(df)
    return df_all


def get_vote_group(votes,  is_secret):
    n_ayes, n_nos, n_abstained, n_absent = votes['n_ayes'], votes['n_nos'], votes['n_abstained'], votes['n_absent']
    n_voting = votes['n_voting']
    n_not_voting = votes['n_absent'] + votes['n_requiring_not_voting'] + votes['n_present_not_voting']\
        + votes['n_mission']
    rebel_vote = 0
    if n_not_voting > 0.8*(n_not_voting + n_voting + n_abstained):
        value_max = n_not_voting
        res = (GroupVote.NOT_VOTING, n_voting)
    elif sorted([n_ayes, n_nos, n_abstained])[-2] != sorted([n_ayes, n_nos, n_abstained])[-1] and not is_secret:
        var = {GroupVote.AYE: n_ayes,
               GroupVote.NO: n_nos,
               GroupVote.ABSTAINED: n_abstained}
        group_vote = max(var, key=var.get)
        value_max = var.pop(group_vote)
        rebel_vote = sum(var.values())
        res = (group_vote, rebel_vote)
    elif is_secret and sorted([n_voting, n_absent, n_abstained])[-2] != sorted([n_voting, n_absent, n_abstained])[-1]:
        var = {
            GroupVote.SECRET: n_voting,
            GroupVote.ABSTAINED: n_abstained,
            GroupVote.NOT_VOTING: n_absent}
        group_vote = max(var, key=var.get)
        value_max = var[group_vote]
        if group_vote in ['NVOT']:
            rebel_vote = var['SEC']
        res = (group_vote, rebel_vote)
    else:
        if is_secret:
            var = {
                GroupVote.SECRET: n_voting,
                GroupVote.ABSTAINED: n_abstained,
                GroupVote.NOT_VOTING: n_absent}
        else:
            var = {GroupVote.AYE: n_ayes,
                   GroupVote.NO: n_nos,
                   GroupVote.ABSTAINED: n_abstained}
        group_vote = max(var, key=var.get)
        value_max = var[group_vote]
        res = (GroupVote.NOT_VALUABLE, 0)
    if votes['group'].exclude_rebel_calc:
        res = (res[0], 0)
    return res + (n_not_voting, value_max, )


def cohesion_rate(group, voting, esito):
    """It returns the cohesion rate of the group during voting

    :param gruppo: object Group
    :param votazione: object Voting
    :param esito: numero voti concordi al gruppo
    :return:
    """
    membri_gruppo = GroupVote.get_membri_gruppo(group, voting).count()
    if membri_gruppo == 0:
        return None
    return round(100.00*esito/membri_gruppo, 2)


def get_supports_majority(gruppo, votazione):
    data_votazione = votazione.sitting.date
    gm = GroupMajority.objects.annotate(new_end_date=Coalesce(F('end_date'), Value(today_strf)))\
        .filter(group=gruppo, start_date__lte=data_votazione, new_end_date__gte=data_votazione)
    if gm.count() == 1:
        return gm.first().value
    elif gm.count() > 1:
        raise Exception
    else:
        return None


def get_meta_group(assemblea, supports_majority):
    return Group.objects.all_and_metagroups().get(is_meta_group=True,
                                                  organization=assemblea,
                                                  supports_majority=supports_majority)


def fix_rebels_meta(row, df):
    if row['group'].is_meta_group:
        if row['group'].acronym == 'Maggioranza':
            return df[df.supports_majority == True].n_rebels.sum() # noqa E712
        elif row['group'].acronym == 'Opposizione':
            return df[df.supports_majority == False].n_rebels.sum() # noqa E712
        else:
            return 0
    return row['n_rebels']


class GroupVoteTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, votazione, include_president):
        Transformation.__init__(self)
        self.votazione = votazione
        self.assemblea = votazione.sitting.assembly
        self.include_president = include_president

    def transform(self):

        od = self.etl.original_data.copy()
        od = od.fillna(0)
        od['group'] = od['group__organization_id'].apply(lambda x: Group.objects.all_and_metagroups().get(id=x))
        od['voting'] = self.votazione
        od['supports_majority'] = od.apply(lambda x: get_supports_majority(x['group'], x['voting']), axis=1)
        meta_vote = get_votes_metagroup(self.votazione, self.assemblea, columns=od.columns)
        od = pd.concat([od, meta_vote])
        od['n_present_not_voting'] = od.apply(lambda x: x['n_present_not_voting'] + (
            x['PRES'] if self.include_president else 0), axis=1)
        od['n_present'] = od.apply(lambda x:
                                   x['n_abstained']
                                   + x['n_ayes']
                                   + x['n_nos']
                                   + x['n_present_not_voting']
                                   + x['n_requiring_not_voting']
                                   + x['SEC'], axis=1)
        od['has_president'] = od['PRES'].apply(lambda x: x == 1)
        od['n_voting'] = od.apply(lambda x: x['n_ayes']+x['n_nos']+x['SEC'], axis=1)

        od['vote'], od['n_rebels'], od['n_not_voting'], od['vote_value'] = zip(*od.apply(
            lambda x: get_vote_group(x, self.votazione.is_secret), axis=1))
        od['cohesion_rate'] = od.apply(lambda x: cohesion_rate(x['group'],
                                                               x['voting'],
                                                               x['vote_value']),
                                       axis=1)
        od['n_rebels'] = od.apply(lambda x: fix_rebels_meta(x, od[od.group.apply(lambda x: not x.is_meta_group)]),
                                  axis=1)
        od['n_group_total'] = od.apply(lambda x: GroupVote.get_membri_gruppo(
            x['group'],
            x['voting']).count(),
                                       axis=1)
        # od = od.fillna(0)
        od.drop(columns='supports_majority').fillna(0)

        od = od.drop(['PRES', 'SEC', 'group__organization_id'], axis=1)

        self.etl.processed_data = od
        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL connect Memberships Popolo and Membership Vote."""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--i",
            type=str,
            dest='identifier',
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)


        identifier_votazione = options["identifier"]
        votazione = Voting.objects.get(identifier=identifier_votazione)
        data_votazione = votazione.sitting.date
        membervote = MemberVote.objects.filter(voting=votazione)
        if membervote.count() == 0:
            self.logger.warning(f"No data for {votazione}")
            return None
        map_vote_group = membervote.annotate(
            org_id=F('membership__membership_groups__group__id'),
            start_date=F('membership__membership_groups__start_date'),
            new_end_date=Coalesce(F('membership__membership_groups__end_date'), Value(today_strf)))\
            .values('id', 'org_id', 'vote', 'new_end_date', 'membership__membership_groups__start_date')\
            .filter(start_date__lte=data_votazione, new_end_date__gte=data_votazione).distinct()

        if not map_vote_group.values('id').distinct().count() == map_vote_group.count():
            raise Exception
        df = pd.DataFrame(
            map_vote_group.values('org_id', 'vote', ).annotate(dcount=Count('id'))
        )

        include_president = True
        if map_vote_group.filter(vote='PRES'):
            include_president = not MemberVote.objects.get(
                id=map_vote_group.get(vote='PRES')['id']).is_president_official
        df_pivot = df.pivot_table(columns='vote' , index='org_id', values='dcount', aggfunc='sum', fill_value=0)
        # df_pivot.columns = df_pivot.columns.get_level_values(1)
        df_pivot.reset_index(inplace=True)
        df_pivot = df_pivot.reindex(
            df_pivot.columns.union(
                ['ABSE',
                 'ABST',
                 'AYE',
                 'MIS',
                 'NO',
                 'PRES',
                 'RNV',
                 'PNV',
                 'SEC'], sort=False), axis=1, fill_value=0)
        df_pivot = df_pivot.rename(columns={
            'org_id': 'group__organization_id',
            'ABSE': 'n_absent',
            'ABST': 'n_abstained',
            'AYE': 'n_ayes',
            'MIS': 'n_mission',
            'NO': 'n_nos',
            'PRES': 'PRES',
            'RNV': 'n_requiring_not_voting',
            'PNV': 'n_present_not_voting',
            'SEC': 'SEC'}, errors="continue")

        ETL(
            extractor=DataframeExtractor(df_pivot),
            transformation=GroupVoteTransformation(votazione=votazione,
                                                   include_president=include_president),
            loader=DjangoUpdateOrCreateLoader(django_model=GroupVote,
                                              fields_to_update=['vote',
                                                                'n_rebels',
                                                                'n_voting',
                                                                'n_abstained',
                                                                'n_absent',
                                                                'n_mission',
                                                                'n_ayes',
                                                                'n_nos',
                                                                'n_present',
                                                                'n_present_not_voting',
                                                                'n_requiring_not_voting',
                                                                'supports_majority',
                                                                'cohesion_rate',
                                                                'vote_value',
                                                                'has_president',
                                                                'n_not_voting',
                                                                'n_group_total']))()

        for index, row in df_pivot.iterrows():
            rebels = None
            org_id = row['group__organization_id']
            gr = Group.objects.all_and_metagroups().get(id=org_id)

            voto_gruppo = GroupVote.objects.get(group_id=org_id, voting=votazione).vote
            if voto_gruppo in ['AYE', 'NO', 'ABST'] and not votazione.is_secret:
                rebels = map_vote_group.filter(org_id=org_id)\
                    .exclude(vote=voto_gruppo)\
                    .filter(vote__in=['AYE',
                                      'ABST',
                                      'NO'])
            elif voto_gruppo == 'NVOT':
                rebels = map_vote_group.filter(org_id=org_id).exclude(vote=voto_gruppo).filter(vote__in=['SEC'])
            elif voto_gruppo in ['NV', 'SEC', 'ABST']:
                continue
            if voto_gruppo != 'NV':
                fidelity = map_vote_group.filter(org_id=org_id)\
                    .filter(vote=voto_gruppo)
            if gr.exclude_rebel_calc:
                fidelity.update(is_fidelity=True)
                continue
            rebels.update(is_rebel=True)
            fidelity.update(is_fidelity=True)

        # n_rebel_votazione = sum(GroupVote.objects.filter(voting=votazione).exclude(group__is_meta_group=True)
        #                         .values_list('n_rebels', flat=True))
        # votazione.n_rebels = n_rebel_votazione

        maj_gv = GroupVote.objects.filter(voting=votazione, group__acronym='Maggioranza').first()
        if maj_gv:
            votazione.majority_cohesion_rate = maj_gv.cohesion_rate

        min_gv = GroupVote.objects.filter(voting=votazione, group__acronym='Opposizione').first()
        if min_gv:
            votazione.minority_cohesion_rate = min_gv.cohesion_rate

        votazione.save()


