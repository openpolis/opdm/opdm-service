import base64
import json

import requests
from django.core.cache import cache
from taskmanager.management.base import LoggingBaseCommand
from django.core.management import call_command
import datetime
import time
import logging
from dateutil.relativedelta import relativedelta
import environ

env = environ.Env()


class Command(LoggingBaseCommand):
    logger = logging.getLogger(f"project.{__name__}")
    help = "Update data for OPENPARLAMENTO given legislature and range.\n" \
           "If no <sd> and <ed> is given, update will occur between [(today-2months)<-->today]"""

    def add_arguments(self, parser):
        """Add arguments to the command."""
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        parser.add_argument(
            "--l",
            type=int,
            choices=[18, 19],
            default=18,
            dest='legislatura',
            required=True,
            help="Legislature number"
        )
        parser.add_argument(
            '--sd',
            dest="start_date",
            required=False,
            help='Start date collection votes, format YYYY-MM-DD. \n'
                 'If not given, it\'s running today - 1month')

        parser.add_argument(
            '--ed',
            dest="end_date",
            default=today_strf,
            required=False,
            help='End date collection votes, format YYYY-MM-DD.\n'
                 'If not given, it\'s running day')
        parser.add_argument(
            "--full",
            dest="full",
            action="store_true",
            help="Full update cache",
        )

    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.

        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        self.logger.info(f"----- Starting {command_name} -----")
        start_time = time.time()
        call_command(command_name, *args, **options, stdout=self.stdout)
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

    def handle(self, *args, **options):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        previous_month_date = today - relativedelta(months=2)
        previous_month_date_strf = previous_month_date.strftime('%Y-%m-%d')
        self.setup_logger(__name__, formatter_key="simple", **options)
        self.logger.setLevel(
            {
                0: logging.ERROR,
                1: logging.WARNING,
                2: logging.INFO,
                3: logging.DEBUG,
            }.get(options["verbosity"])
        )
        legislatura = options['legislatura']
        self.logger.info(f"Legislatura {legislatura}")
        sd = (options.get('start_date', None) or previous_month_date_strf)
        ed = (options.get('end_date', None) or today_strf)
        full = options.get('full', True)
        self.logger.info("Load/Update Acts")
        self.call_command('update_acts',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info("Load/Update Sitting FULL legislature")
        self.call_command('import_sedute_sparql',
                          legislatura=legislatura,
                          start_date=sd,
                          end_date=ed,
                          verbosity=3)

        self.logger.info("Load/Update Membership table FULL legislature")
        self.call_command('import_memberships_opdm',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info("Load/Update Group table FULL legislature")
        self.call_command('import_groups_opdm',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info("Calc/Update MemberGroup FULL legislature")
        self.call_command('calc_membership_group',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info("Calc/Update Member and Group majority FULL  legislature")
        self.call_command('calc_majority_base',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info(f"Load/Update Voting legislature between {sd} end {ed}")
        self.call_command('import_votazioni_sparql',
                          legislatura=legislatura,
                          start_date=sd,
                          end_date=ed,
                          verbosity=3)

        self.logger.info(f"Load/Update MemberVote Deputy chamber between {sd} and {ed}")
        self.call_command('pipeline_votazioni',
                          legislatura=legislatura,
                          ramo='C',
                          start_date=sd,
                          end_date=ed,
                          verbosity=3)
        self.logger.info(f"Load/Update MemberVote Deputy chamber between {sd} and {ed} scraping version")
        self.call_command('import_votazioni_camera_raw',
                          legislatura=legislatura,
                          start_date=sd,
                          end_date=ed,
                          verbosity=3)

        self.logger.info(f"Load/Update MemberVote Senate between {sd} and {ed}")
        self.call_command('pipeline_votazioni',
                          legislatura=legislatura,
                          ramo='S',
                          sd=sd,
                          ed=ed,
                          verbosity=3)

        self.logger.info("Calc group cohesion FULL")
        self.call_command('calc_group_majority_cohesion',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info("Calc/Update GroupsMetricsHST")
        self.call_command('calc_historical_metrics_groups',
                          legislatura=legislatura,
                          start_date=sd,
                          end_date=ed,
                          verbosity=3)

        self.logger.info("Calc/Update MembershipsMetricsHST")
        self.call_command('calc_historical_metrics_memberships',
                          legislatura=legislatura,
                          start_date=sd,
                          end_date=ed,
                          verbosity=3)
        if full:
            self.logger.info("Calc/Update Memberships Metrics absolute")
            self.call_command('update_metric_memberships',
                              legislatura=legislatura,
                              verbosity=3)

        self.logger.info("Update metrics cache")
        self.call_command('metrics_update_cache',
                          verbosity=3)

        self.logger.info("Update positional power")
        self.call_command('pos_power_days',
                          days=10,
                          verbosity=3)
        self.logger.info("Update GOV")
        self.call_command('update_gov',
                          verbosity=3)
        self.logger.info("Update metrics cache")
        self.call_command('org_parl_update_cache',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info("Update GOV")
        self.call_command('update_gov',
                          verbosity=3)
        if full:
            self.logger.info("Update Persons")
            self.call_command('persons_update_cache',
                              leg=legislatura,
                              verbosity=3)
        self.logger.info("Update Timeline")
        self.call_command('timeline',
                          legislatura=legislatura,
                          verbosity=3)
        res = [cache.delete(x) for x in cache.keys(r'*pp_index*')]
        # res = [cache.delete(x) for x in cache.keys(r'*group_api*')]
        res = [cache.delete(x) for x in cache.keys(r'*parl_org_detail*')]
        res = [cache.delete(x) for x in cache.keys(r'*membership_list*')]

        fe_keys_to_delete = ["/",
                             "/deputati/*",
                             "/senatori/*",
                             "/organi_parlamentari/*",
                             "/attivita_legislativa/*/",
                             "/indice_di_forza/*",
                             "/votazioni/"]

        for key in fe_keys_to_delete:
            data = {
                'urlsToDelete': [f"{key}"],
                'token': env('CACHE_INVALIDATE_TOKEN')
            }
            username = env('USERNAME_PROD_OPP')
            password = env('PWD_PROD_OPP')
            credentials = base64.b64encode(f'{username}:{password}'.encode('utf-8')).decode('utf-8')
            response = requests.post('https://staging.new.openparlamento.it/api/delete-cache',
                                     headers={'Content-Type': 'application/json'},
                                     data=json.dumps(data))
            response = requests.post('https://parlamento19.openpolis.it/api/delete-cache',
                                     headers={'Content-Type': 'application/json',
                                              'Authorization': f'Basic {credentials}'},
                                     data=json.dumps(data))
