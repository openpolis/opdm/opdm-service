from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.models import Group
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor

from popolo.models import Organization
import pandas as pd


class Command(LoggingBaseCommand):
    """Command ETL connect Memberships Popolo and Membership Vote."""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        self.logger.info(f'Starting import for organizations legislatura {legislatura}')
        legislatura_padded = str(legislatura).zfill(2)
        groups_legislatura = Organization.objects.filter(classification__icontains='Gruppo politico')\
            .filter(parent__classification='Assemblea parlamentare',
                    parent__identifier__regex=legislatura_padded)

        data = pd.DataFrame(groups_legislatura, columns=['organization'])
        data['legislature'] = data['organization'].apply(lambda x:
                                                         x.parent.key_events.get(
                                                             key_event__event_type='ITL'
                                                         ).key_event)
        if data.shape[0] == 0:
            self.logger.warning(f'No data for legislatura {legislatura} memberships')
        else:
            etl = ETL(
                extractor=DataframeExtractor(data),
                loader=DjangoUpdateOrCreateLoader(django_model=Group,
                                                  fields_to_update=['legislature', ]))()
            if etl.loader.n_created > 0:
                self.logger.warning(f"Created {etl.loader.n_created} new groups. "
                                    f"Check them and assign majority/minority in case")
