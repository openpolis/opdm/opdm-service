def template_query_sparql(**kwargs):
    votazione = kwargs.get('votazione')
    name = kwargs.get('name')
    legislatura = kwargs.get('legislatura')
    aula = kwargs.get('aula')
    sd = kwargs.get('sd')
    ed = kwargs.get('ed')

    QUERY_SEDUTE = {
        'camera': f"""
        PREFIX ocd: <http://dati.camera.it/ocd/>

                        SELECT DISTINCT ?seduta ?number ?date
                        WHERE {{
                          ?seduta a ocd:seduta.
                          ?seduta dc:identifier ?number.
                          ?seduta dc:date ?date.

                          ?seduta ocd:rif_leg <http://dati.camera.it/ocd/legislatura.rdf/repubblica_{legislatura}> .

                          ?seduta ocd:rif_assemblea <http://dati.camera.it/ocd/assemblea.rdf/a{legislatura}> .


                        }}
                        ORDER BY ?date
                        """,
        'senato': f"""
                  PREFIX osr: <http://dati.senato.it/osr/>

                    SELECT distinct ?seduta ?number ?date
                    WHERE
                    {{
                        ?seduta a osr:SedutaAssemblea.
                        ?seduta osr:numeroSeduta ?number.
                        ?seduta osr:dataSeduta ?date.

                        ## filtro per la legislatura
                        ?seduta osr:legislatura {legislatura}
                        filter(?date >=xsd:date('{sd}') && ?date <= xsd:date('{ed}'))
                    }}
                    ORDER BY ?number"""
    }
    QUERY_VOTAZIONI = {
        'camera': f"""
        SELECT distinct ?seduta
        ?votazione
        ?original_title
        ?outcome
        ?is_final
        ?n_present
        ?n_voting
        ?n_ayes
        ?n_nos
        ?n_abstained
        ?n_majority
        ?is_confidence
        ?is_secret
        ?atto
        ?description_title
        ?attoidentifier
        ?data
        WHERE {{
        ?votazione a ocd:votazione;
        dc:date ?data;
        dc:title ?original_title;
        dc:description ?description_title;
        dc:relation ?resoconto;
        dc:type ?type;
        ocd:votazioneSegreta ?is_secret;
        ocd:votazioneFinale ?is_final;
        ocd:approvato ?outcome;
        ocd:presenti ?n_present;
        ocd:votanti ?n_voting;
        ocd:astenuti ?n_abstained;
        ocd:favorevoli ?n_ayes;
        ocd:contrari ?n_nos;
        ocd:maggioranza ?n_majority;
        ocd:richiestaFiducia ?is_confidence;
        ocd:rif_seduta ?seduta.
        FILTER(xsd:date(?data) >= xsd:date('{sd}') &&  xsd:date(?data) <= xsd:date('{ed}'))
        OPTIONAL {{?votazione ocd:rif_attoCamera ?atto}}
        OPTIONAL {{?atto dc:identifier ?attoidentifier}}
        ?votazione ocd:rif_leg <http://dati.camera.it/ocd/legislatura.rdf/repubblica_{legislatura}> .
        ?votazione ocd:rif_assemblea <http://dati.camera.it/ocd/assemblea.rdf/a{legislatura}> .
        }}
        """,
        'senato': f"""
        PREFIX osr: <http://dati.senato.it/osr/>
SELECT DISTINCT ?seduta ?number ?votazione ?original_title ?outcome
?n_present ?n_voting ?n_ayes ?n_nos ?n_majority
?n_abstained
?tipoVotazione ?atto
?attoidentifier
?date as ?data
WHERE
{{
    ?votazione a osr:Votazione;
        osr:numero ?number;
        osr:seduta ?seduta;
        rdfs:label ?original_title;
        osr:esito ?outcome;

        osr:presenti ?n_present;
        osr:votanti ?n_voting;
        osr:astenuti ?n_abstained;
        osr:favorevoli ?n_ayes;
        osr:contrari ?n_nos;

        ## osr:maggioranza ?maggioranza;
        ## osr:numeroLegale ?numeroLegale;
        osr:tipoVotazione ?tipoVotazione.
         OPTIONAL{{?votazione osr:maggioranza ?n_majority}}
            FILTER(
        regex(STR(?tipoVotazione), "elettronica", "i")
        || regex(STR(?tipoVotazione), "segreta", "i")
        || regex(STR(?tipoVotazione), "nominale con appello", "i")
    )
   OPTIONAL {{?votazione osr:oggetto ?oggetto}}
   OPTIONAL {{?oggetto osr:relativoA ?atto}}
   OPTIONAL {{?atto osr:numeroFase ?attoidentifier}}
    ?seduta a osr:SedutaAssemblea;
        osr:numeroSeduta ?number_seduta;
        osr:dataSeduta ?date;
        osr:legislatura {legislatura} .
        filter(?date >=xsd:date('{sd}') && ?date <= xsd:date('{ed}'))
FILTER (!regex(STR(?original_title), "ANNULLATA", "i")).
}}

"""
    }
    QUERY_VOTI = {
        'camera': f"""
select distinct ?person ?vote ?infoAssenza where
{{
    ?person ocd:rif_mandatoCamera ?mandato; a foaf:Person.

    ?d a ocd:deputato; ocd:aderisce ?aderisce;
    ocd:rif_leg <http://dati.camera.it/ocd/legislatura.rdf/repubblica_{legislatura}>;
    ocd:rif_mandatoCamera ?mandato.
    ?voto a ocd:voto;
       ocd:rif_votazione <http://dati.camera.it/ocd/votazione.rdf/{votazione}>;
       ocd:rif_votazione ?votazione;
       dc:type ?vote;
       ocd:rif_deputato ?d.

OPTIONAL{{?voto dc:description ?infoAssenza}}

?d foaf:surname ?cognome; foaf:firstName ?nome . }}         """,
        'senato': f"""
PREFIX osr: <http://dati.senato.it/osr/>

SELECT DISTINCT ?person ?vote
WHERE
{{
?person a osr:Senatore; foaf:firstName ?nome; foaf:lastName ?cognome.

?votazione a osr:Votazione; ?vote ?person
FILTER(?votazione = <http://dati.senato.it/votazione/{votazione}>)

}}      """
    }

    map_name_query = {
        'sedute':  QUERY_SEDUTE,
        'votazioni': QUERY_VOTAZIONI,
        'voti': QUERY_VOTI
    }
    query_dict = map_name_query.get(name)
    return query_dict.get(aula)
