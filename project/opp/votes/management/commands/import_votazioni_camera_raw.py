from urllib.parse import urlparse, parse_qs

from django.core.management import call_command
from ooetl import ETL
from ooetl.loaders import DjangoUpdateOrCreateLoader

from project.opp.acts.models import Bill
from project.opp.votes.management.commands.import_votazioni_sparql import get_type_vote
import requests
from bs4 import BeautifulSoup
from django.db.models import F, Value
from django.db.models.functions import Replace

from taskmanager.management.base import LoggingBaseCommand
from django.contrib.contenttypes.models import ContentType
from project.opp.votes.management.commands.utils.utils import parse_uri_id  # , parse_date
from project.opp.votes.models import Sitting
from ooetl.extractors import DataframeExtractor, Extractor
from popolo.models import Link, Organization, Source
import pandas as pd
import datetime
from project.opp.votes.models import MemberVote, Membership, Voting
from project.opp.votes.management.commands.import_voti_sparql import get_missing_voting

content = ContentType.objects.get(model='voting')


def get_n_margin(x):
    favorevoli = int(x['n_ayes'])
    maggioranza = int(x['n_majority'])
    return abs(favorevoli - maggioranza)


def get_sitting(sitting, assembly, data):
    identifier = parse_uri_id(sitting, assembly.identifier)
    number = None
    if 'CAMERA' in assembly.parent.name:
        date = datetime.datetime.strptime(data, '%Y%m%d')
        number = identifier.split('_')[-1]
    elif 'SENATO' in assembly.parent.name:
        date = datetime.datetime.strptime(data, '%Y-%m-%d')
    return Sitting.objects.get_or_create(identifier=identifier,
                                         assembly=assembly,
                                         defaults={
                                             'date': date,
                                             'number': number})[0]


class VotazioniCameraJsonExtractor(Extractor):
    """Json Extractor"""

    def __init__(self, leg, sd, ed):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.leg = leg
        self.sd = sd
        self.ed = ed
        self.typology = [("FINALE", ''),
                         ("EMENDAMENTO", Voting.AMENDMENT),
                         ("ARTICOLO", Voting.ARTICLE),
                         ("MOZIONE", Voting.MOTION),
                         ("ODG", Voting.ODG),
                         ("RISOLUZIONE", Voting.RESOLUTION),
                         ("DOC", ''),
                         ("PREGIUDIZIALE", Voting.PREJUDICIAL),
                         ("DIMISSIONI", Voting.RESIGNATION),
                         ("SEGRETA", ''),
                         ("FIDUCIA", '')]

        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=self.leg)
        self.org_camera = assemblee.get(name__icontains='camera')

    def get_params(self, url, value):
        parsed_url = urlparse(url)
        query_params = parse_qs(parsed_url.query)
        # Estrai il valore di 'id'
        id_value = query_params.get(value, [None])[0]
        return id_value

    def find_membro(self, membri, nome):
        nome_parts = nome.replace('\'', '').split(' ')
        membri = membri.annotate(nome=Replace(F("opdm_membership__person__given_name"), Value('\''), Value('')),
                                 cognome=Replace(F("opdm_membership__person__family_name"), Value('\''),
                                                 Value('')))
        try:
            return membri.get(cognome__iexact=nome_parts[0])
        except:
            pass
        try:
            return membri.get(cognome__istartswith=nome_parts[0],
                              nome__iexact=nome_parts[1])
        except:
            pass
        try:
            return membri.get(cognome__istartswith=nome_parts[0][:-1],
                              nome__iexact=nome_parts[1])
        except:
            pass
        try:
            return membri.get(cognome__icontains=nome_parts[0])
        except:
            pass
        try:
            return membri.get(cognome__istartswith=nome_parts[0],
                              nome__istartswith=nome_parts[1],
                              )
        except:
            pass
        try:
            return membri.get(cognome__istartswith=nome_parts[0][:-1])

        except:
            pass
        try:
            return membri.get(cognome__istartswith=nome_parts[0][:-1],
                              nome__istartswith=nome_parts[1],
                              )
        except:
            pass
        try:
            return membri.get(cognome__istartswith=nome_parts[0] + ' ' + nome_parts[1])
        except:
            raise Exception

    def get_typology(self, data, is_final, is_confidence):
        return get_type_vote(data['titolo'], data['titolo'], is_final, is_confidence)

    def get_sitting(self, number, data):
        return Sitting.objects.get_or_create(identifier=f"s{self.leg}_{number}",
                                             assembly=self.org_camera,
                                             defaults={
                                                 'date': data,
                                                 'number': number})[0]

    def create_votazione(self, data):
        is_final, is_confidence, is_secret = False, False, False
        seduta = self.get_sitting(data['numero_seduta'], data['date_ref'])
        if data['esito'] == 'A':
            outcome = 'Approvato'
        elif data['esito'] == 'R':
            outcome = 'Respinto'
        else:
            raise Exception
        if data['typology'] == 'FINALE':
            is_final = True
        if data['typology'] == 'FIDUCIA':
            is_confidence = True
        if data['typology'] == 'SEGRETA':
            is_secret = True
        if data['voting_typology']:
            res_typology = data['voting_typology']
        else:
            res_typology = self.get_typology(data, is_final, is_confidence)
        object, created = Voting.objects.get_or_create(
            identifier=f"vs{self.leg}_{data['numero_seduta']}_{data['numero_votazione']}",
            defaults={'original_title': data['titolo'],
                      'outcome': outcome,
                      'is_final': is_final,
                      'is_confidence': is_confidence,
                      'is_secret': is_secret,
                      'n_voting': int(data['votanti']),
                      'n_ayes': int(data['favorevoli']),
                      'n_nos': int(data['contrari']),
                      'n_abstained': int(data['astenuti']),
                      'n_majority': int(data['maggioranza']),
                      'description_title': '',
                      'sitting': seduta,
                      'n_margin': abs(int(data['favorevoli']) - int(data['maggioranza'])),
                      'number': data['numero_votazione'],
                      'type': res_typology,
                      'president': data['membro_presidente'],
                      'n_present': data['presenti']
                      })
        object.president = data['membro_presidente']
        object.save()
        return object

    def create_votanti(self, votazione, votanti):

        mapping = {'Astensione': MemberVote.ABSTAINED,
                   'In missione': MemberVote.MISSION,
                   'Favorevole': MemberVote.AYE,
                   'Contrario': MemberVote.NO,
                   'Non ha partecipato': MemberVote.ABSENT,
                   'Ha votato': MemberVote.SECRET,
                   'Presidente di turno': MemberVote.PRESIDENT}
        df = pd.DataFrame(votanti)
        id_found = [x.id for x in df['membership']]

        df['vote'] = df['vote'].apply(lambda x: mapping.get(x))
        res = get_missing_voting(self.org_camera, id_found, votazione.sitting_date,
                                 any(df['vote'].apply(lambda x: MemberVote.PRESIDENT in x)))
        df = pd.concat([df, res])
        df['voting'] = votazione
        ETL(
            extractor=DataframeExtractor(df),
            loader=DjangoUpdateOrCreateLoader(django_model=MemberVote, fields_to_update=[
                'vote']),
        )()

    def querydb(self, typology='', all=False, year=None, month=None, day=None):
        if all:
            source_typology, voting_typology = '', ''
        else:
            source_typology, voting_typology = typology
        page_corr = 0
        while True:
            page_corr += 1
            if all:
                main_url = (f"https://documenti.camera.it/apps/votazioni/votazionitutte/risultatidb.Asp?Legislatura="
                            f"{self.leg}&CDDANNO={year}&CDDMESE={month}&CDDGIORNO="
                            f"{day}&TipoRicerca=t&PAGESIZE=10&PagCorr={page_corr}")
            else:

                main_url = (
                    f"https://documenti.camera.it/apps/votazioni/votazionitutte/risultatidb.Asp?Legislatura="
                    f"{self.leg}&CDDANNO={year}&CDDMESE={month}&CDDGIORNO={day}"
                    f"&CDDNATURA={typology[0]}&TipoRicerca=t&PAGESIZE=10&PagCorr={page_corr}")

            response = requests.get(main_url)
            soup = BeautifulSoup(response.content, 'html.parser')
            if 'Non esistono votazioni elettroniche con la condizione di ricerca impostata' in soup.get_text():
                self.logger.info(f"No data for {year}-{month}-{day} {typology}")
                return
            self.logger.info(f"Getting info from {main_url}")
            a_tag_voto = soup.find_all('a', class_='voto')
            if not a_tag_voto:
                return
            for a_tag in a_tag_voto:
                url = a_tag.get('href')
                seduta, numero_votazione = self.get_params(url, "RifVotazione").split('_')
                numero_votazione = numero_votazione.zfill(3)
                try:
                    votazione = Voting.objects.get(identifier=f"vs{self.leg}_{seduta}_{numero_votazione}")
                except:
                    votazione = None
                if not votazione:
                    url_detail = "https://documenti.camera.it/apps/votazioni/votazionitutte/" + url
                    response_sub_detail = requests.get(url_detail)
                    soup_sub_detail = BeautifulSoup(response_sub_detail.content, 'html.parser')
                    date_ref = soup_sub_detail.find('div', class_="col-12 center-block text-center").find(
                        'strong').get_text()
                    date_ref = datetime.datetime.strptime(date_ref, '%d/%m/%Y').date()
                    if date_ref < datetime.datetime.strptime(self.sd, '%Y-%m-%d').date():
                        return
                    membri = Membership.get_membership_date_assembly(self.org_camera, date_ref)

                    nome_presidente = \
                        soup_sub_detail.find('div', class_="col-12 center-block text-center").find_all('strong')[
                            2].get_text()
                    membro_presidente = self.find_membro(membri, nome_presidente)

                    tabella_dati = soup_sub_detail.find('div', class_="dati-tabellare")
                    colonne = tabella_dati.find_all('div', class_="col-5 rowTable")
                    valori = tabella_dati.find_all('div', class_="col-3 rowTable right-block text-right")
                    detail_votazione = {
                        'titolo': soup_sub_detail.find('div', 'provvedimento').get_text(strip=True).replace(
                            '(titolo provvisorio)', ''),
                        'typology': source_typology,
                        'voting_typology': voting_typology,
                        'date_ref': date_ref,
                        'numero_votazione': numero_votazione.strip(),
                        'numero_seduta': seduta.strip(),
                        'membro_presidente': membro_presidente,
                        'presenti': '',
                        'votanti': '',
                        'astenuti': '',
                        'maggioranza': '',
                        'favorevoli': '',
                        'contrari': '',
                        'esito': ''}
                    for colonna, valore in zip(colonne, valori):
                        detail_votazione[colonna.get_text(strip=True).lower()] = valore.get_text(strip=True)
                    esito = (tabella_dati.find('div', class_='rowTableEsito')
                             .get_text(strip=True).lower())
                    if 'respinge' in esito:
                        detail_votazione['esito'] = 'R'
                    elif 'approva' in esito:
                        detail_votazione['esito'] = 'A'
                    else:
                        raise Exception
                    votazione = self.create_votazione(detail_votazione)
                print(votazione.identifier)
                if votazione.members_votes.all().count() > 0 and votazione.groups_votes.all().count() > 0:
                    if votazione.sitting.date < datetime.datetime.strptime(self.sd, '%Y-%m-%d').date():
                        return
                    continue
                else:
                    url_detail = "https://documenti.camera.it/apps/votazioni/votazionitutte/" + url
                    response_sub_detail = requests.get(url_detail)
                    soup_sub_detail = BeautifulSoup(response_sub_detail.content, 'html.parser')
                    date_ref = soup_sub_detail.find('div', class_="col-12 center-block text-center").find(
                        'strong').get_text()
                    date_ref = datetime.datetime.strptime(date_ref, '%d/%m/%Y').date()
                    if date_ref < datetime.datetime.strptime(self.sd, '%Y-%m-%d').date():
                        return
                    membri = Membership.get_membership_date_assembly(self.org_camera, date_ref)
                    nome_presidente = \
                        soup_sub_detail.find('div', class_="col-12 center-block text-center").find_all('strong')[
                            2].get_text()
                    membro_presidente = self.find_membro(membri, nome_presidente)
                    votazione.president = membro_presidente
                    votazione.save()

                votanti = soup_sub_detail.find('table', id='tabellaPartecipazioneVoto').find('tbody').find_all('tr')
                votanti_detail = []
                for votante in votanti:
                    nome, gruppo, outcome = votante.find_all('td')
                    nome = nome.get_text(strip=True)
                    outcome = outcome.get_text(strip=True)
                    membro = self.find_membro(membri, nome)

                    votanti_detail.append(
                        {'vote': outcome,
                         'voting': None,
                         'membership': membro

                         }
                    )
                    membri = membri.exclude(id=membro.id)
                self.create_votanti(votazione, votanti_detail)
                self.call_command('calc_group_vote',
                                  identifier=votazione.identifier,
                                  verbosity=3)
                votazione = Voting.objects.get(id=votazione.id)
                votazione.n_absent = MemberVote.objects.filter(voting=votazione, vote=MemberVote.ABSENT).count()
                votazione.n_mission = MemberVote.objects.filter(voting=votazione, vote=MemberVote.MISSION).count()
                votazione.n_rebels = MemberVote.objects.filter(voting=votazione, is_rebel=True).count()
                votazione.save()

    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.

        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        # self.logger.info(f"----- Starting {command_name} -----")
        # start_time = time.time()
        call_command(command_name, *args, **options)
        # elapsed = time.time() - start_time
        # self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

    def associate_act(self):
        base_url = f"https://documenti.camera.it/apps/votazioni/votazionitutte/formVotazioni.asp?legislatura={self.leg}"
        response = requests.get(base_url)
        soup = BeautifulSoup(response.content, 'html.parser')
        acts = soup.find('select', id="CDDNUMEROATTO").find_all("option")
        acts = [x.get('value') for x in acts[1:]]

        for act in acts:
            page_corr = 0
            print(f'Associating act {act}')
            while True:
                page_corr += 1
                sub_url = (f"https://documenti.camera.it/apps/votazioni/votazionitutte/risultatidb.Asp?Legislatura=19"
                           f"&CDDNUMEROATTO={act}&TipoRicerca=t&PAGESIZE=10&PagCorr={page_corr}")

                response = requests.get(sub_url)
                soup = BeautifulSoup(response.content, 'html.parser')
                a_tag_voto = soup.find_all('a', class_='voto')
                if not a_tag_voto:
                    break
                for a_tag in a_tag_voto:
                    url = a_tag.get('href')
                    seduta, numero_votazione = self.get_params(url, "RifVotazione").split('_')
                    numero_votazione = numero_votazione.zfill(3)
                    try:
                        votazione = Voting.objects.get(identifier=f"vs{self.leg}_{seduta.zfill(3)}_{numero_votazione.zfill(3)}")
                        bill = Bill.objects.get(identifier=f"C.{act.strip()}", assembly=self.org_camera)
                        votazione.bills.add(bill)
                    except:
                        print(f"error")
                        continue

    def extract(self):
        sd = self.sd
        ed = self.ed
        start_date = datetime.datetime.strptime(sd, '%Y-%m-%d')
        end_date = datetime.datetime.strptime(ed, '%Y-%m-%d')

        # Ciclo per estrarre tripletta anno, mese, giorno

        current_date = start_date
        while current_date <= end_date:
            year, month, day = current_date.year, current_date.month, current_date.day
            for typology in self.typology:
                self.querydb(typology, all=False, year=year, month=month, day=day)
            self.querydb(all=True, year=year, month=month, day=day)
            current_date += datetime.timedelta(days=1)
        self.associate_act()


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )
        parser.add_argument(
            '--sd',
            dest="start_date",
            required=True,
            help='Start date collection votes, format YYYY-MM-DD.')

        parser.add_argument(
            '--ed',
            dest="end_date",
            required=True,
            help='End date collection votes, format YYYY-MM-DD.')

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        sd = options['start_date']
        ed = options['end_date']

        self.logger.info(f'Starting import for {legislatura} raw Camera')

        legislatura_padded = str(legislatura).zfill(2)
        VotazioniCameraJsonExtractor(legislatura_padded, sd, ed).extract()
