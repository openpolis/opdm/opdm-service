from config.settings.base import ROOT_PATH
from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.models import Group
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor

from popolo.models import Organization
import pandas as pd


class Command(LoggingBaseCommand):
    """Command ETL connect Memberships Popolo and Membership Vote."""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        legislatura = options["legislatura"]
        path_file = ROOT_PATH / "data" / "openparlamento" / 'groups_acronyms.csv'
        df_groups = pd.read_csv(path_file, sep=",")
        self.logger.info(f'Starting import for organizations legislatura {legislatura}')
        legislatura_padded = str(legislatura).zfill(2)
        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=legislatura_padded)
        groups_legislatura = Organization.objects.filter(classification__icontains='Gruppo politico')\
            .filter(parent__classification='Assemblea parlamentare',
                    parent__identifier__regex=legislatura_padded)

        data = pd.DataFrame(groups_legislatura, columns=['organization'])
        data['legislature'] = data['organization'].apply(lambda x:
                                                         x.parent.key_events.get(
                                                             key_event__event_type='ITL'
                                                         ).key_event)
        data['acronym'] = data.organization.apply(
            lambda x: df_groups[df_groups['organization_id'] == x.id]['acronym'].iloc[0])
        if data.shape[0] == 0:
            self.logger.warning(f'No data for legislatura {legislatura} memberships')
        else:
            ETL(
                extractor=DataframeExtractor(data),
                loader=DjangoUpdateOrCreateLoader(django_model=Group,
                                                  fields_to_update=['legislature',
                                                                    'acronym', ]))()

        for assemblea in assemblee:
            for meta_group in ['Maggioranza', 'Opposizione', 'Nessun gruppo']:
                supports_majoity = None
                if meta_group == 'Maggioranza':
                    supports_majoity = True
                    exclude_rebel_calc = False
                elif meta_group == 'Opposizione':
                    supports_majoity = False
                    exclude_rebel_calc = False
                elif meta_group == 'Nessun gruppo':
                    supports_majoity = None
                    exclude_rebel_calc = True

                Group.objects.all_and_metagroups().update_or_create(
                    organization=assemblea,
                    acronym=meta_group,
                    legislature=assemblea.key_events.get(key_event__event_type='ITL').key_event,
                    defaults={
                        'supports_majority': supports_majoity,
                        'exclude_rebel_calc': exclude_rebel_calc,
                        'is_meta_group': True

                    }
                )
        for meta_group in ['Maggioranza', 'Opposizione']:
            Group.objects.all_and_metagroups().update_or_create(
                organization=None,
                acronym=meta_group,
                legislature=assemblea.key_events.get(key_event__event_type='ITL').key_event,
                defaults={
                    'supports_majority': True if meta_group == 'Maggioranza' else False,
                    'exclude_rebel_calc': False,
                    'is_meta_group': True

                }
            )

        Group.objects.filter(organization__name__icontains='"Misto"').update(exclude_rebel_calc=True)
