from project.opp.votes.models import MemberVote, Membership, MembershipsMetricsHST
from project.calendars.models import CalendarDaily
from popolo.models import KeyEvent
from taskmanager.management.base import LoggingBaseCommand
import datetime

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):
    """"""
    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )
        parser.add_argument(
            '--sd',
            dest="start_date",
            required=True,
            help='Start date collection votes, format YYYY-MM-DD.')

        parser.add_argument(
            '--ed',
            dest="end_date",
            required=True,
            help='End date collection votes, format YYYY-MM-DD.')

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        num_legislatura = options["legislatura"]
        sd = options['start_date']
        ed = options['end_date']
        self.logger.info(f'Starting import for historical metrics legislatura {num_legislatura}')
        legislatura_padded = str(num_legislatura).zfill(2)
        legislatura = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        start_legislatura = legislatura.start_date
        end_legislatura = (legislatura.end_date or today_strf)

        for membro in Membership.objects.all():
            # self.logger.info(f'\tStarting import for historical metrics for {membro}')
            start_membro = membro.opdm_membership.start_date
            end_membro = (membro.opdm_membership.end_date or today_strf)
            year_months = CalendarDaily.objects\
                .filter(date__gte=max(start_membro, start_legislatura),
                        date__lte=min(end_legislatura, end_membro)) \
                .filter(date__gte=sd, date__lte=ed) \
                .values('year', 'month').distinct().order_by('year', 'month')
            for year_month in year_months:
                # self.logger.info(f'\t\t  {year_month}')
                start_month = CalendarDaily.objects.get(
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    is_month_start=True).date
                end_month = CalendarDaily.objects.get(
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    is_month_end=True).date
                mvotes = MemberVote.objects.filter(membership=membro)\
                    .filter(voting__sitting__assembly__key_events__key_event=legislatura)\
                    .distinct()\
                    .filter(voting__sitting__date__gte=start_month,
                            voting__sitting__date__lte=end_month)

                n_vot = mvotes.count()
                if n_vot == 0:
                    defaults = {
                        'n_voting': n_vot,
                        'n_present': None,
                        'n_absent': None,
                        'n_mission': None,
                        'n_rebels': None,
                        'n_fidelity': None

                    }
                else:
                    defaults = {
                        'n_voting': n_vot,
                        'n_present': mvotes.filter(vote__in=['PNV',
                                                             'RNV',
                                                             'ABST',
                                                             'PRES',
                                                             'SEC',
                                                             'AYE',
                                                             'NO']).count(),
                        'n_absent': mvotes.filter(vote__in=['ABSE']).count(),
                        'n_mission': mvotes.filter(vote__in=['MIS']).count(),
                        'n_rebels': mvotes.filter(is_rebel=True).count(),
                        'n_fidelity': mvotes.filter(is_fidelity=True).count()

                    }

                MembershipsMetricsHST.objects.update_or_create(
                    membership=membro,
                    year=year_month.get('year'),
                    month=year_month.get('month'),
                    defaults=defaults
                )
