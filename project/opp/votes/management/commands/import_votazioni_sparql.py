from config.settings.base import ENDPOINTS_ASSEMBLEE_PARLAMENTARI

from project.tasks.parsing.sparql.utils import get_bindings
from taskmanager.management.base import LoggingBaseCommand
from django.contrib.contenttypes.models import ContentType

from project.opp.votes.management.commands.raw_sparql.template_query import template_query_sparql
from project.opp.votes.management.commands.utils.utils import parse_uri_id  # , parse_date
from project.opp.votes.models import Sitting, Voting
from project.opp.acts.models import Bill

from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor, Extractor

from popolo.models import Link, Organization, Source
import pandas as pd
import datetime
import html
import re


content = ContentType.objects.get(model='voting')


def get_n_margin(x):
    favorevoli = int(x['n_ayes'])
    maggioranza = int(x['n_majority'])
    return abs(favorevoli-maggioranza)


def get_type_vote(original_title, description_title, is_final, is_confidence):
    titles = [original_title.lower(), description_title.lower()]
    titles_original = [original_title, description_title]
    # if is_final:
    #     return Voting.FINAL
    if any('emendamento'  in x for x in titles) \
        or any('emendemanto'  in x for x in titles) \
        or any(x.startswith('em.') for x in titles) or any(x.startswith('emm.') for x in titles):
        return Voting.AMENDMENT
    elif any('cordinamento' in x for x in titles) \
        or   any([re.match('.*proposta.*di.*coordinamento.*', x)  for x in titles])\
        or any('coord.' in x for x in titles):
        return Voting.COORD
    elif any('ordine del giorno' in x for x in titles) \
        or any('odg' in x for x in titles) or any('ordine del gio' in x for x in titles):
        return Voting.ODG
    elif any('Mozione' in x for x in titles_original):
        return Voting.MOTION
    elif any('risoluzione' in x for x in titles) or any('prop. risol.' in x for x in titles) or any('prop.ris.' in x for x in titles):
        return Voting.RESOLUTION
    elif any([re.match('.*dichiarazione.*urgenza.*', x)  for x in titles]):
        return Voting.EMERG_DECLARATION
    elif any('dimissioni' in x for x in titles):
        return Voting.RESIGNATION
    elif any('questione sospensiv' in x for x in titles):
        return Voting.SUSPENSIVE
    elif any('pregiudiz' in x for x in titles) \
        or any([re.match('.*quest.*pegiudiz.*', x)  for x in titles]):
        return Voting.PREJUDICIAL
    elif any([re.match('.*chiusura.*discuss.*', x)  for x in titles]):
        return Voting.CLOSING_DEBATE
    elif any([re.match('.*non passare.*esam.*', x)  for x in titles])\
        or any([re.match('.* non passaggio.*esame articol.*', x)  for x in titles]):
        return Voting.DONT_PROCEED
    # elif any('comunicazioni' in x for x in titles) or  any('comunicaz. ' in x for x in titles):
    #     return Voting.COM
    elif any('nota di variazioni' in x for x in titles) or  any('comunicaz. ' in x for x in titles):
        return Voting.VARIATION_NOTE
    elif any('proposta della giunta ' in x for x in titles) or any('proposta giunta ' in x for x in titles):
        return Voting.COUNCIL_PROPOSAL
    elif  any([re.match('.*elezion.*component.*', x)  for x in titles]):
        return Voting.ELECTION
    elif is_final:
        return Voting.ALL_ACT
    elif any('articolo' in x for x in titles) or any('art.' in x for x in titles):
        return Voting.ARTICLE
    elif is_confidence:
        return Voting.ALL_ACT


def get_sitting(sitting, assembly, data):
    identifier = parse_uri_id(sitting, assembly.identifier)
    number = None
    if 'CAMERA' in assembly.parent.name:
        date = datetime.datetime.strptime(data, '%Y%m%d')
        number = identifier.split('_')[-1]
    elif 'SENATO' in assembly.parent.name:
        date = datetime.datetime.strptime(data, '%Y-%m-%d')
    return Sitting.objects.get_or_create(identifier=identifier,
                                         assembly=assembly,
                                         defaults={
                                             'date': date,
                                             'number': number})[0]


class JsonExtractor(Extractor):
    """Json Extractor"""

    def __init__(self, df):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.df = df

    def extract(self):
        res = []
        for r in self.df:
            res.append(dict((k, r[k]["value"]) for k in r.keys()))
        od = pd.DataFrame(res)
        return od


class VotazioniTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, assemblea):
        Transformation.__init__(self)
        self.assemblea = assemblea

    def transform(self):
        outcomes = {
            'approvato': 'Approvato',
            'respinto': 'Respinto',
            '1': 'Approvato',
            '0': 'Respinto'
        }
        od = self.etl.original_data.copy()
        od = od.where(pd.notnull(od), None)

        od["sitting"] = od.apply(lambda x: get_sitting(x['seduta'], self.assemblea, x['data']), axis=1)
        od = od[od['sitting'].notna()]
        od["identifier"] = od["votazione"].apply(parse_uri_id)
        od['outcome'] = od['outcome'].apply(lambda x: outcomes.get(str(x)))
        od['n_margin'] = od.apply(lambda x: get_n_margin(x), axis=1)

        if self.assemblea.parent.name == 'CAMERA DEI DEPUTATI':
            od['number'] = od["identifier"].apply(lambda x: int(x.split('_')[-1]))
            od['is_final'] = od.apply(lambda x: bool(int(x['is_final'])) if not Voting.objects.filter(identifier=x['identifier']) else Voting.objects.get(identifier=x['identifier']).is_final, axis=1)
            od['is_confidence'] = od['is_confidence'].apply(lambda x: bool(int(x)))
            od['is_secret'] = od['is_secret'].apply(lambda x: True if str(x) == '1' else False)
            od['description_title'] = od['description_title'].apply(lambda x: html.unescape(html.unescape(x)))
        if self.assemblea.parent.name == 'SENATO DELLA REPUBBLICA':
            od['is_final'] = od.apply(lambda x: 'finale' in x['original_title'].lower() if not Voting.objects.filter(identifier=x['identifier']) else Voting.objects.get(identifier=x['identifier']).is_final, axis=1)
            od['is_confidence'] = od['original_title'].apply(lambda x: 'fiducia' in x)
            od['is_secret'] = od['tipoVotazione'].apply(lambda x: True if str(x) == 'segreta' else False)
            od = od.drop(['tipoVotazione'], axis=1)
        od['type'] = od.apply(lambda x: get_type_vote(x['original_title'],
                                                      x['description_title'] if 'description_title' in od.columns else '',
                                                      x['is_final'],
                                                      x['is_confidence']), axis=1)
        od = od.drop(['seduta', 'votazione', 'data'], axis=1)
        self.etl.processed_data = od
        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )
        parser.add_argument(
            '--sd',
            dest="start_date",
            required=True,
            help='Start date collection votes, format YYYY-MM-DD.')

        parser.add_argument(
            '--ed',
            dest="end_date",
            required=True,
            help='End date collection votes, format YYYY-MM-DD.')
    @staticmethod
    def find_longest_or_alphabetical(titles):
        # Ordinare prima per lunghezza decrescente, poi alfabeticamente
        return sorted(titles, key=lambda x: (-len(x), x))[0]
    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        sd = options['start_date']
        ed = options['end_date']
        tipologia = 'votazioni'
        self.logger.info(f'Starting import for {tipologia} {legislatura}')
        legislatura_padded = str(legislatura).zfill(2)
        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=legislatura_padded)

        def get_query(**kwargs):
            query = template_query_sparql(name='votazioni',
                                          **kwargs)
            return get_bindings(
                    ENDPOINTS_ASSEMBLEE_PARLAMENTARI.get(kwargs.get('aula')),
                    query,
                    legacy=True
            )

        for aula in ENDPOINTS_ASSEMBLEE_PARLAMENTARI.keys():

            assemblea = assemblee.get(identifier__icontains=aula)
            self.logger.info(f'\t Sarting import for {tipologia} {legislatura} {aula} {sd} {ed}')
            data = JsonExtractor(get_query(legislatura=legislatura,
                                           tipologia=tipologia,
                                           aula=aula,
                                           sd=sd,
                                           ed=ed)).extract()
            data = data.drop_duplicates()
            if data.shape[0] == 0:
                self.logger.warning(f'No data for {aula} legislation '
                                    f'{legislatura} for category {tipologia} {sd} {ed}')
                continue
            if 'atto' not in data.columns:
                data['atto'] = None
            if 'attoidentifier' not in data.columns:
                data['attoidentifier'] = None
            data_without_atto = data.drop(['atto', 'attoidentifier'], axis=1).drop_duplicates()
            if aula == 'camera':
                data_without_atto = data_without_atto.groupby(
                    by=[col for col in data_without_atto.columns if col not in ['original_title', 'description_title']]
                ).agg({'original_title': list,
                       'description_title': list}).reset_index()
                data_without_atto['original_title'] = data_without_atto['original_title'].apply(self.find_longest_or_alphabetical)
                data_without_atto['description_title'] = data_without_atto['description_title'].apply(
                self.find_longest_or_alphabetical)
            if not data_without_atto['votazione'].is_unique:
                self.logger.error(f'Multiple data for {aula} legislation '
                                  f'{legislatura} for category {tipologia}  {sd} {ed}')
                raise Exception
            columns_to_update = ['outcome',
                                 'number',
                                 'sitting',
                                 'is_final',
                                 'n_present',
                                 'n_voting',
                                 'n_ayes',
                                 'n_nos',
                                 'n_absteined',
                                 'n_majority',
                                 'is_confidence',
                                 'is_secret',
                                 'original_title',
                                 'type',
                                 'n_margin']
            if aula == 'camera':
                columns_to_update.append('description_title')
            ETL(
                extractor=DataframeExtractor(data_without_atto),
                transformation=VotazioniTransformation(assemblea=assemblea),
                loader=DjangoUpdateOrCreateLoader(django_model=Voting, fields_to_update=columns_to_update),
            )()
            data = data.where(pd.notnull(data), None)
            for index, row in data.iterrows():
                source, created = Source.objects.update_or_create(url=row['votazione'],
                                                                  defaults={'note': f'Open data {aula.title()}'})
                votazione = Voting.objects.get(identifier=parse_uri_id(row["votazione"]),
                                               sitting__assembly=assemblea)
                votazione.sources.update_or_create(content_type=content, source=source)
                if row['attoidentifier']:
                    link, created = Link.objects.update_or_create(url=row['atto'],
                                                                  defaults={'note': f'Open data {aula.title()}'})
                    votazione.links.update_or_create(content_type=content, link=link)
                    assembly = assemblee.get(identifier__icontains=aula)
                    act = Bill.objects.get(identifier__iregex=f"[cs].{row['attoidentifier']}$", assembly=assembly)
                    votazione.bills.add(act)
                    if aula == 'senato':
                        if (act.descriptive_title or '') in (votazione.description_title or '') or\
                                not act.descriptive_title:
                            continue
                        else:
                            if votazione.description_title:
                                votazione.description_title = ' - '.join(
                                    [(votazione.description_title or ''), (act.descriptive_title or '')])
                            else:
                                votazione.description_title = act.descriptive_title
                            votazione.save()
                elif votazione.type=='ODG' and aula == 'camera':
                    regex = r"DDL n.[ 0]*([1-9].*)"
                    matches = re.search(regex, row['original_title'])
                    if matches:
                        id_act = matches.groups()[0]
                        act = Bill.objects.get(identifier__iregex=f"C.{id_act}$", assembly=assembly)
                        votazione.bills.add(act)

        self.logger.info(f'Ending import for {tipologia} {legislatura}')
