from config.settings.base import ENDPOINTS_ASSEMBLEE_PARLAMENTARI
from django.db.models.functions import Coalesce
from django.db.models import F, Value, Case, When, Sum

from project.tasks.parsing.sparql.utils import get_bindings
from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.management.commands.raw_sparql.template_query import template_query_sparql
from project.opp.votes.management.commands.utils.utils import parse_uri_id  # , parse_date
from project.opp.votes.models import MemberVote, Membership, Voting

from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor, Extractor

from popolo.models import Organization
import pandas as pd
import datetime
from django.contrib.contenttypes.models import ContentType


membervote_content = ContentType.objects.get(model='membervote')


def get_membership(assembly, person_identifier, data):
    today = datetime.datetime.now()
    try:
        return Membership.objects.filter(opdm_membership__organization=assembly)\
            .annotate(new_end_date=Coalesce(F('opdm_membership__end_date'), Value(today.strftime('%Y-%m-%d'))))\
            .filter(opdm_membership__start_date__lte=data, new_end_date__gt=data).distinct()\
            .get(opdm_membership__person__identifiers__identifier=person_identifier)
    except Exception as e:
        print(person_identifier)
        print(e)
        raise Exception


def get_missing_voting(assembly, id_found, data, is_president_in_data):
    today = datetime.datetime.now()
    res = pd.DataFrame(columns=['vote', 'voting', 'membership'])
    memberships_not_found = Membership.objects.filter(opdm_membership__organization=assembly)\
        .annotate(new_end_date=Coalesce(F('opdm_membership__end_date'), Value(today.strftime('%Y-%m-%d'))))\
        .filter(opdm_membership__start_date__lte=data, new_end_date__gt=data).exclude(id__in=id_found)
    for member in memberships_not_found:
        if not is_president_in_data and assembly.parent.name == 'CAMERA DEI DEPUTATI':
            if member\
                .opdm_membership\
                .person.memberships\
                .filter(
                    role__icontains='Presidente Organo di presidenza parlamentare',
                    organization__parent=assembly).count() == 1:
                res.loc[len(res)] = [MemberVote.PRESIDENT, None, member]
            else:
                res.loc[len(res)] = [MemberVote.ABSENT, None, member]
        else:
            res.loc[len(res)] = [MemberVote.ABSENT, None, member]
    return res


def map_votes(esito):
    map_vote_dict = {
        'favorevole': MemberVote.AYE,
        'contrario': MemberVote.NO,
        'incongedomissione': MemberVote.MISSION,
        'in missione': MemberVote.MISSION,
        'presidente': MemberVote.PRESIDENT,
        'presidente di turno': MemberVote.PRESIDENT,
        'astenuto': MemberVote.ABSTAINED,
        'astensione': MemberVote.ABSTAINED,
        'presentenonvotante': MemberVote.PRESENT_NOT_VOTING,
        'richiedentenonvotante': MemberVote.REQUIRING_NOT_VOTING,
        'non ha partecipato': MemberVote.ABSENT,
        'ha votato': MemberVote.SECRET,
        'votante': MemberVote.SECRET

    }
    return map_vote_dict.get(esito)


class JsonExtractor(Extractor):
    """Json Extractor
    """

    def __init__(self, df):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.df = df

    def extract(self):
        res = []
        for r in self.df:
            res.append(dict((k, r[k]["value"]) for k in r.keys()))
        od = pd.DataFrame(res)
        return od


class VotiTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, assemblea, votazione):
        Transformation.__init__(self)
        self.assemblea = assemblea
        self.data = votazione.sitting.date
        self.votazione = votazione

    def transform(self):

        od = self.etl.original_data.copy()
        if self.assemblea.parent.name == 'CAMERA DEI DEPUTATI':
            od['membership'] = od['person'].apply(
                lambda x: get_membership(assembly=self.assemblea, person_identifier=x, data=self.data))
            od['vote'] = od.infoAssenza.combine_first(od.vote)
            od['vote'] = od['vote'].apply(lambda x: map_votes(x.lower()))
            od = od.drop(['infoAssenza'], axis=1)

        if self.assemblea.parent.name == 'SENATO DELLA REPUBBLICA':
            od['membership'] = od['person'].apply(
                lambda x: get_membership(assembly=self.assemblea, person_identifier=x, data=self.data))
            od['vote'] = od['vote'].apply(lambda x: parse_uri_id(x).lower())
            if self.votazione.is_secret:
                od = od[~od['vote'].isin(['presente'])]
            else:
                od = od[~od['vote'].isin(['presente', 'votante'])]
            od['vote'] = od['vote'].apply(map_votes)
        president = od[(od['vote'] == 'PRES')]
        if not president.empty:
            self.votazione.president = president.iloc[0]['membership']
        if not od['person'].is_unique:

            cases = sorted(list(od[od['person'].isin(od[od['person'].duplicated()].person)].vote.unique()))
            if sorted(cases) == ['RNV', 'SEC']:
                dupl = od[od['person'].isin(od[od['person'].duplicated()].person)]
                to_eliminate = dupl[dupl['vote'] == 'RNV']
                od = od[~od.index.isin(to_eliminate.index)]

            elif cases == ['PRES']:
                od = od[~(od['vote'] == 'PRES')]

            elif sorted(cases) == ['AYE', 'PRES'] or sorted(cases) == ['NO', 'PRES']:
                od = od[~(od['vote'] == 'PRES')]

            if not od['person'].is_unique:
                raise Exception

        od = od.drop(['person'], axis=1)
        id_found = [x.id for x in od['membership']]
        is_president_in_data = not president.empty
        res = get_missing_voting(self.assemblea, id_found, self.data, is_president_in_data)
        od = pd.concat([od, res])
        od['voting'] = self.votazione

        if not od['membership'].is_unique:
            raise Exception
        self.etl.processed_data = od
        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

        parser.add_argument(
            '--ramo',
            choices=['C', 'S'],
            dest="ramo",
            help='Ramo parlamento')

        parser.add_argument(
            "--i",
            type=str,
            dest='identifier',
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        tipologia = 'voti'
        ramo = {'C': 'camera',
                'S': 'senato'}.get(options["ramo"])

        identifier_votazione = options["identifier"]
        self.logger.info(f'Starting import for {tipologia} {legislatura}')
        legislatura_padded = str(legislatura).zfill(2)
        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=legislatura_padded)

        def get_query(**kwargs):

            query = template_query_sparql(name='voti', **kwargs)
            return get_bindings(
                    ENDPOINTS_ASSEMBLEE_PARLAMENTARI.get(kwargs.get('aula')),
                    query,
                    legacy=True
            )

        assemblea = assemblee.get(identifier__icontains=ramo)
        votazione = Voting.objects.get(identifier=identifier_votazione)
        self.logger.info(f'\t Sarting import for {tipologia} {legislatura} {ramo} {votazione.identifier} '
                         f'{votazione.sitting.date}')
        data = JsonExtractor(get_query(tipologia=tipologia,
                                       legislatura=legislatura,
                                       aula=ramo,
                                       votazione=votazione.identifier)).extract()
        if 'CAMERA' in assemblea.identifier:
            members_camera = Membership.get_membership_date_assembly(assemblea, votazione.sitting.date.strftime('%Y-%m-%d')).count() - 1
            members_extracted = data.shape[0]
            self.logger.info(
                f"{ramo}-votazione {votazione.identifier} expecting {members_camera} and found {members_extracted}")
            if members_camera > members_extracted and votazione.sitting_date.strftime('%Y-%m-%d') != '2024-04-10':
                self.logger.warning(f"{ramo}-votazione {votazione.identifier} missing votes, expecting {members_camera} and found {members_extracted}")
                return None
        if data.shape[0] == 0:
            self.logger.warning(f'No data for {ramo} legislation {legislatura}'
                                f' for category {tipologia} {votazione.identifier}')
            return None

        ETL(
            extractor=DataframeExtractor(data),
            transformation=VotiTransformation(assemblea=assemblea, votazione=votazione),
            loader=DjangoUpdateOrCreateLoader(django_model=MemberVote, fields_to_update=[
                'vote']),
        )()
        votazione.n_absent = MemberVote.objects.filter(voting=votazione, vote=MemberVote.ABSENT).count()
        votazione.n_mission = MemberVote.objects.filter(voting=votazione, vote=MemberVote.MISSION).count()
        votazione.n_rebels = MemberVote.objects.filter(voting=votazione, is_rebel=True).count()
        votazione.n_present_not_voting = MemberVote.objects.filter(voting=votazione,
                                                                   vote=MemberVote.PRESENT_NOT_VOTING).count()
        votazione.n_requiring_not_voting = MemberVote.objects.filter(voting=votazione,
                                                                     vote=MemberVote.REQUIRING_NOT_VOTING).count()
        president = MemberVote.objects.filter(voting=votazione,
                                              vote=MemberVote.PRESIDENT)
        if president:
            votazione.president = president[0].membership
        else:
            pres_row = data[data['vote'].str.contains("presidente")]
            if not pres_row.empty:
                votazione.president = get_membership(assembly=assemblea, person_identifier=pres_row.iloc[0]['person'],
                                                     data=votazione.sitting.date)

        votazione.save()
        # fix rebels votes
        votings = MemberVote.objects.annotate(rebel_v=Case(When(is_rebel=True, then=Value(1)), default=Value(0))).values('voting',
                                                                                                               'voting__n_rebels').annotate(
            sum_rebels=Sum(F('rebel_v'))).order_by().exclude(voting__n_rebels=F('sum_rebels'))
        for voting in votings:
            vote = Voting.objects.get(id=voting['voting'])
            vote.n_rebels = voting.get('sum_rebels')
            vote.save()

