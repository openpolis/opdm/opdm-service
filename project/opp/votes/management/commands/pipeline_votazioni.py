from django.core import management
from django.db.models import Q
from taskmanager.management.base import LoggingBaseCommand
from project.opp.votes.models import MemberVote, Voting, GroupVote
from popolo.models import Organization
import logging
from django.core.management import call_command
import time


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

        parser.add_argument(
            '--ramo',
            choices=['C', 'S'],
            dest="ramo",
            help='Ramo parlamento')
        parser.add_argument(
            '--sd',
            dest="start_date",
            help='Start date')

        parser.add_argument(
            '--ed',
            dest="end_date",
            help='End date')

        parser.add_argument(
            '--refresh',
            dest="refresh",
            type=bool,
            default=False,
            help='If you want to refresh already calculated data')
    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.

        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        # self.logger.info(f"----- Starting {command_name} -----")
        # start_time = time.time()
        call_command(command_name, *args, **options, stdout=self.stdout)
        # elapsed = time.time() - start_time
        # self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.logger.setLevel(
            {
                0: logging.ERROR,
                1: logging.WARNING,
                2: logging.INFO,
                3: logging.DEBUG,
            }.get(options["verbosity"])
        )
        legislatura = options["legislatura"]
        start_date = options["start_date"]
        end_date = options["end_date"]
        tipologia = 'votazioni'
        ramo = {'C': 'camera',
                'S': 'senato'}.get(options["ramo"])

        self.logger.info(f'Starting import for {tipologia} {legislatura}')
        legislatura_padded = str(legislatura).zfill(2)
        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=legislatura_padded)

        assemblea = assemblee.get(identifier__icontains=ramo)
        votazioni = Voting.objects.filter(sitting__assembly__identifier__regex=legislatura_padded,
                                          sitting__assembly=assemblea,
                                          sitting__date__gte=start_date,
                                          sitting__date__lte=end_date).order_by('identifier')
        if not options['refresh']:
            votazioni = votazioni.exclude(Q(id__in=MemberVote.objects.values('voting_id').distinct()) &
                                          Q(id__in=GroupVote.objects.values('voting_id').distinct()))

        for votazione in votazioni:
            print(f'{votazione.identifier}')
            self.call_command('import_voti_sparql',
                                    ramo='C' if 'Camera' in votazione.sitting.assembly.name else 'S',
                                    legislatura=legislatura,
                                    identifier=votazione.identifier,
                                    verbosity=3)
            self.call_command('calc_group_vote',
                                    identifier=votazione.identifier,
                                    verbosity=3)
