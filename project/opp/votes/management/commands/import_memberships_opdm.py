from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.models import Membership as MembershipVote
from django.db.models import Q
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor

from popolo.models import Membership as MembershipPopolo
import pandas as pd


class Command(LoggingBaseCommand):
    """Command ETL connect Memberships Popolo and Membership Vote."""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        self.logger.info(f'Starting import for memberships legislatura {legislatura}')
        legislatura_padded = str(legislatura).zfill(2)

        membri_legislatura = MembershipPopolo.objects.filter(Q(role__icontains='deputato') |
                                                             Q(role__icontains='senatore'))\
            .filter(organization__classification='Assemblea parlamentare',
                    organization__identifier__regex=legislatura_padded)

        data = pd.DataFrame(membri_legislatura, columns=['opdm_membership'])
        if data.shape[0] == 0:
            self.logger.warning(f'No data for legislatura {legislatura} memberships')
        else:
            etl = ETL(
                extractor=DataframeExtractor(data),
                loader=DjangoUpdateOrCreateLoader(django_model=MembershipVote,
                                                  fields_to_update=[]))()
            if etl.loader.n_created > 0:
                self.logger.warning(f"Created {etl.loader.n_created} new members. "
                                    f"Check them and assign majority/minority in case")
