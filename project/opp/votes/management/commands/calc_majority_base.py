from taskmanager.management.base import LoggingBaseCommand
from project.opp.votes.models import MembershipGroup, GroupMajority, MemberMajority, Group
from popolo.models import Organization
import datetime
from django.db.models.functions import Coalesce
from django.db.models import Value, F

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):
    """Command ETL connect Memberships Popolo and Membership Vote."""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        legislatura = options["legislatura"]

        governments = Organization.objects.filter(classification='Governo della Repubblica',
                                                  key_events__key_event__identifier__icontains=legislatura).order_by('-start_date')[:1]

        for gov in governments:
            start_date_gv = gov.start_date
            end_date_gv = (gov.end_date or today_strf)

            groups = Group.objects.all_and_metagroups().\
                annotate(new_end_date=Coalesce(F('organization__end_date'), Value(today_strf))).\
                filter(
                organization__start_date__lte=end_date_gv,
                new_end_date__gte=start_date_gv
                ).exclude(acronym__in=['Maggioranza', 'Opposizione'])
            for group in groups:
                end_date_gov = datetime.datetime.strptime((gov.end_date or today_strf), '%Y-%m-%d')
                end_date_gov += datetime.timedelta(days=-1)
                end_date_gov = end_date_gov.strftime('%Y-%m-%d')
                end_date_gr = datetime.datetime.strptime((group.organization.end_date or today_strf), '%Y-%m-%d')
                end_date_gr += datetime.timedelta(days=-1)
                end_date_gr = end_date_gr.strftime('%Y-%m-%d')
                if group.organization.end_date is None and gov.end_date is None:
                    end_date = None
                else:
                    if end_date_gov <= end_date_gr and not group.organization.end_date:
                        end_reason = 'Cambio di governo'
                        end_date = end_date_gov
                    elif end_date_gov >= end_date_gr and not gov.end_date:
                        end_reason = 'Gruppo sciolto'
                        end_date = end_date_gr
                    else:
                        end_date = end_date_gov
                        end_reason = "Cambio di governo - Gruppo sciolto"

                grm, created = GroupMajority.objects.get_or_create(
                    start_date=max(start_date_gv, group.organization.start_date),
                    government=gov,
                    group=group,
                )
                if created:
                    self.logger.warning(f'New Group Majority to evalute https://{grm}')
                if end_date:
                    GroupMajority.objects.filter(
                        government=grm.government,
                        group=grm.group,
                        end_date__isnull=True
                    ).update(end_date=end_date,
                             end_reason=end_reason)

        for gov in governments:
            group_majs = GroupMajority.objects.filter(government=gov)
            for group_maj in group_majs:
                group_end_date = (group_maj.end_date or today_strf)
                mgs = MembershipGroup.objects.\
                    filter(group=group_maj.group)\
                    .annotate(new_end_date=Coalesce('end_date', Value(today_strf)))\
                    .filter(start_date__lte=group_end_date,
                            new_end_date__gte=group_maj.start_date)

                for mg in mgs.filter(start_date__gte=(today - datetime.timedelta(1)).date()):
                    end_reason = None
                    if mg.new_end_date <= group_end_date and mg.end_date:
                        end_reason = mg.end_reason
                        end_date_mm = min(mg.new_end_date, group_end_date)
                    elif group_end_date <= mg.new_end_date and group_maj.end_date:
                        end_reason = group_maj.end_reason
                        end_date_mm = min(mg.new_end_date, group_end_date)
                    elif group_end_date == mg.new_end_date and group_maj.end_date is not None \
                            and mg.end_date is not None:
                        end_reason = f"{mg.end_reason} - {group_maj.end_reason}"
                        end_date_mm = min(mg.new_end_date, group_end_date)
                    else:
                        end_date_mm = None
                    before = MemberMajority.objects.filter(membership=mg.membership, government=gov).exclude(
                        start_date=max(group_maj.start_date, mg.start_date)).order_by('-start_date')
                    if not before or (before and before[0].value == group_maj.value):
                        continue
                    object, created = MemberMajority.objects.get_or_create(
                        membership=mg.membership,
                        government=gov,
                        start_date=max(group_maj.start_date, mg.start_date),
                        defaults={
                            'value': group_maj.value
                        })
                    if end_date_mm and not object.end_date:
                        object.end_date = end_date_mm
                        object.end_reason = end_reason
                        object.save()
                    if created:
                        self.logger.warning(f'Created new member majority https://{object.get_admin_url()} !')
