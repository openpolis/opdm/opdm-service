from config.settings.base import ENDPOINTS_ASSEMBLEE_PARLAMENTARI

from project.tasks.parsing.sparql.utils import get_bindings
from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.management.commands.raw_sparql.template_query import template_query_sparql
from project.opp.votes.management.commands.utils.utils import parse_uri_id, parse_date
from project.opp.votes.models import Sitting

from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor, Extractor

from popolo.models import Organization, Source
import pandas as pd

from django.contrib.contenttypes.models import ContentType

sitting_content = ContentType.objects.get(model='sitting')


class JsonExtractor(Extractor):
    """Json Extractor
    """

    def __init__(self, df):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.df = df

    def extract(self):
        res = []
        for r in self.df:
            res.append(dict((k, r[k]["value"]) for k in r.keys()))
        od = pd.DataFrame(res)
        return od


class SeduteTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, assemblea):
        Transformation.__init__(self)
        self.assemblea = assemblea

    def transform(self):
        od = self.etl.original_data.copy()
        od = od.rename(columns={"seduta": "identifier"}, errors="raise")
        od["identifier"] = od["identifier"].apply(parse_uri_id)
        if self.assemblea.parent.name == 'CAMERA DEI DEPUTATI':
            od["date"] = od["date"].apply(parse_date, pattern='%Y%m%d')
        od['assembly'] = self.assemblea
        self.etl.processed_data = od
        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )
        parser.add_argument(
            '--sd',
            dest="start_date",
            required=True,
            help='Start date collection votes, format YYYY-MM-DD.')

        parser.add_argument(
            '--ed',
            dest="end_date",
            required=True,
            help='End date collection votes, format YYYY-MM-DD.')

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        tipologia = 'sedute'
        legislatura_padded = str(legislatura).zfill(2)
        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=legislatura_padded)

        sd = options['start_date']
        ed = options['end_date']
        self.logger.info(f"Gettin sitting from {sd} to {ed}")

        def get_query(**kwargs):
            query = template_query_sparql(name='sedute', **kwargs)
            return get_bindings(
                    ENDPOINTS_ASSEMBLEE_PARLAMENTARI.get(kwargs.get('aula')),
                    query,
                    legacy=True
            )

        for aula in ENDPOINTS_ASSEMBLEE_PARLAMENTARI.keys():
            self.logger.debug(f'Getting {aula}')
            data = JsonExtractor(get_query(legislatura=legislatura,
                                           tipologia=tipologia,
                                           aula=aula,
                                           sd=sd,
                                           ed=ed)).extract()
            assemblea = assemblee.get(identifier__icontains=aula)

            if data.shape[0] == 0:
                self.logger.warning(f'No data for {aula} legislation {legislatura} for category {tipologia}')
                continue
            ETL(
                extractor=DataframeExtractor(data),
                transformation=SeduteTransformation(assemblea=assemblea),
                loader=DjangoUpdateOrCreateLoader(django_model=Sitting, fields_to_update=['date',
                                                                                          'number']),
            )()
            for index, row in data.iterrows():
                source, created = Source.objects.update_or_create(url=row['seduta'],
                                                                  defaults={'note': f'Open data {aula.title()}'})
                seduta = Sitting.objects.get(number=row['number'], assembly=assemblea)
                seduta.sources.update_or_create(content_type=sitting_content, source=source)
