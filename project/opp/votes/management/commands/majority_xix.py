from taskmanager.management.base import LoggingBaseCommand
from config.settings.base import ROOT_PATH
from project.opp.gov.models import Government
from project.opp.votes.models import MembershipGroup, GroupMajority, MemberMajority, Group, Membership
from popolo.models import Organization
import datetime
import pandas as pd
from django.db.models.functions import Coalesce
from django.db.models import Value, Count
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor


class Command(LoggingBaseCommand):
    """Command ETL connect Memberships Popolo and Membership Vote."""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        legislatura = options["legislatura"]
        path_file = ROOT_PATH / "data" / "openparlamento" / 'group_majority_first_load.csv'
        df_groups = pd.read_csv(path_file, sep=",")
        gr_maj_leg = df_groups[df_groups['leg'] == legislatura]
        gr_maj_leg['group'] = gr_maj_leg.group_ord_id.apply(lambda x: Group.objects.get(organization_id=x))
        gr_maj_leg = gr_maj_leg.rename(
            columns={"sd_maj": "start_date",
                     "ed_maj": "end_date",
                     "end_reason_maj": "end_reason",
                     "gov_id": "government_id"}, errors="raise")
        gr_maj_leg = gr_maj_leg.drop(['leg', 'group_ord_id'], axis=1)
        gr_maj_leg = gr_maj_leg.where(pd.notnull(gr_maj_leg), None)
        ETL(
            extractor=DataframeExtractor(gr_maj_leg),
            loader=DjangoUpdateOrCreateLoader(django_model=GroupMajority,
                                              fields_to_update=['legislature',
                                                                'acronym', ]))()

        governments = Organization.objects.filter(classification='Governo della Repubblica',
                                                  key_events__key_event__identifier__icontains=legislatura)

        for gov in governments:
            start_date_gv = gov.start_date
            end_date_gv = gov.end_date

            groups = Group.objects.all_and_metagroups().filter(
                acronym='Nessun gruppo',
                organization__identifier__regex=f"{legislatura}")
            for group in groups:
                GroupMajority.objects.update_or_create(
                    start_date=start_date_gv,
                    government=gov,
                    group=group,
                    defaults={
                        'value': None,
                        'end_date': end_date_gv
                    }
                )
        #     for group in groups:
        #         print(gov)
        #         print(group.organization.name)
        #         value = None
        #         end_date_gov = datetime.datetime.strptime((gov.end_date or today_strf), '%Y-%m-%d')
        #         end_date_gov += datetime.timedelta(days=-1)
        #         end_date_gov = end_date_gov.strftime('%Y-%m-%d')
        #         end_date_gr = datetime.datetime.strptime((group.organization.end_date or today_strf), '%Y-%m-%d')
        #         end_date_gr += datetime.timedelta(days=-1)
        #         end_date_gr = end_date_gr.strftime('%Y-%m-%d')
        #         if group.organization.end_date is None and gov.end_date is None:
        #             end_date_gm = None
        #         else:
        #             end_date_gm = min(end_date_gov, end_date_gr)
        #         breakpoint()
        #         GroupMajority.objects.update_or_create(
        #             start_date=max(start_date_gv, group.organization.start_date),
        #             government=gov,
        #             group=group,
        #             defaults={
        #                 'value': value,
        #                 'end_date': end_date_gm
        #             }
        #         )
        # print('finished')
        # breakpoint()
        for gov in governments:

            today = datetime.datetime.now()
            today_strf = today.strftime('%Y-%m-%d')

            # breakpoint()
            group_majs = GroupMajority.objects.filter(government=gov)
            for group_maj in group_majs:
                group_end_date = (group_maj.end_date or today_strf)
                mgs = MembershipGroup.objects.filter(group=group_maj.group)\
                    .annotate(new_end_date=Coalesce('end_date', Value(today_strf)))\
                    .filter(start_date__lte=group_end_date,
                            new_end_date__gte=group_maj.start_date)

                for mg in mgs:
                    # if mg.membership.id==353:
                    #     breakpoint()
                    end_reason = None
                    if mg.new_end_date < group_end_date:
                        end_reason = mg.end_reason
                        end_date_mm = min(mg.new_end_date, group_end_date)
                    elif group_end_date < mg.new_end_date:
                        end_reason = group_maj.end_reason
                        end_date_mm = min(mg.new_end_date, group_end_date)
                    elif group_end_date == mg.new_end_date and group_maj.end_date is not None \
                            and mg.end_date is not None:
                        end_reason = f"{mg.end_reason} - {group_maj.end_reason}"
                        end_date_mm = min(mg.new_end_date, group_end_date)
                    else:
                        end_date_mm = None

                    MemberMajority.objects.update_or_create(
                        membership=mg.membership,
                        government=gov,
                        start_date=max(group_maj.start_date, mg.start_date),
                        defaults={
                            'end_date': end_date_mm,
                            'end_reason': end_reason,
                            'value': group_maj.value
                        }

                    )

        path_file = ROOT_PATH / "data" / "openparlamento" / 'member_majority_first_load.csv'
        df_members = pd.read_csv(path_file, sep=",")
        member_maj_leg = df_members[df_members['leg'] == legislatura]
        member_maj_leg['membership'] = member_maj_leg.opdm_membership_id\
            .apply(lambda x: Membership.objects.get(opdm_membership_id=x))
        member_maj_leg = member_maj_leg.rename(columns={
            "sd_maj": "start_date",
            "ed_maj": "end_date",
            "end_reason_maj": "end_reason",
            "gov_id": "government_id"
                                }, errors="raise")
        member_maj_leg = member_maj_leg.drop(['leg',
                                              'opdm_membership_id',
                                              'end_date',
                                              'end_reason'], axis=1)
        member_maj_leg = member_maj_leg.where(pd.notnull(member_maj_leg), None)
        ETL(
            extractor=DataframeExtractor(member_maj_leg),
            loader=DjangoUpdateOrCreateLoader(django_model=MemberMajority,
                                              fields_to_update=['value', ]))()

        # flatten membermajority
        current_gov = Government.objects.order_by('organization__start_date').last()
        member_maj = MemberMajority.objects.filter(government=current_gov.organization.id)
        to_flatt = member_maj.values('membership').annotate(cnt=Count('*')).filter(cnt__gt=1).values_list(
            'membership', flat=True)
        for memb in to_flatt:
            case_more = member_maj.filter(membership=memb).order_by('start_date')
            for i, row in enumerate(case_more):
                if i == 0:
                    prev = row
                    continue
                if prev.value == row.value:
                    prev.end_date = row.end_date
                    prev.save()
                    row.delete()



