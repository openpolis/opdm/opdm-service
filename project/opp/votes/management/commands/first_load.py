from django.core import management
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""
    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )
        parser.add_argument(
            '--sd',
            dest="start_date",
            help='Start date')

        parser.add_argument(
            '--ed',
            dest="end_date",
            help='End date')

    def handle(self, *args, **options):
        super().handle(*args, **options)

        legislatura = options['legislatura']
        sd = options['start_date']
        ed = options['end_date']
        self.logger.info("Legislatura XVIII")
        self.logger.info("Load sedute")
        management.call_command('import_sedute_sparql',
                                legislatura=legislatura,
                                verbosity=3)
        self.logger.info("Load memberships")
        management.call_command('import_memberships_opdm',
                                legislatura=legislatura,
                                verbosity=3)
        self.logger.info("Load gruppi")
        management.call_command('import_groups_opdm_first_load',
                                legislatura=legislatura,
                                verbosity=3)
        self.logger.info("Calc gruppi memberships")
        management.call_command('calc_membership_group',
                                legislatura=legislatura,
                                verbosity=3)
        self.logger.info("Load votazioni")
        management.call_command('import_votazioni_sparql',
                                legislatura=legislatura,
                                verbosity=3)
        management.call_command('majority_xix',
                                legislatura=legislatura,
                                verbosity=3)
        self.logger.info("Load voti Camera")
        management.call_command('pipeline_votazioni',
                                legislatura=legislatura,
                                ramo='C',
                                sd=sd,
                                ed=ed,
                                verbosity=3)
        self.logger.info("Load voti Senato")
        management.call_command('pipeline_votazioni',
                                legislatura=legislatura,
                                ramo='S',
                                sd=sd,
                                ed=ed,
                                verbosity=3)
        self.logger.info("Calc group cohesion")
        management.call_command('calc_group_majority_cohesion',
                                legislatura=legislatura,
                                verbosity=3)
        self.logger.info("Calc historical groups")
        management.call_command('calc_historical_metrics_groups',
                                legislatura=legislatura,
                                verbosity=3)
        self.logger.info("Calc historical memberships")
        management.call_command('calc_historical_metrics_memberships',
                                legislatura=legislatura,
                                verbosity=3)
