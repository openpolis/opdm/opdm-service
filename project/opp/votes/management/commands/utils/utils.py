import datetime
import urllib.parse as urlparse
import logging
from collections import OrderedDict


logger = logging.getLogger('opp_tools')


def parse_date(string_date, pattern='%Y%-m-%d'):
    return datetime.datetime.strptime(string_date, pattern).date()


def parse_uri_id(uri, ass=None):
    """
    Estrae il l'ultimo segmento del path di un uri.

    Per ora esegue le seguenti operazioni:
    1. prendo il path dell'uri
    2. rimuovo primo e ultimo `/`
    3. splitto la stringa per `/`
    4. ultimo segmento

    esempi:
        http://dati.senato.it/senatore/30509 ritorna 30509
        http://dati.camera.it/ocd/persona.rdf/p15080 ritorna p15080

    :param uri:
    :return: uri_id
    """
    res = urlparse.urlparse(uri).path.strip('/').split('/')[-1]
    if ass == 'ASS-CAMERA-19':
        res_by__ = res.split("_")
        first = res_by__[0]
        second = int(res_by__[1])
        return f"{first}_{second}"
    else:
        return res


def regroup_by(dict_list, field):

    res = OrderedDict()
    for c in dict_list:
        if c[field] in res:
            res[c[field]].append(c)
        else:
            res[c[field]] = [c]
    return res
