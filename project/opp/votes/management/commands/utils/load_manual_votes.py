from operator import itemgetter
from django.core import management
import re
import pandas as pd
from project.opp.votes.models import Sitting, MemberVote, Membership
from difflib import SequenceMatcher
from thefuzz import fuzz
from thefuzz import process

from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor


def similar(a, b):
    return SequenceMatcher(None, a.lower(), b.lower()).ratio()


def get_membership(name, df_memberships):
    res = process.extractOne(name, df_memberships['name'],
                             scorer=fuzz.token_set_ratio)
    return res


class LoadManualVotes:

    def __init__(self, df, voting):
        self.types = ['ayes', 'nos', 'abstained', 'in_mission']
        self.sitting = Sitting.objects.get(id=df['sitting'])
        self.president = df['president']
        self.df = pd.DataFrame(
            itemgetter(*self.types)(df),
            index=self.types
            ).transpose()
        self.voting = voting
        self.map_vote = {
            'ayes': MemberVote.AYE,
            'nos': MemberVote.NO,
            'abstained': MemberVote.ABSTAINED,
            'in_mission': MemberVote.MISSION
        }
        memberships = Membership.get_membership_date_assembly(self.sitting.assembly, self.sitting.date)
        self.df_memberships = pd.DataFrame(memberships.values('id',
                                                              'opdm_membership__person__name',
                                                              'opdm_membership__person__other_names__name').distinct())
        self.df_memberships.columns = ['id', 'name', 'other_names']
        self.df_memberships['other_names'] = self.df_memberships['other_names'].apply(lambda x: x.lower() if x else '')
        self.df_memberships = self.df_memberships.drop_duplicates()
        self.df_memberships = self.df_memberships.groupby(['id', 'name'])['other_names']\
            .apply(lambda x: ' '.join(x)).reset_index()
        self.df_memberships['name'] = self.df_memberships['name'] + ' ' + self.df_memberships['other_names']
        self.df_memberships['mapped'] = False
        self.df_memberships['source_name'] = None
        self.df_memberships['kpi_check'] = None

    def load(self):

        pres = self.df_memberships[self.df_memberships['id'] == int(self.president)].iloc[0]
        MemberVote.objects.get_or_create(membership=Membership.objects.get(id=pres.id),
                                         voting=self.voting,
                                         vote=MemberVote.PRESIDENT)
        self.df_memberships.loc[pres.name, 'mapped'] = True
        for typology in self.types:
            res = self.df[typology].loc[0].split('\n')
            res = [re.sub(r'[^A-Za-z0-9_À-ÿ ]', '', x) for x in res]
            res = [x for x in res if x]
            if not res:
                continue
            best_maches = list(map(lambda x: get_membership(x, self.df_memberships), res))
            ids_df = [x[2] for x in best_maches]
            kpi_check = [x[1] for x in best_maches]
            unique_values = self.df_memberships.loc[ids_df, 'mapped'].unique()
            if not (len(unique_values) == 1 and unique_values[0] == False): # noqa
                case = self.df_memberships.loc[ids_df][self.df_memberships.loc[ids_df]['mapped'] == True]  # noqa
                membership = Membership.objects.get(id=case.iloc[0]['id'])
                vote = MemberVote.objects.get(membership=membership, voting=self.voting).vote
                raise Exception(f'Tried to insert {case.iloc[0]["name"]}'
                                f' with vote {self.map_vote.get(typology)},'
                                f' but already exists as {vote} ')
            self.df_memberships.loc[ids_df, 'mapped'] = True
            self.df_memberships.loc[ids_df, 'source_name'] = res
            self.df_memberships.loc[ids_df, 'kpi_check'] = kpi_check
            df_votes = list(map(lambda x: Membership.objects.get(id=self.df_memberships.loc[x]['id']), ids_df))
            df_all = pd.DataFrame(columns=['membership', 'voting', 'vote'])
            df_all['membership'] = df_votes
            df_all['voting'] = self.voting

            df_all['vote'] = self.map_vote.get(typology)
            ETL(
                extractor=DataframeExtractor(df_all),
                loader=DjangoUpdateOrCreateLoader(django_model=MemberVote, fields_to_update=[
                    'vote']),
            )()

        absent = self.df_memberships[self.df_memberships['mapped'] == False] # noqa
        absent_id = absent['id'].to_list()
        df_votes = list(map(lambda x: Membership.objects.get(id=x), absent_id))
        df_all = pd.DataFrame(columns=['membership', 'voting', 'vote'])
        df_all['membership'] = df_votes
        df_all['voting'] = self.voting

        df_all['vote'] = MemberVote.ABSENT
        ETL(
            extractor=DataframeExtractor(df_all),
            loader=DjangoUpdateOrCreateLoader(django_model=MemberVote, fields_to_update=[
                'vote']),
        )()
        votazione = self.voting
        votazione.n_absent = MemberVote.objects.filter(voting=votazione, vote=MemberVote.ABSENT).count()
        votazione.n_mission = MemberVote.objects.filter(voting=votazione, vote=MemberVote.MISSION).count()
        votazione.n_present_not_voting = MemberVote.objects.filter(voting=votazione,
                                                                   vote=MemberVote.PRESENT_NOT_VOTING).count()
        votazione.n_requiring_not_voting = MemberVote.objects.filter(voting=votazione,
                                                                     vote=MemberVote.REQUIRING_NOT_VOTING).count()
        president = MemberVote.objects.filter(voting=votazione,
                                              vote=MemberVote.PRESIDENT)
        votazione.n_margin = abs(votazione.n_ayes - votazione.n_majority)
        if president:
            votazione.president = president[0].membership

        votazione.save()
        management.call_command('calc_group_vote',
                                identifier=votazione.identifier,
                                verbosity=3)
        management.call_command('calc_group_majority_cohesion',
                                legislatura=self.sitting.get_legislature,
                                verbosity=3)
        return self.df_memberships.sort_values(by='kpi_check')
