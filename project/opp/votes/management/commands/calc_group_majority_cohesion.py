import pandas as pd
from django.db.models import Avg, Count, F, Q, Value, Sum, IntegerField, CharField
from django.db.models.functions import Coalesce, Cast
from project.opp.votes.models import Group, GroupMajority, GroupVote, Membership, MemberMajority, MemberVote, \
    MembershipGroup
from taskmanager.management.base import LoggingBaseCommand
import datetime
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):
    """"""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        legislatura_padded = str(legislatura).zfill(2)

        gruppi = Group.objects.filter(organization__parent__identifier__endswith=legislatura_padded)
        # Calcolo a livello di singolo gruppo la media del cohesion rate e se supporta attualmente la maggioranza
        for gruppo in gruppi:
            majority = GroupMajority.objects.filter(group=gruppo)\
                .filter(Q(end_date__isnull=True) | Q(end_reason='Fine legislatura'))
            if majority.count() == 1:
                gruppo.supports_majority = majority.first().value
            elif majority.count() > 1:
                self.logger.error(f'{gruppo} has multiple majorities')
                raise Exception
            else:
                gruppo.supports_majority = None
            gvotes = GroupVote.objects.filter(group=gruppo).exclude(cohesion_rate__isnull=True)
            cr_gruppo = gvotes.aggregate(Avg('cohesion_rate'))['cohesion_rate__avg']
            if cr_gruppo:
                cr_gruppo = round(cr_gruppo, 2)
            gruppo.cohesion_rate = cr_gruppo
            gruppo.save()

        meta_groups = Group.objects.all_and_metagroups()\
            .filter(is_meta_group=True,
                    legislature__identifier__endswith=legislatura_padded).distinct()
        for gruppo in meta_groups:
            gvotes = GroupVote.objects.filter(group=gruppo).exclude(cohesion_rate__isnull=True)
            if not gruppo.organization:
                gvotes = GroupVote.objects.filter(group__in=meta_groups.
                                                  filter(supports_majority=gruppo.supports_majority))\
                    .exclude(cohesion_rate__isnull=True)
            cr_gruppo = gvotes.aggregate(Avg('cohesion_rate'))['cohesion_rate__avg']
            if cr_gruppo:
                cr_gruppo = round(cr_gruppo, 2)
            gruppo.cohesion_rate = cr_gruppo
            gruppo.save()

        membri = Membership.objects.filter(opdm_membership__organization__identifier__endswith=legislatura_padded)
        # Calcolo a livello di singolo gruppo la somma dei voti ribelli
        # e dei voti fidelity del cohesion rate e se supporta attualmente la maggioranza
        for membro in membri:
            majority = MemberMajority.objects.filter(membership=membro)\
                .filter(Q(end_date__isnull=True) | Q(end_reason='Fine legislatura'))
            if majority.count() == 1:
                membro.supports_majority = majority.first().value
            elif majority.count() > 1:
                self.logger.error(f'{membro} has multiple majorities')
                print(membro)
                raise Exception
            else:
                membro.supports_majority = None
            n_rebels = MemberVote.objects.filter(membership=membro, is_rebel=True).count()
            n_fidelity = MemberVote.objects.filter(membership=membro, is_fidelity=True).count()

            membro.n_rebels = n_rebels
            membro.n_fidelity = n_fidelity
            membro.save()

        agg_votes = MembershipGroup.objects\
            .values('id',
                    'membership',
                    'group',
                    'end_date',
                    'membership__votes__is_rebel',
                    'membership__votes__is_fidelity',
                    'start_date',
                    'membership__votes__voting__sitting__date',
                    'membership__votes__vote')\
            .annotate(is_rebel_int=Cast('membership__votes__is_rebel', IntegerField()),
                      is_fidelity_int=Cast('membership__votes__is_fidelity', IntegerField()),
                      new_end_date=Coalesce(F('end_date'), Value(today_strf)),
                      sitting_date=Cast(F('membership__votes__voting__sitting__date'), CharField()),
                      memb_vote=F('membership__votes__vote'))\
            .filter(start_date__lte=F('sitting_date'),
                    new_end_date__gte=F('sitting_date'))\
            .values('id', 'memb_vote')\
            .annotate(cnt=Count('*'),
                      cnt_rebel=Sum('is_rebel_int'),
                      cnt_fidelity=Sum('is_fidelity_int')).order_by(F('cnt'))
        df_votes = pd.DataFrame(agg_votes)
        df_votes_pivot = pd.pivot_table(df_votes, index=["id"], columns=["memb_vote"], values=['cnt'])
        df_votes_pivot_rebels = pd.pivot_table(df_votes, index=["id"], values=['cnt_rebel'], aggfunc= 'sum')
        df_votes_pivot_fidelity = pd.pivot_table(df_votes, index=["id"], values=['cnt_fidelity'], aggfunc= 'sum')
        df_votes_pivot.columns = df_votes_pivot.columns.get_level_values(1)
        df_votes_pivot = pd.concat([df_votes_pivot, df_votes_pivot_rebels], axis=1)
        df_votes_pivot = pd.concat([df_votes_pivot, df_votes_pivot_fidelity], axis=1)
        df_votes_pivot.reset_index(inplace=True)
        df_votes_pivot = df_votes_pivot.reindex(
            df_votes_pivot.columns.union(
                ['ABSE',
                 'ABST',
                 'AYE',
                 'MIS',
                 'NO',
                 'PRES',
                 'RNV',
                 'PNV',
                 'SEC'], sort=False), axis=1, fill_value=0)
        df_votes_pivot = df_votes_pivot.rename(columns={
            'ABSE': 'n_absent',
            'ABST': 'n_abstained',
            'AYE': 'n_ayes',
            'MIS': 'n_mission',
            'NO': 'n_nos',
            'PRES': 'n_president',
            'RNV': 'n_requiring_not_voting',
            'PNV': 'n_present_not_voting',
            'cnt_rebel': 'n_rebels',
            'cnt_fidelity': 'n_fidelity',
            'SEC': 'SEC'}, errors="continue")
        df_votes_pivot = df_votes_pivot.fillna(0)
        df_votes_pivot['n_present'] = df_votes_pivot.apply(lambda x: x['n_abstained']
                                                           + x['n_ayes']
                                                           + x['n_nos']
                                                           + x['n_president']
                                                           + x['n_requiring_not_voting']
                                                           + x['n_present_not_voting']
                                                           + x['SEC'], axis=1)
        df_votes_pivot['n_not_voting'] = df_votes_pivot.apply(
            lambda x: x['n_absent'] + x['n_requiring_not_voting'] + x['n_present_not_voting'], axis=1)
        df_votes_pivot['n_voting'] = df_votes_pivot.apply(lambda x: x['n_ayes'] + x['n_nos'] + x['SEC'], axis=1)

        df_votes_pivot = df_votes_pivot.drop(['SEC'], axis=1)
        ETL(
                extractor=DataframeExtractor(df_votes_pivot),
                loader=DjangoUpdateOrCreateLoader(django_model=MembershipGroup,
                                                  fields_to_update=['n_rebels',
                                                                    'n_voting',
                                                                    'n_abstained',
                                                                    'n_absent',
                                                                    'n_mission',
                                                                    'n_ayes',
                                                                    'n_nos',
                                                                    'n_present',
                                                                    'n_rebels',
                                                                    'n_fidelity',
                                                                    'n_present_not_voting',
                                                                    'n_requiring_not_voting',
                                                                    'n_not_voting',
                                                                    'n_president']))()
