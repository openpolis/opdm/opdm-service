from taskmanager.management.base import LoggingBaseCommand

from project.opp.votes.models import Membership as MembershipVote, MembershipGroup, Group
from popolo.models import Organization
import datetime
from django.db.models.functions import Coalesce
from django.db.models import F, Value, Q



class Command(LoggingBaseCommand):
    """Command ETL connect Memberships Popolo and Membership Vote."""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        self.logger.info(f'Starting import for memberships legislatura {legislatura}')
        legislatura_padded = str(legislatura).zfill(2)
        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=legislatura_padded)
        today = datetime.datetime.now()

        for membro in MembershipVote.objects.filter(opdm_membership__organization__in=assemblee):
            # self.logger.debug(membro)
            assemblea = membro.opdm_membership.organization
            today_str = today.strftime('%Y-%m-%d')
            sd = membro.opdm_membership.start_date
            ed = (membro.opdm_membership.end_date or today_str)
            groups = membro.opdm_membership.person.memberships.filter(
                organization__classification='Gruppo politico in assemblea elettiva',
                organization__parent=assemblea).annotate(new_end_date=Coalesce(F('end_date'),
                                                                               Value(today_str)))\
                .filter(start_date__lt=ed, new_end_date__gt=sd).order_by('start_date')
            no_group = Group.objects.all_and_metagroups().get(organization=assemblea, acronym='Nessun gruppo')
            list_groups = [{'group_id': no_group.id,
                            'start_date': sd,
                            'end_date': None}]
            for idx, group in enumerate(groups):
                old_data = list_groups[-1]
                if old_data['group_id'] == group.organization.id:
                    old_data['start_date'] = min(
                        old_data['start_date'],
                        group.start_date
                    )
                    if old_data['end_date'] is None or group.end_date is None:
                        old_data['end_date'] = None
                    else:
                        old_data['end_date'] = max(old_data['end_date'], group.end_date)

                else:
                    dict_group = {}
                    dict_group['group_id'] = group.organization.id
                    start_date = group.start_date
                    if start_date == old_data['end_date']:
                        start_date = datetime.datetime.strptime(group.start_date, '%Y-%m-%d')
                        start_date += datetime.timedelta(days=1)
                        start_date = start_date.strftime('%Y-%m-%d')
                    dict_group['start_date'] = start_date
                    dict_group['end_date'] = group.end_date
                    if idx == 0 and start_date == old_data['start_date']:
                        list_groups.pop(idx)
                    elif idx == 0 and start_date != old_data['start_date']:
                        end_date_prev = datetime.datetime.strptime(start_date, '%Y-%m-%d')
                        end_date_prev -= datetime.timedelta(days=1)
                        end_date_prev = end_date_prev.strftime('%Y-%m-%d')
                        list_groups[idx]['end_date'] = end_date_prev
                    if idx > 0:
                        next_end_date = datetime.datetime.strptime(start_date, '%Y-%m-%d')
                        next_end_date -= datetime.timedelta(days=1)
                        next_end_date = next_end_date.strftime('%Y-%m-%d')

                        next_start_date = datetime.datetime.strptime(list_groups[-1]['end_date'], '%Y-%m-%d')
                        next_start_date += datetime.timedelta(days=1)
                        next_start_date = next_start_date.strftime('%Y-%m-%d')

                        if list_groups[-1]['end_date'] != next_end_date:
                            list_groups.append(
                                    {'group_id': no_group.id,
                                     'start_date': next_start_date,
                                     'end_date': next_end_date})
                    list_groups.append(
                        dict_group
                    )
            for idx, item in enumerate(list_groups):
                gr = Group.objects.all_and_metagroups().get(
                    Q(organization_id=item.get('group_id')) |
                    Q(id=item.get('group_id'), acronym='Nessun gruppo'))
                end_date = item.get('end_date')
                end_date_parliament = membro.opdm_membership.end_date
                end_reason = None
                if end_date:
                    if end_date == end_date_parliament:
                        end_reason = membro.opdm_membership.end_reason
                        end_date = datetime.datetime.strptime(end_date_parliament, '%Y-%m-%d')
                        end_date += datetime.timedelta(days=-1)
                        end_date = end_date.strftime('%Y-%m-%d')
                    else:
                        if gr.acronym == 'Nessun gruppo':
                            end_reason = 'Iscrizione gruppo'
                        else:
                            end_reason = 'Cambio di gruppo'
                MembershipGroup.objects.update_or_create(
                    membership=membro,
                    group=gr,
                    start_date=item.get('start_date'),
                    defaults={
                        'end_date': end_date,
                        'end_reason': end_reason
                    }
                )
