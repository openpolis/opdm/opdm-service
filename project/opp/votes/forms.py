from project.opp.votes.models import Voting
from django import forms
from django.utils.translation import gettext_lazy as _


class CustomWidget(forms.Textarea):
    pass


class VotingForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        # first call parent's constructor
        super(VotingForm, self).__init__(*args, **kwargs)
        # there's a `fields` property now
        self.fields['president'].required = True

    ayes = forms.Field(
        widget=CustomWidget,
        label='Hanno detto sì',
        help_text=_('Hanno detto sì'),
        required=False
    )

    nos = forms.Field(
        widget=CustomWidget,
        label=_('Hanno detto no'),
        help_text=_('Hanno detto no'),
        required=False
    )

    abstained = forms.Field(
        widget=CustomWidget,
        label='Astenuti',
        help_text=_('Astenuti'),
        required=False
    )

    in_mission = forms.Field(
        widget=CustomWidget,
        label=_('In missione'),
        help_text=_('In missione'),
        required=False,

    )

    class Meta:
        model = Voting
        required_fields = ['president', ]
        fields = ('original_title',
                  'number',
                  'sitting',
                  'president',
                  'description_title',
                  'public_title',
                  'n_present',
                  'n_voting',
                  'n_majority',
                  'n_absent',
                  'n_mission',
                  'n_abstained',
                  'n_ayes',
                  'n_nos',
                  'n_present_not_voting',
                  'n_requiring_not_voting',
                  'n_not_voting',
                  'is_final',
                  'is_confidence',
                  'outcome',
                  'is_secret',
                  'is_key_vote')
