import factory

from popolo.tests.factories import OrganizationFactory
from project.opp.votes.tests import faker


class SittingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "votes.Sitting"

    number = factory.Faker("pyint", min_value=1, max_value=20)
    identifier = factory.Faker("url")

    @factory.lazy_attribute
    def assembly(self):
        return OrganizationFactory.create()

    @factory.lazy_attribute
    def date(self):
        return faker.date_between(start_date="-1y", end_date="-2m").strftime("%Y-%m-%d")


class GroupFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "votes.Group"

    acronym = factory.Faker("pystr", max_chars=32)
    cohesion_rate = factory.Faker("pyfloat", min_value=0., max_value=1.)
    supports_majority = factory.Faker("pybool")

    @factory.lazy_attribute
    def organization(self):
        return OrganizationFactory.create()
