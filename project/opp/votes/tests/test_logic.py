from .factories import SittingFactory, GroupFactory
from django.test import TestCase
from popolo.tests.factories import OrganizationFactory


class SittingTestCase(TestCase):

    def test_branch_returns_correct_value(self):
        """Test that the branch function reports correctly the branch of a sitting

        post is sent as simple ID.

        :return:
        """

        sitting = SittingFactory.create(
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        self.assertEquals(sitting.branch, 'C')

        sitting = SittingFactory.create(
            assembly=OrganizationFactory.create(identifier='ASS-SENATO-19')
        )
        self.assertEquals(sitting.branch, 'S')

    def test_wrong_identifier_raise_exception(self):
        sitting = SittingFactory.create(
            assembly=OrganizationFactory.create(identifier='ASS-REGIONALE')
        )
        with self.assertRaises(Exception):
            _ = sitting.branch

    def test_string_representation(self):
        s = SittingFactory.create(
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        self.assertEquals(s.__str__(), f"{s.branch}.{s.number} @ {s.date}")


class GroupTestCase(TestCase):

    def test_group_name_correctly_assigned(self):
        group = GroupFactory.create()
        self.assertEquals(group.name, group.organization.name)
