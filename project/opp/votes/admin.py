"""Define elections admins."""
from django.contrib import admin, messages
from django import forms
from django.db.models import F, Q
from django.utils.translation import gettext_lazy as _
from project.opp.votes.models import (GroupMajority,
                                      Membership,
                                      MemberMajority,
                                      MembershipGroup,
                                      Group,
                                      GroupsMetricsHST,
                                      GroupVote,
                                      Voting,
                                      Sitting,
                                      MembershipsMetricsHST,
                                      MemberVote)
from project.opp.votes.forms import VotingForm
from popolo.models import KeyEvent
from django.utils.safestring import mark_safe
from django.contrib.admin import SimpleListFilter
from .jobs import generate_voting, send_notification_slack


def create_voting(df, voting):
    statistics_vote = generate_voting.delay(df, voting)
    send_notification_slack.delay(depends_on=statistics_vote)


def custom_titled_filter(title):
    class Wrapper(admin.FieldListFilter):
        def __new__(cls, *args, **kwargs):
            instance = admin.FieldListFilter.create(*args, **kwargs)
            instance.title = title
            return instance
    return Wrapper


class GovernmentFilter(SimpleListFilter):
    title = _('Government filter')

    parameter_name = 'Governo'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return [(i, i) for i in qs.values_list('government__name', flat=True)
                                  .distinct().order_by('-government__start_date')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(government__name=self.value())


class GroupFilter(SimpleListFilter):
    title = _('Filtro gruppi')

    parameter_name = 'Gruppo'

    def lookups(self, request, model_admin):
        qs = model_admin.get_queryset(request)
        return qs.values_list('group__acronym', 'group__acronym').distinct().order_by('-group__acronym')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(group__acronym=self.value())


class VotingLegislatureFilter(SimpleListFilter):
    title = _('Legislature filter')

    parameter_name = 'Votazioni'

    def lookups(self, request, model_admin):
        # qs = model_admin.get_queryset(request)
        return KeyEvent.objects.filter(event_type='ITL', start_date__gt='2018-01-01')\
            .values_list('identifier', 'identifier')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(sitting__assembly__key_events__key_event__identifier=self.value())


@admin.register(GroupMajority)
class GroupMajorityAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "government":
            fk = super(GroupMajorityAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
            kwargs["queryset"] = fk.queryset.order_by('-start_date')
            return super(GroupMajorityAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
        elif db_field.name == "group":
            kwargs["queryset"] = Group.objects.all_and_metagroups()
        return super(GroupMajorityAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    list_display = ('government', 'group', 'value', 'start_date', 'end_date', 'end_reason')

    list_filter = (GovernmentFilter,
                   ('group__organization__parent__parent', admin.RelatedOnlyFieldListFilter))


@admin.register(MemberMajority)
class MemberMajorityAdmin(admin.ModelAdmin):

    def get_person(self, obj):
        return obj.membership.opdm_membership.person

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "government":
            fk = super(MemberMajorityAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
            kwargs["queryset"] = fk.queryset.order_by('-start_date')
            return super(MemberMajorityAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)
        return super(MemberMajorityAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    list_display = ('government', 'get_person', 'value', 'start_date', 'end_date', 'end_reason')

    search_fields = ['membership__opdm_membership__person__name', ]
    list_filter = (GovernmentFilter,
                   ('membership__opdm_membership__organization__parent', admin.RelatedOnlyFieldListFilter),
                   'value')
    ordering = ('membership', 'start_date')
    raw_id_fields = ("membership",)
    # list_display = ("name",)
    # ordering = ("name", "pk")


@admin.register(MembershipGroup)
class MembershipGroupAdmin(admin.ModelAdmin):
    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "group":
            kwargs["queryset"] = Group.objects.all_and_metagroups()
        return super(MembershipGroupAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def get_person(self, obj):
        return obj.membership.opdm_membership.person

    search_fields = ['membership__opdm_membership__person__name', ]
    list_display = ('get_person', 'group', 'start_date', 'end_date', 'end_reason')
    list_filter = (GroupFilter,
                   ('group__organization__parent__parent', admin.RelatedOnlyFieldListFilter),
                   ('group__legislature', admin.RelatedOnlyFieldListFilter))
    raw_id_fields = ('membership', )
    ordering = ('start_date', )


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):

    def get_queryset(self, request):
        qs = self.model.objects.all_and_metagroups()

        ordering = self.ordering or ()  # otherwise we might try to *None, which is bad ;)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs
    # readonly_fields = ('organization', )

    def nome_gruppo(self, obj):
        return obj.__str__()

    list_display = ('nome_gruppo', 'acronym', 'supports_majority', 'legislature')
    search_fields = ("organization__name",)
    raw_id_fields = ("organization",)
    list_filter = (('legislature', admin.RelatedOnlyFieldListFilter),)


class MembershipAdminForm(forms.ModelForm):
    class Meta:
        model = Membership
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        instance = kwargs.get('instance')
        if instance.opdm_membership.start_date > instance.opdm_membership.organization.start_date:
            self.fields['replaced_member'].queryset = self.fields['replaced_member']\
                .queryset\
                .filter(
                opdm_membership__label=instance.opdm_membership.label).\
                filter(
                Q(replaced_membership_parliament__isnull=True) | Q(id=instance.replaced_member_id)).exclude(
                opdm_membership__end_date=F('opdm_membership__organization__end_date')
            )


@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    search_fields = ['opdm_membership__person__name', ]
    form = MembershipAdminForm

    def nome_membro(self, obj):
        return obj.opdm_membership.person.name

    def legislatura(self, obj):
        return obj.opdm_membership.organization.key_events.get(key_event__identifier__startswith='ITL_').key_event.\
            identifier

    def start_date(self, obj):
        return obj.opdm_membership.start_date

    start_date.admin_order_field = 'opdm_membership__start_date'
    ordering = ('-opdm_membership__start_date',)
    list_display = ('nome_membro',
                    'start_date',
                    'legislatura', 'supports_majority', 'n_rebels', 'n_fidelity',)
    raw_id_fields = ('opdm_membership', 'election_area',)

    # def get_fields(self, request, obj=None):
    #     fields = super(MembershipAdmin, self).get_fields(request, obj)
    #     if not obj.opdm_membership.start_date > obj.opdm_membership.organization.start_date:
    #         fields.remove('replaced_member')
    #     return fields

    def get_readonly_fields(self, request, obj=None):
        fields = [f.name for f in self.model._meta.fields]
        if obj.opdm_membership.start_date > obj.opdm_membership.organization.start_date:
            fields.remove('replaced_member')
        return fields


@admin.register(GroupsMetricsHST)
class GroupsMetricsHSTAdmin(admin.ModelAdmin):

    def nome_gruppo(self, obj):
        return obj.group.__str__()

    def legislatura(self, obj):
        return obj.group.legislature
    list_display = ('nome_gruppo', 'year', 'month', 'cohesion_rate',
                    'legislatura')
    search_fields = ("group__organization__name",)
    list_filter = (('group__legislature', admin.RelatedOnlyFieldListFilter),
                   'year',
                   'month')


@admin.register(GroupVote)
class GroupVoteAdmin(admin.ModelAdmin):

    def has_change_permission(self, request, obj=None):
        return False

    def nome_gruppo(self, obj):
        return obj.group.__str__()

    def legislatura(self, obj):
        return obj.group.legislature

    list_display = ('voting', 'nome_gruppo', 'cohesion_rate',
                    'vote',
                    'legislatura')
    search_fields = ("group__organization__name", 'voting__identifier')
    list_filter = (('group__legislature', admin.RelatedOnlyFieldListFilter),)
    readonly_fields = ('voting', 'group')


class BillInline(admin.TabularInline):

    def has_change_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return True

    fields = ('bill', )
    raw_id_fields = ("bill",)
    model = Voting.bills.through
    extra = 1


@admin.register(Voting)
class VotingAdmin(admin.ModelAdmin):
    date_hierarchy = 'sitting__date'

    def get_form(self, request, obj=None, change=False, **kwargs):
        if 'add' in request.path:
            self.form = VotingForm
            return super(VotingAdmin, self).get_form(request, obj, change, **kwargs)
        else:
            self.form = forms.ModelForm
            return super(VotingAdmin, self).get_form(request, obj, change, **kwargs)

    def save_model(self, request, obj, form, change):
        obj.save()
        if not change:  # newly created object
            create_voting(df=form.data, voting=obj)
            self.message_user(
                request,
                mark_safe("Data loading and updating (usually 30 secs), check "
                          "<a target='_blank' href='https://openpolis.slack.com/archives/GMRM4ELP9'>slack channel</a>"
                          " to see report"),
                level=messages.INFO
            )

    def has_add_permission(self, request):
        return True

    def get_readonly_fields(self, request, obj=None):
        if 'add' in request.path or 'manual' in obj.identifier:
            return []
        unwanted_num = {'public_title', 'is_key_vote', 'is_final', 'type' }
        fields = [f.name for f in self.model._meta.fields]
        return [ele for ele in fields if ele not in unwanted_num]

    search_fields = ('identifier', 'original_title', 'description_title')

    def legislatura(self, obj):
        return obj.sitting.assembly

    def sitting_date(self, obj):
        return obj.sitting_date

    # filtri  legislatura
    list_display = ('identifier',
                    'original_title',
                    'description_title',
                    'public_title',
                    'sitting_date',
                    'sitting_branch',
                    'legislature',
                    'outcome',
                    'is_confidence',
                    'is_final',
                    'is_key_vote')
    raw_id_fields = ('president', 'bills', 'sitting')
    list_filter = (('sitting__assembly__parent', admin.RelatedOnlyFieldListFilter),
                   'is_confidence',
                   'is_key_vote',
                   'is_final',
                   'type',
                   VotingLegislatureFilter)
    list_editable = ('public_title', 'is_key_vote')

    sitting_date.admin_order_field = 'sitting__date'

    inlines = [
        BillInline,
    ]


@admin.register(Sitting)
class SittingAdmin(admin.ModelAdmin):

    date_hierarchy = 'date'

    def has_change_permission(self, request, obj=None):
        return False

    list_display = ('number', 'date', 'assembly')
    list_filter = (('assembly', admin.RelatedOnlyFieldListFilter), )


@admin.register(MembershipsMetricsHST)
class MembershipsMetricsHSTAdmin(admin.ModelAdmin):

    def nome_membro(self, obj):
        return obj.membership.opdm_membership.person.__str__()

    list_display = ('nome_membro', 'year', 'month', 'n_rebels',
                    'n_fidelity',)
    search_fields = ("membership__opdm_membership__person__name",)

    raw_id_fields = ("membership", )


@admin.register(MemberVote)
class MemberVoteAdmin(admin.ModelAdmin):

    def has_change_permission(self, request, obj=None):
        return False

    def nome_membro(self, obj):
        return obj.membership.opdm_membership.person.__str__()

    list_display = ('voting', 'membership', 'is_rebel',
                    'vote',)
    search_fields = ("membership__opdm_membership__person__name",
                     'voting__identifier')
