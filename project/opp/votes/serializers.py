"""Define elections serializers."""
from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField

# from api_v1.serializers import LinkRelSerializer, SourceRelSerializer
from .models import (
    Voting, Sitting, GroupVote, MemberVote, Membership, Group, MembershipGroup
)

from project.opp.acts.serializers import BillListSerializer, BillListSerializer2
from ..serializers import BaseSerializerOPP

from popolo.models import Membership as OPDMMembership
import datetime


class LegislatureHyperlinkedIdentityField(HyperlinkedIdentityField):

    def get_url(self, obj, view_name, request, format):
        """
        Given an object, return the URL that hyperlinks to the object,
        considering the legislature
        """
        # Unsaved objects will not yet have a valid URL.
        if hasattr(obj, 'pk') and obj.pk is None:
            return None
        if hasattr(obj, 'pk2') and obj.pk2 is not None:
            self.lookup_field = 'pk2'

        lookup_value = getattr(obj, self.lookup_field)
        kwargs = {
            'legislature': request.parser_context['kwargs']['legislature'],
            self.lookup_field: lookup_value,
        }
        return self.reverse(view_name, kwargs=kwargs, request=request, format=format)


class SittingInlineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Sitting
        ref_name = "Sitting (inline)"
        fields = (
            "id",
            # "url",
            "date",
            "number",
            "branch"
        )


class MembershipInlineSerializer(serializers.HyperlinkedModelSerializer):
    family_name = serializers.CharField(source='opdm_membership.person.family_name')
    given_name = serializers.CharField(source='opdm_membership.person.given_name')
    image = serializers.CharField(source='opdm_membership.person.image')
    gender = serializers.CharField(source='opdm_membership.person.gender')
    slug = serializers.SlugField(source='opdm_membership.person.slug')
    election_area = serializers.CharField()
    election_macro_area = serializers.CharField(default='Estero', )

    # url = LegislatureHyperlinkedIdentityField(
    #     view_name='membership-detail',
    # )

    class Meta:
        model = Membership
        ref_name = "Membership (inline)"
        fields = (
            "id",
            "slug",
            "n_rebels",
            "family_name",
            "given_name",
            "image",
            "gender",
            "election_area",
            "election_macro_area"
        )


class VotingListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='voting-detail',
    )
    sitting = SittingInlineSerializer(many=False)

    title = serializers.CharField(source="title_compact")
    slug = serializers.CharField(source='identifier')
    sub_vote_type = serializers.CharField(source="get_type_display")
    outcome = serializers.CharField(source="get_outcome_display")

    class Meta:
        """Define serializer Meta."""

        model = Voting
        ref_name = "Voting"
        fields = (
            "id",
            "slug",
            "identifier",
            "url",
            "sitting",
            "number",
            "title",
            "original_title",
            "description_title",
            "public_title",
            "is_key_vote",
            "is_final",
            "is_confidence",
            "is_secret",
            "sub_vote_type",
            "outcome",
            "n_rebels",
            "n_margin",
            "majority_cohesion_rate",
            "minority_cohesion_rate",
        )


class GroupSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source="name_compact")
    branch = serializers.SerializerMethodField()
    acronym = serializers.SerializerMethodField()

    def get_acronym(self, obj):
        return obj.acronym_last
    def get_branch(self, obj):
        return obj.branch.lower()

    class Meta:
        model = Group
        ref_name = "Group"
        fields = ("id", "acronym", "slug", "branch", "name", "color")


class MembershipGroupSerializer(serializers.ModelSerializer):
    group = GroupSerializer()

    class Meta:
        model = MembershipGroup
        ref_name = "Membership Group"
        fields = ("id",
                  "group"
                  )


class MemberVoteSerializer(serializers.ModelSerializer):
    membership = MembershipInlineSerializer()
    group = GroupSerializer(source='member_group_at_vote')
    supports_majority = serializers.NullBooleanField(source='member_supports_majority_at_vote')

    class Meta:
        model = MemberVote
        ref_name = "MemberVote"
        fields = [
            'vote', 'is_rebel', 'membership',
            'group',
            'supports_majority',
            # 'election_area'
        ]


class GroupVoteDetailSerializer(serializers.ModelSerializer):
    id = serializers.CharField(source='voting.id')
    slug = serializers.CharField(source='voting.identifier')
    sitting = SittingInlineSerializer(many=False, source='voting.sitting')
    title = serializers.CharField(source="voting.title_compact")
    outcome = serializers.CharField(source="voting.get_outcome_display")
    n_margin = serializers.CharField(source="voting.n_margin")
    group_vote = serializers.CharField(source="get_vote_display")

    # cohesion_rate = serializers.CharField()

    class Meta:
        model = GroupVote
        ref_name = "GroupVote"
        fields = ('id', 'slug', 'sitting', 'title', 'outcome', 'n_margin', 'group_vote', 'cohesion_rate')


class GroupVoteListSerializer(serializers.ModelSerializer):
    id = serializers.CharField(source='voting.id')
    slug = serializers.CharField(source='voting.identifier')
    sitting = SittingInlineSerializer(many=False, source='voting.sitting')
    title = serializers.CharField(source="voting.title_compact")
    outcome = serializers.CharField(source="voting.get_outcome_display")
    n_margin = serializers.CharField(source="voting.n_margin")
    group_vote = serializers.CharField(source="get_vote_display")

    # cohesion_rate = serializers.CharField()

    class Meta:
        model = GroupVote
        ref_name = "GroupVote"
        fields = ('id', 'slug', 'sitting', 'title', 'outcome', 'n_margin', 'group_vote', 'cohesion_rate')


class GroupVoteSerializer(serializers.ModelSerializer):
    group = GroupSerializer()

    class Meta:
        model = GroupVote
        ref_name = "GroupVote"
        fields = '__all__'


class VotingDetailSerializer(BaseSerializerOPP, VotingListSerializer):
    """A serializer for the Voting model, for detailed view"""
    president = MembershipInlineSerializer()
    groups_votes = GroupVoteSerializer(many=True, source='groups_votes_joined')
    members_votes = MemberVoteSerializer(many=True, source='members_votes_filtered')
    bills = BillListSerializer2(many=True)
    related_votings = VotingListSerializer(many=True)
    slug = serializers.CharField(source='identifier')
    def _context(self):
        context = super()._context()
        context.update({"voting": self.instance})
        return context
    def get_field_names(self, declared_fields, info):
        expanded_fields = super().get_field_names(declared_fields, info)

        if getattr(self.Meta, 'extra_fields', None):
            return expanded_fields + self.Meta.extra_fields
        else:
            return expanded_fields

    class Meta:
        """Define serializer Meta."""
        model = Voting
        fields = '__all__'
        extra_fields = ['groups_votes', 'members_votes']


class MembershipListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Memberships model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='membership-detail',
    )
    person_url = LegislatureHyperlinkedIdentityField(
        view_name='person-detail',
    )
    latest_group = MembershipGroupSerializer(source='get_latest_membership_group')
    has_changed_group = serializers.BooleanField()
    family_name = serializers.CharField(source='opdm_membership.person.family_name')
    given_name = serializers.CharField(source='opdm_membership.person.given_name')
    image = serializers.CharField(source='opdm_membership.person.image')
    gender = serializers.CharField(source='opdm_membership.person.gender')
    election_area = serializers.CharField()
    election_macro_area = serializers.CharField()
    assembly = serializers.CharField(source='opdm_membership.organization.identifier')
    start_date = serializers.CharField(source='opdm_membership.start_date')
    end_date = serializers.CharField(source='opdm_membership.end_date')
    role = serializers.CharField(source='opdm_membership.role')
    n_voting = serializers.IntegerField()
    days_in_parliament = serializers.DurationField()
    parse_days_in_parliament = serializers.CharField()
    id = serializers.IntegerField(source='opdm_membership.person.id')
    slug = serializers.SlugField(source='opdm_membership.person.slug')
    pp = serializers.SerializerMethodField()
    is_president = serializers.SerializerMethodField()
    stepped_in_date = serializers.SerializerMethodField()
    fidelity_index = serializers.IntegerField()

    def get_is_president(self, obj):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        return obj.is_official_president_given_date(today_strf)


    def get_pp(self, obj):
        return {
            'branch': ("C" if obj.opdm_membership.role == 'Deputato' else "S"),
            'pp_branch': obj.pp,
            'pp_branch_ordering': obj.pp_ordering
        } if obj.pp else None

    def get_stepped_in_date(self, obj):
        if obj.replaced_member:
            return obj.opdm_membership.start_date

    class Meta:
        model = Membership
        ref_name = "Membership"
        fields = (
            "id",
            "stepped_in_date",
            "slug",
            "url",
            "pp",
            "is_president",
            "is_active",
            'person_url',
            "n_rebels",
            "family_name",
            "given_name",
            "role",
            "latest_group",
            "has_changed_group",
            "image",
            "gender",
            "election_area",
            "election_macro_area",
            "start_date",
            "end_date",
            "assembly",
            "supports_majority",
            "n_voting",
            "n_present",
            "n_absent",
            "n_mission",
            "days_in_parliament",
            "parse_days_in_parliament",
            "fidelity_index"
        )


class CareerPositionDetailSerializer(serializers.ModelSerializer):
    """A serializer for the Memberships model when seen in lists."""
    org = serializers.CharField(source='organization.name')
    date_start = serializers.DateField(source='start_date')
    date_end = serializers.DateField(source='end_date')
    note = serializers.SerializerMethodField()

    def get_note(self, obj):
        note = None
        if obj.organization.classification in ['Assemblea parlamentare',
                                               'Consiglio comunale',
                                               'Consiglio regionale',
                                               'Giunta comunale',
                                               'Giunta regionale',
                                               'Organo legislativo internazionale o europeo']:

            if obj.organization.classification == 'Assemblea parlamentare':
                if obj.electoral_event.start_date > '2008-04-13':
                    return ''

            lista = obj.electoral_list_descr_tmp
            if lista:
                note = f'Lista di elezione {lista}'
                return note
        elif obj.organization.classification in ['Agenzia dello Stato',
                                                 'Azienda o ente del servizio sanitario nazionale',
                                                 'Dipartimento/Direzione/Ufficio',
                                                 'Governo della Repubblica',
                                                 'Prefettura',
                                                 'Istituto o ente pubblico di ricerca',
                                                 'Ufficio di diretta collaborazione']:
            if obj.organization.classification == 'Dipartimento/Direzione/Ufficio':
                if obj.role in ["Ministro Dipartimento/Direzione/Ufficio",
                                "Viceministro Dipartimento/Direzione/Ufficio",
                                "Sottosegretario Dipartimento/Direzione/Ufficio"]:
                    return ''
            appointed = obj.appointed_by
            if appointed:
                note = f'Nomina di {appointed.person.name}'
                return note

    class Meta:
        model = OPDMMembership
        ref_name = "OPDM Membership"
        fields = (
            # "id",
            "org",
            "role",
            "date_start",
            "date_end",
            "note"
        )


class MembershipDetailSerializer(BaseSerializerOPP, MembershipListSerializer):
    """A serializer for the Membership model, for detailed view"""
    previous_groups = MembershipGroupSerializer(source='get_previous_membership_groups', many=True)
    days_in_parliament = serializers.DurationField()
    parse_days_in_parliament = serializers.CharField()
    has_changed_group = serializers.BooleanField()
    birth_date = serializers.CharField(source='opdm_membership.person.birth_date')
    birth_place = serializers.CharField(source='opdm_membership.person.birth_location')

    class Meta(MembershipListSerializer.Meta):
        """Define serializer Meta."""
        fields = (
            "codelists",
            "id",
            "slug",
            "url",
            "n_rebels",
            "family_name",
            "given_name",
            "role",
            "latest_group",
            "previous_groups",
            "image",
            "gender",
            "election_area",
            "election_macro_area",
            "start_date",
            "stepped_in_date",
            "end_date",
            "assembly",
            "supports_majority",
            "days_in_parliament",
            "parse_days_in_parliament",
            "has_changed_group",
            "birth_date",
            "birth_place"
        )


class MemberVoteListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Memberships model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='membership-votes-detail',
    )

    sitting = SittingInlineSerializer(many=False, source='voting.sitting')
    title = serializers.CharField(source="voting.title_compact")
    # vote
    group = GroupSerializer(source='member_group')
    voting_slug = serializers.CharField(source="voting.identifier")

    # group_vote

    class Meta:
        model = MemberVote
        ref_name = "Membership Vote"
        fields = (
            "id",
            "voting_slug",
            "url",
            "sitting",
            "title",
            "vote",
            "group",
            "group_vote"
        )


class MemberVoteDetailSerializer(BaseSerializerOPP, MemberVoteListSerializer):
    """A serializer for the Membership model, for detailed view"""

    class Meta(MemberVoteListSerializer.Meta):
        """Define serializer Meta."""
        fields = (
            "id",
            "voting_slug",
            # "url",
            "sitting",
            "title",
            "vote",
            "group",
            "group_vote"
        )
