# from django_filters import rest_framework as extra_filters
# from drf_rw_serializers import viewsets as rw_viewsets
# from rest_framework import filters
# from api_v1.filters import FiltersInListOnlySchema
# from project.opp.votes.models import Voting
# from project.opp.votes.serializers import VotingListSerializer
# from project.opp.votes.filtersets import VotingFilterSet
#
#
# class VotingViewSet(rw_viewsets.ModelViewSet):
#     """A viewset for the Votings"""
#
#     queryset = Voting.objects.select_related('sitting__assembly')
#     search_fields = ['original_title', 'description_title', 'public_title']
#     ordering_fields = (
#         "sitting__date",
#         "created_at",
#     )
#     ordering = ("-sitting__date",)
#     filter_backends = (
#         filters.SearchFilter,
#         filters.OrderingFilter,
#         extra_filters.DjangoFilterBackend,
#     )
#     filter_class = VotingFilterSet
#
#     # use a custom AutoSchema class to show filters only in list action
#     schema = FiltersInListOnlySchema()
#
#     serializer_class = VotingListSerializer
