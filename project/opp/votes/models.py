import statistics
import math
import pandas as pd
from django.db import models
from django.core.cache import caches
cache = caches['default']
db_cache = caches['db']
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.contenttypes.fields import GenericRelation
from django.db.models import Prefetch, Q, Value, F, Sum, DateField, ExpressionWrapper, fields, Min, CharField
from django.db.models.functions import Coalesce, Cast
from django.utils.translation import gettext_lazy as _
from django.urls import reverse
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site

from popolo.mixins import SourceShortcutsMixin, LinkShortcutsMixin
from popolo.behaviors.models import Timestampable, Dateframeable
from popolo.models import Area, KeyEvent, Organization, SourceRel, LinkRel, Membership as OPDMMembership

from project.calendars.models import CalendarDaily
from project.opp.acts.models import Bill
from colorfield.fields import ColorField

import re
import datetime

from project.opp.utils import parse_days



class GroupManagerOrganization(models.Manager):
    def all_and_metagroups(self):
        return super().get_queryset()

    def get_queryset(self):
        return super().get_queryset().exclude(is_meta_group=True)

    def by_legislature(self, leg=None):
        if leg:
            leg_identifier = f"ITL_{leg}"
            return self.get_queryset().select_related('legislature').filter(
                legislature__identifier=leg_identifier
            )
        else:
            return self.get_queryset().select_related('legislature')


class Group(models.Model):
    """An organization extension, dedicated to store voting fields"""
    organization = models.ForeignKey(
        to=Organization,
        on_delete=models.CASCADE,
        related_name="groups",
        null=True, blank=True
    )
    legislature = models.ForeignKey(
        to=KeyEvent,
        on_delete=models.CASCADE,
        null=True, blank=True,
        limit_choices_to={'event_type__in': ["ITL", ]},
    )
    color = ColorField(default='#FFFFFF')

    @property
    def name(self):
        return self.organization.name

    @property
    def slug(self):
        if self.acronym == 'Nessun gruppo':
            return None
        else:
            return self.acronym

    @property
    def branch(self):
        return self.organization.parent.parent.name.split(' ')[0]

    acronym = models.CharField(
        max_length=32,
        blank=True, null=True,
        help_text=_("Acommonly used acronym for this group, if existing")
    )
    cohesion_rate = models.FloatField(
        _("cohesion rate"),
        help_text=_("A cohesion rate for this group, from 0 to 100"),
        validators=[MinValueValidator(0.0), MaxValueValidator(100.0)],
        blank=True, null=True
    )
    supports_majority = models.BooleanField(
        null=True,
        help_text=_("Whether the group currently supports the majority")
    )
    exclude_rebel_calc = models.BooleanField(
        null=True,
        help_text=_("Whether to exclude the group from rebel calculation"),
        default=False
    )
    is_meta_group = models.BooleanField(
        null=True,
        default=False,
        help_text=_("Whether the group is meta (like Maggioranza and Opposizione)")
    )
    objects = GroupManagerOrganization()  # manager for only actual groups

    def members_at_date(self, date):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        return self.organization.memberships.all().annotate(end_date_coalesce=Coalesce(F('end_date'),
                                                Value(today_strf))).filter(start_date__lte=date,
                                                                          end_date_coalesce__gte=date)
    @property
    def members_stats(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')


        all_members = Membership.objects.filter(opdm_membership__end_date__isnull=True).annotate(
            age=Value(today) - Cast(F('opdm_membership__person__birth_date'), fields.DateField())
        )
        members = self.membership_groups.filter(end_date__isnull=True)
        members = members.annotate(
            age=Value(today) - Cast(F('membership__opdm_membership__person__birth_date'), fields.DateField())
        )
        list_ages = [x['age'].days / 365.2425 for x in members.values('age')]
        list_ages_all_members = [x['age'].days / 365.2425 for x in all_members.values('age')]
        years_number = statistics.mean([x.membership.days_in_parliament.days for x in members]) / 365
        years_number_all = statistics.mean([x.days_in_parliament.days for x in all_members]) / 365
        gained_lost = self.gained_lost_parliamentarians()
        lost_gained_list = gained_lost.get('gained_list') + gained_lost.get('lost_list')
        res = {
            'total': members.count(),
            'gained': gained_lost.get('timeline')[-1]['values'][-1]['gained'],
            'lost': gained_lost.get('timeline')[-1]['values'][-1]['lost'],
            'lost_gained_list': sorted(lost_gained_list, key=lambda x: x['date'], reverse=True),
            'time_in_parliament': {
                'years': math.trunc(years_number),
                'days': round(365 * (years_number - math.trunc(years_number))),
                'years_avg': math.trunc(years_number_all),
                'days_avg': round(365 * (years_number_all - math.trunc(years_number_all)))
            },
            'stats': {
                'gender': {
                    'female': members.filter(membership__opdm_membership__person__gender='F').count(),
                    'male': members.filter(membership__opdm_membership__person__gender='M').count()
                },
                'age': {
                    'avg': round((sum(list_ages) / len(list_ages)), 0),
                    'distribution': {
                        '_35': len(list(filter(lambda x: x < 35, list_ages))),
                        '35_45': len(list(filter(lambda x: x >= 35 and x < 45, list_ages))),
                        '45_55': len(list(filter(lambda x: x >= 45 and x < 55, list_ages))),
                        '55_65': len(list(filter(lambda x: x >= 55 and x < 65, list_ages))),
                        '65_': len(list(filter(lambda x: x >= 65, list_ages)))
                    }
                },
                "general_avg_age": round((sum(list_ages_all_members) / len(list_ages_all_members)), 0)
            },
            'timeline': gained_lost.get('timeline')

        }

        return res

    @property
    def name_compact(self):
        if not self.is_meta_group:
            regex = r"\"(.+)\""
            nome_gruppo = re.findall(regex, self.organization.name, re.MULTILINE)
            res = f"{nome_gruppo[0]}"
        else:
            if self.organization:
                res = f"{self.acronym}"
            else:
                res = self.acronym
        return res

    def update_cache_group_detail_parliament(self):
        res = self._group_detail_parliament()
        cache.set(
            f'{self.__class__.__name__}_group_detail_parliament:{self.organization.id}',
            {'res': res},
            timeout=None
        )

    def group_detail_parliament(self):
        if not cache.get(f'{self.__class__.__name__}_{"group_detail_parliament"}:{self.organization.id}'):
            self.update_cache_group_detail_parliament()
            return self._group_detail_parliament()
        return cache.get(f'{self.__class__.__name__}_{"group_detail_parliament"}:{self.organization.id}').get('res')

    @property
    def pp(self):
        cache_key = f'parl_org_detail:{self.branch.lower()}_groups'
        cached_results = cache.get(cache_key)
        groups = cached_results.get('groups').get('groups_power')
        if self.organization.end_date:
            list_groups = groups.get('dissolved')['detail']
            group = next(filter(lambda x: x['id'] == self.id, list_groups))
        elif self.supports_majority:
            list_groups = groups.get('majority')['detail']
            group = next(filter(lambda x: x['id'] == self.id, list_groups))
        elif self.supports_majority == False:
            list_groups = groups.get('minority')['detail']
            group = next(filter(lambda x: x['id'] == self.id, list_groups))
        else:
            list_groups = groups.get('others')['detail']
            group = next(filter(lambda x: x['id'] == self.id, list_groups))
        hist = []
        years = sorted(list(set([x['year'] for x in group['historical_pp']])))
        for year in years:
            hist.append(
                {'year': year,
                 'values': []}
            )
            months = list(filter(lambda x: x['year'] == year, group['historical_pp']))
            for month in months:
                hist[-1]['values'].append(
                    {
                        'month': month['month'],
                        'pp_branch': month['pp_branch']
                    }
                )
        res = {
            'branch': self.branch[0],
            'pp_branch': hist[-1].get('values')[-1].get('pp_branch') if hist else group['pp']['pp_branch'],
            'timeline': hist

        }
        return res

    def _group_detail_parliament(self):
        group_act = self.membership_groups.filter(end_date=self.organization.end_date)
        women = group_act.filter(membership__opdm_membership__person__gender='F')
        presence = self.votes.all().aggregate(sum_total=Sum(F('n_group_total')), sum_present=Sum(F('n_present')))
        gained_lost = self.gained_lost_parliamentarians()
        res = {
            'acronym': self.acronym_last,
            'start_date': self.organization.start_date,
            'end_date': self.organization.end_date,
            'name': self.name_compact,
            'color': self.color,
            'branch': self.branch.lower(),
            'slug': self.slug,
            'supports_majority': self.supports_majority,
            'avg_present': round((100 * presence['sum_present']) / presence['sum_total'], 2),
            'perc_women': round((100 * women.count()) / group_act.count(), 2) if group_act.count() else None,
            'cnt': self.membership_groups.filter(end_date=self.organization.end_date).count(),
            'gained': gained_lost.get('timeline')[-1]['values'][-1]['gained'],
            'lost': gained_lost.get('timeline')[-1]['values'][-1]['lost'],

        }
        return res

    def group_cohesion_rate_detail(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        dates_from_leg = (list(CalendarDaily.objects.filter(date__gte=self.legislature.start_date,
                                                           date__lte=today_strf).values('year', 'month').distinct().order_by('year', 'month')
                          .annotate(value=Value(0))))
        hist = list(GroupsMetricsHST.objects.filter(group=self)\
            .annotate(value=F('cohesion_rate_overall'))\
            .values('year', 'month', 'value').order_by('year', 'month'))
        historical = dates_from_leg[:-len(hist)] + hist
        res = {
            'acronym': self.acronym_last,
            'name': self.name_compact,
            'slug': self.slug,
            'branch': self.branch.lower(),
            'color': self.color,
            'value': historical[-1].get('value'),
            'historical': historical
        }

        return res

    def supporting_parties(self):
        if self.organization.end_date is None:
            return self.organization.from_relationships.filter(classification__code='OT_02', end_date__isnull=True)
        return self.organization.from_relationships.filter(classification__code='OT_02', end_date__isnull=True).none()

    def ex_supporting_parties(self):
        if self.organization.end_date is None:
            return self.organization.from_relationships.filter(classification__code='OT_02', end_date__isnull=False)
        return self.organization.from_relationships.filter(classification__code='OT_02')

    def vote_behaviour(self):
        vote_behaviour = self.votes.all().aggregate(denom=Sum(F('n_group_total')),
                                                    vote_behaviour=Sum(F('n_present')),
                                                    absence=Sum(F('n_absent')), mission=Sum(F('n_mission')),
                                                    )

        historical_metrics = self.historic_metrics.order_by('year', 'month')
        years = sorted(list(set(historical_metrics.values_list('year', flat=True))))
        timeline = []
        for year in years:
            timeline.append(
                {'year': year,
                 'values': []}
            )
            subset = historical_metrics.filter(year=year)
            for i in subset:
                timeline[-1]['values'].append(
                    {
                        "month": i.month,
                        "n_absent_perc": i.perc_absent,
                        "n_mission_perc": i.perc_mission,
                        "n_present_perc": i.perc_present,
                    }
                )

        res = {
            'n_absent': vote_behaviour['absence'],
            'n_mission': vote_behaviour['mission'],
            'n_present': vote_behaviour['vote_behaviour'],
            'n_voting': vote_behaviour['denom'],
            'timeline': timeline
        }
        return res

    def cohesion(self):

        votes = self.votes.all()
        historical_metrics = self.historic_metrics.order_by('year', 'month')
        years = sorted(list(set(historical_metrics.values_list('year', flat=True))))
        timeline = []
        for year in years:
            timeline.append(
                {'year': year,
                 'values': []}
            )
            subset = historical_metrics.filter(year=year)
            for i in subset:
                timeline[-1]['values'].append(
                    {
                        "month": i.month,
                        "cohesion_rate": i.cohesion_rate_overall,
                    }
                )

        res = {
            'cohesion_rate': timeline[-1]['values'][-1].get('cohesion_rate'),
            'stacked_barplot': {
                '_60': votes.filter(cohesion_rate__lt=60).count(),
                '60_80': votes.filter(Q(cohesion_rate__gte=60) | Q(cohesion_rate__lt=80)).count(),
                '80_100': votes.filter(cohesion_rate__gte=80).count()
            },
            'timeline': timeline
        }

        return res

    def discordant_votes(self):

        from project.opp.votes.serializers import GroupVoteDetailSerializer
        votes = self.votes.all().order_by('cohesion_rate', '-voting__sitting__date')[:5]
        res = {
            'count': votes.count(),
            'results': [GroupVoteDetailSerializer(x).data for x in votes]
        }
        return res

    def key_votes(self):

        from project.opp.votes.serializers import GroupVoteDetailSerializer
        votes = self.votes.filter(voting__is_key_vote=True).order_by('-voting__sitting__date')
        res = {
            'count': votes.count(),
            'results': [GroupVoteDetailSerializer(x).data for x in votes[:5]]
        }
        return res

    def group_components_power_roles(self):
        from project.opp.metrics.models import PositionalPower
        from project.opp.gov.models import GovPartyMember
        memberships = self.membership_groups.filter(end_date__isnull=True).order_by('-membership__pp')
        person_ids = memberships.values_list('membership__opdm_membership__person', flat=True)
        pp_roles = PositionalPower.objects.filter(opdm_membership__person__in=person_ids, end_date__isnull=True)
        roles_gov = pp_roles.filter(
            opdm_membership__organization__classification__in=[
                'Presidenza del Consiglio',
                'Ministero',
                'Ministero senza portafoglio'])
        roles_other_parl = OPDMMembership.objects.filter(
            organization__classification__in=[
                'Giunta parlamentare',
                'Commissione/comitato bicamerale parlamentare',
                'Organo di presidenza parlamentare',
                'Commissione permanente parlamentare',
                'Commissione speciale/straordinaria parlamentare',
                'Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza',
                'Commissione d\'inchiesta parlamentare monocamerale/bicamerale',
                'Delegazione parlamentare presso assemblea internazionale']).filter(end_date__isnull=True)
        map_role_value = {
            'Presidente': 1,
            'Ministro': 2,
            'Viceministro': 3,
            'Sottosegretario': 4,
            'Vicevicario': 3,
            'Vicepresidente': 4,
            'Tesoriere': 5,
            'Questore': 6,
            'Segretario': 7
        }

        map_org_value = {
            'Organo di presidenza parlamentare': 1,
            'Presidenza del Consiglio': 1,
            'Ministero': 2,
            'Ministero senza portafoglio': 2,
            'Giunta parlamentare': 4,
            'Commissione/comitato bicamerale parlamentare': 6,
            'Commissione permanente parlamentare': 3,
            'Commissione speciale/straordinaria parlamentare': 4,
            'Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza': 6,
            'Commissione d\'inchiesta parlamentare monocamerale/bicamerale': 6,
            'Delegazione parlamentare presso assemblea internazionale': 7
        }
        members_roles = []
        for i in self.organization.memberships.filter(end_date__isnull=True):
            # if 'Membro' in i.role:
            #     continue
            total_ordering = None
            membership_group = memberships.get(membership__opdm_membership__person=i.person)
            if getattr(membership_group.membership, 'branch', None) == 'C':
                total_ordering = Membership.objects.filter(opdm_membership__end_date__isnull=True,
                                                           opdm_membership__role__startswith='D').count()
            elif getattr(membership_group.membership, 'branch', None) == 'S':
                total_ordering = Membership.objects.filter(opdm_membership__end_date__isnull=True,
                                                           opdm_membership__role__startswith='S').count()
            members_roles.append(
                {
                    'id': i.person.id,
                    'slug': i.person.slug,
                    'img': i.person.image,
                    'name': i.person.name,
                    'surname': i.person.family_name,
                    'group_cohesion_rate': round(
                        100 * (membership_group.n_fidelity or 0) / ((membership_group.n_present or 0) + (membership_group.n_mission or 0) + (membership_group.n_absent or 0)),
                        2) if ((membership_group.n_present or 0) + (membership_group.n_mission or 0) + (membership_group.n_absent or 0)) else None,
                    'vote_presence_rate': round(
                        100 * (membership_group.n_present or 0) / ((membership_group.n_present or 0) + (membership_group.n_mission or 0) + (membership_group.n_absent or 0)),
                        2) if (
                        (membership_group.n_present or 0) + (membership_group.n_mission or 0) + (membership_group.n_absent or 0)) else None,
                    'pp': {
                        'branch': self.branch.upper()[0],
                        'pp_branch': membership_group.membership.pp,
                        'pp_branch_ordering': membership_group.membership.pp_ordering,
                        'pp_branch_total_ordering': total_ordering
                    },
                    'roles': []
                }
            )
            members_roles[-1]['roles'].append(
                {'role': i.role.split(' ')[0],
                 'role_value': map_role_value.get(i.role.split(' ')[0], 10),
                 'org_name': ''}
            )

        members_roles = sorted(members_roles, key=lambda x: (x['roles'][0]['role_value'], x['surname']))
        roles_gov_roles = []
        for i in memberships:
            roles_gov_membro = roles_gov.filter(
                opdm_membership__person__id=i.membership.opdm_membership.person_id)
            if roles_gov_membro.count() > 0:
                gov_member = GovPartyMember.objects.filter(
                    gov_member__end_date__isnull=True, gov_member__person_id=i.membership.opdm_membership.person.id) \
                    .first()
                roles_gov_roles.append(
                    {
                        'id': i.membership.opdm_membership.person.id,
                        'slug': i.membership.opdm_membership.person.slug,
                        'img': i.membership.opdm_membership.person.image,
                        'name': i.membership.opdm_membership.person.name,
                        'group_cohesion_rate': round(
                            100 * (i.n_fidelity or 0) / ((i.n_present or 0) + (i.n_mission or 0) + (i.n_absent or 0)),
                            2) if ((i.n_present or 0) + (i.n_mission or 0) + (i.n_absent or 0)) else None,
                        'vote_presence_rate': round(100 * i.n_present / (i.n_present + i.n_mission + i.n_absent),
                                                    2) if (
                            i.n_present + i.n_mission + i.n_absent) else None,
                        'pp': {
                            'ppg': getattr(gov_member, 'ppg', None).get('ppg'),
                            'ppg_branch_ordering': getattr(gov_member, 'ppg', None).get('ppg_branch_ordering'),
                            'ppg_branch_total_ordering': getattr(gov_member, 'ppg', None).get('ppg_branch_total_ordering')
                        },
                        'roles': []
                    }
                )
                for role in roles_gov_membro:
                    roles_gov_roles[-1]['roles'].append(
                        {'role': role.opdm_membership.role.split(' ')[0],
                         'role_value': map_role_value.get(role.opdm_membership.role.split(' ')[0], 100),
                         'org_name': role.opdm_membership.organization.name,
                         'org_value': map_org_value.get(role.opdm_membership.organization.classification, 100)}
                    )
                    roles_gov_roles[-1]['roles'] = sorted(roles_gov_roles[-1]['roles'],
                                                          key=lambda x: (x['org_value'], x['role_value']))
        roles_gov_roles = sorted(roles_gov_roles,
                                 key=lambda x: (x['roles'][0]['org_value'], x['roles'][0]['role_value']))
        roles_pres_parl_roles = []
        for i in memberships:
            roles_pres_parl_membro = roles_other_parl.filter(
                person__id=i.membership.opdm_membership.person_id).exclude(role__icontains='Componente')
            if roles_pres_parl_membro.count() > 0:
                total_ordering = None
                if getattr(i.membership, 'branch', None) == 'C':
                    total_ordering = Membership.objects.filter(opdm_membership__end_date__isnull=True,
                                                               opdm_membership__role__startswith='D').count()
                elif getattr(i.membership, 'branch', None) == 'S':
                    total_ordering = Membership.objects.filter(opdm_membership__end_date__isnull=True,
                                                               opdm_membership__role__startswith='S').count()
                roles_pres_parl_roles.append(
                    {
                        'id': i.membership.opdm_membership.person.id,
                        'slug': i.membership.opdm_membership.person.slug,
                        'img': i.membership.opdm_membership.person.image,
                        'name': i.membership.opdm_membership.person.name,
                        'group_cohesion_rate': round(
                            100 * (i.n_fidelity or 0) / ((i.n_present or 0) + (i.n_mission or 0) + (i.n_absent or 0)),
                            2) if ((i.n_present or 0) + (i.n_mission or 0) + (i.n_absent or 0)) else None,
                        'vote_presence_rate': round(100 * i.n_present / ((i.n_present or 0) + (i.n_mission or 0) + (i.n_absent or 0)),
                                                    2) if (
                            (i.n_present or 0) + (i.n_mission or 0) + (i.n_absent or 0)) else None,
                        'pp': {
                            'branch': self.branch.upper()[0],
                            'pp_branch': i.membership.pp,
                            'pp_branch_ordering': i.membership.pp_ordering,
                            'pp_branch_total_ordering': total_ordering
                        },
                        'roles': []
                    }
                )
                role_memberships = roles_pres_parl_membro  # OPDMMembership.objects.filter(
                # id__in=roles_pres_parl_membro.values('opdm_membership'))
                for role in role_memberships:
                    pattern = r"(.*) (Senato|Camera)*\([XIV ]*legislatura\)"
                    org_name = re.findall(pattern, role.organization.name)[0][0].strip()
                    roles_pres_parl_roles[-1]['roles'].append(
                        {'role': role.role.split(' ')[0],
                         'role_value': map_role_value.get(role.role.split(' ')[0], 10),
                         'org_name': org_name,
                         'org_value': map_org_value.get(role.organization.classification, 100)}
                    )
                roles_pres_parl_roles[-1]['roles'] = sorted(roles_pres_parl_roles[-1]['roles'],
                                                            key=lambda x: (x['org_value'], x['role_value']))
        roles_pres_parl_roles = sorted(roles_pres_parl_roles,
                                       key=lambda x: (x['roles'][0]['org_value'], x['roles'][0]['role_value']))
        res = {
            'member_roles': members_roles,
            'gov_roles': roles_gov_roles,
            'president_roles': roles_pres_parl_roles
        }
        return res

    def gained_lost_parliamentarians(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        sd = self.membership_groups.aggregate(Min('start_date'))['start_date__min']

        dates = CalendarDaily.objects.filter(date__gte=sd,
                                             date__lte=(self.organization.end_date or today)).filter(
            Q(is_month_end=True) | Q(date=today))
        timeline = []
        years = sorted(list(set(dates.values_list('year', flat=True))))
        for year in years:
            timeline.append(
                {'year': year,
                 'values': []}
            )
            dates_year = dates.filter(year=year)
            for date in dates_year:
                gained = self.membership_groups.exclude(start_date=sd).filter(start_date__gt=sd, start_date__lte=date)
                lost = self.membership_groups.exclude(end_date__isnull=True).filter(end_date__gte=sd,
                                                                                    end_date__lte=date)
                timeline[-1]['values'].append(
                    {'month': date.month,
                     'gained': gained.count(),
                     'lost': lost.count()}
                )
        gained_list = []

        for g in gained:
            repl_member = False
            if g.start_date == g.membership.opdm_membership.start_date:
                repl_member = True
                group_out = None
            else:
                group = MembershipGroup.objects.filter(membership=g.membership, start_date__lt=g.start_date).order_by(
                    'start_date').last().group
                group_out = {
                    'id': group.id,
                    'acronym': group.acronym_last,
                    'name': group.name_compact,
                    'slug': group.slug,
                    'branch': group.branch.lower(),
                    'color': group.color
                }
            gained_list.append(
                {
                    'slug': g.membership.opdm_membership.person.slug,
                    'name': g.membership.opdm_membership.person.name,
                    'img': g.membership.opdm_membership.person.image,
                    'date': g.start_date,
                    'out': group_out,
                    'in': {
                        'id': self.id,
                        'acronym': self.acronym,
                        'name': self.name_compact,
                        'slug': self.slug,
                        'branch': self.branch.lower(),
                        'color': self.color
                    },
                    'replaced_member': {
                        'slug': g.membership.replaced_member.opdm_membership.person.slug,
                        'name': g.membership.replaced_member.opdm_membership.person.name,
                        'img': g.membership.replaced_member.opdm_membership.person.image
                    } if repl_member else None
                }
            )
        lost_list = []
        for l in lost:
            group_in = None
            replaced_by = None
            if (datetime.datetime.strptime(l.end_date, '%Y-%m-%d') + datetime.timedelta(days=1)).strftime(
                '%Y-%m-%d') == l.membership.opdm_membership.end_date:
                replaced_by = getattr(l.membership, 'replaced_membership_parliament', None)

            if l.end_reason not in ('Decesso', 'Dimissioni'):
                group = MembershipGroup.objects.filter(membership=l.membership, start_date__gt=l.end_date).order_by(
                    'start_date').first().group
                group_in = {
                    'id': group.id,
                    'acronym': group.acronym_last,
                    'name': group.name_compact,
                    'slug': group.slug,
                    'branch': group.branch.lower(),
                    'color': group.color
                }
            lost_list.append(
                {
                    'slug': l.membership.opdm_membership.person.slug,
                    'name': l.membership.opdm_membership.person.name,
                    'img': l.membership.opdm_membership.person.image,
                    'date': l.end_date,
                    'in': group_in,
                    'out': {
                        'id': self.id,
                        'acronym': self.acronym,
                        'name': self.name_compact,
                        'slug': self.slug,
                        'branch': self.branch.lower(),
                        'color': self.color
                    },
                    'end_reason': l.end_reason,
                    'replaced_by': {
                        'slug': replaced_by.opdm_membership.person.slug,
                        'name': replaced_by.opdm_membership.person.name,
                        'img': replaced_by.opdm_membership.person.image
                    } if replaced_by else None
                }
            )

        return {'timeline': timeline,
                'gained_list': gained_list,
                'lost_list': lost_list}
    @property
    def acronym_last(self):
        res = self.organization.other_names.filter(othername_type='ACR').order_by('end_date').last()
        if res:
            return res.name
        return self.acronym

    def __str__(self):

        if not self.is_meta_group:
            org = f"- {self.organization.parent.parent.name} "
        else:
            if self.organization:
                org = f"- {self.organization.name} "
            else:
                org = ' '
        return f"{self.name_compact}{org}({self.legislature})"


    class Meta:
        verbose_name = _("Gruppo")
        verbose_name_plural = _("Gruppi")
        ordering = ('organization__start_date',)

class MemberVoteManager(models.Manager):
    def by_legislature(self, leg=None):
        if leg:
            leg_identifier = f"ITL_{leg}"
            return super().get_queryset().select_related('voting__sitting', 'voting__sitting__assembly').filter(
                voting__sitting__assembly__key_events__key_event__identifier=leg_identifier
            )
        else:
            return super().get_queryset().select_related('voting__sitting__assembly', 'voting__sitting')


class GroupVoteManager(models.Manager):
    def by_legislature(self, leg=None):
        if leg:
            leg_identifier = f"ITL_{leg}"
            return super().get_queryset().select_related('voting__sitting', 'voting__sitting__assembly').filter(
                voting__sitting__assembly__key_events__key_event__identifier=leg_identifier
            )
        else:
            return super().get_queryset().select_related('voting__sitting__assembly', 'voting__sitting')


class Sitting(Timestampable, SourceShortcutsMixin, models.Model):
    """Sitting of the organ where votations occurr."""

    date = models.DateField(
        _("start date"),
        help_text=_("The date of the sitting (starting point)"),
    )
    number = models.PositiveSmallIntegerField(
        help_text=_("Sitting number"),
        null=True
    )
    assembly = models.ForeignKey(
        to=Organization,
        limit_choices_to={'classification__in': ["Assemblea parlamentare", ]},
        on_delete=models.PROTECT,
        related_name="sittings",
        help_text=_("The assembly where the votation occurs")
    )
    identifier = models.CharField(
        _("identifier"), max_length=512, blank=True, null=True,
        help_text=_("The unique URI")
    )
    sources = GenericRelation(
        to=SourceRel,
        help_text=_("URLs to source documents")
    )

    @property
    def branch(self):
        if 'ASS-CAMERA' in self.assembly.identifier:
            return 'C'
        elif 'ASS-SENATO' in self.assembly.identifier:
            return 'S'
        else:
            raise Exception("Wrong branch")

    @property
    def get_legislature(self):
        return \
            self.assembly.key_events.get(key_event__identifier__startswith='ITL').key_event.identifier.split('_')[
                -1]

    class Meta:
        verbose_name = _("Assembly sitting")
        verbose_name_plural = _("Assembly sittings")

    def __str__(self):
        return f"{self.branch}.{self.number} @ {self.date}"


class LegVotingManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_legislature(self, leg=None):
        if leg:
            leg_identifier = f"ITL_{leg}"
            return super().get_queryset().select_related('sitting', 'sitting__assembly').filter(
                sitting__assembly__key_events__key_event__identifier=leg_identifier
            )
        else:
            return super().get_queryset().select_related('sitting__assembly', 'sitting')


class LegMembershipsManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_legislature(self, leg=None):
        if leg:
            leg_identifier = f"ITL_{leg}"
            return super().get_queryset().select_related('opdm_membership__organization').filter(
                opdm_membership__organization__key_events__key_event__identifier=leg_identifier
            )
        else:
            return super().get_queryset().select_related('opdm_membership__organization')


class Membership(models.Model):
    """A Membership extension, dedicated to store voting fields"""
    opdm_membership = models.OneToOneField(
        to=OPDMMembership,
        on_delete=models.CASCADE,
        unique=True,
        related_name='membership_parliament'
    )
    n_rebels = models.PositiveSmallIntegerField(
        _("n. of rebel voters"),
        help_text=_("Number of voters rebelling against their group"),
        blank=True, null=True
    )
    n_fidelity = models.PositiveSmallIntegerField(
        _("n. of fidelity voters"),
        help_text=_("Number of votes same as group"),
        blank=True, null=True
    )
    perc_fidelity = models.SmallIntegerField(
        "perc voti fedeli",
        blank=True, null=True
    )
    n_present = models.PositiveSmallIntegerField(
        _("n. of votes member was present"),
        help_text=_("Number of votes member was present"),
        blank=True, null=True
    )
    n_absent = models.PositiveSmallIntegerField(
        _("n. of votes member was absent"),
        help_text=_("Number of votes member was absent"),
        blank=True, null=True
    )
    n_mission = models.PositiveSmallIntegerField(
        _("n. of fidelity voters"),
        help_text=_("Number of votes member was in mission"),
        blank=True, null=True
    )
    supports_majority = models.BooleanField(
        null=True,
        help_text=_("Whether the member currently supports the majority")
    )
    election_area = models.ForeignKey(
        to=Area,
        on_delete=models.CASCADE,
        related_name="opp_membership",
        limit_choices_to={'classification__in': ["ADM1", "ELECT_RIP"]},
        help_text=_("It represents the area where the memberships was elected,"
                    "if possibile it is the regional level, otherwise it is the closest are to Regional level"),
        null=True, blank=True,
        db_index=True
    )

    days_in_parliament = models.DurationField(
        _("Member of Parliament for"),
        help_text=_("Historical record for member duration in parliament at current date"),
        blank=True,
        null=True
    )

    replaced_member = models.OneToOneField(
        to='self',
        on_delete=models.CASCADE,
        blank=True,
        null=True,
        limit_choices_to={
            'opdm_membership__role__in': [
                "Senatore",
                "Deputato",
                "Senatore a vita"],
            'opdm_membership__end_date__isnull': False
        },
        related_name='replaced_membership_parliament'
    )

    pp = models.FloatField(
        _("Positional Power"),
        help_text=_("Positional Power"),
        blank=True,
        null=True
    )

    pp_ordering = models.IntegerField(
        _("Positional Power Ordering"),
        help_text=_("Positional Power Ordering"),
        blank=True,
        null=True
    )

    is_active = models.BooleanField(
        _("Positional Power Ordering"),
        help_text=_("Positional Power Ordering"),
        default=True,
    )

    objects = LegMembershipsManager()

    @property
    def fidelity_index(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        if self.is_official_president_given_date(today_strf):
            return -1
        n_fidelity = (self.n_fidelity or 0)
        n_present = (self.n_present or 0)
        n_absent = (self.n_absent or 0)
        n_missing = (self.n_mission or 0)

        return None if not (n_present + n_absent + n_missing) else round(100 * n_fidelity /
                                                                         (n_present + n_absent + n_missing),
                                                                         2)

    def is_official_president_given_date(self, date) -> bool:
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        """

        :param date: [YYYY--MM--DD]
        :return: if is official president during time
        :rtype: bool
        """
        return self.opdm_membership.person.memberships \
            .filter(role="Presidente Organo di presidenza parlamentare") \
            .annotate(new_end_date=Coalesce(F('end_date'), Value(today_strf))) \
            .filter(start_date__lte=date, new_end_date__gte=date).count() > 0

    def get_absolute_metrics(self, metric):
        return self.historical_metrics.aggregate(Sum(f'{metric}')).get(f'{metric}__sum')

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        rev = reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        return f'https://{Site.objects.first().name}{rev}'

    @property
    def n_voting(self):
        return (self.n_absent or 0) + (self.n_mission or 0) + (self.n_present or 0)

    @property
    def get_latest_membership_group(self):
        "Returns the current membership group of the deputy/senator"
        return self.membership_groups.order_by('start_date').last()

    @property
    def get_previous_membership_groups(self):
        "Returns the previous memberships group of the deputy/senator"
        new_date = datetime.datetime.strptime(self.opdm_membership.start_date, '%Y-%m-%d') + datetime.timedelta(days=15)
        new_date = new_date.strftime('%Y-%m-%d')
        return self.membership_groups.exclude(group__acronym__in=['Misto', 'Nessun gruppo'], end_date__lte=new_date)

    @property
    def has_changed_group(self):
        "Returns if the deputy/senator has changed group at least once"
        return self.get_previous_membership_groups.count() > 1

    @property
    def get_latest_group(self) -> Group:
        "Returns the current group of the deputy/senator"
        return self.get_latest_membership_group.group

    def get_group_at_date(self, date) -> Group:
        ""
        return MembershipGroup.objects.filter(membership=self).get(
            Q(start_date__lte=date) & (
                Q(end_date__gte=date) | Q(end_date__isnull=True))
        )

    def get_changed_maj_min(self, gov, date):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        beg = MemberMajority.objects.get(government=gov,
                                         membership=self.lower_member_reference,
                                         start_date=gov.start_date)
        at_date = MemberMajority.objects. \
            annotate(end_date_coalesce=Coalesce(F('end_date'),
                                                Value(today_strf))) \
            .filter(government=gov, membership=self.upper_member_reference,
                    start_date__lte=date,
                    end_date_coalesce__gte=date).first()
        if at_date:
            return not beg.value == at_date.value
        else:
            return False


    @staticmethod
    def _get_upper_level_from_area(area, level=None):
        '''
        Returns the associated UPPER level according to level choices
        :param level {'RIP', 'REG'}
        :param area popolo.models.Area object
        :return: str of Area object of level <level> upper closer to self
        :rtype str
        '''
        assert level in ['REG', 'RIP', None]
        if not level:
            return area

        elif area:
            if area.classification == 'ELECT_RIP':
                return area
            parent = area.parent
            if parent:
                if parent.istat_classification == level:
                    return parent
                else:
                    return Membership._get_upper_level_from_area(parent, level)

    def _get_elections_region(self):
        return self._get_upper_level_from_area(self.opdm_membership.area, level='REG')

    @property
    def lower_member_reference(self):
        if self.replaced_member:
            return self.replaced_member.lower_member_reference
        else:
            return self

    @property
    def upper_member_reference(self):
        if hasattr(self, 'replaced_membership_parliament'):
            return self.replaced_membership_parliament.upper_member_reference
        else:
            return self

    @property
    def election_macro_area(self):
        if self.election_area:
            res = self.election_area.parent
            if not res:
                return 'Estero'
            return res.name

    def _days_in_parliament(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        """Returns the days the member was in the parliament historically, both Senate and Deputy together"""

        ed_cast = Cast(Coalesce(F('end_date'), Value(today_strf)), output_field=DateField())
        sd_cast = Cast(F('start_date'), output_field=DateField())
        duration = ExpressionWrapper(F('ed') - F('sd'), output_field=fields.DurationField())
        hist_parl = OPDMMembership.objects.filter(person=self.opdm_membership.person).filter(
            Q(label__icontains='senatore') | Q(label__icontains='deputato'))
        res = hist_parl.annotate(sd=sd_cast, ed=ed_cast).annotate(duration=duration).aggregate(
            n_days_parliament=Sum(duration))
        return res.get('n_days_parliament')

    @property
    def parse_days_in_parliament(self):
        return parse_days(self.days_in_parliament.days)

    @property
    def _days_in_related_parliament(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        start_date =  datetime.datetime.strptime(self.opdm_membership.start_date, '%Y-%m-%d')
        end_date = datetime.datetime.strptime((self.opdm_membership.end_date or today_strf), '%Y-%m-%d')
        return end_date - start_date

    @property
    def parse_days_in_related_parliament(self):
        import math
        days_p = self._days_in_related_parliament.days
        res = []
        if (years := math.floor(days_p / 365)):
            if years > 1:
                res.append(f"{years} anni")
            else:
                res.append(f"{years} anno")
        if (months := math.floor((days_p % 365) / 30)):
            if months > 1:
                res.append(f"{months} mesi")
            else:
                res.append(f"{months} mese")
        if len(res) == 0:
            return f"{days_p} giorni"

        elif len(res) == 1:
            return ' '.join(res)

        else:
            all_but_last = ' '.join(res[:-1])
            last = res[-1]
            return ' e '.join([all_but_last, last])

    def member_supports_majority_at_date(self, date):
        mm = self.majority_periods.filter(
            Q(start_date__lte=date) & (Q(end_date__gte=date) | Q(end_date__isnull=True))
        )
        if mm:
            return mm[0].value
        else:
            return None

    @classmethod
    def get_membership_date_assembly(cls, assembly, date):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        return cls.objects.annotate(new_end_date=Coalesce(F('opdm_membership__end_date'),
                                                          Value(today.strftime('%Y-%m-%d')))) \
            .filter(opdm_membership__start_date__lt=date,
                    new_end_date__gt=date,
                    opdm_membership__organization=assembly)


    @property
    def other_election_parl(self):
        return self.opdm_membership.person.memberships.filter(Q(role__iexact='Senatore a vita')|Q(role__iexact='Senatore')|Q(role__iexact='Deputato')).exclude(
            id=self.opdm_membership.id).filter(start_date__lt=self.opdm_membership.start_date)
    @property
    def branch(self):
        if self.opdm_membership.role[0] == 'D':
            return 'C'
        else:
            return self.opdm_membership.role[0]

    @property
    def get_positional_power(self):
        return pd.DataFrame(self.opdm_membership.organization.opp_assembly.positional_power())

    @property
    def pp_total_ordering(self):
        return self.get_positional_power.shape[0]

    def get_pp(self):

        from project.opp.metrics.models import PositionalPower
        chamber_members_values = PositionalPower.get_chamber_leg() \
            .filter(end_date__isnull=True)
        if self.branch == 'S':
            denom_chamber = chamber_members_values.filter(
                Q(opdm_membership__organization__parent__name__icontains='Senato')|Q(
                    opdm_membership__organization__parent__name__icontains='Parlamento Italiano',
                        opdm_membership__person__in=Membership.objects.filter(
                            opdm_membership__organization=self.opdm_membership.organization)\
                        .values_list('opdm_membership__person_id', flat=True))) \
                .aggregate(denom=Sum('value')).get('denom')
        else:
            denom_chamber = chamber_members_values.filter(
                Q(opdm_membership__organization__parent__name__icontains='Camera')|Q(
                    opdm_membership__organization__parent__name__icontains='Parlamento Italiano',
                        opdm_membership__person__in=Membership.objects.filter(
                            opdm_membership__organization=self.opdm_membership.organization)\
                        .values_list('opdm_membership__person_id', flat=True))) \
                .aggregate(denom=Sum('value')).get('denom')

        pp_branch = ((100 * chamber_members_values.filter(opdm_membership__person_id=self.opdm_membership.person.id)
                      .aggregate(
            num=Sum('value')).get('num') / denom_chamber)) \
            if not self.opdm_membership.end_date else None

        ordering_df = self.get_positional_power
        return (
        round(pp_branch, 2), ordering_df[ordering_df['person'] == self.opdm_membership.person.id]['order'].iloc[0])

    def set_pp(self):
        return self.get_pp()[0]

    def set_pp_ordering(self):
        return self.get_pp()[1]

    def save(self, *args, **kwargs):
        self.election_area = self._get_elections_region()
        self.is_active = self.opdm_membership.end_date is None
        self.perc_fidelity = self.fidelity_index
        if not self.opdm_membership.end_date:
            try:
                self.pp = self.set_pp()
                self.pp_ordering = self.set_pp_ordering()
            except Exception:
                pass
        super(Membership, self).save(*args, **kwargs)

    def __str__(self):
        return f'{self.opdm_membership.person.name}'

    class Meta:
        ordering = ('-pp','id')


class Voting(Timestampable, SourceShortcutsMixin, LinkShortcutsMixin, models.Model):
    """The collection of votes on a given parliamentary act"""

    objects = LegVotingManager()

    sitting = models.ForeignKey(
        to=Sitting,
        on_delete=models.PROTECT,
        related_name="votings",
        help_text=_("The sitting ")
    )
    parliamentary_act_uris = GenericRelation(
        to=LinkRel,
        verbose_name=_("parliamentary act URIs"),
        help_text=_("URIs to linked parliamentary acts")
    )
    number = models.PositiveSmallIntegerField(
        _("sequential number"),
        help_text=_("Voting sequential number in the sitting"),
    )
    identifier = models.CharField(
        _("identifier"),
        max_length=512, blank=True, null=True,
        help_text=_("The unique URI"),
        unique=True
    )
    sources = GenericRelation(
        to=SourceRel,
        verbose_name=_("sources"),
        help_text=_("URLs to source documents")
    )
    links = GenericRelation(
        to=LinkRel,
        verbose_name=_("links"),
        help_text=_("URLs to linked documents")
    )
    original_title = models.TextField(
        _("original title"),
        help_text=_("Title, as it comes from the data")
    )
    description_title = models.TextField(
        _("Description title"),
        help_text=_("Description title, as it comes from the data"),
        blank=True, null=True
    )
    public_title = models.CharField(
        _("public title"),
        max_length=1024,
        blank=True, null=True,
        help_text=_("Title, as it's known publicly")
    )
    n_present = models.PositiveSmallIntegerField(
        _("n. of present voters"),
        help_text=_("Number of voters present to the votation"),
        blank=True, null=True
    )
    n_voting = models.PositiveSmallIntegerField(
        _("n. of  voting voters"),
        help_text=_("Number of voters taking part to the votation"),
        blank=True, null=True
    )
    n_majority = models.PositiveSmallIntegerField(
        _("majority threshold"),
        help_text=_("Number of voters needed to reach the majority"),
        blank=True, null=True
    )
    n_absent = models.PositiveSmallIntegerField(
        _("n. of absent voters"),
        help_text=_("Number of voters absent to the votation"),
        blank=True, null=True
    )
    n_mission = models.PositiveSmallIntegerField(
        _("n. of mission voters"),
        help_text=_("Number of voters in mission during the votation"),
        blank=True, null=True
    )
    n_abstained = models.PositiveSmallIntegerField(
        _("n. of absteined voters"),
        help_text=_("Number of voters who abstained from voting"),
        blank=True, null=True
    )
    n_ayes = models.PositiveSmallIntegerField(
        _("n. of for voters in favor"),
        help_text=_("Number of votes in favor"),
        blank=True, null=True
    )
    n_nos = models.PositiveSmallIntegerField(
        _("n. of against voters"),
        help_text=_("Number of votes against"),
        blank=True, null=True
    )
    n_present_not_voting = models.PositiveSmallIntegerField(
        _("n. of present and not voting"),
        help_text=_("Number of votes present and not voting"),
        blank=True, null=True
    )
    n_requiring_not_voting = models.PositiveSmallIntegerField(
        _("n. of requiring and not voting"),
        help_text=_("Number of requiring and not voting"),
        blank=True, null=True
    )
    n_not_voting = models.PositiveSmallIntegerField(
        _("n. of non voting"),
        help_text=_("Number of people not voting whether,"
                    "absent or declaring not voting (excluded in mission)"),
        blank=True, null=True
    )
    n_rebels = models.PositiveSmallIntegerField(
        _("n. of rebel voters"),
        help_text=_("Number of voters rebelling against their group"),
        blank=True, null=True
    )
    n_margin = models.PositiveSmallIntegerField(
        _("margin above the majority threshold"),
        help_text=_("Number of votes above the needed majority"),
        blank=True, null=True
    )
    is_final = models.BooleanField(
        _("Is final"),
        default=False,
        help_text=_("If the voting was a final one")
    )
    is_confidence = models.BooleanField(
        _("Is confidence"),
        default=False,
        help_text=_("If the voting was a confidence one")
    )
    is_majority_saved = models.BooleanField(
        _("is majority saved"),
        null=True, blank=True,
        default=None,
        help_text=_("If the voting was one in which the majority was seved by votes from the opposition")
    )
    RESPINTO = 'Respinto'
    APPROVATO = 'Approvato'
    OUTCOME_TYPES = [
        (RESPINTO, _('Respinta')),
        (APPROVATO, _('Approvata')),
    ]

    outcome = models.CharField(
        _("outcome"),
        max_length=10,
        help_text=_("Outcome of the voting"),
        choices=OUTCOME_TYPES

    )

    is_secret = models.BooleanField(
        _("is voting secret"),
        default=False,
        help_text=_("If the voting is secret")
    )
    is_key_vote = models.BooleanField(
        _("is a key vote"),
        default=False,
        help_text=_("If the voting is important")
    )

    majority_cohesion_rate = models.FloatField(
        _("Majority cohesion rate"),
        help_text=_("Majority cohesion rate for this vote, from 0 to 100"),
        validators=[MinValueValidator(0.0), MaxValueValidator(100.0)],
        blank=True, null=True
    )

    minority_cohesion_rate = models.FloatField(
        _("Minority cohesion rate"),
        help_text=_("Minority cohesion rate for this vote, from 0 to 100"),
        validators=[MinValueValidator(0.0), MaxValueValidator(100.0)],
        blank=True, null=True
    )
    president = models.ForeignKey(
        to=Membership,
        on_delete=models.PROTECT,
        related_name="voting_as_president",
        help_text=_("President at the vote"),
        blank=True, null=True
    )

    bills = models.ManyToManyField(
        to=Bill,
        related_name="votings",
        blank=True,
    )

    ALL_ACT = 'ALL'
    ARTICLE = 'ART'
    AMENDMENT = 'AMEND'
    ODG = 'ODG'
    PREJUDICIAL = 'PREJ'
    EMERG_DECLARATION = 'EM_DECL'
    SUSPENSIVE = 'SUSP'
    VARIATION_NOTE = 'VAR_N'
    DONT_PROCEED = 'DONT_P'
    CLOSING_DEBATE = 'CLOS_DEB'
    MOTION = 'MOZ'
    RESOLUTION = 'RES'
    RESIGNATION = 'DIM'
    ELECTION = 'ELE'
    COORD = 'COORD'
    COUNCIL_PROPOSAL = 'CPROP'
    VOTE_TYPES = [
        (ALL_ACT, 'Atto intero'),
        (ARTICLE, 'Articolo'),
        (AMENDMENT, 'Emendamenti'),
        (ODG, 'ODG'),
        (PREJUDICIAL, 'Questione pregiudiziale'),
        (EMERG_DECLARATION, 'Dichiarazione d\'urgenza'),
        (SUSPENSIVE, 'Questione sospensiva'),
        (VARIATION_NOTE, 'Nota di variazione'),
        (DONT_PROCEED, 'Non passaggio agli articoli'),
        (CLOSING_DEBATE, 'Chiusura della discussione'),
        (RESOLUTION, 'Risoluzione'),
        (RESIGNATION, 'Dimissioni'),
        (ELECTION, 'Elezioni'),
        # (COM, 'Comunicazioni'),
        (COORD, 'Proposta coordinamento'),
        (MOTION, 'Mozione'),
        (COUNCIL_PROPOSAL, 'Proposta giunta')
    ]

    type = models.CharField(
        _("type"),
        max_length=10,
        help_text=_("Type of the voting"),
        choices=VOTE_TYPES,
        null=True,
        blank=True
    )

    @property
    def get_gov(self):
        from project.opp.gov.models import Government
        return Government.get_government_by_date(self.sitting.date)

    @property
    def title_compact(self):
        description = ""

        if self.public_title:
            return self.public_title
        if self.description_title:
            description = f"{self.description_title} - "
        return f"{description}{self.original_title}"

    def groups_votes_joined(self):
        """Return groups' votes for this votation, joined with the
        group and organization classes, to avoid further queries when serialising."""
        date = self.sitting_date
        return self.groups_votes.select_related(
            'group__organization'
        ).annotate(voting_date=Value(date))

    def members_votes_filtered(self):
        """Return members' votes for this votation, filtered so that
        membership to majority and to groups are related to the time of the sitting."""
        vd = self.sitting.date
        if not db_cache.get(f'voting_members:{self.id}'):
            members_votes = self.members_votes.select_related(
                'membership',
                'membership__opdm_membership',
                'membership__opdm_membership__person',
            ).prefetch_related(
                Prefetch(
                    'membership__election_area',
                    queryset=Area.objects.filter(classification__in=['ADM1', 'ELECT_RIP']).only('name',
                                                                                                'parent_id')),
                Prefetch(
                    'membership__election_area__parent',
                    queryset=Area.objects.filter(classification__in=['RGNE', ]).only('id', 'name')),
                Prefetch(
                    'membership__majority_periods',
                    queryset=MemberMajority.objects.select_related('government').filter(
                        Q(start_date__lte=vd) & (Q(end_date__gte=vd) | Q(end_date__isnull=True))
                    ),
                    to_attr='membership_majority_periods_filtered'
                ),
                Prefetch(
                    'membership__membership_groups',
                    queryset=MembershipGroup.objects.select_related('group__organization').filter(
                        Q(start_date__lte=vd) & (Q(end_date__gte=vd) | Q(end_date__isnull=True))
                    ),
                    to_attr='membership_groups_filtered'
                ),

            )

            db_cache.set(
                f'voting_members:{self.id}',
                {'res': list(members_votes)},
                timeout=60*60*60*24
            )
        return db_cache.get(f'voting_members:{self.id}').get('res')

    @property
    def sitting_date(self):
        return self.sitting.date

    def sitting_branch(self):
        return self.sitting.assembly

    @property
    def related_votings(self):
        return Voting.objects.filter(bills__in=self.bills.all()).exclude(id=self.id) \
            .annotate(pk2=F('identifier')) \
            .order_by('-sitting__date', )

    sitting_branch.short_description = 'Assembly'
    sitting_branch = property(sitting_branch)

    def check_voting_members(self):

        memb_aye = MemberVote.objects.filter(voting=self.id, vote=MemberVote.AYE).count()
        memb_no = MemberVote.objects.filter(voting=self.id, vote=MemberVote.NO).count()
        memb_abst = MemberVote.objects.filter(voting=self.id, vote=MemberVote.ABSTAINED).count()
        memb_abse = MemberVote.objects.filter(voting=self.id, vote=MemberVote.ABSENT).count()
        memb_mis = MemberVote.objects.filter(voting=self.id, vote=MemberVote.MISSION).count()
        memb_pres = MemberVote.objects.get(voting=self.id, vote=MemberVote.PRESIDENT)

        df = pd.DataFrame(columns=['type', 'yes', 'no', 'abstained', 'absent', 'mission', 'president'])

        df.loc[len(df)] = ['Voting',
                           self.n_ayes,
                           self.n_nos,
                           self.n_abstained,
                           self.n_absent,
                           self.n_mission,
                           self.president.id]
        df.loc[len(df)] = ['MembersAggregate', memb_aye, memb_no, memb_abst, memb_abse, memb_mis,
                           memb_pres.membership.id]
        df.loc[-1] = df.nunique(axis=0)
        df.loc[-1] = df.loc[-1].apply(lambda x: x == 1)
        return df

    @classmethod
    def between_dates(cls, sd, ed):
        return cls.objects.filter(sitting__date__lt=ed, sitting__date__gte=sd)

    @property
    def legislature(self):
        return self.sitting \
                   .assembly.key_events \
                   .prefetch_related('key_event') \
                   .get(key_event__identifier__startswith='ITL').key_event.identifier[-2:]

    class Meta:
        verbose_name = _('Voting')
        verbose_name_plural = _('Votings')
        ordering = ("-sitting__date",)

    def save(self, *args, **kwargs):
        if not self.identifier:
            self.identifier = f'manual_{self.sitting.get_legislature}_{self.sitting.number}_{self.number}'
        super(Voting, self).save(*args, **kwargs)

    def __str__(self):
        return self.identifier


class MemberMajority(Dateframeable, models.Model):
    """Historic records of support for majority, by single member
    the support is a nullable boolean value, where null means "Don't know"
    """
    membership = models.ForeignKey(
        to=Membership,
        on_delete=models.CASCADE,
        related_name="majority_periods"
    )
    value = models.BooleanField(null=True)

    government = models.ForeignKey(
        to=Organization,
        on_delete=models.CASCADE,
        related_name="majority_members_support",
        limit_choices_to={'classification__in': ["Governo della Repubblica", ]},
        blank=True, null=True
    )

    @property
    def branch(self):
        return self.membership.branch

    @classmethod
    def sustain_chamber(cls, gov):
        return cls.objects.select_related('membership') \
            .filter(government=gov, end_date__isnull=True,
                    membership__opdm_membership__role__istartswith='D')

    @classmethod
    def sustain_senate(cls, gov):
        return cls.objects.select_related('membership') \
            .filter(government=gov, end_date__isnull=True,
                    membership__opdm_membership__role__istartswith='S')

    class Meta:
        verbose_name = _("Sostegno parlamentare governo")
        verbose_name_plural = _("Sostegno parlamentare governo")
        ordering = ('government__start_date',
                    '-value',
                    'membership__opdm_membership__start_date',
                    'membership__opdm_membership__person__name')

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        rev = reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        return f'{Site.objects.first().name}{rev}'

    def __str__(self):

        if self.value is True:
            case = 'sostiene'
        elif self.value is False:
            case = 'non sostiene'
        else:
            case = 'non valutabile'

        return f"{self.government.name} - {self.membership.opdm_membership.person.name} ({case})"


class MemberVote(models.Model):
    """A> single vote by a Membership"""

    objects = MemberVoteManager()
    voting = models.ForeignKey(
        to=Voting,
        on_delete=models.CASCADE,
        related_name="members_votes"
    )
    membership = models.ForeignKey(
        to=Membership,
        on_delete=models.CASCADE,
        related_name="votes"
    )
    is_rebel = models.BooleanField(
        _("is rebel"),
        default=False,
        help_text=_("If this was a rebel vote (against the group)")
    )
    is_fidelity = models.BooleanField(
        _("is fidelity"),
        default=False,
        help_text=_("If voted exactly the same as group")
    )

    @property
    def member_group_at_vote(self):
        mg = self.membership.membership_groups_filtered  # noqa
        if mg:
            return mg[0].group
        else:
            return None

    @property
    def member_group(self):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        return self.membership.membership_groups.annotate(
            end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
            .get(start_date__lte=self.voting.sitting.date, end_date_coalesce__gte=self.voting.sitting.date).group

    @property
    def group_vote(self):
        return GroupVote.objects.get(voting=self.voting, group=self.member_group).vote

    @property
    def member_supports_majority_at_vote(self):
        mm = self.membership.membership_majority_periods_filtered  # noqa
        if mm:
            return mm[0].value
        else:
            return None

    @property
    def member_area_election(self):
        mm = self.membership.area_name  # noqa
        return mm

    @property
    def is_president_official(self):
        """

        :return: if member was official president during votation
        :rtype bool
        """
        if self.vote == 'PRES':
            data_votazione = self.voting.sitting.date
            return self.membership.is_official_president_given_date(data_votazione)
        else:
            return None

    AYE = 'AYE'
    NO = 'NO'
    ABSENT = 'ABSE'
    ABSTAINED = 'ABST'
    MISSION = 'MIS'
    PRESENT_NOT_VOTING = 'PNV'
    REQUIRING_NOT_VOTING = 'RNV'
    PRESIDENT = 'PRES'
    SECRET = 'SEC'
    VOTE_TYPES = [
        (AYE, _('Aye')),
        (NO, _('No')),
        (ABSENT, _('Absent')),
        (ABSTAINED, _('Abstained')),
        (MISSION, _('Mission')),
        (PRESENT_NOT_VOTING, _('Present and not voting')),
        (REQUIRING_NOT_VOTING, _('Requiring votation and not voting')),
        (PRESIDENT, _('President on duty')),
        (SECRET, _('Secret vote')),
    ]
    vote = models.CharField(
        _("vote"),
        max_length=4,
        choices=VOTE_TYPES,
        help_text=_("The vote expressed"),
        db_index=True
    )

    def __str__(self):
        return f'{self.membership.__str__()} @ {self.voting.__str__()}'

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['membership', 'voting'], name='unique_membership_voting'
            )
        ]
        ordering = ('-voting__sitting__date', 'membership__opdm_membership__person__family_name',
                    'membership__opdm_membership__person__given_name')


class GroupVote(models.Model):
    """A vote by a Group"""
    objects = GroupVoteManager()
    voting = models.ForeignKey(
        to=Voting,
        on_delete=models.CASCADE,
        related_name="groups_votes"
    )
    group = models.ForeignKey(
        to=Group,
        on_delete=models.CASCADE,
        related_name="votes"
    )
    cohesion_rate = models.FloatField(
        _("cohesion rate"),
        help_text=_("A cohesion rate for this vote, from 0 to 100"),
        validators=[MinValueValidator(0.0), MaxValueValidator(100.0)],
        blank=True, null=True
    )

    AYE = 'AYE'
    NO = 'NO'
    ABSTAINED = 'ABST'
    SECRET = 'SEC'
    NOT_VOTING = 'NVOT'
    NOT_VALUABLE = 'NV'
    VOTE_TYPES = [
        (AYE, 'Favorevole'),
        (NO, 'Contrario'),
        (ABSTAINED, 'Astenuto'),
        (NOT_VALUABLE, 'Non valutabile'),
        (SECRET, 'Voto segreto'),
        (NOT_VOTING, 'Non votante')
    ]
    vote = models.CharField(
        _("vote"),
        max_length=4,
        choices=VOTE_TYPES,
        help_text=_("The vote expressed")
    )
    vote_value = models.PositiveSmallIntegerField(
        _("Cache for value of vote"),
        help_text=_("Cache for value of vote"),
        blank=True, null=True
    )
    n_group_total = models.IntegerField(
        _("n. of members of group on the date of voting"),
        help_text=_("Number of members on the date of voting"),
        blank=True, null=True
    )
    n_present = models.PositiveSmallIntegerField(
        _("n. of present voters"),
        help_text=_("Number of voters present to the votation"),
        blank=True, null=True
    )
    n_voting = models.PositiveSmallIntegerField(
        _("n. of  voting voters"),
        help_text=_("Number of voters taking part to the votation"),
        blank=True, null=True
    )
    n_abstained = models.PositiveSmallIntegerField(
        _("n. of absteined voters"),
        help_text=_("Number of voters who abstained from voting"),
        blank=True, null=True
    )
    n_absent = models.PositiveSmallIntegerField(
        _("n. of absent voters"),
        help_text=_("Number of voters absent to the votation"),
        blank=True, null=True
    )
    n_mission = models.PositiveSmallIntegerField(
        _("n. of mission voters"),
        help_text=_("Number of voters in mission during the votation"),
        blank=True, null=True
    )
    n_ayes = models.PositiveSmallIntegerField(
        _("n. of for voters"),
        help_text=_("Number of votes in favor"),
        blank=True, null=True
    )
    n_nos = models.PositiveSmallIntegerField(
        _("n. of against voters"),
        help_text=_("Number of votes against"),
        blank=True, null=True
    )
    n_present_not_voting = models.PositiveSmallIntegerField(
        _("n. of present and not voting"),
        help_text=_("Number of votes present and not voting"),
        blank=True, null=True
    )
    n_requiring_not_voting = models.PositiveSmallIntegerField(
        _("n. of requiring and not voting"),
        help_text=_("Number of requiring and not voting"),
        blank=True, null=True
    )
    n_not_voting = models.PositiveSmallIntegerField(
        _("n. of non voting in group"),
        help_text=_("Number of people not voting in the group whether,"
                    "absent or declaring not voting (excluded in mission)"),
        blank=True, null=True
    )
    n_rebels = models.PositiveSmallIntegerField(
        _("n. of rebel voters"),
        help_text=_("Number of voters rebelling against their group"),
        blank=True, null=True
    )
    has_president = models.BooleanField(
        _("Has president"),
        help_text=_("If a member of the group is president"),
        default=False
    )
    supports_majority = models.BooleanField(
        null=True,
        help_text=_("Whether the group currently supports the majority")
    )

    @staticmethod
    def get_membri_gruppo(group, voting):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        """Returns the number of member group during voting (excluding official President of Senate/Deputy Chamber
        :param group
        :param votazione
        :return: count of group memebrs
        :rtype int
        """
        data_votazione = voting.sitting.date
        official_presidents_ids = Membership \
            .objects \
            .values('opdm_membership__role',
                    'opdm_membership__person__memberships__role') \
            .annotate(role_2_new_end_date=Coalesce(F('opdm_membership__person__memberships__end_date'),
                                                   Value(today_strf)),
                      role_1_new_end_date=Coalesce(F('opdm_membership__end_date'),
                                                   Value(today_strf)),
                      other_roles=F('opdm_membership__person__memberships__role'),
                      other_roles_sd=F('opdm_membership__person__memberships__start_date')) \
            .filter(other_roles="Presidente Organo di presidenza parlamentare",
                    other_roles_sd__lte=data_votazione,
                    role_2_new_end_date__gte=data_votazione,
                    opdm_membership__start_date__lte=data_votazione,
                    role_1_new_end_date__gte=data_votazione).distinct('id').values('id').order_by('id')

        if group.is_meta_group:
            membri_gruppo = MemberMajority.objects.annotate(
                new_end_date=Coalesce(F('end_date'),
                                      Value(today_strf))) \
                .filter(membership__opdm_membership__organization=voting.sitting.assembly,
                        value=group.supports_majority,
                        start_date__lte=data_votazione,
                        new_end_date__gte=data_votazione) \
                .exclude(membership__id__in=official_presidents_ids)
        else:
            membri_gruppo = MembershipGroup.objects.annotate(
                new_end_date=Coalesce(F('end_date'), Value(today_strf))) \
                .filter(group=group, start_date__lte=data_votazione, new_end_date__gte=data_votazione) \
                .exclude(membership__id__in=official_presidents_ids)
        return membri_gruppo

    def __str__(self):
        return f"{self.group.acronym} @ {self.voting}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['group', 'voting'], name='unique_group_voting'
            )
        ]


class GroupMajority(Dateframeable, models.Model):
    """Historic records of support for majority, by single member
    the support is a nullable boolean value, where null means "Don't know"
    """
    group = models.ForeignKey(
        to=Group,
        on_delete=models.CASCADE,
        related_name="majority_periods"
    )
    government = models.ForeignKey(
        to=Organization,
        on_delete=models.CASCADE,
        related_name="majority_groups_support",
        limit_choices_to={'classification__in': ["Governo della Repubblica", ]},
        blank=True, null=True
    )
    value = models.BooleanField(null=True)

    class Meta:
        verbose_name = _("Sostegno governo gruppo")
        verbose_name_plural = _("Sostegno governo gruppi")
        ordering = ('government__start_date',
                    '-value',
                    'group__organization__start_date',
                    'group__organization__name')
        constraints = [
            models.UniqueConstraint(
                fields=['group', 'start_date'], name='unique_group_start_date_majority'
            )
        ]

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        rev = reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        return f'{Site.objects.first().name}{rev}'

    def __str__(self):

        if self.value is True:
            case = 'sostiene'
        elif self.value is False:
            case = 'non sostiene'
        else:
            case = 'non valutabile'

        return f"{self.government.name} - {self.group} ({case})"


class MembershipGroup(Dateframeable, models.Model):
    membership = models.ForeignKey(
        to=Membership,
        on_delete=models.CASCADE,
        related_name="membership_groups"
    )
    group = models.ForeignKey(
        to=Group,
        on_delete=models.CASCADE,
        related_name="membership_groups"
    )
    n_present = models.PositiveSmallIntegerField(
        _("n. of times member was present"),
        help_text=_("Number of times member was present"),
        blank=True, null=True
    )
    n_voting = models.PositiveSmallIntegerField(
        _("n. of the member votes"),
        help_text=_("Number of times member votes"),
        blank=True, null=True
    )
    n_abstained = models.PositiveSmallIntegerField(
        _("n. of absteined votes"),
        help_text=_("Number of absteined votes"),
        blank=True, null=True
    )
    n_absent = models.PositiveSmallIntegerField(
        _("n. of absent voters"),
        help_text=_("Number of voters absent to the votation"),
        blank=True, null=True
    )
    n_mission = models.PositiveSmallIntegerField(
        _("n. of times in mission"),
        help_text=_("Number of times member was in mission"),
        blank=True, null=True
    )
    n_ayes = models.PositiveSmallIntegerField(
        _("n. of for yes votes"),
        help_text=_("Number of times member voted yes"),
        blank=True, null=True
    )
    n_nos = models.PositiveSmallIntegerField(
        _("n. of no votes"),
        help_text=_("Number of times member voted no"),
        blank=True, null=True
    )
    n_present_not_voting = models.PositiveSmallIntegerField(
        _("n. of times present and not voting"),
        help_text=_("Number of times present and not voting"),
        blank=True, null=True
    )
    n_requiring_not_voting = models.PositiveSmallIntegerField(
        _("n. of times requiring and not voting"),
        help_text=_("Number of times requiring and not voting"),
        blank=True, null=True
    )
    n_not_voting = models.PositiveSmallIntegerField(
        _("n. of times non voting"),
        help_text=_("Number of times member not voted in the group whether,"
                    "absent or declaring not voting (excluded in mission)"),
        blank=True, null=True
    )
    n_rebels = models.PositiveSmallIntegerField(
        _("n. of rebel votes"),
        help_text=_("Number of rebelling votes against the group"),
        blank=True, null=True
    )
    n_fidelity = models.PositiveSmallIntegerField(
        _("n. of fidelity voters inside group"),
        help_text=_("Number of votes same as group"),
        blank=True, null=True
    )
    n_president = models.PositiveSmallIntegerField(
        _("n. of times member was president during vote"),
        help_text=_("Number of times member was president during vote"),
        blank=True, null=True
    )

    @property
    def previous_group(self):
        return MembershipGroup.objects.filter(group__legislature=self.group.legislature, membership=self.membership)\
            .exclude(id=self.id).filter(start_date__lt=self.start_date).exclude(group__acronym='Nessun gruppo').order_by('-start_date').first()

    def __str__(self):
        return f"{self.membership.opdm_membership.person.name} - {self.group}"

    class Meta:
        verbose_name = _("Membri gruppo")
        verbose_name_plural = _("Membri gruppi")
        ordering = ('start_date',)


class GroupsMetricsHST(models.Model):
    """Historical cache for groups and meta-groups metrics"""
    group = models.ForeignKey(
        to=Group,
        on_delete=models.CASCADE,
        related_name='historic_metrics'
    )
    month = models.PositiveSmallIntegerField(
        _("Month"),
        help_text=_("Month")
    )
    year = models.IntegerField(
        _("Year"),
        help_text=_("Year"),
    )
    n_voting = models.IntegerField(
        _("N Voting"),
        help_text=_("Number of votings"),
        blank=True,
        null=True
    )
    cohesion_rate = models.FloatField(
        _("Avg cohesion rate"),
        help_text=_("Average cohesion rate for this group during year-month"),
        validators=[MinValueValidator(0.0), MaxValueValidator(100.0)],
        blank=True, null=True
    )

    cohesion_rate_overall = models.FloatField(
        _("Avg cohesion rate"),
        help_text=_("Average cohesion rate for this group during from start to year-month"),
        validators=[MinValueValidator(0.0), MaxValueValidator(100.0)],
        blank=True, null=True
    )
    perc_present = models.PositiveSmallIntegerField(
        _("% of present voters"),
        help_text=_("Percentage of voters present during year-month"),
        blank=True, null=True
    )
    perc_absent = models.PositiveSmallIntegerField(
        _("% of absent voters"),
        help_text=_("Percentage of voters absent during year-month"),
        blank=True, null=True
    )
    perc_mission = models.PositiveSmallIntegerField(
        _("% of mission voters"),
        help_text=_("Percentage of voters in mission during year-month"),
        blank=True, null=True
    )
    n_rebels = models.IntegerField(
        _("n. of rebel voters"),
        help_text=_("Number of voters rebelling against their group"),
        blank=True, null=True
    )

    def __str__(self):
        return f'{self.group.__str__()} - {self.year}/{self.month}'

    class Meta:
        verbose_name = _("Storico metriche gruppo")
        verbose_name_plural = _("Storico metriche gruppi")
        constraints = [
            models.UniqueConstraint(
                fields=['group', 'year', 'month'], name='unique_groupmetricshst_group_year_month'
            )
        ]


class MembershipsMetricsHST(models.Model):
    """Historical cache for groups and meta-groups metrics"""
    membership = models.ForeignKey(
        to=Membership,
        on_delete=models.CASCADE,
        related_name="historical_metrics"
    )
    month = models.PositiveSmallIntegerField(
        _("Month"),
        help_text=_("Month")
    )
    year = models.PositiveSmallIntegerField(
        _("Year"),
        help_text=_("Year"),
    )
    n_voting = models.PositiveSmallIntegerField(
        _("n. of votings"),
        help_text=_("Number of votings members"),
        blank=True,
        null=True
    )
    n_present = models.PositiveSmallIntegerField(
        _("n. of present votes"),
        help_text=_("Number of voters present during year-month"),
        blank=True, null=True
    )
    n_absent = models.PositiveSmallIntegerField(
        _("n. of absent votes"),
        help_text=_("Number of voters absent during year-month"),
        blank=True, null=True
    )
    n_mission = models.PositiveSmallIntegerField(
        _("n. of mission votes"),
        help_text=_("Number of voters in mission during year-month"),
        blank=True, null=True
    )
    n_rebels = models.PositiveSmallIntegerField(
        _("n. of rebel votes"),
        help_text=_("Number of voters rebelling against their group"),
        blank=True, null=True
    )
    n_fidelity = models.PositiveSmallIntegerField(
        _("n. of fidelity votes"),
        help_text=_("Number of votes same as group given month-yea"),
        blank=True, null=True
    )

    class Meta:
        verbose_name = _("Storico metriche parlamentare")
        verbose_name_plural = _("Storico metriche parlamentari")
