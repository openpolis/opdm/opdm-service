import logging
import os
from project.opp.votes.management.commands.utils.load_manual_votes import LoadManualVotes
from django_rq import job
from rq import Queue, get_current_job
import time
from redis import StrictRedis
import slack
import environ
from config.settings.base import DOMAIN


env = environ.Env()

staging_suffix = ''
if 'staging' in DOMAIN:
    staging_suffix = 'staging'

conn = StrictRedis()
q = Queue('default', connection=conn)

logger = logging.getLogger(__name__)


@job
def generate_voting(df, voting):
    """Generate a GDocs receipt, and download it into the model"""
    # voting.save()
    time.sleep(5)
    return {'df': LoadManualVotes(df=df, voting=voting).load(),
            'voting': voting
            }


@job
def send_notification_slack():
    """Send an email whose content to the new subscriber, with the invoice attached, if required"""
    slack_client = slack.WebClient(token=env.str("UWSGI_TASKMANAGER_NOTIFICATIONS_SLACK_TOKEN", ''))
    current_job = get_current_job(conn)
    result_dependency = current_job.dependency.result
    df_det = result_dependency.get('df')
    voting = result_dependency.get('voting')
    df_gen = voting.check_voting_members()
    df_det.to_html(f'data/voting_manual_added_check_{voting.identifier}_det.html')
    df_gen.to_html(f'data/voting_manual_added_check_{voting.identifier}_gen.html')

    blocks = [
        {
            "type": "context",
            "elements": [{"type": "mrkdwn", "text": "django-uwsgi-taskmanager", }],
        },
        {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": (
                    "File check"
                ),
            },
        },
    ]

    slack_client.files_upload(channels=f'django-uwsgi-taskmanager-notifications{staging_suffix}',
                              file=f'data/voting_manual_added_check_{voting.identifier}_det.html',
                              blocks=blocks)
    slack_client.files_upload(channels=f'django-uwsgi-taskmanager-notifications{staging_suffix}',
                              file=f'data/voting_manual_added_check_{voting.identifier}_gen.html',
                              blocks=blocks)
    os.remove(f"data/voting_manual_added_check_{voting.identifier}_det.html")
    os.remove(f"data/voting_manual_added_check_{voting.identifier}_gen.html")
