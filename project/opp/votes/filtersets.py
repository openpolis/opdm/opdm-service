from django.utils.translation import ugettext as _
from django.db.models import F, Q
from django_filters import CharFilter
from django_filters.rest_framework import (
    FilterSet,
    ChoiceFilter,
    ModelChoiceFilter,
    # CharFilter,
    # NumberFilter,
    # LookupChoiceFilter,
    # BooleanFilter, BaseInFilter,
)

from popolo.models import Organization, KeyEvent

from project.opp.acts.models import BillSigner, GovDecree
from project.opp.gov.models import Government
from project.opp.home.models import Event
from project.opp.votes.models import Voting, Membership, Group, MemberVote, GroupVote
import datetime

def get_years(request=None):
    if request is None:
        return []

    leg = request.parser_context['kwargs'].get('legislature', None)
    today = datetime.datetime.now()
    today_strf = today.strftime('%Y-%m-%d')

    legislature_identifier = f"ITL_{leg}"
    e = KeyEvent.objects.get(identifier=legislature_identifier)
    qs = Event.objects.filter(date__date__gte=e.start_date, date__date__lte=(e.end_date or today)).annotate(
        y=F("date__year")).order_by('-y')

    return qs.values_list('y', 'y').distinct().order_by('-y')


def get_election_areas(request=None):
    if request is None:
        return []

    regional_cases = list(Membership.objects.filter(election_area__isnull=False)
                          .values_list('election_area', 'election_area__name')
                          .distinct()
                          .exclude(election_area__classification='ELECT_RIP')
                          .order_by('election_area__name'))
    regional_cases.extend([['Estero', 'Estero'], ['no_area', 'No area']])
    return regional_cases


def get_election_macro_areas(request=None):
    if request is None:
        return []

    rip_cases = list(Membership.objects.filter(election_area__isnull=False)
                     .values_list('election_area__parent', 'election_area__parent__name')
                     .distinct()
                     .exclude(election_area__classification='ELECT_RIP')
                     .order_by('election_area__parent__id'))
    rip_cases.extend([['Estero', 'Estero'], ['no_area', 'No area']])
    return rip_cases


def get_election_latest_group(request=None):
    if request is None:
        return []

    leg = request.parser_context['kwargs'].get('legislature', None)
    role_filter = Q()
    if (case := request.query_params.get('role', None)):
        if case == 'Senatore':
            role_filter = Q(organization__parent__name__icontains='senato')
        elif case == 'Deputato':
            role_filter = Q(organization__parent__name__icontains='camera')
        else:
            raise Exception
    list_groups = []
    latest_groups = Group.objects.by_legislature(leg).filter(role_filter)
    latest_groups = Group.objects.filter(id__in=latest_groups.values('id')).distinct()
                         # .values_list('acronym', 'acronym')
                         # .distinct().order_by('acronym')

    for x in latest_groups:
        list_groups.append((x.acronym_last, x.acronym_last))
    list_groups = sorted(list_groups)
    list_groups.extend([['no_group', 'Nessun gruppo']])
    return list_groups


def get_branches(request=None):
    if request is None:
        return []

    leg = request.parser_context['kwargs'].get('legislature', None)

    qs = Organization.objects.filter(classification='Assemblea parlamentare',
                                     key_events__key_event__identifier=f"ITL_{leg}")

    res = []
    for i in qs:
        res.append({
            'id': i.name.split(" ")[1][0],
            'value': i.name.split(" ")[1]
        })

    return res  # list(qs.values('identifier', 'name').annotate(id=F('identifier')).values('id', 'name'))


VOTE_TYPES = (
    ('is_key_vote', _('Key vote')),
    ('is_final', _('Final vote')),
    ('is_confidence', _('Confidence vote')),
    ('all', 'Tutti')
)


def get_vote_types(descriptive=False):
    if descriptive:
        return VOTE_TYPES
    else:
        return ['is_key_vote', 'is_final', 'is_confidence', 'all']


def get_sub_vote_types():
    return Voting.VOTE_TYPES


GENDER_TYPES = (
    ('M', _('Male')),
    ('F', _('Female')),
)

ROLE_TYPES = (
    ('Senatore', _('Senator')),
    ('Deputato', _('Deputy')),
)

SUPPORS_MAJORITY_TYPES = (
    (False, _('Does not support')),
    (True, _('Supports')),
    ('null', _('Unknown'))
)

IS_ACTIVE = (
    (True, _('Active')),
    (False, _('Ceased')),
)


def get_governments(request, descriptive=False):
    leg = request.parser_context['kwargs'].get('legislature', None)
    legislature_identifier = f"ITL_{leg}"
    e = KeyEvent.objects.get(identifier=legislature_identifier)
    qs = Organization.objects.filter(id__in=Government.objects.filter(organization_id__in=GovDecree.objects.by_legislature(leg)
                                                   .values('sitting__government__id')
                                                   .distinct()).values('organization_id'))

    if descriptive:
        return qs.values_list('id', 'name')
    else:
        return qs.all()


VM_COHESION_RATES = (
    ('80_100', _('80% to 100%')),
    ('60_80', _('60% to 80%')),
    ('_60', _('Less than 60%')),
)


def get_vm_cohesion_rates(descriptive=False):
    if descriptive:
        return VM_COHESION_RATES
    else:
        return ['80_100', '60_80', '_60', ]


class VotingFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and years, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)

        # self.filters['branch'].extra['choices'] = get_branches(request=self.request)
        self.filters['year'].extra['choices'] = [
            (y[0], y[1]) for y in get_years(request=self.request)
        ]

    year = ChoiceFilter(
        method="year_filter",
        label=_("Year"),
        empty_label=_("All years"),
        help_text=_("Filter votings based on the year of the sitting date"),
    )

    @staticmethod
    def year_filter(queryset, name, value):
        if value:
            return queryset.filter(sitting__date__year=value)
        else:
            return queryset

    bill = CharFilter(
        method="bill_filter",
        label=_("Bill Slug"),
        help_text=_("Filter votings based on the bill slug"),
    )

    @staticmethod
    def bill_filter(queryset, name, value):
        value = value.replace('_', '.').replace('/','-')
        if value:
            return queryset.filter(bills__identifier=value).order_by('-sitting__date', '-number')
        else:
            return queryset

    branch = ChoiceFilter(
        method="branch_filter",
        label=_("Branch"),
        empty_label=_("All branches"),
        choices=[('C', 'Camera'), ('S', 'Senato')],
        help_text=_("Filter votings based on the branch they took place (ASS-CAMERA-18, ASS-SENATO-19, ...)"),
    )

    @staticmethod
    def branch_filter(queryset, name, value):
        if value:
            c_s_mapping = \
                {'S': 'SENATO',
                 'C': 'CAMERA'}
            return queryset.annotate(b=F("sitting__assembly__identifier")).filter(b__icontains=c_s_mapping.get(value))
        else:
            return queryset.all()

    main_vote_type = ChoiceFilter(
        method="main_vote_type_filter",
        label=_("Main vote type"),
        choices=VOTE_TYPES,
        help_text=_("Filter votings based on the type of vote (is_final, is_confidence, is_keyvote)"),
    )

    @staticmethod
    def main_vote_type_filter(queryset, name, value):
        if value == 'all':
            return queryset.all()
        if value:
            kwargs = {value: True}
            return queryset.filter(**kwargs)
        else:
            return queryset.all()

    sub_vote_type = ChoiceFilter(
        method="sub_vote_type_filter",
        label=_("Sub vote type"),
        choices=Voting.VOTE_TYPES,
        help_text=_("Filter votings based on the type of vote (is_final, is_confidence, is_keyvote)"),
    )

    @staticmethod
    def sub_vote_type_filter(queryset, name, value):
        if value:
            # kwargs = {value: True}
            return queryset.filter(type=value)
        else:
            return queryset.all()

    government = ModelChoiceFilter(
        method="government_filter",
        label=_("Government ID"),
        queryset=get_governments,
        help_text=_("Filter votings based on the Government period (Organization ID)"),
    )

    @staticmethod
    def government_filter(queryset, name, value):
        if value:
            g = Organization.objects.get(id=value.id)
            if g.end_date:
                return queryset.filter(sitting__date__gte=g.start_date, sitting__date__lte=g.end_date)
            else:
                return queryset.filter(sitting__date__gte=g.start_date)
        else:
            return queryset.all()

    vm_cohesion_rate = ChoiceFilter(
        method="vm_cohesion_rate_filter",
        label=_("Cohesion rate of the majority"),
        choices=VM_COHESION_RATES,
        help_text=_("Filter votings based on the cohesion rate of the majority"),
    )

    @staticmethod
    def vm_cohesion_rate_filter(queryset, name, value):
        if value in get_vm_cohesion_rates():
            filter_dict = {}
            if value == '80_100':
                filter_dict.update({
                    'majority_cohesion_rate__gte': 80
                })
            elif value == '60_80':
                filter_dict.update({
                    'majority_cohesion_rate__gte': 60,
                    'majority_cohesion_rate__lt': 80,
                })
            else:
                filter_dict.update({
                    'majority_cohesion_rate__lt': 60,
                })
            return queryset.filter(**filter_dict)
        else:
            return queryset.all()


    outcome = ChoiceFilter(
        choices = Voting.objects.all().values_list('outcome', 'outcome').distinct().order_by('outcome')
    )


    class Meta:
        model = Voting
        fields = []


class MembershipFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)

        self.filters['election_area'].extra['choices'] = [
            (y[0], y[1]) for y in get_election_areas(request=self.request)
        ]

        self.filters['election_macro_area'].extra['choices'] = [
            (y[0], y[1]) for y in get_election_macro_areas(request=self.request)
        ]

        self.filters['latest_group'].extra['choices'] = [
            (y[0], y[1]) for y in get_election_latest_group(request=self.request)
        ]

    role = ChoiceFilter(
        method="role_filter",
        label=_("Role"),
        empty_label=_("All roles"),
        choices=ROLE_TYPES,
        help_text=_("Filter memberships based on the role (Senator, Deputy, ...)"),
    )

    @staticmethod
    def role_filter(queryset, name, value):
        if value:
            return queryset.annotate(b=F("opdm_membership__role")).filter(b__icontains=value)
        else:
            return queryset.all()

    supports_majority = ChoiceFilter(
        method="supports_majority_filter",
        label=_("Supports majority"),
        empty_label=_("All"),
        choices=SUPPORS_MAJORITY_TYPES,
        help_text=_("Filter memberships based on whether they support majority"),
    )

    @staticmethod
    def supports_majority_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("supports_majority")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("supports_majority")).filter(b=value)

        else:
            return queryset.all()

    is_active = ChoiceFilter(
        method="is_active_filter",
        label=_("Is active"),
        empty_label=_("All"),
        choices=IS_ACTIVE,
        help_text=_("Filter memberships based on whether they are active"),
    )

    @staticmethod
    def is_active_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("is_active")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("is_active")).filter(b=value)

        else:
            return queryset.all()

    gender_type = ChoiceFilter(
        method="gender_type_filter",
        label=_("Gender type"),
        empty_label=_("All genders"),
        choices=GENDER_TYPES,
        help_text=_("Filter votings based on the gender"),
    )

    @staticmethod
    def gender_type_filter(queryset, name, value):
        if value:
            return queryset.filter(opdm_membership__person__gender=value)
        else:
            return queryset.all()

    election_area = ChoiceFilter(
        method="election_area_filter",
        label=_("Election area"),
        empty_label=_("All election areas"),
        help_text=_("Filter memberships based on the election area they were elected into"),
    )

    @staticmethod
    def election_area_filter(queryset, name, value):
        if value:
            if value == 'Estero':
                return queryset.filter(election_area__classification='ELECT_RIP')
            elif value == 'no_area':
                return queryset.filter(election_area__isnull=True)
            else:
                return queryset.filter(election_area=value)

        else:
            return queryset.all()

    election_macro_area = ChoiceFilter(
        method="election_macro_area_filter",
        label=_("Election macro area"),
        empty_label=_("All election macro areas"),
        help_text=_("Filter memberships based on the election macro area they were elected into"),
    )

    @staticmethod
    def election_macro_area_filter(queryset, name, value):
        if value:
            if value == 'Estero':
                return queryset.filter(election_area__classification='ELECT_RIP')
            elif value == 'no_area':
                return queryset.filter(election_area__isnull=True)
            else:
                return queryset.filter(election_area__parent=value)

        else:
            return queryset.all()

    latest_group = ChoiceFilter(
        method="latest_group_filter",
        label=_("Latest group"),
        empty_label=_("All groups"),
        help_text=_("Filter memberships based on the latest group the member is in"),
    )

    @staticmethod
    def latest_group_filter(queryset, name, value):
        qs = queryset.prefetch_related('opdm_membership').filter(opdm_membership__end_date__isnull=True)
        if value == 'no_group':
            return qs.prefetch_related('membership_groups').filter(membership_groups__group__acronym='Nessun gruppo', membership_groups__end_date__isnull=True)
        res = qs.prefetch_related('membership_groups').filter(Q(Q(membership_groups__group__acronym=value)|Q(
            membership_groups__group__organization__other_names__name=value

        )),
                                                               membership_groups__end_date__isnull=True)
        return qs.prefetch_related('membership_groups').filter(id__in=res.values_list('id', flat=True)).distinct()

    class Meta:
        model = Membership
        fields = []


class MemberVoteFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)

    is_key_vote = ChoiceFilter(
        method="is_key_vote_filter",
        label="Voto chiave",
        empty_label=_("All"),
        choices=(
            (True, 'Sì'),
            (False, 'No'),
        ),
        help_text=_("Filter member votes for key votes"),
    )

    @staticmethod
    def is_key_vote_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("voting__is_key_vote")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("voting__is_key_vote")).filter(b=value)

        else:
            return queryset.all()

    is_rebel_vote = ChoiceFilter(
        method="is_rebel_vote_filter",
        label="Voto ribelle",
        empty_label=_("All"),
        choices=(
            (True, 'Sì'),
            (False, 'No'),
        ),
        help_text=_("Filter member votes for rebel votes"),
    )

    @staticmethod
    def is_rebel_vote_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("is_rebel")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("is_rebel")).filter(b=value)

        else:
            return queryset.all()

    is_confidence_vote = ChoiceFilter(
        method="is_confidence_vote_filter",
        label="Voto fiducia",
        empty_label=_("All"),
        choices=(
            (True, 'Sì'),
            (False, 'No'),
        ),
        help_text=_("Filter member votes for confidence votes"),
    )

    @staticmethod
    def is_confidence_vote_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("voting__is_confidence")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("voting__is_confidence")).filter(b=value)

        else:
            return queryset.all()

    person = CharFilter(
        method="person_filter",
        label=_("Person slug"),
        help_text=_("Filter votings based on the person slug"),
    )

    @staticmethod
    def person_filter(queryset, name, value):
        if value:
            return queryset.filter(membership__opdm_membership__person__slug=value)
        else:
            return queryset

    class Meta:
        model = MemberVote
        fields = []


class GroupVoteFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)

    is_key_vote = ChoiceFilter(
        method="is_key_vote_filter",
        label="Voto chiave",
        empty_label=_("All"),
        choices=(
            (True, 'Sì'),
            (False, 'No'),
        ),
        help_text=_("Filter member votes for key votes"),
    )

    @staticmethod
    def is_key_vote_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("voting__is_key_vote")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("voting__is_key_vote")).filter(b=value)

        else:
            return queryset.all()

    group = CharFilter(
        method="group_filter",
        label=_("Group id"),
        help_text=_("Filter votings based on the group id"),
    )

    @staticmethod
    def group_filter(queryset, name, value):
        if value:
            return queryset.filter(group_id=value)
        else:
            return queryset

    class Meta:
        model = GroupVote
        fields = []


class BillSignerFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)

    is_first_signer = ChoiceFilter(
        method="is_first_signer_filter",
        label="Primo firmatario",
        empty_label=_("All"),
        choices=(
            (True, 'Sì'),
            (False, 'No'),
        ),
        help_text=_("Filter member votes for first signers"),
    )

    @staticmethod
    def is_first_signer_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("first_signer")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("first_signer")).filter(b=value)

        else:
            return queryset.all()

    person = CharFilter(
        method="person_filter",
        label=_("Person slug"),
        help_text=_("Filter votings based on the person slug"),
    )

    @staticmethod
    def person_filter(queryset, name, value):
        if value:
            return queryset.filter(membership__person__slug=value)
        else:
            return queryset

    class Meta:
        model = BillSigner
        fields = []
