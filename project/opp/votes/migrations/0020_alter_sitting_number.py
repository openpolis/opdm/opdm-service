# Generated by Django 3.2.16 on 2023-02-17 11:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('votes', '0019_alter_membership_election_area'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sitting',
            name='number',
            field=models.PositiveSmallIntegerField(help_text='Sitting number', null=True),
        ),
    ]
