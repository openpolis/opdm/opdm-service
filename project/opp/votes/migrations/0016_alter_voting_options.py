# Generated by Django 3.2.16 on 2023-01-03 13:22

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('votes', '0015_rename_acts_voting_bills'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='voting',
            options={'ordering': ('-sitting__date',), 'verbose_name': 'Voting', 'verbose_name_plural': 'Votings'},
        ),
    ]
