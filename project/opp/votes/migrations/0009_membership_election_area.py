# Generated by Django 3.2.16 on 2022-12-12 17:37

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('popolo', '0004_auto_20211018_1253'),
        ('votes', '0008_alter_group_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='membership',
            name='election_area',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='opp_membership', to='popolo.area'),
        ),
    ]
