from config.settings.base import ENDPOINTS_ASSEMBLEE_PARLAMENTARI
from django.db import IntegrityError

from project.tasks.parsing.sparql.utils import get_bindings
from taskmanager.management.base import LoggingBaseCommand

from project.opp.acts.management.commands.raw_sparql.template_query import template_query_sparql
from project.opp.acts.models import Bill
from popolo.models import Organization

from ooetl.loaders import Loader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor, Extractor

import pandas as pd


def divide_chunks(data, n):
    for i in range(0, len(data), n):
        yield data[i:i + n]


def get_bill(fase, assemblee):
    assembly = fase[0]
    if assembly == 'S':
        org = assemblee.get(identifier__icontains='senato')
    elif assembly == 'C':
        org = assemblee.get(identifier__icontains='camera')
    else:
        raise NotImplementedError
    identifier = fase

    return Bill.objects.get(identifier=identifier, assembly=org)


def create_relationships(subset, assemblee):
    subset = subset.sort_values('progrIter')
    rows = subset.shape[0] - 1

    for row in range(rows):
        prev = subset.iloc[row]
        next = subset.iloc[row+1]
        prev_bill = get_bill(prev['identifier'], assemblee)
        next_bill = get_bill(next['identifier'], assemblee)
        if int(prev['progrIter']) > 0:
            try:
                prev_bill.add_next(next_bill)
            except IntegrityError:
                pass
        else:
            try:
                prev_bill.add_split(next_bill)
            except IntegrityError:
                pass


class JsonExtractor(Extractor):
    """Json Extractor
    """

    def __init__(self, df):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.df = df

    def extract(self):
        res = []
        for r in self.df:
            res.append(dict((k, r[k]["value"]) for k in r.keys()))
        od = pd.DataFrame(res)
        return od


class RelActTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, legislatura):
        Transformation.__init__(self)
        self.legislatura = legislatura

    def transform(self):
        od = self.etl.original_data.copy()
        od['progrIter'] = od['progrIter'].astype('int')
        ids = od.idDdl.unique()
        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=self.legislatura)
        for id in ids:
            subset = od[od['idDdl'] == id]
            create_relationships(subset, assemblee)

        od = od.where(pd.notnull(od), None)
        self.etl.processed_data = od

        return self.etl


class RelActAssorbTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, legislatura):
        Transformation.__init__(self)
        self.legislatura = legislatura

    def transform(self):
        od = self.etl.original_data.copy()
        for index, item in od.iterrows():
            from_bill = Bill.objects.get(source_identifier=item['from_assorb'])
            to_bill = Bill.objects.get(source_identifier=item['to_assorb'])
            try:
                to_bill.add_joined(from_bill)
            except IntegrityError:
                pass

        self.etl.processed_data = od

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA and SENATO for relationships."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        legislatura_padded = str(legislatura).zfill(2)
        tipologia = 'rel_atti'
        self.logger.info("Getting acts FULL")

        def get_query(**kwargs):
            query = template_query_sparql(name=tipologia, **kwargs)
            return get_bindings(
                    ENDPOINTS_ASSEMBLEE_PARLAMENTARI.get(kwargs.get('aula')),
                    query,
                    legacy=True
            )

        self.logger.debug('Getting acts')
        min_ids = Bill.objects.filter(assembly__identifier__endswith=legislatura_padded).order_by('ddl').first().ddl
        max_ids = Bill.objects.filter(assembly__identifier__endswith=legislatura_padded).order_by('ddl').last().ddl

        for i, ids_range in enumerate(range(min_ids, max_ids, 500)):
            self.logger.debug(f'Getting {i}')
            data = JsonExtractor(get_query(legislatura=legislatura,
                                           tipologia=tipologia,
                                           aula='senato',
                                           ids_range=ids_range
                                           )).extract()

            if data.shape[0] == 0:
                self.logger.warning(f'No data for legislation {legislatura} for category {tipologia}')
                continue

            data_filt = data[data['idDdl'].isin(data[data['idDdl'].duplicated()]['idDdl'])]

            if data_filt.shape[0] == 0:
                self.logger.warning(f'No data for legislation {legislatura} for category {tipologia}')
                continue

            ETL(
                extractor=DataframeExtractor(data_filt),
                transformation=RelActTransformation(legislatura_padded),
                loader=Loader(),
            )()

        self.logger.debug('Getting acts assorbimento')
        tipologia = 'rel_assorb'
        data = JsonExtractor(get_query(legislatura=legislatura,
                                       tipologia=tipologia,
                                       aula='senato')).extract()

        ETL(
            extractor=DataframeExtractor(data),
            transformation=RelActAssorbTransformation(legislatura_padded),
            loader=Loader(),
        )()


        conversions_law = Bill.objects.filter(assembly__identifier__endswith=legislatura_padded,
                                              type=Bill.DL_CONVERSION)
        for c in conversions_law:
            try:
                gd = c.get_gov_decree
            except:
                continue
            if c.is_omnibus != gd.is_omnibus:
                print(c)
                c.is_omnibus = gd.is_omnibus
                c.save()
