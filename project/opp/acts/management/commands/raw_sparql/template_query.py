def template_query_sparql(**kwargs):
    legislatura = kwargs.get('legislatura')
    name = kwargs.get('name')
    aula = kwargs.get('aula')

    year_month = kwargs.get('year_month')
    ids_range = kwargs.get('ids_range', 0)

    QUERY_ATTI = {
        'senato': f"""

PREFIX osr: <http://dati.senato.it/osr/>
SELECT
 ?ramo ?fase ?titolo ?titoloBreve ?tipoiniziativa ?idDdl ?dataPresentazione ?statoDdl ?numeroLegge
?dataLegge ?dataStatoDdl ?idFase ?natura MAX(?is_ratifica) as ?is_ratification
WHERE{{{{
SELECT DISTINCT ?ramo ?fase ?titolo ?titoloBreve ?tipoiniziativa ?idDdl ?dataPresentazione ?statoDdl ?numeroLegge
?dataLegge ?dataStatoDdl ?idFase ?natura
(regex(str(?prefLabel), "RATIFICA DEI TRATTATI", "i")) as ?is_ratifica
WHERE
{{
    ?ddl a osr:Ddl.
    ?ddl osr:idDdl ?idDdl.
    ?ddl osr:statoDdl ?statoDdl.
    ?ddl osr:titolo ?titolo.
    ?ddl osr:dataPresentazione ?dataPresentazione.
    ?ddl osr:ramo ?ramo.
    ?ddl osr:fase ?fase.
    ?ddl osr:idFase ?idFase.
    ?ddl osr:natura ?natura.
    OPTIONAL {{?ddl osr:titoloBreve ?titoloBreve}}
    OPTIONAL {{?ddl osr:dataPresentazione ?dataPresentazione}}
    OPTIONAL {{?ddl osr:numeroLegge ?numeroLegge}}
    OPTIONAL {{?ddl osr:dataLegge ?dataLegge}}
    OPTIONAL {{?ddl osr:dataStatoDdl ?dataStatoDdl}}
    OPTIONAL {{?ddl osr:classificazione ?classificazione}}
    OPTIONAL {{?classificazione dcterms:subject ?subject  }}
    OPTIONAL {{?subject skos:prefLabel ?prefLabel}}
    ?ddl osr:legislatura ?legislatura.
?ddl osr:iniziativa ?iniziativa.
?iniziativa osr:tipoIniziativa ?tipoiniziativa.

    FILTER (?legislatura= {legislatura} )
FILTER (regex(STR(?dataStatoDdl), "{year_month}", "i"))
}}
}}}}
group by ?ramo ?fase ?titolo ?titoloBreve ?tipoiniziativa ?idDdl ?dataPresentazione ?statoDdl ?numeroLegge
?dataLegge ?dataStatoDdl ?idFase ?natura

ORDER BY ?fase

                  """
    }

    QUERY_REL_ATTI = {
        'senato': f"""

        PREFIX osr: <http://dati.senato.it/osr/>

SELECT DISTINCT ?idDdl ?progrIter ?identifier ?legislatura
WHERE
{{
 ?iter a osr:IterDdl.
?iter osr:idDdl ?idDdl.
optional {{?iter osr:assorbimento ?assorbimento.}}
optional {{?iter osr:testoUnificato ?testoUnificato.}}
?iter osr:fase ?fase.
?fase osr:ddl ?ddl.
?fase osr:progrIter ?progrIter.
?ddl osr:legislatura ?legislatura.
?ddl osr:fase ?identifier.
?ddl osr:statoDdl ?statoDdl.
?ddl osr:dataPresentazione ?dataPresentazione.
filter (?legislatura = {legislatura})
FILTER (?idDdl >= {ids_range} && ?idDdl <={ids_range + 500})
}}


        """
    }

    QUERY_REL_ASSORBIMENTO = {
        'senato': f"""

        PREFIX osr: <http://dati.senato.it/osr/>

SELECT DISTINCT ?from_assorb ?to_assorb
WHERE
{{

?iter a osr:IterDdl.
?iter osr:idDdl ?idDdl.
?iter osr:assorbimento ?assorbimento.
?assorbimento osr:idFase ?from_assorb.
?assorbimento osr:ramo ?from_branch.
?assorbimento osr:statoDdl ?statoDdlfrom.
?iter osr:fase ?fase.
?fase osr:ddl ?ddl.
?fase osr:progrIter ?progrIter.
?ddl osr:legislatura ?legislatura.
?ddl osr:fase ?identifier.
?ddl osr:idFase ?to_assorb.
?ddl osr:ramo ?to_branch.

filter (?legislatura = {legislatura})
filter (regex(STR(?statoDdlfrom), "assorbito", "i"))
filter(?to_branch = ?from_branch)
}}


                """
    }

    QUERY_PRESENTING_ACTS = {
        'senato': f"""
        PREFIX osr: <http://dati.senato.it/osr/>
    PREFIX ocd: <http://dati.camera.it/ocd/>

    SELECT DISTINCT
      ?idFase
      xsd:date(?dataAggiuntaFirma) as ?dataAggiuntaFirma
      ?senatore
      ?presentatore
      xsd:date(?dataPresentazione) ?dataPresentazione
      ?primoFirmatario
      xsd:date(?dataRitiroFirma) as ?dataRitiroFirma
      ?deputato
      ?tipoiniziativa
    WHERE
    {{
        ?ddl a osr:Ddl.
        ?ddl osr:idDdl ?idDdl.
        ?ddl osr:idFase ?idFase.
        ?ddl osr:legislatura ?legislatura.
         ?ddl osr:dataPresentazione ?dataPresentazione.
    ?ddl osr:iniziativa ?iniziativa.
    ?iniziativa osr:tipoIniziativa ?tipoiniziativa.
    ?iniziativa osr:presentatore ?presentatore
    optional {{?iniziativa osr:dataAggiuntaFirma ?dataAggiuntaFirma}}
    optional {{ ?iniziativa osr:senatore ?senatore }}
    optional {{?iniziativa osr:primoFirmatario ?primoFirmatario}}
    optional {{?iniziativa  osr:dataRitiroFirma ?dataRitiroFirma}}
    optional {{?iniziativa ocd:rif_deputato ?deputato}}

         FILTER (?legislatura= {legislatura} )
         FILTER ( (regex(STR(?dataPresentazione), "{year_month}", "i")) || (regex(STR(?dataRitiroFirma), "{year_month}", "i")) || (regex(STR(?dataAggiuntaFirma), "{year_month}", "i")) )
    }} ORDER BY ?tipoiniziativa

        """
    }

    QUERY_SPOKEPERSONS_ACTS = {
        'senato': f"""
        PREFIX osr: <http://dati.senato.it/osr/>

            SELECT DISTINCT ?ddl, ?identifier, ?senatore as ?parlamentare, ?diMinoranza,
            contains(lcase(?organo),"commissione") as ?is_commissione, ?dataPresentazione,
            ?presentatore, ?dataNomina, ?ramo
            WHERE
            {{
                ?ddl a osr:Ddl.
                ?ddl osr:legislatura ?legislatura.
                ?ddl osr:relatore ?relatore.
                ?ddl osr:ramo ?ramo.
                ?ddl osr:numeroFase ?identifier.
                ?ddl osr:dataPresentazione ?dataPresentazione.
                ?relatore osr:dataNomina ?dataNomina.
                optional{{?relatore osr:senatore ?senatore}}.
                ?relatore rdfs:label ?presentatore.
                ?relatore osr:diMinoranza ?diMinoranza.
                ?relatore osr:tipoRelatore ?tipoRelatore.
                ?relatore osr:organo ?organo.
                ?relatore osr:dataNomina ?dataNomina.
                FILTER (?legislatura= {legislatura} )
            filter (regex(?tipoRelatore, 'relatore', 'i'))
            }}

            """,

        'camera': """
SELECT distinct ?d,  ?firstName, ?surname
WHERE {{

## deputato
?d a ocd:deputato;
     ocd:rif_leg <http://dati.camera.it/ocd/legislatura.rdf/repubblica_19>;
     foaf:firstName ?firstName;
     foaf:surname ?surname.

}}
        """
    }

    QUERY_COMMISSIONS = {
        'senato': f"""
                PREFIX osr: <http://dati.senato.it/osr/>

            SELECT ?idFase, ?identifier, ?ramo, ?labelCommissione, ?tipoCommissione, ?sede, ?dataAssegnazione, ?organo
            WHERE
            {{
                ?ddl a osr:Ddl.
                ?ddl osr:legislatura ?legislatura.
                ?ddl osr:fase ?fase.
                ?ddl osr:idFase ?idFase.
                ?ddl osr:ramo ?ramo.
                ?ddl osr:numeroFase ?identifier.
                ?ddl osr:dataPresentazione ?dataPresentazione.
                ?ddl osr:assegnazione ?assegnazione.
                ?assegnazione rdfs:label ?labelCommissione.
                ?assegnazione osr:tipoCommissione ?tipoCommissione.
                optional{{?assegnazione osr:sede ?sede}}.
                ?assegnazione osr:dataAssegnazione ?dataAssegnazione
                optional{{?assegnazione osr:organo ?organo}}.
                FILTER (?legislatura= {legislatura} )
         FILTER ( (regex(STR(?dataAssegnazione), "{year_month}", "i"))  )

}}

        """
    }

    map_name_query = {
        'atti': QUERY_ATTI,
        'rel_atti': QUERY_REL_ATTI,
        'rel_assorb': QUERY_REL_ASSORBIMENTO,
        'presenting_acts': QUERY_PRESENTING_ACTS,
        'spokepersons_acts': QUERY_SPOKEPERSONS_ACTS,
        'commissions': QUERY_COMMISSIONS
    }
    query_dict = map_name_query.get(name)
    return query_dict.get(aula)
