import pandas as pd
from bs4 import BeautifulSoup
from popolo.models import Organization
from project.opp.acts.models import ImplementingDecree, Bill, GovBill, GovDecree, ImplementingDecreeOrganizations
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import RemoteExtractor, DataframeExtractor
from django.db.models.functions import Concat
from datetime import datetime
from django.db import IntegrityError
from django.db.models import Q, TextField
import locale
import re
import requests
from taskmanager.management.base import LoggingBaseCommand
from django.core.exceptions import ObjectDoesNotExist

locale.setlocale(locale.LC_ALL, 'it_IT.utf-8')


def get_ministry(ministry_text, mins):
    ministero2 = [x for x in ministry_text.split(' ') if len(x) > 3]
    ministero2 = (' '.join(ministero2[:min(len(ministero2), 2)]) or ministry_text).replace(',', ' ')
    minist = mins.get(
        Q(name__iregex=ministry_text.replace(' ', r'.*')) |
        Q(name__iregex=r'[^\w]' + ministero2.replace(' ', r'.*')) |
        Q(name__iregex=r'[^\w]' + ministero2.split('\\n')[0].replace(' ', r'.*'))
    )
    return minist


def get_agreements(concerto, mins, aut_ind):
    concerto2 = [x for x in concerto.split(' ') if len(x) > 3]
    concerto2 = (' '.join(concerto2[:min(len(concerto2), 2)]) or concerto).replace(',', ' ')
    concert = mins.filter(
        Q(name__iregex=r'[^\w]' + concerto.replace(' ', r'.*')) |
        Q(name__iregex=r'[^\w]' + concerto2.replace(' ', r'.*'))
    )
    if concert.count() == 0:
        concert = mins.filter(other_names__name__iregex=r'[^\w]' + concerto2.replace(' ', r'.*'))
    if concert.count() != 1:
        concert = aut_ind.filter(
            Q(name__iregex=concerto.replace(' ', r'.*')) |
            Q(other_names__name__iregex=r'[^\w]{0,1}' + concerto.replace(' ', r'.*'))
        )
    if concert.count() != 1:
        return None
    return concert[0]


def get_fundings(df):
    return {int(df[x]): float(df[x + 1].replace('.', '').replace(',', '.'))
            for x in [0, 2, 4] if df[x]
            and df[x + 1]
            and float(df[x + 1].replace('.', '').replace(',', '.')) > 0}


def get_advisories(text):
    if not text:
        return None
    list_entities = text.split('\n')
    return '; '.join(list(set([x for x in list_entities if len(x) > 1])))


def get_gov(gov_text):
    gov = ['I']
    gov.extend(gov_text.split(' ')[::-1])
    gov = ' Governo '.join(gov[-2:])
    return Organization.objects.filter(classification='Governo della Repubblica') \
        .get(name__iexact=gov)


def get_type(source_type):
    if source_type == 'Governativa':
        return ImplementingDecree.GOVERNMENT
    elif source_type == 'Parlamentare':
        return ImplementingDecree.PARLIAMENT
    else:
        return NotImplementedError


def get_soup(url):
    page = requests.get(url, verify=False)
    soup = BeautifulSoup(page.content, "html.parser")
    return soup


def get_fonte_provvedimento(source):
    regex = r'(^(?:D\.lgs\.)|^(?:L\.)|^(?:D\.L\.)) (\d+)\/(\d+)'
    matches = re.findall(regex, source, re.MULTILINE)
    law_type, law_number, law_year = matches[0]
    try:
        if law_type == 'D.L.':
            return GovDecree.objects.get(identifier=f'{law_year}-{law_number}')

        elif law_type == 'D.lgs.':
            return GovBill.objects.get(identifier=f'{law_year}-{law_number}')
        elif law_type == 'L.':
            return Bill.objects.get(law_number=law_number, law_publication_date__year=law_year)
        else:
            return NotImplementedError
    except ObjectDoesNotExist:
        print(f'NOT FOUND {law_type}, {law_number}, {law_year}')



class ImplementingDecreeExtractor(RemoteExtractor):
    def get_governments(self):
        soup = get_soup(self.remote_url)
        select_tag = soup.find('select', id='esecutivo')
        govs = select_tag.get_text().split('\n')
        govs.remove('Governo')
        return [x for x in govs if x.strip()]
    def extract(self):
        govs = self.get_governments()
        df_all = pd.DataFrame()
        for gov in govs:
            try:
                df = pd.read_csv(f'https://www.programmagoverno.gov.it/it/'
                                 f'ricerca-provvedimenti/?altTemplate=customExportProvvedimenti&esecutivo='
                                 f'{gov.replace(" ", "+")}',
                                 sep=";")
            except:
                import ssl
                ssl._create_default_https_context = ssl._create_unverified_context
                df = pd.read_csv(f'https://www.programmagoverno.gov.it/it/'
                                 f'ricerca-provvedimenti/?altTemplate=customExportProvvedimenti&esecutivo='
                                 f'{gov.replace(" ", "+")}',
                                 sep=";")
            df_all = df_all.append(df)
        df = df_all[~df_all['Fonte Provvedimento'].isna()]
        df['source'] = df['Fonte Provvedimento'].apply(get_fonte_provvedimento)
        df = df[~df['source'].isna()]
        df = df.rename(columns={"Governo": "government",
                                "Ministero Proponente": "ministeri",
                                "Natura Provvedimento": 'type',
                                "Concerti": "concerti",
                                "Termine di scadenza": "deadline_date",
                                "Oggetto": "original_title",
                                "Adozione": "adopted",
                                "DataAdozione": "adoption_date",
                                "Fonte Provvedimento": "identifier",
                                "Data pubblicazione G.U.": "publication_gu_date",
                                "Numero G.U.": "publication_gu_number",
                                "Link provvedimento": "url",
                                "Policy": "policy",
                                "Area Tematica": "theme",
                                "Provvedimento Previsto": "type_act",
                                "Anno Finanziamento 1": "year_1",
                                "Importo Finanziamento 1": "imp_1",
                                "Anno Finanziamento 2": "year_2",
                                "Importo Finanziamento 2": "imp_2",
                                "Anno Finanziamento 3": "year_3",
                                "Importo Finanziamento 3": "imp_3",
                                },
                       errors="raise")
        df = df.where(pd.notnull(df), None)
        df['adoption_date'] = df['adoption_date'].apply(lambda x: datetime.strptime(x, '%d/%m/%Y') if x and x!='31/12/2099' else None)
        df['deadline_date'] = df['deadline_date'].apply(lambda x: datetime.strptime(x, '%d/%m/%Y') if x and x!='31/12/2099' else None)
        df['publication_gu_date'] = df['publication_gu_date'].apply(
            lambda x: datetime.strptime(x, '%d/%m/%Y') if x and x!='31/12/2099' else None)
        df['adoption_date'] = df.adoption_date.astype(object).where(df.adoption_date.notnull(), None)
        df['deadline_date'] = df.deadline_date.astype(object).where(df.deadline_date.notnull(), None)
        df['publication_gu_date'] = df.publication_gu_date.astype(object).where(df.publication_gu_date.notnull(), None)
        df['is_adopted'] = df['adopted'].apply(lambda x: x == 'Adottato')
        df['is_valid'] = True
        df['type'] = df['type'].apply(get_type)
        df['publication_gu_number'] = df.publication_gu_number.str.extract(r'(\d+)')
        df['government'] = df['government'].apply(get_gov)
        df['advisories_raw'] = df['Pareri'].apply(get_advisories)
        df['fundings'] = df.filter(regex=r'^imp|^year', axis=1).apply(get_fundings, axis=1)
        df = df.where(pd.notnull(df), None)
        return df


class ImplementingDecreeTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """

    def __init__(self):
        Transformation.__init__(self)

    def transform(self):
        od = self.etl.original_data.copy()
        od = od[['deadline_date',
                 'is_adopted', 'adoption_date', 'identifier',
                 'original_title', 'is_valid', 'url',
                 'policy', 'theme', 'publication_gu_number', 'publication_gu_date',
                 'type_act', 'type', 'government', 'advisories_raw', 'fundings']]
        self.etl.processed_data = od
        return self.etl


class ImplementingDecreePropTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self):
        Transformation.__init__(self)
    def transform(self):
        mins = Organization.objects.filter(end_date__isnull=True) \
            .filter(
            Q(classification__icontains='Ministero') |
            Q(name='PRESIDENZA DEL CONSIGLIO DEI MINISTRI'))
        od = self.etl.original_data.copy()
        od['act'] = od.apply(lambda x: ImplementingDecree.objects.get(identifier=x['identifier'],
                                                                      original_title=x['original_title']), axis=1)
        min_obj = pd.Series(list(map(lambda x: get_ministry(x, mins), od['ministeri'].unique())))
        min_obj.index = od['ministeri'].unique()
        min_obj = min_obj.to_dict()
        od['organization'] = od['ministeri'].apply(lambda x: min_obj.get(x))
        od['type'] = ImplementingDecreeOrganizations.PROPOSER
        od = od[['act', 'organization', 'type']]
        self.etl.processed_data = od
        return self.etl


class ImplementingDecreeAgreeTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """

    def __init__(self):
        Transformation.__init__(self)

    def transform(self):
        mins = Organization.objects.filter(end_date__isnull=True) \
            .filter(
            Q(classification__icontains='Ministero') |
            Q(name='PRESIDENZA DEL CONSIGLIO DEI MINISTRI'))

        aut_ind = Organization.objects.filter(end_date__isnull=True) \
            .filter(classification__in=['Autorità indipendente', 'Agenzia dello Stato',
                                        'Altro ente pubblico non economico'])

        od = self.etl.original_data.copy()
        od['act'] = od.apply(lambda x: ImplementingDecree.objects.get(identifier=x['identifier'],
                                                                      original_title=x['original_title']),
                             axis=1)
        conc_obj = pd.Series(list(map(lambda x: get_agreements(x, mins, aut_ind), od['concerti'].unique())))
        conc_obj.index = od['concerti'].unique()
        conc_obj = conc_obj.to_dict()
        od['organization'] = od['concerti'].apply(lambda x: conc_obj.get(x))
        od['type'] = ImplementingDecreeOrganizations.JOINTLY

        null_org = od[od['organization'].isnull()]
        null_org = null_org.groupby(by=['identifier', 'original_title'])['concerti'] \
            .apply(lambda x: '; '.join(x)).reset_index()
        null_org['miss_jointly_agr_raw'] = null_org['concerti']
        ETL(
            extractor=DataframeExtractor(null_org[['identifier',
                                                   'original_title',
                                                   'miss_jointly_agr_raw']]),
            loader=DjangoUpdateOrCreateLoader(django_model=ImplementingDecree,
                                              fields_to_update=['miss_jointly_agr_raw']),
        )()
        od = od[['act', 'organization', 'type']]
        self.etl.processed_data = od[~od['organization'].isnull()]
        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql for Gov Sittings."""

    help = ""

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        # ImplementingDecree.objects.update(is_valid=False)
        df = ImplementingDecreeExtractor(url='https://www.programmagoverno.gov.it/it/ricerca-provvedimenti/').extract()
        ETL(
            extractor=DataframeExtractor(df),
            transformation=ImplementingDecreeTransformation(),
            loader=DjangoUpdateOrCreateLoader(django_model=ImplementingDecree,
                                              fields_to_update=['deadline_date',
                                                                'is_adopted',
                                                                'adoption_date',
                                                                'is_valid',
                                                                'url',
                                                                'policy',
                                                                'theme',
                                                                'publication_gu_number',
                                                                'publication_gu_date',
                                                                'type_act',
                                                                'type',
                                                                'government',
                                                                'advisories_raw',
                                                                'fundings']),
        )()
        to_check = list(map(lambda x: ''.join(x), df[['identifier', 'original_title']].values))
        impl = ImplementingDecree.objects.annotate(
            conc=Concat('identifier', 'original_title', output_field=TextField()))
        impl.exclude(conc__in=to_check).update(is_valid=False)
        ministeri = df[['identifier', 'ministeri', 'original_title']].copy()
        ministeri['ministeri'] = ministeri['ministeri'].apply(lambda x: [y.strip()
                                                                         for y in x.split(' -') if y.strip()])
        ministeri = ministeri.explode('ministeri')
        ETL(
            extractor=DataframeExtractor(ministeri),
            transformation=ImplementingDecreePropTransformation(),
            loader=DjangoUpdateOrCreateLoader(django_model=ImplementingDecreeOrganizations,
                                              fields_to_update=['type']),
        )()

        concerti = df[['identifier', 'original_title', 'concerti']].copy()
        concerti = concerti[~concerti['concerti'].isnull()]
        concerti['concerti'] = concerti['concerti'].apply(lambda x: [y.strip()
                                                                     for y in x.split('\n') if y.strip()])
        concerti = concerti.explode('concerti')
        ETL(
            extractor=DataframeExtractor(concerti),
            transformation=ImplementingDecreeAgreeTransformation(),
            loader=DjangoUpdateOrCreateLoader(django_model=ImplementingDecreeOrganizations,
                                              fields_to_update=['type']),
        )()

        for index, item in df.iterrows():

            im = ImplementingDecree.objects.get(identifier=item['identifier'], original_title=item['original_title'])
            try:
                im.add_previous(item['source'])
            except IntegrityError:
                pass
