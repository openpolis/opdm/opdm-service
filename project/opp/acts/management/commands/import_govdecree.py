from bs4 import BeautifulSoup
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime
from project.opp.acts.models import Bill, GovDecree, GovSitting
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor

import locale
import pandas as pd
import pytz
import requests
import re
from taskmanager.management.base import LoggingBaseCommand

locale.setlocale(locale.LC_ALL, 'it_IT.utf-8')


def get_identifier(url):
    values = re.findall(r'decreto.legge:(.*)', url)[0].split(";")
    return '-'.join(list(map(lambda x: str(int(x)), values)))


def get_soup(url):
    page = requests.get(url)

    soup = BeautifulSoup(page.content, "html.parser")
    return soup


def get_date_sitting(description):
    regex = r"riunion[ei].{0,25}[ '](\d{1,2})°* (\w+ \d{4})"
    matches = re.findall(regex, description, re.MULTILINE)
    return ' '.join(matches[0])


def get_date_gu(description):
    regex = r'\(GU n.\d+ del (\d+-\d+-\d+)'
    matches = re.findall(regex, description, re.MULTILINE)
    return matches[0]


def get_sitting(date):
    try:
        return GovSitting.objects.get(starting_datetime__date=date)
    except Exception:
        return None


def get_end_date(description):

    regex_time = r'è terminato alle ore (\d+.\d+)'
    match_time = re.findall(regex_time, description, re.MULTILINE)
    regex_date = r'è terminato alle ore \d+.\d+ di .* (\d+°*) (\w+ \d+)'
    match_date = re.findall(regex_date, description, re.MULTILINE)
    if not match_date:
        regex = r'si è riunito .+ (\d+)°* (\w+ \d+)'
        match_date = re.findall(regex, description, re.MULTILINE)
    match_date = ' '.join(match_date[0])

    return f'{match_date} {match_time[0]}'


class GovDecreeExtractor(DataframeExtractor):

    def get_bill(self, act, date):
        try:
            return Bill.objects.get(identifier=act, date_presenting__gte=date)
        except ObjectDoesNotExist:
            pass

    @staticmethod
    def get_text_decree(href):
        soup = get_soup(href)
        atto_orig = soup.find(text=re.compile('originario'))
        href_atto_orig = atto_orig.parent.get('href')
        soup_atto_orig = get_soup(f"https://www.normattiva.it/{href_atto_orig}")
        return soup_atto_orig.get_text()

    def extract(self):
        soup = self.df

        dls = soup.find_all('dt')
        iters = soup.find_all('div', class_='iter')
        first_iter = [x.find('a').get_text() for x in iters]
        urls = [x.find('a').get('href') for x in dls]
        identifier = list(map(get_identifier, urls))
        original_title = [x.find('a').get_text() for x in dls]
        titoli = soup.find_all('p', class_='titoloLegge')
        descriptive_title = [x.get_text()[1:-1] for x in titoli]
        df = pd.DataFrame(list(zip(urls, original_title,
                                   descriptive_title, first_iter,
                                   identifier)),
                          columns=['url',
                                   # 'text_decree',
                                   'original_title',
                                   'descriptive_title',
                                   'act',
                                   'identifier'])
        already_existing = set(GovDecree.objects.all().values_list('identifier', flat=True))
        identifier = list(set(identifier) - already_existing)
        df = df[df['identifier'].isin(identifier)]
        if df.empty:
            return df

        df['text_decree'] = df['url'].apply(lambda x: self.get_text_decree(x))
        df['date_sitting'] = df['text_decree'].apply(get_date_sitting)
        df['date_sitting'] = df['date_sitting'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
        df['sitting'] = df['date_sitting'].apply(lambda x: get_sitting(x))
        df['publication_gu_date'] = df['text_decree'].apply(get_date_gu)
        df['publication_gu_date'] = df['publication_gu_date'].apply(lambda x: datetime.strptime(x, '%d-%m-%Y'))
        df['bill'] = df.apply(lambda x: self.get_bill(x['act'], x['date_sitting']), axis=1)
        rome_timezone = pytz.timezone('Europe/Rome')
        df['date_sitting'] = df['date_sitting'].apply(lambda x: rome_timezone.localize(x))

        return df


class GovDecreeTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self):
        Transformation.__init__(self)

    def transform(self):
        od = self.etl.original_data.copy()
        od = od[['original_title', 'descriptive_title', 'identifier',
                 'sitting', 'publication_gu_date']]
        self.etl.processed_data = od

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql for Gov Sittings."""

    help = ""

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.setup_logger(__name__, formatter_key="simple", **options)

        soup_data = get_soup('https://www.parlamento.it/leg/ldl_new/v3/sldlelencodlconvers.htm')
        df = GovDecreeExtractor(soup_data).extract()
        if df.empty:
            return None
        ETL(
            extractor=DataframeExtractor(df),
            transformation=GovDecreeTransformation(),
            loader=DjangoUpdateOrCreateLoader(django_model=GovDecree, fields_to_update=['descriptive_title',
                                                                                        'sitting',
                                                                                        'publication_gu_date',
                                                                                        'original_title'
                                                                                        ]),
        )()
        for index, item in df.iterrows():
            gd = GovDecree.objects.get(identifier=item['identifier'])
            try:
                gd.add_next(item['bill'])
            except IntegrityError:
                pass
