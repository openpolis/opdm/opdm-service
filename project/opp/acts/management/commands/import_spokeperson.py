from config.settings.base import ENDPOINTS_ASSEMBLEE_PARLAMENTARI

from project.tasks.parsing.sparql.utils import get_bindings
from taskmanager.management.base import LoggingBaseCommand

from project.opp.acts.management.commands.raw_sparql.template_query import template_query_sparql
from project.opp.acts.models import BillSpeaker, Bill
from django.db.models import Q
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor, Extractor
from .import_signers_acts import get_parlamentare, get_parlamentare_id

from popolo.models import Membership as OPDMMembership
import pandas as pd
import pytz
import datetime


def get_camera_parl(parl_camera, dep, parl_source):
    if not parl_source:
        res = dep[dep.parl_name.apply(lambda x: x.lower() == parl_camera.replace('On. Giandiego Gatta',
                                                                                 'On. GIACOMO DIEGO GATTA')
                                      .replace('On. Pino Bicchielli',
                                               'On. Giuseppe Bicchielli').replace('On. Andrea Orsini',
                                                                                  'On. ANDREA GIORGIO FELICE MARIA ORSINI').replace('On. Gloria Saccani Jotti',
                                                                                                                                    'On. GLORIA SACCANI')
                                      .lower())]
        if res.shape[0] != 1:
                raise ValueError
        else:
            return res.d.values[0]
    else:
        return parl_source


def get_unit(is_commission):
    return BillSpeaker.COMMISSION if int(is_commission) == 1 else BillSpeaker.ASSEMBLY


def get_bill(identifier, assemblea, leg):

    return Bill.objects.get(identifier__iexact=f'{assemblea}.{identifier}',
                            assembly__identifier__endswith=leg)


class JsonExtractor(Extractor):
    """Json Extractor
    """
    def __init__(self, df):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.df = df
    def extract(self):
        res = []
        for r in self.df:
            res.append(dict((k, r[k]["value"]) for k in r.keys()))
        od = pd.DataFrame(res)
        return od


class SpeakerTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, aula, legislatura, deputati):
        Transformation.__init__(self)
        self.aula = aula
        self.legislatura = legislatura
        self.deputati = deputati

    def transform(self):
        od = self.etl.original_data.copy()
        od = od.where(pd.notnull(od), None)
        od['parlamentare'] = od.apply(lambda x: get_camera_parl(x['presentatore'], self.deputati, x['parlamentare']),
                                      axis=1)
        od['bill'] = od.apply(lambda x: get_bill(x['identifier'], x['ramo'], self.legislatura), axis=1)

        od['unit'] = od['is_commissione'].apply(get_unit)
        od['is_minority'] = od['diMinoranza'].apply(lambda x: int(x) == 1)

        memberships = OPDMMembership.objects.filter(Q(role__icontains='deputato') |
                                                    Q(role__icontains='senatore')) \
            .filter(organization__classification='Assemblea parlamentare',
                    organization__identifier__regex=self.legislatura)

        memberships = pd.DataFrame(memberships
                                   .filter(person__identifiers__identifier__icontains='dati')
                                   .values('id',
                                           'start_date',
                                           'end_date',
                                           'person__identifiers__identifier')
                                   )
        od['parlamentare'] = od['parlamentare'].apply(get_parlamentare_id)
        od['membership'] = od.apply(lambda x: get_parlamentare(
            x['parlamentare'],
            x['dataPresentazione'], memberships), axis=1)

        od['appointing_date'] = od['dataNomina']\
            .apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d') if x else None)
        rome_timezone = pytz.timezone('Europe/Rome')
        od['appointing_date'] = od['appointing_date']\
            .apply(lambda x: rome_timezone.localize(x) if not pd.isnull(x) else None)
        od.replace({pd.NaT: None}, inplace=True)
        self.etl.processed_data = od[['bill', 'unit', 'is_minority', 'membership', 'appointing_date']]

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        tipologia = 'spokepersons_acts'
        legislatura_padded = str(legislatura).zfill(2)

        def get_query(**kwargs):
            query = template_query_sparql(name='spokepersons_acts', **kwargs)
            return get_bindings(
                    ENDPOINTS_ASSEMBLEE_PARLAMENTARI.get(kwargs.get('aula')),
                    query,
                    legacy=True
            )

        aula = 'senato'
        self.logger.debug(f'Getting {aula}')
        data = JsonExtractor(get_query(legislatura=legislatura,
                                       tipologia=tipologia,
                                       aula=aula)).extract()

        deputati = JsonExtractor(get_query(legislatura=legislatura,
                                           tipologia=tipologia,
                                           aula='camera')).extract()
        deputati["parl_name"] = "On. " + deputati['firstName'] + " " + deputati['surname']
        deputati.drop(columns=['firstName', 'firstName'], inplace=True)
        if data.shape[0] == 0:
            self.logger.warning(f'No data for {aula} legislation {legislatura} for category {tipologia}')

        else:
            ETL(
                extractor=DataframeExtractor(data),
                transformation=SpeakerTransformation(aula=aula, legislatura=legislatura_padded,
                                                     deputati=deputati),
                loader=DjangoUpdateOrCreateLoader(django_model=BillSpeaker, fields_to_update=['is_minority',
                                                                                              'appointing_date']),
            )()
