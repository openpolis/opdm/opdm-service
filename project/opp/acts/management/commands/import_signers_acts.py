from config.settings.base import ENDPOINTS_ASSEMBLEE_PARLAMENTARI

from django.db.models import Q
from project.tasks.parsing.sparql.utils import get_bindings
from project.calendars.models import CalendarDaily
from taskmanager.management.base import LoggingBaseCommand

from project.opp.acts.management.commands.raw_sparql.template_query import template_query_sparql
from project.opp.acts.models import Bill, BillSigner
from difflib import SequenceMatcher

from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor, Extractor

from popolo.models import KeyEvent, Organization, Membership as OPDMMembership
import pandas as pd
import pytz
import datetime
import re



def similar(a, b):
    return SequenceMatcher(None, a.lower(), b.lower()).ratio()


def get_governo(gov_string):
    gov = gov_string.split('-')
    return Organization.objects.get(
        classification='Governo della Repubblica',
        name__iexact=' Governo '.join(gov[::-1])
    )


def get_membro_governo(pres, data, gov_memberships):
    gov_string = re.findall(r'Gov. (.*)\)', pres)[0]
    gov = get_governo(gov_string)
    pres = pres.split('(')[0].strip()
    pres = pres.replace('Pres.', 'Presidente')

    res = gov_memberships[gov_memberships['organization'] == gov.id]
    res['pres_min'] = res['label'] + ' ' + res['person__name']
    res['sim'] = [similar(x, pres) for x in res['pres_min']]
    id = res.sort_values(by='sim').iloc[-1]['id']
    return OPDMMembership.objects.get(id=id)


def get_parlamentare_id(parl):
    try:
        identifier = parl.split('_')[0].split('/')[-1]
        identifier = re.findall(r'\d+', identifier)
        return identifier[0]
    except:
        return None


def get_parlamentare(parl, data, memberships):
    today = datetime.datetime.now()
    today_strf = today.strftime('%Y-%m-%d')
    res = memberships[memberships.person__identifiers__identifier.str.contains('/' + r'[pd]{0,1}' + parl + '$')]
    if res.shape[0] == 1:
        return OPDMMembership.objects.get(id=res['id'])
    res['end_date'] = res['end_date'].fillna(today_strf)
    res = res[(res['start_date'] <= data) & (res['end_date'] > data)]
    return OPDMMembership.objects.get(id=res['id'])


class JsonExtractor(Extractor):
    """Json Extractor
    """

    def __init__(self, df):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.df = df

    def extract(self):
        res = []
        for r in self.df:
            res.append(dict((k, r[k]["value"]) for k in r.keys()))
        od = pd.DataFrame(res)
        return od


class BillSignerTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, assemblee, legislatura):
        Transformation.__init__(self)
        self.assemblee = assemblee
        self.legislatura = legislatura

    def transform(self):
        od = self.etl.original_data.copy()
        columns_possibly_missing = ['dataAggiuntaFirma',
                                    'dataRitiroFirma',
                                    'primoFirmatario',
                                    'deputato',
                                    'senatore']
        for column in columns_possibly_missing:
            if column not in od.columns:
                od[column] = None
        od = od.reindex(
            od.columns.union(
                ['dataAggiuntaFirma',
                 'dataRitiroFirma'], sort=False), axis=1, fill_value=None)
        od = od.where(pd.notnull(od), None)
        od = od[od['tipoiniziativa'].isin(['Parlamentare', 'Governativa'])]
        od['adding_date'] = od['dataAggiuntaFirma']\
            .apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d') if x else None)
        od['withdrawal_date'] = od['dataRitiroFirma']\
            .apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d') if x else None)
        rome_timezone = pytz.timezone('Europe/Rome')
        od['adding_date'] = od['adding_date']\
            .apply(lambda x: rome_timezone.localize(x) if not pd.isnull(x) else None)
        od['withdrawal_date'] = od['withdrawal_date']\
            .apply(lambda x: rome_timezone.localize(x) if not pd.isnull(x) else None)
        od['first_signer'] = od['primoFirmatario'].apply(lambda x: str(x) == '1')
        od['bill'] = od['idFase'].apply(lambda x: Bill.objects.get(source_identifier=x))

        memberships = OPDMMembership.objects.filter(Q(role__icontains='deputato') |
                                                    Q(role__icontains='senatore')) \
            .filter(organization__classification='Assemblea parlamentare',
                    organization__identifier__regex=self.legislatura)\
            .select_related('identifiers')

        memberships = pd.DataFrame(memberships
                                   .filter(person__identifiers__identifier__icontains='dati')
                                   .values('id',
                                           'start_date',
                                           'end_date',
                                           'person__identifiers__identifier')
                                   )

        parl = od.copy()
        parl = parl[parl['tipoiniziativa'] == 'Parlamentare']
        parl['parlamentare'] = parl[['deputato', 'senatore']].bfill(axis=1).iloc[:, 0]
        parl['parlamentare'] = parl['parlamentare'].apply(get_parlamentare_id)
        parl = parl[parl.parlamentare.notnull()] #TODO da rimuovere fix momentaneo errore sorgente
        if parl.shape[0]>0:
            parl['membership'] = parl.apply(lambda x: get_parlamentare(
            x['parlamentare'],
            x['dataPresentazione'], memberships), axis=1)
        parl.replace({pd.NaT: None}, inplace=True)

        gov = od.copy()
        gov = gov[gov['tipoiniziativa'] == 'Governativa']
        if not gov.empty:
            gov_memberships = OPDMMembership.objects.filter(
                Q(role__iexact='Ministro') |
                Q(role__iexact='Presidente del Consiglio') |
                Q(role__iexact='Ministro senza portafoglio'))
            gov_memberships = pd.DataFrame(gov_memberships
                                           .values('id',
                                                   'label',
                                                   'start_date',
                                                   'end_date',
                                                   'person__name',
                                                   'organization'))
            gov['membership'] = gov.apply(lambda x: get_membro_governo(x['presentatore'],
                                                                       x['dataPresentazione'], gov_memberships), axis=1)
            gov.replace({pd.NaT: None}, inplace=True)
            frames = [parl, gov]
            result = pd.concat(frames)
        else:
            result = parl

        self.etl.processed_data = result[[
            'first_signer',
            'bill',
            'membership',
            'adding_date',
            'withdrawal_date']]

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = "Importing signers for bills given legislature"

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        legislatura_padded = str(legislatura).zfill(2)
        legislature_identifier = f"ITL_{legislatura_padded}"

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        year_months = CalendarDaily.objects.filter(date__gte=e.start_date,
                                                   date__lte=(e.end_date or today_strf))\
            .values_list('year',
                         'month').distinct().order_by('year', 'month')
        tipologia = 'presenting_acts'

        self.logger.info("Getting acts FULL")

        def get_query(**kwargs):
            query = template_query_sparql(name=tipologia, **kwargs)
            return get_bindings(
                    ENDPOINTS_ASSEMBLEE_PARLAMENTARI.get(kwargs.get('aula')),
                    query,
                    legacy=True
            )

        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=legislatura_padded)
        self.logger.debug('Getting signers for acts')
        for year, month in list(year_months):
            year_month = f'{year}-{str(month).zfill(2)}'
            self.logger.debug(f'{year_month}')
            data = JsonExtractor(get_query(legislatura=legislatura,
                                           tipologia=tipologia,
                                           aula='senato',
                                           year_month=year_month)).extract()

            if data.shape[0] == 0:
                self.logger.warning(f'No data for legislation {legislatura} for category {tipologia}')
                continue
            data = data.where(pd.notnull(data), None)
            data = data[data['tipoiniziativa'].isin(['Parlamentare', 'Governativa'])]
            if data.shape[0] <= BillSigner.objects.filter(bill__date_presenting__gte=f"{year_month}-01",
                                                          bill__date_presenting__lte=CalendarDaily.objects.get(year=year,
                                                                                                               month=month,
                                                                                                               is_month_end=True).date.strftime('%Y-%m-%d')).count():
                continue
            ETL(
                extractor=DataframeExtractor(data),
                transformation=BillSignerTransformation(assemblee=assemblee,
                                                        legislatura=legislatura_padded),
                loader=DjangoUpdateOrCreateLoader(django_model=BillSigner, fields_to_update=['first_signer',
                                                                                             'adding_date',
                                                                                             'withdrawal_date']
                                                  ),
            )()
