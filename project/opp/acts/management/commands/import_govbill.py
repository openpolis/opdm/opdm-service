from bs4 import BeautifulSoup
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from datetime import datetime
from project.opp.acts.models import Bill, GovBill, GovSitting
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor

import locale
import pandas as pd
import pytz
import requests
import re
from taskmanager.management.base import LoggingBaseCommand

locale.setlocale(locale.LC_ALL, 'it_IT.utf-8')

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) " \
             "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"

HTTP_PROXIES = {
    'http': f'http://{settings.HTTP_PROXY_USERNAME}:{settings.HTTP_PROXY_PASSWORD}@'
            f'{settings.HTTP_PROXY_HOST}:{settings.HTTP_PROXY_HOST_PORT}',
    'https': f'http://{settings.HTTP_PROXY_USERNAME}:{settings.HTTP_PROXY_PASSWORD}@'
             f'{settings.HTTP_PROXY_HOST}:{settings.HTTP_PROXY_HOST_PORT}',
}


def get_govbill(identifier):
    try:
        GovBill.objects.get(identifier=identifier)
        return True
    except ObjectDoesNotExist:
        return False


def get_identifier(url):
    values = re.findall(r'decreto.legislativo:(.*)', url)[0].split(";")
    return '-'.join(list(map(lambda x: str(int(x)), values)))


def get_soup(url):
    page = requests.get(url, proxies=HTTP_PROXIES, headers={"user-agent": USER_AGENT})
    soup = BeautifulSoup(page.content, "html.parser")
    return soup


def get_bill_date(description):
    regex = r'DECRETO LEGISLATIVO (\d{1,2}) (\w+) (\d{4})'
    matches = re.findall(regex, description, re.MULTILINE)
    return ' '.join(matches[0])


def get_date_gu(description):
    regex = r'\(GU n.\d+ del (\d+-\d+-\d+)'
    matches = re.findall(regex, description, re.MULTILINE)
    return matches[0]


def get_sitting(date):
    try:
        return GovSitting.objects.filter(starting_datetime__date=date).first()
    except Exception:
        return None


def get_decision_sitting(description):
    description = description.replace('\n', ' ')
    description = re.sub(' +', ' ', description).strip()
    regex = r'(?i)Vist.* deliberazion[ei] .{1,20}Consiglio.+riunion[ei] del[ ]*(?:l\')*[ ]*(\d+)°* (\w+ \d+)'
    matches = re.findall(regex, description, re.MULTILINE)
    if not matches:
        return None
    return ' '.join(matches[0])


def get_preliminary_sitting(description):
    description = description.replace('\n', ' ')
    description = re.sub(' +', ' ', description).strip()
    regex = r'Vista la preliminare.{0,70}riunione del[ ]*(?:l\')*[ ]*(\d+)°* (\w+ \d+)'
    matches = re.findall(regex, description, re.MULTILINE)
    if not matches:
        return None
    return ' '.join(matches[0])


def get_delegation_laws(description, descriptive_title):

    descriptive_title = descriptive_title.replace('\n', ' ')
    descriptive_title = re.sub(' +', ' ', descriptive_title).strip()
    regex = r'[(?:ai sensi)|(?:in attuazione)].{0,2}legge \d+ \w+ (\d{4}).{0,2}n\.[ ]*(\d+)'
    matches = re.findall(regex, descriptive_title, re.MULTILINE)
    if not matches:
        description = description.replace('\n', ' ')
        description = re.sub(' +', ' ', description).strip()
        regex = r' Vist.{0,80}legge.{0,10}\d+°* \w+ (\d{4}).{0,10}n\.[ ]*(\d+).{0,90}(?i)deleg'
        matches = re.findall(regex, description, re.MULTILINE)
    return list(set(matches))


class GovBillExtractor(DataframeExtractor):

    def get_bill(self, act, date):
        try:
            return Bill.objects.get(identifier=act, date_presenting__gte=date)
        except ObjectDoesNotExist:
            pass

    @staticmethod
    def get_text_decree(href):
        soup = get_soup(href)
        atto_orig = soup.find(text=re.compile('originario'))
        href_atto_orig = atto_orig.parent.get('href')
        soup_atto_orig = get_soup(f"https://www.normattiva.it/{href_atto_orig}")
        return soup_atto_orig.get_text()

    def extract(self):
        soup = self.df

        dls = soup.find_all('dt')
        urls = [x.find('a').get('href') for x in dls]
        identifier = list(map(get_identifier, urls))
        titoli = soup.find_all('dd')

        df = pd.DataFrame(list(zip(urls,
                                   identifier,
                                   dls,
                                   titoli)),
                          columns=['url',
                                   'identifier',
                                   'dls',
                                   'titoli'])
        already_existing = set(GovBill.objects.all().values_list('identifier', flat=True))
        identifier = list(set(identifier) - already_existing)
        df = df[df['identifier'].isin(identifier)]
        if df.empty:
            return df

        df['text_decree'] = df['url'].apply(lambda x: self.get_text_decree(x))
        df['original_title'] = df['dls'].apply(lambda x: x.find('a').get_text())
        df['descriptive_title'] = df['titoli'].apply(lambda x: x.find('p').get_text())
        df['date_sitting_decision'] = df['text_decree'].apply(lambda x: get_decision_sitting(x))
        df['date_sitting_decision'] = df['date_sitting_decision'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
        df['decision_sitting'] = df['date_sitting_decision'].apply(lambda x: get_sitting(x))

        df['date_sitting_preliminary'] = df['text_decree'].apply(lambda x: get_preliminary_sitting(x))
        df['date_sitting_preliminary'] = df['date_sitting_preliminary'].apply(
            lambda x: datetime.strptime(x, '%d %B %Y') if x else None)
        df['preliminary_sitting'] = df['date_sitting_preliminary'].apply(lambda x: get_sitting(x) if x else None)

        df['bill_date'] = df['text_decree'].apply(get_bill_date)
        df['publication_gu_date'] = df['text_decree'].apply(get_date_gu)
        df['publication_gu_date'] = df['publication_gu_date'].apply(lambda x: datetime.strptime(x, '%d-%m-%Y'))
        rome_timezone = pytz.timezone('Europe/Rome')
        df['bill_date'] = df['bill_date'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
        df['bill_date'] = df['bill_date'].apply(lambda x: rome_timezone.localize(x))

        df['delegation_laws'] = df.apply(lambda x: get_delegation_laws(x['text_decree'], x['descriptive_title']),
                                         axis=1)

        return df


class GovBillTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, type):
        Transformation.__init__(self)
        self.type = type

    def transform(self):
        od = self.etl.original_data.copy()
        od = od[['identifier', 'original_title', 'descriptive_title',
                 'decision_sitting', 'preliminary_sitting', 'bill_date',
                 'publication_gu_date']]
        od['type'] = self.type
        self.etl.processed_data = od

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql for Gov Sittings."""

    help = ""

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.setup_logger(__name__, formatter_key="simple", **options)
        base = 'https://www.parlamento.it/'
        parlamento_soup = get_soup(f'{base}/home')
        leggi_url = base + parlamento_soup.find_all(string='Leggi')[0].parent.get('href')
        leggi_soup = get_soup(leggi_url)
        dlgs_url = leggi_soup.find_all(string='Decreti legislativi')[0].parent.get('href')

        for code, desc in GovBill.BILL_TYPES:
            if code == GovBill.DELEGATION:
                type = 1
            elif code == GovBill.DIRECTIVE:
                type = 2
            elif code == GovBill.REGIONAL_STAT:
                type = 3

            for anno in list(range(datetime.now().year-1, datetime.now().year+1)):
                dlgs_url_year = dlgs_url + f'?tipo_ricerca=anno_tipo&tipologia={type}&anno={anno}'
                dlsg_soup = get_soup(dlgs_url_year)

                df = GovBillExtractor(dlsg_soup).extract()
                if df.empty:
                    continue
                ETL(
                    extractor=DataframeExtractor(df),
                    transformation=GovBillTransformation(code),
                    loader=DjangoUpdateOrCreateLoader(django_model=GovBill, fields_to_update=['original_title',
                                                                                              'descriptive_title',
                                                                                              'bill_date',
                                                                                              'publication_gu_date',
                                                                                              'decision_sitting',
                                                                                              'preliminary_sitting',
                                                                                              'type']),
                )()
                if code != GovBill.REGIONAL_STAT:
                    for index, item in df.iterrows():
                        for year, law_number in item['delegation_laws']:
                            bill = Bill.objects.get(law_number=law_number, law_publication_date__icontains=year)
                            try:
                                bill.add_next(GovBill.objects.get(identifier=item['identifier']))
                            except IntegrityError:
                                pass
