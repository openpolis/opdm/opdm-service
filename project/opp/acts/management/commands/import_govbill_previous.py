from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime
from project.opp.acts.models import Bill, GovBill
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor
from .import_govbill import get_soup, get_preliminary_sitting, get_delegation_laws, get_date_gu, get_sitting, \
    get_bill_date, get_decision_sitting
import locale
import pandas as pd
import pytz
import re
from taskmanager.management.base import LoggingBaseCommand

locale.setlocale(locale.LC_ALL, 'it_IT.utf-8')


def get_max_law(source, max_value=300, step=100, down=True):
    data = get_soup(f'{source};{max_value})')
    res = 'Errore nel caricamento delle informazioni' in data.get_text()
    if step == 1:
        if res:
            return max_value - 1
        else:
            return max_value
    if res:
        step = round(step/(2-down*1))
        max_value = max(1, max_value - step)
        return get_max_law(source, max_value, step, True)
    else:
        step = round(step/(down*1+1))
        max_value = max(1, max_value + step)
        return get_max_law(source, max_value, step, False)


def get_original_title(soup_data):
    title = soup_data.find('div', id='titoloAtto').get_text()
    title = title.replace('\n', ' ').replace('\t', ' ').replace('\r', ' ')
    title = re.sub(' +', ' ', title).strip()
    regex = r'(.*\. \d+)'
    matches = re.findall(regex, title, re.MULTILINE)
    return matches[0]


def get_descriptive_title(soup_data):
    title = soup_data.find('h3').get_text()
    title = title.replace('\n', ' ').replace('\t', ' ').replace('\r', ' ')
    title = re.sub(' +', ' ', title).strip()
    return title


def get_type_dlgs(descriptive_title):
    descriptive_title = descriptive_title.lower()
    if ('regione' in descriptive_title or 'statuto speciale' in descriptive_title) and 'norme di attuazione':
        return GovBill.REGIONAL_STAT
    if ('direttiva' in descriptive_title or 'regolamento' in descriptive_title) and '(ue)' in descriptive_title:
        return GovBill.DIRECTIVE
    else:
        return GovBill.DELEGATION


class PreviousGovBillExtractor(DataframeExtractor):

    def get_bill(self, act, date):
        try:
            return Bill.objects.get(identifier=act, date_presenting__gte=date)
        except ObjectDoesNotExist:
            pass

    @staticmethod
    def get_text_decree(href):
        soup = get_soup(href)
        atto_orig = soup.find(text=re.compile('Mostra Atto Originario'))
        href_atto_orig = atto_orig.parent.get('href')
        soup_atto_orig = get_soup(f"https://www.normattiva.it/{href_atto_orig}")
        return soup_atto_orig

    def extract(self):
        df = pd.DataFrame(self.df, columns=['url'])

        df['identifier'] = df['url'].apply(lambda x: re.findall(r'decreto.legislativo:(.*)', x)[0].replace(';', '-'))
        df['text_decree'] = df['url'].apply(self.get_text_decree)
        # import pickle
        # import sys
        # sys.setrecursionlimit(20000)

        # with open('df_test.pickle', 'wb') as handle:
        #     pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)

        # with open('df_test.pickle', 'rb') as handle:
        #     df = pickle.load(handle)

        df['original_title'] = df['text_decree'].apply(get_original_title)
        df['descriptive_title'] = df['text_decree'].apply(get_descriptive_title)
        df = df[df['original_title'].str.contains('DECRETO LEGISLATIVO')]
        if df.empty:
            return df
        df['text_decree'] = df['text_decree'].apply(lambda x: x.get_text())
        df['date_sitting_decision'] = df['text_decree'].apply(lambda x: get_decision_sitting(x))
        df['date_sitting_decision'] = df['date_sitting_decision'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
        df['decision_sitting'] = df['date_sitting_decision'].apply(lambda x: get_sitting(x))

        df['date_sitting_preliminary'] = df['text_decree'].apply(lambda x: get_preliminary_sitting(x))
        df['date_sitting_preliminary'] = df['date_sitting_preliminary'].apply(
            lambda x: datetime.strptime(x, '%d %B %Y') if x else None)
        df['preliminary_sitting'] = df['date_sitting_preliminary'].apply(lambda x: get_sitting(x) if x else None)

        df['bill_date'] = df['text_decree'].apply(get_bill_date)
        df['publication_gu_date'] = df['text_decree'].apply(get_date_gu)
        df['publication_gu_date'] = df['publication_gu_date'].apply(lambda x: datetime.strptime(x, '%d-%m-%Y'))
        rome_timezone = pytz.timezone('Europe/Rome')
        df['bill_date'] = df['bill_date'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
        df['bill_date'] = df['bill_date'].apply(lambda x: rome_timezone.localize(x))

        df['delegation_laws'] = df.apply(lambda x: get_delegation_laws(x['text_decree'],
                                                                       x['descriptive_title']), axis=1)
        df['type'] = df['descriptive_title'].apply(get_type_dlgs)
        return df


class GovBillTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self):
        Transformation.__init__(self)

    def transform(self):
        od = self.etl.original_data.copy()
        od = od.drop(['url', 'text_decree',
                      'date_sitting_decision',
                      'date_sitting_preliminary',
                      'delegation_laws'], axis=1)
        self.etl.processed_data = od

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql for Gov Sittings."""

    help = ""

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.setup_logger(__name__, formatter_key="simple", **options)
        base = 'https://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:'

        for anno in [2018, 2019, 2020, 2021, 2022, 2023]:
            url = f'{base}{anno}'
            max_value = get_max_law(url)
            indeces = list(range(1, max_value+1))
            url_laws = map(lambda x: f'https://www.normattiva.it'
                                     f'/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:{anno};{x}', indeces)

            df = PreviousGovBillExtractor(list(url_laws)).extract()
            if df.empty:
                continue
            ETL(
                extractor=DataframeExtractor(df),
                transformation=GovBillTransformation(),
                loader=DjangoUpdateOrCreateLoader(django_model=GovBill, fields_to_update=['original_title',
                                                                                          'descriptive_title',
                                                                                          'bill_date',
                                                                                          'publication_gu_date',
                                                                                          'decision_sitting',
                                                                                          'preliminary_sitting',
                                                                                          'type']),
            )()

            for index, item in df.iterrows():
                if item['type'] == GovBill.REGIONAL_STAT:
                    continue
                for year, law_number in item['delegation_laws']:
                    bill = Bill.objects.get(law_number=law_number, law_publication_date__icontains=year)
                    try:
                        bill.add_next(GovBill.objects.get(identifier=item['identifier']))
                    except IntegrityError:
                        pass
