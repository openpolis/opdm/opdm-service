from taskmanager.management.base import LoggingBaseCommand
from django.core.management import call_command
import time
import logging


class Command(LoggingBaseCommand):
    logger = logging.getLogger(f"project.{__name__}")
    help = "It loads the gov sittings of the previous governments" \
           "from archive web site https://www.sitiarcheologici.palazzochigi.it/"

    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.

        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        self.logger.info(f"----- Starting {command_name} -----")
        start_time = time.time()
        call_command(command_name, *args, **options, stdout=self.stdout)
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        self.logger.setLevel(
            {
                0: logging.ERROR,
                1: logging.WARNING,
                2: logging.INFO,
                3: logging.DEBUG,
            }.get(options["verbosity"])
        )

        self.logger.info("Load previous Gov Sittings")
        domains = {
            'Draghi I': 'https://www.sitiarcheologici.palazzochigi.it/www.governo.it/ottobre2022/www.governo.it/it/',
            'Conte II': 'https://www.sitiarcheologici.palazzochigi.it/www.governo.it/febbraio%202021/it/',
            'Conte I': 'https://www.sitiarcheologici.palazzochigi.it/www.governo.it/settembre%202019/it/',
            'Gentiloni I': 'https://www.sitiarcheologici.palazzochigi.it/www.governo.it/giugno%202018/'}
        self.call_command('import_prev_govsitting',
                          url_domains=list(domains.values()),
                          verbosity=3)
