from django.db.models import Q

from project.opp.acts.models import Bill, GovDecree, GovBill
import datetime
from taskmanager.management.base import LoggingBaseCommand

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):
    help = 'Create materialized view if it does not exist'

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **kwargs):
        leg = kwargs["legislatura"]
        Bill.update_cache_get_api_aggregate()
        GovDecree.update_cache_get_api_aggregate(leg=leg)
        GovBill.update_cache_get_api_aggregate(leg=leg)

        not_law = [{
            'id': x.id,
            'appr_bef': x.last_act_approved_before()} for x in Bill.get_last_bill(leg=leg)]
        ids_law_not_published = [x.id for x in Bill.get_last_bill()
                                 if x.last_status().phase.phase == 'Approvato definitivamente non ancora pubblicato']
        Bill.objects.all().filter(Q(is_law=True)|Q(id__in=ids_law_not_published)).update(status=Bill.LAW)

        one_branch_ids = [x['id'] for x in not_law if x['appr_bef']]
        Bill.get_last_bill().filter(id__in=one_branch_ids).exclude(Q(is_law=True)|Q(id__in=ids_law_not_published)).update(status=Bill.ONE_BRANCH)

        first_phase = Bill.get_last_bill().exclude(id__in=one_branch_ids).exclude(Q(is_law=True)|Q(id__in=ids_law_not_published))
        started = [x.id for x in first_phase if x.last_status().phase.phase not in ['Assegnato (no esame)',
                                                                                    'Da assegnare a commissione',
                                                                                    'Ritirato',
                                                                                    'Respinto',
                                                                                    'Restituito al governo',
                                                                                    'D-L decaduto'
                                                                                    ]]

        started_objects = Bill.get_last_bill().filter(id__in=started)
        Bill.get_last_bill().filter(id__in=[x.id for x in started_objects]).update(status=Bill.EXAMINATION)

        to_start = [x.id for x in first_phase if x.last_status().phase.phase in ['Assegnato (no esame)',
                                                                                 'Da assegnare a commissione']]
        to_start_objects = Bill.get_last_bill().filter(id__in=to_start)

        Bill.get_last_bill().filter(id__in=[x.id for x in to_start_objects]).update(status=Bill.TO_START)

        rejected = Bill.get_last_bill().exclude(Q(is_law=True)|Q(id__in=ids_law_not_published)) \
            .exclude(id__in=[x.id for x in to_start_objects] + [x.id for x in started_objects] + one_branch_ids)
        to_reject = [x.id for x in rejected if x.last_status().phase.phase in ['Ritirato',
                                                                               'Respinto',
                                                                               'Restituito al governo',
                                                                               'D-L decaduto'
                                                                               ]]
        Bill.get_last_bill().filter(id__in=to_reject).update(status=Bill.REJECTED)

        Bill.update_previous_codes(leg)
