from config.settings.base import ENDPOINTS_ASSEMBLEE_PARLAMENTARI
from django.core.exceptions import ObjectDoesNotExist

from project.tasks.parsing.sparql.utils import get_bindings
from project.calendars.models import CalendarDaily
from taskmanager.management.base import LoggingBaseCommand

from project.opp.acts.management.commands.raw_sparql.template_query import template_query_sparql
from project.opp.acts.models import Bill, Iter, IterPhase

from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor, Extractor

from popolo.models import KeyEvent, Organization
import pandas as pd
import datetime


def get_is_ratification(is_ratification, title, source_identifier):
    try:
        bill = Bill.objects.get(source_identifier=source_identifier)
        return bill.is_ratification
    except ObjectDoesNotExist:
        if str(is_ratification) == '1' or title.lower().startswith('ratifica'):
            return True
        return False


def get_assembly(assemblee, ramo):
    if ramo == 'C':
        return assemblee[0]
    elif ramo == 'S':
        return assemblee[1]
    else:
        raise NotImplementedError


def map_initiative(initiative):
    map_initiative = {
        'Popolare': Bill.POPULAR,
        'Parlamentare': Bill.PARLIAMENT,
        'Regionale': Bill.REGION,
        'Governativa': Bill.GOVERNMENT,
        'CNEL': Bill.CNEL
    }
    return map_initiative.get(initiative)


def get_iter_phase(stato):
    mappings = {
        'approvato': 'Approvato',
        'appr.quest.sosp.': 'Approvazione questione sospensiva',
        'appr. quest. sosp.': 'Approvazione questione sospensiva',
        'appr. con modificaz': 'Approvato con modificazioni',
        'approvato con modificazioni': 'Approvato con modificazioni',
        'appr. definit. Legge': 'Approvato definitivamente legge',
        'approvato definitivamente. Legge': 'Approvato definitivamente legge',
        'appr. def. non pubbl': 'Approvato definitivamente non ancora pubblicato',
        'approvato definitivamente, non ancora pubblicato': 'Approvato definitivamente non ancora pubblicato',
        'appr. in t.u.': 'Approvato in testo unificato',
        'approvato in testo unificato': 'Approvato in testo unificato',
        'appr. non pass. art.': 'Approvato non passaggio articoli',
        'assegnato (no esame)': 'Assegnato (no esame)',
        'Assegnato (non ancora iniziato l\'esame)': 'Assegnato (no esame)',
        'assegnato (non ancora iniziato l\'esame)': 'Assegnato (no esame)',
        'assorbito': 'Assorbito',
        'cancellato dall\'OdG': 'Cancellato dall\'ODG',
        'cancellato dall\'Ordine del Giorno': 'Cancellato dall\'ODG',
        'concl. anomala stral': 'Conclusione anomala per stralcio',
        'conclusione anomala per stralcio': 'Conclusione anomala per stralcio',
        'da assegn. a commis.': 'Da assegnare a commissione',
        'da assegnare': 'Da assegnare a commissione',
        'D-L decaduto': 'D-L decaduto',
        'decreto legge decaduto': 'D-L decaduto',
        'concluso l\'esame': 'Esame concluso in commissione',
        'concluso l\'esame da parte della commissione': 'Esame concluso in commissione',
        'all\'esame assemblea': 'Esame in assemblea',
        'all\'esame dell\'assemblea': 'Esame in assemblea',
        'esame in comm.': 'Esame in commissione',
        'in corso di esame in commissione': 'Esame in commissione',
        'in relazione': 'In relazione',
        'in stato di relazione': 'In relazione',
        'insussistenza quorum': 'Insussistenza quorum',
        'verificata insussistenza del quorum': 'Insussistenza quorum',
        'respinto': 'Respinto',
        'restit. al Governo': 'Restituito al governo',
        'restituito al Governo per essere ripresentato all\'altro ramo': 'Restituito al governo',
        'rimesso assemblea': 'Rimesso all\'assemblea',
        'rimesso all\'Assemblea': 'Rimesso all\'assemblea',
        'rinviato camere PdR': 'Rinviato camere',
        'rinviato alle Camere dal Presidente della Repubblica': 'Rinviato camere',
        'rinviato ass.->comm.': 'Rinviato in commissione',
        'rinviato dall\'assemblea in commissione': 'Rinviato in commissione',
        'ritirato': 'Ritirato'
    }
    obj = IterPhase.objects.get(
        phase=mappings.get(stato)
    )
    return obj


def get_type(natura):
    if natura == 'ordinaria':
        return Bill.ORDINARY
    elif natura == 'costituzionale':
        return Bill.COSTITUTIONAL
    elif natura == 'di conversione di decreto-legge':
        return Bill.DL_CONVERSION
    elif natura == 'di approvazione di bilancio':
        return Bill.FINANCIAL
    else:
        raise NotImplementedError


def is_law(stato, numero, data, type):
    if stato == 'appr. definit. Legge' or stato == 'approvato definitivamente. Legge':
        url_cost = ''
        if type == Bill.COSTITUTIONAL:
            url_cost = '.costituzionale'
        return (True,
                f'https://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:legge{url_cost}:{data[:4]};{numero}',
                numero,
                data)
    else:
        return (False,
                None,
                None,
                None)


class JsonExtractor(Extractor):
    """Json Extractor
    """

    def __init__(self, df):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.df = df

    def extract(self):
        res = []
        for r in self.df:
            res.append(dict((k, r[k]["value"]) for k in r.keys()))
        od = pd.DataFrame(res)
        return od


class BillTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, assemblee):
        Transformation.__init__(self)
        self.assemblee = assemblee

    def transform(self):
        od = self.etl.original_data.copy()
        od = od.reindex(
            od.columns.union(
                ['ramo', 'fase', 'titolo', 'tipoiniziativa', 'idDdl',
                 'dataPresentazione', 'statoDdl', 'dataStatoDdl', 'idFase', 'natura',
                 'numeroLegge', 'dataLegge', 'titoloBreve', 'is_ratification'], sort=False), axis=1, fill_value=None)
        od['identifier'] = od['fase']
        od['type'] = od['natura'].apply(get_type)
        od = od.rename(columns={"titolo": "original_title",
                                "titoloBreve": "descriptive_title",
                                "idDdl": "ddl",
                                "dataPresentazione": "date_presenting",
                                "numeroLegge": "law_number",
                                "dataLegge": "law_publication_date",
                                "idFase": "source_identifier"
                                },
                       errors="raise")

        ass_list = list(self.assemblee.order_by('identifier'))
        od['assembly'] = od['ramo'].apply(lambda x: get_assembly(ass_list, x))

        od['initiative'] = od['tipoiniziativa'].apply(map_initiative)
        od['is_law'], od['gu_url'],\
            od['law_number'], od['law_publication_date'] = zip(
                *od.apply(lambda x: is_law(
                    x['statoDdl'],
                    x['law_number'],
                    x['law_publication_date'],
                    x['type']),
                          axis=1))

        od['is_ratification'] = od.apply(lambda x: get_is_ratification(x['is_ratification'],
                                                                       x['original_title'],
                                                                       x['source_identifier']), axis=1)
        od = od.drop(['ramo', 'tipoiniziativa', 'fase', 'statoDdl',
                      'dataStatoDdl', 'natura'], axis=1)
        od = od.where(pd.notnull(od), None)
        self.etl.processed_data = od

        return self.etl


class IterTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self, assemblee):
        Transformation.__init__(self)
        self.assemblee = assemblee

    def transform(self):
        od = self.etl.original_data.copy()
        od = od.rename(columns={"dataStatoDdl": "status_date"
                                }, errors="raise")
        od['identifier'] = od['fase']
        ass_list = list(self.assemblee.order_by('identifier'))
        od['bill'] = od.apply(lambda x: Bill.objects.get(
            assembly=get_assembly(ass_list, x['ramo']),
            identifier=x['identifier']),
                               axis=1)

        od['phase'] = od['statoDdl'].apply(get_iter_phase)
        od['last_status'] = od['bill'].apply(lambda x: x.last_status().phase if x.last_status() else None)
        od = od[od['last_status'] != od['phase']]
        od = od.drop(['ramo',
                      'fase',
                      'statoDdl',
                      'last_status',
                      'identifier'], axis=1)
        od = od.where(pd.notnull(od), None)
        self.etl.processed_data = od

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        legislatura_padded = str(legislatura).zfill(2)
        legislature_identifier = f"ITL_{legislatura_padded}"

        e = KeyEvent.objects.get(identifier=legislature_identifier)

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        year_months = CalendarDaily.objects.filter(date__gte=e.start_date,
                                                   date__lte=(e.end_date or today_strf))\
            .values_list('year',
                         'month').distinct().order_by('year', 'month')
        tipologia = 'atti'

        self.logger.info("Getting acts FULL")

        def get_query(**kwargs):
            query = template_query_sparql(name=tipologia, **kwargs)
            return get_bindings(
                    ENDPOINTS_ASSEMBLEE_PARLAMENTARI.get(kwargs.get('aula')),
                    query,
                    legacy=True
            )

        assemblee = Organization.objects.filter(classification='Assemblea parlamentare',
                                                identifier__regex=legislatura_padded)
        self.logger.debug('Getting acts')
        for year, month in list(year_months)[-2:]:
            year_month = f'{year}-{str(month).zfill(2)}'
            self.logger.debug(f'{year_month}')
            data = JsonExtractor(get_query(legislatura=legislatura,
                                           tipologia=tipologia,
                                           aula='senato',
                                           year_month=year_month)).extract()

            if data.shape[0] == 0:
                self.logger.warning(f'No data for legislation {legislatura} for category {tipologia} {year} {month}')
                continue

            ETL(
                extractor=DataframeExtractor(data),
                transformation=BillTransformation(assemblee=assemblee),
                loader=DjangoUpdateOrCreateLoader(django_model=Bill, fields_to_update=['original_title',
                                                                                       'descriptive_title',
                                                                                       'ddl',
                                                                                       'initiative',
                                                                                       'date_presenting',
                                                                                       'is_law',
                                                                                       'gu_url',
                                                                                       'law_number',
                                                                                       'law_publication_date',
                                                                                       'source_identifier',
                                                                                       'type',
                                                                                       'is_ratification']),
            )()

            ETL(
                extractor=DataframeExtractor(data[['ramo',
                                                   'fase',
                                                   'statoDdl',
                                                   'dataStatoDdl']]),
                transformation=IterTransformation(assemblee=assemblee),
                loader=DjangoUpdateOrCreateLoader(django_model=Iter),
            )()

            # HOT FIX
            a = Bill.objects.get(date_presenting='2023-3-15', identifier='S.602')
            a.law_number = '55'
            a.save()
