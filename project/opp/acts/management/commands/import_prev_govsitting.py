import unicodedata

from bs4 import BeautifulSoup
from datetime import datetime
from datetime import timedelta
from project.opp.acts.models import GovSitting
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor
from .import_govsitting import get_governemnt
import locale
import pandas as pd
import pytz
import requests
import re
from taskmanager.management.base import LoggingBaseCommand

import urllib3
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
locale.setlocale(locale.LC_ALL, 'it_IT.utf-8')


def get_soup(url):
    page = requests.get(url,
                        verify=False)

    soup = BeautifulSoup(page.content, "html.parser")
    return soup


def get_start_date(description):
    if 'Comunicato stampa del Consiglio dei Ministri n. 54\n7 Luglio 2020' in description:
        return '6 luglio 2020 22.50'

    if 'Consiglio dei Ministri n. 7 - Disciplina degli stupefacenti\n15 Ottobre 2014' in description:
        return '5 ottobre 2014 11.15'

    if 'Comunicato stampa del Consiglio dei Ministri n. 25 - Nuova Politica Agricola\n31 Luglio 2014' in description:
        return '5 ottobre 2014 11.15'

    regex = r'(?:convocato|riunito)\D+(\d+)°* (\w+)[ ]*(\d{4})*[ ,]* all\D+(\d+)\.*(\d+)*'
    matches = list(re.findall(regex, description, re.MULTILINE)[0])
    if not matches[2]:
        regex_year = r'\d{2} \w+ (\d{4})'
        match_year = re.findall(regex_year, description, re.MULTILINE)[0]
        matches[2] = match_year
    matches[-1] = matches[-1].zfill(2)
    return f"{' '.join(matches[:-1])}.{matches[-1]}"


def get_end_date(description):
    if 'Comunicato stampa del consiglio dei Ministri n. 140\n09 Novembre 2016' in description:
        return '9 novembre 2016 13.55'

    if 'Consiglio dei Ministri n. 7 - Disciplina degli stupefacenti\n15 Ottobre 2014' in description:
        return '5 ottobre 2014 12.20'

    regex_time = r'(?: terminato| concluso).+all\D+(\d+(?:.\d+)*)'
    match_time = re.findall(regex_time, description, re.MULTILINE)
    match_time = match_time[-1].replace(',', '.').replace(':', '.')
    if '.' not in match_time:
        match_time = (match_time + '.00')
    regex_date = r'(?: terminato| concluso) all\D+\d+\.\d* di .* (\d+°*) (\w+ \d+)'
    match_date = re.findall(regex_date, description, re.MULTILINE)
    if not match_date:
        regex = r'(?:convocato|riunito)\D+(\d+)°* (\w+)[ ]*(\d{4})*'
        match_date = list(re.findall(regex, description, re.MULTILINE)[0])
        if not match_date[2]:
            regex_year = r'\d{2} \w+ (\d{4})'
            match_year = re.findall(regex_year, description, re.MULTILINE)[0]
            match_date[2] = match_year
        match_date = [match_date]
    match_date = ' '.join(match_date[0])
    return f'{match_date} {match_time}'


class GovSittingExtractor(DataframeExtractor):

    def __init__(self, df, domain):
        super().__init__(df)
        self.domain = domain

    @staticmethod
    def correct_end_date(start, end):
        if start > end:
            return end + timedelta(days=1)
        return end

    @staticmethod
    def get_description(href, domain):
        soup = get_soup(f'{domain}{href}')
        text_div = soup.find('div', class_='post_content post_content_cell clearfix')
        return unicodedata.normalize('NFKD', text_div.get_text())

    def extract(self):
        soup = self.df
        df = pd.DataFrame()

        titles = soup.find_all('a', class_='box_text_anchor')
        df['title'] = [x.get_text() for x in titles]
        df['href_detail'] = [x.get('href') for x in titles]
        df = df[~df['href_detail'].str.contains('comunicato-stampa-del-consiglio-dei-ministri-n-39-'
                                                'approva-la-nota-di-variazioni-al-bilancio|'
                                                'comunicato-stampa-del-consiglio-dei-ministri-n-31-e-32/984.html|'
                                                'comunicato-stampa-del-consiglio-dei-ministri-n-'
                                                '25-nuova-politica-agricola/934.html')]
        df = df[~df['title'].str.contains('Cabinet')]
        df['url'] = df['href_detail'].apply(lambda x: f"{self.domain}{x}")
        df['description'] = df['href_detail'].apply(lambda x: self.get_description(x, self.domain))
        df['starting_datetime'] = df['description'].apply(get_start_date)
        df['ending_datetime'] = df['description'].apply(get_end_date)
        df['number'] = df['title'].str.extract(r'n\.[ ]*(\d+)')
        df['starting_datetime'] = df['starting_datetime'].apply(lambda x: datetime.strptime(x, '%d %B %Y %H.%M'))
        df['ending_datetime'] = df['ending_datetime'].apply(lambda x: datetime.strptime(x, '%d %B %Y %H.%M'))
        df['ending_datetime'] = df.apply(lambda x: self.correct_end_date(
            x['starting_datetime'],
            x['ending_datetime']),
                                       axis=1)
        rome_timezone = pytz.timezone('Europe/Rome')
        df['starting_datetime'] = df['starting_datetime'].apply(lambda x: rome_timezone.localize(x))
        df['ending_datetime'] = df['ending_datetime'].apply(lambda x: rome_timezone.localize(x))

        return df


class GovSittingTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self):
        Transformation.__init__(self)

    def transform(self):
        od = self.etl.original_data.copy()
        od['government'] = od.apply(lambda x: get_governemnt(
            date=x['starting_datetime'].date().strftime('%Y-%m-%d'),
            number=x['number']
        ), axis=1)
        od = od.drop(['title', 'href_detail'], axis=1)
        self.etl.processed_data = od

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql for Gov Sittings."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--url_domains",
            type=list,
            dest='url_domains',
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        domains = options["url_domains"]
        for domain in domains:
            id_page = ''
            content = True
            while content:
                soup_data = get_soup(f'{domain}archivio-riunioni{id_page}.html?page=')
                content = soup_data.find('div', class_='view-content')
                if content:
                    ETL(
                        extractor=GovSittingExtractor(soup_data, domain),
                        transformation=GovSittingTransformation(),
                        loader=DjangoUpdateOrCreateLoader(django_model=GovSitting, fields_to_update=['description',
                                                                                                     'government',
                                                                                                     'ending_datetime',
                                                                                                     'url']),
                    )()
                    next_page = content.find('li', class_='pager-next')
                    if not next_page:
                        break
                    page = next_page.find('a').get('href')
                    m = re.search('riunioni(.*).html', page)
                    id_page = m.group(1)
