from taskmanager.management.base import LoggingBaseCommand
from project.opp.acts.models import IterPhase
import logging


class Command(LoggingBaseCommand):
    logger = logging.getLogger(f"project.{__name__}")
    help = "Load iter phases."

    def handle(self, *args, **options):
        custom_cases = [
            ('Approvato', True),
            ('Approvato con modificazioni', True),
            ('Approvato definitivamente legge', True),
            ('Approvato definitivamente non ancora pubblicato', False),
            ('Approvato in testo unificato', True),
            ('Approvato non passaggio articoli', True),
            ('Approvazione questione sospensiva', False),
            ('Assegnato (no esame)', False),
            ('Assorbito', True),
            ('Cancellato dall\'ODG', True),
            ('Conclusione anomala per stralcio', True),
            ('Da assegnare a commissione', False),
            ('D-L decaduto', True),
            ('Esame concluso in commissione', False),
            ('Esame in assemblea', False),
            ('Esame in commissione', False),
            ('In relazione', False),
            ('Insussistenza quorum', True),
            ('Respinto', True),
            ('Restituito al governo', True),
            ('Rimesso all\'assemblea', False),
            ('Rinviato camere', True),
            ('Rinviato in commissione', False),
            ('Ritirato', True),

        ]

        for case in custom_cases:
            IterPhase.objects.get_or_create(
                phase=case[0],
                defaults={
                    'is_completed': case[1]
                }
            )
