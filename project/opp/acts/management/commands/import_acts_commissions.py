from django.db.models import F, Value

from config.settings.base import ENDPOINTS_ASSEMBLEE_PARLAMENTARI
from project.calendars.models import CalendarDaily

from project.tasks.parsing.sparql.utils import get_bindings
from taskmanager.management.base import LoggingBaseCommand

from project.opp.acts.management.commands.raw_sparql.template_query import template_query_sparql
from project.opp.acts.models import Bill, BillCommission

from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor, Extractor
from django.db.models.functions import Coalesce, Cast
from django.db.models import Q

from popolo.models import Organization, KeyEvent
import pandas as pd
import pytz
import re
import datetime



class JsonExtractor(Extractor):
    """Json Extractor
    """

    def __init__(self, df):
        """Create a new instance of the extractor, with dataframe in memory.
        """
        self.df = df

    def extract(self):
        res = []
        for r in self.df:
            res.append(dict((k, r[k]["value"]) for k in r.keys()))
        od = pd.DataFrame(res)
        return od


def get_bill(identifier, assemblea, leg):
    return Bill.objects.get(identifier__iexact=f'{assemblea}.{identifier}',
                            assembly__identifier__endswith=leg)


# Function to convert integer to Roman values
def intToRoman(num):
    # Storing roman values of digits from 0-9
    # when placed at different places
    m = ["", "M", "MM", "MMM"]
    c = ["", "C", "CC", "CCC", "CD", "D",
         "DC", "DCC", "DCCC", "CM "]
    x = ["", "X", "XX", "XXX", "XL", "L",
         "LX", "LXX", "LXXX", "XC"]
    i = ["", "I", "II", "III", "IV", "V",
         "VI", "VII", "VIII", "IX"]

    # Converting to roman
    thousands = m[num // 1000]
    hundreds = c[(num % 1000) // 100]
    tens = x[(num % 100) // 10]
    ones = i[num % 10]

    ans = (thousands + hundreds +
           tens + ones)

    return ans


def get_typology(tipocomm, sede):
    if tipocomm == 'consultiva':
        return BillCommission.CONSULTIVA
    elif sede == 'referente':
        return BillCommission.REFERENTE
    elif sede == 'redigente':
        return BillCommission.REDIGENTE
    elif sede == 'deliberante':
        return BillCommission.DELIBERANTE
    return BillCommission.REFERENTE


def extract_numbers_commisions(list_text):
    def convert_int(text):
        try:
            return int(text)
        except:
            return text

    return list(set([convert_int(x) for x in list_text if
              isinstance(convert_int(x), int)]))


def get_organization(org, ramo, labelcomm, date, leg):
    date = date.strftime('%Y-%m-%d')
    if org:
        try:
            return Organization.objects.filter(identifiers__identifier=org) \
                .annotate(end_date_coalesce=Coalesce(F('end_date'), Value('9999-12-31'))) \
                .get(start_date__lte=date, end_date_coalesce__gte=date)
        except:
            print('a')

    else:
        if ramo == 'C':
            aula = 'camera'
        else:
            aula = 'senato'
        labelcomm = str(labelcomm)
        labelcomm = labelcomm.replace('&ordf;', '').strip()
        if labelcomm.lower() in ['legislazione', 'comitato legislazione']:
            return Organization.objects.filter(name__icontains='COMITATO PER LA LEGISLAZIONE').filter(
                name__icontains=aula) \
                .annotate(end_date_coalesce=Coalesce(F('end_date'), Value('9999-12-31'))) \
                .get(start_date__lte=date, end_date_coalesce__gte=date)

        if 'Commissione speciale' in labelcomm :
            try:
                return Organization.objects.filter(classification='Commissione speciale/straordinaria parlamentare').filter(
                    name__icontains=aula) \
                    .annotate(end_date_coalesce=Coalesce(F('end_date'), Value('9999-12-31'))) \
                    .filter(start_date__lte=date, end_date_coalesce__gte=date).get(Q(name=labelcomm)|Q(other_names__name=labelcomm))
            except:
                print('a')
        regex = r"\d+"
        matches = re.findall(regex, labelcomm, re.MULTILINE)


        cpp = Organization.objects.filter(classification='Commissione permanente parlamentare', name__icontains=aula) \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value('9999-12-31'))) \
            .filter(start_date__lte=date, end_date_coalesce__gte=date)

        try:
            introman = intToRoman(int(matches[0]))
            return cpp.get(name__istartswith=f"{introman} COMMISSIONE")
        except:
            print('a')


class BillCommissionTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """

    def __init__(self, legislatura):
        Transformation.__init__(self)
        self.legislatura = legislatura

    def transform(self):
        od = self.etl.original_data.copy()
        od = od.where(pd.notnull(od), None)

        od['appointing_date'] = od['dataAssegnazione'] \
            .apply(lambda x: datetime.datetime.strptime(x, '%Y-%m-%d') if x else None)
        rome_timezone = pytz.timezone('Europe/Rome')
        od['appointing_date'] = od['appointing_date'] \
            .apply(lambda x: rome_timezone.localize(x) if not pd.isnull(x) else None)
        od.replace({pd.NaT: None}, inplace=True)
        if 'sede' not in od.columns:
            od['sede'] = None
        od['typology'] = od.apply(lambda x: get_typology(x['tipoCommissione'], x['sede']),
                                  axis=1)
        od['organization'] = od.apply(lambda x: get_organization(org=x['organo'],
                                                                 ramo=x['ramo'],
                                                                 labelcomm=x['labelCommissione'],
                                                                 date=x['appointing_date'],
                                                                 leg=self.legislatura), axis=1)
        od['bill'] = od.apply(lambda x: get_bill(x['identifier'], x['ramo'], self.legislatura), axis=1)
        self.etl.processed_data = od[['bill', 'organization', 'typology', 'appointing_date']]

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql data CAMERA e SENATO."""

    help = ""

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            dest='legislatura',
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        legislatura = options["legislatura"]
        tipologia = 'commissions'
        legislatura_padded = str(legislatura).zfill(2)
        legislature_identifier = f"ITL_{legislatura_padded}"
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        e = KeyEvent.objects.get(identifier=legislature_identifier)
        year_months = CalendarDaily.objects.filter(date__gte=e.start_date,
                                                   date__lte=(e.end_date or today_strf))\
            .values_list('year',
                         'month').distinct().order_by('year', 'month')
        def get_query(**kwargs):
            query = template_query_sparql(name=tipologia, **kwargs)
            return get_bindings(
                ENDPOINTS_ASSEMBLEE_PARLAMENTARI.get(kwargs.get('aula')),
                query,
                legacy=True
            )

        aula = 'senato'
        for year, month in list(year_months)[-2:]:
            year_month = f'{year}-{str(month).zfill(2)}'
            data = JsonExtractor(get_query(legislatura=legislatura,
                                           tipologia=tipologia,
                                           aula=aula,
                                           year_month=year_month)).extract()

            if data.shape[0] == 0:
                continue

            if 'organo' not in data.columns:
                data['organo'] = None
            data_multi_org_camera = data[data['organo'].isna() & data['labelCommissione'].str.contains('riunite')]
            data = data[~(data['organo'].isna() & data['labelCommissione'].str.contains('riunite'))]
            if data.shape[0] == 0:
                self.logger.warning(f'No data for {aula} legislation {legislatura} for category {tipologia}')

            else:
                ETL(
                    extractor=DataframeExtractor(data),
                    transformation=BillCommissionTransformation(legislatura=legislatura_padded),
                    loader=DjangoUpdateOrCreateLoader(django_model=BillCommission, fields_to_update=['appointing_date']),
                )()
            data_multi_org_camera['labelCommissione'] = data_multi_org_camera['labelCommissione'].str.split(r"(\d+)")
            data_multi_org_camera['labelCommissione'] = data_multi_org_camera['labelCommissione'].apply(extract_numbers_commisions)
            data_multi_org_camera = data_multi_org_camera.explode('labelCommissione').reset_index()
            if data_multi_org_camera.shape[0] == 0:
                self.logger.warning(f'No data for {aula} legislation {legislatura} for category {tipologia}')

            else:
                ETL(
                    extractor=DataframeExtractor(data_multi_org_camera),
                    transformation=BillCommissionTransformation(legislatura=legislatura_padded),
                    loader=DjangoUpdateOrCreateLoader(django_model=BillCommission, fields_to_update=['appointing_date']),
                )()
