from taskmanager.management.base import LoggingBaseCommand
from django.core.management import call_command
import time
import logging


class Command(LoggingBaseCommand):
    logger = logging.getLogger(f"project.{__name__}")
    help = "Update data for OPENPARLAMENTO for ACTS data."

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--l",
            type=int,
            choices=[13, 14, 15, 16, 17, 18, 19],
            default=18,
            dest='legislatura',
            required=True,
            help="Legislature number"
        )

    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.

        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        self.logger.info(f"----- Starting {command_name} -----")
        start_time = time.time()
        call_command(command_name, *args, **options, stdout=self.stdout)
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        self.logger.setLevel(
            {
                0: logging.ERROR,
                1: logging.WARNING,
                2: logging.INFO,
                3: logging.DEBUG,
            }.get(options["verbosity"])
        )
        legislatura = options['legislatura']
        self.logger.info(f"Legislatura {legislatura}")

        self.logger.info("Update IterPhases cases")
        self.call_command('create_iterphases',
                          verbosity=3)

        self.logger.info("Load/Update Gov Sitting FULL current government")
        self.call_command('import_govsitting',
                          verbosity=3)

        self.logger.info("Load/Update ACTS FULL legislature")
        self.call_command('import_acts',
                          legislatura=legislatura,
                          verbosity=3)
        self.logger.info("Load/Update Gov Decree legislature")
        self.call_command('import_govdecree',
                          verbosity=3)

        self.logger.info("Load/Update ACTS relationships table FULL legislature")
        self.call_command('import_rel_acts',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info("Load/Update GOV BILLS")
        self.call_command('import_govbill',
                          verbosity=3)

        self.logger.info("Load/Update BILLS Presenting Memberships")
        self.call_command('import_signers_acts',
                          legislatura=legislatura,
                          verbosity=3)

        self.logger.info("Load/Update BILLS Spoke persons Memberships")
        self.call_command('import_spokeperson',
                          legislatura=legislatura,
                          verbosity=3)
        self.logger.info("Load/Update BILLS Commissions rel")
        self.call_command('import_acts_commissions',
                          legislatura=legislatura,
                          verbosity=3)
        try:
            self.logger.info("Load/Update Implementing decrees FULL")
            self.call_command('import_implementingdecree',
                              verbosity=3)
        except Exception:
            self.logger.error("Error for implementing decree")

        self.logger.info("Load/Update Implementing decrees FULL")
        self.call_command('update_cache_acts',
                          legislatura=legislatura,
                          verbosity=3)
