from django.core.exceptions import ObjectDoesNotExist
from datetime import datetime
from project.opp.acts.models import Bill, GovDecree
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.transformations import Transformation
from ooetl.extractors import DataframeExtractor
from .import_govdecree import get_soup, get_sitting, get_date_gu, get_date_sitting
import locale
import pytz
from django.db import IntegrityError
import pandas as pd
import re
from taskmanager.management.base import LoggingBaseCommand

locale.setlocale(locale.LC_ALL, 'it_IT.utf-8')


def get_original_title(soup_data):
    title = soup_data.find('div', id='titoloAtto').get_text()
    title = title.replace('\n', ' ').replace('\t', ' ').replace('\r', ' ')
    title = re.sub(' +', ' ', title).strip()
    regex = r'(.*\. \d+)'
    matches = re.findall(regex, title, re.MULTILINE)
    return matches[0]


def get_descriptive_title(soup_data):
    title = soup_data.find('h3').get_text()
    title = title.replace('\n', ' ').replace('\t', ' ').replace('\r', ' ')
    title = re.sub(' +', ' ', title).strip()
    return title


def get_max_law(source, max_value=300, step=100, down=True):
    data = get_soup(f'{source};{max_value})')
    res = 'Errore nel caricamento delle informazioni' in data.get_text()
    if step == 1:
        if res:
            return max_value - 1
        else:
            return max_value
    if res:
        step = round(step/(2-down*1))
        max_value = max(1, max_value - step)
        return get_max_law(source, max_value, step, True)
    else:
        step = round(step/(down*1+1))
        max_value = max(1, max_value + step)
        return get_max_law(source, max_value, step, False)


class PreviousGovDecreeExtractor(DataframeExtractor):

    def get_bill(self, act, date):
        try:
            return Bill.objects.get(identifier=act, date_presenting__gte=date)
        except ObjectDoesNotExist:
            pass

    @staticmethod
    def get_text_decree(href):
        soup = get_soup(href)
        atto_orig = soup.find(text=re.compile('originario'))
        href_atto_orig = atto_orig.parent.get('href')
        soup_atto_orig = get_soup(f"https://www.normattiva.it/{href_atto_orig}")
        return soup_atto_orig

    def extract(self):
        df = pd.DataFrame(self.df, columns=['url'])

        df['identifier'] = df['url'].apply(lambda x: re.findall(r'decreto.legge:(.*)', x)[0].replace(';', '-'))
        df['text_decree'] = df['url'].apply(self.get_text_decree)
        # import pickle
        # import sys
        # sys.setrecursionlimit(20000)
        # with open('df_test.pickle', 'wb') as handle:
        #     pickle.dump(df, handle, protocol=pickle.HIGHEST_PROTOCOL)

        # with open('df_test.pickle', 'rb') as handle:
        #     df = pickle.load(handle)

        df['original_title'] = df['text_decree'].apply(get_original_title)
        df['descriptive_title'] = df['text_decree'].apply(get_descriptive_title)
        df = df[df['original_title'].str.contains('DECRETO-LEGGE')]
        if df.empty:
            return None
        df['text_decree'] = df['text_decree'].apply(lambda x: x.get_text())

        df['date_sitting'] = df['text_decree'].apply(get_date_sitting)
        df['date_sitting'] = df['date_sitting'].apply(lambda x: datetime.strptime(x, '%d %B %Y'))
        df['sitting'] = df['date_sitting'].apply(lambda x: get_sitting(x))
        df['publication_gu_date'] = df['text_decree'].apply(get_date_gu)
        df['publication_gu_date'] = df['publication_gu_date'].apply(lambda x: datetime.strptime(x, '%d-%m-%Y'))
        rome_timezone = pytz.timezone('Europe/Rome')
        df['date_sitting'] = df['date_sitting'].apply(lambda x: rome_timezone.localize(x))

        return df


class PreviousGovDecreeTransformation(Transformation):
    """Trasformazione dati sedute parlamentari di Camera e Senato della Repubblica
    """
    def __init__(self):
        Transformation.__init__(self)

    def transform(self):
        od = self.etl.original_data.copy()
        od = od.drop(['url', 'text_decree', 'date_sitting', ], axis=1)
        self.etl.processed_data = od

        return self.etl


class Command(LoggingBaseCommand):
    """Command ETL sparql for Gov Sittings."""

    help = ""

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.setup_logger(__name__, formatter_key="simple", **options)
        base = 'https://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legge:'

        for anno in [2021, 2022]:
            url = f'{base}{anno}'
            max_value = get_max_law(url)
            indeces = list(range(1, max_value+1))
            url_laws = map(lambda x: f'https://www.normattiva.it'
                                     f'/uri-res/N2Ls?urn:nir:stato:decreto.legge:{anno};{x}', indeces)

            df = PreviousGovDecreeExtractor(list(url_laws)).extract()
            if df.empty:
                continue
            ETL(
                extractor=DataframeExtractor(df),
                transformation=PreviousGovDecreeTransformation(),
                loader=DjangoUpdateOrCreateLoader(django_model=GovDecree, fields_to_update=['descriptive_title',
                                                                                            'sitting',
                                                                                            'publication_gu_date',
                                                                                            'original_title'
                                                                                            ]),
            )()

            for index, item in df.iterrows():
                gd = GovDecree.objects.get(identifier=item['identifier'])
                bill = Bill.objects.filter(type=Bill.DL_CONVERSION,
                                           original_title__icontains=item['original_title'])\
                    .order_by('date_presenting').first()
                if gd:
                    try:
                        gd.add_next(bill)
                    except IntegrityError:
                        pass
