from taskmanager.management.base import LoggingBaseCommand
from project.opp.acts.models import Iter, IterPhase
import logging


class Command(LoggingBaseCommand):
    logger = logging.getLogger(f"project.{__name__}")
    help = "Load iter phases."
    mappings = {
        'approvato': 'Approvato',
        'appr. con modificaz': 'Approvato con modificazioni',
        'approvato con modificazioni': 'Approvato con modificazioni',
        'appr. definit. Legge': 'Approvato definitivamente legge',
        'approvato definitivamente. Legge': 'Approvato definitivamente legge',
        'appr. def. non pubbl': 'Approvato definitivamente non ancora pubblicato',
        'approvato definitivamente, non ancora pubblicato': 'Approvato definitivamente non ancora pubblicato',
        'appr. in t.u.': 'Approvato in testo unificato',
        'approvato in testo unificato': 'Approvato in testo unificato',
        'appr. non pass. art.': 'Approvato non passaggio articoli',
        'assegnato (no esame)': 'Assegnato (no esame)',
        'Assegnato (non ancora iniziato l\'esame)': 'Assegnato (no esame)',
        'assegnato (non ancora iniziato l\'esame)': 'Assegnato (no esame)',
        'assorbito': 'Assorbito',
        'cancellato dall\'OdG': 'Cancellato dall\'ODG',
        'cancellato dall\'Ordine del Giorno': 'Cancellato dall\'ODG',
        'concl. anomala stral': 'Conclusione anomala per stralcio',
        'conclusione anomala per stralcio': 'Conclusione anomala per stralcio',
        'da assegn. a commis.': 'Da assegnare a commissione',
        'da assegnare': 'Da assegnare a commissione',
        'D-L decaduto': 'D-L decaduto',
        'decreto legge decaduto': 'D-L decaduto',
        'concluso l\'esame': 'Esame concluso in commissione',
        'concluso l\'esame da parte della commissione': 'Esame concluso in commissione',
        'all\'esame assemblea': 'Esame in assemblea',
        'all\'esame dell\'assemblea': 'Esame in assemblea',
        'esame in comm.': 'Esame in commissione',
        'in corso di esame in commissione': 'Esame in commissione',
        'in relazione': 'In relazione',
        'in stato di relazione': 'In relazione',
        'insussistenza quorum': 'Insussistenza quorum',
        'verificata insussistenza del quorum': 'Insussistenza quorum',
        'respinto': 'Respinto',
        'restit. al Governo': 'Restituito al governo',
        'restituito al Governo per essere ripresentato all\'altro ramo': 'Restituito al governo',
        'rimesso assemblea': 'Rimesso all\'assemblea',
        'rimesso all\'Assemblea': 'Rimesso all\'assemblea',
        'rinviato camere PdR': 'Rinviato camere',
        'rinviato alle Camere dal Presidente della Repubblica': 'Rinviato camere',
        'rinviato ass.->comm.': 'Rinviato in commissione',
        'rinviato dall\'assemblea in commissione': 'Rinviato in commissione',
        'ritirato': 'Ritirato'
        }

    iters = Iter.objects.all()

    for iter in iters:

        new_phase = IterPhase.objects.get(phase=mappings.get(iter.phase.phase))
        iter.phase = new_phase
        iter.save()
