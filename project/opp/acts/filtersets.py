import django_filters
from django.contrib.contenttypes.models import ContentType
from django.db.models.functions import Coalesce
from django.utils.translation import ugettext as _
from django.core.cache import cache
from django.db.models import F, OuterRef, Subquery, Value
from django_filters import ChoiceFilter, CharFilter, ModelChoiceFilter, MultipleChoiceFilter, ModelMultipleChoiceFilter
from django_filters.rest_framework import (
    FilterSet, BooleanFilter)
from popolo.models import Organization, KeyEvent

from project.opp.acts.models import Bill, IterPhase, Iter, GovDecree, GovBill, ImplementingDecree, \
    ImplementingDecreeOrganizations
import datetime

from project.opp.gov.models import Government
from project.opp.votes.filtersets import get_governments



# from project.acts.models import Bill
def get_branches(request=None):
    if request is None:
        return []

    leg = request.parser_context['kwargs'].get('legislature', None)

    qs = Organization.objects.filter(classification='Assemblea parlamentare',
                                     key_events__key_event__identifier=f"ITL_{leg}")

    return qs.values_list('identifier', 'name', )


class BillFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)

        self.filters['branch'].extra['choices'] = [
            ['C', 'Camera'], ['S', 'Senato']
        ]
        leg = '19'
        legislature_identifier = f"ITL_{leg}"
        ke = KeyEvent.objects.get(identifier=legislature_identifier)
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')

        self.filters['government'].extra['choices'] = Government.objects \
            .annotate(
            end_date_coalesce=Coalesce(F('organization__end_date'), Value(today_strf))) \
            .filter(organization__start_date__lte=(ke.end_date or today), end_date_coalesce__gte=ke.start_date) \
            .values_list('organization_id', 'organization__name')

    is_key_act = ChoiceFilter(
        method="is_key_act_filter",
        label=_("Is_key_act"),
        choices=[(True, 'Yes'), (False, 'No')],
        help_text=_("Filter bills based on whether they are key acts"),
    )

    @staticmethod
    def is_key_act_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("is_key_act")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("is_key_act")).filter(b=value)

        else:
            return queryset.all()

    is_ratification = ChoiceFilter(
        method="is_ratification_filter",
        label="Ratifica",
        choices=[(True, 'Yes'), (False, 'No')],
        help_text=_("Filter bills based on whether they are ratification acts"),
    )

    @staticmethod
    def is_ratification_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("is_ratification")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("is_ratification")).filter(b=value)

        else:
            return queryset.all()

    branch = ChoiceFilter(
        method="branch_filter",
        label=_("Branch"),
        empty_label=_("All branches"),
        help_text=_("Filter votings based on the branch they took place (ASS-CAMERA-18, ASS-SENATO-19, ...)"),
    )

    @staticmethod
    def branch_filter(queryset, name, value):
        if value:
            if value == 'C':
                return queryset.annotate(b=F("assembly__identifier")).filter(b__icontains='camera')
            else:
                return queryset.annotate(b=F("assembly__identifier")).filter(b__icontains='senato')
        else:
            return queryset.all()

    initiative = ChoiceFilter(
        method="initiative_filter",
        label=_("Initiative"),
        empty_label=_("All"),
        choices=Bill.INITIATIVE_TYPES,
        help_text=_("Filter bills based on initiative"),
    )

    @staticmethod
    def initiative_filter(queryset, name, value):
        if value:
            return queryset.annotate(b=F("initiative")).filter(b=value)
        else:
            return queryset.all()

    type = MultipleChoiceFilter(
        label=_("Type"),
        choices=Bill.BILL_TYPES,
        help_text=_("Filter bills based on type"),
    )


    typology = ChoiceFilter(
        method="typology_filter",
        label=_("Typology"),
        choices=[('min', 'Minotauro'), ('omni', 'Omnibus'), ('salv_int', 'Salvo intese')],
        help_text=_("Filter bills based on typology"),
    )

    @staticmethod
    def typology_filter(queryset, name, value):
        if value == 'min':
            return queryset.annotate(b=F("is_minotaurus")).filter(b=True)
        elif value == 'omni':
            return queryset.annotate(b=F("is_omnibus")).filter(b=True)
        elif value == 'salv_int':
            return queryset.annotate(b=F("is_subject_to_agreements")).filter(b=True)

        else:
            return queryset.all()

    status = MultipleChoiceFilter(
        choices=Bill.STATUS,
        label="Macro status",
        help_text=_("Filter bills based on macro status")
    )

    first_signer = CharFilter(
        method="first_signer_filter",
        label=_("First signer person Slug"),
        help_text=_("Filter votings based on the person slug"),
    )

    @staticmethod
    def first_signer_filter(queryset, name, value):
        if value:
            return queryset.filter(bill_signers__membership__person__slug=value, bill_signers__first_signer=True)
        else:
            return queryset

    government = ChoiceFilter(
        method="government_filter",
        empty_label="Tutti i Governi",
        label=_("Government"),
        help_text=_("Filter bills based on government period")
    )

    @staticmethod
    def government_filter(queryset, name, value):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        if value == 'null':
            return queryset.all()
        elif value:
            gov = Organization.objects.get(id=value)
            return queryset.filter(date_presenting__gte=gov.start_date, date_presenting__lte=(gov.end_date or today))

        else:
            return queryset.all()

    class Meta:
        model = Bill
        fields = []


class GovDecreeFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)

        self.filters['typology'].extra['choices'] = [
            ('min', 'Minotauro'), ('omni', 'Omnibus'),
            ('salv_int', 'Salvo intese')]

        self.filters['status'].extra['choices'] = [('l', 'Legge'), ('c', 'In conversione'),
                                                   ('e', 'Decaduto')]

        # self.filters['government'].extra['choices'] = get_governments(kwargs['request']).annotate(label=F('name')).values('id', 'label')

    typology = ChoiceFilter(
        method="typology_filter",
        label=_("Typology"),
        help_text=_("Filter bills based on typology"),
    )

    @staticmethod
    def typology_filter(queryset, name, value):
        if value == 'min':
            return queryset.annotate(b=F("is_minotaurus")).filter(b=True)
        elif value == 'omni':
            return queryset.annotate(b=F("is_omnibus")).filter(b=True)
        elif value == 'salv_int':
            return queryset.annotate(b=F("is_subject_to_agreements")).filter(b=True)

        else:
            return queryset.all()

    status = ChoiceFilter(
        method="status_filter",
        label=_("Status"),
        help_text=_("Filter bills based on status")
    )

    @staticmethod
    def status_filter(queryset, name, value):
        if value and value != "":
            ids = [x.id for x in queryset if x.status()['status'] == value]
            return queryset.filter(id__in=ids)
        return queryset

    government = ModelChoiceFilter(
        method="government_filter",
        label=_("Government ID"),
        queryset=get_governments,
        help_text=_("Filter votings based on the Government period (Organization ID)"),
    )

    @staticmethod
    def government_filter(queryset, name, value):
        if value:
            g = Organization.objects.get(id=value.id)
            return queryset.filter(sitting__government_id=g.id)
        else:
            return queryset.all()

    class Meta:
        model = GovDecree
        fields = []


class GovBillFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)
        leg = kwargs['request'].parser_context['kwargs']['legislature']
        legislature_identifier = f"ITL_{leg}"
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        govs = GovBill.objects.by_date(sd=e.start_date,
                                       ed=(e.end_date or today_strf)).annotate(pk2=F('identifier'))
        self.filters['government'].extra['choices'] = list(
            govs.values_list('decision_sitting__government_id', 'decision_sitting__government__name').distinct())

    government = ChoiceFilter(
        method="government_filter",
        empty_label="Tutti i Governi",
        label=_("Government"),
        help_text=_("Filter bills based on government period")
    )

    @staticmethod
    def government_filter(queryset, name, value):
        if value == 'null':
            return queryset.all()
        elif value:
            gov = Organization.objects.get(id=value)
            return queryset.filter(decision_sitting__government__id=value)

        else:
            return queryset.all()

    is_key_act = ChoiceFilter(
        method="is_key_act_filter",
        label=_("Is_key_act"),
        choices=[(True, 'Yes'), (False, 'No')],
        help_text=_("Filter bills based on whether they are key acts"),
    )

    @staticmethod
    def is_key_act_filter(queryset, name, value):
        if value == 'null':
            return queryset.annotate(b=F("is_key_act")).filter(b__isnull=True)
        elif value:
            return queryset.annotate(b=F("is_key_act")).filter(b=value)

        else:
            return queryset.all()

    class Meta:
        model = GovBill
        fields = []


class ImplementingDecreeFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)
        self.request = kwargs.get('request')

    is_adopted = BooleanFilter(
        field_name='is_adopted',
        label=_("Is adopted"),
        help_text=_("Filter bills based on whether they are key acts"),
    )

    is_next_30_days = BooleanFilter(
        method="is_next_30_days_filter",
        label=_("Next 30 days deadline"),
        help_text=_("Filter bills based on whether deadline is in 30 days"),
    )

    government = ChoiceFilter(
        field_name='government_id',
        choices=ImplementingDecree.objects.select_related('government')
        .values_list('government_id', 'government__name').distinct()
    )

    mins_proposer = ChoiceFilter(
        label=_("Proposer"),
        choices=ImplementingDecreeOrganizations.objects.filter(
            type=ImplementingDecreeOrganizations.PROPOSER).values_list('organization_id',
                                                                       'organization__name').distinct(),
        method="proposers_filter",
    )

    policy = ChoiceFilter(
        label="Policy",
        choices=ImplementingDecree.objects.all().values_list('policy', 'policy').distinct().order_by('policy'),
        # method="proposers_filter",
    )

    @staticmethod
    def proposers_filter(queryset, name, value):
        if value:
            return queryset.filter(organizations_impl_dec__type='PROP', organizations_impl_dec__organization_id=value)

        else:
            return queryset.all()

    type_law = ChoiceFilter(
        method="type_law_filter",
        label=_("Tipologia atto a cui si riferisce"),
        choices=[('bill', 'Legge'), ('govdecree', 'Decreto legge'), ('govbill', 'Decreto legislativo')],
        help_text=_("Filter implementing decree based on type reference law"),
    )

    @staticmethod
    def type_law_filter(queryset, name, value):
        if value:
            return queryset.annotate(b=F("ending_relations__starting_ct__model")).filter(b=value)
        else:
            return queryset.all()

    govbill = CharFilter(
        method="govbill_filter",
        label=_("Decreto legislativo a cui si riferisce"),
        help_text=_("Filter implementing govbill based on slug"),
    )

    @staticmethod
    def govbill_filter(queryset, name, value):

        if value:
            try:
                gov_bill = GovBill.objects.get(identifier=value).id
            except:
                return GovBill.objects.none()
            return queryset.annotate(b=F("ending_relations__starting_id")).filter(b=gov_bill,
                                                                                  ending_relations__starting_ct=ContentType.objects.get(
                                                                                      model='govbill'))
        else:
            return queryset.all()

    govdecree = CharFilter(
        method="govdecree_filter",
        label=_("Decreto legge a cui si riferisce"),
        help_text=_("Filter implementing govdecree based on slug"),
    )

    @staticmethod
    def govdecree_filter(queryset, name, value):

        if value:
            try:
                govdecree = GovDecree.objects.get(identifier=value).id
            except:
                return GovDecree.objects.none()
            return queryset.annotate(b=F("ending_relations__starting_id")).filter(b=govdecree,
                                                                                  ending_relations__starting_ct=ContentType.objects.get(
                                                                                      model='govdecree')
                                                                                  )
        else:
            return queryset.all()

    bill = CharFilter(
        method="bill_filter",
        label=_("Disegno di legge a cui si riferisce"),
        help_text=_("Filter implementing bill based on slug"),
    )

    def bill_filter(self, queryset, name, value, **kwargs):

        if value:
            try:
                leg = self.request.parser_context['kwargs'].get('legislature')
                legislature_identifier = f"ITL_{leg}"
                ke = KeyEvent.objects.get(identifier=legislature_identifier)
                bill = Bill.objects.get(date_presenting__gte=ke.start_date, identifier=value.replace('_', '.').replace('-', '/')).id
            except:
                return Bill.objects.none()
            return queryset.annotate(b=F("ending_relations__starting_id")).filter(b=bill,
                                                                                  ending_relations__starting_ct=ContentType.objects.get(
                                                                                      model='bill'))
        else:
            return queryset.all()

    status = ChoiceFilter(
        method="status_filter",
        label="Status",
        choices=[('app', 'Adottato'), ('napp', 'Non adottato'), ('exp', 'Scaduto')],
    )

    @staticmethod
    def status_filter(queryset, name, value):
        if value == 'app':
            return queryset.filter(is_adopted=True)
        elif value == 'napp':
            return queryset.filter(is_adopted=False)
        elif value == 'exp':
            today = datetime.datetime.now()
            return queryset.filter(is_adopted=False, deadline_date__isnull=False).filter(deadline_date__lt=today)
        else:
            return queryset.all()


    @staticmethod
    def is_next_30_days_filter(queryset, name, value):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        if value:
            return queryset.filter(deadline_date__gte=today, deadline_date__lte=(today + datetime.timedelta(days=30)))
        else:
            return queryset.all()

    class Meta:
        model = ImplementingDecree
        fields = []
