import factory
from project.opp.acts.tests import faker

from project.opp.acts.models import Bill
from popolo.tests.factories import OrganizationFactory


class BillFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "acts.Bill"

    initiative = factory.Faker(
        'random_element',
        elements=[x[0] for x in Bill.INITIATIVE_TYPES]
    )
    is_law = factory.Faker("pybool")

    @factory.lazy_attribute
    def assembly(self):
        return OrganizationFactory.create()


class GovDecreeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "acts.GovDecree"

    @factory.lazy_attribute
    def presentation_date(self):
        return faker.date_between(start_date="-1y", end_date="-2m").strftime("%Y-%m-%d")

    @factory.lazy_attribute
    def publication_date(self):
        return faker.date_between(start_date="-2m", end_date="-1m").strftime("%Y-%m-%d")


class GovSittingFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "acts.GovSitting"

    number = factory.Faker("pyint", min_value=1, max_value=20)
    url = factory.Faker("url")

    @factory.lazy_attribute
    def starting_datetime(self):
        return faker.date_time_between(start_date="-1y", end_date="-2m")


class ImplementingDecreeFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = "acts.ImplementingDecree"

    @factory.lazy_attribute
    def deadline_date(self):
        return faker.date_between(start_date="-2m", end_date="-1m").strftime("%Y-%m-%d")
