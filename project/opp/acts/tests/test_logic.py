import factory
from .factories import BillFactory, GovSittingFactory
from django.conf import settings
from django.test import TestCase
from popolo.tests.factories import OrganizationFactory, ClassificationFactory
from project.opp.acts.models import ActRelationshipException


class ActTestCase(TestCase):

    def test_add_relation_to(self):
        """Test that an add_to call creates a proper generic relation
        """
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        c = ClassificationFactory.create(
            scheme=settings.RELATION_TYPE_CLASSIFICATION['scheme'],
            descr="Test"
        )

        bill_1.add_relation_to(bill_2, c)
        endings = bill_1.related_ending_acts(classification=c)

        self.assertEquals(len(endings), 1)
        self.assertEquals(endings[0].identifier, "C.11")

    def test_add_relation_from(self):
        """Test that an add_to call creates a proper generic relation
        """
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        c = ClassificationFactory.create(
            scheme=settings.RELATION_TYPE_CLASSIFICATION['scheme'],
        )

        bill_1.add_relation_from(bill_2, c)

        startings = bill_1.related_starting_acts(classification=c)

        self.assertEquals(len(startings), 1)
        self.assertEquals(startings[0].identifier, "C.11")

    def test_add_relation_wrong_classification_fails(self):
        """Test that an add_relation_to with wrong classification fails
        """
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        c = ClassificationFactory.create(
            scheme="OP_ALTRA",
        )

        with self.assertRaises(ActRelationshipException):
            bill_1.add_relation_to(bill_2, c)

    def test_split(self):
        """Test creation of act splitting into many subsequent acts
        """
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_3 = BillFactory.create(
            identifier="C.12",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_1.add_split([bill_2, bill_3])
        splitted = bill_1.split_acts

        self.assertEquals(len(splitted), 2)
        self.assertEquals(splitted[0].identifier, "C.11")
        self.assertEquals(splitted[1].identifier, "C.12")

    def test_split_using_next(self):
        """Test creation of act splitting into many subsequent acts,
        using the nextprev relation type and add_next method
        """
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_3 = BillFactory.create(
            identifier="C.12",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_1.add_next([bill_2, bill_3])
        splitted = bill_1.next_acts

        self.assertEquals(len(splitted), 2)
        self.assertEquals(splitted[0].identifier, "C.11")
        self.assertEquals(splitted[1].identifier, "C.12")

    def test_join(self):
        """Test creation of act joined from many previous acts
        """
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_3 = BillFactory.create(
            identifier="C.12",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_1.add_joined([bill_2, bill_3])
        joined = bill_1.joined_acts

        self.assertEquals(len(joined), 2)
        self.assertEquals(joined[0].identifier, "C.11")
        self.assertEquals(joined[1].identifier, "C.12")

    def test_join_using_prev(self):
        """Test creation of multiple acts joining into a subsequent act,
        using the nextprev relation type and add_previous method
        """
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_3 = BillFactory.create(
            identifier="C.12",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_1.add_previous([bill_2, bill_3])
        joined = bill_1.previous_acts

        self.assertEquals(len(joined), 2)
        self.assertEquals(joined[0].identifier, "C.11")
        self.assertEquals(joined[1].identifier, "C.12")

    def test_add_next(self):
        """Test adding a single next act"""
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_1.add_next(bill_2)
        n = bill_1.next_acts

        # test direct relationship
        self.assertEquals(len(n), 1)
        self.assertEquals(n[0].identifier, "C.11")

        # inverse relationship test
        p = bill_2.previous_acts
        self.assertEquals(len(p), 1)
        self.assertEquals(p[0].identifier, "C.1")

    def test_add_previous(self):
        """Test adding a single next act"""
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2.add_previous(bill_1)
        p = bill_2.previous_acts

        # test direct relationship
        self.assertEquals(len(p), 1)
        self.assertEquals(p[0].identifier, "C.1")

        # inverse relationship test
        n = bill_1.next_acts
        self.assertEquals(len(n), 1)
        self.assertEquals(n[0].identifier, "C.11")

    def test_actrelationship_string_representation(self):
        bill_1 = BillFactory.create(
            identifier="C.1",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        bill_2 = BillFactory.create(
            identifier="C.11",
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        c = ClassificationFactory.create(
            scheme=settings.RELATION_TYPE_CLASSIFICATION['scheme'],
            descr="Test"
        )

        bill_1.add_relation_to(bill_2, c)
        r = bill_1.starting_relations.first()

        self.assertEquals(
            r.__str__(),
            f"({bill_1}) -[{c}]-> ({bill_2})"
        )


class BillTestCase(TestCase):

    def test_branch_returns_correct_value(self):
        """Test that the branch function reports correctly the branch of a bill.
        """

        bill = BillFactory.create(
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        self.assertEquals(bill.branch, 'C')

        bill = BillFactory.create(
            assembly=OrganizationFactory.create(identifier='ASS-SENATO-19')
        )
        self.assertEquals(bill.branch, 'S')

    def test_wrong_identifier_raise_exception(self):
        bill = BillFactory.create(
            assembly=OrganizationFactory.create(identifier='ASS-REGIONALE')
        )
        with self.assertRaises(Exception):
            bill = bill.branch

    def test_string_original_title_only(self):
        bill = BillFactory.create(
            original_title=factory.Faker("sentence"),
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        self.assertEquals(bill.__str__(), f"{bill.identifier} - {bill.original_title}")

    def test_string_descriptive_title(self):
        bill = BillFactory.create(
            original_title=factory.Faker("sentence"),
            descriptive_title=factory.Faker("sentence"),
            assembly=OrganizationFactory.create(identifier='ASS-CAMERA-19')
        )
        self.assertEquals(bill.__str__(), f"{bill.identifier} - {bill.descriptive_title}")


class GovSittingTestCase(TestCase):

    def test_string_representation(self):
        s = GovSittingFactory.create()
        self.assertEquals(s.__str__(), f"{s.number} @ {s.starting_datetime.date()}")
