import datetime
import statistics
from typing import Union

import pandas as pd
from django.contrib.contenttypes.fields import GenericRelation, GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.postgres.fields import ArrayField
from django.core.cache import cache
from django.db import models
from django.conf import settings
from django.db.models.functions import Coalesce, Cast
from django.utils.translation import gettext_lazy as _
from popolo.models import Organization, Classification, SourceRel, Membership as OPDMMembership, KeyEvent
from django.db.models import JSONField, F, ExpressionWrapper, fields, Avg, Q, Value, OuterRef, Subquery, DateField, \
    Lookup, Max, CharField
from popolo.mixins import SourceShortcutsMixin
from popolo.behaviors.models import Timestampable
from django.urls import reverse
from django.contrib.sites.models import Site
from bs4 import BeautifulSoup
from project.calendars.models import CalendarDaily

import re


class ArrayContainsExact(Lookup):
    lookup_name = 'contains_exact'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return f"{lhs} @> ARRAY[{rhs}]::varchar[]", params


ArrayField.register_lookup(ArrayContainsExact)


class LegBillsManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_legislature(self, leg=None):
        if leg:
            leg_identifier = f"ITL_{leg}"
            return super().get_queryset().select_related('assembly').filter(
                assembly__key_events__key_event__identifier=leg_identifier,
                # date_presenting__gte=Cast(F('assembly__key_events__key_event__start_date'), output_field=DateField())
            )
        else:
            return super().get_queryset().select_related('assembly')


class LegBillSignerManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_legislature(self, leg=None):
        if leg:
            leg_identifier = f"ITL_{leg}"
            return super().get_queryset().select_related('bill__assembly').filter(
                bill__assembly__key_events__key_event__identifier=leg_identifier,
                bill__date_presenting__gte=Cast(F('bill__assembly__key_events__key_event__start_date'),
                                                output_field=DateField()))
        else:
            return super().get_queryset().select_related('bill__assembly')


class LegGovDecreeManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_date(self, sd=None, ed=None):
        if sd and ed:
            sd_date = datetime.datetime.strptime(sd, '%Y-%m-%d').date()
            ed_date = datetime.datetime.strptime(ed, '%Y-%m-%d').date()
            return super().get_queryset().select_related('sitting').filter(
                Q(sitting__starting_datetime__gte=sd_date,
                  sitting__starting_datetime__lte=ed_date) |
                Q(sitting__starting_datetime__gte=sd_date - datetime.timedelta(days=60),
                  sitting__starting_datetime__lte=ed_date - datetime.timedelta(days=60))
            )
        else:
            return super().get_queryset()

    def by_legislature(self, leg='19'):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        legislatura_padded = str(leg).zfill(2)
        ke = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        return GovDecree.objects.by_date(sd=ke.start_date, ed=(ke.end_date or today_strf))


class IterManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_date(self, sd=None, ed=None):
        if sd and ed:

            return super().get_queryset().select_related('bill').filter(
                bill__date_presenting__gte=sd,
                bill__date_presenting__lte=ed
            )
        else:
            return super().get_queryset()

    def by_legislature(self, leg='19'):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        legislatura_padded = str(leg).zfill(2)
        ke = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        return Iter.objects.by_date(sd=ke.start_date, ed=(ke.end_date or today_strf))


class LegGovBillManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_date(self, sd=None, ed=None):
        if sd and ed:

            return super().get_queryset().select_related('decision_sitting').filter(
                decision_sitting__starting_datetime__gte=sd,
                decision_sitting__starting_datetime__lte=ed
            )
        else:
            return super().get_queryset()

    def by_legislature(self, leg='19'):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        legislatura_padded = str(leg).zfill(2)
        ke = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        return GovBill.objects.by_date(sd=ke.start_date, ed=(ke.end_date or today_strf))


class LegImplementingDecreeManager(models.Manager):
    """Add a by_legislature(leg) method to the default objects manager."""

    def by_date(self, sd=None, ed=None):
        if sd and ed:

            return super().get_queryset().select_related('government').filter(
                government__start_date__gte=sd, government__start_date__lte=ed
            )
        else:
            return super().get_queryset()

    def by_legislature(self, leg='19'):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        from project.opp.gov.models import Government
        legislatura_padded = str(leg).zfill(2)
        ke = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        govs = Government.get_governments_by_leg(leg)
        govs_id = [x.organization.id for x in govs]
        return ImplementingDecree.objects.filter(is_valid=True) \
            .filter(
            Q(government__in=govs_id) |
            Q(adoption_date__gte=ke.start_date, adoption_date__lte=(ke.end_date or today_strf)) |
            Q(adoption_date__isnull=True))


class GovSitting(Timestampable, SourceShortcutsMixin, models.Model):
    """Sitting of the government where Decrees are deliberated"""

    starting_datetime = models.DateTimeField(
        _("Start datetme"),
        null=True,
        blank=True,
        help_text=_("The date of the sitting (starting point)"),
    )
    ending_datetime = models.DateTimeField(
        _("End datetme"),
        null=True,
        blank=True,
        help_text=_("The date of the sitting (ending point)"),
    )
    number = models.PositiveSmallIntegerField(
        help_text=_("Sitting number")
    )
    url = models.URLField(
        _("url identifier"), blank=True, null=True,
        help_text=_("The url of official communicate")
    )
    sources = GenericRelation(
        to=SourceRel,
        help_text=_("URLs to source documents")
    )

    description = models.TextField(
        null=True,
        help_text=_("Description of government sitting")
    )

    government = models.ForeignKey(
        to=Organization,
        on_delete=models.CASCADE,
        related_name="gov",
        limit_choices_to={'classification__in': ["Governo della Repubblica", ]},
        blank=True, null=True
    )

    @property
    def gov_name(self):
        from project.opp.gov.models import Government
        return Government.objects.get(organization_id=self.government).name

    class Meta:
        verbose_name = _("Gov sitting")
        verbose_name_plural = _("Gov sittings")
        ordering = ('-starting_datetime',)

    def __str__(self):
        return f"{self.number} @ {self.starting_datetime.date()}"


class IterPhase(models.Model):
    """The phase an act can be in its iter"""
    phase = models.CharField(
        _("phase"),
        max_length=128,
        help_text=_("Phase of the iter")
    )
    is_completed = models.BooleanField(
        _("is completed"),
        default=False,
        help_text=_("Whether this phase means that the iter is completed")
    )

    def __str__(self) -> str:
        return self.phase


class Iter(Timestampable, models.Model):
    """Descript an iter step for a bill"""
    phase = models.ForeignKey(
        to="IterPhase",
        on_delete=models.PROTECT,
        related_name="steps"
    )
    bill = models.ForeignKey(
        to="Bill",
        on_delete=models.CASCADE,
        related_name="steps"
    )
    status_date = models.DateField(
        _("start date"),
        blank=True, null=True,
        help_text=_("The date to which the status refers"),
    )
    objects = IterManager()

    class Meta:
        ordering = ('-status_date',)

    def __str__(self) -> str:
        return f"{self.bill.branch}.{self.bill.identifier}" \
               f" {self.phase} on {self.status_date}"


class ActRelationshipException(Exception):
    pass


class ActRelationship(Timestampable, models.Model):
    """
    A generic relationship between two act (may be a bill, a decree, an impl decree).
    Must be defined by a classification (type, ex: prev_next, split, aggregation, ...).
    A relationship starts from an act and ends in another one.
    """

    class Meta:
        verbose_name = _("Act relationship")
        verbose_name_plural = _("Act relationships")
        constraints = [
            models.UniqueConstraint(
                fields=["starting_ct", "starting_id", "ending_ct", "ending_id", "classification"],
                name='unique_relation'
            )
        ]

    starting_ct = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name="starting_relations")
    starting_id = models.PositiveIntegerField()
    starting_object = GenericForeignKey('starting_ct', 'starting_id')

    ending_ct = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name="ending_relations")
    ending_id = models.PositiveIntegerField()
    ending_object = GenericForeignKey('ending_ct', 'ending_id')

    classification = models.ForeignKey(
        to=Classification,
        related_name="act_relationships",
        limit_choices_to={"scheme": settings.RELATION_TYPE_CLASSIFICATION['scheme']},
        help_text=_("The classification for this relationship among acts"),
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        return f"({self.starting_object}) -[{self.classification}]-> ({self.ending_object})"

    def save(self, *args, **kwargs):
        if self.classification and self.classification.scheme != settings.RELATION_TYPE_CLASSIFICATION['scheme']:
            raise ActRelationshipException("Wrong classification schema")
        super(ActRelationship, self).save(*args, **kwargs)


class Act(Timestampable, models.Model):
    """A legislative act"""
    identifier = models.CharField(
        _("identifier"),
        max_length=512,
        blank=True,
        null=True,
        help_text=_("The unique identifier (sequence, number) given a chamber")
    )
    original_title = models.TextField(
        _("original title"),
        help_text=_("The act's original title")
    )
    descriptive_title = models.CharField(
        _("descriptive title"),
        max_length=2048,
        blank=True, null=True,
        help_text=_("A descriptive title for this act, so that it makes sense")
    )
    public_title = models.CharField(
        _("public title"),
        max_length=1024,
        blank=True, null=True,
        help_text=_("Title, as it's known publicly")
    )
    description = models.TextField(
        blank=True, null=True,
        help_text=_("A textual description for the Act")
    )

    starting_relations = GenericRelation(
        to=ActRelationship,
        related_query_name="act",
        content_type_field="starting_ct",
        object_id_field="starting_id",
        help_text=_("relations starting from this act")
    )
    ending_relations = GenericRelation(
        to=ActRelationship,
        related_query_name="act",
        content_type_field="ending_ct",
        object_id_field="ending_id",
        help_text=_("relations ending in this act")
    )

    is_key_act = models.BooleanField(
        default=False,
        help_text=_("If the act is a key act")
    )

    def add_relation_to(self, act, classification):
        """Add a relation starting from this instance into `act` using given classification"""
        ActRelationship.objects.create(
            starting_object=self,
            ending_object=act,
            classification=classification
        )

    def add_relation_from(self, act, classification):
        """Add a relation ending into this instance from `act` using given classification"""
        ActRelationship.objects.create(
            ending_object=self,
            starting_object=act,
            classification=classification
        )

    def related_ending_acts(self, **kwargs: dict) -> list:
        """Return the complete list of acts related to this one.
        The self instance is considered to be the starting point.
        The related acts are the 'ending' acts.

        :param kwargs: dictionary to be used for filtering
        :return: a list of acts
        """
        return [i.ending_object for i in self.starting_relations.filter(**kwargs)]

    def related_starting_acts(self, **kwargs: dict) -> list:
        """Return the complete list of acts related to this one.
        The self instance is considered to be the ending point.
        The related acts are the 'starting' acts.

        :param kwargs: dictionary to be used for filtering
        :return: a list of acts
        """
        return [i.starting_object for i in self.ending_relations.filter(**kwargs)]

    def add_next(self, acts: Union["Act", list["Act"]]):
        """Add a relation  to one or more `acts` as the next ones (classification=nextprev)"""
        c, created = Classification.objects.get_or_create(
            scheme=settings.RELATION_TYPE_CLASSIFICATION['scheme'],
            descr=settings.RELATION_TYPE_CLASSIFICATION['nextprev_value']
        )
        if type(acts) == list:
            for act in acts:
                self.add_relation_to(act, c)
        else:
            self.add_relation_to(acts, c)

    @property
    def slug(self):
        return self.identifier.replace('.', '_').replace('/', '-')

    def add_previous(self, acts: Union["Act", list["Act"]]):
        """Add a relation  to one or more `acts` as the previous ones (classification=nextprev, inverted)"""
        c, created = Classification.objects.get_or_create(
            scheme=settings.RELATION_TYPE_CLASSIFICATION['scheme'],
            descr=settings.RELATION_TYPE_CLASSIFICATION['nextprev_value']
        )
        if type(acts) == list:
            for act in acts:
                self.add_relation_from(act, c)
        else:
            self.add_relation_from(acts, c)

    @property
    def next_acts(self, **kwargs) -> list:
        """Return the list of next acts

        Use kwargs to filter.
        """
        classification_scheme = kwargs.pop(
            'classification__scheme', settings.RELATION_TYPE_CLASSIFICATION['scheme']
        )
        classification_descr = kwargs.pop(
            'classification__descr', settings.RELATION_TYPE_CLASSIFICATION['nextprev_value']
        )
        return self.related_ending_acts(
            classification__scheme=classification_scheme,
            classification__descr=classification_descr,
            **kwargs
        )

    @property
    def previous_acts(self, **kwargs) -> list:
        """Return the list of previous acts

        Use kwargs to filter.
        """
        classification_scheme = kwargs.pop(
            'classification__scheme', settings.RELATION_TYPE_CLASSIFICATION['scheme']
        )
        classification_descr = kwargs.pop(
            'classification__descr', settings.RELATION_TYPE_CLASSIFICATION['nextprev_value']
        )
        return self.related_starting_acts(
            classification__scheme=classification_scheme,
            classification__descr=classification_descr,
            **kwargs
        )

    @property
    def previous_tree(self):
        def flatten_list(lst):
            flattened_list = []
            for item in lst:
                if isinstance(item, list):
                    flattened_list.extend(flatten_list(item))
                else:
                    flattened_list.append(item)
            return flattened_list

        def _previous_tree(branch):
            if branch.previous_acts:
                return [branch] + [_previous_tree(x) for x in branch.previous_acts]
            return [branch]

        res = _previous_tree(self)
        return flatten_list(res)

    @property
    def next_tree(self):
        def flatten_list(lst):
            flattened_list = []
            for item in lst:
                if isinstance(item, list):
                    flattened_list.extend(flatten_list(item))
                else:
                    flattened_list.append(item)
            return flattened_list

        def _next_tree(branch):
            if branch.next_acts:
                return [branch] + [_next_tree(x) for x in branch.next_acts]
            return [branch]

        res = _next_tree(self)
        return flatten_list(res)

    def add_split(self, split_acts_list: Union["Act", list["Act"]]):
        """Add a relation from this instance to a list of acts this one has been split into
        :param split_acts_list:
        """
        c, created = Classification.objects.get_or_create(
            scheme=settings.RELATION_TYPE_CLASSIFICATION['scheme'],
            descr=settings.RELATION_TYPE_CLASSIFICATION['split_value']
        )
        if type(split_acts_list) == list:
            for act in split_acts_list:
                self.add_relation_to(act, c)
        else:
            self.add_relation_to(split_acts_list, c)

    @property
    def split_acts(self, **kwargs) -> list:
        """Return the list of split acts

        Use kwargs to filter.
        """
        classification_scheme = kwargs.pop(
            'classification__scheme', settings.RELATION_TYPE_CLASSIFICATION['scheme']
        )
        classification_descr = kwargs.pop(
            'classification__descr', settings.RELATION_TYPE_CLASSIFICATION['split_value']
        )
        return self.related_ending_acts(
            classification__scheme=classification_scheme,
            classification__descr=classification_descr,
            **kwargs
        )

    def add_joined(self, joined_acts_list: Union["Act", list["Act"]]):
        """Add a relation from this instance to a list of acts this one has been joined into
        :param joined_acts_list:
        """
        c, created = Classification.objects.get_or_create(
            scheme=settings.RELATION_TYPE_CLASSIFICATION['scheme'],
            descr=settings.RELATION_TYPE_CLASSIFICATION['joined_value']
        )
        if type(joined_acts_list) == list:
            for act in joined_acts_list:
                self.add_relation_from(act, c)
        else:
            self.add_relation_from(joined_acts_list, c)

    @property
    def joined_acts(self, **kwargs) -> list:
        """Return the list of joined acts

        Use kwargs to filter.
        """
        classification_scheme = kwargs.pop(
            'classification__scheme', settings.RELATION_TYPE_CLASSIFICATION['scheme']
        )
        classification_descr = kwargs.pop(
            'classification__descr', settings.RELATION_TYPE_CLASSIFICATION['joined_value']
        )
        return self.related_starting_acts(
            classification__scheme=classification_scheme,
            classification__descr=classification_descr,
            **kwargs
        )

    class Meta:
        abstract = True

    def __str__(self) -> str:
        title = self.original_title
        if self.descriptive_title:
            title = self.descriptive_title
        if self.public_title:
            title = self.public_title

        return f"{self.identifier} - {title}"


class GovDecree(Act):
    """A government decree, presented """
    sitting = models.ForeignKey(
        to=GovSitting,
        blank=True, null=True,
        on_delete=models.PROTECT,
        related_name="decrees",
        help_text=_("The sitting where the decree was presented")
    )

    publication_gu_date = models.DateField(
        _("publication date"),
        help_text=_("The date of the decree is officially published"),
    )

    is_omnibus = models.BooleanField(
        default=False,
        help_text=_("If the act is omnibus")
    )

    is_minotaurus = models.BooleanField(
        default=False,
        help_text=_("If the act is minotaurus")
    )

    is_subject_to_agreements = models.BooleanField(
        default=False,
        help_text=_("If the act is published 'salvo intese' ")
    )

    objects = LegGovDecreeManager()

    @property
    def conversion_act(self):
        acr = ActRelationship.objects.filter(starting_id=self.id,
                                             starting_ct=ContentType.objects.get(model='govdecree'),
                                             ending_ct=ContentType.objects.get(model='bill'))
        if acr.first():
            return acr.first().ending_object

    @property
    def source_url(self):
        return f"https://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legge:{self.identifier.replace('-', ';')}"

    def status(self):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        conv_act = self.conversion_act
        if not conv_act:
            return {'status': 'c',
                    'date': today.strftime('%Y-%m-%d')}

        def get_status(act):
            next_act = act.starting_relations.filter(classification__descr='Successivo', ending_ct__model='bill')
            if next_act.count() > 0:
                return get_status(next_act.first().ending_object)
            else:
                return act.last_status()

        status = get_status(conv_act)

        if status.phase.phase in ['Approvato definitivamente legge', 'Approvato definitivamente non ancora pubblicato']:
            res = 'l'
        elif status.phase.phase == 'D-L decaduto':
            res = 'e'
        else:
            res = 'c'
        return {'status': res,
                'date': status.status_date.strftime('%Y-%m-%d')}

    def status_desc(self):
        res = self.status()['status']
        if res == 'l':
            return 'Diventato legge'
        elif res == 'e':
            return 'Decaduto'
        else:
            return 'In conversione'

    def status_date(self):
        res = self.status()['date']
        return res

    def status_at_date(self, date):
        publication_date = self.publication_gu_date
        if date < publication_date:
            return {'status': 'n', 'date': date}
        elif date > self.date_expiring:
            return self.status()
        else:
            status = self.status()
            status_date = status['date']
            if date < datetime.datetime.strptime(status_date, '%Y-%m-%d').date():
                return {'status': 'c', 'date': date}
            else:
                return status

    @property
    def title(self):
        return (self.public_title or self.descriptive_title)

    @property
    def date_presenting(self):
        return self.sitting.ending_datetime.date()

    @property
    def date_expiring(self):
        return self.publication_gu_date + datetime.timedelta(days=60)

    @property
    def days_from_approval(self):
        return (self.publication_gu_date - self.date_presenting).days

    @property
    def days_to_expiry(self):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        days = ((self.publication_gu_date + datetime.timedelta(days=60)) - today.date()).days
        if days >= 0:
            return days

    @property
    def parliament_iter(self):
        return self.conversion_act.parliament_iter

    @classmethod
    def update_cache_get_api_aggregate(cls, leg='19'):
        res = cls._get_api_aggregate(leg=leg)
        cache.set(
            'govdecree_get_api_aggregate',
            {'res': res},
            timeout=None
        )

    @classmethod
    def get_api_aggregate(cls, leg='19'):
        if not cache.get('govdecree_get_api_aggregate'):
            cls.update_cache_get_api_aggregate(leg)
        return cache.get('govdecree_get_api_aggregate').get('res')

    @classmethod
    def _get_api_aggregate(cls, leg='19'):
        from project.opp.gov.models import Government
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        legislatura_padded = str(leg).zfill(2)
        ke = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        sd = ke.start_date
        ed = (ke.end_date or today_strf)
        n_months = round(CalendarDaily.objects.filter(date__gte=sd, date__lte=ed).distinct().count() / 30)

        dl = cls.objects.by_legislature(leg=leg)
        duration = ExpressionWrapper(F('publication_gu_date') - F('sitting__ending_datetime__date'),
                                     output_field=fields.DurationField())
        avg_pub_days = dl.annotate(duration=duration).aggregate(avg_days=Avg(F('duration')))['avg_days']
        govs_before = Government.objects.all().order_by('-organization__start_date')[:4]

        year_month = []
        years = GovDecree.objects.by_legislature(leg='19').values_list('publication_gu_date__year', flat=True)
        for year in sorted(set(years)):
            year_month.append({'year': year,
                               'months': []})
            months = GovDecree.objects.by_legislature(leg='19').filter(publication_gu_date__year=year).values(
                'publication_gu_date__month').distinct('publication_gu_date__month') \
                .order_by('publication_gu_date__month').values_list('publication_gu_date__month', flat=True)
            for month in set(months):
                print(f'{year} - {month}')
                gov_decrees = GovDecree.objects.by_legislature(leg='19').filter(publication_gu_date__year=year,
                                                                                publication_gu_date__month=month)
                year_month[-1]['months'].append({
                    'name': month,
                    'value': gov_decrees.count()
                })

        return {
            'cnt': dl.count(),
            'days_pub_avg': round(avg_pub_days.total_seconds() / (60 * 60 * 24), 1),
            'month_avg': round(dl.count() / n_months, 1),
            'govs_before': [x.get_historical() for x in govs_before],
            'historical': year_month,
            'detail': {
                'is_law': len([x for x in dl if x.status().get('status') == 'l']),
                'converting': len([x for x in dl if x.status().get('status') == 'c']),
                'expired': len([x for x in dl if x.status().get('status') == 'e'])
            },
        }

    @property
    def implementing_decrees(self):
        return ImplementingDecree.objects.filter(
            is_valid=True,
            id__in=self.starting_relations.filter(ending_ct__model='implementingdecree').values_list('ending_id',

                                                                                                     flat=True))

    @property
    def implemented_decrees(self):
        return self.implementing_decrees.filter(is_valid=True)

    @property
    def implementing(self):
        if not self.implementing_decrees.count():
            return {
                "to_adopt": {
                    "cnt": 0
                },
                "adopted":
                    {
                        "cnt": 0
                    },
                "to_adopt_locked_resources":
                    {
                        'total': 0,
                        'detail': []

                    }
            }
        df_tot_impl = pd.DataFrame(self.implementing_decrees.values_list('fundings', flat=True))
        df_tot_impl = df_tot_impl.sum()
        indeces = df_tot_impl > 0
        if not indeces.empty:
            min_year = int(min(indeces[indeces.values].index))
            max_year = int(
                max([max(x.keys()) for x in self.implementing_decrees.values_list('fundings', flat=True) if x]))
            impl_decree_min_to_adopt = self.implementing_decrees.filter(is_adopted=False)
            fundings = impl_decree_min_to_adopt.values_list('fundings', flat=True)
            df = pd.DataFrame(fundings).fillna(0).apply(sum)
            df = df.sort_index()
            default_index = list(map(str, range(min_year, max_year + 1)))
            df = df.reindex(default_index, fill_value=0)
            detail = [
                {
                    'year': int(x[0]),
                    'value': float(x[1])
                } for x in zip(df.index, df)]
        else:
            detail = []

        res = {
            "to_adopt": {
                "cnt": self.implementing_decrees.count() - self.implemented_decrees.filter(is_adopted=True).count()
            },
            "adopted":
                {
                    "cnt": self.implemented_decrees.filter(is_adopted=True).count()
                },
            "to_adopt_locked_resources":
                {
                    'total': 0 if detail == [] else float(df.sum()),
                    'detail': detail

                }
        }
        return res

    class Meta:
        verbose_name = _("Gov decree")
        verbose_name_plural = _("Gov decrees")


class ImplementingDecreeOrganizations(Timestampable, models.Model):
    """Descript an iter step for a bill"""
    organization = models.ForeignKey(
        to="popolo.Organization",
        on_delete=models.PROTECT,
        related_name="implementing_decrees_org"
    )
    act = models.ForeignKey(
        to="ImplementingDecree",
        on_delete=models.CASCADE,
        related_name="organizations_impl_dec"
    )

    PROPOSER = 'PROP'
    JOINTLY = 'JOIN'
    COUNSEL = 'CONS'
    TYPE = [
        (PROPOSER, _('Proposer')),
        (JOINTLY, _('In agreement with')),
        (COUNSEL, _('Advisory')),
    ]

    type = models.CharField(
        null=True,
        max_length=5,
        choices=TYPE,
        help_text=_("The type relationship the organization has with the act (implementing decree)")
    )

    def __str__(self) -> str:
        return f"{self.act}"


class BillSigner(Timestampable, models.Model):
    """Descript who presented a bill"""
    membership = models.ForeignKey(
        to="popolo.Membership",
        on_delete=models.PROTECT,
        related_name="membership_signers"
    )
    bill = models.ForeignKey(
        to="Bill",
        on_delete=models.CASCADE,
        related_name="bill_signers"
    )

    first_signer = models.BooleanField(
        _("is first signer"),
        default=False,
        help_text=_("If the memberships is the first signer of the act")
    )

    adding_date = models.DateField(
        _("Adding date"),
        blank=True, null=True,
        help_text=_("The date when signature was added, if has value, the membership added after"),
    )

    withdrawal_date = models.DateField(
        _("Revoking date"),
        blank=True, null=True,
        help_text=_("The date when signature was revoked"),
    )

    objects = LegBillSignerManager()

    @property
    def member_group_at_presenting(self):
        from project.opp.votes.models import Membership, Group
        person = self.membership.person
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        membership = Membership.objects.annotate(
            opdm_membership__end_date_coalesce=Coalesce(F('opdm_membership__end_date'), Value(today_strf))) \
            .filter(opdm_membership__person=person,
                    opdm_membership__start_date__lte=self.bill.date_presenting,
                    opdm_membership__end_date_coalesce__gte=self.bill.date_presenting
                    )
        if membership.count() == 1:
            return membership.first().get_group_at_date(self.bill.date_presenting).group
        return None

    @property
    def member_party_at_presenting(self):
        from project.opp.votes.models import Membership, Group
        from project.opp.gov.models import Member
        person = self.membership.person
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        membership = Member.objects.annotate(
            end_date_coalesce=Coalesce(F('end_date'), Value(today.date()))) \
            .filter(person=person,
                    start_date__lte=self.bill.date_presenting,
                    end_date_coalesce__gte=self.bill.date_presenting
                    )
        if membership.count() == 1:
            political_parties = membership.first().political_party.annotate(
                end_date_coalesce=Coalesce(F('end_date'), Value(today.date()))) \
                .filter(
                start_date__lte=self.bill.date_presenting,
                end_date_coalesce__gte=self.bill.date_presenting
            ).first()
            if political_parties:
                return political_parties.political_party
        return None

    def __str__(self) -> str:
        return f"{self.membership} - {self.bill}"

    class Meta:
        ordering = ('-bill__date_presenting',)


class BillSpeaker(Timestampable, models.Model):
    """Descript who is the spokepserson of the bill"""
    membership = models.ForeignKey(
        to="popolo.Membership",
        on_delete=models.PROTECT,
        related_name="membership_speakers"
    )
    bill = models.ForeignKey(
        to="Bill",
        on_delete=models.CASCADE,
        related_name="bill_speakers"
    )

    is_minority = models.BooleanField(
        _("is it is minority"),
        default=False,
        help_text=_("If the membership represents minority")
    )

    appointing_date = models.DateField(
        _("Appointing date"),
        blank=True, null=True,
        help_text=_("The date when membership was appointed"),
    )

    ASSEMBLY = 'ASSMY'
    COMMISSION = 'COMME'
    TYPE = [
        (ASSEMBLY, 'Assemblea'),
        (COMMISSION, 'Commissione'),
    ]

    unit = models.CharField(
        null=True,
        max_length=5,
        choices=TYPE,
        help_text=_("The unit where the membership is spokeperson")
    )

    @property
    def member_group_at_presenting(self):
        return self.membership.membership_parliament.get_group_at_date(self.appointing_date)

    @property
    def ass_commission(self):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        if self.unit != 'ASSMY':
            coms = self.bill.commissions.exclude(typology='CONS')
            coms = Organization.objects.filter(id__in=coms.values('organization_id'))
            membs = self.membership.person.memberships \
                .filter(organization__classification__startswith='Commission',
                        organization__classification__icontains='parlament',
                        start_date__lte=self.appointing_date).annotate(
                end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
                .filter(end_date_coalesce__gte=self.appointing_date)
            com = coms.filter(id__in=membs.values('organization_id')).first()

            res = f"dal {self.appointing_date.strftime('%d/%m/%Y')} in {com.name}" if com else 'Commissione'
        else:
            res = f"dal {self.appointing_date.strftime('%d/%m/%Y')} in {self.get_unit_display()}"
        if self.is_minority:
            res += " di minoranza"
        return res

    def __str__(self) -> str:
        return f"{self.membership} - {self.bill} @ {self.unit}"


class BillCommission(Timestampable, models.Model):
    """Descript who is the spokepserson of the bill"""

    organization = models.ForeignKey(
        to="popolo.Organization",
        on_delete=models.PROTECT,
        limit_choices_to={'classification__in': [
            'Giunta parlamentare',
            'Commissione/comitato bicamerale parlamentare',
            'Organo di presidenza parlamentare',
            'Commissione permanente parlamentare',
            'Commissione speciale/straordinaria parlamentare',
            'Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza',
            'Commissione d\'inchiesta parlamentare monocamerale/bicamerale',
            'Delegazione parlamentare presso assemblea internazionale']},
        related_name="bills_assigned"
    )

    bill = models.ForeignKey(
        to="Bill",
        on_delete=models.CASCADE,
        related_name="commissions"
    )

    appointing_date = models.DateField(
        _("Appointing date"),
        blank=True, null=True,
        help_text=_("The date when membership was appointed"),
    )

    REFERENTE = 'REF'
    REDIGENTE = 'RED'
    DELIBERANTE = 'DEL'
    CONSULTIVA = 'CONS'
    TYPE = [
        (REFERENTE, 'Referente'),
        (REDIGENTE, 'Redigente'),
        (DELIBERANTE, 'Deliberante'),
        (CONSULTIVA, 'Consultiva'),
    ]

    typology = models.CharField(
        null=True,
        max_length=5,
        choices=TYPE,
        help_text=_("The unit where the membership is spokeperson")
    )


class ImplementingDecree(Act):
    """A decree required to implement a bill """

    government = models.ForeignKey(
        to=Organization,
        on_delete=models.CASCADE,
        related_name="implementing_decrees",
        limit_choices_to={'classification__in': ["Governo della Repubblica", ]},
        blank=True, null=True
    )

    adoption_date = models.DateField(
        _("adoption date"),
        blank=True, null=True,
        help_text=_("The date when the decree is adopted (null = not adopted"),
    )
    is_adopted = models.BooleanField(
        _("is adopted"),
        default=False,
        help_text=_("If the decree has been adopted")
    )
    deadline_date = models.DateField(
        _("deadline date"),
        blank=True, null=True,
        help_text=_("The date when the decree must be completed"),
    )

    is_valid = models.BooleanField(
        _("Is still present from source"),
        default=False,
        help_text=_("It indicates if this record it still exists in the source."
                    "It is introduced to distinguish the impl. decree that are linked to GovDecree or"
                    "Bill converted in order to not duplicate the information."),
    )

    GOVERNMENT = 'GOV'
    PARLIAMENT = 'PARL'
    TYPE = [
        (GOVERNMENT, _('Government')),
        (PARLIAMENT, _('Parliament')),
    ]

    type = models.CharField(
        null=True,
        max_length=5,
        choices=TYPE,
        help_text=_("The type of nature for this implementing decree (gov or parliament)")
    )

    type_act = models.CharField(
        _("Type of the act expected"),
        max_length=124,
        blank=True, null=True,
        help_text=_("The type of the act expected")
    )

    publication_gu_date = models.DateField(
        _("publication date"),
        help_text=_("The date of the decree is officially published"),
        null=True, blank=True
    )

    publication_gu_number = models.PositiveSmallIntegerField(
        _("publication gu number"),
        help_text=_("The number of the Gazzetta Ufficial where the act is published"),
        null=True, blank=True
    )

    url = models.URLField(
        _("url act"), blank=True, null=True,
        help_text=_("The url of the act if adopted"),
        max_length=596
    )

    policy = models.CharField(
        _("Act policy"),
        max_length=596,
        blank=True, null=True,
        help_text=_("The policy of the act")
    )

    theme = models.CharField(
        _("Type of the act expected"),
        max_length=596,
        blank=True, null=True,
        help_text=_("The theme of the act")
    )

    fundings = JSONField(default=dict)

    organizations = models.ManyToManyField(
        to=Organization,
        verbose_name=_("popolo.Organizations"),
        through=ImplementingDecreeOrganizations,
        related_name="implementing_decree",
        help_text=_("All organization associatd to the implementing decree")
    )

    miss_jointly_agr_raw = models.CharField(
        _("In agreement with entities not directly joined"),
        max_length=596,
        blank=True, null=True,
        help_text=_("The jointly entities for the impl decree not joined in the db")
    )

    advisories_raw = models.CharField(
        _("Advisory entities not directly joined"),
        max_length=596,
        blank=True, null=True,
        help_text=_("The advisory entities for the impl decree not joined in the db")
    )

    objects = LegImplementingDecreeManager()

    @property
    def get_ministries_prop(self):
        return self.organizations_impl_dec.filter(type='PROP').values_list('organization__name', flat=True)

    def jointly_org(self):
        res = list(self.organizations_impl_dec.filter(type='JOIN').values_list('organization__name', flat=True))
        if self.miss_jointly_agr_raw and self.miss_jointly_agr_raw != '':
            res += [self.miss_jointly_agr_raw]
        return res

    @property
    def type_reference_law(self):
        return self.ending_relations.all().first().starting_ct.name

    @property
    def reference_law(self):
        return self.ending_relations.all().first()

    @property
    def get_resources(self):
        fundings = self.fundings
        tot = 0
        detail = []
        for x in fundings.keys():
            tot += fundings.get(x)
            detail.append(
                {x: fundings.get(x)}
            )

        return {'total': tot,
                'detail': detail}

    @classmethod
    def update_cache_get_api_aggregate(cls, leg='19'):
        res = cls._get_api_aggregate(leg=leg)
        cache.set(
            'impdecree_get_api_aggregate',
            {'res': res},
            timeout=None
        )

    @classmethod
    def _get_api_aggregate(cls, leg='19'):
        if not cache.get('impdecree_get_api_aggregate'):
            cls.update_cache_get_api_aggregate(leg)
        return cache.get('impdecree_get_api_aggregate').get('res')

    @classmethod
    def _get_ministries_impl_decr(cls, leg='19'):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        # from project.opp.gov.models import Government
        legislatura_padded = str(leg).zfill(2)
        ke = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')
        sd = ke.start_date
        ed = (ke.end_date or today_strf)
        ministries = (Organization.objects.filter(
            Q(classification__icontains='Ministero') |
            Q(classification__icontains='Presidenza del consiglio')). \
                      annotate(
            start_date_coalesce=Coalesce(F('start_date'), Value('1900-01-01')),
            end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))). \
                      filter(start_date_coalesce__lte=ed, end_date_coalesce__gte=sd)
                      .filter(end_date__isnull=True).order_by('name'))

        res = []
        impl_dec = ImplementingDecree.objects.by_legislature(leg)
        impl_dec_to_adopt = impl_dec.filter(is_adopted=False)
        impl_dec_adopted = impl_dec.filter(is_adopted=True)
        tot_impl = ImplementingDecree.objects.by_legislature(leg).filter(
            is_valid=True,
            is_adopted=False,
            fundings__isnull=False) \
            .exclude(fundings={})
        df_tot_impl = pd.DataFrame(tot_impl.values_list('fundings', flat=True))
        df_tot_impl = df_tot_impl.sum()
        indeces = df_tot_impl > 500_000_000
        min_year = int(min(indeces[indeces.values].index))
        max_year = int(max([max(x.keys()) for x in tot_impl.values_list('fundings', flat=True)]))
        for ministry in ministries:
            impl_decree_min_to_adopt = impl_dec_to_adopt \
                .filter(organizations_impl_dec__organization=ministry,
                        organizations_impl_dec__type=ImplementingDecreeOrganizations.PROPOSER)
            impl_decree_min_adopted = impl_dec_adopted \
                .filter(organizations_impl_dec__organization=ministry,
                        organizations_impl_dec__type=ImplementingDecreeOrganizations.PROPOSER)
            fundings = impl_decree_min_to_adopt.values_list('fundings', flat=True)
            df = pd.DataFrame(fundings).fillna(0).apply(sum)
            df = df.sort_index()
            default_index = list(map(str, range(min_year, max_year + 1)))
            df = df.reindex(default_index, fill_value=0)
            if int(impl_decree_min_to_adopt.count()) == 0 and int(impl_decree_min_adopted.count()) == 0:
                continue
            res.append(
                {

                    'name': str(ministry.name),
                    'id': ministry.id,
                    'n_impl_dec_to_adopt': int(impl_decree_min_to_adopt.count()),
                    'n_expired': impl_decree_min_to_adopt.exclude(deadline_date__isnull=True).filter(
                        deadline_date__lt=today
                    ).count(),
                    'n_impl_dec_adopted': int(impl_decree_min_adopted.count()),
                    'resources':
                        {
                            'total': float(df.sum()),
                            'detail': [
                                {
                                    'year': int(x[0]),
                                    'value': float(x[1])
                                } for x in zip(df.index, df)]

                        },
                }
            )

        return res

    @property
    def government_name(self):
        gov_name = self.government.name
        return ' '.join(gov_name.split('Governo')[::-1]).strip()

    @classmethod
    def get_api_aggregate(cls, leg='19'):

        imp = cls.objects.by_legislature(leg)
        to_adopt = imp.filter(is_adopted=False)
        adopted = imp.filter(is_adopted=True)
        governments_detail = []
        for id, name in imp.values_list('government', 'government__name').order_by('government__start_date').distinct():
            governments_detail.append({
                'name': ' '.join(name.split('Governo')[::-1]).strip(),
                'id': id,
                'to_adopt': to_adopt.filter(government=id).count(),
                'adopted': adopted.filter(government=id).count(),
            })

        return {
            'cnt': {
                'to_adopt': to_adopt.count(),
                'adopted': adopted.count(),
                'detail':
                    {
                        'governments': governments_detail,
                        'act_typology': [
                            {
                                'name': 'Legge',
                                'to_adopt': to_adopt.filter(
                                    ending_relations__starting_ct__model='bill').distinct().count(),
                                'adopted': adopted.filter(
                                    ending_relations__starting_ct__model='bill').distinct().count()
                            },
                            {
                                'name': 'Decreto legge',
                                'to_adopt': to_adopt.filter(
                                    ending_relations__starting_ct__model='govdecree').distinct().count(),
                                'adopted': adopted.filter(
                                    ending_relations__starting_ct__model='govdecree').distinct().count()
                            },
                            {
                                'name': 'Decreto legislativo',
                                'to_adopt': to_adopt.filter(
                                    ending_relations__starting_ct__model='govbill').distinct().count(),
                                'adopted': adopted.filter(
                                    ending_relations__starting_ct__model='govbill').distinct().count()
                            },
                        ]
                    }
            },
            'orgs': cls._get_ministries_impl_decr()
        }

    @property
    def title(self):
        return (self.public_title or self.original_title)


class GovBill(Act):
    """A government bill """
    preliminary_sitting = models.ForeignKey(
        to=GovSitting,
        blank=True, null=True,
        on_delete=models.PROTECT,
        related_name="gov_bills_preliminary",
        help_text=_("The sitting where the decree was announced")
    )

    decision_sitting = models.ForeignKey(
        to=GovSitting,
        on_delete=models.PROTECT,
        related_name="gov_bills_adopted",
        help_text=_("The sitting where the decree was adopted")
    )

    bill_date = models.DateField(
        _("Bill date"),
        help_text=_("The date of the d.lgs"),
    )

    publication_gu_date = models.DateField(
        _("publication date"),
        help_text=_("The date of the decree is officially published"),
    )

    is_omnibus = models.BooleanField(
        default=False,
        help_text=_("If the act is omnibus")
    )

    is_minotaurus = models.BooleanField(
        default=False,
        help_text=_("If the act is minotaurus")
    )

    is_subject_to_agreements = models.BooleanField(
        default=False,
        help_text=_("If the act is published 'salvo intese' ")
    )

    DELEGATION = 'DEL'
    DIRECTIVE = 'DIR'
    REGIONAL_STAT = 'RSTA'
    BILL_TYPES = [
        (DELEGATION, 'Legge delega'),
        (DIRECTIVE, 'Direttiva'),
        (REGIONAL_STAT, 'Decreti attuativi degli statuti regionali'),
    ]

    type = models.CharField(
        null=True,
        max_length=5,
        choices=BILL_TYPES,
        help_text=_("The type of nature for this bill")
    )

    objects = LegGovBillManager()

    @classmethod
    def update_cache_get_api_aggregate(cls, leg='19'):
        res = cls._get_api_aggregate(leg=leg)
        cache.set(
            'govbill_get_api_aggregate',
            {'res': res},
            timeout=None
        )

    @property
    def delegation_law(self):
        del_law = ActRelationship.objects.filter(ending_id=self.id,
                                                 ending_ct=ContentType.objects.get(model='govbill'),
                                                 starting_ct=ContentType.objects.get(model='bill'))
        return Bill.objects.filter(id__in=del_law.values('starting_id'))

    @classmethod
    def get_api_aggregate(cls, leg='19'):
        if not cache.get('govbill_get_api_aggregate'):
            cls.update_cache_get_api_aggregate(leg)
        return cache.get('govbill_get_api_aggregate').get('res')

    @classmethod
    def _get_api_aggregate(cls, leg='19'):

        dlgs = cls.objects.by_legislature(leg=leg)

        year_month = []
        years = cls.objects.by_legislature(leg='19').values_list('publication_gu_date__year', flat=True)
        for year in sorted(list(set(years))):
            year_month.append({'year': year,
                               'months': []})
            months = cls.objects.by_legislature(leg='19').filter(publication_gu_date__year=year).values(
                'publication_gu_date__month').distinct('publication_gu_date__month') \
                .order_by('publication_gu_date__month').values_list('publication_gu_date__month', flat=True)
            for month in set(months):
                gov_bills = cls.objects.by_legislature(leg='19').filter(publication_gu_date__year=year,
                                                                        publication_gu_date__month=month)
                year_month[-1]['months'].append({
                    'name': month,
                    'value': gov_bills.count()
                })

        return {
            'cnt': dlgs.count(),
            'historical': year_month
        }

    @property
    def title(self):
        if self.public_title:
            return self.public_title
        elif self.descriptive_title:
            return BeautifulSoup(self.descriptive_title, 'html.parser').text.strip()
        else:
            return BeautifulSoup(self.original_title, 'html.parser').text.strip()

    @property
    def implementing_decrees(self):
        return ImplementingDecree.objects.filter(
            is_valid=True,
            id__in=self.starting_relations.filter(ending_ct__model='implementingdecree').values_list('ending_id',
                                                                                                     flat=True))

    @property
    def implemented_decrees(self):
        return self.implementing_decrees.filter(is_valid=True)

    @property
    def implementing(self):
        if not self.implementing_decrees.count():
            return {
                "to_adopt": {
                    "cnt": 0
                },
                "adopted":
                    {
                        "cnt": 0
                    },
                "to_adopt_locked_resources":
                    {
                        'total': 0,
                        'detail': []

                    }
            }
        df_tot_impl = pd.DataFrame(self.implementing_decrees.values_list('fundings', flat=True))
        df_tot_impl = df_tot_impl.sum()
        indeces = df_tot_impl > 0
        if not indeces.empty:
            min_year = int(min(indeces[indeces.values].index))
            max_year = int(
                max([max(x.keys()) for x in self.implementing_decrees.values_list('fundings', flat=True) if x]))
            impl_decree_min_to_adopt = self.implementing_decrees.filter(is_adopted=False)
            fundings = impl_decree_min_to_adopt.values_list('fundings', flat=True)
            df = pd.DataFrame(fundings).fillna(0).apply(sum)
            df = df.sort_index()
            default_index = list(map(str, range(min_year, max_year + 1)))
            df = df.reindex(default_index, fill_value=0)
            detail = [
                {
                    'year': int(x[0]),
                    'value': float(x[1])
                } for x in zip(df.index, df)]
        else:
            detail = []

        res = {
            "to_adopt": {
                "cnt": self.implementing_decrees.count() - self.implemented_decrees.filter(is_adopted=True).count()
            },
            "adopted":
                {
                    "cnt": self.implemented_decrees.filter(is_adopted=True).count()
                },
            "to_adopt_locked_resources":
                {
                    'total': 0 if detail == [] else float(df.sum()),
                    'detail': detail

                }
        }
        return res


def map_color_status(phase):
    if phase in ["Approvato",
                 "Approvato con modificazioni",
                 "Approvato definitivamente legge",
                 "Approvato in testo unificato",
                 "Assorbito"]:
        return "Approvato"
    else:
        return "Respinto"


class Bill(Act):
    """A bill, from the Parliament"""

    assembly = models.ForeignKey(
        to=Organization,
        limit_choices_to={'classification__in': ["Assemblea parlamentare", ]},
        on_delete=models.PROTECT,
        related_name="acts",
        help_text=_("The assembly where the act was presented")
    )
    ORDINARY = 'ORD'
    DL_CONVERSION = 'CONV'
    FINANCIAL = 'FIN'
    COSTITUTIONAL = 'COST'
    BILL_TYPES = [
        (ORDINARY, 'Legge ordinaria'),
        (DL_CONVERSION, 'Legge di conversione'),
        (FINANCIAL, 'Legge di bilancio'),
        (COSTITUTIONAL, 'Legge costituzionale'),
    ]
    type = models.CharField(
        null=True,
        max_length=5,
        choices=BILL_TYPES,
        help_text=_("The type of nature for this bill")
    )

    is_omnibus = models.BooleanField(
        default=False,
        help_text=_("If the act is omnibus")
    )

    is_minotaurus = models.BooleanField(
        default=False,
        help_text=_("If the act is minotaurus")
    )

    is_subject_to_agreements = models.BooleanField(
        default=False,
        help_text=_("If the act is published 'salve intese' ")
    )

    is_ratification = models.BooleanField(
        default=False,
        help_text=_("If the act is ratifcation' ")
    )

    @property
    def branch(self) -> str:
        if 'ASS-CAMERA' in self.assembly.identifier:
            return 'C'
        elif 'ASS-SENATO' in self.assembly.identifier:
            return 'S'
        else:
            raise Exception("Wrong branch")

    @property
    def branch_detail(self) -> str:
        if 'ASS-CAMERA' in self.assembly.identifier:
            return "Camera"
        elif 'ASS-SENATO' in self.assembly.identifier:
            return "Senato"
        else:
            raise Exception("Wrong branch")

    @property
    def title(self):
        return (self.public_title or self.original_title)

    @property
    def leg(self):
        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        dp = self.date_presenting
        ke = KeyEvent.objects.annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today_strf))) \
            .get(start_date__lte=dp, end_date_coalesce__gte=dp, event_type='ITL')
        return ke.identifier.split('_')[-1]

    @property
    def source_url(self):
        if self.identifier.startswith('S'):
            return f'https://www.senato.it/leg/{self.leg}/BGT/Schede/Ddliter/{self.source_identifier}.htm'
        else:
            return (f'https://www.camera.it/leg{self.leg}/126?tab=&leg={self.leg}&idDocumento='
                    f'{self.identifier.split(".")[-1]}&sede=&tipo=')

    PARLIAMENT = 'PARL'
    GOVERNMENT = 'GOV'
    REGION = 'REG'
    POPULAR = 'POP'
    CNEL = 'CNEL'
    INITIATIVE_TYPES = [
        (PARLIAMENT, 'Parlamentare'),
        (GOVERNMENT, 'Governativa'),
        (REGION, 'Regionale'),
        (POPULAR, 'Popolare'),
        (CNEL, _('CNEL')),
    ]
    initiative = models.CharField(
        max_length=5,
        choices=INITIATIVE_TYPES,
        help_text=_("The type of initiative for this bill"),
        null=True
    )

    is_law = models.BooleanField(
        default=False,
        help_text=_("Whether this act is a law")
    )
    is_final_by_commission = models.BooleanField(
        default=False,
        help_text=_("Whether this act is approved in commission")
    )
    is_final_by_hands_voting = models.BooleanField(
        default=False,
        help_text=_("Whether this act is is approved by hand-voting")
    )
    signers = models.ManyToManyField(
        to=OPDMMembership,
        verbose_name=_("popolo.Membership"),
        through=BillSigner,
        related_name="bills_signed",
        help_text=_("All signer memberships associatd to the bill")
    )

    spokepersons = models.ManyToManyField(
        to=OPDMMembership,
        verbose_name=_("popolo.Membership"),
        through=BillSpeaker,
        related_name="bills_spokeperson",
        help_text=_("All spokeperson memberships associatd to the bill")
    )

    iter_steps = models.ManyToManyField(
        to=IterPhase,
        verbose_name=_("Iter steps"),
        through=Iter,
        related_name="bills",
        help_text=_("All iter phases this bill has gone throug")
    )
    gu_url = models.URLField(
        blank=True, null=True,
        help_text=_("The Gazzetta Ufficiale URL where this bill was published")
    )

    ddl = models.IntegerField(
        blank=True, null=True,
        help_text=_("It identifies the overall act across all legislature phases")
    )

    date_presenting = models.DateField(
        null=True, blank=True,
        help_text=_("It identifies the date the act was presented in the chamber"))

    law_number = models.SmallIntegerField(
        null=True, blank=True,
        help_text=_("It identifies the number of the law approved")
    )

    law_publication_date = models.DateField(
        null=True, blank=True,
        help_text=_("It identifies the date of the law approved")
    )

    source_identifier = models.IntegerField(
        null=True, blank=True,
        help_text=_("It identifies the source unique identifier")
    )

    LAW = 'L'
    ONE_BRANCH = 'A'
    EXAMINATION = 'E'
    TO_START = 'S'
    REJECTED = 'R'
    STATUS = [
        (LAW, 'Divenuto legge'),
        (ONE_BRANCH, 'Approvato da un ramo'),
        (EXAMINATION, 'Esame in prima lettura'),
        (TO_START, 'Iter da avviare'),
        (REJECTED, 'Rigettato/Respinto/Restituito'),
    ]
    status = models.CharField(
        max_length=5,
        choices=STATUS,
        help_text="Macro status dell'atto",
        null=True
    )
    previous_codes = ArrayField(
        models.CharField(max_length=10),
        size=10,
        null=True, blank=True,
    )

    origin = models.BooleanField(default=False)

    objects = LegBillsManager()

    @property
    def get_gov_decree(self) -> GovDecree:
        """Only if type DL CONVERSION"""
        if self.type == Bill.DL_CONVERSION:
            return GovDecree.objects.get(id=self.parliament_iter['detail'][0]['previous_codes'][0]['id'])
        return GovDecree.objects.none()

    @property
    def days_to_publication(self):
        if self.is_law:
            return (self.previous_tree[0].law_publication_date - self.previous_tree[-1].date_presenting).days
        else:
            return (self.previous_tree[0].steps.last().status_date - self.previous_tree[-1].date_presenting).days

    def get_admin_url(self):
        content_type = ContentType.objects.get_for_model(self.__class__)
        rev = reverse("admin:%s_%s_change" % (content_type.app_label, content_type.model), args=(self.id,))
        return f'{Site.objects.first().name}{rev}'

    def last_status(self):
        return self.steps.order_by('-status_date', '-phase__is_completed').first()

    @property
    def last_status_last_date(self):
        return self.last_status().status_date

    @property
    def last_main_status(self):
        return {
            'date': None,
            'phase': self.get_last_bill_tree().get_status_display()
        }

    @property
    def last_status_dict(self):
        ls = self.last_status()
        return {
            'date': ls.status_date,
            'phase': ls.phase.phase
        }

    def get_last_bill_tree(self):
        next_act = self.starting_relations.filter(classification__descr='Successivo', ending_ct__model='bill')
        if next_act.count() > 0:
            return next_act.first().ending_object.get_last_bill_tree()
        else:
            return (next_act.first().ending_object if next_act else self)

    def get_last_status(self, act, if_approved=False):
        next_act = act.starting_relations.filter(classification__descr='Successivo', ending_ct__model='bill')
        if next_act.count() > 0:
            if_approved = len([x for x in self.iter_steps.values_list('phase', flat=True) if 'Approvato' in x]) > 0
            return self.get_last_status(next_act.first().ending_object, if_approved)
        else:
            if act.last_status().phase.phase in ['Approvato definitivamente legge']:
                return 'l'
            else:
                if if_approved:
                    return 'c'
                else:
                    return 'n'

    def is_origin(self):

        last_staus = self.last_status().phase

        if (last_staus.phase == 'Conclusione anomala per stralcio' or 'Assorbito' in self.steps.values_list(
            'phase__phase', flat=True)):
            return (False, None)
        elif self.ending_relations.filter(starting_ct__model='bill',
                                          classification__code='nextprev_value').count() == 0:
            return (True, self.get_last_status(self))
        else:
            return (False, None)

    @property
    def first_signers(self):
        return self.bill_signers.filter(first_signer=True) \
            .annotate(name=F('membership__person__name')) \
            .values('name', 'membership', )

    @property
    def first_signers_instances(self):
        return self.bill_signers.filter(first_signer=True)

    @property
    def co_signers(self):
        return self.bill_signers.filter(first_signer=False)

    @property
    def relators(self):
        return self.bill_speakers

    @classmethod
    def get_bills_by_leg(cls, leg='19'):
        return cls.objects.filter(assembly__identifier__endswith=leg)

    @classmethod
    def get_last_bill(cls, leg='19'):
        ''' It returns only the acts that do not have next Bill acts'''
        top_n_steps_per_bill = Iter.objects.filter(bill=OuterRef('bill')).order_by('-status_date',
                                                                                   '-phase__is_completed')[:1]
        top_n_steps = Iter.objects.filter(id__in=Subquery(top_n_steps_per_bill.values('id')))
        bills = top_n_steps.values('bill')
        return cls.objects.by_legislature(leg=leg).filter(id__in=bills) \
            .exclude(starting_relations__ending_ct_id=ContentType
                     .objects.get_for_model(cls).id).distinct()

    def last_act_approved_before(self):
        ls = [x.starting_object.assembly for x in self.ending_relations.filter(starting_ct__model='bill')]
        if (len(ls) > 0 and ls != [self.assembly]) or 'Approvato' in self.last_status().phase.phase:
            return True
        return False

    @classmethod
    def update_cache_get_api_aggregate(self):
        res = self._get_api_aggregate()
        cache.set(
            'bill_get_api_aggregate',
            {'res': res},
            timeout=None
        )

    @classmethod
    def get_api_aggregate(cls):
        if not cache.get('bill_get_api_aggregate'):
            cls.update_cache_get_api_aggregate()
        return cache.get('bill_get_api_aggregate').get('res')

    @classmethod
    def _get_api_aggregate(cls, leg='19'):
        ddl = cls.get_last_bill(leg)

        not_law = [{
            'id': x.id,
            'appr_bef': x.last_act_approved_before(),
            'rat': x.is_ratification,
            'gov': x.initiative == Bill.GOVERNMENT} for x in ddl.exclude(
            steps__phase__phase__in=['Approvato definitivamente legge',
                                     'Approvato definitivamente non ancora pubblicato'])]

        one_branch_ids = [x['id'] for x in not_law if x['appr_bef']]

        one_branch_gov = sum([int(x['appr_bef']) for x in not_law if x['gov'] == True and x['rat'] == False])  # noqa
        one_branch_not_gov = sum(
            [int(x['appr_bef']) for x in not_law if x['gov'] == False and x['rat'] == False])  # noqa
        one_branch_rat_gov = sum([int(x['appr_bef']) for x in not_law if x['gov'] == True and x['rat'] == True])  # noqa
        one_branch_rat_not_gov = sum(
            [int(x['appr_bef']) for x in not_law if x['gov'] == False and x['rat'] == True])  # noqa

        # Quelli o da iniziare o in prima fase
        first_phase = ddl.exclude(id__in=one_branch_ids).exclude(
            steps__phase__phase__in=['Approvato definitivamente legge',
                                     'Approvato definitivamente non ancora pubblicato'])
        to_start = [x.id for x in first_phase if x.last_status().phase.phase in ['Assegnato (no esame)',
                                                                                 'Da assegnare a commissione']]
        to_start_objects = Bill.objects.filter(id__in=to_start)
        started = [x.id for x in first_phase if x.last_status().phase.phase not in ['Assegnato (no esame)',
                                                                                    'Da assegnare a commissione',
                                                                                    'Ritirato',
                                                                                    'Respinto',
                                                                                    'Restituito al governo',
                                                                                    'D-L decaduto']]
        started_objects = Bill.objects.filter(id__in=started)

        rejected = ddl.exclude(steps__phase__phase__in=['Approvato definitivamente legge',
                                                        'Approvato definitivamente non ancora pubblicato']) \
            .exclude(id__in=[x.id for x in to_start_objects] + [x.id for x in started_objects] + one_branch_ids)
        to_reject = Bill.objects.filter(id__in=[x.id for x in rejected if x.last_status().phase.phase in ['Ritirato',
                                                                                                          'Respinto',
                                                                                                          'Restituito '
                                                                                                          'al governo',
                                                                                                          'D-L decaduto'
                                                                                                          ]])

        year_month = []
        years = Bill.objects.by_legislature(leg='19').values_list('date_presenting__year', flat=True)
        years_unique = sorted(set(years))
        approvato_definitivamente_legge = (
            Bill.objects.by_legislature(leg='19').filter(steps__phase__phase='Approvato definitivamente legge')
            .exclude(steps__phase__phase='Approvato con modificazioni'))
        approvato_definitivamente_non_ancora_pubblicato = (
            (Bill.objects.by_legislature(leg='19').exclude(id__in=approvato_definitivamente_legge.values('id'))
             .filter(steps__phase__phase='Approvato definitivamente non ancora pubblicato'))
            .exclude(steps__phase__phase='Approvato con modificazioni'))
        approved_all = Bill.objects.filter(
            id__in=approvato_definitivamente_legge.union(approvato_definitivamente_non_ancora_pubblicato)
            .values('id')).annotate(last_status_date=Max(F('steps__status_date')))
        for year in years_unique:
            year_month.append({'year': year,
                               'months': []})
            months = Bill.objects.by_legislature(leg='19').filter(date_presenting__year=year).values(
                'date_presenting__month').distinct('date_presenting__month') \
                .order_by('date_presenting__month').values_list('date_presenting__month', flat=True)
            for month in set(months):
                pym = Bill.objects.by_legislature(leg='19').filter(date_presenting__year=year,
                                                                   date_presenting__month=month)

                approved = approved_all.filter(last_status_date__year=year, last_status_date__month=month)
                k_p_acts = pym.filter(is_key_act=True)
                k_p_acts_approved = approved.filter(is_key_act=True)
                year_month[-1]['months'].append(
                    {
                        'name': month,
                        'res': {
                            'all':
                                {
                                    'presented': {
                                        'gov': pym.filter(initiative=Bill.GOVERNMENT).exclude(
                                            is_ratification=True).count(),
                                        'not_gov': pym.exclude(initiative=Bill.GOVERNMENT).exclude(is_ratification=True)
                                        .count(),
                                        'cnt_ratifiction': {
                                            'gov': pym.filter(initiative=Bill.GOVERNMENT).filter(is_ratification=True)
                                        .count(),
                                            'not_gov': pym.exclude(initiative=Bill.GOVERNMENT).filter(
                                                is_ratification=True)
                                        .count(),
                                        }
                                    },
                                    'approved': {
                                        'gov': approved.filter(initiative=Bill.GOVERNMENT, steps__phase__phase__in=[
                                            'Approvato definitivamente legge',
                                            'Approvato definitivamente non ancora pubblicato']).exclude(
                                            is_ratification=True).distinct().count(),
                                        'not_gov': approved.exclude(initiative=Bill.GOVERNMENT).filter(
                                            steps__phase__phase__in=['Approvato definitivamente legge',
                                                                     'Approvato definitivamente non ancora '
                                                                     'pubblicato']).exclude(
                                            is_ratification=True).distinct().count(),
                                        'cnt_ratifiction': {
                                            'gov': approved.filter(initiative=Bill.GOVERNMENT, steps__phase__phase__in=[
                                                'Approvato definitivamente legge',
                                                'Approvato definitivamente non ancora pubblicato'],
                                                                   is_ratification=True).distinct().count(),
                                            'not_gov': approved.exclude(initiative=Bill.GOVERNMENT)
                                        .filter(steps__phase__phase__in=['Approvato definitivamente legge',
                                                                         'Approvato definitivamente non ancora '
                                                                         'pubblicato'],
                                                is_ratification=True).distinct().count(),
                                        }
                                    },
                                },
                            'key_acts':
                                {
                                    'presented': {
                                        'gov': k_p_acts.filter(initiative=Bill.GOVERNMENT).exclude(is_ratification=True)
                                        .count(),
                                        'not_gov': k_p_acts.exclude(initiative=Bill.GOVERNMENT)
                                        .exclude(is_ratification=True).count(),
                                        'cnt_ratification': {
                                            'gov': k_p_acts.filter(initiative=Bill.GOVERNMENT, is_ratification=True)
                                        .count(),
                                            'not_gov': k_p_acts.exclude(initiative=Bill.GOVERNMENT,
                                                                        is_ratification=True).count()
                                        }
                                    },
                                    'approved': {
                                        'gov': k_p_acts_approved.filter(initiative=Bill.GOVERNMENT,
                                                                        steps__phase__phase__in=[
                                                                            'Approvato definitivamente legge',
                                                                            'Approvato definitivamente non ancora '
                                                                            'pubblicato'])
                                        .exclude(is_ratification=True).count(),
                                        'not_gov': k_p_acts_approved.exclude(initiative=Bill.GOVERNMENT).filter(
                                            steps__phase__phase__in=['Approvato definitivamente legge',
                                                                     'Approvato definitivamente non ancora '
                                                                     'pubblicato']).exclude(
                                            is_ratification=True).count(),
                                        'cnt_ratification': {
                                            'gov': k_p_acts_approved.filter(initiative=Bill.GOVERNMENT,
                                                                            steps__phase__phase__in=[
                                                                                'Approvato definitivamente legge',
                                                                                'Approvato definitivamente non ancora '
                                                                                'pubblicato'],
                                                                            is_ratification=True).count(),
                                            'not_gov': k_p_acts_approved.exclude(initiative=Bill.GOVERNMENT)
                                        .filter(steps__phase__phase__in=['Approvato definitivamente legge',
                                                                         'Approvato definitivamente non ancora '
                                                                         'pubblicato'],
                                                is_ratification=True).count()
                                        }
                                    }
                                }
                        }
                    }
                )

        res = {
            'cnt': {
                'gov': ddl.filter(initiative=Bill.GOVERNMENT).exclude(is_ratification=True).count(),
                'not_gov': ddl.exclude(initiative=Bill.GOVERNMENT).exclude(is_ratification=True).count()
            },
            'cnt_ratification': {
                'gov': ddl.filter(is_ratification=True).filter(initiative=Bill.GOVERNMENT).count(),
                'not_gov': ddl.filter(is_ratification=True).exclude(initiative=Bill.GOVERNMENT).count()
            },
            'detail': {
                'is_law': {
                    'total': {
                        'gov': ddl.filter(steps__phase__phase__in=['Approvato definitivamente legge',
                                                                   'Approvato definitivamente non ancora '
                                                                   'pubblicato']).filter(
                            initiative=Bill.GOVERNMENT)
                        .exclude(is_ratification=True).count(),
                        'not_gov': ddl.filter(steps__phase__phase__in=['Approvato definitivamente legge',
                                                                       'Approvato definitivamente non ancora '
                                                                       'pubblicato']).exclude(
                            initiative=Bill.GOVERNMENT)
                        .exclude(is_ratification=True).count()},
                    'ratification':
                        {
                            'gov': ddl.filter(steps__phase__phase__in=['Approvato definitivamente legge',
                                                                       'Approvato definitivamente non ancora '
                                                                       'pubblicato'],
                                              is_ratification=True,
                                              initiative=Bill.GOVERNMENT).count(),
                            'not_gov': ddl.filter(steps__phase__phase__in=['Approvato definitivamente legge',
                                                                           'Approvato definitivamente non ancora '
                                                                           'pubblicato'],
                                                  is_ratification=True)
                            .exclude(initiative=Bill.GOVERNMENT).count()
                        }
                },
                'one_branch': {
                    'total': {
                        'gov': one_branch_gov,
                        'not_gov': one_branch_not_gov
                    },
                    'ratification': {
                        'gov': one_branch_rat_gov,
                        'not_gov': one_branch_rat_not_gov
                    }
                },
                'first_step': {
                    'total':
                        {
                            'gov': started_objects.exclude(is_ratification=True)
                            .filter(initiative=Bill.GOVERNMENT).count(),
                            'not_gov': started_objects.exclude(is_ratification=True)
                            .exclude(initiative=Bill.GOVERNMENT).count()
                        },
                    'ratification':
                        {
                            'gov': started_objects.filter(initiative=Bill.GOVERNMENT, is_ratification=True).count(),
                            'not_gov': started_objects.exclude(initiative=Bill.GOVERNMENT).filter(is_ratification=True)
                            .count()
                        }
                },
                'to_begin': {
                    'total':
                        {
                            'gov': to_start_objects.exclude(is_ratification=True)
                            .filter(initiative=Bill.GOVERNMENT).count(),
                            'not_gov': to_start_objects.exclude(is_ratification=True)
                            .exclude(initiative=Bill.GOVERNMENT).count()
                        },
                    'ratification':
                        {
                            'gov': to_start_objects.filter(initiative=Bill.GOVERNMENT,
                                                           is_ratification=True).count(),
                            'not_gov': to_start_objects.exclude(initiative=Bill.GOVERNMENT).filter(is_ratification=True)
                            .count()
                        }
                },
                'rejected': {
                    'total':
                        {
                            'gov': to_reject.exclude(is_ratification=True)
                            .filter(initiative=Bill.GOVERNMENT).count(),
                            'not_gov': to_reject.exclude(is_ratification=True)
                            .exclude(initiative=Bill.GOVERNMENT).count()
                        },
                    'ratification':
                        {
                            'gov': to_reject.filter(initiative=Bill.GOVERNMENT,
                                                    is_ratification=True).count(),
                            'not_gov': to_reject.exclude(initiative=Bill.GOVERNMENT).filter(is_ratification=True)
                            .count()
                        }
                }
            },
            'historical': year_month
        }
        return res

    @property
    def get_count_trust_votes(self):
        res = 0
        laws_tree = self.previous_tree
        for i in laws_tree:
            if isinstance(i, Bill):
                res += i.votings.filter(is_confidence=True).count()
        return res

    @property
    def act_iter(self):
        return self.steps.all().order_by('-status_date', '-phase__is_completed')

    @property
    def voting_stats(self):
        related = self.votings.filter(id__in=self.votings.filter(members_votes__isnull=False).values('id')).distinct()
        return {
            "related": related.count(),
            "with_confidence": related.filter(is_confidence=True).count(),
            "with_rebels": sum([(x.n_rebels or 0) > 0 for x in related]),
            "majority_cohesion_rate": round(
                statistics.mean([x for x in related.values_list('majority_cohesion_rate', flat=True) if x]),
                2) if related else None,
            "minority_cohesion_rate": round(
                statistics.mean([x for x in related.values_list('minority_cohesion_rate', flat=True) if x]),
                2) if related else None,
        }

    @property
    def get_votings(self):
        return self.votings.all().order_by('sitting__date', 'number')

    @property
    def implementing_decrees(self):
        return ImplementingDecree.objects.filter(
            is_valid=True,
            id__in=self.starting_relations.filter(ending_ct__model='implementingdecree').values_list('ending_id',
                                                                                                     flat=True))

    @property
    def implemented_decrees(self):
        return self.implementing_decrees.filter(is_adopted=True)

    @property
    def implementing(self):
        if not self.implementing_decrees.count():
            return {
                "to_adopt": {
                    "cnt": 0
                },
                "adopted":
                    {
                        "cnt": 0
                    },
                "to_adopt_locked_resources":
                    {
                        'total': 0,
                        'detail': []

                    }
            }
        df_tot_impl = pd.DataFrame(self.implementing_decrees.values_list('fundings', flat=True))
        df_tot_impl = df_tot_impl.sum()
        indeces = df_tot_impl > 0
        if not indeces.empty:
            min_year = int(min(indeces[indeces.values].index))
            max_year = int(
                max([max(x.keys()) for x in self.implementing_decrees.values_list('fundings', flat=True) if x]))
            impl_decree_min_to_adopt = self.implementing_decrees.filter(is_adopted=False)
            fundings = impl_decree_min_to_adopt.values_list('fundings', flat=True)
            df = pd.DataFrame(fundings).fillna(0).apply(sum)
            df = df.sort_index()
            default_index = list(map(str, range(min_year, max_year + 1)))
            df = df.reindex(default_index, fill_value=0)
            detail = [
                {
                    'year': int(x[0]),
                    'value': float(x[1])
                } for x in zip(df.index, df)]
        else:
            detail = []

        res = {
            "to_adopt": {
                "cnt": self.implementing_decrees.count() - self.implemented_decrees.filter(is_adopted=True).count()
            },
            "adopted":
                {
                    "cnt": self.implemented_decrees.filter(is_adopted=True).count()
                },
            "to_adopt_locked_resources":
                {
                    'total': 0 if detail == [] else float(df.sum()),
                    'detail': detail

                }
        }
        return res

    @property
    def parliament_iter(self):
        previous_steps = self.previous_tree[::-1]
        next_steps = self.next_tree[1::]
        detail = []
        previous_case = {'label': None}
        for i in previous_steps:
            if i._meta.model_name == 'govdecree':
                detail.append({
                    "label": "Decreto legge",
                    "branch": i.sitting.government.name,
                    "date": i.sitting.ending_datetime.date(),
                    "previous_codes": [{'value': i.identifier, 'slug': i.slug, 'id': i.id, "type": 'decree'}],
                    "code_prefix": None,
                    "post_codes": [],
                    "color": "Approvato"
                })
            if i._meta.model_name == 'bill':
                code_prefix = None
                prev_union = i.ending_relations.filter(classification__descr='Unione')
                prev_union = [x for x in prev_union if x.starting_object.identifier.startswith(i.identifier[0])]
                if i.last_status_dict.get('phase') == 'Assorbito':
                    code_prefix = 'da'
                elif 'unificato' in i.last_status_dict.get('phase') and i.id == self.id:
                    code_prefix = 'con'
                if i.last_status_dict.get('phase') == previous_case.get('label') \
                    and i.last_status_dict.get('phase') == 'Approvato in testo unificato':
                    previous_case['previous_codes'].append(
                        {'value': i.identifier, 'slug': i.slug, 'id': i.id, "type": 'bill'})
                else:
                    post_codes = []
                    if code_prefix == 'da':
                        post_codes = i.related_ending_acts(classification__descr='Unione')
                    elif code_prefix == 'con':
                        try:
                            post_codes = [x for x in i.related_ending_acts()[0].previous_acts if x.id != self.id]
                        except:
                            post_codes = []
                    if prev_union:
                        code_prefix = 'assorbe'
                        post_codes = [x.starting_object for x in prev_union]
                    detail.append({
                        "label": i.last_status_dict.get('phase'),
                        "branch": i.branch,
                        "date": i.last_status_dict.get('date'),
                        "previous_codes": [{'value': i.identifier, 'slug': i.slug, 'id': i.id, "type": 'bill'}],
                        "code_prefix": code_prefix,
                        "post_codes": [{'value': x.identifier, 'slug': x.slug, 'id': x.id, "type": 'bill'} for x in
                                       post_codes] if post_codes else None,
                        "color": "greylight" if not i.last_status().phase.is_completed else
                        map_color_status(i.last_status().phase.phase)
                    })
                previous_case = detail[-1]
        for i in next_steps:
            post_codes = []
            prev_union = i.ending_relations.filter(classification__descr='Unione')
            prev_union = [x for x in prev_union if x.starting_object.identifier.startswith(i.identifier[0])]
            if i._meta.model_name == 'bill':
                code_prefix = None
                if i.last_status_dict.get('phase') == 'Assorbito':
                    code_prefix = 'da'
                elif 'unificato' in i.last_status_dict.get('phase'):
                    code_prefix = 'con'
                if prev_union:
                    code_prefix = 'assorbe'
                    post_codes = [x.starting_object for x in prev_union]
                detail.append({
                    "label": i.last_status_dict.get('phase'),
                    "branch": i.branch,
                    "date": i.last_status_dict.get('date'),
                    "previous_codes": [{'value': i.identifier, 'slug': i.slug, 'id': i.id, "type": 'bill'}],
                    "code_prefix": code_prefix,
                    "post_codes": [{'value': x.identifier, 'slug': x.slug, 'id': x.id, "type": 'bill'} for x in
                                   post_codes] if post_codes else None,
                    "color": "greylight" if not i.last_status().phase.is_completed else
                    map_color_status(i.last_status().phase.phase)
                })
        if 'Approvato definitivamente legge' == detail[-1]['label'] \
            or 'Assorbito' in detail[-1]['label'] \
            or detail[-1]['color'] == 'Respinto':
            pass
        elif 'Approvato definitivamente non ancora pubblicato' == detail[-1]['label']:
            detail.append({
                "label": 'Diventa legge',
                "branch": detail[-1]['branch'],
                "date": None,
                # "previous_codes": detail[-1]['code'],
                "code_prefix": None,
                "post_codes": [],
                "color": "greysuperlight"
            })
        else:
            if detail[-1]['label'] in ['Approvato', 'Approvato con modificazioni']:
                detail.append({
                    "label": 'Da approvare',
                    "branch": "C" if detail[-1]['branch'] == "S" else "S",
                    "date": None,
                    "previous_codes": None,
                    "code_prefix": None,
                    "post_codes": [],
                    "color": "greysuperlight"
                })
                if i.type == Bill.COSTITUTIONAL:
                    approvals = [x for x in detail if 'approvato' in x['label'].lower()]
                    len_approvals = len(approvals)

                    if len_approvals == 2:
                        detail.append({
                            "label": 'Da approvare',
                            "branch": "C" if detail[-1]['branch'] == "S" else "S",
                            "date": None,
                            "previous_codes": None,
                            "code_prefix": None,
                            "post_codes": [],
                            "color": "greysuperlight"
                        })
                    elif len_approvals == 1:
                        detail.append({
                            "label": 'Da approvare',
                            "branch": "C" if detail[-1]['branch'] == "S" else "S",
                            "date": None,
                            "previous_codes": None,
                            "code_prefix": None,
                            "post_codes": [],
                            "color": "greysuperlight"
                        })
                        detail.append({
                            "label": 'Da approvare',
                            "branch": "C" if detail[-1]['branch'] == "S" else "S",
                            "date": None,
                            "previous_codes": None,
                            "code_prefix": None,
                            "post_codes": [],
                            "color": "greysuperlight"
                        })

            else:
                detail.append({
                    "label": 'Da approvare',
                    "branch": detail[-1]['branch'],
                    "date": None,
                    "previous_codes": detail[-1]['previous_codes'],
                    "code_prefix": None,
                    "post_codes": [],
                    "color": "greysuperlight"
                })
                if len(list(set([x['branch'] for x in detail if x['branch'] in ['S', 'C']]))) == 1:
                    detail.append({
                        "label": 'Da approvare',
                        "branch": "C" if detail[-1]['branch'] == "S" else "S",
                        "date": None,
                        "previous_codes": None,
                        "code_prefix": None,
                        "post_codes": [],
                        "color": "greysuperlight"
                    })
                    if i.type == Bill.COSTITUTIONAL:
                        detail.append({
                            "label": 'Da approvare',
                            "branch": "C" if detail[-1]['branch'] == "S" else "S",
                            "date": None,
                            "previous_codes": None,
                            "code_prefix": None,
                            "post_codes": [],
                            "color": "greysuperlight"
                        })
                        detail.append({
                            "label": 'Da approvare',
                            "branch": "C" if detail[-1]['branch'] == "S" else "S",
                            "date": None,
                            "previous_codes": None,
                            "code_prefix": None,
                            "post_codes": [],
                            "color": "greysuperlight"
                        })
            if i.type == Bill.COSTITUTIONAL:
                detail.append({
                    "label": 'Referendum se non raggiunta maggioranza 2/3 in entrambi i rami',
                    "branch": None,
                    "date": None,
                    "previous_codes": None,
                    "code_prefix": None,
                    "post_codes": [],
                    "color": "greysuperlight"
                })
            detail.append({
                "label": 'Diventa legge',
                "branch": None,
                "date": None,
                "previous_codes": None,
                "code_prefix": None,
                "post_codes": [],
                "color": "greysuperlight"
            })

        res = {
            "status": {
                "label": "",
                "date": ""
            },
            "detail": detail
        }

        return res

    @property
    def get_codes(self):
        results = []
        starting_acts = self.related_starting_acts()
        for item in starting_acts:
            if isinstance(item, list):
                results.extend(self.get_codes)
            else:
                results.append(item)
        return [x.identifier for x in results]

    @property
    def get_date_presenting_tree(self):
        results = []
        starting_acts = self.related_starting_acts()
        for item in starting_acts:
            if isinstance(item, list):
                results.extend(self.get_codes)
            else:
                results.append(item)
        return min([x.date_presenting for x in results + [self]])

    @property
    def commissions_list(self):
        from project.opp.parl.models import decode_roman_numeral
        res = []
        cm = self.commissions.order_by('-appointing_date')
        not_cons = cm.exclude(typology='CONS')
        cons = cm.filter(typology='CONS')
        values_typology = not_cons.values_list('typology', flat=True).distinct()
        for v in values_typology:
            cms = not_cons.filter(typology=v)
            name = ''
            to_concat = []
            if cms.count() > 1:
                name = "Commissioni riunite "
            for c in cms:
                if c.organization.classification == 'Commissione permanente parlamentare':
                    if 'senato' in c.organization.parent.name.lower():
                        pattern = r"(\d+ª).*- (.*)\("
                        to_concat.append(' - '.join(re.findall(pattern, c.organization.name)[0]).strip())
                    else:
                        order = decode_roman_numeral(c.organization.name.split(' ')[0])
                        pattern = r"\((.*)\) Camera"
                        name_commission = re.findall(pattern, c.organization.name)[0].strip()
                        to_concat.append(f" {order}ª - {name_commission.capitalize()}")
                else:
                    to_concat.append(c.organization.name)
            name += ' e '.join(to_concat)
            res.append(
                {"sede": list(filter(lambda x: x[0] == v, BillCommission.TYPE))[0][1],
                 "name": name,
                 "classification": cms.first().organization.classification,
                 "date": cms.first().appointing_date

                 }
            )
        for con in cons:

            if con.organization.classification == 'Commissione permanente parlamentare':
                if 'senato' in con.organization.parent.name.lower():
                    pattern = r"(\d+ª).*- (.*)\("
                    name = ' - '.join(re.findall(pattern, con.organization.name)[0]).strip()
                else:
                    order = decode_roman_numeral(con.organization.name.split(' ')[0])
                    pattern = r"\((.*)\) Camera"
                    name_commission = re.findall(pattern, con.organization.name)[0].strip()
                    name = f" {order}ª - {name_commission.capitalize()}"
            else:
                name = con.organization.name
            res.append(
                {"sede": "Consultiva",
                 "name": name,
                 "classification": con.organization.classification,
                 "date": con.appointing_date

                 }
            )
        return res

    @property
    def gov_at_presenting_date(self):
        from project.opp.gov.models import Government
        return Government.get_government_by_date(self.date_presenting)

    @classmethod
    def update_previous_codes(cls, leg=None):
        bills = cls.get_bills_by_leg(leg)
        for bill in bills:
            bill.previous_codes = bill.get_codes
            bill.origin = bill.is_origin()[0]
            bill.save()

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["identifier", "assembly"],
                name='%(app_label)s_%(class)s_unique_identifier'
            )
        ]

        # ordering = ('-date_presenting',)
