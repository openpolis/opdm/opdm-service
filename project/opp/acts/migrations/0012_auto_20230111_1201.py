# Generated by Django 3.2.16 on 2023-01-11 11:01

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('acts', '0011_auto_20230105_1644'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='govbill',
            name='sitting',
        ),
        migrations.AddField(
            model_name='bill',
            name='public_title',
            field=models.CharField(blank=True, help_text="Title, as it's known publicly", max_length=1024, null=True, verbose_name='public title'),
        ),
        migrations.AddField(
            model_name='govbill',
            name='decision_sitting',
            field=models.ForeignKey(default=None, help_text='The sitting where the decree was adopted', on_delete=django.db.models.deletion.PROTECT, related_name='gov_bills_adopted', to='acts.govsitting'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='govbill',
            name='preliminary_sitting',
            field=models.ForeignKey(blank=True, help_text='The sitting where the decree was announced', null=True, on_delete=django.db.models.deletion.PROTECT, related_name='gov_bills_preliminary', to='acts.govsitting'),
        ),
        migrations.AddField(
            model_name='govbill',
            name='public_title',
            field=models.CharField(blank=True, help_text="Title, as it's known publicly", max_length=1024, null=True, verbose_name='public title'),
        ),
        migrations.AddField(
            model_name='govbill',
            name='type',
            field=models.CharField(choices=[('DEL', 'Delegation law'), ('DIR', 'Directive'), ('RSTA', 'Regional special charters')], help_text='The type of nature for this bill', max_length=5, null=True),
        ),
        migrations.AddField(
            model_name='govdecree',
            name='public_title',
            field=models.CharField(blank=True, help_text="Title, as it's known publicly", max_length=1024, null=True, verbose_name='public title'),
        ),
        migrations.AddField(
            model_name='implementingdecree',
            name='public_title',
            field=models.CharField(blank=True, help_text="Title, as it's known publicly", max_length=1024, null=True, verbose_name='public title'),
        ),
        migrations.AlterField(
            model_name='bill',
            name='initiative',
            field=models.CharField(choices=[('PARL', 'Parliament'), ('GOV', 'Government'), ('REG', 'Region'), ('POP', 'Popular'), ('CNEL', 'CNEL')], help_text='The type of initiative for this bill', max_length=5, null=True),
        ),
        migrations.DeleteModel(
            name='Ministry',
        ),
    ]
