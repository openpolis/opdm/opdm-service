"""Define elections admins."""
from django.contrib import admin
from django.db.models.functions import Replace
from django.utils.translation import gettext_lazy as _
from project.opp.acts.models import (Bill, GovDecree, GovSitting, ImplementingDecree,
                                     ImplementingDecreeOrganizations,
                                     Iter, IterPhase, ActRelationship, GovBill,
                                     BillSigner, BillSpeaker, BillCommission)
from project.opp.votes.models import Voting
from popolo.models import KeyEvent
from django.contrib.admin import SimpleListFilter
from django.db.models import F
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.contrib.contenttypes.admin import GenericStackedInline
from django.utils.html import format_html
from django.db.models import OuterRef, Subquery
from django.contrib.admin import DateFieldListFilter
from django.db.models import  F, Value, Max

def create_html_list(x):
    init = [" <ul> "]
    for item in x.keys():
        res = '{0:,}'.format(x.get(item))
        init.append(
            f' <li> {item}: {res}</li> ')

    init.append(' </ul> ')
    return ' '.join(init)


class VotingInline(admin.TabularInline):

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    def get_queryset(self, request):
        qs = super(VotingInline, self).get_queryset(request)
        qs = qs.select_related('voting')
        return qs.order_by('-voting__sitting__date', '-voting__is_final')

    def is_final(self, obj):
        return obj.voting.is_final

    def is_confidence(self, obj):
        return obj.voting.is_confidence

    def is_key_vote(self, obj):
        return obj.voting.is_key_vote

    def title_compact(self, obj):
        return obj.voting.title_compact

    def sitting_date(self, obj):
        return obj.voting.sitting_date

    model = Voting.bills.through
    fields = ['voting',
              'title_compact',
              'is_confidence',
              'is_key_vote',
              'is_final',
              'sitting_date']
    readonly_fields = ('voting',
                       'title_compact',
                       'is_confidence',
                       'is_key_vote',
                       'is_final',
                       'sitting_date')
    extra = 0


class BillLegislatureFilter(SimpleListFilter):
    title = _('Legislature filter')

    parameter_name = 'Legislature'

    def lookups(self, request, model_admin):
        return KeyEvent.objects.filter(event_type='ITL', start_date__gt='1996-01-01')\
            .values_list('identifier', 'identifier')

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(assembly__key_events__key_event__identifier=self.value())


class CurrentPhaseFilter(SimpleListFilter):
    title = _('Current phase filter')

    parameter_name = 'phase'

    def lookups(self, request, model_admin):
        return IterPhase.objects.values_list('id', 'phase').order_by('phase')

    def queryset(self, request, queryset):
        if self.value():
            top_n_steps_per_bill = Iter.objects.filter(bill=OuterRef('bill')).order_by('-status_date',
                                                                                       '-phase__is_completed')[:1]
            top_n_steps = Iter.objects.filter(phase_id=self.value(),
                                              id__in=Subquery(top_n_steps_per_bill.values('id')))
            bills = top_n_steps.select_related('bill').values('bill')

            return queryset.filter(id__in=bills)


class IterInline(admin.TabularInline):

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    model = Iter
    extra = 0


class SignerInline(admin.TabularInline):

    def parliament_membership(self, obj):

        if obj.bill.initiative == Bill.GOVERNMENT:
            return format_html(
                '<a href="{}">{}</a>',
                f'/admin/popolo/membership/{obj.membership.id}',
                f"{obj.membership.person} @ GOV")

        parl_obj = obj.membership.membership_parliament
        return format_html(
            '<a href="{}">{}</a>',
            parl_obj.get_admin_url(),
            f"{obj.membership.membership_parliament} @ {obj.member_group_at_presenting.group.acronym}"
        )

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    def get_queryset(self, request):
        return super(SignerInline, self).get_queryset(request).order_by(F('adding_date')
                                                                        .desc(nulls_last=False))

    fields = ('parliament_membership', 'first_signer', 'adding_date', 'withdrawal_date')
    readonly_fields = ('parliament_membership', )
    model = BillSigner
    extra = 0


class SpokePersonInline(admin.TabularInline):

    def parliament_membership(self, obj):

        parl_obj = obj.membership.membership_parliament
        return format_html(
            '<a href="{}">{}</a>',
            parl_obj.get_admin_url(),
            f"{obj.membership.membership_parliament} @ {obj.member_group_at_presenting.group.acronym}"
        )

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    fields = ('parliament_membership', 'unit', 'is_minority', 'appointing_date')
    readonly_fields = ('parliament_membership', )
    model = BillSpeaker
    extra = 0


class CommissionsInline(admin.TabularInline):

    def organization(self, obj):

        org_obj = obj.organization
        return format_html(
            '<a href="{}">{}</a>',
            org_obj.get_admin_url(),
            f"{org_obj.name}"
        )

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    fields = ('organization', 'typology', 'appointing_date')
    readonly_fields = ('organization', )
    model = BillCommission
    extra = 0


class OrganizationsImplDecreeInline(admin.TabularInline):

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    model = ImplementingDecreeOrganizations
    extra = 0


class EndingActsInline(GenericStackedInline):

    # def has_change_permission(self, request, obj=None):
    #     return False
    #
    # def has_delete_permission(self, request, obj=None):
    #     return False
    #
    # def has_add_permission(self, request, obj):
    #     return False

    readonly_fields = ('generic_relation_field',
                       'classification_descr')

    def generic_relation_field(self, obj):
        starting_object = obj.starting_object
        url = reverse('admin:%s_%s_change' % (starting_object._meta.app_label, starting_object._meta.model_name),
                      args=[starting_object.id])
        return mark_safe('<a href="%s">%s</a>' % (url, starting_object.__str__()))

    def classification_descr(self, obj):
        classification = obj.classification.descr
        if classification == 'Successivo':
            classification = _("Previous")
        return classification

    generic_relation_field.short_description = _('Previous Act')
    verbose_name = _("Previous Act")
    verbose_name_plural = _("Previous Acts")
    model = ActRelationship
    ct_field = "ending_ct"
    ct_fk_field = "ending_id"
    fields = ('starting_ct',
              'classification_descr',
              'generic_relation_field',)
    extra = 0


class StartingActsInline(GenericStackedInline):

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False

    readonly_fields = ('generic_relation_field',
                       'classification_descr',
                       )

    def generic_relation_field(self, obj):
        starting_object = obj.ending_object
        url = reverse('admin:%s_%s_change' % (starting_object._meta.app_label, starting_object._meta.model_name),
                      args=[starting_object.id])
        return mark_safe('<a href="%s">%s</a>' % (url, starting_object.__str__()))

    def classification_descr(self, obj):
        classification = obj.classification.descr
        return classification

    generic_relation_field.short_description = _('Next Act')
    verbose_name = _("Next Act")
    verbose_name_plural = _("Next Acts")
    model = ActRelationship
    ct_field = "starting_ct"
    ct_fk_field = "starting_id"
    fields = ('ending_ct',
              'classification_descr',
              'generic_relation_field',)

    def ending_id(self, obj):
        # Return a link to the related object
        return format_html(
            '<a href="{}">{}</a>',
            obj.ending_id.get_absolute_url(),
            obj.ending_id
        )
    ending_id.short_description = 'Related Object'
    extra = 0


@admin.register(Bill)
class BillAdmin(admin.ModelAdmin):

    date_hierarchy = 'date_presenting'

    def get_queryset(self, request):
        return super().get_queryset(request).annotate(
            pk2=Replace(F('identifier'), Value('.'), Value('_')),
            last_status_date=Max(F('steps__status_date'))).order_by('-last_status_date')
    # Metodo per restituire il valore di last_status_date
    def get_last_status_date(self, obj):
        return obj.last_status_date
    get_last_status_date.short_description = 'Last Status Date'

    def get_readonly_fields(self, request, obj=None):
        unwanted_num = {'description',
                        'public_title',
                        'is_key_act',
                        'is_omnibus',
                        'is_minotaurus',
                        'is_subject_to_agreements',
                        'is_ratification',
                        'is_final_by_commission',
                        'is_final_by_hands_voting'
                        }
        fields = [f.name for f in self.model._meta.fields]
        return [ele for ele in fields if ele not in unwanted_num] + ['title', ]

    def title(self, obj):
        return obj.title
    list_editable = (
        'is_ratification',
        'is_key_act',
        'is_omnibus',
        'is_minotaurus',
        'is_subject_to_agreements',
        'public_title',
                     )
    list_display = ('title', 'assembly', 'initiative', 'type', 'date_presenting','last_status','get_last_status_date',
                    ) + list_editable
    # ordering = ('-steps__status_date',)
    list_filter = (('assembly__parent', admin.RelatedOnlyFieldListFilter),
                   'is_law',
                   'initiative',
                   'type',
                   BillLegislatureFilter,
                   CurrentPhaseFilter,
                   'is_key_act',
                   'is_omnibus',
                   'is_minotaurus',
                   'is_subject_to_agreements',
                   'is_ratification'
                   )
    search_fields = ('original_title', 'descriptive_title', 'identifier')

    inlines = [
        IterInline,
        StartingActsInline,
        EndingActsInline,
        VotingInline,
        SignerInline,
        SpokePersonInline,
        CommissionsInline
    ]


@admin.register(GovSitting)
class GovSittingAdmin(admin.ModelAdmin):

    date_hierarchy = 'starting_datetime'

    def source_url(self, obj):
        return mark_safe('<a href="%s">%s</a>' % (obj.url, obj.url))

    def get_readonly_fields(self, request, obj=None):
        unwanted_num = {'government', }
        fields = [f.name for f in self.model._meta.fields]
        return [ele for ele in fields if ele not in unwanted_num] + ['source_url', ]

    list_display = ('__str__', 'government', 'starting_datetime', 'number', 'source_url')

    fields = (
        'government',
        'number',
        'source_url',
        'starting_datetime',
        'ending_datetime',
        'description')
    list_filter = (('government', admin.RelatedOnlyFieldListFilter),
                   ('starting_datetime', DateFieldListFilter))


@admin.register(GovDecree)
class GovDecreeAdmin(admin.ModelAdmin):
    class GovernmentFilter(SimpleListFilter):
        title = _('Government filter')

        parameter_name = 'Governo'

        def lookups(self, request, model_admin):
            qs = model_admin.get_queryset(request)
            return [(i, i) for i in qs.values_list('sitting__government__name', flat=True)
                    .distinct().order_by('-sitting__government__start_date')]

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(sitting__government__name=self.value())

    def source_url(self, obj):
        return mark_safe('<a href="%s">%s</a>' % (obj.source_url, obj.source_url))

    def get_readonly_fields(self, request, obj=None):
        unwanted_num = {'description', 'public_title', 'identifier', 'original_title', 'publication_gu_date',
                        'sitting', 'descriptive_title', 'source_url'}
        fields = [f.name for f in self.model._meta.fields]
        return [ele for ele in fields if ele not in unwanted_num] + ['next_act', 'source_url']

    def next_act(self, obj):
        next_act = obj.conversion_act
        if next_act:
            url = reverse('admin:%s_%s_change' % (next_act._meta.app_label, next_act._meta.model_name),
                          args=[next_act.id])
            return mark_safe('<a href="%s">%s</a>' % (url, next_act.identifier))

    list_editable = ('is_key_act',
                     'is_omnibus',
                     'is_minotaurus',
                     'is_subject_to_agreements',
                     'public_title'
                     )
    list_display = ('__str__', 'publication_gu_date', ) + list_editable
    list_filter = ('is_key_act',
                   'is_omnibus',
                   'is_minotaurus',
                   'is_subject_to_agreements',
                   GovernmentFilter
                   )
    fields = (
        'id',
        'identifier',
        'original_title',
        'descriptive_title',
        'description',
        'publication_gu_date',
        'sitting',
        'next_act',
        'source_url'
    )

    inlines = (StartingActsInline, )

    ordering = ('-publication_gu_date', )


@admin.register(GovBill)
class GovBillAdmin(admin.ModelAdmin):

    class GovernmentFilter(SimpleListFilter):
        title = _('Government filter')

        parameter_name = 'Governo'

        def lookups(self, request, model_admin):
            qs = model_admin.get_queryset(request)
            return [(i, i) for i in qs.values_list('decision_sitting__government__name', flat=True)
                    .distinct().order_by('-decision_sitting__government__start_date')]

        def queryset(self, request, queryset):
            if self.value():
                return queryset.filter(decision_sitting__government__name=self.value())

    search_fields = ('original_title', 'descriptive_title', )

    # def get_readonly_fields(self, request, obj=None):
    #     unwanted_num = {'description', 'public_title', }
    #     fields = [f.name for f in self.model._meta.fields]
    #     return [ele for ele in fields if ele not in unwanted_num]

    list_editable = ('is_key_act',
                     'is_omnibus',
                     'is_minotaurus',
                     'is_subject_to_agreements',
                     'public_title'
                     )
    list_display = ('__str__', 'publication_gu_date', 'type') + list_editable
    date_hierarchy = 'bill_date'

    inlines = [
        EndingActsInline,
    ]

    list_filter = ('type',
                   GovernmentFilter
                   )
    ordering = ('-bill_date', '-identifier',)


@admin.register(ImplementingDecree)
class ImplementingDecreeAdmin(admin.ModelAdmin):
    search_fields = ('identifier', 'original_title', 'descriptive_title', )

    def fundings_list(self, obj):
        res = obj.fundings
        return mark_safe(create_html_list(res))

    fundings_list.allow_tags = True

    def get_readonly_fields(self, request, obj=None):
        unwanted_num = {'description', 'public_title', 'government' }
        fields = [f.name for f in self.model._meta.fields]
        return [ele for ele in fields if ele not in unwanted_num] + ['fundings_list', ]

    def get_fields(self, request, obj=None):
        return super(ImplementingDecreeAdmin, self).get_fields(request)

    list_editable = ('is_key_act',
                     'public_title',
                     )

    list_display = ('__str__', 'is_adopted', 'deadline_date', 'is_valid', ) + list_editable
    list_filter = (('government', admin.RelatedOnlyFieldListFilter),
                   'is_adopted', 'deadline_date', 'adoption_date', 'is_valid', )
    readonly_fields = ('fundings_list', )
    raw_id_fields = ('government', )
    inlines = [
        OrganizationsImplDecreeInline,
        EndingActsInline,
    ]
