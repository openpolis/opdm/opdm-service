"""Define elections serializers."""
import datetime

from popolo.models import Organization, OrganizationRelationship
from rest_framework import serializers
from rest_framework.fields import ListField
from rest_framework.relations import HyperlinkedIdentityField

from bs4 import BeautifulSoup

from project.opp.acts.models import Bill, GovDecree, GovBill, ImplementingDecree, Iter, BillSigner, BillSpeaker, \
    BillCommission
from project.opp.serializers import BaseSerializerOPP
from project.opp.utils import get_identifier_dlgs, get_type_act
from project.opp.votes.models import Group


class PartySerializer(serializers.ModelSerializer):
    class Meta:
        model = Organization
        ref_name = "Party"
        fields = ('id', 'name', 'image')


class GroupSerializer(serializers.ModelSerializer):
    name = serializers.CharField(source="name_compact")
    branch = serializers.SerializerMethodField()
    acronym = serializers.CharField(source='acronym_last')
    def get_branch(self, obj):

        return obj.branch.lower()

    class Meta:
        model = Group
        ref_name = "Group"
        fields = ("id", "acronym", "name", "color", "slug", "branch")


class LegislatureHyperlinkedIdentityField(HyperlinkedIdentityField):

    def get_url(self, obj, view_name, request, format):
        """
        Given an object, return the URL that hyperlinks to the object,
        considering the legislature
        """
        # Unsaved objects will not yet have a valid URL.
        if hasattr(obj, 'pk') and obj.pk is None:
            return None
        if hasattr(obj, 'pk2') and obj.pk2 is not None:
            self.lookup_field = 'pk2'

        lookup_value = getattr(obj, self.lookup_field)
        kwargs = {
            'legislature': request.parser_context['kwargs']['legislature'],
            self.lookup_field: lookup_value,
        }
        return self.reverse(view_name, kwargs=kwargs, request=request, format=format)


class SignerSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='membership.person.id')
    slug = serializers.CharField(source='membership.person.slug')
    name = serializers.CharField(source='membership.person.name')
    image = serializers.CharField(source='membership.person.image')
    group = GroupSerializer(source='member_group_at_presenting')
    party = PartySerializer(source='member_party_at_presenting', )

    class Meta:
        model = BillSigner
        fields = ('id', 'slug', 'name', 'image', 'group', 'party')


# class CommissionBillSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = BillCommission
#         fields = ('id', 'slug', 'name', 'image', 'group', 'party')


class RelatorSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(source='membership.person.id')
    slug = serializers.CharField(source='membership.person.slug')
    name = serializers.CharField(source='membership.person.name')
    image = serializers.CharField(source='membership.person.image')
    unit = serializers.CharField(source='ass_commission')
    group = GroupSerializer(source='member_group_at_presenting.group')
    # commission = serializers.CharField()

    class Meta:
        model = BillSpeaker
        fields = ('id', 'slug', 'name', 'image', 'unit', 'group')


class ActIterSerializer(serializers.ModelSerializer):
    date = serializers.DateField(source='status_date')
    label = serializers.CharField(source='phase')

    class Meta:
        model = Iter
        fields = ('date', 'label')


class ImplementingDecreeListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='implementingdecree-detail',
    )
    act_adopted = serializers.URLField(source='url')
    # title = serializers.CharField(source='__str__')
    gov = serializers.CharField(source='government_name')
    mins_proposer = serializers.ListField(source='get_ministries_prop')
    reference_law = serializers.CharField(source='identifier')
    type_act_to_adopt = serializers.CharField(source='type_act')
    status = serializers.SerializerMethodField()
    resources = serializers.DictField(source='get_resources')

    def get_status(self, obj):
        today = datetime.datetime.now()
        # today_strf = today.strftime('%Y-%m-%d')

        if obj.deadline_date:
            if obj.is_adopted:
                if obj.adoption_date > obj.deadline_date:
                    return 'Adottato in ritardo'
                else:
                    return 'Adottato'
            else:
                if today.date() > obj.deadline_date:
                    return 'Scaduto'
                else:
                    return 'Non adottato'
        else:
            if obj.is_adopted:
                return 'Adottato'
            else:
                return 'Non adottato'

    class Meta:
        """Define serializer Meta."""

        model = ImplementingDecree
        ref_name = "Implementing Decree"
        fields = (
            'id',
            'url',
            'title',
            'gov',
            'identifier',
            'mins_proposer',
            'deadline_date',
            'type_act_to_adopt',
            'is_adopted',
            'reference_law',
            'act_adopted',
            'status',
            'resources',
            'policy'
        )


class BillListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    # url = LegislatureHyperlinkedIdentityField(
    #     view_name='bill-detail',
    # )
    slug = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    source_url = serializers.URLField()
    first_signers = ListField(child=SignerSerializer(), source='first_signers_instances')
    status = serializers.DictField(source='last_status_dict')
    branch = serializers.CharField(source='branch_detail')
    type = serializers.CharField(source='get_type_display')
    date_presenting = serializers.DateField(source='get_date_presenting_tree')

    def get_title(self, obj):
        if obj.get_codes:
            return f"{obj.title} ({', '.join(obj.get_codes)})"
        return obj.title

    def get_slug(self, obj):
        return obj.identifier.replace('.', '_').replace('/', '-')

    class Meta:
        """Define serializer Meta."""

        model = Bill
        ref_name = "Bill"
        fields = (
            'id',
            'slug',
            # 'url',
            'title',
            'source_url',
            'identifier',
            'date_presenting',
            'first_signers',
            'type',
            'status',
            'branch'
        )


class BillListSerializer2(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    # url = LegislatureHyperlinkedIdentityField(
    #     view_name='bill-detail',
    # )
    slug = serializers.SerializerMethodField()
    title = serializers.SerializerMethodField()
    source_url = serializers.URLField()
    first_signers = ListField(child=SignerSerializer(), source='first_signers_instances')
    status = serializers.DictField(source='last_status_dict')
    branch = serializers.CharField(source='branch_detail')
    type = serializers.CharField(source='get_type_display')
    date_presenting = serializers.DateField(source='get_date_presenting_tree')

    def get_title(self, obj):
        if obj.get_codes:
            return f"{obj.title}"
        return obj.title

    def get_slug(self, obj):
        return obj.identifier.replace('.', '_').replace('/', '-')

    class Meta:
        """Define serializer Meta."""

        model = Bill
        ref_name = "Bill"
        fields = (
            'id',
            'slug',
            # 'url',
            'title',
            'source_url',
            'identifier',
            'date_presenting',
            'first_signers',
            'type',
            'status',
            'branch'
        )


class ActSerializer(serializers.ModelSerializer):
    slug = serializers.CharField()
    type = serializers.SerializerMethodField()
    identifier = serializers.SerializerMethodField()

    def get_identifier(self, obj):
        leg = self.context['request'].parser_context['kwargs']['legislature']
        return get_identifier_dlgs(obj, leg)

    def get_type(self, obj):
        leg = self.context['request'].parser_context['kwargs']['legislature']
        return get_type_act(obj, leg)

    class Meta:
        model = Bill
        ref_name = "Bill"
        fields = ("id", "identifier", 'slug', 'type')


class SupportingPartySerializer(serializers.ModelSerializer):
    name = serializers.CharField(source='source_organization.name')
    image = serializers.CharField(source='source_organization.image')
    class Meta:
        """Define serializer Meta."""
        model = OrganizationRelationship
        ref_name = "Partito sostenitore"
        fields = ('name', 'image', 'end_date')


class GovDecreeListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='govdecree-detail',
    )
    # title = serializers.CharField(source='__str__')
    slug = serializers.CharField(source='identifier')
    source_url = serializers.URLField()
    date_presenting = serializers.DateField()
    conversion_act = ActSerializer()
    status = serializers.CharField()
    status_desc = serializers.CharField()
    status_date = serializers.DateField()

    class Meta:
        """Define serializer Meta."""

        model = GovDecree
        ref_name = "Gov Decree"
        fields = (
            'id',
            'slug',
            'url',
            'title',
            'source_url',
            'identifier',
            'date_presenting',
            'status',
            'status_desc',
            'conversion_act',
            'status_date'
        )


class GovDecreeDetailSerializer(BaseSerializerOPP, GovDecreeListSerializer):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='govdecree-detail',
    )
    title = serializers.CharField()
    source_url = serializers.URLField()
    council_ministers_approval_date = serializers.DateField(source='date_presenting')
    days_from_approval = serializers.IntegerField()
    GU_publishing_date = serializers.DateField(source='publication_gu_date')
    gov = serializers.CharField(source='sitting.gov_name')

    days_to_expiry = serializers.IntegerField()
    parliament_iter = serializers.DictField()
    implementing = serializers.DictField()

    class Meta:
        """Define serializer Meta."""

        model = GovDecree
        ref_name = "GovDecree"
        fields = GovDecreeListSerializer.Meta.fields + (
            'codelists',
            'council_ministers_approval_date',
            'days_from_approval',
            'days_to_expiry',
            'GU_publishing_date',
            'is_key_act',
            'is_omnibus',
            'is_minotaurus',
            'is_subject_to_agreements',
            'parliament_iter',
            'gov',
            'implementing')


class GovBillListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='govbill-detail',
    )
    # title = serializers.CharField(source='__str__')
    delegation_law = ListField(child=ActSerializer())

    class Meta:
        """Define serializer Meta."""

        model = GovBill
        ref_name = "Gov Bill"
        fields = (
            'id',
            'url',
            'title',
            'bill_date',
            'identifier',
            'delegation_law',

        )


class GovBillDetailSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='govbill-detail',
    )
    type = serializers.CharField(source="get_type_display")

    announcing_date = serializers.SerializerMethodField()
    adopting_date = serializers.SerializerMethodField()
    delegation_law = ListField(child=ActSerializer())
    descriptive_title = serializers.SerializerMethodField()
    act_document = serializers.SerializerMethodField()
    # implementing_decrees = ImplementingDecreeListSerializer(many=True)
    implementing = serializers.DictField()

    def get_descriptive_title(self, obj):
        return BeautifulSoup(obj.descriptive_title, 'html.parser').text.strip()

    def get_act_document(self, obj):
        return f'https://www.normattiva.it/uri-res/N2Ls?urn:nir:stato:decreto.legislativo:{obj.identifier.replace("-", ";")}'

    def get_announcing_date(self, obj):
        return obj.preliminary_sitting.ending_datetime.date() if obj.preliminary_sitting else None

    def get_adopting_date(self, obj):
        return obj.decision_sitting.ending_datetime.date()

    class Meta:
        """Define serializer Meta."""

        model = GovBill
        ref_name = "GovDecree"
        fields = (
            'id',
            'url',
            'title',
            'announcing_date',
            'adopting_date',
            'is_key_act',
            'descriptive_title',
            'type',
            'is_minotaurus',
            'is_omnibus',
            'is_subject_to_agreements',
            'identifier',
            'bill_date',
            'publication_gu_date',
            'delegation_law',
            'act_document',
            # 'implemented_decrees',
            'implementing',
        )


class GroupListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""

    #
    # url = LegislatureHyperlinkedIdentityField(
    #     view_name='govbill-detail',
    # )
    # title = serializers.CharField(source='__str__')
    # delegation_law = ListField(child=ActSerializer())

    class Meta:
        """Define serializer Meta."""

        model = Group
        ref_name = "Gruppo"
        fields = (
            'id',

        )


class GroupDetailSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    date_start = serializers.DateField(source='organization.start_date')
    date_end = serializers.DateField(source='organization.end_date')
    supporting_parties = ListField(child=SupportingPartySerializer())
    ex_supporting_parties = ListField(child=SupportingPartySerializer())
    members = serializers.DictField(source='members_stats')
    vote_behaviour = serializers.DictField()
    cohesion = serializers.DictField()
    discordant_votes = serializers.DictField()
    key_votes = serializers.DictField()
    group_components_power_roles = serializers.DictField()
    pp = serializers.DictField()
    name = serializers.SerializerMethodField()
    acronym = serializers.CharField(source='acronym_last')
    def get_name(self, obj):
        return obj.name.split('"')[1]
    class Meta:
        """Define serializer Meta."""
        model = Group
        ref_name = "Gruppo"
        fields = (
            'id',
            'date_start',
            'date_end',
            'supports_majority',
            'acronym',
            'name',
            'color',
            'supporting_parties',
            'ex_supporting_parties',
            'members',
            'vote_behaviour',
            'cohesion',
            'discordant_votes',
            'key_votes',
            'group_components_power_roles',
            'pp'
        )


class ImplementingDecreeDetailSerializer(BaseSerializerOPP, serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='implementingdecree-detail',
    )
    title = serializers.CharField(source='__str__')
    gov = serializers.CharField(source='government_name')
    mins_proposer = serializers.ListField(source='get_ministries_prop')
    reference_law = ActSerializer(source='reference_law.starting_object')
    type_act_to_adopt = serializers.CharField(source='type_act')
    jointly_org = serializers.ListField()
    nature = serializers.CharField(source='get_type_display')
    status = serializers.SerializerMethodField()
    resources = serializers.DictField(source='get_resources')
    act_adopted = serializers.URLField(source='url')

    def get_status(self, obj):
        today = datetime.datetime.now()
        # today_strf = today.strftime('%Y-%m-%d')

        if obj.deadline_date:
            if obj.is_adopted:
                if obj.adoption_date > obj.deadline_date:
                    return 'Adottato in ritardo'
                else:
                    return 'Adottato'
            else:
                if today.date() > obj.deadline_date:
                    return 'Scaduto'
                else:
                    return 'Non adottato'
        else:
            if obj.is_adopted:
                return 'Adottato'
            else:
                return 'Non adottato'

    class Meta:
        """Define serializer Meta."""

        model = ImplementingDecree
        ref_name = "Implementing Decree"
        fields = (
            'codelists',
            'id',
            'url',
            'title',
            'gov',
            'identifier',
            'mins_proposer',
            'deadline_date',
            'type_act_to_adopt',
            'is_adopted',
            'reference_law',
            'adoption_date',
            'is_key_act',
            'jointly_org',
            'policy',
            'publication_gu_date',
            'theme',
            'type_act',
            'nature',
            'status',
            # 'fundings',
            'resources',
            'act_adopted'

        )


class BillDetailSerializer(BaseSerializerOPP, serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""

    url = LegislatureHyperlinkedIdentityField(
        view_name='bill-detail',
    )
    act_iter = ListField(child=ActIterSerializer())
    slug = serializers.SerializerMethodField()
    original_title = serializers.SerializerMethodField()
    source_url = serializers.URLField()
    branch = serializers.CharField()
    first_signers = ListField(child=SignerSerializer(), source='first_signers_instances')
    co_signers = ListField(child=SignerSerializer())
    relators = RelatorSerializer(many=True)
    voting_stats = serializers.DictField()
    parliament_iter = serializers.DictField()
    implementing = serializers.DictField()
    initiative = serializers.CharField(source='get_initiative_display')
    type = serializers.CharField(source='get_type_display')
    date_presenting = serializers.DateField()
    commissions = serializers.ListField(source='commissions_list')

    def get_original_title(self, obj):
        return obj.title

    def get_slug(self, obj):
        return obj.identifier.replace('.', '_').replace('/', '-')

    class Meta:
        """Define serializer Meta."""

        model = Bill
        ref_name = "Bill"
        fields = (
            'codelists',
            'id',
            'slug',
            'url',
            'date_presenting',
            'original_title',
            'identifier',
            'source_url',
            'is_key_act',
            'initiative',
            'type',
            'is_minotaurus',
            'is_omnibus',
            'is_subject_to_agreements',
            'is_ratification',
            'branch',
            'act_iter',
            'first_signers',
            'co_signers',
            'relators',
            'voting_stats',
            # 'implementing_decrees',
            'implementing',
            'parliament_iter',
            "commissions"

        )


class BillSignerListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    # url = LegislatureHyperlinkedIdentityField(
    #     view_name='billsigner-detail',
    # )
    title = serializers.SerializerMethodField()
    identifier = serializers.CharField(source='bill.identifier')
    slug = serializers.SerializerMethodField()
    date_presenting = serializers.DateField(source='bill.date_presenting')

    type = serializers.CharField(source='bill.get_type_display')
    status = serializers.DictField(source='bill.last_main_status')
    branch = serializers.CharField(source='bill.branch_detail')

    def get_title(self, obj):
        return obj.bill.title

    def get_slug(self, obj):
        return obj.bill.identifier.replace('.', '_').replace('/', '-')

    class Meta:
        """Define serializer Meta."""

        model = BillSigner
        ref_name = "Bill Signer"
        fields = (
            'id',
            # 'url',
            'title',
            'identifier',
            'slug',
            'date_presenting',
            'type',
            'status',
            'branch'

        )


class BillSignerDetailSerializer(BaseSerializerOPP, BillSignerListSerializer):
    """A serializer for the Voting model when seen in lists."""

    # url = LegislatureHyperlinkedIdentityField(
    #     view_name='billsigner-detail',
    # )

    class Meta:
        """Define serializer Meta."""

        model = BillSigner
        ref_name = "Bill Signer"
        fields = (
            'id',
            # 'url',
            'title',
            'identifier',
            'slug',
            'date_presenting',
            'type',
            'status',
            'branch'

        )
