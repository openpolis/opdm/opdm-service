from rest_framework import serializers
from rest_framework.relations import HyperlinkedIdentityField

from project.opp.utils import TodayDate
from .models import PersonsOPP
from ..serializers import BaseSerializerOPP
from django.core.cache import cache
import datetime




class LegislatureHyperlinkedIdentityField(HyperlinkedIdentityField):

    def get_url(self, obj, view_name, request, format):
        """
        Given an object, return the URL that hyperlinks to the object,
        considering the legislature
        """
        # Unsaved objects will not yet have a valid URL.
        if hasattr(obj, 'pk') and obj.pk is None:
            return None
        if hasattr(obj, 'pk2') and obj.pk2 is not None:
            self.lookup_field = 'pk2'

        lookup_value = getattr(obj, self.lookup_field)
        kwargs = {
            'legislature': request.parser_context['kwargs']['legislature'] if request else self.context['leg'],
            self.lookup_field: lookup_value,
        }
        return self.reverse(view_name, kwargs=kwargs, request=request, format=format)


class PersonsListSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField()
    url = LegislatureHyperlinkedIdentityField(
        view_name='person-detail',
    )
    roles = serializers.SerializerMethodField()
    is_active = serializers.SerializerMethodField()

    def get_is_active(self, obj):

        gov = obj.gov_member(
            leg=self.context['request'].parser_context['kwargs']['legislature'])
        parl = obj.parl_member(
            leg=self.context['request'].parser_context['kwargs']['legislature'])
        res = []
        if gov:
            res = [not x['end_date'] for x in gov['roles']]
        if parl:
            res = res + [not parl['end_date']]
        return any(res)

    def get_roles(self, obj):
        is_active = self.get_is_active(obj)
        res = []
        gov = obj.gov_member(
            leg=self.context['request'].parser_context['kwargs']['legislature'])
        is_active_gov = False
        if gov:
            is_active_gov = any([x['end_date'] is None for x in gov['roles']])
        parl = obj.parl_member(
            leg=self.context['request'].parser_context['kwargs']['legislature'])

        if gov and (is_active_gov or (not is_active_gov and not is_active)):
            for i in gov.get('roles'):
                res.append(i['role'])
        if parl:
            res.append(parl['role'])
        return res

    class Meta:
        """Define serializer Meta."""

        model = PersonsOPP

        fields = (
            'id',
            'url',
            'name',
            'image',
            'slug',
            'roles',
            'is_active'
        )


class PPIndexListSerializer(serializers.HyperlinkedModelSerializer):
    name = serializers.CharField()
    url = LegislatureHyperlinkedIdentityField(
        view_name='person-detail',
    )
    pp = serializers.SerializerMethodField()
    current_roles = serializers.SerializerMethodField()


    def get_pp(self, obj):
        if not cache.get(f'pp_index:{obj.id}'):
            res = obj.pp(
                leg=self.context['request'].parser_context['kwargs']['legislature'])
            cache.set(f'pp_index:{obj.id}',
                      {'res': res},
                      timeout=None)
        return cache.get(f'pp_index:{obj.id}').get('res')


    def get_current_roles(self, obj):

        if not cache.get(f'pp_index_get_current_roles:{obj.id}'):
            gov = obj.gov_member(
                leg=self.context['request'].parser_context['kwargs']['legislature'])
            if gov:
                gov['roles'] = [x for x in gov['roles'] if x['end_date'] is None  ]
                if not gov['roles']:
                    gov=None
            parl = obj.parl_member(
                leg=self.context['request'].parser_context['kwargs']['legislature'])

            date = TodayDate().today_formatted
            if gov:
                for i in gov.get('roles'):
                    date = min(date, i.get('start_date'))
            if parl:
                date = min(date, parl.get('start_date'))

            res = {
                'date': date,
                'gov': gov,
                'parl': parl
            }

            cache.set(f'pp_index_get_current_roles:{obj.id}',
                      {'res': res},
                      timeout=None)
            return res

        return cache.get(f'pp_index_get_current_roles:{obj.id}').get('res')

    class Meta:
        """Define serializer Meta."""

        model = PersonsOPP

        fields = (
            'id',
            'url',
            'name',
            'image',
            'slug',
            'pp',
            'current_roles'
        )


class PersonsDetailSerializer(BaseSerializerOPP):

    birth_place = serializers.CharField(source='birth_location')

    n_absent = serializers.SerializerMethodField()
    n_mission = serializers.SerializerMethodField()
    n_present = serializers.SerializerMethodField()
    n_voting = serializers.SerializerMethodField()
    n_rebels = serializers.SerializerMethodField()
    is_president = serializers.SerializerMethodField()
    parse_days_in_parliament = serializers.SerializerMethodField()
    days_in_parliament = serializers.SerializerMethodField()
    current_roles = serializers.SerializerMethodField()
    pp = serializers.SerializerMethodField()
    fidelity = serializers.SerializerMethodField()
    vote_behaviours = serializers.SerializerMethodField()
    first_signer_bills = serializers.SerializerMethodField()
    parliamentary_positions_current = serializers.SerializerMethodField()
    parliamentary_positions_prev = serializers.SerializerMethodField()
    carreer_positions = serializers.SerializerMethodField()

    def get_is_president(self, obj):

        today = datetime.datetime.now()
        today_strf = today.strftime('%Y-%m-%d')
        return obj.is_president(today_strf)

    def get_n_rebels(self, obj):
        return obj.get_n_rebels_by_leg(leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])

    def get_n_absent(self, obj):
        return obj.get_n_absent_by_leg(leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])


    def get_n_mission(self, obj):
        return obj.get_n_mission_by_leg(leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])


    def get_n_present(self, obj):
        return obj.get_n_present_by_leg(leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])


    def get_n_voting(self, obj):
        return obj.get_n_voting_by_leg(leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])


    def get_parse_days_in_parliament(self, obj):
        return obj.get_parse_days_in_parliament_by_leg(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])


    def get_days_in_parliament(self, obj):
        return obj.get_days_in_parliament_by_leg(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])


    def get_has_changed_group(self, obj):
        return obj.has_changed_group(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])


    def get_current_roles(self, obj):
        gov = obj.gov_member(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])

        parl = obj.parl_member(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])


        date = TodayDate().today_formatted
        max_end_dates = []
        if gov:
            for i in gov.get('roles'):
                date = min(date, i.get('start_date'))
                max_end_dates.append(i.get('end_date'))
        if parl:
            date = min(date, parl.get('start_date'))
            max_end_dates.append(parl.get('end_date'))

        if not max_end_dates or None in max_end_dates:
            end_date = None
        else:
            end_date = max(max_end_dates)

        res = {
            'date': date,
            'end_date': end_date,
            'gov': gov,
            'parl': parl
        }
        return res

    def get_pp(self, obj):
        # end_date=None
        # gov = obj.gov_member(
        #     leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])
        #
        # parl = obj.parl_member(
        #     leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])
        #
        #
        # date = TodayDate().today_formatted
        # max_end_dates = []
        # if gov:
        #     for i in gov.get('roles'):
        #         date = min(date, i.get('start_date'))
        #         max_end_dates.append(i.get('end_date'))
        # if parl:
        #     date = min(date, parl.get('start_date'))
        #     max_end_dates.append(parl.get('end_date'))
        #
        # if not max_end_dates or None in max_end_dates:
        #     end_date = None
        # else:
        #     end_date = max(max_end_dates)
        #
        # res = obj.get_pp_indeces(
        #     leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'],
        # end_date=end_date)
        #
        # return res
        res = obj.get_pp_indeces(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])

        return res

    def get_fidelity(self, obj):
        res = obj.get_fidelity(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])

        return res

    def get_vote_behaviours(self, obj):
        res = obj.get_vote_behaviours(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])
        return res


    def get_first_signer_bills(self, obj):
        res = obj.get_first_signer_bills(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])

        return res

    def get_parliamentary_positions_current(self, obj):
        res = obj.get_parliamentary_positions_current(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])

        return res

    def get_parliamentary_positions_prev(self, obj):
        res = obj.get_parliamentary_positions_prev(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg'])

        return res

    def get_carreer_positions(self, obj):
        from ..votes.serializers import CareerPositionDetailSerializer
        res = obj.get_carreer_positions(
            leg=self.context['request'].parser_context['kwargs']['legislature'] if self.context['request'] else self.context['leg']).order_by('-start_date')
        return {
            "count": len(res),
            "results": [CareerPositionDetailSerializer(x).data for x in res[:5]]}

    class Meta:
        """Define serializer Meta."""

        model = PersonsOPP

        fields = (
            'codelists',
            'id',
            'is_president',
            'slug',
            'family_name',
            'given_name',
            'image',
            'birth_date',
            'birth_place',
            'n_rebels',
            'n_absent',
            'n_voting',
            'n_present',
            'n_mission',
            'parse_days_in_parliament',
            'current_roles',
            'gender',
            'days_in_parliament',
            'pp',
            'fidelity',
            'vote_behaviours',
            'first_signer_bills',
            'parliamentary_positions_current',
            'parliamentary_positions_prev',
            'carreer_positions'

        )
