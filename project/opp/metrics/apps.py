from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MetricsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'project.opp.metrics'
    verbose_name = _("Openparlamento metrics")
