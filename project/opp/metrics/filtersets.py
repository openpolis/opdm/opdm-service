from django.utils.translation import ugettext as _
from django_filters import ChoiceFilter
from django_filters.rest_framework import (
    FilterSet)
from opp.votes.filtersets import Membership
from project.opp.gov.models import Member
from project.opp.metrics.models import PersonsOPP

ROLE_TYPES = (
    ('Senatore', _('Senator')),
    ('Deputato', _('Deputy')),
    ('Nessuno', _('No role'))
)


class PersonsFilterSet(FilterSet):

    def __init__(self, *args, **kwargs):
        """Override init to build custom choices for branches and Election areas, dependent on the request."""
        super(FilterSet, self).__init__(*args, **kwargs)

    is_gov = ChoiceFilter(
        method="is_gov_filter",
        label=_("Is Government member"),
        choices=[(True, 'Yes'), (False, 'No')],
        empty_label=_("All"),
        help_text=_("Filter bills based on whether they are key acts"),
    )

    @staticmethod
    def is_gov_filter(queryset, name, value):

        if value == 'True':
            return queryset.filter(id__in=Member.objects.filter(end_date__isnull=True).values('person_id'))
        elif value == 'False':
            return queryset.exclude(id__in=Member.objects.filter(end_date__isnull=True).values('person_id'))
        else:
            return queryset.all()

    role = ChoiceFilter(
        method="role_filter",
        label=_("Parliament role"),
        empty_label=_("All roles"),
        choices=ROLE_TYPES,
        help_text=_("Filter memberships based on the role (Senator, Deputy, ...)"),
    )

    @staticmethod
    def role_filter(queryset, name, value):
        if value in ['Senatore', 'Deputato']:
            res = Membership.objects.filter(opdm_membership__end_date__isnull=True,
                                            opdm_membership__role__icontains=value)
            return queryset.filter(id__in=res.values('opdm_membership__person_id'))
        elif value == 'Nessuno':
            res = Membership.objects.filter(opdm_membership__end_date__isnull=True)
            return queryset.exclude(id__in=res.values('opdm_membership__person_id'))
        else:
            return queryset.all()

    class Meta:
        model = PersonsOPP
        fields = []
