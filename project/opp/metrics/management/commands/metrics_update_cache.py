from project.opp.metrics.models import PositionalPower
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = 'Create materialized view if it does not exist'

    def handle(self, *args, **kwargs):
        PositionalPower.update_cache_gov_abs_metric()
