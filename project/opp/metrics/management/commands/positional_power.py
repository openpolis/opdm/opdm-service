# coding=utf-8
# import json
from django.core.management.base import BaseCommand
import logging
import time
from popolo.models import Organization, Membership as OPDMMembership
from project.opp.metrics.models import PositionalPower
import datetime
from django.db.models import F, Q, Value, DateField
from django.db.models.functions import Cast, Coalesce

import copy
from django.db.models import Subquery, OuterRef
today = datetime.datetime.now()



def calc_count(res, org_parl_filtered, filters, date):
    for category in res:
        if category['name'] == 'PortafoglioC':
            print('a')
        children = category['children']
        org_parl_filtered_copy = copy.deepcopy(org_parl_filtered)
        sub_children = filters.get(category['name'], None)
        sub_filters = None
        if sub_children:
            count_items = org_parl_filtered_copy.filter(sub_children.get('filter')) \
                .values('id').distinct().count()
            category['objects'] = []
            sub_filters = sub_children.get('sub_filters', None)
            for item in org_parl_filtered_copy.filter(sub_children.get('filter')).values('id').distinct():
                obj = Organization.objects.get(id=item.get('id'))
                category['objects'].append({'name': obj.name})
                if sub_filters:
                    category['objects'][-1]['roles'] = {}
                    for key in sub_filters:
                        sub_filter = sub_filters.get(key).get('filter')
                        roles = org_parl_filtered_copy.filter(sub_children.get('filter'), id=item.get('id')) \
                            .filter(sub_filter).values('memberships__id').distinct()
                        value = float([x['value'] for x in children if x['name'] == key][0])
                        category['objects'][-1]['roles'][key] = {
                            'count': roles.count(),
                            'value': value,
                            'ids': list(roles.values_list('memberships__id', flat=True))}
            org_parl_filtered_copy = org_parl_filtered_copy.filter(
                Q(parent__in=org_parl_filtered_copy.filter(sub_children.get('filter')).values('id').distinct())
                | Q(id__in=org_parl_filtered_copy.filter(sub_children.get('filter')).values('id').distinct())
            )
        else:
            count_items = 1
        if category['name'] == 'Senato' or category['name'] == 'Camera':
            del category['objects']
        elif 'objects' in category.keys():
            del category['children']
            category['children'] = category.pop('objects')
        category['count'] = count_items
        calc_count(children, org_parl_filtered_copy,
                   (sub_filters or filters), date)


def calculate_weights(res, parent_weight=1, **k):
    weight_blocked = k.get('weight_blocked') == 'True'
    for item in res:
        if item.get('children'):
            if weight_blocked:
                denom = sum([float(x['value']) for x in res])
                next_value = (parent_weight * float(item.get('value'))) / denom
            else:
                denom = sum([float(x['value'])*x['count'] for x in res])
                next_value = (parent_weight * float(item.get('value'))*item.get('count', 1)) / denom
            calculate_weights(item.get('children'), next_value,
                              weight_blocked=item.get('weight_blocked'))
        else:
            if 'Gruppo parlamentare' in item['name']:
                denom = sum([x.get('roles').get('Membro').get('count') for x in res])
                item['level_value'] = parent_weight*item.get('roles').get('Membro').get('count')/denom
            else:
                item['level_value'] = parent_weight / len(res)
            if 'roles' in item.keys():
                denom = sum([float(x.get('count'))*float(x.get('value')) for x in item['roles'].values()])
                for key, values in item['roles'].items():
                    item['roles'][key]['level_value'] = (item['level_value']*values.get('value'))/denom


def create_records(res, date):
    PositionalPower.objects.filter(end_date__isnull=True)\
        .update(end_date=Cast(Subquery(PositionalPower.objects.filter(pk=OuterRef('pk'))
                                       .values('opdm_membership__end_date')[:1]), DateField()))
    pos_pows = PositionalPower.objects \
        .annotate(new_end_date=Coalesce(F('end_date'), Cast(Value((today+datetime.timedelta(1))
                                                                  .strftime('%Y-%m-%d')),
                                                            DateField()))) \
        .filter(start_date__lte=date, new_end_date__gt=date)
    for item in res:
        if item.get('children'):
            create_records(item.get('children'), date)
        else:
            roles = item.get('roles')
            if roles:
                for key, values in roles.items():
                    print(key)
                    print(values)
                    description = key
                    value = values.get('level_value')
                    ids = values.get('ids')
                    for id in ids:
                        if not id:
                            continue
                        if OPDMMembership.objects.get(id=id).end_date == date:
                            continue
                        pos_pow, created = pos_pows.get_or_create(
                            opdm_membership=OPDMMembership.objects.get(id=id),
                            description=description,
                            defaults={'start_date': date,
                                      'value': round(value*10_000, 2)})
                        if not created and pos_pow.value != round(value*10_000, 2):
                            pos_pow.end_date = date
                            pos_pow.save()
                            PositionalPower.objects.create(
                                opdm_membership=OPDMMembership.objects.get(id=id),
                                description=description,
                                value=round(value*10_000, 2),
                                start_date=date
                            )


class Command(BaseCommand):
    """
    """
    help = ''
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            '--date',
            dest="date",
            help='Start date')

        parser.add_argument(
            '--res',
            dest="res",
        )

        parser.add_argument(
            '--filters',
            dest="filters",)

    def handle(self, *args, **options):
        date = options["date"]
        res = copy.deepcopy(options["res"])
        filters = options["filters"]

        verbosity = options['verbosity']
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)


        org_filtered = Organization.objects.annotate(
                start_date_c=Coalesce(F('start_date'), Value('1900-01-01')),
                end_date_c=Coalesce(F('end_date'), Value(date))
            ) \
            .filter(start_date_c__lte=date, end_date_c__gte=date)

        org_parl_filtered = org_filtered.prefetch_related('memberships')\
            .annotate(
            role=F('memberships__role'),
            start_date_m=Coalesce(F('memberships__start_date'), Value('1900-01-01')),
            end_date_m=Coalesce(F('memberships__end_date'), Value(date))).filter(start_date_m__lte=date,
                                                                                 end_date_m__gte=date)

        self.logger.info(f"----- Starting calc_count -----")
        start_time = time.time()
        calc_count([res], org_parl_filtered, filters, date)
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

        self.logger.info(f"----- Starting calculate_weights -----")
        start_time = time.time()
        calculate_weights([res])
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

        self.logger.info(f"----- Starting create_records -----")
        start_time = time.time()
        create_records([res], date)
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")
        self.logger.info("Finished!")
