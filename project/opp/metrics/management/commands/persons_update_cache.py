import base64
import json

import requests

import environ
from project.opp.metrics.models import PersonsOPP
from project.opp.metrics.serializers import PersonsDetailSerializer
import datetime
from taskmanager.management.base import LoggingBaseCommand
from django.core.cache import cache

env = environ.Env()

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')


class Command(LoggingBaseCommand):
    help = 'Create materialized view if it does not exist'

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            "--leg",
            type=int,
            choices=[18, 19],
            default=19,
            dest='legislatura',
            help="Legislature number"
        )

    def handle(self, *args, **kwargs):
        super().handle(*args, **kwargs)
        leg = kwargs['legislatura']
        persons = PersonsOPP.objects \
            .all_and_removed()
        for instance in persons:
            cache_key = f'person_opp_:{instance.id}'
            serializer = PersonsDetailSerializer(instance)
            serializer.context['leg'] = leg
            serializer.context['request'] = None
            cache.set(cache_key, serializer.data, timeout=None)
            data = {
                'urlsToDelete': [f"persone/{instance.slug}"],
                'token': env('CACHE_INVALIDATE_TOKEN')
            }
            username = env('USERNAME_PROD_OPP')
            password = env('PWD_PROD_OPP')
            # Codifica le credenziali in base64
            credentials = base64.b64encode(f'{username}:{password}'.encode('utf-8')).decode('utf-8')
            requests.post(f"https://staging.new.openparlamento.it/api/delete-cache",
                          headers={'Content-Type': 'application/json'},
                          data=json.dumps(data))
            requests.get(f"https://staging.new.openparlamento.it/persone/{instance.slug}")
            requests.post(f"https://parlamento19.openpolis.it/api/delete-cache",
                          headers={'Content-Type': 'application/json',
                                   'Authorization': f'Basic {credentials}'},
                          data=json.dumps(data))
            requests.get(f"https://parlamento19.openpolis.it/persone/{instance.slug}")
            self.logger.debug(f"{instance} updated ({cache_key})")
