from taskmanager.management.base import LoggingBaseCommand
from django.core.management import call_command
import time
import logging
import datetime
import environ
import pandas as pd
from neo4j import GraphDatabase
import operator
from django.db.models import F, Q, Value, DateField
from functools import reduce
from popolo.models import Organization, Membership as OPDMMembership, KeyEvent

today = datetime.datetime.now()
today_strf = today.strftime('%Y-%m-%d')
env = environ.Env()


def hierarchical_json(item, df):
    filt_data = df[df['source'] == item.get('name')]
    if filt_data.empty:
        return []
    test = filt_data.rename(columns={'target': 'name',
                                     }, errors="raise")
    to_add = test[['name', 'weight_blocked', 'value']].to_dict(orient="records")
    for new_item in to_add:
        new_item['children'] = hierarchical_json(new_item, df)
    return to_add


class Neo4jConnection:
    def __init__(self, uri, user, pwd, db):
        self.__uri = uri
        self.__user = user
        self.__pwd = pwd
        self.__driver = None
        self.__db = db
        try:
            self.__driver = GraphDatabase.driver(self.__uri, auth=(self.__user, self.__pwd),
                                                 max_connection_lifetime=3600)
            print('Connected to ' + self.__uri)
        except Exception as e:
            print("Failed to create the driver:", e)
    def close(self):
        if self.__driver is not None:
            self.__driver.close()
    def restart(self):
        try:
            self.__driver = GraphDatabase.driver(self.__uri, auth=(self.__user, self.__pwd),
                                                 max_connection_lifetime=3600)
            print('Connected to ' + self.__uri)
        except Exception as e:
            print("Failed to create the driver:", e)
    def run(self, query, db=None):
        if db is None:
            db = self.__db
        return (self.__driver.session(database=db)).run(query)
    def query(self, query, params={}, db=None):
        if db is None:
            db = self.__db
        assert self.__driver is not None, "Driver not initialized!"
        session = None
        response = None
        try:
            with self.__driver.session(database=db) as session:
                print('Query on ' + db)
                result = session.run(query, params)
                response = pd.DataFrame([r.data() for r in result], columns=result.keys())
        except Exception as e:
            return print("Query failed:", e)
        finally:
            if session is not None:
                session.close()
        return response


class Command(LoggingBaseCommand):
    logger = logging.getLogger(f"project.{__name__}")
    help = "Update mertics"

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            '--leg',
            default=19,
            type=int,
            dest="leg",
            help='Legislature')

        parser.add_argument(
            '--days',
            default=7,
            type=int,
            dest="days",
            help='Number days before')

    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.

        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        self.logger.info(f"----- Starting {command_name} -----")
        start_time = time.time()
        call_command(command_name, *args, **options, stdout=self.stdout)
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)
        self.logger.setLevel(
            {
                0: logging.ERROR,
                1: logging.WARNING,
                2: logging.INFO,
                3: logging.DEBUG,
            }.get(options["verbosity"])
        )

        self.logger.info("Update Metrics")
        positional_db = Neo4jConnection(uri=f'neo4j://{env("NEO4J_DOMAIN")}',
                                        user=env("NEO4J_USERNAME"),
                                        pwd=env("NEO4J_PASS"),
                                        db='positionalpower')
        query = 'match (a)-[b]->(c) return a.name as source, b.value as value, ' \
                'c.name as target, c.weight_blocked as weight_blocked'
        df = positional_db.query(query)
        res = {
            "name": "Potere Posizionale",
            "value": 100,
        }

        res['children'] = hierarchical_json(res, df)
        filters = {
            'NoPortafoglio': {'filter': Q(classification='Ministero senza portafoglio') & ~Q(name__icontains='europei'),
                              'sub_filters': {
                                  'Ministro': {'filter': Q(role__istartswith='Ministro')},
                                  'Viceministro': {'filter': Q(role__istartswith='Viceministro')},
                                  'Sottosegretario': {'filter': Q(role__istartswith='Sottosegretario')}}},
            'PortafoglioA': {
                'filter': Q(classification='Ministero', ) & ~Q(name__icontains='Economia') & Q(
                    reduce(operator.or_, (Q(name__icontains=x) for x in ['Esteri', 'Interno', 'Giustizia', 'Difesa']))),
                'sub_filters': {
                    'Ministro': {'filter': Q(role__istartswith='Ministro')},
                    'Viceministro': {'filter': Q(role__istartswith='Viceministro')},
                    'Sottosegretario': {'filter': Q(role__istartswith='Sottosegretario')}

                }
            },
            'PortafoglioB': {
                'filter': Q(classification='Ministero') & ~Q(name__icontains='Economia') & ~Q(
                    reduce(operator.or_, (Q(name__icontains=x) for x in
                                          ['Cultura', 'Turismo', 'Ricerca', 'Esteri', 'Interno', 'Giustizia',
                                           'Difesa']))),
                'sub_filters': {
                    'Ministro': {'filter': Q(role__istartswith='Ministr')},
                    'Viceministro': {'filter': Q(role__istartswith='Viceministr')},
                    'Sottosegretario': {'filter': Q(role__istartswith='Sottosegretari')}
                }
            },
            'PortafoglioC': {
                'filter': (Q(classification='Ministero') & ~Q(name__icontains='Economia') & Q(
                    reduce(operator.or_, (Q(name__icontains=x) for x in
                                          ['Cultura', 'Turismo', 'Ricerca'])))) | (
                              Q(classification='Ministero senza portafoglio') & Q(name__icontains='europei')),
                'sub_filters': {
                    'Ministro': {'filter': Q(role__istartswith='Ministr')},
                    'Viceministro': {'filter': Q(role__istartswith='Viceministr')},
                    'Sottosegretario': {'filter': Q(role__istartswith='Sottosegretari')}
                }
            },
            'MEF': {
                'filter': Q(classification='Ministero', name__icontains='Economia'),
                'sub_filters': {
                    'Ministro': {'filter': Q(role__istartswith='Ministro')},
                    'Viceministro': {'filter': Q(role__istartswith='Viceministro')},
                    'Sottosegretario': {'filter': Q(role__istartswith='Sottosegretario')}
                }

            },
            'Presidenza': {
                'filter': Q(classification='Presidenza del Consiglio'),
                'sub_filters': {
                    'Presidente': {'filter': Q(role__istartswith='Presidente')},
                    'Vicepresidente': {'filter': Q(role__istartswith='Vicepresidente')},
                    'Verbalizzante': {'filter': Q(role__istartswith='Sottosegretario di Stato - Segretario del Consiglio dei Ministri')},
                    'Sottosegretario': {'filter': Q(role__istartswith='Sottosegretario Presidenza del Consiglio')},
                }
            },
            'Senato': {'filter': Q(name__icontains='senato', classification='Assemblea parlamentare')},
            'Camera': {'filter': Q(name__icontains='camera', classification='Assemblea parlamentare')},
            'VigilanzaControllo': {
                'filter': Q(classification='Commissione/comitato bicamerale parlamentare') & ~Q(
                    name__icontains='Sottocomissione') & (Q(name__icontains='SERVIZI RADIOTELEVISIVI')
                                                          | Q(name__icontains='SICUREZZA DELLA REPUBBLICA')),
                'sub_filters': {
                    'Presidente': {'filter': Q(role__istartswith='Presidente')},
                    'Vicepresidente': {'filter': Q(role__istartswith='Vicepresidente')},
                    'Segretario': {'filter': Q(role__istartswith='Segretario')},
                    'Membro': {'filter': Q()},
                }

            },
            'Giunta': {
                'filter': Q(classification='Giunta parlamentare', ),
                'sub_filters': {
                    'Presidente': {'filter': Q(role__istartswith='Presidente')},
                    'Vicepresidente': {'filter': Q(role__istartswith='Vicepresidente')},
                    'Segretario': {'filter': Q(role__istartswith='Segretario')},
                    'Membro': {'filter': Q()},
                }

            },
            'Gruppo': {
                'filter': Q(classification='Gruppo politico in assemblea elettiva'),
                'sub_filters': {
                    'Presidente': {'filter': Q(role__istartswith='Presidente')},
                    'Vicepresidente': {'filter': Q(role__istartswith='Vicepresidente gruppo')},
                    'Vicevicario': {'filter': Q(role__icontains='vicario')},
                    'Tesoriere': {'filter': Q(role__istartswith='Tesoriere')},
                    'Membro': {'filter': Q()},
                }

            },
            'CommissionePermanente': {
                'filter': Q(classification='Commissione permanente parlamentare') & ~Q(
                    name__icontains='sottocommissione'),
                'sub_filters': {
                    'Presidente': {'filter': Q(role__istartswith='Presidente')},
                    'Vicepresidente': {'filter': Q(role__istartswith='Vicepresidente')},
                    'Segretario': {'filter': Q(role__istartswith='Segretario')},
                    'Membro': {'filter': Q()},
                }

            },
            'ConsiglioPresidenza': {
                'filter': Q(classification='Organo di presidenza parlamentare'),
                'sub_filters': {
                    'Presidente': {'filter': Q(role__istartswith='Presidente')},
                    'Vicepresidente': {'filter': Q(role__istartswith='Vicepresidente')},
                    'Questore': {'filter': Q(role__istartswith='Questore')},
                    'Segretario': {'filter': Q(role__istartswith='Segretario')},
                }
            },
            'Assemblea': {
                'filter': Q(classification='Assemblea parlamentare'),
                'sub_filters': {
                    'Membro': {'filter': Q()},
                }
            }
        }
        legislatura_num = options["leg"]
        legislatura_padded = str(legislatura_num).zfill(2)
        legislatura = KeyEvent.objects.get(identifier=f'ITL_{legislatura_padded}')

        starts = OPDMMembership.objects.filter(
            Q(organization__classification__in=['Ministero senza portafoglio', 'Ministero', 'Presidenza del Consiglio',
                                                'Assemblea parlamentare',
                                                'Giunta parlamentare', 'Gruppo politico in assemblea elettiva',
                                                'Commissione permanente parlamentare',
                                                'Organo di presidenza parlamentare']) | (
                Q(organization__classification='Commissione/comitato bicamerale parlamentare')
                & (Q(organization__name__icontains='SERVIZI RADIOTELEVISIVI')
                   | Q(organization__name__icontains='SICUREZZA DELLA REPUBBLICA'))
            )).exclude(
            role='Candidato uninominale').filter(start_date__gte=legislatura.start_date).values_list('start_date',
                                                                                                     flat=True).distinct().order_by(
            'start_date')
        ends = OPDMMembership.objects.filter(
            Q(organization__classification__in=['Ministero senza portafoglio', 'Ministero', 'Presidenza del Consiglio',
                                                'Assemblea parlamentare',
                                                'Giunta parlamentare', 'Gruppo politico in assemblea elettiva',
                                                'Commissione permanente parlamentare',
                                                'Organo di presidenza parlamentare']) | (
                Q(organization__classification='Commissione/comitato bicamerale parlamentare')
                & (Q(organization__name__icontains='SERVIZI RADIOTELEVISIVI')
                   | Q(organization__name__icontains='SICUREZZA DELLA REPUBBLICA'))
            )).exclude(
            role='Candidato uninominale').exclude(end_date__isnull=True).filter(
            start_date__gte=legislatura.start_date).values_list('end_date',
                                                                flat=True).distinct().order_by(
            'end_date')

        day = today - datetime.timedelta(days=abs(options['days']))
        dates = sorted(set(list(starts) + list(ends) + [today_strf]))
        dates_filterd = list(filter(lambda x: x > day.strftime('%Y-%m-%d'), dates))
        for dates_filterd_day in dates_filterd:
            self.logger.info(f"Update Metrics for {dates_filterd_day}")

            self.call_command('positional_power',
                              date=dates_filterd_day,
                              res=res,
                              filters=filters,
                              verbosity=3)
