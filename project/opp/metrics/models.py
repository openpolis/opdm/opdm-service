import re
from operator import itemgetter

import pandas as pd
from django.db import models
from django.core.cache import cache
from django.db.models import Q, F, Sum, Value, Window, Func
from django.db.models.functions import Coalesce, Rank, Round
from django.utils.translation import gettext_lazy as _
from popolo.models import Membership as OPDMMembership, Person, KeyEvent
import datetime
import requests

from project.calendars.models import CalendarDaily
from project.opp.votes.models import Membership

# Create your models here.


class PositionalPower(models.Model):
    """Positional power for each memberships"""
    opdm_membership = models.ForeignKey(
        to=OPDMMembership,
        on_delete=models.CASCADE,
        related_name='positional_power'
    )

    description = models.TextField(null=True)

    value = models.FloatField()

    start_date = models.DateField(
        _("start date"),
        help_text=_("The date value starts"),
    )

    end_date = models.DateField(
        _("end date"),
        null=True,
        help_text=_("The date value ends"),
    )

    @classmethod
    def get_persons_range(cls, sd, ed):

        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')

        today_delta_plus_days = today + datetime.timedelta(days=1)
        today_delta_plus_days_strf = today_delta_plus_days.strftime('%Y-%m-%d')
        ids = cls.objects.annotate(
            end_date_coalesce=Coalesce(F('end_date'), Value(today_delta_plus_days))). \
            filter(start_date__lte=(ed or today_delta_plus_days_strf), end_date_coalesce__gte=sd,
                   end_date_coalesce=(ed or today_delta_plus_days_strf)). \
            annotate(person=F('opdm_membership__person__id')). \
            values_list('person', flat=True).distinct()

        return Person.objects.filter(id__in=ids)

    @classmethod
    def get_gov(cls):

        return cls.objects.select_related('opdm_membership__organization') \
            .filter(
            Q(opdm_membership__organization__classification__icontains='Ministero') |
            Q(opdm_membership__organization__classification__icontains='Presidenza del Consiglio')
        ).filter(
            opdm_membership__end_date__isnull=True
        )

    @classmethod
    def get_gov_leg(cls, leg='19'):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        persons_values = PositionalPower.objects.select_related('opdm_membership__organization') \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today))) \
            .filter(start_date__gte=ke.start_date,
                    end_date_coalesce__lte=(ke.end_date or today)) \
            .filter(
            Q(opdm_membership__organization__classification__icontains='Ministero') |
            Q(opdm_membership__organization__classification__icontains='Presidenza del Consiglio')
        )
        return persons_values

    @classmethod
    def get_chamber_leg(cls, leg='19'):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        persons_values = PositionalPower.objects.select_related('opdm_membership__organization') \
            .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today))) \
            .filter(start_date__gte=ke.start_date,
                    end_date_coalesce__lte=(ke.end_date or today)) \
            .exclude(opdm_membership__organization__classification__icontains='Ministero').exclude(
            opdm_membership__organization__classification__icontains='Presidenza del Consiglio')

        return persons_values

    @classmethod
    def gov_abs_metric(cls):
        if not cache.get('gov_positionalpower_gov_abs_metric'):
            res = cls.get_gov().aggregate(Sum(F('value'))).get('value__sum')
            cache.set(
                'gov_positionalpower_gov_abs_metric',
                {'res': res},
                timeout=None
            )
            return res
        else:
            return cache.get('gov_positionalpower_gov_abs_metric').get('res')

    @classmethod
    def update_cache_gov_abs_metric(cls):
        res = cls.get_gov().aggregate(Sum(F('value'))).get('value__sum')
        cache.set(
            'gov_positionalpower_gov_abs_metric',
            {'res': res},
            timeout=None
        )

    @classmethod
    def aggregate_gov(cls):
        denom = cls.gov_abs_metric()
        res = cls.get_gov().values('opdm_membership__person',
                                   'opdm_membership__person__name').annotate(sum=Sum(100 * F('value')) / denom)
        return res.order_by('-sum')

    @classmethod
    def positional_power_all(cls, leg=None):
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        persons_values = PositionalPower.objects.filter(end_date=ke.end_date) \
            .annotate(person=F('opdm_membership__person')).values('person', 'value')

        aggregated = persons_values.values('person').annotate(sum=Sum(F('value')))
        denom = sum(aggregated.values_list('sum', flat=True))
        aggregated_std = aggregated.annotate(std_value_ord=Round((10000 * F('sum')) / denom),
                                             std_value=((100 * F('sum')) / denom))
        return aggregated_std.order_by('-std_value').annotate(
            order=Window(
                expression=Rank(),
                order_by=[
                    F('std_value_ord').desc(),
                ]))

    @classmethod
    def positional_power_gov(cls, leg=None):
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        persons_values = cls.get_gov().filter(end_date=ke.end_date) \
            .annotate(person=F('opdm_membership__person')).values('person', 'value')

        aggregated = persons_values.values('person').annotate(sum=Sum(F('value')))
        denom = sum(aggregated.values_list('sum', flat=True))
        aggregated_std = aggregated.annotate(std_value=(100 * F('sum')) / denom)
        return aggregated_std.order_by('-std_value').annotate(
            order=Window(
                expression=Rank(),
                order_by=[
                    F('std_value').desc(),
                ]))

    def __str__(self):
        return f"{self.opdm_membership.person.name} @ {self.opdm_membership.organization.name} as {self.description}"


class PersonsOPPManager(models.Manager):

    def all(self, sd, ed):
        all = PersonsOPP.objects.filter(id__in=PositionalPower.get_persons_range(sd,
                                                                                 ed))
        return all

    def all_and_removed(self):
        all = PersonsOPP.objects.filter(
            id__in=PositionalPower.objects.all().values_list('opdm_membership__person_id', flat=True)).annotate(
            pk2=F('slug')
        )
        return all


class PersonsOPP(Person):
    objects = PersonsOPPManager()

    def parliament_by_leg(self, leg=None):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        memb = self.memberships.filter(
            membership_parliament__isnull=False,
            start_date__gte=ke.start_date,
            start_date__lte=(ke.end_date or today_strf)).first()
        if memb:
            return memb.membership_parliament
        return None

    def is_president(self, date):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        return self.memberships \
            .filter(role="Presidente Organo di presidenza parlamentare") \
            .annotate(new_end_date=Coalesce(F('end_date'), Value(today_strf))) \
            .filter(start_date__lte=date, new_end_date__gte=date).count() > 0
    def get_n_rebels_by_leg(self, leg=None):
        membership = self.parliament_by_leg(leg)
        if membership:
            return membership.n_rebels

    def get_n_absent_by_leg(self, leg=None):
        membership = self.parliament_by_leg(leg)
        if membership:
            return membership.n_absent

    def get_n_mission_by_leg(self, leg=None):
        membership = self.parliament_by_leg(leg)
        if membership:
            return membership.n_mission

    def get_n_present_by_leg(self, leg=None):
        membership = self.parliament_by_leg(leg)
        if membership:
            return membership.n_present

    def get_n_voting_by_leg(self, leg=None):
        membership = self.parliament_by_leg(leg)
        if membership:
            return membership.n_voting

    def get_parse_days_in_parliament_by_leg(self, leg=None):
        membership = self.parliament_by_leg(leg)
        if membership:
            return membership.parse_days_in_parliament

    def get_days_in_parliament_by_leg(self, leg=None):
        membership = self.parliament_by_leg(leg)
        if membership:
            return membership.days_in_parliament

    def gov(self, leg=None):
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        gm = self.gov_membership.filter(end_date=ke.end_date)
        if gm:
            pp_party = gm.first().political_party.get(end_date=ke.end_date).political_party

            return \
                {
                    'political_party': getattr(pp_party, 'name', 'Indipendenti'),
                    'image': getattr(pp_party, 'image', None),
                }
        else:
            return {}

    def chamber(self, leg):
        parl = self.parliament_by_leg(leg)
        if parl:
            role = parl.opdm_membership.role
            if self.gender == 'F':
                if role == 'Deputato':
                    role = 'Deputata'
                elif role == 'Senatore':
                    role = 'Senatrice'
            return {'role': role}
        return {}

    def pp(self, leg):
        pp_all = pd.DataFrame(PositionalPower.positional_power_all(leg))

        values = pp_all[pp_all['person'] == self.id]
        if values.empty:
            return None
        else:
            values = values[['std_value', 'order']].iloc[0]
        return {
            'pp_branch': values[0],
            'pp_branch_ordering': values[1],
            'pp_branch_total_ordering': pp_all.shape[0]
        }

    def get_pp_gov(self, leg):
        PositionalPower.positional_power_all(leg)

    def has_changed_group(self, leg):
        return self.parliament_by_leg(leg).has_changed_group

    def gov_member(self, leg):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        from project.opp.gov.models import Member
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        member = (Member.objects.filter(person=self.id, start_date__gte=ke.start_date)
                  .exclude(start_date__gt=(ke.end_date or today_strf))
                  .first())
        if member:
            return {
                'party_img': member.roles[-1].get('party_img'),
                'party_name': (member.roles[-1].get('party_name') or 'Indipendenti'),
                'roles': member.roles}

    def parl_member(self, leg):
        from project.opp.gov.models import Member
        from project.opp.votes.serializers import GroupSerializer
        parl_member = self.parliament_by_leg(leg)

        if parl_member:
            replaced_by = getattr(parl_member, 'replaced_membership_parliament', None)
            start_date = parl_member.opdm_membership.start_date
            if self.chamber(leg).get('role') == 'Senatore a vita':
                start_date = (OPDMMembership.objects.filter(role='Senatore a vita', person=self.id)
                              .order_by('start_date').first().start_date)

            return {'role': self.chamber(leg).get('role'),
                    'start_date': start_date,
                    'end_date': parl_member.opdm_membership.end_date,
                    'latest_group': GroupSerializer(parl_member.get_latest_membership_group.group).data,
                    'has_changed_group': parl_member.has_changed_group,
                    'election_area': parl_member.election_area.name if parl_member.election_area else None,
                    'supports_majority': parl_member.supports_majority,
                    'replaced_by': {
                        'slug': replaced_by.opdm_membership.person.slug,
                        'name': replaced_by.opdm_membership.person.name,
                        'img': replaced_by.opdm_membership.person.image
                    } if replaced_by else None,
                    'replaced_member': {
                        'slug': parl_member.replaced_member.opdm_membership.person.slug,
                        'name': parl_member.replaced_member.opdm_membership.person.name,
                        'img': parl_member.replaced_member.opdm_membership.person.image
                    } if getattr(parl_member, 'replaced_member', None) else None}

    def get_pp_indeces(self, leg):
        today = datetime.datetime.now().date()

        from project.opp.gov.models import Member, GovPartyMember
        from project.opp.votes.models import Membership
        parl_member = self.parliament_by_leg(leg)
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        if parl_member:
            end_months = CalendarDaily.objects.filter(Q(is_month_end=True, date__gte=ke.start_date,
                                                        date__lte=(parl_member.opdm_membership.end_date or ke.end_date or today))
                                                      | Q(date=(parl_member.opdm_membership.end_date or ke.end_date or today))
                                                      )
        else:
            end_months = CalendarDaily.objects.filter(Q(is_month_end=True, date__gte=ke.start_date,
                                                        date__lte=(ke.end_date or today))
                                                      | Q(
                date=(ke.end_date or today)))

        timeline = []
        show_timeline = False
        prev_values = {"ppg": None,
                       "pp_branch": None}
        for year in end_months.values_list('date__year', flat=True).distinct():
            values_month = []
            timeline.append({"year": year,
                             "values": values_month})
            for month in end_months.filter(date__year=year):
                # gov
                gov_members_values = PositionalPower.get_gov_leg() \
                    .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today))).filter(
                    start_date__lte=month.date, end_date_coalesce__gte=month.date)
                denom_gov = gov_members_values.aggregate(denom=Sum('value')).get('denom')
                # chamber
                chamber_members_values = PositionalPower.get_chamber_leg() \
                    .annotate(end_date_coalesce=Coalesce(F('end_date'), Value(today))).filter(
                    start_date__lte=month.date, end_date_coalesce__gte=month.date)
                denom_chamber = None
                if parl_member:
                    if parl_member.branch == 'S':
                        denom_chamber = chamber_members_values.filter(
                            Q(opdm_membership__organization__parent__name__icontains='Senato') |
                            (Q(opdm_membership__organization__parent__name__icontains='Parlamento Italiano',
                               opdm_membership__person__in=Membership.objects.filter(
                                   opdm_membership__organization=parl_member.opdm_membership.organization)
                               .values_list('opdm_membership__person_id', flat=True)))) \
                            .aggregate(denom=Sum('value')).get('denom')
                    else:
                        denom_chamber = chamber_members_values.filter(
                            Q(opdm_membership__organization__parent__name__icontains='Camera') |
                            (Q(opdm_membership__organization__parent__name__icontains='Parlamento Italiano',
                               opdm_membership__person__in=Membership.objects.filter(
                                   opdm_membership__organization=parl_member.opdm_membership.organization)
                               .values_list('opdm_membership__person_id', flat=True)))) \
                            .aggregate(denom=Sum('value')).get('denom')
                ppg = (round(
                    100 * gov_members_values.filter(opdm_membership__person_id=self.id).aggregate(num=Sum('value')).get(
                        'num') / denom_gov, 2)) \
                    if gov_members_values.filter(opdm_membership__person_id=self.id).aggregate(num=Sum('value')).get(
                    'num') else None
                pp_branch = (round(100 * chamber_members_values.filter(opdm_membership__person_id=self.id).aggregate(
                    num=Sum('value')).get('num') / denom_chamber, 2)) \
                    if parl_member and chamber_members_values.filter(
                    opdm_membership__person_id=self.id) else None
                values_month.append(
                    {"month": month.month,
                     "ppg": ppg,
                     "pp_branch": pp_branch
                     }
                )
                new_values = {
                    "ppg": ppg,
                    "pp_branch": pp_branch
                }
                if not show_timeline and \
                    ((prev_values.get('ppg') or new_values.get('ppg')) != new_values.get('ppg') or (
                        prev_values.get('pp_branch') or new_values.get('pp_branch')) != new_values.get('pp_branch')):
                    show_timeline = True
                prev_values = new_values
        total_ordering = None
        if getattr(parl_member, 'branch', None) == 'C':
            total_ordering = Membership.objects.filter(opdm_membership__end_date__isnull=True,
                                                       opdm_membership__role__startswith='D').count()
        elif getattr(parl_member, 'branch', None) == 'S':
            total_ordering = Membership.objects.filter(opdm_membership__end_date__isnull=True,
                                                       opdm_membership__role__startswith='S').count()

        gov_member = PositionalPower.positional_power_gov(leg)
        df_gov_member = pd.DataFrame(gov_member)
        PPG_branch_ordering = None
        PPG_branch_total_ordering = None
        # ppg = None
        gov_member_specific = Member.objects.by_legislature(leg).filter(person=self).first()
        if gov_member.filter(person=self.id):

            memb = df_gov_member[df_gov_member['person'] == self.id]
            # ppg = round(memb.iloc[0]['std_value'], 2)
            PPG_branch_ordering = memb.iloc[0]['order']
            PPG_branch_total_ordering = pd.DataFrame(gov_member).shape[0]

        res = {
            'has_changed_timeline': show_timeline,
            'branch': getattr(parl_member, 'branch', None),
            'pp_branch': timeline[-1]['values'][-1].get('pp_branch', None),
            'pp_branch_ordering': getattr(parl_member, 'pp_ordering', None),
            'pp_branch_total_ordering': total_ordering,
            'ppg': ppg,
            'ppg_branch_ordering': int(PPG_branch_ordering or 0),
            'ppg_branch_total_ordering': PPG_branch_total_ordering,
            'timeline': timeline
        }
        return res

    def get_fidelity(self, leg):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        res = {

            "current": None,
            "voting_timeline": [],
            "groups_timeline": [],
            "majority_support_timeline": []
        }
        parl_member = self.parliament_by_leg(leg)
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        end_months = CalendarDaily.objects.filter(Q(is_month_end=True, date__gte=ke.start_date,
                                                    date__lte=(ke.end_date or today)) | Q(date=(ke.end_date or today)))

        voting_timeline = []

        if parl_member:
            groups = parl_member.membership_groups.all().order_by('start_date')
            groups_timeline = []
            for g in groups:
                if g.group.acronym == 'Nessun gruppo' and len(groups)>1:
                    continue
                groups_timeline.append({
                    "start_date": g.start_date,
                    "end_date": g.end_date,
                    "group": {
                        "acronym": g.group.acronym_last,
                        "name": g.group.organization.name,
                        "color": g.group.color
                    }
                })
            if groups_timeline[-1].get('end_date', None):
                end_months = end_months.filter(date__lte=groups_timeline[-1].get('end_date', None))

            for year in end_months.values_list('date__year', flat=True).distinct():
                values_month = []
                voting_timeline.append({"year": year,
                                        "values": values_month})
                for month in end_months.filter(date__year=year):
                    hist = parl_member.historical_metrics.filter(year__lte=year).exclude(year=year, month__gt=month.month)
                    # hist = parl_member.historical_metrics.filter(month=month.month, year=year).first()
                    if not hist:
                        values_month.append(
                            {"month": month.month,
                             "perc": None
                             }
                        )
                    else:
                        n_fidelity =  (hist.aggregate(sum=Sum('n_fidelity')).get('sum') or 0)
                        n_present = (hist.aggregate(sum=Sum('n_present')).get('sum') or 0)
                        n_absent = (hist.aggregate(sum=Sum('n_absent')).get('sum') or 0)
                        n_mission = (hist.aggregate(sum=Sum('n_mission')).get('sum') or 0)
                        # n_fidelity = (hist.n_fidelity or 0)
                        # n_present = (hist.n_present or 0)
                        # n_absent = (hist.n_absent or 0)
                        # n_missing = (hist.n_mission or 0)

                        values_month.append(
                            {"month": month.month,
                             "perc": None if not (n_present + n_absent + n_mission) else round(100 * n_fidelity /
                                                                                               (
                                                                                                   n_present + n_absent + n_mission),
                                                                                               2)
                             }
                        )

            majority_support_timeline = []
            maj_per = parl_member.majority_periods.all().order_by('start_date')
            for m in maj_per:
                majority_support_timeline.append({
                    "start_date": m.start_date,
                    "end_date": m.end_date,
                    "supports_majority": m.value
                })

            res = {
                "current": parl_member.fidelity_index,
                "voting_timeline": voting_timeline,
                "groups_timeline": groups_timeline,
                "majority_support_timeline": majority_support_timeline
            }
        return res

    def get_vote_behaviours(self, leg):
        from project.opp.votes.serializers import MemberVoteDetailSerializer
        parl_member = self.parliament_by_leg(leg)
        if not parl_member:
            return {
                "rebel": {
                    "count": None,
                    "results": []
                },
                "key": {
                    "count": None,
                    "results": []
                },

                "confidence": {
                    "count": None,
                    "results": []
                },
            }
        votes = parl_member.votes.order_by('-voting__sitting')
        rebel = votes.filter(is_rebel=True)
        key = votes.filter(voting__is_key_vote=True)
        confidence = votes.filter(voting__is_confidence=True)
        res = {
            "rebel": {
                "count": rebel.count(),
                "results": [MemberVoteDetailSerializer(x).data for x in rebel[:5]]
            },
            "key": {
                "count": key.count(),
                "results": [MemberVoteDetailSerializer(x).data for x in key[:5]]
            },

            "confidence": {
                "count": confidence.count(),
                "results": [MemberVoteDetailSerializer(x).data for x in confidence[:5]]
            },
        }
        return res

    def get_first_signer_bills(self, leg):
        from project.opp.acts.serializers import BillSignerDetailSerializer
        from project.opp.acts.models import BillSigner, Bill

        billsigners = BillSigner.objects.by_legislature('19').filter(membership__person_id=self.id, first_signer=True,
                                                                     bill__origin=True)
        detail_status = [x.bill.get_last_bill_tree().status for x in billsigners]
        res = {
            "count": billsigners.count(),
            "is_law": len(list(filter(lambda x: x == Bill.LAW, detail_status))),
            "one_branch": len(list(filter(lambda x: x == Bill.ONE_BRANCH, detail_status))),
            "first_step": len(list(filter(lambda x: x == Bill.EXAMINATION, detail_status))),
            "to_begin": len(list(filter(lambda x: x == Bill.TO_START, detail_status))),
            "rejected": len(list(filter(lambda x: x == Bill.REJECTED, detail_status))),
            "results": [BillSignerDetailSerializer(x).data for x in billsigners[:5]]
        }
        return res

    def get_parliamentary_positions_current(self, leg):
        today = datetime.datetime.now().date()
        today_strf = today.strftime('%Y-%m-%d')
        ke = KeyEvent.objects.get(identifier=f'ITL_{leg}')
        memberships = OPDMMembership.objects.filter(person=self).exclude(
            organization__classification__icontains='Ministero') \
            .exclude(
            organization__classification__icontains='Presidenza del Consiglio') \
            .exclude(
            role__in=['Deputato', 'Senatore', 'Membro gruppo politico in assemblea elettiva']) \
            .filter(
            organization__classification__in=[
                'Gruppo politico in assemblea elettiva',
                'Giunta parlamentare',
                'Commissione/comitato bicamerale parlamentare',
                'Organo di presidenza parlamentare',
                'Commissione permanente parlamentare',
                'Commissione speciale/straordinaria parlamentare',
                'Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza',
                'Commissione d\'inchiesta parlamentare monocamerale/bicamerale',
                'Delegazione parlamentare presso assemblea internazionale']) \
            .filter(start_date__gte=ke.start_date, start_date__lte=(ke.end_date or today)).values(
            'id').distinct().order_by('-end_date', '-start_date')

        results = memberships.annotate(date_start=F('start_date'),
                                       date_end=F('end_date'),
                                       org=F('organization__name')).values('org', 'role', 'date_start', 'date_end')
        for i in results:
            i['role'] = i['role'].split(' ')[0].strip()
            i['original_org'] = i['org']
            i['org'] = re.sub(r'\(X.*legislatura\)', '', i['org']).strip()
        started = []
        ended = []
        for i in results:
            if i['date_end'] and results.filter(org=i['original_org'], date_start=i['date_end']):
                contiguous_role = results.filter(org=i['original_org'], date_start=i['date_end']).order_by('date_end').last()
                i['date_end'] = contiguous_role['date_end']
            if i['date_end']:
                ended.append(i)
            else:
                started.append(i)

        return {
            "count": memberships.count(),
            "results": started + ended}

    def get_parliamentary_positions_prev(self, leg):
        results = []
        op_id_istance = self.identifiers.filter(scheme='OP_ID').first()
        if op_id_istance:
            op_id = op_id_istance.identifier
            r = requests.get(f'http://api3.openpolis.it/parlamento/17/parliamentarians/{op_id}')
            if r.ok and r.json() and not 'errore' in r.json().keys():
                data = r.json()
                sen_dep = list(
                    filter(lambda x: x['name'] in ['Deputato', 'Senatore', 'Senatore a vita'], data['charges']))
                if len(sen_dep) > 0:
                    case = sen_dep[0]
                    leg_data = {
                        "legislature": "XVII",
                        "role": case['name'],
                        "group": case['groups'][-1]['name'],
                        "has_changed_group": len(case['groups']) > 1,
                        "approfondisci_url": f"https://parlamento17.openpolis.it/parlamentare/{op_id}"
                    }

                    if case.get('statistics'):
                        leg_data["PRES_perc"] = case['statistics']['presences_perc']
                        leg_data["ABS_perc"] = case['statistics']['absences_perc']
                        leg_data["MIS_perc"] = case['statistics']['missions_perc']
                    else:
                        leg_data["PRES_perc"] = None
                        leg_data["ABS_perc"] = None
                        leg_data["MIS_perc"] = None
                    results.append(leg_data)
            r = requests.get(f'http://api3.openpolis.it/parlamento/18/parliamentarians/{op_id}')
            data = r.json()
            if data and not 'errore' in r.json().keys():
                sen_dep = list(
                    filter(lambda x: x['name'] in ['Deputato', 'Senatore', 'Senatore a vita'], data['charges']))
                if len(sen_dep) > 0:
                    case = sen_dep[0]
                    results.append({
                        "legislature": "XVIII",
                        "role": case['name'],
                        "group": case['groups'][-1]['name'],
                        "has_changed_group": len(case['groups']) > 1,
                        "approfondisci_url": f"https://parlamento18.openpolis.it/parlamentare/{op_id}",
                        "PRES_perc": case['statistics']['presences_perc'],
                        "ABS_perc": case['statistics']['absences_perc'],
                        "MIS_perc": case['statistics']['missions_perc']
                    })

        return {
            "count": len(results),
            "results": results[::-1]}

    def get_carreer_positions(self, leg):
        res = []

        membs = self.memberships.all().filter(
            organization__classification__in=['Assemblea parlamentare',
                                              'Consiglio comunale',
                                              'Consiglio regionale',
                                              'Giunta comunale',
                                              'Giunta regionale',
                                              'Organo legislativo internazionale o europeo',
                                              'Agenzia dello Stato',
                                              'Azienda o ente del servizio sanitario nazionale',
                                              'Dipartimento/Direzione/Ufficio',
                                              'Governo della Repubblica',
                                              'Istituto o ente pubblico di ricerca',
                                              'Ufficio di diretta collaborazione',
                                              'Ambasciata',
                                              'Autorità indipendente',
                                              'Forza armata o di ordine pubblico e sicurezza',
                                              'Istituzione internazionale o europea',
                                              'Organo costituzionale o a rilevanza costituzionale',
                                              'Organo esecutivo internazionale o europeo',
                                              'Prefettura',
                                              'Università pubblica', ]).exclude(role__istartswith='Candidato plurinominale').order_by('-start_date')

        for memb in membs:

            if memb.organization.classification in ['Assemblea parlamentare',
                                                    'Consiglio comunale',
                                                    'Consiglio regionale',
                                                    'Giunta comunale',
                                                    'Giunta regionale',
                                                    'Organo legislativo internazionale o europeo']:

                if memb.organization.classification == 'Assemblea parlamentare':
                    if memb.electoral_event.start_date > '2008-04-13':
                        continue

            elif memb.organization.classification in ['Agenzia dello Stato',
                                                      'Azienda o ente del servizio sanitario nazionale',
                                                      'Dipartimento/Direzione/Ufficio',
                                                      'Governo della Repubblica',
                                                      'Prefettura',
                                                      'Istituto o ente pubblico di ricerca',
                                                      'Ufficio di diretta collaborazione']:
                if memb.organization.classification == 'Dipartimento/Direzione/Ufficio':
                    if memb.role in ["Ministro Dipartimento/Direzione/Ufficio",
                                     "Viceministro Dipartimento/Direzione/Ufficio",
                                     "Sottosegretario Dipartimento/Direzione/Ufficio"]:
                        continue

            res.append(memb.id)

        return OPDMMembership.objects.filter(id__in=res)

    class Meta:
        proxy = True
