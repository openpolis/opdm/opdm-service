from django.contrib import admin
from project.opp.metrics.models import PositionalPower
# Register your models here.


@admin.register(PositionalPower)
class PositionalPowerAdmin(admin.ModelAdmin):
    search_fields = ('opdm_membership__person__name', )

    list_display = ('__str__', 'value', 'start_date', 'end_date')
    ordering = ('opdm_membership', '-start_date', )
    raw_id_fields = ('opdm_membership', )
    pass
