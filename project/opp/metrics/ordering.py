from django.db.models import Sum, Case, When
from rest_framework.filters import OrderingFilter

class PersonsOrdering(OrderingFilter):

    allowed_custom_filters = ['pp', ]

    def get_ordering(self, request, queryset, view):
        """
        Ordering is set by a comma delimited ?ordering=... query parameter.

        The `ordering` query parameter can be overridden by setting
        the `ordering_param` value on the OrderingFilter or by
        specifying an `ORDERING_PARAM` value in the API settings.
        """
        params = request.query_params.get(self.ordering_param)
        if params in ['pp', '-pp']:
            return params
        if params:
            fields = [param.strip() for param in params.split(',')]
            ordering = self.remove_invalid_fields(queryset, fields, view, request)
            if ordering:
                return ordering

        # No ordering was included, or all the ordering fields were invalid
        return self.get_default_ordering(view)


    def filter_queryset(self, request, queryset, view):

        ordering = self.get_ordering(request, queryset, view)
        if ordering in ['pp', '-pp']:
            pk_list = queryset.filter(memberships__positional_power__end_date__isnull=True).distinct().annotate(
                pp=Sum('memberships__positional_power__value')).order_by(ordering).values_list('id', flat=True)

            preserved = Case(*[When(pk=pk, then=pos) for pos, pk in enumerate(pk_list)])
            queryset = queryset.filter(pk__in=pk_list).order_by(preserved)
            return queryset

        if ordering:
            return queryset.order_by(*ordering)

        return queryset
