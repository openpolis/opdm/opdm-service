from ckeditor.fields import RichTextField
from django.db import models
from django.db.models import TextField


class Topic(models.Model):
    title = models.CharField(
        max_length=512,
    )
    ordering = models.PositiveSmallIntegerField(
        null=True,
        help_text="Priorità di ordinamento"
    )
    description = TextField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "Topic"
        verbose_name_plural = "Topic"
        ordering = ('ordering', )


class FAQ(models.Model):
    """Classe che contiene testi da utilizzare nella pagina delle FAQ.

    Il campo ordinamento permette di ordinare i testi.
    """
    title = models.CharField(
        max_length=512,
    )
    description = RichTextField(
    )
    ordering = models.PositiveSmallIntegerField(
        null=True,
    )
    topic = models.ForeignKey(
        to="Topic",
        on_delete=models.PROTECT,
        related_name="faqs"
    )

    def __str__(self) -> str:
        s = self.title
        return s

    class Meta:
        verbose_name = "Testo FAQ"
        verbose_name_plural = "Testi FAQ"
        ordering = ('topic__ordering', 'ordering' )
