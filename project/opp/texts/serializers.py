"""Define elections serializers."""
from rest_framework import serializers
from rest_framework.fields import ListField
from rest_framework.relations import HyperlinkedIdentityField
from project.opp.texts.models import Topic, FAQ


class LegislatureHyperlinkedIdentityField(HyperlinkedIdentityField):

    def get_url(self, obj, view_name, request, format):
        """
        Given an object, return the URL that hyperlinks to the object,
        considering the legislature
        """
        # Unsaved objects will not yet have a valid URL.
        if hasattr(obj, 'pk') and obj.pk is None:
            return None
        if hasattr(obj, 'pk2') and obj.pk2 is not None:
            self.lookup_field = 'pk2'

        lookup_value = getattr(obj, self.lookup_field)
        kwargs = {
            'legislature': request.parser_context['kwargs']['legislature'],
            self.lookup_field: lookup_value,
        }
        return self.reverse(view_name, kwargs=kwargs, request=request, format=format)


class FaqSerializer(serializers.ModelSerializer):


    class Meta:
        model = FAQ
        ref_name = "Faq"
        fields = ("id", "title", "description", "ordering")

class TopicListSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    url = LegislatureHyperlinkedIdentityField(
        view_name='topic-detail',
    )

    faqs = FaqSerializer(many=True)
    class Meta:
        """Define serializer Meta."""

        model = Topic
        ref_name = "Topic"
        fields = ("url", "title", "description",  "ordering", "faqs")


class TopicDetailSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for the Voting model when seen in lists."""
    url = LegislatureHyperlinkedIdentityField(
        view_name='topic-detail',
    )

    class Meta:
        """Define serializer Meta."""

        model = Topic
        ref_name = "Topic"
        fields = "__all__"
