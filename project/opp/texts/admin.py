from django.contrib import admin

from project.opp.texts.models import Topic, FAQ


@admin.register(Topic)
class TopicAdmin(admin.ModelAdmin):

    search_fields = ('title', )
    list_display = ('ordering', 'title')
    list_display_links = ('title', )
    ordering = ('ordering', )


@admin.register(FAQ)
class FAQAdmin(admin.ModelAdmin):

    search_fields = ('title', 'description', )
    list_display = ('title', 'topic','ordering')
    list_display_links = ('title', )
    ordering = ('topic__ordering', 'ordering' )
