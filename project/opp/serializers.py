from django.db.models import F
from rest_framework import serializers
from project.opp.gov.models import Government


class BaseSerializerOPP(serializers.HyperlinkedModelSerializer):
    codelists = serializers.SerializerMethodField()

    def get_codelists(self, obj):

        if self.context.get('request', None):
            leg =self.context['request'].parser_context['kwargs']['legislature']
        elif self.context and self.context['leg']:
            leg = self.context['leg']
        elif hasattr(obj, 'legislature_num'):
            leg = obj.legislature_num

        governments = Government.get_governments_by_leg(leg=leg)\
            .annotate(value=F('organization__name'),
                      slug=F('organization__slug')).values('id', 'slug', 'value')
        governments = list(governments)
        for gov in governments:
            gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()
        return {
            'governments': governments
        }
