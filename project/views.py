import os

from rest_framework.decorators import api_view
from rest_framework.response import Response

from django.urls import reverse
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from popolo.models import Area, Organization, Person, Membership, Ownership

from config import settings


class AboutView(TemplateView):
    def get_context_data(self, **kwargs):
        return {
            "n_memberships": Membership.objects.count(),
            "n_ownerships": Ownership.objects.count(),
            "n_persons": Person.objects.count(),
            "n_organizations": Organization.objects.count(),
            "n_current_areas": Area.objects.current().count(),
            "n_past_areas": Area.objects.past().count(),
        }


class DatalogView(TemplateView):
    def get_context_data(self, **kwargs):
        with open(os.path.join(settings.ROOT_PATH, "DATA_CHANGELOG.md"), "r") as f:
            return {
                "md_content": f.read(),
            }


class ChangelogView(TemplateView):
    def get_context_data(self, **kwargs):
        with open(os.path.join(settings.ROOT_PATH, "CHANGELOG.md"), "r") as f:
            return {
                "md_content": f.read(),
            }


@api_view()
def api_list(request, *args, **kwargs):
    """A browsable list of all available API endpoints"""
    endpoints = [
        request.build_absolute_uri(reverse('openparlamento_v1:legislature-list'))
    ]
    if request.user.is_authenticated:
        endpoints.append(request.build_absolute_uri(reverse('mappepotere_v1:api-root')),)
    return Response(endpoints)


class V1RedirectView(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        url = self.request.build_absolute_uri(reverse('mappepotere_v1:api-root'))
        if self.request.GET:
            return f"{url}{kwargs.get('path', '')}?{self.request.GET.urlencode()}"
        else:
            return f"{url}{kwargs.get('path', '')}"
