# coding=utf-8
import jwt
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import SlidingToken


class JWTAuthenticationTests(APITestCase):
    """Test JWT authentication http endpoints."""

    def setUp(self):
        self.testuser = User.objects.create_user("test", "admin@test.com", "pass")
        self.testuser.save()

        self.groupA = Group.objects.create(name='groupA')
        self.groupB = Group.objects.create(name='groupB')
        self.groupC = Group.objects.create(name='groupC')

        self.testuser.groups.add(self.groupA, self.groupB)

    @staticmethod
    def _get_token(user) -> str:
        return str(SlidingToken.for_user(user))

    def test_jwt_auth_obtain(self):
        """Ensure POSTing form over JWT auth with correct credentials succeds."""
        url = reverse("simplejwt_obtain_token")
        data = {"username": self.testuser.username, "password": "pass"}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("token", response.data)

    def test_jwt_contains_claims(self):
        """Ensure the JWT contains the expected custom claims."""
        url = reverse("simplejwt_obtain_token")
        data = {"username": self.testuser.username, "password": "pass"}
        response = self.client.post(url, data)
        token = response.json()['token']
        decoded_token = jwt.decode(token, settings.SECRET_KEY, algorithms=['HS256'])

        self.assertTrue(all(
            k in decoded_token.keys() for k in ['https://hasura.io/jwt/claims', 'https://openpolis.io/jwt/claims']
        ))

        self.assertTrue(all(
            g in decoded_token['https://hasura.io/jwt/claims']['x-hasura-allowed-roles']
            for g in self.testuser.groups.values_list('name', flat=True)
        ))

        self.assertFalse(
            'groupC' in decoded_token['https://hasura.io/jwt/claims']['x-hasura-allowed-roles']
        )

        self.assertTrue(all(
            g in decoded_token['https://openpolis.io/jwt/claims']['groups']
            for g in self.testuser.groups.values_list('name', flat=True)
        ))

        self.assertFalse(
            'groupC' in decoded_token['https://openpolis.io/jwt/claims']['groups']
        )

    def test_jwt_auth_obtain_fail(self):
        """Ensure POSTing form over JWT auth without correct credentials fails. """
        url = reverse("simplejwt_obtain_token")
        data = {
            "username": self.testuser.username,
            "password": "totally-wrong-password",
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_jwt_auth_refresh(self):
        """Refresh a JWT token which is not expired. """
        url = reverse("simplejwt_refresh_token")
        token = self._get_token(self.testuser)
        data = {"token": token}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn("token", response.data)
        new_token = response.data.get("token")
        self.assertEqual(token, new_token)

    def test_jwt_auth_verify(self):
        """Verify a JWT token which is not expired. """
        url = reverse("simplejwt_verify_token")
        token = self._get_token(self.testuser)
        data = {"token": token}
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_jwt_auth_verify_fail(self):
        """Ensure POSTing an invalid token to the verification endpoint fails. """
        url = reverse("simplejwt_verify_token")
        response = self.client.post(url, {"token": "definitely.invalid.token"})
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
