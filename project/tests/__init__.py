# coding=utf-8
from django.test import TestCase


class MockResponse:
    """class that mocks requests' response (json method)
    """

    def __init__(self, json_data, status_code, ok, reason=None):
        self.json_data = json_data
        self.status_code = status_code
        self.ok = ok
        self.reason = reason

    def json(self):
        return self.json_data


class ProjectTests(TestCase):
    def test_homepage(self):
        response = self.client.get("/")
        self.assertRedirects(
            response=response, expected_url="/about", target_status_code=301
        )

    def test_about(self):
        response = self.client.get("/about")
        self.assertEqual(response.status_code, 301)
        response = self.client.get("/about/")
        self.assertEqual(response.status_code, 200)

    def test_about_it(self):
        response = self.client.get("/about/it")
        self.assertEqual(response.status_code, 301)
        response = self.client.get("/about/it/")
        self.assertEqual(response.status_code, 200)
