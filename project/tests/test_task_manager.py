import logging

import django.test
from io import StringIO
from django.core.management import call_command


class TaskManagerTest(django.test.TestCase):
    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.tasks.management.commands.test_logging_command"
        )
        logger.handlers = []

    def test_command_output_debug(self):
        out = StringIO()
        call_command(
            "test_logging_command", verbosity=3, debug="test_debug", stdout=out
        )
        self.assertTrue("DEBUG" in out.getvalue() and "test_debug" in out.getvalue())

    def test_command_output_info(self):
        out = StringIO()
        call_command("test_logging_command", verbosity=2, info="test_info", stdout=out)
        self.assertTrue("INFO" in out.getvalue() and "test_info" in out.getvalue())
        self.assertFalse("DEBUG" in out.getvalue())

    def test_command_output_warning(self):
        out = StringIO()
        call_command(
            "test_logging_command", verbosity=1, warning="test_warning", stdout=out
        )
        self.assertTrue(
            "WARNING" in out.getvalue() and "test_warning" in out.getvalue()
        )
        self.assertFalse("INFO" in out.getvalue())
        self.assertFalse("DEBUG" in out.getvalue())

    def test_command_output_error(self):
        out = StringIO()
        call_command(
            "test_logging_command", verbosity=1, error="test_error", stdout=out
        )
        self.assertTrue("ERROR" in out.getvalue() and "test_error" in out.getvalue())

    def test_command_no_empty_lines(self):
        out = StringIO()
        call_command(
            "test_logging_command", verbosity=3, debug="test_debug", stdout=out
        )

        out.seek(0)
        lines = out.readlines()
        found = False
        for line in lines:
            found = found or (line == "\n")
        self.assertFalse(found)
