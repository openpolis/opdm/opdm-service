from ooetl.extractors import CSVExtractor
from ooetl.transformations import Transformation
from popolo.models import OrganizationRelationship, Classification
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import CSVDiffCompositeETL
from project.tasks.etl.loaders import PopoloLoader
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class SRTransformation(Transformation):
    """Instance of ``ooetl.transformations.Transformation`` class that handles
    transforming OrganizationSupervitionRelationships csv data (cleanup).
    """

    def transform(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns.

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original dataframe
        od = self.etl.original_data.copy()

        # convert ids to integers
        od.id_org_vigilata = od.id_org_vigilata.astype(int)
        od.id_org_vigilante = od.id_org_vigilante.astype(int)
        od.loc[od.start_date.isnull(), "start_date"] = None
        od.loc[od.end_date.isnull(), "end_date"] = None

        # store processed data into the ETL instance
        self.etl.processed_data = od

        # return ETL instance
        return self


class OrgRelationshipLoader(PopoloLoader):
    cl = None

    def load(self, **kwargs):
        self.cl, created = Classification.objects.get_or_create(
            scheme='OP_TIPO_RELAZIONE_ORG',
            code='OT_01',
            descr='Vigilanza'
        )
        super().load(**kwargs)

    def load_item(self, item, **kwargs):
        try:
            rel, created = OrganizationRelationship.objects.update_or_create(
                source_organization_id=item['id_org_vigilante'],
                dest_organization_id=item['id_org_vigilata'],
                classification=self.cl,
                defaults={
                    'start_date': item['start_date'],
                    'end_date': item['end_date']
                }
            )
            if created:
                self.logger.info(f"Relazione {rel} creata.")
            else:
                self.logger.info(f"Relazione {rel} aggiornata.")

        except Exception as e:
            self.logger.error(f"{e} while importing rel {item}")


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Script that imports supervision (vigilanza) relationships between orgs"
    csv_url = "https://opdm-service-data.s3.eu-central-1.amazonaws.com/parsers/relazioni_vigilanza_organizzazioni.csv"

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.handle_cache(*args, **options)

        self.logger.info("Start")

        CSVDiffCompositeETL(
            extractor=CSVExtractor(
                source=self.csv_url,
                sep=",",
                na_values=["", "-"], keep_default_na=False
            ),
            transformation=SRTransformation(),
            loader=OrgRelationshipLoader(),
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename="org_supervision_relationships.csv",
            clear_cache=self.clear_cache,
            unique_idx_cols=(1, 2, ),
            log_level=self.logger.level,
            logger=self.logger,
        )()

        self.logger.info("End")
