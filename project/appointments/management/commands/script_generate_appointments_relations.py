import itertools
import operator
from collections import namedtuple

from django.db import transaction
from popolo.models import Membership
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Script that generates appointments' relations, starting from rules"

    def add_arguments(self, parser):
        parser.add_argument(
            '--reset',
            dest='reset',
            action='store_true',
            help="Whether to reset all relations. Defaults to false (only generate new ones)."
        )

    def handle(self, *args, **options):
        """The general handler wraps calls to specific meta-rules handlers

        :param args:
        :param options:
        :return:
        """
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        reset = options.get("reset")

        self.logger.info("Start generating relations")

        self.logger.info("Elaborating appointment relationships from Posts ...")

        AppointmentTuple = namedtuple('AppointmentTuple', ['appointer_id', 'appointed_id'])
        MembershipTuple = namedtuple('MembershipTuple', ['id', 'start_date', 'end_date'])

        appointments = []

        memberships = Membership.objects.filter(
            post__appointed_by__isnull=False
        ).exclude(is_appointment_locked=True)
        if not reset:
            memberships = memberships.exclude(
                appointed_by__isnull=False
            )
        memberships = memberships.values(
            'id', 'start_date', 'end_date', 'post',
            'post__appointed_by', 'post__appointed_by__memberships',
            'post__appointed_by__memberships__start_date', 'post__appointed_by__memberships__end_date'
        )
        sorted_memberships = sorted(memberships, key=operator.itemgetter('post', 'post__appointed_by'))
        self.logger.info(f"Elaborating cross table of {memberships.count()} records.")

        groups = itertools.groupby(
            sorted_memberships,
            key=operator.itemgetter('post', 'post__appointed_by')
        )
        for g in groups:
            memberships = [m for m in g[1]]
            appointed_memberships = sorted(
                list(set([
                    MembershipTuple(
                        m['id'], m['start_date'], m['end_date']
                    ) for m in memberships])),
                key=operator.itemgetter(1)
            )
            appointer_memberships = sorted(
                list(set([
                    MembershipTuple(
                        m['post__appointed_by__memberships'],
                        m['post__appointed_by__memberships__start_date'],
                        m['post__appointed_by__memberships__end_date'],
                    ) for m in memberships])),
                key=operator.itemgetter(1)
            )
            self.logger.debug(
                f"Appointed: {g[0][0]}: {len(appointed_memberships)}. "
                f"Appointer: {g[0][1]}: {len(appointer_memberships)}"
            )
            for m in appointed_memberships:
                try:
                    appointer = next(filter(
                        lambda x:
                        (x.start_date is None or x.start_date <= m.start_date) and
                        (x.end_date is None or x.end_date >= m.start_date),
                        appointer_memberships
                    ))
                    appointments.append(AppointmentTuple(appointer.id, m.id))
                except StopIteration:
                    continue

        # reset appointments relationships,
        # excluding manually created ones (is_appointment_locked)
        with transaction.atomic():
            if reset:
                self.logger.info("Resetting appointments relationships in DB ...")
                Membership.objects\
                    .filter(appointed_by__isnull=False)\
                    .exclude(is_appointment_locked=True)\
                    .update(appointed_by_id=None)

            # re-generate appointments relationships
            # excluding manually created ones (is_appointment_locked)
            self.logger.info(f"Re-creating {len(appointments)} relationships in DB ...")
            for n, appointment in enumerate(appointments, start=1):
                Membership.objects\
                    .filter(id=appointment.appointed_id)\
                    .exclude(is_appointment_locked=True)\
                    .update(appointed_by_id=appointment.appointer_id)

                if n % 1000 == 0:
                    self.logger.info(f"{n}/{len(appointments)}")

        self.logger.info("Stop generating relations")
