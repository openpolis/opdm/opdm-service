import itertools
import operator

from django.db.models import Q
from popolo.models import Classification, Post, Area, Organization
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Script that generates appointments' rules, starting from meta-rules"

    def handle(self, *args, **options):
        """The general handler wraps calls to specific meta-rules handlers

        :param args:
        :param options:
        :return:
        """
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.handle_first()
        self.handle_second()
        self.handle_third()
        self.handle_fourth()
        self.handle_fifth()

    def handle_first(self):
        """Generates appointment rules (`Post.appointed_by`) for the first meta-rule:
        1. Within all organizations of given classifications, identify the appointers and the appointables,
        then create/update the `appointed_by` pointer of every `appointable` to its `appointer`.

        Whenever the appointer is not found, no relation is created.
        The exception of commissari straordinari,
        appointed by the interior minister or region presidents
        must be considered (see meta-rules 1.bis and 1.ter)

        Rule 1.bis: commissari straordinari are appointed by the interior minister political chief
                    (administrative role, not government), in *normal* regions
        Rule 1.ter: commissari straordinari are appointed by the president of the region (special statute)

        Locked rules are left untouched (`is_appointment_locked`).
        :return:
        """

        self.logger.info("Start generating rules for first meta-rule")

        # 1. governments (giunte): responsibles nominates all others,
        #
        classifications = Classification.objects.filter(
            descr__in=[
                'Giunta comunale', 'Giunta provinciale', 'Giunta regionale',
                'Governo della Repubblica', 'Dipartimento/Direzione/Ufficio',
                'Ministero'
            ],
            scheme='FORMA_GIURIDICA_OP'
        )
        interested_posts = Post.objects.filter(
            role_type__classification__in=classifications,
        ).filter(
            Q(role_type__is_appointer=True) | Q(role_type__is_appointable=True)
        ).exclude(
            is_appointment_locked=True
        ).values(
            'id', 'organization',
            'role_type__is_appointer', 'role_type__is_appointable'
        )

        self.update_interested_posts(interested_posts)

        # 1.bis
        p = Post.objects.get(
            organization__name__iexact="ministero dell'interno",
            role__iexact='ministro ministero'
        )
        regioni_statuto_speciale = Area.objects.filter(
            classification='ADM1',
            identifier__in=['02', '04', '06', '19', '20']
        )
        commissari = Post.objects.filter(
            role_type__classification__in=classifications,
            role_type__label__icontains='commissario',
        ).exclude(
            Q(organization__parent__area__in=regioni_statuto_speciale) |
            Q(organization__parent__area__parent__in=regioni_statuto_speciale) |
            Q(organization__parent__area__parent__parent__in=regioni_statuto_speciale)
        ).exclude(
            is_appointment_locked=True
        )
        self.logger.info(f"{commissari.count()} rules to be written for commissari (non reg statuto speciale)")
        commissari.update(appointed_by=p)

        # 1.ter
        regioni_speciali = Area.objects.filter(
            classification='ADM1',
            identifier__in=['02', '04', '06', '19', '20']
        )
        for regione_speciale in regioni_speciali:
            presidente = regione_speciale.\
                organizations.get(classification='Regione').\
                children.get(classification='Giunta regionale').\
                posts.get(role_type__label='Presidente di Regione')

            commissari = Post.objects.filter(
                role_type__classification__in=classifications,
                role_type__label__icontains='commissario',
            ).filter(
                Q(organization__parent__area=regione_speciale) |
                Q(organization__parent__area__parent=regione_speciale) |
                Q(organization__parent__area__parent__parent=regione_speciale)
            ).exclude(
                is_appointment_locked=True
            )
            self.logger.info(f"{commissari.count()} rules to be written for commissari in {regione_speciale}")
            commissari.update(appointed_by=presidente)

        self.logger.info("End generating rules for first meta-rule")

    def handle_second(self):
        """Generates appointment rules (`Post.appointed_by`) for the second meta-rule:
        2. Within all organizations having parents classified as 'Presidenza del Consiglio' or 'Ministero',
        create/update the `appointed_by` pointer of every `appointable` to its `appointer`.

        Whenever the appointer is not found, the appointer is looked for in the parent organization.

        If no appointer is found there, then no relation is created.

        Locked rules are left untouched (`is_appointment_locked`).
        :return:
        """

        self.logger.info("Start generating rules for second meta-rule")

        # 2.1 dipartimenti (children of PDC, Ministero)
        classifications = Classification.objects.filter(
            descr__in=[
                'Presidenza del Consiglio', 'Ministero'
            ],
            scheme='FORMA_GIURIDICA_OP'
        )
        interested_posts = Post.objects.filter(
            organization__parent__classifications__classification__in=classifications,
        ).filter(
            Q(role_type__is_appointer=True) | Q(role_type__is_appointable=True)
        ).exclude(
            is_appointment_locked=True
        ).values(
            'id', 'organization', 'organization__parent',
            'role_type__is_appointer', 'role_type__is_appointable',
        )

        self.update_interested_posts(interested_posts, appointer_lookup='organization__parent')

        self.logger.info("End generating rules for second meta-rule")

    def handle_third(self):
        """Generates appointment rules (`Post.appointed_by`) for the third meta-rule:
        3. Within all organizations having a *supervision* relationship with another organization,
        create/update the `appointed_by` pointer of every `appointable` to its `appointer`.

        Exclude the case of ASL sub-commissari, that are appointed by their commissari, and treated in rule n. 5

        Whenever the appointer is not found, the appointer is looked for in the supervisioning organization.

        If no appointer is found there, then no relation is created.

        Locked rules are left untouched (`is_appointment_locked`).
        :return:
        """
        self.logger.info("Start generating rules for third meta-rule")

        try:
            cl = Classification.objects.get(
                scheme='OP_TIPO_RELAZIONE_ORG',
                code='OT_01',
                descr='Vigilanza'
            )
        except Classification.DoesNotExist:
            self.logger("Could not find Vigilanza Classification for OrganizationRelationship")
            return

        interested_posts = Post.objects.filter(
            organization__from_relationships__classification=cl
        ).filter(
            Q(role_type__is_appointer=True) | Q(role_type__is_appointable=True)
        ).exclude(
            is_appointment_locked=True
        ).exclude(
            role_type__label__icontains='sub commissario',
            role_type__classification__descr="Azienda o ente del servizio sanitario nazionale"
        ).values(
            'id', 'organization', 'organization__from_relationships__source_organization',
            'role_type__is_appointer', 'role_type__is_appointable',
        )

        self.update_interested_posts(
            interested_posts,
            appointer_lookup='organization__from_relationships__source_organization'
        )

        self.logger.info("End generating rules for third meta-rule")

    def handle_fourth(self):
        """Generates appointment rules (`Post.appointed_by`) for the fourth meta-rule:
        4. Within all organizations owned for more than 90% by an organization in
           one of these classifications:
            ['Presidenza del Consiglio','Ministero','Regione','Provincia', 'Comune'],
        the appointer in the owner appoints all appointables in owned.
        Appointers in local institution are the ones with roles in the related Giunta.

        create/update the `appointed_by` pointer of every `appointable` to its `appointer`.

        Whenever the appointer is not found, the appointer is looked for in the supervisioning organization.
        If no appointer is found there, then no relation is created.
        Locked rules are left untouched (`is_appointment_locked`).
        :return:
        """
        self.logger.info("Start generating rules for fourth meta-rule")

        classifications = Classification.objects.filter(
            descr__in=[
                'Presidenza del Consiglio', 'Ministero', 'Regione', 'Provincia', 'Comune'
            ],
            scheme='FORMA_GIURIDICA_OP'
        )

        interested_posts = Post.objects.filter(
            organization__ownerships_as_owned__owner_organization__classifications__classification__in=classifications,
            organization__ownerships_as_owned__percentage__gte=90
        ).filter(
            Q(role_type__is_appointer=True) | Q(role_type__is_appointable=True)
        ).exclude(
            is_appointment_locked=True
        ).values(
            'id', 'organization', 'organization__ownerships_as_owned__owner_organization',
            'role_type__is_appointer', 'role_type__is_appointable',
        )

        self.update_interested_posts(
            interested_posts,
            appointer_lookup='organization__ownerships_as_owned__owner_organization'
        )

        self.logger.info("End generating rules for fourth meta-rule")

    def handle_fifth(self):
        """Generates appointment rules (`Post.appointed_by`) for the fifth meta-rule:

        Within organizations classified as "Azienda o ente del servizio sanitario nazionale"
        create/update the `appointed_by` pointer of every `appointable` to its `appointer`.

        If no appointer is found there, then no relation is created.
        Locked rules are left untouched (`is_appointment_locked`).
        :return:
        """
        self.logger.info("Start generating rules for fifth meta-rule")

        classifications = Classification.objects.filter(
            descr__in=[
                "Azienda o ente del servizio sanitario nazionale"
            ],
            scheme='FORMA_GIURIDICA_OP'
        )

        interested_posts = Post.objects.filter(
            role_type__classification__in=classifications,
            role_type__label__icontains='commissario',
        ).filter(
            Q(role_type__is_appointer=True) | Q(role_type__is_appointable=True)
        ).exclude(
            is_appointment_locked=True
        ).values(
            'id', 'organization',
            'role_type__is_appointer', 'role_type__is_appointable',
        )

        self.update_interested_posts(
            interested_posts,
        )

        self.logger.info("End generating rules for fifth meta-rule")

    def update_interested_posts(self, interested_posts, appointer_lookup=None, log_process_steps=1000):
        """Helper function that
        - groups interested_posts by organization,
        - extracts appointer and apointables,
        - loops over all resulting groups and discover appointers in other organizations:
            - parent organization or
            - supervisioning organization
            - owner organization (more than 90% of ownerhip)

        :param interested_posts:
        :param appointer_lookup:
        :param log_process_steps:
        :return:
        """
        sorted_posts = sorted(
            interested_posts,
            key=operator.itemgetter('organization')
        )
        groups = itertools.groupby(
            sorted_posts,
            key=operator.itemgetter("organization")
        )
        appointments_list = []
        groups_dict = dict((g[0], list(g[1])) for g in groups)
        for org_id, posts in groups_dict.items():
            cdict = {'appointables': []}
            for post in posts:
                if appointer_lookup:
                    cdict.setdefault(appointer_lookup, post.get(appointer_lookup, None))
                if post['role_type__is_appointer']:
                    cdict['appointer'] = post['id']
                if post['role_type__is_appointable']:
                    cdict['appointables'].append(post['id'])
            appointments_list.append(cdict)

        c = sum(map(lambda x: len(x['appointables']), appointments_list))
        self.logger.info(f"{c} rules to be written for {len(appointments_list)} posts")
        for n, post_appointments in enumerate(appointments_list, start=1):

            # clean all those cases where a opst can be appointer and appointable (commissario ASL)
            if (
                'appointer' in post_appointments and
                post_appointments['appointer'] in post_appointments['appointables']
            ):
                post_appointments['appointables'] = [
                    x for x in post_appointments['appointables'] if x != post_appointments['appointer']
                ]

            # extract appointer if not already there, or
            # if appointer is among the appointables (which signals a different
            # use case, like some post who is defined simultaneously as an appointer
            # and an appointable)
            if (
                'appointer' not in post_appointments or
                post_appointments['appointer'] in post_appointments['appointables']
            ):

                if appointer_lookup is None:
                    continue
                try:
                    appointer_org = Organization.objects.get(id=post_appointments[appointer_lookup])
                    if appointer_org.classification.lower() in ['comune', 'provincia', 'regione']:
                        appointer_org = appointer_org.children.get(classification__icontains='giunta')
                    appointer_post = appointer_org.posts.filter(
                        role_type__is_appointer=True
                    ).order_by('role_type__priority').first()
                    post_appointments['appointer'] = appointer_post.id
                except KeyError:
                    self.logger.error(
                        f"Could not find appointer post in {post_appointments} (no {appointer_lookup}). Skipping."
                    )
                    continue
                except Organization.DoesNotExist:
                    self.logger.error(
                        f"Could not find appointer post in {post_appointments} "
                        f"(organization {post_appointments[appointer_lookup]}). Skipping."
                    )
                    continue
                except Post.DoesNotExist:
                    self.logger.error(
                        f"Could not find appointer post in parent organization for {post_appointments}. Skipping."
                    )
                    continue
            Post.objects.\
                filter(id__in=post_appointments['appointables']).\
                update(appointed_by=post_appointments['appointer'])
            if n % log_process_steps == 0:
                self.logger.info(f"{n}/{len(appointments_list)}")
