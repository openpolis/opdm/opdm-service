"""
Openpolis Data Manager service package (backend)
"""
from typing import Optional

__version__ = (2, 0, 0)


def get_version_str() -> str:
    """
    Get the current version of this package as a string.
    :return: a string representing the current version.
    """
    return "v{major}.{minor}.{patch}".format(
        major=__version__[0], minor=__version__[1], patch=__version__[2],
    )


def parse_revision(ref: str = "HEAD") -> Optional[str]:
    """
    Get the current Git revision.

    This is an incomplete Python implementation of command `git rev-parse $ref`.

    :return: the current Git revision as string.
    """
    import re

    try:
        with open(f".git/{ref}") as git_head_file:
            line = git_head_file.readline()

        result = re.search(r"ref: (?P<path>.+)", line)

        if result:
            with open(f".git/{result['path']}") as git_ref_file:
                return git_ref_file.readline().strip()

    except FileNotFoundError:
        return
