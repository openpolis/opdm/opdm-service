from django.db.models.signals import post_save
from popolo.models import Person, Area

from project.core import person_utils
from project.core.person_utils import CFException


def add_cf_to_person(**kwargs):
    codice_fiscale = None
    person: Person = kwargs.get("instance", None)
    if person:
        item = {
            "family_name": person.family_name,
            "given_name": person.given_name,
            "gender": person.gender,
            "birth_date": person.birth_date,
            "birth_location": person.birth_location,
        }
        try:
            codice_fiscale = person_utils.compute_cf(item)
        except CFException:
            codice_fiscale = None

    if codice_fiscale and person:
        person.add_identifier(
            identifier=codice_fiscale, scheme="CF", overwrite_overlapping=True
        )


def correct_birth_location(**kwargs):
    """
    If birth_location_area is known, check and eventually fix birth_location

    """
    person = kwargs.get("instance", None)
    ba = person.birth_location_area
    bl = person.birth_location
    new_bl = None
    if ba:
        if ba.istat_classification == Area.ISTAT_CLASSIFICATIONS.comune:
            new_bl = "{0} ({1})".format(ba.name, ba.parent.identifier)
        if new_bl and new_bl != bl:
            post_save.disconnect(correct_birth_location, sender=Person)
            person.birth_location = new_bl
            person.save()
            post_save.connect(correct_birth_location, sender=Person)


def correct_names(**kwargs):
    """
    If check and eventually fix name and sort_name field, from family_name and given_name

    """
    person = kwargs.get("instance", None)
    new_n = "{0} {1}".format(person.given_name, person.family_name)
    new_sn = "{0} {1}".format(person.family_name, person.given_name).lower()

    post_save.disconnect(correct_birth_location, sender=Person)
    to_save = False
    if new_n != person.name:
        person.name = new_n
        to_save = True
    if new_sn != person.sort_name:
        person.sort_name = new_sn
        to_save = True
    if to_save:
        person.save()
    post_save.connect(correct_birth_location, sender=Person)


def connect():
    """
    Connect all the signals.
    """

    post_save.connect(receiver=add_cf_to_person, sender=Person)
    post_save.connect(receiver=correct_birth_location, sender=Person)
    post_save.connect(receiver=correct_names, sender=Person)
