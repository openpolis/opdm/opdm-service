from rest_framework.exceptions import ValidationError


class Interval:
    def __call__(self, args):
        if (
            isinstance(args, dict)
            and "start_date" in args.keys()
            and "end_date" in args.keys()
        ):
            if (
                args["start_date"]
                and args["end_date"]
                and args["start_date"] > args["end_date"]
            ):
                if "id" in args.keys():
                    message = f"start_date must precede end_date for object with id={args['id']}"
                else:
                    message = "start_date must precede end_date"
                raise ValidationError(message)
