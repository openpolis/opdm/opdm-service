# coding=utf-8
from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django_filters import (
    FilterSet,
    IsoDateTimeFilter,
    ChoiceFilter,
    CharFilter,
)

from project.api_v1.filters.filters import PartialDateFilter


class DateframeableFilterSet(FilterSet):
    active_status = ChoiceFilter(
        choices=(("current", _("Current")), ("past", _("Past"))),
        label=_("Status of activity"),
        help_text=_("Status of the items: current, past or future."),
        method="status_filter",
    )

    active_at = PartialDateFilter(
        field_name="active_at",
        label=_("Active at..."),
        help_text=_(
            "Filters items active at the given date. Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
        method="active_at_filter",
    )

    active_from = PartialDateFilter(
        field_name="active_from",
        label=_("Active since..."),
        help_text=_(
            "Filters items active since the given date. Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
        method="active_from_filter",
    )

    active_to = PartialDateFilter(
        field_name="active_to",
        label=_("Active up to..."),
        help_text=_(
            "Filters items active up to the given date. Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
        method="active_to_filter",
    )

    start_date = PartialDateFilter(
        field_name="start_date",
        lookup_expr="exact",
        help_text=_(
            "Get items having this exact start date. Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
    )

    end_date = PartialDateFilter(
        field_name="end_date",
        lookup_expr="exact",
        help_text=_(
            "Get items having this exact end date. Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
    )

    @staticmethod
    def status_filter(queryset, name, value):
        if value == "alive":
            value = "current"
        if value == "deceased":
            value = "past"
        return getattr(queryset, value)()

    @staticmethod
    def active_at_filter(queryset, name, value):
        return queryset.filter(
            Q(start_date__lte=value) | Q(start_date__isnull=True)
        ).filter(Q(end_date__gt=value) | Q(end_date__isnull=True))

    @staticmethod
    def active_from_filter(queryset, name, value):
        return queryset.filter(Q(start_date__lte=value) | Q(start_date__isnull=True))

    @staticmethod
    def active_to_filter(queryset, name, value):
        return queryset.filter(Q(end_date__gt=value) | Q(end_date__isnull=True))

    @staticmethod
    def updated_after_filter(queryset, name, value):
        return queryset.filter(updated_at__gt=value)

    class Meta:
        model = None
        fields = []


class TimestampableFilterset(FilterSet):
    created_after = IsoDateTimeFilter(
        field_name="created_at",
        lookup_expr="gte",
        label=_("Created after..."),
        help_text=_(
            "Filters items created in the database after the given date and time. "
            "Date and time must be expressed according to ISO 8601."
        ),
    )

    updated_after = IsoDateTimeFilter(
        field_name="updated_at",
        lookup_expr="gte",
        label=_("Updated after..."),
        help_text=_(
            "Filters items updated in the database after the given date and time. "
            "Date and time must be expressed according to ISO 8601."
        ),
    )

    class Meta:
        model = None
        fields = []


class IdentifiersFilterSet(FilterSet):
    identifier_scheme = CharFilter(
        field_name="identifiers__scheme",
        lookup_expr="exact",
        label=_("Identifier scheme"),
        help_text=_(
            "Lookup items with identifiers having this scheme. "
            "See `/identifier_types` for available schemes"
        ),
    )

    identifier_value = CharFilter(
        field_name="identifiers__identifier",
        lookup_expr="exact",
        label=_("Identifier value"),
        help_text=_(
            "Lookup items with identifiers having this value. Usually used together with identifier_scheme."
        ),
    )

    class Meta:
        model = None
        fields = []
