# coding=utf-8
from django import forms
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import ugettext_lazy as _
from django_filters.rest_framework import (
    FilterSet,
    ChoiceFilter,
    CharFilter,
    NumberFilter,
    LookupChoiceFilter,
    BooleanFilter, BaseInFilter,
)

from popolo.models import (
    Area,
    Organization,
    Membership,
    Person,
    Post,
    RoleType,
    Classification,
    KeyEvent,
    Ownership,
    OrganizationRelationship)
from project.akas.models import AKA
from project.api_v1.filters.filters import PartialDateFilter
from project.api_v1.filters.filtersets.generic import (
    DateframeableFilterSet,
    TimestampableFilterset,
    IdentifiersFilterSet,
)


class CharInFilter(BaseInFilter, CharFilter):
    pass


class PersonFilterSet(
    FilterSet, DateframeableFilterSet, TimestampableFilterset, IdentifiersFilterSet
):
    active_status = ChoiceFilter(
        choices=(("alive", _("Alive")), ("deceased", _("Deceased"))),
        label=_("Status"),
        help_text=_("Status of the persons: alive, or deceased."),
        method="status_filter",
    )

    birth_date = PartialDateFilter(
        field_name="birth_date",
        lookup_expr="exact",
        help_text=_(
            "Get persons having this exact birth date. Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
    )

    death_date = PartialDateFilter(
        field_name="death_date",
        lookup_expr="exact",
        help_text=_(
            "Get persons having this exact death date. Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
    )

    given_name = CharFilter(
        field_name="given_name",
        lookup_expr="iexact",
        label=_("Given name"),
        help_text=_("Get persons by given name. Case insensitive."),
    )

    family_name = CharFilter(
        field_name="family_name",
        lookup_expr="iexact",
        label=_("Family name"),
        help_text=_("Get persons by family name. Case insensitive."),
    )

    gender = CharFilter(
        field_name="gender",
        lookup_expr="iexact",
        label=_("Gender"),
        help_text=_("Get persons by gender."),
    )
    gender_not_specified = BooleanFilter(
        field_name="gender",
        lookup_expr="isnull",
        label=_("Gender unknown"),
        help_text=_("Select Yes to get all persons whose gender is unknown"),
    )

    op_id = CharFilter(
        method="opid_filter",
        label=_("Openpolis ID"),
        help_text=_("Get persons by Openpolis ID"),
    )

    cf_id = CharFilter(
        method="cfid_filter",
        label=_("Fiscal code"),
        help_text=_("Get persons by Fiscal Code"),
    )

    classification_id = NumberFilter(
        field_name="classifications__classification_id",
        label=_("Classification"),
        help_text=_(
            "Filter persons by a known Classification object (ID). For example:"
            " 2605 (OPDM_PERSON_LABEL=Politico locale)."
        ),
    )

    contesto = CharFilter(
        method="contesto_filter",
        label=_("Context"),
        help_text=_("Filter members or owners of organizations in a context (BandiCovid, PNRR, ...)"),
    )

    @staticmethod
    def contesto_filter(queryset, name, value):
        if value:
            return queryset.filter(
                memberships__organization__classifications__classification__scheme='CONTESTO_OP',
                memberships__organization__classifications__classification__descr=value
            )
        else:
            return queryset.all()

    @staticmethod
    def cfid_filter(queryset, name, value):
        return queryset.filter(identifiers__scheme="CF", identifiers__identifier=value)

    @staticmethod
    def opid_filter(queryset, name, value):
        return queryset.filter(
            identifiers__scheme="OP_ID", identifiers__identifier=value
        )

    class Meta:
        model = Person
        fields = []


class OrganizationFilterSet(
    FilterSet, DateframeableFilterSet, TimestampableFilterset, IdentifiersFilterSet
):
    area_identifier = CharFilter(
        field_name="area__identifier",
        label=_("Organization's area identifier"),
        help_text=_("Filter organizations by the main identifier of the parent."),
    )

    parent_area_identifier = CharFilter(
        field_name="parent__area__identifier",
        label=_("Parent's area identifier"),
        help_text=_(
            "Filter organizations by the main identifier of the parent's area."
        ),
    )

    identifier = CharFilter(
        field_name="identifier",
        lookup_expr="iexact",
        label=_("Identifier"),
        help_text=_("Get organization by main identifier (Fiscal Code)."),
    )

    min_employees = NumberFilter(
        field_name="economics__employees",
        lookup_expr="gte",
        label=_("Min. employees"),
        help_text=_("Minimum number of employees"),
    )

    max_employees = NumberFilter(
        field_name="economics__employees",
        lookup_expr="lte",
        label=_("Max. employees"),
        help_text=_("Maximum number of employees"),
    )

    min_revenue = NumberFilter(
        field_name="economics__revenue",
        lookup_expr="gte",
        label=_("Min. revenue"),
        help_text=_("Minimum revenue (in euro)"),
    )

    max_revenue = NumberFilter(
        field_name="economics__revenue",
        lookup_expr="lte",
        label=_("Max. revenue"),
        help_text=_("Maximum revenue (in euro)"),
    )

    min_capital_stock = NumberFilter(
        field_name="economics__capital_stock",
        lookup_expr="gte",
        label=_("Min. capital_stock"),
        help_text=_("Minimum capital_stock (in euro)"),
    )

    max_capital_stock = NumberFilter(
        field_name="economics__capital_stock",
        lookup_expr="lte",
        label=_("Max. capital_stock"),
        help_text=_("Maximum capital_stock (in euro)"),
    )

    classification = CharFilter(
        field_name="classification", lookup_expr="iexact", label=_("Main classification")
    )

    forma_giuridica = CharFilter(
        field_name="classification",
        lookup_expr="iexact",
        label=_("Legal form"),
        help_text=_(
            "Filter organizations by the legal form. For example: Comune, Regione, Consiglio Comunale."
        ),
    )

    classification_id = NumberFilter(
        field_name="classifications__classification_id",
        label=_("Classification"),
        help_text=_(
            "Filter organizations by a known Classification object (ID). For example:"
            " 207 (FORMA_GIURIDICA_ISTAT_BDAP=Provincia)."
        ),
    )

    founding_date = PartialDateFilter(
        field_name="founding_date",
        lookup_expr="exact",
        help_text=_(
            "Get organizations having this exact founding_date date. "
            "Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
    )

    dissolution_date = PartialDateFilter(
        field_name="dissolution_date",
        lookup_expr="exact",
        help_text=_(
            "Get organizations having this exact dissolution_date. "
            "Dates can be expressed as: YYYY-MM-DD, YYYY-MM or YYYY."
        ),
    )

    name__icontains = CharFilter(
        field_name="name",
        lookup_expr="icontains",
        label=_("Name contains..."),
        help_text=_(
            "Get organizations containing the specified value in the `name` field. Case insensitive."
        ),
    )

    contesto = CharFilter(
        method="contesto_filter",
        label=_("Context"),
        help_text=_("Get organizations by context"),
    )

    @staticmethod
    def contesto_filter(queryset, name, value):
        if value:
            return queryset.filter(
                classifications__classification__scheme='CONTESTO_OP',
                classifications__classification__descr__iexact=value
            )
        else:
            return queryset.all()

    class Meta:
        model = Organization
        fields = []


class OrganizationRelationshipFilterSet(FilterSet, DateframeableFilterSet, TimestampableFilterset):
    class Meta:
        model = OrganizationRelationship

        fields = [
            "source_organization__id",
            "dest_organization__id",
            "source_organization__name",
            "dest_organization__name",
            "classification",
            "weight", "descr"
        ]


class AreaFilterSet(
    FilterSet, DateframeableFilterSet, TimestampableFilterset, IdentifiersFilterSet
):
    name__iexact = CharFilter(
        field_name="name",
        lookup_expr="iexact",
        label=_("Exact name"),
        help_text=_('Filter areas by exact name. Case insensitive. Example: "Roma"'),
    )

    name__icontains = CharFilter(
        field_name="name",
        lookup_expr="icontains",
        label=_("Case insensitive containment"),
        help_text=_('Filter areas by string in name. Case insensitive. Example: "Piemonte 1"'),
    )

    min_inhabitants = NumberFilter(
        field_name="inhabitants",
        lookup_expr="gte",
        label=_("Min. inhabitants"),
        help_text=_("Minimum number of inhabitants"),
    )

    max_inhabitants = NumberFilter(
        field_name="inhabitants",
        lookup_expr="lte",
        label="Max. inhabitants",
        help_text=_("Maximum number of inhabitants"),
    )

    classification = CharInFilter(
        field_name="classification",
        lookup_expr="in",
        label=_("Classification"),
        help_text=_("Geonames classification: ADM1, ADM2, ADM3, ..."),
    )

    istat_classification = ChoiceFilter(
        field_name="istat_classification",
        choices=Area.ISTAT_CLASSIFICATIONS,
        help_text=_("ISTAT classifications: RIP, REG, PROV, CM, COM"),
    )

    op_id = CharFilter(
        field_name="op_id",
        label=_("Openpolis ID"),
        help_text=_("Get areas by Openpolis ID"),
        method="lookup_by_op_id",
    )

    identifier = CharFilter(
        field_name="identifier",
        lookup_expr="exact",
        label=_("Identifier"),
        help_text=_(
            "Get areas by main identifier. An identifier can be "
            "a cadastral code for municipalities (e.g. H501), "
            "a province acronym for provinces (e.g. RM), "
            "a region code for regions (e.g. 12)."
        ),
    )

    identifier__istartswith = CharFilter(
        field_name="identifier",
        lookup_expr="istartswith",
        label=_("Identifier starts with"),
        help_text=_(
            "Get areas by main identifier's prefix."
        ),
    )

    parent__identifier = CharFilter(
        field_name="parent__identifier",
        label=_("Parent identifier"),
        help_text=_("Filter areas by identifier of the parent. Example: RM."),
    )

    @staticmethod
    def lookup_by_op_id(queryset, name, value):
        return queryset.filter(
            identifiers__scheme="OP_ID", identifiers__identifier=value
        )

    class Meta:
        model = Area
        fields = [
            "name__iexact",
            "name__icontains",
            "identifier__istartswith",
            "parent__identifier",
            "min_inhabitants",
            "max_inhabitants",
            "classification",
            "istat_classification",
        ]


class OwnershipFilterSet(FilterSet, DateframeableFilterSet, TimestampableFilterset):
    class Meta:
        model = Ownership
        fields = [
            "owned_organization__id",
            "owner_organization__id",
            "owner_person__id",
            "owned_organization__name",
            "owner_organization__name",
            "owner_person__name",
        ]


class MembershipFilterSet(FilterSet, DateframeableFilterSet, TimestampableFilterset):
    class Meta:
        model = Membership
        fields = ["label", "role", "organization__id", "person__id"]


class PostFilterSet(FilterSet, DateframeableFilterSet, TimestampableFilterset):
    class Meta:
        model = Post
        fields = ["label", "role", "other_label"]


class ClassificationFilterSet(
    FilterSet, DateframeableFilterSet, TimestampableFilterset
):
    class Meta:
        model = Classification
        fields = ["scheme", "code", "descr"]


class TmpListsFilterSet(
    FilterSet,
):
    organization_id = NumberFilter(
        field_name="organization",
        method="organization_id_filter",
        label=_("Organization"),
        help_text=_("Filter lists in memberships of given organization (ID), and its children"),
    )

    @staticmethod
    def organization_id_filter(queryset, name, value):
        try:
            org = Organization.objects.get(pk=value)
        except Organization.DoesNotExist:
            # just return an empty queryset
            return queryset.none()

        return queryset.filter(organization__id=org.id) | queryset.filter(organization__parent__id=org.id)

    class Meta:
        model = Membership
        fields = ["electoral_event_id", "organization_id"]


class RoleTypeFilterSet(FilterSet):
    forma_giuridica = CharFilter(
        field_name="classification__descr",
        lookup_expr="iexact",
        label=_("Legal form"),
        help_text=_(
            "Filter role types for given legal form classification: Giunta regionale, Consiglio regionale, ..."
        ),
    )

    organization = NumberFilter(
        field_name="organization",
        method="organization_id_filter",
        label=_("Organization"),
        help_text=_("Filter role types for given organization (ID):"),
    )

    label = LookupChoiceFilter(
        field_name="label",
        field_class=forms.CharField,
        lookup_choices=["iexact", "icontains"],
        label=_("Label"),
        help_text=_(
            "Filter role types by label: Assessore Regionale, Consigliere regionale, ..."
        ),
    )

    other_label = LookupChoiceFilter(
        field_name="other label",
        field_class=forms.CharField,
        lookup_choices=["iexact", "icontains"],
        label=_("Other Label"),
        help_text=_(
            "Filter role types by other label: Amm. Del., Amm. Unico, Dir. Gen."
        ),
    )

    @staticmethod
    def organization_id_filter(queryset, name, value):
        try:
            org = Organization.objects.get(pk=value)
        except Organization.DoesNotExist:
            return queryset.filter(classification__descr="NONEXISTING")

        return queryset.filter(classification__descr__iexact=org.classification)

    class Meta:
        model = RoleType
        fields = [
            "label",
            "other_label",
            "classification",
            "forma_giuridica",
            "organization",
        ]


class AKAFilterSet(FilterSet):
    search_given_name = CharFilter(
        field_name="search_given_name",
        method="search_given_name_filter",
        label=_("Given name searched"),
        help_text=_("Get AKAs through given name searched"),
    )

    search_family_name = CharFilter(
        field_name="search_family_name",
        method="search_family_name_filter",
        label=_("Family name searched"),
        help_text=_("Get AKAs through family name searched"),
    )

    op_context = CharFilter(
        field_name="context",
        method="context_filter",
        label=_("Context filter"),
        help_text=_(
            "Get AKAs through operations context (regioni|metro|province|comuni)"
        ),
    )

    loc_desc = CharFilter(
        field_name="loc_desc",
        method="loc_desc_filter",
        label=_("Location filter"),
        help_text=_("Get AKAs with this location"),
    )

    person_id = CharFilter(
        field_name="person_id",
        method="person_id_filter",
        label=_("Person ID filter"),
        help_text=_("Get solved AKAs pointing to the person having the ID"),
    )

    class Meta:
        model = AKA
        fields = [
            "is_resolved",
            "search_given_name",
            "search_family_name",
            "op_context",
            "loc_desc",
        ]

    @staticmethod
    def search_given_name_filter(queryset, name, value):
        return queryset.filter(search_params__given_name__iexact=value)

    @staticmethod
    def search_family_name_filter(queryset, name, value):
        return queryset.filter(search_params__family_name__iexact=value)

    @staticmethod
    def context_filter(queryset, name, value):
        return queryset.filter(loader_context__context__iexact=value)

    @staticmethod
    def loc_desc_filter(queryset, name, value):
        return queryset.filter(loader_context__item__loc_desc__iexact=value)

    @staticmethod
    def person_id_filter(queryset, name, value):
        return queryset.filter(
            content_type_id=ContentType.objects.get(model="person").id, object_id=value
        )


class KeyEventFilterSet(FilterSet):
    name = CharFilter(
        field_name="name",
        method="name_filter",
        label=_("Name"),
        help_text=_("Filter key events by name"),
    )

    identifier = CharFilter(
        field_name="identifier",
        method="identifier_filter",
        label=_("Identifier"),
        help_text=_("Filter key events by identifier"),
    )

    event_type = ChoiceFilter(
        field_name="event_type",
        choices=KeyEvent.EVENT_TYPES,
        label=_("Event type"),
        help_text=_(
            "Filter key events by type: ELE: election, XAD: external adm , ITL: italian leg., EUL: eu leg."
        ),
    )

    @staticmethod
    def name_filter(queryset, name, value):
        return queryset.filter(name__icontains=value)

    @staticmethod
    def identifier_filter(queryset, name, value):
        return queryset.filter(identifier__iexact=value)

    class Meta:
        model = KeyEvent
        fields = ["name", "event_type", "identifier"]
