# coding=utf-8
from rest_framework.schemas import AutoSchema


class FiltersInListOnlySchema(AutoSchema):
    """Customize AutoSchema, so that filters are only visible in 'list' action

    An `actions` argument with the whitelisted action (as a list), can
    be passed to the constructor, so that filters will also appear in
    the coreapi schema of those actions.

    """

    def __init__(self, **kwargs):
        """
        Parameters:

        * `manual_fields`: list of `coreapi.Field` instances that
            will be added to auto-generated fields, overwriting on `Field.name`
        * `actions`: list of whitelisted actions (appended to `list`)
        """
        self.actions = kwargs.pop("actions", [])
        super(FiltersInListOnlySchema, self).__init__(**kwargs)

    def _allows_filters(self, path, method):
        whitelisted_actions = ["list"]
        whitelisted_actions.extend(self.actions)
        if hasattr(self.view, "action"):
            return self.view.action in whitelisted_actions
        return super(FiltersInListOnlySchema, self)._allows_filters(path, method)
