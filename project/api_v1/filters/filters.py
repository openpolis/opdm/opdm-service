# coding=utf-8
from django.db.models import F
from django_filters import DateFilter
from rest_framework.filters import OrderingFilter

from project.api_v1.forms.fields import PartialDateField


class PartialDateFilter(DateFilter):
    field_class = PartialDateField


class NullsLastOrderingFilter(OrderingFilter):
    def filter_queryset(self, request, queryset, view):
        ordering = self.get_ordering(request, queryset, view)

        fields = [
            F(o[1:]).desc(nulls_last=True)
            if (o[0] == "-")
            else F(o).asc(nulls_last=True)
            for o in ordering
        ]
        return queryset.order_by(*fields)
