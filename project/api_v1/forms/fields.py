from datetime import datetime

from django.forms import DateField
from django.utils.encoding import force_str


class PartialDateField(DateField):
    input_formats = ["%Y-%m-%d", "%Y-%m", "%Y"]

    def strptime(self, value, format_):
        if value.lower() in ["now", "today"]:
            return datetime.today().date()
        else:
            return datetime.strptime(force_str(value), format_).date()
