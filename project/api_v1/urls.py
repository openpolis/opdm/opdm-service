from django.conf.urls import include
from django.urls import path

from project.api_v1 import routers
from project.api_v1.views import (
    ContactTypesChoicesView,
    NameTypesChoicesView,
    IstatClassificationsChoicesView,
)

router = routers.register(routers.CustomAPIv1Router, trailing_slash=False)

patterns = [
    path("", include(router.urls)),
    path("contact_types_choices", ContactTypesChoicesView.as_view()),
    path("name_types_choices", NameTypesChoicesView.as_view()),
    path("istat_classifications_choices", IstatClassificationsChoicesView.as_view(),),
]


urlpatterns = [
    path("v1/", include((patterns, "api_v1"), namespace="mappepotere_v1")),
]
