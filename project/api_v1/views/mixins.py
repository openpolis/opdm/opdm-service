from django.contrib.contenttypes.models import ContentType
from drf_rw_serializers import viewsets as rw_viewsets
from rest_framework.decorators import action
from rest_framework.response import Response


class IdentifierTypesMixin(rw_viewsets.ModelViewSet):
    """Adds the `identifier_types` extra action to the views.
    """

    @action(detail=False, filter_backends=())
    def identifier_types(self, request, **kwargs):
        """Gets the list of all distinct Identifier types used so far.

        :return:    A paginated Response, containing the list of all used
                    identifier values as of now.

        :rtype:     Response

        """
        queryset = (
            ContentType.objects.get(model=self.basename)
            .identifier_set.values_list("scheme", flat=True)
            .order_by("scheme")
            .distinct()
        )
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page:
            return self.get_paginated_response(queryset)
        return Response(queryset)


class ClassificationTypesMixin(rw_viewsets.ModelViewSet):
    """Adds the `classification_types` extra action to the views
    """

    @action(detail=False, filter_backends=())
    def classification_types(self, request, *args, **kwargs):
        """Gets the list of all distinct Classification types used so far.

        :return:    A paginated Response, containing the list of all used
                    classification values as of now.

        :rtype:     Response

        """
        queryset = (
            ContentType.objects.get(model=self.basename)
            .classificationrel_set.values_list("classification__scheme", flat=True)
            .order_by("classification__scheme")
            .distinct()
        )
        queryset = self.filter_queryset(queryset)
        page = self.paginate_queryset(queryset)
        if page:
            return self.get_paginated_response(queryset)
        return Response(queryset)


class BulkPartialUpdateMixin(rw_viewsets.ModelViewSet):
    def partial_bulk_update(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        write_serializer = self.get_write_serializer(
            queryset, data=request.data, many=True, partial=True
        )
        write_serializer.is_valid(raise_exception=True)
        self.perform_update(write_serializer)
        return super(BulkPartialUpdateMixin, self).list(request, *args, **kwargs)
