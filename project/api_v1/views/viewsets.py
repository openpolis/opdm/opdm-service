from datetime import datetime

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.db.models import Count, F, Q
from django_filters import rest_framework as extra_filters
from drf_rw_serializers import viewsets as rw_viewsets
from drf_rw_serializers.mixins import (
    UpdateModelMixin,
    ListModelMixin,
    RetrieveModelMixin,
)
from drf_yasg.utils import swagger_auto_schema
from popolo.models import (
    Area,
    Organization,
    Membership,
    Person,
    Post,
    Classification,
    OriginalProfession,
    OriginalEducationLevel,
    Profession,
    EducationLevel,
    RoleType,
    KeyEvent,
    Ownership,
    OrganizationRelationship)
from rest_framework import filters
from rest_framework.decorators import action
from rest_framework.exceptions import APIException, MethodNotAllowed
from rest_framework.response import Response

from api_v1.views.DetailActionsMixin import DetailActionsMixin
from project.akas.models import AKA
from project.api_v1.filters import (
    AreaFilterSet,
    FiltersInListOnlySchema,
    OrganizationFilterSet,
    MembershipFilterSet,
    PersonFilterSet,
    NullsLastOrderingFilter,
    PostFilterSet,
    RoleTypeFilterSet,
    ClassificationFilterSet,
    AKAFilterSet,
    KeyEventFilterSet,
)
from project.api_v1.filters.filtersets.filterset import OwnershipFilterSet, OrganizationRelationshipFilterSet, \
    TmpListsFilterSet
from project.api_v1.serializers import (
    AreaListResultSerializer,
    AreaInlineSerializer,
    AreaSerializer,
    AreaWriteSerializer,
    OrganizationSerializer,
    OrganizationListResultSerializer,
    OrganizationWriteSerializer,
    PersonListResultSerializer,
    PersonSerializer,
    PersonWriteSerializer,
    PersonSearchResultSerializer,
    MembershipListResultSerializer,
    MembershipSerializer,
    MembershipWriteSerializer,
    PostListResultSerializer,
    FormerChildrenSerializer,
    ClassificationInlineSerializer,
    PostSerializer,
    PostWriteSerializer,
    RoleTypeSerializer,
    RoleTypeWriteSerializer,
    KeyEventSerializer,
    AKASerializer,
    MembershipInlineSerializer,
    PostInlineSerializer,
)
from project.api_v1.serializers import (
    OriginalProfessionSerializer,
    OriginalProfessionWriteSerializer,
    OriginalEducationLevelSerializer,
    OriginalEducationLevelWriteSerializer,
    ProfessionSerializer,
    EducationLevelSerializer,
    ClassificationSerializer,
    ClassificationWriteSerializer,
    OwnershipWriteSerializer,
    OwnershipSerializer,
    OrganizationSearchResultSerializer,
    OwnershipListResultSerializer,
)
from project.api_v1.serializers.serializers import (
    OrganizationRelationshipSerializer, OrganizationRelationshipWriteSerializer)
from project.api_v1.views import ClassificationTypesMixin
from project.api_v1.views import IdentifierTypesMixin
from project.api_v1.views.mixins import BulkPartialUpdateMixin
from project.core import labels_utils
from project.core import person_utils
from project.core import search_utils
from project.core.exceptions import SearchException
from project.core.exceptions import (
    UnprocessableEntitytAPIException,
    InternalServerErrorAPIException,
)


class AreaViewSet(IdentifierTypesMixin, DetailActionsMixin, rw_viewsets.ModelViewSet):
    """
    A ViewsSet for viewing and editing Area resources.

    list:           Return a list of all the existing Area resources.
    retrieve:       Return the given Area resource.
    create:         Create a new Area resource.
    update:         Update the given Area resource.
    partial_update: Update the given Area resource.
    delete:         Delete the given Area resource.
    """
    def get_view_description(self, html=False):
        if hasattr(self, 'action') and self.action == 'list':
            return "Return a list of all the existing Area resources"
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return "Return the given Area resource"
        else:
            return super().get_view_description()

    queryset = (
        Area.objects.all()
        .select_related("parent",)
        .prefetch_related("other_names", "identifiers", "sources", "links")
    )
    search_fields = ("name",)
    ordering_fields = (
        "inhabitants",
        "name",
        "start_date",
        "end_date",
        "created_at",
        "updated_at",
    )
    ordering = ("-inhabitants", "name")
    filter_backends = (
        filters.SearchFilter,
        NullsLastOrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = AreaFilterSet

    # use a custom AutoSchema class to show filters only in list action
    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = AreaSerializer
    write_serializer_class = AreaWriteSerializer

    def list(self, request, *args, **kwargs):
        self.read_serializer_class = AreaListResultSerializer
        return super(AreaViewSet, self).list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        self.queryset = (
            Area.objects.all()
            .select_related("parent",)
            .prefetch_related(
                "related_areas",
                "new_places",
                "other_names",
                "identifiers",
                "sources",
                "links",
            )
        )
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(request_body=AreaWriteSerializer)
    def create(self, request, *args, **kwargs):
        return super(AreaViewSet, self).create(request, *args, **kwargs)

    @swagger_auto_schema(request_body=AreaWriteSerializer)
    def update(self, request, *args, **kwargs):
        return super(AreaViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=AreaWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        return super(AreaViewSet, self).partial_update(request, *args, **kwargs)

    @action(detail=True, serializer_class=OrganizationListResultSerializer)
    def organizations(self, request, pk=None, **kwargs):
        """ Shows all organizations of a given area"""
        instance = self.get_object_no_filter()
        items = instance.organizations.all().order_by("id")
        page = self.paginate_queryset(items)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    @action(detail=True)
    def children(self, request, pk=None, **kwargs):
        """ Shows all areas children of a given one """
        self.serializer_class = AreaListResultSerializer
        instance = self.get_object_no_filter()
        items = instance.children.all().order_by("id")
        page = self.paginate_queryset(items)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    @action(detail=True)
    def former_children(self, request, pk=None, **kwargs):
        """ Shows an area's former children """
        self.serializer_class = FormerChildrenSerializer
        instance = self.get_object_no_filter()
        items = (
            instance.to_relationships.filter(classification="FIP")
            .select_related("source_area")
            .prefetch_related("source_area__identifiers")
        )
        page = self.paginate_queryset(items)
        serializer = self.get_serializer(page, many=True)
        return self.get_paginated_response(serializer.data)

    @action(detail=False)
    def istat_classifications(self, request, **kwargs):
        """ Returns all available values for ISTAT classifications """
        items = Area.ISTAT_CLASSIFICATIONS
        return Response(items)

    @action(detail=False)
    def classifications(self, request, **kwargs):
        """ Returs all used values for geonames classifications """
        items = Area.objects.values_list("classification", flat=True).distinct()
        return Response(items)

    @action(detail=False)
    def comuni_with_prov_and_istat_identifiers(self, request, **kwargs):
        """ Returs all comuni, with prov and ISTAT identifiers """
        d = request.query_params.get(
            "date", datetime.strftime(datetime.now(), "%Y-%m-%d")
        )
        prov = request.query_params.get("prov_identifier", None)
        prov_id = request.query_params.get("prov_id", None)
        prov_name = request.query_params.get("prov_name", None)
        name = request.query_params.get("name", None)
        id = request.query_params.get("id", None)
        items = (
            {
                "id": a.id,
                "name": a.name,
                "prov_id": a.prov_id,
                "prov_name": a.prov_name,
                "prov_identifier": a.prov_identifier,
                "istat_identifier": a.istat_identifier,
            }
            for a in Area.historic_objects.comuni_with_prov_and_istat_identifiers(d)
        )
        if prov:
            items = filter(lambda i: i["prov_identifier"] == prov, items)
        if prov_id:
            items = filter(lambda i: i["prov_id"] == prov_id, items)
        if prov_name:
            items = filter(lambda i: i["prov_name"] == prov_name, items)
        if name:
            items = filter(lambda i: i["name"] == name, items)
        if id:
            items = filter(lambda i: i["id"] == id, items)

        return Response(items)

    @action(
        detail=False, serializer_class=AreaInlineSerializer, search_fields=("^name",)
    )
    def autocompleter(self, request, **kwargs):
        """ Values for area auto-completer. The same parameters of the ``list`` section apply"""
        self.queryset = Area.objects.all().annotate(
            parent_identifier=F("parent__identifier")
        )
        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)


class OrganizationRelationshipViewSet(
    rw_viewsets.ModelViewSet
):
    """
    A ViewsSet for viewing and editing OrganizationRelationship resources.

    list:           Return a list of all the existing OrganizationRelationship resources.
    retrieve:       Return the given OrganizationRelationship resource.
    create:         Create a new OrganizationRelationship resource.
    update:         Update the given OrganizationRelationship resource.
    partial_update: Update the given OrganizationRelationship resource.
    delete:         Delete the given OrganizationRelationship resource.
    """
    def get_view_description(self, html=False):
        if hasattr(self, 'action') and self.action == 'list':
            return "Return a list of all the existing OrganizationRelationship resources"
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return "Return the given OrganizationRelationship resource"
        else:
            return super().get_view_description()

    queryset = OrganizationRelationship.objects.all().prefetch_related(
        "source_organization", "dest_organization", "classification"
    )

    search_fields = (
        "source_organization__name", "source_organization__identifier",
        "dest_organization__name", "dest_organization__identifier")
    ordering_fields = (
        "source_organization",
        "dest_organization",
    )
    ordering = ("-updated_at",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = OrganizationRelationshipFilterSet

    # use a custom AutoSchema class
    # to show filters only in list action
    schema = FiltersInListOnlySchema()

    serializer_class = OrganizationRelationshipSerializer
    read_serializer_class = OrganizationRelationshipSerializer
    write_serializer_class = OrganizationRelationshipWriteSerializer


class OrganizationViewSet(
    IdentifierTypesMixin, ClassificationTypesMixin, DetailActionsMixin, rw_viewsets.ModelViewSet
):
    """
    A ViewsSet for viewing and editing Organization resources.

    list:           Return a list of all the existing Organization resources.
    retrieve:       Return the given Organization resource.
    create:         Create a new Organization resource.
    update:         Update the given Organization resource.
    partial_update: Update the given Organization resource.
    delete:         Delete the given Organization resource.
    """

    queryset = (
        Organization.objects.all()
        .select_related("area", "parent",)
        .prefetch_related(
            "other_names",
            "identifiers",
            "sources",
            "links",
            "children",
            "classifications__classification",
            "memberships",
            "economics",
        )
    )
    search_fields = ("name", "abstract", "description")
    ordering_fields = (
        "name",
        "founding_date",
        "dissolution_date",
        "created_at",
        "updated_at",
        "area__inhabitants",
        "economics__employees",
        "economics__revenue",
        "economics__capital_stock",
    )
    ordering = ("-updated_at",)
    filter_backends = (
        filters.SearchFilter,
        NullsLastOrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = OrganizationFilterSet

    # use a custom AutoSchema class
    # to show filters only in list and memberships action
    schema = FiltersInListOnlySchema(actions=["memberships"])

    read_serializer_class = serializer_class = OrganizationSerializer
    write_serializer_class = OrganizationWriteSerializer

    def list(self, request, *args, **kwargs):
        self.read_serializer_class = OrganizationListResultSerializer
        return super(OrganizationViewSet, self).list(request, *args, **kwargs)

    @swagger_auto_schema(request_body=OrganizationWriteSerializer)
    def create(self, request, *args, **kwargs):
        return super(OrganizationViewSet, self).create(request, *args, **kwargs)

    @swagger_auto_schema(request_body=OrganizationWriteSerializer)
    def update(self, request, *args, **kwargs):
        return super(OrganizationViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=OrganizationWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        return super(OrganizationViewSet, self).partial_update(request, *args, **kwargs)

    @action(
        detail=False,
        queryset=Classification.objects.all(),
        serializer_class=ClassificationInlineSerializer,
        ordering=("scheme", "code"),
        ordering_fields=("scheme", "code", "descr"),
        search_fields=("scheme", "code", "descr"),
        filter_backends=(
            filters.SearchFilter,
            NullsLastOrderingFilter,
            extra_filters.DjangoFilterBackend,
        ),
        filter_class=ClassificationFilterSet,
    )
    def classifications(self, request, *args, **kwargs):
        """ All Organization's classifications """
        ct = ContentType.objects.get(model="organization")
        self.queryset = (
            Classification.objects.filter(related_objects__content_type_id=ct.id)
            .values("scheme", "code", "descr", "id")
            .distinct()
        )

        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)

    @action(detail=True)
    def ownerships_as_owner(self, request, pk=None, **kwargs):
        """
        Shows all organizations owned by the given `Organization`.

        The usual filters for ownerships can be used, for example:

            /organizations/{id}/ownerships_as_owner?status=current

        will only show **current owned organizations** for the organization.

        """
        instance = self.get_object_no_filter()

        self.serializer_class = OwnershipListResultSerializer

        ownership_viewset = OwnershipViewSet(request=request)
        self.search_fields = ownership_viewset.search_fields
        self.ordering_fields = ownership_viewset.ordering_fields
        self.filter_backends = ownership_viewset.filter_backends
        self.filter_class = ownership_viewset.filter_class

        self.queryset = (
            instance.ownerships.all()
            .prefetch_related("owned_organization", "owner_organization")
            .order_by(*self.ordering_fields)
        )
        self.queryset = ownership_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @action(detail=True)
    def ownerships_as_owned(self, request, pk=None, **kwargs):
        """
        Shows all organizations owning the given `Organization`.

        The usual filters for ownerships can be used, for example:

            /organizations/{id}/ownerships_as_owned?status=current

        will only show **current owning organizations** for the organization.

        """
        instance = self.get_object_no_filter()

        self.serializer_class = OwnershipListResultSerializer

        ownership_viewset = OwnershipViewSet(request=request)
        self.search_fields = ownership_viewset.search_fields
        self.ordering_fields = ownership_viewset.ordering_fields
        self.filter_backends = ownership_viewset.filter_backends
        self.filter_class = ownership_viewset.filter_class

        self.queryset = (
            instance.ownerships_as_owned.all()
            .prefetch_related("owned_organization", "owner_organization")
            .order_by(*self.ordering_fields)
        )
        self.queryset = ownership_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @action(detail=True)
    def memberships(self, request, pk=None, **kwargs):
        """
        Shows all memberships belonging to given `Organization`.
        The usual filters for memberships can be used, for example:

            /organizations/{id}/memberships?status=current

        will only show **current memberships** in the organization.

        """
        instance = self.get_object_no_filter()

        self.serializer_class = MembershipListResultSerializer

        membership_viewset = MembershipViewSet(request=request)
        self.search_fields = membership_viewset.search_fields
        self.ordering_fields = membership_viewset.ordering_fields
        self.filter_backends = membership_viewset.filter_backends
        self.filter_class = membership_viewset.filter_class

        self.queryset = (
            instance.memberships.all()
            .prefetch_related("person", "organization", "post", "area")
            .order_by(*self.ordering_fields)
        )
        self.queryset = membership_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @action(detail=True)
    def n_memberships_by_dates(self, request, pk=None, **kwargs):
        """ Shows numbers of memberships in this organization
        for start and end dates.

        This is internally used to detect starting and ending dates
        of *legislatures* and *governments*.
        """
        instance = self.get_object_no_filter()

        start_date_groups = (
            instance.memberships.values("start_date")
            .annotate(n=Count("pk"))
            .order_by("-n")
        )

        end_date_groups = (
            instance.memberships.values("end_date")
            .annotate(n=Count("pk"))
            .order_by("-n")
        )

        resp = {
            "start_date_groups": start_date_groups,
            "end_date_groups": end_date_groups,
        }

        return Response(resp)

    @action(detail=True)
    def posts(self, request, pk=None, **kwargs):
        """
        Shows all posts belonging to given `Organization`.
        The usual filters for posts can be used, for example:

            /organizations/{id}/posts?status=current

        will only show **current posts** in the organization.

        """
        instance = self.get_object_no_filter()

        self.serializer_class = PostListResultSerializer

        post_viewset = PostViewSet(request=request)
        self.search_fields = post_viewset.search_fields
        self.ordering_fields = post_viewset.ordering_fields
        self.filter_backends = post_viewset.filter_backends
        self.filter_class = post_viewset.filter_class

        self.queryset = (
            instance.posts.all()
            .prefetch_related("area")
            .order_by(*self.ordering_fields)
        )
        self.queryset = post_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @action(detail=True)
    def relations_to_organizations(self, request, pk=None, **kwargs):
        """
        Shows all relations with other organization belonging to a given `Organization`.
        The relations can be filtered by scheme and other parameters:

            /organizations/{id}/to_relations?status=current

        will only show **current relations** of the organization.

        """
        instance = self.get_object_no_filter()

        or_viewset = OrganizationRelationshipViewSet(request=request)
        self.search_fields = or_viewset.search_fields
        self.ordering_fields = or_viewset.ordering_fields
        self.filter_backends = or_viewset.filter_backends
        self.filter_class = OrganizationRelationshipFilterSet
        self.serializer_class = OrganizationRelationshipSerializer

        self.queryset = (
            instance.to_relationships.all()
            .order_by(*self.ordering_fields)
        )
        self.queryset = or_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @action(detail=True)
    def relations_from_organizations(self, request, pk=None, **kwargs):
        """
        Shows all relations coming from other organizations, belonging to a given `Organization`.
        The relations can be filtered by scheme and other parameters:

            /organizations/{id}/from_relations?status=current

        will only show **current relations** of the organization.

        """
        instance = self.get_object_no_filter()

        or_viewset = OrganizationRelationshipViewSet(request=request)
        self.search_fields = or_viewset.search_fields
        self.ordering_fields = or_viewset.ordering_fields
        self.filter_backends = or_viewset.filter_backends
        self.filter_class = OrganizationRelationshipFilterSet
        self.serializer_class = OrganizationRelationshipSerializer

        self.queryset = (
            instance.from_relationships.all()
            .order_by(*self.ordering_fields)
        )
        self.queryset = or_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @action(
        detail=False,
        serializer_class=OrganizationSearchResultSerializer,
        filter_backends=(),
    )
    def search(self, request):
        """Returns results of search with given parameters

        :param request: the current request
        :return:
        """

        name = request.GET.get("name", None)
        identifier = request.GET.get("identifier", None)

        if name is None and identifier is None:
            raise APIException(
                "at least one between name or identifier field must be passed "
                "as query string parameter for {0}".format(request.GET),
                code="wrong_data",
            )

        # uses api_v1.core.search_utils.search  to query the solr index
        # and builds the results set
        try:
            self.queryset = search_utils.search_organization(name, identifier)
        except SearchException as e:
            raise APIException("{0} for {1}".format(str(e), request), code="wrong_data")

        # serialize the queryset and return the response
        serializer = self.get_serializer(self.get_queryset(), many=True)
        response = {"count": len(self.queryset), "results": serializer.data}

        return Response(response)


class PersonViewSet(IdentifierTypesMixin, ClassificationTypesMixin, rw_viewsets.ModelViewSet):
    """
    A ViewsSet for viewing and editing Person resources.

    list: Return a list of all the existing Person resources.
    retrieve: Return the given Person resource.
    create: Create a new Person resource.
    update: Update the given Person resource.
    partial_update: Update the given Person resource.
    delete: Delete the given Person resource.
    """
    def get_view_description(self, html=False):
        if hasattr(self, 'action') and self.action == 'list':
            return "Return a list of all the existing Person resources"
        elif hasattr(self, 'action') and self.action == 'retrieve':
            return "Return the given Person resource"
        else:
            return super().get_view_description()

    queryset = (
        Person.objects.all()
        .select_related(
            "birth_location_area",
            "profession",
            "original_profession",
            "education_level",
            "original_education_level",
        )
        .prefetch_related(
            "other_names",
            "identifiers",
            "contact_details",
            "related_persons",
            "links",
            "sources",
            "memberships",
            "ownerships",
            "classifications__classification",
        )
    )
    search_fields = ("name",)
    ordering_fields = (
        "name",
        "family_name",
        "given_name",
        "additional_name",
        "patronymic_name",
        "sort_name",
        "birth_location",
        "birth_location_area__name",
        "birth_date",
        "death_date",
        "created_at",
        "updated_at",
    )
    ordering = ("sort_name",)
    filter_backends = (
        filters.SearchFilter,
        NullsLastOrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = PersonFilterSet

    # use a custom AutoSchema class
    # to show filters only in list and memberships action
    schema = FiltersInListOnlySchema(actions=["memberships"])

    read_serializer_class = serializer_class = PersonSerializer
    write_serializer_class = PersonWriteSerializer

    def list(self, request, *args, **kwargs):
        self.read_serializer_class = PersonListResultSerializer
        return super(PersonViewSet, self).list(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        self.queryset = self.queryset
        return super().retrieve(request, *args, **kwargs)

    @swagger_auto_schema(request_body=PersonWriteSerializer)
    def create(self, request, *args, **kwargs):
        return super(PersonViewSet, self).create(request, *args, **kwargs)

    @swagger_auto_schema(request_body=PersonWriteSerializer)
    def update(self, request, *args, **kwargs):
        return super(PersonViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=PersonWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        return super(PersonViewSet, self).partial_update(request, *args, **kwargs)

    @action(detail=False)
    def professions(self, request, *args, **kwargs):
        # TODO: implement endpoint returning a list of all possible professions
        return Response()

    @action(detail=False)
    def education_levels(self, request, *args, **kwargs):
        # TODO: implement endpoint returning a list of all possible education_levels
        return Response()

    @action(
        detail=False,
        queryset=Classification.objects.all(),
        serializer_class=ClassificationInlineSerializer,
        ordering=("scheme", "code"),
        ordering_fields=("scheme", "code", "descr"),
        search_fields=("scheme", "code", "descr"),
        filter_backends=(
            filters.SearchFilter,
            NullsLastOrderingFilter,
            extra_filters.DjangoFilterBackend,
        ),
        filter_class=ClassificationFilterSet,
    )
    def classifications(self, request, *args, **kwargs):
        """ All Persons' classifications """
        ct = ContentType.objects.get(model="person")
        self.queryset = (
            Classification.objects.filter(related_objects__content_type_id=ct.id)
            .values("scheme", "code", "descr", "id")
            .distinct()
        )

        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)

    @action(
        detail=False, serializer_class=PersonSearchResultSerializer, filter_backends=()
    )
    def search(self, request, *args, **kwargs):
        """Returns results of search with given parameters

        :param request: the current request
        :return:
        """

        family_name = request.GET.get("family_name", None)
        given_name = request.GET.get("given_name", None)
        birth_date = request.GET.get("birth_date", None)
        birth_location = request.GET.get("birth_location", None)
        birth_location_area = request.GET.get("birth_location_area", None)

        if family_name is None:
            raise APIException(
                "family_name field must be passed as query string parameter for {0}".format(
                    request.GET
                ),
                code="wrong_data",
            )
        if given_name is None:
            raise APIException(
                "given_name field must be passed as query string parameter for {0}".format(
                    request.GET
                ),
                code="wrong_data",
            )

        # uses api_v1.core.search_utils.search  to query the solr index
        # and builds the results set
        try:
            self.queryset = search_utils.search_person_by_anagraphics(
                family_name, given_name, birth_date, birth_location, birth_location_area
            )
        except SearchException as e:
            raise APIException("{0} for {1}".format(str(e), request), code="wrong_data")
        else:
            # filter out results below threshold,
            # keep first 30 results
            # return number of results below threshold and number of results within threshold
            n_low = len(
                [
                    res
                    for res in self.queryset
                    if res.score < settings.AKA_LO_THRESHOLD and res.score > 0
                ]
            )
            self.queryset = [
                res
                for res in self.queryset
                if res.score >= settings.AKA_LO_THRESHOLD or res.score == 0.0
            ][:30]

            if len(self.queryset) >= 1:
                # check if more than one results above thresholds
                hi_res = [
                    res
                    for res in self.queryset
                    if res.score > settings.AKA_HI_THRESHOLD or res.score == 0.0
                ]
                if len(hi_res) > 0:
                    # if at least one result, then return only those
                    self.queryset = hi_res

        # serialize the queryset and return the response
        serializer = self.get_serializer(self.get_queryset(), many=True)
        response = {
            "count": len(self.queryset),
            "results": serializer.data,
            "n_discarded": n_low,
        }

        return Response(response)


class OwnershipViewSet(rw_viewsets.ModelViewSet):
    """
    A ViewsSet for viewing and editing Ownership resources.

    list:
    Return a list of all the existing Ownership resources.

    retrieve:
    Return the given Ownership resource.

    create:
    Create a new Ownership resource.

    update:
    Update the given Ownership resource.

    partial_update:
    Update the given Ownership resource.

    delete:
    Delete the given Ownership resource.
    """

    queryset = (
        Ownership.objects.all()
        .select_related("owned_organization", "owner_organization", "owner_person")
        .prefetch_related("sources")
    )

    search_fields = ("owned_organization__name", "owner_organization__name")
    ordering_fields = (
        "owned_organization__name",
        "owner_organization__name",
        "percentage",
        "start_date",
        "end_date",
        "created_at",
        "updated_at",
    )
    ordering = ("-updated_at",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = OwnershipFilterSet

    # use a custom AutoSchema class
    # to show filters only in list action
    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = OwnershipSerializer
    write_serializer_class = OwnershipWriteSerializer

    def list(self, request, *args, **kwargs):
        self.read_serializer_class = OwnershipListResultSerializer
        return super(OwnershipViewSet, self).list(request, *args, **kwargs)

    @swagger_auto_schema(request_body=OwnershipWriteSerializer)
    def create(self, request, *args, **kwargs):
        """Transform the request.data before creating the membership record:
        """
        return super(OwnershipViewSet, self).create(request, *args, **kwargs)

    @swagger_auto_schema(request_body=OwnershipWriteSerializer)
    def update(self, request, *args, **kwargs):
        """Transform the request.data before updating the membership record:
        """
        return super(OwnershipViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=OwnershipWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        """Transform the request.data before patching the membership record:
        """
        return super().partial_update(request, *args, **kwargs)


class MembershipViewSet(BulkPartialUpdateMixin, ClassificationTypesMixin, DetailActionsMixin, rw_viewsets.ModelViewSet):
    """
    A ViewsSet for viewing and editing Membership resources.

    list:
    Return a list of all the existing Membership resources.

    partial_bulk_update:
    Partially update a collection of Mermbership resources.

    retrieve:
    Return the given Membership resource.

    create:
    Create a new Membership resource.

    update:
    Update the given Membership resource.

    partial_update:
    Partially update the given Membership resource.

    delete:
    Delete the given Membership resource.

    """

    queryset = Membership.objects.all().prefetch_related(
        "person", "organization", "post", "area"
    )

    search_fields = ("person__given_name", "person__family_name", "label", "role")
    ordering_fields = (
        "label",
        "role",
        "organization__name",
        "start_date",
        "end_date",
        "created_at",
        "updated_at",
        "person__sort_name",
    )
    ordering = ("-updated_at",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = MembershipFilterSet

    # use a custom AutoSchema class
    # to show filters only in list action
    schema = FiltersInListOnlySchema()

    serializer_class = MembershipSerializer
    read_serializer_class = MembershipSerializer
    write_serializer_class = MembershipWriteSerializer

    @action(detail=True, filter_class=None)
    def electoral_list_results(self, request, **kwargs):
        """Return electoral list results connected to this membership."""
        from project.elections.serializers import ElectoralListResultInlineSerializer
        from project.elections.views import ElectoralListResultViewSet

        instance = self.get_object_no_filter()
        self.serializer_class = ElectoralListResultInlineSerializer
        _viewset = ElectoralListResultViewSet(request=request)
        self.queryset = (
            instance.electoral_list_results.all()
        )
        self.queryset = _viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)

    @action(detail=True)
    def appointees(self, request, pk=None, **kwargs):
        """
        Shows all memberships appointed by this one.
        The usual filters for memberships can be used, for example:

            /memberships/{id}/appointees?status=current

        will only show **current appointed memberships** in the organization.

        """
        instance = self.get_object_no_filter()

        self.serializer_class = MembershipInlineSerializer

        membership_viewset = MembershipViewSet(request=request)
        self.search_fields = membership_viewset.search_fields
        self.ordering_fields = membership_viewset.ordering_fields
        self.filter_backends = membership_viewset.filter_backends
        self.filter_class = membership_viewset.filter_class

        self.queryset = (
            instance.appointees.all()
            .prefetch_related("person", "organization", "post")
            .order_by(*self.ordering_fields)
        )
        self.queryset = membership_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @action(
        detail=False,
        queryset=Classification.objects.all(),
        serializer_class=ClassificationInlineSerializer,
        ordering=("scheme", "code"),
        ordering_fields=("scheme", "code", "descr"),
        search_fields=("scheme", "code", "descr"),
        filter_backends=(
            filters.SearchFilter,
            NullsLastOrderingFilter,
            extra_filters.DjangoFilterBackend,
        ),
        filter_class=ClassificationFilterSet,
    )
    def classifications(self, request, *args, **kwargs):
        """ All membership's classifications """
        ct = ContentType.objects.get(model="membership")
        self.queryset = (
            Classification.objects.filter(related_objects__content_type_id=ct.id)
            .values("scheme", "code", "descr", "id")
            .distinct()
        )

        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)

    @action(
        detail=False,
        filter_backends=(
            extra_filters.DjangoFilterBackend,
        ),
        filter_class=TmpListsFilterSet,
    )
    def tmp_lists(self, request, *args, **kwargs):
        """ List of all distinct values of tmp_lists for the memberships.
        organization_id needs to be specified in filters,
        electoral_event_id can be specified

        A limit of 100 is imposed, to avoid problems whenever filters are not set

        Null values are purged from the list
        """
        self.queryset = (
            Membership.objects
            .values_list("electoral_list_descr_tmp", flat=True)
            .distinct()
        )

        result = self.filter_queryset(self.get_queryset())
        return Response([r for r in result[:100] if r is not None])

    @staticmethod
    def _request_transform(request):
        """Internal request.data transformation before Membership creation or update:
        - Membership.is_appointment_locked <- True (if appointment_by is set)
        - Membership.post <- f(post_role, organization)
        - Membership.label <- coalesce(label, Post.label)
        - Membership.role <- Post.role

        :param request:
        :return: request
        """
        if "appointed_by" in request.data:
            if request.data["appointed_by"]:
                request.data["is_appointment_locked"] = True
            else:
                request.data["is_appointment_locked"] = False
                request.data["appointment_note"] = None

        if "post_role" in request.data and "organization" in request.data:
            request.data["role"] = request.data.pop("post_role")

            try:
                organization = Organization.objects.get(pk=request.data["organization"])
                role_type = RoleType.objects.get(
                    label__iexact=request.data["role"],
                    classification__descr=organization.classification,
                )
                if organization.classification:
                    try:
                        prep = labels_utils.organ2art(
                            organization.classification.lower()
                        )
                    except KeyError:
                        prep = "-"
                else:
                    prep = "-"

                post, post_created = Post.objects.get_or_create(
                    role_type=role_type,
                    organization=organization,
                    defaults={
                        "label": "{0} {1} {2}".format(
                            request.data["role"], prep, organization.name
                        ),
                        "role": request.data["role"],
                    },
                )

                if (
                    not request.data.get("label", None)
                ):
                    request.data["label"] = post.label
            except Exception as e:
                raise InternalServerErrorAPIException(e)
            else:
                request.data["post"] = post.id

        return request

    def list(self, request, *args, **kwargs):
        self.read_serializer_class = MembershipListResultSerializer
        return super(MembershipViewSet, self).list(request, *args, **kwargs)

    @swagger_auto_schema(request_body=MembershipWriteSerializer)
    def create(self, request, *args, **kwargs):
        """Transform the request.data before creating the membership record:
        """
        request = self._request_transform(request)
        return super(MembershipViewSet, self).create(request, *args, **kwargs)

    @swagger_auto_schema(request_body=MembershipWriteSerializer)
    def update(self, request, *args, **kwargs):
        """Transform the request.data before updating the membership record:
        """
        request = self._request_transform(request)
        return super(MembershipViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=MembershipWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        """Transform the request.data before patching the membership record:
        """
        request = self._request_transform(request)
        return super().partial_update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=MembershipWriteSerializer(many=True))
    def partial_bulk_update(self, request, *args, **kwargs):
        self.read_serializer_class = MembershipListResultSerializer
        return super(MembershipViewSet, self).partial_bulk_update(
            request, *args, **kwargs
        )


class PostViewSet(DetailActionsMixin, rw_viewsets.ModelViewSet):
    """
    A ViewsSet for viewing and editing Post resources.

    list:
    Return a list of all the existing Post resources.

    retrieve:
    Return the given Post resource.

    create:
    Create a new Post resource.

    update:
    Update the given Post resource.

    partial_update:
    Update the given Post resource.

    delete:
    Delete the given Post resource.
    """

    queryset = Post.objects.all().prefetch_related("organization", "area")
    search_fields = ("label", "other_label", "role", "organization__name")
    ordering_fields = (
        "label",
        "other_label",
        "role",
        "organization__name",
        "area__name",
        "created_at",
        "updated_at",
    )
    ordering = ("-updated_at",)
    filter_backends = (
        filters.SearchFilter,
        extra_filters.DjangoFilterBackend,
        filters.OrderingFilter,
    )
    filter_class = PostFilterSet

    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = PostSerializer
    write_serializer_class = PostWriteSerializer

    def list(self, request, *args, **kwargs):
        self.read_serializer_class = PostListResultSerializer
        return super(PostViewSet, self).list(request, *args, **kwargs)

    @swagger_auto_schema(request_body=PostWriteSerializer)
    def create(self, request, *args, **kwargs):
        return super(PostViewSet, self).create(request, *args, **kwargs)

    @swagger_auto_schema(request_body=PostWriteSerializer)
    def update(self, request, *args, **kwargs):
        request = self._request_transform(request)
        return super(PostViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=PostWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        request = self._request_transform(request)
        return super(PostViewSet, self).partial_update(request, *args, **kwargs)

    @action(detail=True)
    def appointed_posts(self, request, pk=None, **kwargs):
        """
        Shows all posts appointed by this one.
        The usual filters for posts can be used, for example:

            /posts/{id}/appointed_posts?status=current

        will only show **current appointed posts**

        """
        instance = self.get_object_no_filter()

        self.serializer_class = PostInlineSerializer

        post_viewset = PostViewSet(request=request)
        self.search_fields = post_viewset.search_fields
        self.ordering_fields = post_viewset.ordering_fields
        self.filter_backends = post_viewset.filter_backends
        self.filter_class = post_viewset.filter_class

        self.queryset = (
            instance.appointees.all()
            .prefetch_related("organization", "area")
            .order_by(*self.ordering_fields)
        )
        self.queryset = post_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @action(detail=True)
    def memberships(self, request, pk=None, **kwargs):
        """
        Shows all memberships belonging to given `Post`.
        The usual filters for memberships can be used, for example:

            /posts/{id}/memberships?status=current

        will only show **current memberships** in the organization.

        """
        instance = self.get_object_no_filter()

        self.serializer_class = MembershipListResultSerializer

        membership_viewset = MembershipViewSet(request=request)
        self.search_fields = membership_viewset.search_fields
        self.ordering_fields = membership_viewset.ordering_fields
        self.filter_backends = membership_viewset.filter_backends
        self.filter_class = membership_viewset.filter_class

        self.queryset = (
            instance.memberships.all()
            .prefetch_related("person", "organization", "post", "area")
            .order_by(*self.ordering_fields)
        )
        self.queryset = membership_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(self.queryset, many=True)

        return Response(serializer.data)

    @staticmethod
    def _request_transform(request):
        """Internal request.data transformation before Post creation or update:
        - Post.is_appointment_locked <- True (if appointment_by is set)

        :param request:
        :return: request
        """
        if "appointed_by" in request.data:
            if request.data["appointed_by"]:
                request.data["is_appointment_locked"] = True
            else:
                request.data["is_appointment_locked"] = False
                request.data["appointment_note"] = None

        return request


class OriginalProfessionViewset(rw_viewsets.ModelViewSet):
    queryset = OriginalProfession.objects.all().prefetch_related(
        "normalized_profession"
    )
    search_fields = ("name",)
    ordering_fields = ("name",)
    ordering = ("name",)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)

    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = OriginalProfessionSerializer
    write_serializer_class = OriginalProfessionWriteSerializer


class OriginalEducationLevelViewset(rw_viewsets.ModelViewSet):
    queryset = OriginalEducationLevel.objects.all().prefetch_related(
        "normalized_education_level"
    )
    search_fields = ("name",)
    ordering_fields = ("name",)
    ordering = ("name",)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)

    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = OriginalEducationLevelSerializer
    write_serializer_class = OriginalEducationLevelWriteSerializer


class ProfessionViewset(rw_viewsets.ModelViewSet):
    queryset = Profession.objects.all()
    search_fields = ("name",)
    ordering_fields = ("name",)
    ordering = ("name",)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)

    schema = FiltersInListOnlySchema()

    read_serializer_class = (
        serializer_class
    ) = write_serializer_class = ProfessionSerializer


class EducationLevelViewset(rw_viewsets.ModelViewSet):
    queryset = EducationLevel.objects.all()
    search_fields = ("name",)
    ordering_fields = ("name",)
    ordering = ("name",)
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)

    schema = FiltersInListOnlySchema()
    read_serializer_class = (
        serializer_class
    ) = write_serializer_class = EducationLevelSerializer


class RoleTypeViewset(rw_viewsets.ModelViewSet):
    queryset = RoleType.objects.all().prefetch_related("classification")
    search_fields = (
        "label",
        "other_label",
        "classification__descr",
        "classification__code",
    )
    ordering_fields = ("label",)
    ordering = ("classification__descr", "priority")
    filter_backends = (
        filters.SearchFilter,
        extra_filters.DjangoFilterBackend,
        filters.OrderingFilter,
    )
    filter_class = RoleTypeFilterSet

    schema = FiltersInListOnlySchema()
    read_serializer_class = serializer_class = RoleTypeSerializer
    write_serializer_class = RoleTypeWriteSerializer


class ClassificationViewSet(rw_viewsets.ModelViewSet):
    queryset = Classification.objects.all().prefetch_related("parent")
    search_fields = ("descr", "code")
    ordering_fields = ("code",)
    ordering = ("code",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = ClassificationFilterSet

    schema = FiltersInListOnlySchema()
    read_serializer_class = serializer_class = ClassificationSerializer
    write_serializer_class = ClassificationWriteSerializer


class KeyEventViewSet(rw_viewsets.ModelViewSet):
    queryset = KeyEvent.objects.all()
    search_fields = ("name",)
    ordering_fields = ("start_date",)
    ordering = ("start_date",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = KeyEventFilterSet
    schema = FiltersInListOnlySchema()
    read_serializer_class = (
        write_serializer_class
    ) = serializer_class = KeyEventSerializer


class AKAViewset(
    UpdateModelMixin, ListModelMixin, RetrieveModelMixin, rw_viewsets.GenericViewSet
):
    """
    A ViewsSet for viewing and editing AKA resources.

    list:
    Return a list of all the existing AKA resources.

    retrieve:
    Return the given AKA resource.

    partial_update:
    Update the given AKA resource.
    """

    queryset = AKA.objects.all()
    search_fields = ("search_params", "loader_context")
    ordering_fields = ("n_similarities",)
    ordering = ("-created_at",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = AKAFilterSet

    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = AKASerializer
    write_serializer_class = AKASerializer


class PersonAKAViewset(AKAViewset):
    import sys

    TESTING = sys.argv[1:2] == ["test"]
    MIGRATING = any("migrat" in a for a in sys.argv[1:2])
    if not TESTING and not MIGRATING:
        ct = ContentType.objects.get(model="person")
        queryset = AKA.objects.filter(
            Q(content_type_id=ct.id) | Q(content_type_id__isnull=True)
        )

    @swagger_auto_schema(auto_schema=None)
    def update(self, request, *args, **kwargs):
        if kwargs.get("partial", False) is False:
            raise MethodNotAllowed("POST")
        return super().update(request, *args, **kwargs)

    @transaction.atomic
    @swagger_auto_schema(request_body=AKASerializer)
    def partial_update(self, request, *args, **kwargs):
        """Performs internal logic, based on data passed in body, before patching the AKA record:

        See login inside source code.

        A request to solve (set `is_resolved` to true) for an AKA record that has already been solved
        will result in a 422 (Unprocessable Entity) exception response.
        """

        # Logic implemented
        #
        #  if data == {'is_resolved': true}
        #    -  **create** a new Person, using the values in loader_context.item
        #    - add the Membership using Post, Organization and ElectionEvent defined by:
        #       - loader_context.post_id,
        #       - loader_context.organization_id,
        #       - loader_context.election_id
        #    - connect the AKA to the Person
        #    - set is_resolved to true in the AKA
        #    - return 200 with the new AKA record
        #
        #  if data == {'is_resolved': true, 'object_id': $PID}
        #    - **update** the Person with id equal to $PID, with values from loader_context.item
        #    - add the Membership Person using Post, Organization and ElectionEvent defined by:
        #       - loader_context.post_id,
        #       - loader_context.organization_id,
        #       - loader_context.election_id
        #    - connect the AKA to the Person
        #    - set is_resolved to true in the AKA
        #    - return 200 with the new AKA record
        #
        #  if data == {'is_resolved': false}
        #    - disconnect the AKA from anything it may be connected to
        #    - set is_resolved to false
        #    - return 200 with the new AKA record
        #
        #  if data == {'is_resolved': false, 'object_id': $PID}
        #    - return 422: Unprocessable Entity
        #
        #  if data == {'is_resolved': 'pippo'}
        #    - return 422: Unprocessable Entity

        aka = AKA.objects.get(pk=kwargs["pk"])
        item = aka.loader_context["item"]

        requested_is_resolved = request.data["is_resolved"]
        if (
            not isinstance(requested_is_resolved, (int, float, str))
            or isinstance(requested_is_resolved, str)
            and requested_is_resolved.lower() not in ["true", "false"]
        ):
            raise UnprocessableEntitytAPIException(
                "is_resolved can only be set to true or false"
            )

        requested_is_resolved = bool(requested_is_resolved)

        # handle request to solve/unsolve an AKA
        if requested_is_resolved is True:

            # avoid repeating handling an already solved one
            if aka.is_resolved:
                return Response(AKASerializer(aka, context={"request": request}).data)

            # get associated person_id (None means a NEW one)
            person_id = int(request.data.get("object_id", 0))

            # create or update person's anagraphical data
            # use update_strategy read from loader_context,
            # defaults to overwrite_minint_opdm
            persons_update_strategy = aka.loader_context.get(
                "persons_update_strategy", None
            )

            # use update_strategy read from loader_context,
            # defaults to overwrite_minint_opdm
            memberships_update_strategy = aka.loader_context.get(
                "memberships_update_strategy", "overwrite_minint_opdm"
            )

            check_membership_label = aka.loader_context.get(
                "check_membership_label", False
            )

            (
                person,
                created,
            ) = person_utils.update_or_create_person_and_details_from_item(
                person_id,
                item,
                loader_context=aka.loader_context,
                persons_update_strategy=persons_update_strategy,
                memberships_update_strategy=memberships_update_strategy,
                check_membership_label=check_membership_label,
            )

            # update person connected to aka instance
            aka.content_object = person
            aka.save()

            # populate other_names from aka
            person_utils.populate_other_names_from_aka(aka)

        else:
            if int(request.data.get("object_id", 0)):
                raise UnprocessableEntitytAPIException(
                    "object_id cannot be passed when setting is_resolved to false"
                )

            aka.content_object = None
            aka.save()

        # generate response
        response = super().partial_update(request, *args, **kwargs)

        # update response before responding
        response.data["object_id"] = aka.object_id

        return response
