from drf_rw_serializers import viewsets as rw_viewsets
from drf_yasg.utils import swagger_auto_schema
from rest_framework import filters, permissions

from popolo.models import (
    OriginalProfession,
    OriginalEducationLevel,
    Profession,
    EducationLevel,
)
from project.api_v1.filters import FiltersInListOnlySchema
from project.api_v1.serializers.serializers import (
    OriginalProfessionSerializer,
    OriginalProfessionWriteSerializer,
    OriginalEducationLevelSerializer,
    OriginalEducationLevelWriteSerializer,
    ProfessionSerializer,
    EducationLevelSerializer,
)


class ProfessionViewset(rw_viewsets.ModelViewSet):
    queryset = Profession.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    search_fields = ("name",)
    ordering_fields = ("name",)
    ordering = ("name",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
    )

    schema = FiltersInListOnlySchema()

    read_serializer_class = (
        serializer_class
    ) = write_serializer_class = ProfessionSerializer


class EducationLevelViewset(rw_viewsets.ModelViewSet):
    queryset = EducationLevel.objects.all()
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    search_fields = ("name",)
    ordering_fields = ("name",)
    ordering = ("name",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
    )

    schema = FiltersInListOnlySchema()

    read_serializer_class = (
        serializer_class
    ) = write_serializer_class = EducationLevelSerializer


class OriginalProfessionViewset(rw_viewsets.ModelViewSet):
    queryset = OriginalProfession.objects.all().prefetch_related(
        "normalized_profession",
    )
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    search_fields = ("name",)
    ordering_fields = ("name",)
    ordering = ("name",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
    )

    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = OriginalProfessionSerializer
    write_serializer_class = OriginalProfessionWriteSerializer

    @swagger_auto_schema(request_body=OriginalProfessionWriteSerializer)
    def create(self, request, *args, **kwargs):
        return super(OriginalProfessionViewset, self).create(request, *args, **kwargs)

    @swagger_auto_schema(request_body=OriginalProfessionWriteSerializer)
    def update(self, request, *args, **kwargs):
        return super(OriginalProfessionViewset, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=OriginalProfessionWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        return super(OriginalProfessionViewset, self).partial_update(
            request, *args, **kwargs
        )


class OriginalEducationLevelViewset(rw_viewsets.ModelViewSet):
    queryset = OriginalEducationLevel.objects.all().prefetch_related(
        "normalized_education_level",
    )
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
    search_fields = ("name",)
    ordering_fields = ("name",)
    ordering = ("name",)
    filter_backends = (
        filters.SearchFilter,
        filters.OrderingFilter,
    )

    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = OriginalEducationLevelSerializer
    write_serializer_class = OriginalEducationLevelWriteSerializer

    @swagger_auto_schema(request_body=OriginalEducationLevelWriteSerializer)
    def create(self, request, *args, **kwargs):
        return super(OriginalEducationLevelViewset, self).create(
            request, *args, **kwargs
        )

    @swagger_auto_schema(request_body=OriginalEducationLevelWriteSerializer)
    def update(self, request, *args, **kwargs):
        return super(OriginalEducationLevelViewset, self).update(
            request, *args, **kwargs
        )

    @swagger_auto_schema(request_body=OriginalEducationLevelWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        return super(OriginalEducationLevelViewset, self).partial_update(
            request, *args, **kwargs
        )
