from rest_framework.generics import get_object_or_404


class DetailActionsMixin(object):
    """
    A mixin which is useful to extend, in case a viewset needs to generate actions subviews,
    ie: `/objects/123/subobjects
    """

    def get_object_no_filter(self):
        """
        Returns the object the view is displaying, does not apply filters.
        """
        queryset = self.get_queryset()

        # Perform the lookup filtering.
        lookup_url_kwarg = self.lookup_url_kwarg or self.lookup_field

        assert lookup_url_kwarg in self.kwargs, (
            'Expected view %s to be called with a URL keyword argument '
            'named "%s". Fix your URL conf, or set the `.lookup_field` '
            'attribute on the view correctly.' %
            (self.__class__.__name__, lookup_url_kwarg)
        )

        filter_kwargs = {self.lookup_field: self.kwargs[lookup_url_kwarg]}
        obj = get_object_or_404(queryset, **filter_kwargs)

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)

        return obj
