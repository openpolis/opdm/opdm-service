from model_utils import Choices
from popolo.models import OtherName, ContactDetail, Area
from rest_framework import views
from rest_framework.response import Response


class ChoicesAPIView(views.APIView):
    choices: Choices

    def get(self, *args, **kwargs) -> Response:
        return Response(
            [{"name": choice[1], "type": choice[0]} for choice in self.choices]
        )


class ContactTypesChoicesView(ChoicesAPIView):
    choices = ContactDetail.CONTACT_TYPES


class NameTypesChoicesView(ChoicesAPIView):
    choices = OtherName.NAME_TYPES


class IstatClassificationsChoicesView(ChoicesAPIView):
    choices = Area.ISTAT_CLASSIFICATIONS
