from django.apps import AppConfig


class APIv1Config(AppConfig):
    """
    Default "api_v1" app configuration.
    """

    name = "project.api_v1"
    verbose_name = "API v1"

    def ready(self):
        from . import signals

        signals.connect()  # Connect the signals
