from typing import Type

from rest_framework.routers import (
    DefaultRouter,
    SimpleRouter,
    APIRootView,
    Route,
    DynamicRoute,
)
from rest_framework.schemas.coreapi import SchemaGenerator
from rest_framework.schemas.views import SchemaView

from project.api_v1.views import (
    AreaViewSet,
    OriginalProfessionViewset,
    ProfessionViewset,
    OriginalEducationLevelViewset,
    EducationLevelViewset,
    RoleTypeViewset,
    PersonAKAViewset,
    PersonViewSet,
    OrganizationViewSet,
    MembershipViewSet,
)
from project.api_v1.views.viewsets import (
    OwnershipViewSet,
    PostViewSet,
    KeyEventViewSet,
    ClassificationViewSet,
    OrganizationRelationshipViewSet)

from project.elections.views import (
    ElectoralCandidateResultViewSet,
    ElectoralListResultViewSet,
    ElectoralResultViewSet,
    PoliticalColourViewSet,
)
# from project.opp.votes.views import VotingViewSet


class MappeDelPotereAPIView(APIRootView):
    """Just give the APIRoot a correct name"""

    def get_view_name(self):
        return "API delle Mappe del potere - v1"


class CustomAPIv1Router(DefaultRouter):
    """
    Custom router.

    If a viewset implementing `partial_bulk_update` method is registered, this router
    will route PATCH requests to it.

    It acts as a `DefaultRouter`: generates a default API root that returns a response
    containing hyperlinks to all the list views.
    """

    routes = [
        # List route.
        Route(
            url=r"^{prefix}{trailing_slash}$",
            mapping={
                "get": "list",
                "post": "create",
                "patch": "partial_bulk_update",  # Route partial_bulk_update action
            },
            name="{basename}-list",
            detail=False,
            initkwargs={"suffix": "List"},
        ),
        # Dynamically generated list routes. Generated using
        # @action(detail=False) decorator on methods of the viewset.
        DynamicRoute(
            url=r"^{prefix}/{url_path}{trailing_slash}$",
            name="{basename}-{url_name}",
            detail=False,
            initkwargs={},
        ),
        # Detail route.
        Route(
            url=r"^{prefix}/{lookup}{trailing_slash}$",
            mapping={
                "get": "retrieve",
                "put": "update",
                "patch": "partial_update",
                "delete": "destroy",
            },
            name="{basename}-detail",
            detail=True,
            initkwargs={"suffix": "Instance"},
        ),
        # Dynamically generated detail routes. Generated using
        # @action(detail=True) decorator on methods of the viewset.
        DynamicRoute(
            url=r"^{prefix}/{lookup}/{url_path}{trailing_slash}$",
            name="{basename}-{url_name}",
            detail=True,
            initkwargs={},
        ),
    ]
    include_root_view = True
    include_format_suffixes = True
    root_view_name = "api-root"
    default_schema_renderers = None
    APIRootView = MappeDelPotereAPIView
    APISchemaView = SchemaView
    SchemaGenerator = SchemaGenerator


def register(router_class: Type[SimpleRouter], **initkwargs) -> SimpleRouter:

    router = router_class(**initkwargs)

    # /areas
    router.register(prefix=r"areas", viewset=AreaViewSet)
    # /persons
    # NOTE: Order is important!
    router.register(
        prefix=r"persons/original_professions", viewset=OriginalProfessionViewset
    )
    router.register(prefix=r"persons/professions", viewset=ProfessionViewset)
    router.register(
        prefix=r"persons/original_education_levels",
        viewset=OriginalEducationLevelViewset,
    )
    router.register(prefix=r"persons/education_levels", viewset=EducationLevelViewset)
    router.register(prefix=r"persons/role_types", viewset=RoleTypeViewset)
    router.register(prefix=r"persons/akas", viewset=PersonAKAViewset)
    router.register(prefix=r"persons", viewset=PersonViewSet)
    # /organizations
    router.register(prefix=r"organizations", viewset=OrganizationViewSet)
    # /organization_relationships
    router.register(prefix=r"organization_relationships", viewset=OrganizationRelationshipViewSet)
    # /memberships
    router.register(prefix=r"memberships", viewset=MembershipViewSet)
    # /ownerships
    router.register(prefix=r"ownerships", viewset=OwnershipViewSet)
    # /posts
    router.register(prefix=r"posts", viewset=PostViewSet)
    # /keyevents
    router.register(prefix=r"keyevents", viewset=KeyEventViewSet)
    # /classifications
    router.register(prefix=r"classifications", viewset=ClassificationViewSet)
    # /political_colours
    router.register(r"political_colours", PoliticalColourViewSet)
    # /electoral_results
    router.register(r"electoral_results", ElectoralResultViewSet)
    # /list_results
    router.register(r"list_results", ElectoralListResultViewSet)
    # /candidate_results
    router.register(r"candidate_results", ElectoralCandidateResultViewSet)
    # /votings
    # router.register(prefix=r"votings", viewset=VotingViewSet)

    return router
