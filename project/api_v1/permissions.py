from rest_framework.permissions import BasePermission, SAFE_METHODS


class OPDMPermission(BasePermission):
    """
    /api-openparlamento
    Allows access to anonymous and authenticated users for api-openparlamento.

    /api-mappepotere
    Allows access only to users in given groups:
      - users in the ``opdm_api_readers`` group are granted reading permissions
      - staff users, and users in the ``opdm_api_writers`` and ``opdm_redazione`` groups
        are granted read/write permissions
      - anonymous users do not have any access to the API data
    """

    def has_permission(self, request, view):

        groups = []
        if request.user:
            groups = list(
                request.user.groups.values_list('name', flat=True)
            )

        return bool(
            (
                (
                    '/api-openparlamento/v1' in request.path or
                    '/api-list' in request.path or
                    '/docs' in request.path
                ) and
                request.method in SAFE_METHODS
            ) or (
                '/api-mappepotere/v1' in request.path and
                request.user and
                request.user.is_authenticated and
                (
                    request.user.is_staff or
                    request.method in SAFE_METHODS and any(
                        g in ['opdm_api_readers', 'opdm_api_writers', 'opdm_redazione']
                        for g in groups
                    ) or
                    any(g in ['opdm_api_writers', 'opdm_redazione'] for g in groups)
                )
            )
        )
