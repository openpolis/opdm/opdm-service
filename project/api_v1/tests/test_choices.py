import json
from typing import Dict

from rest_framework import status

from .utils import mixins


class ChoicesTestCase(mixins.APIResourceTestCase,):
    """Test choices endpoints

    """

    @staticmethod
    def get_basic_data() -> Dict:
        pass

    def test_contact_types_choices(self):
        response = self.client.get("/api-mappepotere/v1/contact_types_choices", format="json")
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content, list)
        self.assertIsInstance(content[0], dict)
        self.assertEqual(list(content[0].keys()), ["name", "type"])

    def test_name_types_choices(self):
        response = self.client.get("/api-mappepotere/v1/name_types_choices", format="json")
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content, list)
        self.assertIsInstance(content[0], dict)
        self.assertEqual(list(content[0].keys()), ["name", "type"])

    def test_istat_classifications_choices(self):
        response = self.client.get("/api-mappepotere/v1/istat_classifications_choices", format="json")
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content, list)
        self.assertIsInstance(content[0], dict)
        self.assertEqual(list(content[0].keys()), ["name", "type"])
