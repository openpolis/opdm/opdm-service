# coding=utf-8
import json

import factory
from popolo.tests.factories import (
    PersonFactory,
    AreaFactory,
    PostFactory,
    OrganizationFactory,
)
from rest_framework import status

from project.api_v1.tests.utils import mixins
from . import faker, locale


class PostTestCase(
    mixins.APIResourceCRUDTestCase, mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/posts"
    model_factory_class = PostFactory

    @staticmethod
    def get_basic_data():
        return {
            "label": faker.pystr(max_chars=32),
            "other_label": faker.pystr(max_chars=32),
            "role": faker.pystr(max_chars=32),
        }

    @staticmethod
    def get_extra_data():
        return {
            "area": AreaFactory().id,
            "appointed_by": PostFactory().id,
            "organization": OrganizationFactory().id,
            "holders": [PersonFactory().id, PersonFactory().id, PersonFactory().id],
        }

    def test_create_post_full(self):
        """
        Test person creation
        """
        request_body = self.get_basic_data()
        request_body.update(self.get_extra_data())
        response = self.client.post(self.endpoint, request_body, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )
        content = json.loads(response.content)
        # Test on flat fields
        for field in ["label", "other_label", "role"]:
            self.assertEquals(content[field], request_body[field])
        # Test many-to-one relationships
        for field in ["area", "appointed_by", "organization"]:
            self.assertIsInstance(content[field], dict)
            self.assertEquals(request_body[field], content[field]["id"])

    def test_update_post_full(self):
        post = self.model_factory_class.create()
        request_body = dict()
        request_body.update(self.get_basic_data())
        request_body.update(self.get_extra_data())
        response = self.client.put(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=post.id),
            request_body,
            format="json",
        )
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        content = json.loads(response.content)
        # Test on flat fields
        for field in ["label", "other_label", "role"]:
            self.assertEquals(content[field], request_body[field])
        # Test many-to-one relationships
        for field in ["area", "appointed_by", "organization"]:
            self.assertIsInstance(content[field], dict)
            self.assertEquals(request_body[field], content[field]["id"])

    def test_partial_update_post_full(self):
        post = self.model_factory_class.create()
        request_body = dict()
        request_body.update(self.get_basic_data())
        request_body.update(self.get_extra_data())
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=post.id),
            request_body,
            format="json",
        )
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        content = json.loads(response.content)
        # Test on flat fields
        for field in ["label", "other_label", "role"]:
            self.assertEquals(content[field], request_body[field])
        # Test many-to-one relationships
        for field in ["area", "appointed_by", "organization"]:
            self.assertIsInstance(content[field], dict)
            self.assertEquals(request_body[field], content[field]["id"])

    def test_update_part_post_with_appointed_by_set(self):
        """Test basic post partial update (PATCH), adding an appointed_by reference
        """
        with factory.Faker.override_default_locale(locale):
            post = PostFactory()
            appointer = PostFactory()

        patched_data = {
            "appointed_by": appointer.id,
            "appointment_note": "Inserted manually",
        }
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(post.id)), patched_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEqual(content["appointed_by"]["id"], appointer.id)
        self.assertTrue(content["is_appointment_locked"])
        self.assertEqual(content["appointment_note"], "Inserted manually")

    def test_update_part_post_with_appointed_by_unset(self):
        """Test basic post partial update (PATCH), adding an appointed_by reference
        """
        with factory.Faker.override_default_locale(locale):
            appointer = PostFactory()
            post = PostFactory(appointed_by=appointer, appointment_note="Test note")
        patched_data = {"appointed_by": None}
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(post.id)), patched_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEqual(content["appointed_by"], None)
        self.assertFalse(content["is_appointment_locked"])
        self.assertEqual(content["appointment_note"], None)

    def test_update_part_post_with_appointed_by_changed(self):
        """Test basic post partial update (PATCH), adding an appointed_by reference
        """
        with factory.Faker.override_default_locale(locale):
            appointer1 = PostFactory()
            appointer2 = PostFactory()
            post = PostFactory(appointed_by=appointer1, appointment_note="Test note")
        patched_data = {"appointed_by": appointer2.id}
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(post.id)), patched_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEqual(content["appointed_by"]["id"], appointer2.id)
        self.assertTrue(content["is_appointment_locked"])
        self.assertEqual(content["appointment_note"], "Test note")
