import json
import random

from popolo.models import Area, OtherName
from popolo.tests.factories import AreaFactory, OrganizationFactory, IdentifierFactory
from rest_framework import status

from . import faker
from .utils import mixins


class AreaTestCase(
    mixins.APIResourceWithIdentifiersTestCase,
    mixins.APIResourceWithSourcesTestCase,
    mixins.APIResourceWithLinksTestCase,
    mixins.APIResourceWithOtherNamesTestCase,
    mixins.APIResourceDateframeableTestCase,
    mixins.APIResourceTimestampableTestCase,
    mixins.APIResourceCRUDTestCase,
    mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/areas"
    model_factory_class = AreaFactory

    @staticmethod
    def get_basic_data():

        return {
            "name": faker.city(),
            "identifier": faker.pystr(max_chars=4),
            "classification": faker.pystr(max_chars=5),
            "istat_classification": random.choice(
                [a[0] for a in Area.ISTAT_CLASSIFICATIONS]
            ),
            "is_provincial_capital": faker.pybool(),
            "inhabitants": faker.pyint(),
            "start_date": "1970-01-01",
            "end_date": "1982-01-01",
            "end_reason": "end reason...",
        }

    @staticmethod
    def get_extended_data():
        return {
            "parent": AreaFactory.create().id,
            "related_areas": [
                AreaFactory.create().id,
                AreaFactory.create().id,
                AreaFactory.create().id,
            ],
            "new_places": [
                AreaFactory.create().id,
                AreaFactory.create().id,
                AreaFactory.create().id,
            ],
            "identifiers": [{"scheme": "OP_ID", "identifier": str(faker.pyint())}],
            "other_names": [
                {
                    "name": faker.pystr(),
                    "othername_type": random.choice(
                        [t[0] for t in OtherName.NAME_TYPES]
                    ),
                    "note": faker.pystr(),
                    "source": faker.uri(),
                }
                for _ in range(0, 3)
            ],
            "links": [
                {"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 5)
            ],
            "sources": [
                {"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 5)
            ],
        }

    def test_create_area_full(self):
        """
        Test person creation
        """
        request_body = self.get_basic_data()
        request_body.update(self.get_extended_data())
        response = self.client.post(self.endpoint, request_body, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )
        content = json.loads(response.content)
        # Test collections
        for field in ["identifiers", "other_names", "links", "sources", "new_places"]:
            self.assertIsInstance(content[field], list)
            self.assertEquals(len(request_body[field]), len(content[field]))
        # Test many-to-one relationships
        for field in ["parent"]:
            self.assertIsInstance(content[field], dict)
            self.assertEquals(request_body[field], content[field]["id"])

    def test_create_area_with_parent(self):
        """Test creation of an area with a parent.

        This tests Foreign Key sent by POSTs as simple ID.
        The parent_area will contain a non null children field.

        :return:
        """

        parent_area = self.get_basic_data()
        response = self.client.post(self.endpoint, parent_area, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )
        parent_r = json.loads(response.content)
        parent_id = parent_r["id"]

        response = self.client.get(
            "{0}/{1}/children".format(self.endpoint, parent_id), format="json"
        )
        parent_r = json.loads(response.content)
        self.assertEquals(parent_r["count"], 0)

        area = self.get_basic_data()
        area["parent"] = parent_id
        response = self.client.post(self.endpoint, area, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        response = self.client.get(
            "{0}/{1}/children".format(self.endpoint, parent_id), format="json"
        )
        parent_r = json.loads(response.content)
        self.assertEquals(parent_r["count"], 1)

    def test_update_part_area(self):
        """Test partial area update

        :return:
        """

        # first, an already created area must be fetched through a GET request
        # we use a factory instance to create the area into the DB
        area = AreaFactory.create()
        response = self.client.get(
            "{0}/{1}".format(self.endpoint, area.id), format="json"
        )
        a = json.loads(response.content)
        patch = {"name": faker.city(), "identifier": str(faker.pyint())}

        # then the area is partially updated
        response = self.client.patch(
            "{0}/{1}".format(self.endpoint, a["id"]), patch, format="json"
        )
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        content = json.loads(response.content)

        for k in patch:
            self.assertEquals(content[k], patch[k])

    def test_list_area_organizations(self):
        """Test fetching of area organizations

        :return:
        """

        # we use a factory instance to create the area into the DB
        area = AreaFactory.create()
        OrganizationFactory.create(area=area)
        OrganizationFactory.create(area=area)
        response = self.client.get(
            "{0}/{1}/organizations".format(self.endpoint, area.id), format="json"
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content["results"], list)
        self.assertEqual(content["count"], 2)

    def test_list_area_organizations_empty(self):
        """Test fetching of area organizations when none are there

        :return:
        """

        # we use a factory instance to create the area into the DB
        area = AreaFactory.create()
        response = self.client.get(
            "{0}/{1}/organizations".format(self.endpoint, area.id), format="json"
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content["results"], list)
        self.assertEqual(content["count"], 0)

    def test_list_area_former_children(self):
        """Test fetching of area's former children

        :return:
        """

        # we use a factory instance to create the area into the DB
        area = AreaFactory.create()

        response = self.client.get(
            "{0}/{1}/organizations".format(self.endpoint, area.id), format="json"
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content["results"], list)
        self.assertEqual(content["count"], 0)

    def test_filter_by_identifier_scheme_and_value(self):
        """Test fetching of areas filtered by identifier scheme and value.
        It should always return one record.
        :return:
        """
        i = IdentifierFactory()
        a = AreaFactory()
        a.add_identifier(scheme=i.scheme, identifier=i.identifier)
        AreaFactory.create()
        response = self.client.get(
            "{0}?identifier_scheme={1}&identifier_value={2}".format(
                self.endpoint, i.scheme, i.identifier
            ),
            format="json",
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Area.objects.filter(
                identifiers__scheme=i.scheme, identifiers__identifier=i.identifier
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_op_id(self):
        """Test fetching of areas filtered by op_id
        :return:
        """
        i = IdentifierFactory(scheme="OP_ID")
        a = AreaFactory()
        a.add_identifier(scheme=i.scheme, identifier=i.identifier)
        AreaFactory.create()
        response = self.client.get(
            "{0}?op_id={1}".format(self.endpoint, i.identifier), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Area.objects.filter(
                identifiers__scheme=i.scheme, identifiers__identifier=i.identifier
            ).count(),
        )

    def test_filter_by_identifier(self):
        identifier = faker.pystr(max_chars=4)
        AreaFactory(identifier=identifier)
        AreaFactory.create()
        response = self.client.get(
            "{0}?identifier={1}".format(self.endpoint, identifier), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"], Area.objects.filter(identifier=identifier).count()
        )
