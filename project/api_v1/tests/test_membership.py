import json

import factory
from popolo.tests.factories import (
    MembershipFactory,
    PersonFactory,
    OrganizationFactory,
    PostFactory,
    ElectoralEventFactory,
    RoleTypeFactory,
    ClassificationFactory,
)
from rest_framework import status

from project.api_v1.tests.utils import mixins
from . import faker, locale


class MembershipTestCase(
    mixins.APIResourceWithLinksTestCase,
    mixins.APIResourceWithSourcesTestCase,
    mixins.APIResourceWithContactDetailsTestCase,
    mixins.APIResourceTimestampableTestCase,
    mixins.APIResourceDateframeableTestCase,
    mixins.APIResourceCRUDTestCase,
    mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/memberships"
    model_factory_class = MembershipFactory

    @staticmethod
    def get_basic_data():
        person = PersonFactory.create()
        organization = OrganizationFactory.create()

        return {
            "person": person.id,
            "organization": organization.id,
            "label": faker.sentence(nb_words=8),
            "role": faker.sentence(nb_words=8),
            "start_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
        }

    def test_create_membership_with_post(self):
        """Test membership creation, with a post.

        post is sent as simple ID.

        :return:
        """
        membership = self.get_basic_data()
        membership["post"] = PostFactory().id
        response = self.client.post(self.endpoint, membership, format="json")
        self.assertEquals(response.status_code, 201, response.content)

    def test_update_part_membership_with_person_and_post(self):
        """Test basic membership partial update (PATCH)
        """
        with factory.Faker.override_default_locale(locale):
            membership = MembershipFactory()

        patched_data = {
            "label": faker.sentence(nb_words=10),
            "role": faker.sentence(nb_words=10),
            "person": PersonFactory.create().id,
            "post": PostFactory.create().id,
        }
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(membership.id)),
            patched_data,
            format="json",
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        for k in patched_data.keys():
            if isinstance(content[k], dict):
                c = content[k].pop("id")
            else:
                c = content[k]
            self.assertEquals(c, patched_data[k])

    def test_create_membership_with_ui_form(self):
        """Test membership creation using data passed from the form in UI
        - the membership is created
        - label and role are assigned values from post and post.role_type if None
        """
        classification = ClassificationFactory()
        role_type = RoleTypeFactory.create(classification=classification)
        person = PersonFactory.create()
        organization = OrganizationFactory.create(classification=classification.descr)
        post = PostFactory.create(organization=organization, role_type=role_type)

        data = {
            "person": person.id,
            "organization": organization.id,
            "label": None,
            "role": None,
            "post_role": role_type.label,
            "start_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
            "end_reason": faker.sentence(nb_words=3),
            "electoral_event": ElectoralEventFactory.create().id,
            "electoral_list_descr_tmp": faker.sentence(nb_words=2),
            "constituency_descr_tmp": faker.sentence(nb_words=2),
        }
        response = self.client.post("{}".format(self.endpoint), data, format="json")
        self.assertEquals(response.status_code, 201, response.content)
        content = json.loads(response.content)
        self.assertIsNotNone(content["post"], "a Post could not be found")
        self.assertEqual(content["label"], post.label)
        self.assertNotEqual(content["role"], post.role)
        self.assertEqual(content["post"]["id"], post.id)
        self.assertEqual(content["post"]["role"], post.role)

    def test_update_membership_with_ui_form(self):
        """Test membership update using data passed from the form in UI
        """
        classification = ClassificationFactory()
        appointer = MembershipFactory()
        role_type = RoleTypeFactory.create(classification=classification)
        person = PersonFactory.create()
        organization = OrganizationFactory.create(classification=classification.descr)
        electoral_event = ElectoralEventFactory.create()
        post = PostFactory.create(
            organization=organization, role_type=role_type, role=role_type.label
        )
        full_label = faker.sentence(nb_words=3)
        data = {
            "label": full_label,
            "start_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
            "electoral_event": electoral_event,
            "electoral_list_descr_tmp": faker.sentence(nb_words=2),
            "constituency_descr_tmp": faker.sentence(nb_words=2),
        }
        membership = MembershipFactory.create(
            person=person, organization=organization, post=post, **data
        )

        role_type_2 = RoleTypeFactory.create(classification=classification)
        post_2 = PostFactory.create(
            organization=organization, role_type=role_type_2, role=role_type_2.label
        )
        updated_data = {
            "id": membership.id,
            "forma_giuridica": "Giunta comunale",
            "person": person.id,
            "organization": organization.id,
            "appointed_by": appointer.id,
            "appointment_note": "Inserted manually",
            "label": full_label,
            "post_role": role_type_2.label,
            "start_date": data["start_date"],
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
            "electoral_event": data["electoral_event"].id,
            "electoral_list_descr_tmp": data["electoral_list_descr_tmp"],
            "constituency_descr_tmp": data["constituency_descr_tmp"],
        }
        response = self.client.put(
            "{}/{}".format(self.endpoint, membership.id), updated_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)

        content = json.loads(response.content)
        self.assertEqual(content["start_date"], data["start_date"])
        self.assertNotEqual(content["end_date"], data["end_date"])
        self.assertEqual(content["label"], full_label)
        self.assertEqual(content["role"], post_2.role)
        self.assertEqual(content["appointed_by"]["id"], appointer.id)
        self.assertTrue(content["is_appointment_locked"])
        self.assertEqual(content["appointment_note"], "Inserted manually")

    def test_update_membership_with_ui_form_with_appointed_by(self):
        """Test membership update using data passed from the form in UI
        """
        classification = ClassificationFactory()
        appointer = MembershipFactory()
        role_type = RoleTypeFactory.create(classification=classification)
        person = PersonFactory.create()
        organization = OrganizationFactory.create(classification=classification.descr)
        electoral_event = ElectoralEventFactory.create()
        post = PostFactory.create(
            organization=organization, role_type=role_type, role=role_type.label
        )
        full_label = faker.sentence(nb_words=3)
        data = {
            "label": full_label,
            "start_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
            "electoral_event": electoral_event,
            "electoral_list_descr_tmp": faker.sentence(nb_words=2),
            "constituency_descr_tmp": faker.sentence(nb_words=2),
        }
        membership = MembershipFactory.create(
            person=person, organization=organization, post=post, **data
        )

        role_type_2 = RoleTypeFactory.create(classification=classification)
        post_2 = PostFactory.create(
            organization=organization, role_type=role_type_2, role=role_type_2.label
        )
        updated_data = {
            "id": membership.id,
            "forma_giuridica": "Giunta comunale",
            "person": person.id,
            "organization": organization.id,
            "appointed_by": appointer.id,
            "appointment_note": "Inserted manually",
            "label": full_label,
            "post_role": role_type_2.label,
            "start_date": data["start_date"],
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
            "electoral_event": data["electoral_event"].id,
            "electoral_list_descr_tmp": data["electoral_list_descr_tmp"],
            "constituency_descr_tmp": data["constituency_descr_tmp"],
        }
        response = self.client.put(
            "{}/{}".format(self.endpoint, membership.id), updated_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)

        content = json.loads(response.content)
        self.assertEqual(content["start_date"], data["start_date"])
        self.assertNotEqual(content["end_date"], data["end_date"])
        self.assertEqual(content["label"], full_label)
        self.assertEqual(content["role"], post_2.role)
        self.assertEqual(content["appointed_by"]["id"], appointer.id)
        self.assertTrue(content["is_appointment_locked"])
        self.assertEqual(content["appointment_note"], "Inserted manually")

    def test_update_part_membership_with_appointed_by_set(self):
        """Test basic membership partial update (PATCH), adding an appointed_by reference
        """
        with factory.Faker.override_default_locale(locale):
            membership = MembershipFactory()
            appointer = MembershipFactory()

        patched_data = {
            "appointed_by": appointer.id,
            "appointment_note": "Inserted manually",
        }
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(membership.id)),
            patched_data,
            format="json",
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEqual(content["appointed_by"]["id"], appointer.id)
        self.assertTrue(content["is_appointment_locked"])
        self.assertEqual(content["appointment_note"], "Inserted manually")

    def test_update_part_membership_with_appointed_by_unset(self):
        """Test basic membership partial update (PATCH), adding an appointed_by reference
        """
        with factory.Faker.override_default_locale(locale):
            appointer = MembershipFactory()
            membership = MembershipFactory(
                appointed_by=appointer, appointment_note="Test note"
            )
        patched_data = {"appointed_by": None}
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(membership.id)),
            patched_data,
            format="json",
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEqual(content["appointed_by"], None)
        self.assertFalse(content["is_appointment_locked"])
        self.assertEqual(content["appointment_note"], None)

    def test_update_part_membership_with_appointed_by_changed(self):
        """Test basic membership partial update (PATCH), adding an appointed_by reference
        """
        with factory.Faker.override_default_locale(locale):
            appointer1 = MembershipFactory()
            appointer2 = MembershipFactory()
            membership = MembershipFactory(
                appointed_by=appointer1, appointment_note="Test note"
            )
        patched_data = {"appointed_by": appointer2.id}
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(membership.id)),
            patched_data,
            format="json",
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEqual(content["appointed_by"]["id"], appointer2.id)
        self.assertTrue(content["is_appointment_locked"])
        self.assertEqual(content["appointment_note"], "Test note")

    def test_update_membership_remove_link(self):
        """Test membership update using PUT and removing a link
        """
        classification = ClassificationFactory()
        role_type = RoleTypeFactory.create(classification=classification)
        person = PersonFactory.create()
        organization = OrganizationFactory.create(classification=classification.descr)
        electoral_event = ElectoralEventFactory.create()
        post = PostFactory.create(
            organization=organization, role_type=role_type, role=role_type.label
        )
        full_label = faker.sentence(nb_words=3)
        data = {
            "label": full_label,
            "start_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
            "electoral_event": electoral_event,
            "electoral_list_descr_tmp": faker.sentence(nb_words=2),
            "constituency_descr_tmp": faker.sentence(nb_words=2),
        }
        membership = MembershipFactory.create(
            person=person, organization=organization, post=post, **data
        )
        membership.add_link(url=faker.url(), note=faker.sentence())
        membership.add_source(url=faker.url(), note=faker.sentence())

        updated_data = {
            "id": membership.id,
            "forma_giuridica": "Giunta comunale",
            "person": person.id,
            "organization": organization.id,
            "label": full_label,
            "post_role": role_type.label,
            "start_date": data["start_date"],
            "end_date": data["end_date"],
            "electoral_event": data["electoral_event"].id,
            "electoral_list_descr_tmp": data["electoral_list_descr_tmp"],
            "constituency_descr_tmp": data["constituency_descr_tmp"],
            "links": [],
            "sources": [],
        }
        response = self.client.put(
            "{}/{}".format(self.endpoint, membership.id), updated_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)

        content = json.loads(response.content)
        self.assertEqual(content["start_date"], data["start_date"])
        self.assertEqual(content["end_date"], data["end_date"])
        self.assertEqual(content["label"], full_label)
        self.assertEqual(len(content["links"]), 0)
        self.assertEqual(len(content["sources"]), 0)

    def test_update_part_membership_with_bulk_form(self):
        """Test membership partial update (PATCH) using
        the bulk update form in the admin UI
        """
        classification = ClassificationFactory()
        role_type = RoleTypeFactory.create(classification=classification)

        with factory.Faker.override_default_locale(locale):
            membership = MembershipFactory()

        patched_data = {
            "post_role": role_type.label,
            "person": PersonFactory.create().id,
            "organization": OrganizationFactory.create(
                classification=classification.descr
            ).id,
            "start_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
        }
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(membership.id)),
            patched_data,
            format="json",
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        for k in patched_data.keys():
            if k == "post_role":
                kk = "role"
            else:
                kk = k
            if isinstance(content[kk], dict):
                c = content[kk].pop("id")
            else:
                c = content[kk]
            self.assertEquals(c, patched_data[k])

    def test_bulk_partial_update(self):
        memberships = [MembershipFactory() for _ in range(10)]

        patched_collection = [
            {
                "id": m.id,
                "label": faker.sentence(nb_words=8),
                "start_date": faker.date_between(
                    start_date="-3y", end_date="-2y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-2y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
                "end_reason": faker.sentence(nb_words=16),
                "person": PersonFactory().id,
            }
            for m in memberships
        ]

        response = self.client.patch(self.endpoint, patched_collection, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        for item in content["results"]:
            d = {
                "id": item["id"],
                "label": item["label"],
                "start_date": item["start_date"],
                "end_date": item["end_date"],
                "end_reason": item["end_reason"],
                "person": item["person"]["id"],
            }

            self.assertTrue((d in patched_collection))

    def test_bulk_partial_update_with_nulls(self):
        memberships = [MembershipFactory() for _ in range(10)]

        patched_collection = [
            {
                "id": m.id,
                "label": faker.sentence(nb_words=8),
                "start_date": None,
                "end_date": faker.date_between(
                    start_date="-2y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
                "end_reason": None,
                "person": PersonFactory().id,
            }
            for m in memberships
        ]

        response = self.client.patch(self.endpoint, patched_collection, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_200_OK)
        for item in content["results"]:
            d = {
                "id": item["id"],
                "label": item["label"],
                "start_date": item["start_date"],
                "end_date": item["end_date"],
                "end_reason": item["end_reason"],
                "person": item["person"]["id"],
            }

            self.assertTrue((d in patched_collection))

    def test_bulk_partial_update_400_bad_request(self):
        bad_request = {}  # Should be a list of JSONs (send a single JSON instead)
        response = self.client.patch(self.endpoint, bad_request, format="json")
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)
        bad_request = [
            {  # Bad: start date precedes end date
                "id": MembershipFactory().id,
                "start_date": faker.date_between(
                    start_date="-2y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-3y", end_date="-2y"
                ).strftime("%Y-%m-%d"),
            },
            {  # Good!
                "id": MembershipFactory().id,
                "start_date": faker.date_between(
                    start_date="-3y", end_date="-2y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-2y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
            },
        ]
        response = self.client.patch(self.endpoint, bad_request, format="json")
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_create_membership_with_classifications(self):
        """ Test creation of an organization with three different classifications

        :return:
        """
        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()

        # test various methods to add a classification
        classifications = [
            {"classification": classification_a.id},  # existing classification id
            {
                "scheme": faker.word(),
                "code": faker.ssn(),
                "descr": faker.word(),
            },  # non-existing classification by dict
            {
                "scheme": classification_b.scheme,
                "code": classification_b.code,
            },  # existing classification by dict
            {
                "scheme": classification_b.scheme,
                "code": faker.word(),
            },  # no duplication of same-scheme-class
        ]

        membership = self.get_basic_data()
        membership["post"] = PostFactory().id
        membership["classifications"] = classifications

        response = self.client.post(self.endpoint, membership, format="json")

        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 3)

    def test_create_membership_classifications_donot_repeat(self):
        """ Test creation of 2 organizations with the same 2 classifications.

        The total amount of classifications is 2.

        :return:
        """
        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()
        classifications = [
            {"classification": classification_a.id},
            {"classification": classification_b.id},
        ]

        membership_a = self.get_basic_data()
        membership_a["post"] = PostFactory().id
        membership_a["classifications"] = classifications

        response = self.client.post(self.endpoint, membership_a, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 2)

        response = self.client.get("/api-mappepotere/v1/classifications", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 2)

        membership_b = self.get_basic_data()
        membership_b["post"] = PostFactory().id
        membership_b["classifications"] = classifications

        response = self.client.post(self.endpoint, membership_b, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 2)

        response = self.client.get("/api-mappepotere/v1/classifications", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 2)
