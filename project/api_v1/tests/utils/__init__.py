# coding=utf-8
import random

from codicefiscale import codicefiscale

from project.api_v1.tests import faker


def get_random_cf():
    return codicefiscale.encode(
        name=faker.first_name(),
        surname=faker.last_name(),
        sex=random.choice(["M", "F"]),
        birthdate=faker.date(pattern="%Y-%m-%d", end_datetime="-27y"),
        birthplace="Roma",
    )
