from abc import ABC
import json
import random
from typing import Dict

from django.contrib.auth.models import User, Group
from django.utils.timezone import get_current_timezone, make_naive
import factory
from factory.django import DjangoModelFactory
from popolo.models import OtherName, ContactDetail, Link, Source
from popolo.tests.factories import SourceFactory, LinkFactory
from rest_framework import status
from rest_framework.test import APITestCase

from project.api_v1.tests import faker, locale
from project.api_v1.tests.utils import get_random_cf


class APIResourceTestCase(APITestCase):
    """
    Base class for mixins.
    Class that use an APIResourceTestCaseMixin, must comply to the following requirements:
    - must inherit BaseAPITestCase
    - must define `endpoint` attribute
    - must implement get_basic_data method which generates and returns dictionary representing data to be used as
    payload when POSTing or PUTting new resources.
    """

    endpoint: str
    """
    A resource-specific API endpoint (e.g. 'v1/persons')
    """

    model_factory_class: DjangoModelFactory
    """
    The model class to be used to instantiate test objects
    """

    def setUp(self):
        """
        force false authentication for the client

        :return:
        """
        self.writers_group = Group.objects.create(name='opdm_api_writers')

        testuser = User.objects.create_user("writer", "writer@test.com", "pass")
        testuser.save()
        testuser.groups.add(self.writers_group)

        self.client.force_authenticate(user=testuser)

    @staticmethod
    def get_basic_data() -> Dict:  # pragma: no cover
        """
        Generate a dictionary containing resource-specific fields (should contain required fields at least) ,
        and returns it.
        Every resource is different, so a resource-specific implementation is required for each test class.
        :return: a dictionary containing resource-specific fields
        """
        raise NotImplementedError


def copy_flat(origin_data, field_name="id"):
    """copy flat attributes,
    substitute dictionaries with given field_name's value

    :param origin_data:
    :param field_name: name of the field (usually id)
    :return:
    """
    data = origin_data.copy()
    for k, v in data.items():
        if isinstance(v, dict) and field_name in v:
            data[k] = v[field_name]

    return data


class APIResourceRetrieveTestCase(APIResourceTestCase, ABC):
    def test_list(self):
        obj_list = self.model_factory_class.create_batch(10)
        response = self.client.get(self.endpoint, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(len(obj_list), len(content["results"]))
        self.assertEquals(len(obj_list), content["count"])

    def test_retrieve(self):
        obj = self.model_factory_class.create()
        response = self.client.get(f"{self.endpoint}/{obj.id}", format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(obj.id, content["id"])

    def test_retrieve_non_existing(self):
        response = self.client.get(
            f"{self.endpoint}/probably-non-existing-resource-id", format="json"
        )
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)


class APIResourceCreateTestCase(APIResourceTestCase, ABC):
    def test_create_simple(self):
        """Test basic resource creation
        """
        data = self.get_basic_data()
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        for k in data.keys():
            if isinstance(content[k], dict):
                c = content[k].pop("id")
            else:
                c = content[k]
            self.assertEquals(c, data[k])


class APIResourceUpdateTestCase(APIResourceTestCase, ABC):
    def test_update_simple(self):
        """Test basic resource update
        """
        origin_obj = self.model_factory_class.create()
        updated_data = self.get_basic_data()
        response = self.client.put(
            f"{self.endpoint}/{origin_obj.id}", updated_data, format="json"
        )
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        for k in updated_data.keys():
            if isinstance(content[k], dict):
                c = content[k].pop("id")
            else:
                c = content[k]
            self.assertEquals(c, updated_data[k])


class APIResourceDeleteTestCase(APIResourceTestCase, ABC):
    def test_delete(self):
        with factory.Faker.override_default_locale(locale):
            obj = self.model_factory_class.create()
        response = self.client.delete(f"{self.endpoint}/{obj.id}", format="json")
        self.assertEquals(
            response.status_code, status.HTTP_204_NO_CONTENT, response.content
        )

    def test_delete_non_existing(self):
        response = self.client.delete(
            self.endpoint + "/probably-non-existing-resource-id", format="json"
        )
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND, content)


class APIResourceCRUDTestCase(
    APIResourceCreateTestCase,
    APIResourceRetrieveTestCase,
    APIResourceUpdateTestCase,
    APIResourceDeleteTestCase,
    APIResourceTestCase,
    ABC,
):
    """
    Basic test methods which cover common cases when testing OPDM resources.
    """


class APIResourceWithContactDetailsTestCase(APIResourceTestCase, ABC):
    def test_create_with_one_contact_details(self):
        """ Test creation of a resource with an ContactDetail
        """
        data = self.get_basic_data()
        data["contact_details"] = [
            {
                "label": " ".join(faker.words()),
                "contact_type": random.choice(
                    [a[0] for a in ContactDetail.CONTACT_TYPES]
                ),
                "note": faker.paragraph(2),
                "value": faker.pystr(max_chars=32),
            }
        ]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["contact_details"]), len(data["contact_details"]))

    def test_create_with_many_contact_details(self):
        """ Test creation of a resource with three different ContactDetails
        """
        data = self.get_basic_data()
        data["contact_details"] = [
            {
                "label": " ".join(faker.words()),
                "contact_type": random.choice(
                    [a[0] for a in ContactDetail.CONTACT_TYPES]
                ),
                "note": faker.paragraph(2),
                "value": faker.pystr(max_chars=32),
            }
            for _ in range(0, 3)
        ]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["contact_details"]), len(data["contact_details"]))

    def test_update_contact_details(self):
        """ Test update of contact_details.
        """
        origin_obj = self.model_factory_class.create()
        objects = []
        for n in range(3):
            objects.append(
                {
                    "label": " ".join(faker.words()),
                    "contact_type": random.choice(
                        [a[0] for a in ContactDetail.CONTACT_TYPES]
                    ),
                    "note": faker.paragraph(2),
                    "value": faker.pystr(max_chars=32),
                }
            )
        origin_obj.add_contact_details(objects)
        origin_response = self.client.get(
            f"{self.endpoint}/{origin_obj.id}", format="json"
        )
        origin_data = json.loads(origin_response.content)

        # copy original data, substitute complex objects with their id
        data = copy_flat(origin_data)

        # modify one existing contact_detail
        test_contact_detail = "TEST"
        data["contact_details"][0]["value"] = test_contact_detail

        # remove last contact_detail
        data["contact_details"].pop()

        # add two new contact_details,
        # one of which twice
        identical_label = " ".join(faker.words())
        identical_type = random.choice([a[0] for a in ContactDetail.CONTACT_TYPES])
        identical_note = faker.paragraph(2)
        identical_value = faker.pystr(max_chars=32)
        data["contact_details"].append(
            {
                "label": " ".join(faker.words()),
                "contact_type": random.choice(
                    [a[0] for a in ContactDetail.CONTACT_TYPES]
                ),
                "note": faker.paragraph(2),
                "value": faker.pystr(max_chars=32),
            }
        )
        data["contact_details"].append(
            {
                "label": identical_label,
                "contact_type": identical_type,
                "note": identical_note,
                "value": identical_value,
            }
        )
        data["contact_details"].append(
            {
                "label": identical_label,
                "contact_type": identical_type,
                "note": identical_note,
                "value": identical_value,
            }
        )

        response = self.client.put(
            f"{self.endpoint}/{origin_obj.id}", data, format="json"
        )
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(
            len(content["contact_details"]), len(data["contact_details"]) - 1
        )
        for obj in content["contact_details"]:
            self.assertTrue(
                obj["label"] in [x["label"] for x in data["contact_details"]]
            )
            self.assertTrue(obj["note"] in [x["note"] for x in data["contact_details"]])
            self.assertTrue(
                obj["value"] in [x["value"] for x in data["contact_details"]]
            )
            self.assertTrue(
                obj["contact_type"]
                in [x["contact_type"] for x in data["contact_details"]]
            )


class APIResourceWithOtherNamesTestCase(APIResourceTestCase, ABC):
    def test_create_with_one_other_names(self):
        """ Test creation of a resource with an OtherName
        """
        data = self.get_basic_data()
        data["other_names"] = [
            {
                "name": faker.pystr(max_chars=256),
                "othername_type": random.choice([a[0] for a in OtherName.NAME_TYPES]),
                "note": faker.paragraph(2),
                "source": faker.uri(),
            }
        ]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["other_names"]), len(data["other_names"]))

    def test_create_with_many_other_names(self):
        """ Test creation of a resource with three different OtherNames
        """
        data = self.get_basic_data()
        data["other_names"] = [
            {
                "name": faker.pystr(max_chars=256),
                "othername_type": random.choice([a[0] for a in OtherName.NAME_TYPES]),
                "note": faker.paragraph(2),
                "source": faker.uri(),
            }
            for _ in range(0, 3)
        ]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["other_names"]), len(data["other_names"]))

    def test_update_other_names(self):
        """ Test update of other_names.
        """
        origin_obj = self.model_factory_class.create()
        objects = []
        for n in range(3):
            objects.append(
                {
                    "name": faker.pystr(max_chars=256),
                    "othername_type": random.choice(
                        [a[0] for a in OtherName.NAME_TYPES]
                    ),
                    "note": faker.paragraph(2),
                    "source": faker.uri(),
                }
            )
        origin_obj.add_other_names(objects)
        origin_response = self.client.get(
            f"{self.endpoint}/{origin_obj.id}", format="json"
        )
        origin_data = json.loads(origin_response.content)

        data = origin_data.copy()

        # modify one existing other_name
        test_other_name = "TEST"
        data["other_names"][0]["name"] = test_other_name

        # remove last other_name
        data["other_names"].pop()

        # add two new other_names,
        # one of which twice
        identical_name = faker.pystr(max_chars=256)
        identical_type = random.choice([a[0] for a in OtherName.NAME_TYPES])
        identical_note = faker.paragraph(2)
        identical_source = faker.uri()
        data["other_names"].append(
            {
                "name": faker.pystr(max_chars=256),
                "othername_type": random.choice([a[0] for a in OtherName.NAME_TYPES]),
                "note": faker.paragraph(2),
                "source": faker.uri(),
            }
        )
        data["other_names"].append(
            {
                "name": identical_name,
                "othername_type": identical_type,
                "note": identical_note,
                "source": identical_source,
            }
        )
        data["other_names"].append(
            {
                "name": identical_name,
                "othername_type": identical_type,
                "note": identical_note,
                "source": identical_source,
            }
        )

        response = self.client.put(
            f"{self.endpoint}/{origin_obj.id}", data, format="json"
        )
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(len(content["other_names"]), len(data["other_names"]) - 1)
        for obj in content["other_names"]:
            self.assertTrue(obj["name"] in [x["name"] for x in data["other_names"]])
            self.assertTrue(obj["note"] in [x["note"] for x in data["other_names"]])
            self.assertTrue(obj["source"] in [x["source"] for x in data["other_names"]])
            self.assertTrue(
                obj["othername_type"]
                in [x["othername_type"] for x in data["other_names"]]
            )


class APIResourceWithSourcesTestCase(APIResourceTestCase, ABC):
    def test_create_with_one_source(self):
        """
        Test creation of a object with 3 sources
        The created object is expected to have just 3 sources.
        """
        data = self.get_basic_data()
        data["sources"] = [{"note": faker.paragraph(2), "url": faker.uri()}]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["sources"]), 1)

    def test_create_with_one_existing_source(self):
        """
        Test creation of an object with 1 already existing Source
        The created object is expected to have 1 element in sources,
        The number of Source instances must be 1
        """
        data = self.get_basic_data()
        source = SourceFactory()
        data["sources"] = [{"url": source.url, "note": source.note}]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["sources"]), 1)
        self.assertEquals(Source.objects.count(), 1)

    def test_create_with_many_sources(self):
        """
        Test creation of a object with 3 sources
        The created object is expected to have just 3 sources.
        """
        data = self.get_basic_data()
        data["sources"] = [
            {"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 3)
        ]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["sources"]), len(data["sources"]))

    def test_create_with_many_identical_sources(self):
        """
        Test creation of an object with 4 Sources, of which 2 are identical.
        The created object is expected to have just 3 (duplicates should count as 1)
        """
        data = self.get_basic_data()
        data["sources"] = [
            {"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 3)
        ]
        data["sources"].append(random.choice(data["sources"]))
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["sources"]), len(data["sources"]) - 1)

    def test_update_sources(self):
        """ Test update of a resource with 1 source.
        """
        origin_obj = self.model_factory_class.create()
        objects = []
        for n in range(3):
            objects.append({"url": faker.uri(), "note": faker.text(max_nb_chars=500)})
        origin_obj.add_sources(objects)
        origin_response = self.client.get(
            f"{self.endpoint}/{origin_obj.id}", format="json"
        )
        origin_data = json.loads(origin_response.content)

        # copy original data, substitute complex objects with their id
        data = copy_flat(origin_data)

        # modify one existing source
        test_note = "TEST"
        data["sources"][0]["note"] = test_note

        # remove last source
        data["sources"].pop()

        # add two new sources
        data["sources"].append({"note": faker.paragraph(2), "url": faker.uri()})
        data["sources"].append({"note": faker.paragraph(2), "url": faker.uri()})

        response = self.client.put(
            f"{self.endpoint}/{origin_obj.id}", data, format="json"
        )
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(len(content["sources"]), len(data["sources"]))
        for obj in content["sources"]:
            self.assertTrue(obj["url"] in [x["url"] for x in data["sources"]])
            self.assertTrue(obj["note"] in [x["note"] for x in data["sources"]])


class APIResourceWithLinksTestCase(APIResourceTestCase, ABC):
    def test_create_with_one_link(self):
        """
        Test creation of a object with 3 Links
        The created object is expected to have just 3 Links.
        """
        data = self.get_basic_data()
        data["links"] = [{"note": faker.paragraph(2), "url": faker.uri()}]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["links"]), 1)

    def test_create_with_one_existing_link(self):
        """
        Test creation of an object with 1 already existing SoLinkurce
        The created object is expected to have 1 element in links,
        The number of Link instances must be 1
        """
        data = self.get_basic_data()
        link = LinkFactory()
        data["links"] = [{"id": link.id, "url": link.url, "note": link.note}]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["links"]), 1)
        self.assertEquals(Link.objects.count(), 1)

    def test_create_with_many_links(self):
        """
        Test creation of a object with 3 Links
        The created object is expected to have just 3 Links.
        """
        data = self.get_basic_data()
        data["links"] = [
            {"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 3)
        ]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["links"]), len(data["links"]))
        self.assertEquals(Link.objects.count(), len(data["links"]))

    def test_create_with_many_identical_links(self):
        """
        Test creation of a object with 4 Links, of which 2 are identical.
        The created object is expected to have just 3 (duplicates should count as 1)
        """
        data = self.get_basic_data()
        data["links"] = [
            {"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 3)
        ]
        data["links"].append(random.choice(data["links"]))
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["links"]), len(data["links"]) - 1)
        self.assertEquals(Link.objects.count(), len(data["links"]) - 1)

    def test_update_links(self):
        """ Test update of a resource with 1 link.
        """
        origin_obj = self.model_factory_class.create()
        objects = []
        for n in range(3):
            objects.append({"url": faker.uri(), "note": faker.text(max_nb_chars=500)})
        origin_obj.add_links(objects)
        origin_response = self.client.get(
            f"{self.endpoint}/{origin_obj.id}", format="json"
        )
        origin_data = json.loads(origin_response.content)

        # copy original data, substitute complex objects with their id
        data = copy_flat(origin_data)

        # modify one existing link
        test_note = "TEST"
        data["links"][0]["note"] = test_note

        # remove last link
        data["links"].pop()

        # add two new links
        data["links"].append({"note": faker.paragraph(2), "url": faker.uri()})
        data["links"].append({"note": faker.paragraph(2), "url": faker.uri()})

        response = self.client.put(
            f"{self.endpoint}/{origin_obj.id}", data, format="json"
        )
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(len(content["links"]), len(data["links"]))
        for obj in content["links"]:
            self.assertTrue(obj["url"] in [x["url"] for x in data["links"]])
            self.assertTrue(obj["note"] in [x["note"] for x in data["links"]])


class APIResourceWithIdentifiersTestCase(APIResourceTestCase, ABC):
    def test_create_with_one_identifier(self):
        """ Test creation of a resource with 1 identifier.
        """
        data = self.get_basic_data()
        data["identifiers"] = [{"scheme": "OP_ID", "identifier": str(faker.pyint())}]
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["identifiers"]), 1)

    def test_create_with_many_identifiers(self):
        """ Test creation of a resource with 2 identifiers,
        """
        cf = get_random_cf()
        data = self.get_basic_data()
        data["identifiers"] = [
            {"scheme": "OP_ID", "identifier": str(faker.pyint())},
            {"scheme": "CF", "identifier": cf},
        ]

        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["identifiers"]), 2)

    def test_create_with_many_identical_identifiers(self):
        """ Test creation of an organization with 4 identifiers, of which 2 are identical.
        The total count of identifiers must be 2.
        """
        cf = get_random_cf()
        identical_identifier = str(faker.pyint())
        data = self.get_basic_data()
        data["identifiers"] = [
            {"scheme": "OP_ID", "identifier": identical_identifier},
            {"scheme": "OP_ID", "identifier": identical_identifier},
            {"scheme": "CF", "identifier": cf},
            {"scheme": "CF", "identifier": cf},
        ]

        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(len(content["identifiers"]), 2)

    def test_update_identifiers(self):
        """ Test update of a reidentifier with 1 identifier.
        """
        origin_obj = self.model_factory_class.create()
        objects = []
        for n in range(3):
            objects.append(
                {"scheme": faker.pystr(max_chars=10), "identifier": str(faker.pyint())}
            )
        origin_obj.add_identifiers(objects)
        origin_response = self.client.get(
            f"{self.endpoint}/{origin_obj.id}", format="json"
        )
        origin_data = json.loads(origin_response.content)

        data = origin_data.copy()

        # modify one existing identifier
        test_identifier = "TEST"
        data["identifiers"][0]["identifier"] = test_identifier

        # remove last identifier
        data["identifiers"].pop()

        # add two new identifiers,
        # one of which twice
        identical_scheme = faker.pystr(max_chars=10)
        identical_identifier = str(faker.pyint())
        data["identifiers"].append(
            {"scheme": faker.pystr(max_chars=10), "identifier": str(faker.pyint())}
        )
        data["identifiers"].append(
            {"scheme": identical_scheme, "identifier": identical_identifier}
        )
        data["identifiers"].append(
            {"scheme": identical_scheme, "identifier": identical_identifier}
        )

        response = self.client.put(
            f"{self.endpoint}/{origin_obj.id}", data, format="json"
        )
        content = json.loads(response.content)

        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(len(content["identifiers"]), len(data["identifiers"]) - 1)
        for obj in content["identifiers"]:
            self.assertTrue(obj["scheme"] in [x["scheme"] for x in data["identifiers"]])
            self.assertTrue(
                obj["identifier"] in [x["identifier"] for x in data["identifiers"]]
            )


class APIResourceDateframeableTestCase(APIResourceTestCase, ABC):
    def test_create_start_date_end_date(self):
        """Test creation of a resource with start_date and end_date. """
        data = self.get_basic_data()
        updated_data = {
            "start_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
            "end_reason": faker.sentence(nb_words=16),
        }
        data.update(updated_data)
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)

        for key in updated_data.keys():
            self.assertEquals(content[key], data[key])

    def test_create_start_date_end_date_400_bad_request(self):
        """Test creation of a resource with start_date and end_date. """
        data = self.get_basic_data()
        updated_data = {
            "end_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "start_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
            "end_reason": faker.sentence(nb_words=16),
        }
        data.update(updated_data)
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_400_BAD_REQUEST, content)

    def test_base_filters_start_date(self):
        """Test filtering items by start_date
        :return:
        """

        start_date = faker.date_between(start_date="-33y", end_date="-17y").strftime(
            "%Y-%m-%d"
        )
        end_date = faker.date_between(start_date="-13y", end_date="-2y").strftime(
            "%Y-%m-%d"
        )
        self.model_factory_class.create(start_date=start_date, end_date=end_date)
        self.model_factory_class.create(start_date=start_date, end_date=end_date)
        self.model_factory_class.create(start_date=start_date, end_date=end_date)

        response = self.client.get(
            f"{self.endpoint}?start_date={start_date}", format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(content["count"], 3)

    def test_base_filters_end_date(self):
        """
        Test filtering items by end_date
        """

        start_date = faker.date_between(start_date="-33y", end_date="-17y").strftime(
            "%Y-%m-%d"
        )
        end_date = faker.date_between(start_date="-13y", end_date="-2y").strftime(
            "%Y-%m-%d"
        )

        self.model_factory_class.create(start_date=start_date, end_date=end_date)
        self.model_factory_class.create(start_date=start_date, end_date=end_date)
        self.model_factory_class.create(start_date=start_date, end_date=end_date)

        response = self.client.get(
            f"{self.endpoint}?end_date={end_date}", format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(content["count"], 3)


class APIResourceTimestampableTestCase(APIResourceTestCase, ABC):
    def test_base_filters_updated_after(self):
        """Test fitlering with updated_after"""

        self.model_factory_class.create(
            updated_at=faker.date_time_between(
                start_date="-30d", end_date="-20d", tzinfo=get_current_timezone()
            )
        )
        self.model_factory_class.create(
            updated_at=faker.date_time_between(
                start_date="-10d", end_date="-1d", tzinfo=get_current_timezone()
            )
        )

        updated_after_dt = faker.date_time_between(
            start_date="-19d", end_date="-11d", tzinfo=get_current_timezone()
        )
        updated_after_str = make_naive(updated_after_dt).isoformat()

        response = self.client.get(
            f"{self.endpoint}?updated_after={updated_after_str}", format="json"
        )

        content = json.loads(response.content)

        n_returned = content["count"]
        expected = 2

        self.assertEqual(n_returned, expected, content)
