import json

from django.contrib.auth.models import User, Group
from popolo.models import Area
from rest_framework.test import APITestCase
from rest_framework import status
from popolo.tests.factories import AreaFactory

from project.api_v1.tests.test_area import AreaTestCase


class PermissionsTestCase(APITestCase):

    endpoint = "/api-mappepotere/v1/areas"
    model_factory_class = AreaFactory

    def setUp(self):
        self.anon_user = User.objects.create_user("anon", "anon@test.com", "pass")
        self.anon_user.save()

        self.writers_group = Group.objects.create(name='opdm_api_writers')
        self.redattori_group = Group.objects.create(name='opdm_redazione')
        self.readers_group = Group.objects.create(name='opdm_api_readers')

        self.reader_user = User.objects.create_user("reader", "reader@test.com", "pass")
        self.reader_user.save()
        self.reader_user.groups.add(self.readers_group)

        self.writer_user = User.objects.create_user("writer", "writer@test.com", "pass")
        self.writer_user.save()
        self.writer_user.groups.add(self.writers_group)

        self.redattore_user = User.objects.create_user("redattore", "redattore@test.com", "pass")
        self.redattore_user.save()
        self.redattore_user.groups.add(self.redattori_group)

    def test_anonymous_has_no_access(self):
        self.client.force_authenticate(user=self.anon_user)
        response = self.client.get(self.endpoint, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN, content)

    def test_reader_can_read(self):
        obj_list = self.model_factory_class.create_batch(10)
        self.client.force_authenticate(user=self.reader_user)

        response = self.client.get(self.endpoint, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(len(obj_list), len(content["results"]))
        self.assertEquals(len(obj_list), content["count"])

    def test_writer_can_read(self):
        obj_list = self.model_factory_class.create_batch(10)
        self.client.force_authenticate(user=self.writer_user)

        response = self.client.get(self.endpoint, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(len(obj_list), len(content["results"]))
        self.assertEquals(len(obj_list), content["count"])

    def test_redattore_can_read(self):
        obj_list = self.model_factory_class.create_batch(10)
        self.client.force_authenticate(user=self.redattore_user)

        response = self.client.get(self.endpoint, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, content)
        self.assertEquals(len(obj_list), len(content["results"]))
        self.assertEquals(len(obj_list), content["count"])

    def test_reader_cannot_write(self):
        obj_list = self.model_factory_class.create_batch(10)

        self.client.force_authenticate(user=self.reader_user)
        data = AreaTestCase.get_basic_data()
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_403_FORBIDDEN, content)
        self.assertEquals(len(obj_list), Area.objects.count())

    def test_writer_can_write(self):
        obj_list = self.model_factory_class.create_batch(10)

        self.client.force_authenticate(user=self.writer_user)
        data = AreaTestCase.get_basic_data()
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(1 + len(obj_list), Area.objects.count())

    def test_redattore_can_write(self):
        obj_list = self.model_factory_class.create_batch(10)

        self.client.force_authenticate(user=self.redattore_user)
        data = AreaTestCase.get_basic_data()
        response = self.client.post(self.endpoint, data, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_201_CREATED, content)
        self.assertEquals(1 + len(obj_list), Area.objects.count())
