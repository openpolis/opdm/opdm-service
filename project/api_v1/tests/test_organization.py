# coding=utf-8
import json

from django.db.models import Q
from popolo.models import KeyEvent, Organization
from popolo.tests.factories import (
    AreaFactory,
    OrganizationFactory,
    ClassificationFactory,
    IdentifierFactory,
    LegislatureEventFactory,
    ElectoralEventFactory,
    XadmEventFactory,
)
from rest_framework import status

from project.api_v1.tests.utils import mixins
from . import faker


class OrganizationTestCase(
    mixins.APIResourceWithOtherNamesTestCase,
    mixins.APIResourceWithLinksTestCase,
    mixins.APIResourceWithSourcesTestCase,
    mixins.APIResourceWithIdentifiersTestCase,
    mixins.APIResourceWithContactDetailsTestCase,
    mixins.APIResourceTimestampableTestCase,
    mixins.APIResourceCRUDTestCase,
    mixins.APIResourceTestCase,
):
    """ API TestCase for the Organization class
    """

    endpoint = "/api-mappepotere/v1/organizations"
    model_factory_class = OrganizationFactory

    @staticmethod
    def get_basic_data():
        return {"name": faker.company(), "identifier": str(faker.pyint())}

    def test_create_basic_organization(self):
        """Test basic organization creation

        :return:
        """
        org = {
            "name": faker.company(),
            "identifier": "12345678901",
            "image": faker.url(),
        }
        response = self.client.post(self.endpoint, org, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        for k, v in org.items():
            self.assertEquals(r[k], v)

    def test_create_organization_with_parent(self):
        """Test organization creation, with a parent field.

        This tests Foreign Key sent by POSTs as simple ID.

        :return:
        """
        org = {
            "name": faker.company(),
            "identifier": faker.pystr(max_chars=11),
            "parent": OrganizationFactory().id,
        }
        response = self.client.post(self.endpoint, org, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        for k, v in org.items():
            if k == "parent":
                self.assertEqual(r[k]["id"], v)
            else:
                self.assertEquals(r[k], v)

    def test_create_organization_with_area(self):
        """Test organization creation, with a related area.

        This tests Foreign Key sent by POSTs as simple ID.

        :return:
        """
        org = {
            "name": faker.company(),
            "identifier": faker.pystr(max_chars=11),
            "area": AreaFactory().id,
        }
        response = self.client.post(self.endpoint, org, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        for k, v in org.items():
            if k == "area":
                # get the ID from the field, represented as a url
                self.assertEqual(r[k]["id"], v)
            else:
                self.assertEquals(r[k], v)

    def test_create_organization_with_classifications(self):
        """ Test creation of an organization with three different classifications

        :return:
        """
        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()

        # test various methods to add a classification
        classifications = [
            {"classification": classification_a.id},  # existing classification id
            {
                "scheme": faker.word(),
                "code": faker.ssn(),
                "descr": faker.word(),
            },  # non-existing classification by dict
            {
                "scheme": classification_b.scheme,
                "code": classification_b.code,
            },  # existing classification by dict
            {
                "scheme": classification_b.scheme,
                "code": faker.word(),
            },  # no duplication of same-scheme-class
        ]

        org = {
            "name": faker.company(),
            "identifier": faker.pystr(max_chars=11),
            "classifications": classifications,
        }
        response = self.client.post(self.endpoint, org, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 3)

    def test_create_organizations_classifications_donot_repeat(self):
        """ Test creation of 2 organizations with the same 2 classifications.

        The total amount of classifications is 2.

        :return:
        """
        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()
        classifications = [
            {"classification": classification_a.id},
            {"classification": classification_b.id},
        ]

        org_a = {
            "name": faker.company(),
            "identifier": faker.pystr(max_chars=11),
            "classifications": classifications,
        }
        response = self.client.post(self.endpoint, org_a, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 2)

        response = self.client.get(self.endpoint + "/classifications", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 2)

        org_b = {
            "name": faker.company(),
            "identifier": faker.pystr(max_chars=11),
            "classifications": classifications,
        }
        response = self.client.post(self.endpoint, org_b, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 2)

        response = self.client.get(self.endpoint + "/classifications", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 2)

    def test_create_organization_with_non_existing_key_event_fails(self):
        """ Test creation of an organization with a non-existing key_event
        :return:
        """
        kes = [
            {
                "name": faker.sentence(nb_words=3),
                "identifier": faker.pystr(max_chars=11),
                "event_type": "ITL",
                "start_date": faker.date(pattern="%Y-%m-%d", end_datetime="-27y"),
            }
        ]

        org = {
            "name": faker.company(),
            "identifier": faker.pystr(max_chars=11),
            "key_events": kes,
        }
        with self.assertRaises(Exception):
            self.client.post(self.endpoint, org, format="json")

    def test_create_organization_with_key_events(self):
        """ Test creation of an organization with different key_events
        - the events link back to the organization
        :return:
        """
        ke_a = LegislatureEventFactory()
        ke_b = ElectoralEventFactory()
        ke_c = XadmEventFactory()

        kes = [{"key_event": ke_a.id}, {"key_event": ke_b.id}, {"key_event": ke_c.id}]

        org = {
            "name": faker.company(),
            "identifier": faker.pystr(max_chars=11),
            "key_events": kes,
        }
        response = self.client.post(self.endpoint, org, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["key_events"]), 3)
        self.assertEquals(KeyEvent.objects.count(), 3)
        self.assertEquals(
            KeyEvent.objects.first().related_objects.first().content_type.name,
            "Organizzazione",
        )
        self.assertEquals(
            KeyEvent.objects.first().related_objects.first().content_object,
            Organization.objects.get(id=r["id"]),
        )

    def test_update_basic_organization(self):
        """ Test basic organization update
        """

        # first, an already created org must be fetched through a GET request
        # we use a factory instance to create the organization into the DB
        org_response = self.client.get(
            "{0}/{1}".format(self.endpoint, OrganizationFactory().id), format="json"
        )
        org = json.loads(org_response.content)
        patch = {"name": faker.company(), "identifier": str(faker.pyint())}
        org_updated = org.copy()
        org_updated.update(patch)

        # then the org is updated
        response = self.client.put(
            "{0}/{1}".format(self.endpoint, org["id"]), org_updated, format="json"
        )
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        content = json.loads(response.content)

        for k in patch:
            self.assertEquals(content[k], org_updated[k])

    def test_update_complex_organization(self):
        """ Test  organization update with nested fields
        """
        # first we create some Classification instances
        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()
        classification_c = ClassificationFactory()
        classifications = [
            {"classification": classification_a.id},
            {"classification": classification_b.id, "start_date": "2014-01-01"},
        ]

        identifier_a = IdentifierFactory.create()
        identifier_b = IdentifierFactory.create()
        identifiers = [
            {
                "scheme": identifier_a.scheme,
                "identifier": identifier_a.identifier,
                "source": identifier_a.source,
            },
            {
                "scheme": identifier_b.scheme,
                "identifier": identifier_b.identifier,
                "source": identifier_b.source,
            },
        ]

        # first, a new org must be created,
        # as the test DB is empty,
        # we parse the response to get the ID
        org = OrganizationFactory()
        org.add_identifiers(identifiers)
        org.add_classifications(classifications)

        org_updated_resp = self.client.put(
            "{0}/{1}".format(self.endpoint, org.id),
            {
                "name": org.name,
                "identifier": org.identifier,
                "classifications": [
                    {"classification": classification_b.id, "start_date": "2015-01-01"},
                    {"classification": classification_c.id},
                ],
                "identifiers": [
                    {
                        "scheme": identifier_a.scheme,
                        "identifier": identifier_b.identifier,
                    }
                ],
            },
            format="json",
        )
        self.assertEquals(
            org_updated_resp.status_code, status.HTTP_200_OK, org_updated_resp.content
        )
        org_updated = json.loads(org_updated_resp.content)
        self.assertEquals(len(org_updated["classifications"]), 2)
        self.assertEquals(len(org_updated["identifiers"]), 1)
        self.assertIn(
            classification_c.id,
            [c["classification"]["id"] for c in org_updated["classifications"]],
        )
        self.assertNotIn(
            classification_a.id,
            [c["classification"]["id"] for c in org_updated["classifications"]],
        )

        # only references to existing classifications are update, not content
        self.assertNotIn(
            "2015-01-01",
            [c.get("start_date", None) for c in org_updated["classifications"]],
        )

    def test_update_part_basic_organization(self):
        """Test basic organization update

        :return:
        """

        # first, an already created org must be fetched through a GET request
        # we use a factory instance to create the organization into the DB
        org_response = self.client.get(
            "{0}/{1}".format(self.endpoint, OrganizationFactory().id), format="json"
        )
        org = json.loads(org_response.content)
        patch = {"name": faker.company(), "identifier": str(faker.pyint())}

        # then the org is updated
        response = self.client.patch(
            "{0}/{1}".format(self.endpoint, org["id"]), patch, format="json"
        )
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        content = json.loads(response.content)

        for k in patch:
            self.assertEquals(content[k], patch[k])

    def test_organizations_identifier_types(self):
        # first we create some Identifier instances
        identifier_a = IdentifierFactory.create()
        identifier_b = IdentifierFactory.create()
        identifiers = [
            {
                "scheme": identifier_a.scheme,
                "identifier": identifier_a.identifier,
                "source": identifier_a.source,
            },
            {
                "scheme": identifier_b.scheme,
                "identifier": identifier_b.identifier,
                "source": identifier_b.source,
            },
        ]

        # a new org must be created,
        org = OrganizationFactory()
        org.add_identifiers(identifiers)

        response = self.client.get("/api-mappepotere/v1/organizations/identifier_types", format="json")
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content["results"], list)
        self.assertEqual(content["count"], 2)
        self.assertEqual(identifier_a.scheme in content["results"], True)

    def test_organizations_empty_identifier_types(self):
        # a new org must be created,
        OrganizationFactory.create()

        response = self.client.get("/api-mappepotere/v1/organizations/identifier_types", format="json")
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content, list)
        self.assertEqual(len(content), 0)

    def test_organizations_classification_types(self):
        # first we create some Classification instances
        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()
        classification_c = ClassificationFactory()
        classifications = [
            {"classification": classification_a.id},
            {"classification": classification_b.id, "start_date": "2014-01-01"},
            {"classification": classification_c.id},
        ]

        org = OrganizationFactory()
        org.add_classifications(classifications)
        response = self.client.get(
            "/api-mappepotere/v1/organizations/classification_types", format="json"
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content["results"], list)
        self.assertEqual(content["count"], 3)
        self.assertEqual(classification_a.scheme in content["results"], True)

    def test_organizations_empty_classification_types(self):
        OrganizationFactory.create()
        response = self.client.get(
            "/api-mappepotere/v1/organizations/classification_types", format="json"
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertIsInstance(content, list)
        self.assertEqual(len(content), 0)

    def test_base_filters_active_status(self):
        """Test fetching of organizations, filtered by active_status
        - active_from
        - active_to
        - updated_after
        :return:
        """

        OrganizationFactory.create(
            founding_date=faker.date_between(
                start_date="-30y", end_date="-20y"
            ).strftime("%Y-%m-%d")
        )
        OrganizationFactory.create(
            founding_date=faker.date_between(
                start_date="-30y", end_date="-20y"
            ).strftime("%Y-%m-%d"),
            dissolution_date=faker.date_between(
                start_date="-10y", end_date="-1y"
            ).strftime("%Y-%m-%d"),
        )
        response = self.client.get(
            "{0}?active_status=current".format(self.endpoint), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Organization.objects.filter(dissolution_date__isnull=True).count(),
        )

        response = self.client.get(
            "{0}?active_status=past".format(self.endpoint), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Organization.objects.filter(dissolution_date__isnull=False).count(),
        )

    def test_base_filters_active_at(self):
        """Test fetching of organizations, filtered by active_at
        :return:
        """

        OrganizationFactory.create(
            founding_date=faker.date_between(
                start_date="-30y", end_date="-20y"
            ).strftime("%Y-%m-%d")
        )
        OrganizationFactory.create(
            founding_date=faker.date_between(
                start_date="-30y", end_date="-20y"
            ).strftime("%Y-%m-%d"),
            dissolution_date=faker.date_between(
                start_date="-10y", end_date="-1y"
            ).strftime("%Y-%m-%d"),
        )

        distant_date = faker.date_between(start_date="-60y", end_date="-40y").strftime(
            "%Y-%m-%d"
        )

        response = self.client.get(
            "{0}?active_at={1}".format(self.endpoint, distant_date), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Organization.objects.filter(
                Q(founding_date__lte=distant_date)
                & (
                    Q(dissolution_date__gte=distant_date)
                    | Q(dissolution_date__isnull=True)
                )
            ).count(),
        )

        response = self.client.get(
            "{0}?active_at=now".format(self.endpoint), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Organization.objects.filter(dissolution_date__isnull=True).count(),
        )

    def test_base_filters_active_from(self):
        """Test fetching of organizations, filtered by active_from
        :return:
        """

        OrganizationFactory.create(
            founding_date=faker.date_between(
                start_date="-30y", end_date="-20y"
            ).strftime("%Y-%m-%d")
        )
        OrganizationFactory.create(
            founding_date=faker.date_between(
                start_date="-30y", end_date="-20y"
            ).strftime("%Y-%m-%d"),
            dissolution_date=faker.date_between(
                start_date="-10y", end_date="-1y"
            ).strftime("%Y-%m-%d"),
        )

        distant_date = faker.date_between(start_date="-60y", end_date="-40y").strftime(
            "%Y-%m-%d"
        )

        response = self.client.get(
            "{0}?active_from={1}".format(self.endpoint, distant_date), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Organization.objects.filter(Q(founding_date__lte=distant_date)).count(),
        )

    def test_base_filters_active_to(self):
        """Test fetching of organizations, filtered by active_to
        :return:
        """

        OrganizationFactory.create(
            founding_date=faker.date_between(
                start_date="-30y", end_date="-20y"
            ).strftime("%Y-%m-%d")
        )
        OrganizationFactory.create(
            founding_date=faker.date_between(
                start_date="-30y", end_date="-20y"
            ).strftime("%Y-%m-%d"),
            dissolution_date=faker.date_between(
                start_date="-10y", end_date="-1y"
            ).strftime("%Y-%m-%d"),
        )

        distant_date = faker.date_between(start_date="-60y", end_date="-40y").strftime(
            "%Y-%m-%d"
        )

        response = self.client.get(
            "{0}?active_to={1}".format(self.endpoint, distant_date), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Organization.objects.filter(
                Q(dissolution_date__gt=distant_date) | Q(dissolution_date__isnull=True)
            ).count(),
        )

    def test_filter_by_identifier_scheme_and_value(self):
        """Test fetching of organizations filtered by identifier scheme and value.
        It should always return one record.
        :return:
        """
        i = IdentifierFactory()
        org = OrganizationFactory()
        org.add_identifier(scheme=i.scheme, identifier=i.identifier)
        OrganizationFactory.create()
        response = self.client.get(
            "{0}?identifier_scheme={1}&identifier_value={2}".format(
                self.endpoint, i.scheme, i.identifier
            ),
            format="json",
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Organization.objects.filter(
                identifiers__scheme=i.scheme, identifiers__identifier=i.identifier
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_base_filters_founding_date(self):
        """Test filtering items by founding_date
        :return:
        """

        founding_date = faker.date_between(start_date="-33y", end_date="-17y").strftime(
            "%Y-%m-%d"
        )
        dissolution_date = faker.date_between(
            start_date="-13y", end_date="-2y"
        ).strftime("%Y-%m-%d")
        self.model_factory_class.create(
            founding_date=founding_date, dissolution_date=dissolution_date
        )
        self.model_factory_class.create(
            founding_date=founding_date, dissolution_date=dissolution_date
        )
        self.model_factory_class.create(
            founding_date=founding_date, dissolution_date=dissolution_date
        )

        response = self.client.get(
            "{0}?founding_date={1}".format(self.endpoint, founding_date), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(content["count"], 3)

    def test_base_filters_dissolution_date(self):
        """Test filtering items by dissolution_date
        :return:
        """

        founding_date = faker.date_between(start_date="-33y", end_date="-17y").strftime(
            "%Y-%m-%d"
        )
        dissolution_date = faker.date_between(
            start_date="-13y", end_date="-2y"
        ).strftime("%Y-%m-%d")
        self.model_factory_class.create(
            founding_date=founding_date, dissolution_date=dissolution_date
        )
        self.model_factory_class.create(
            founding_date=founding_date, dissolution_date=dissolution_date
        )
        self.model_factory_class.create(
            founding_date=founding_date, dissolution_date=dissolution_date
        )

        response = self.client.get(
            "{0}?dissolution_date={1}".format(self.endpoint, dissolution_date),
            format="json",
        )
        content = json.loads(response.content)
        self.assertEqual(content["count"], 3)
