# coding=utf-8
from datetime import datetime
import json
import random
from unittest.mock import patch

import factory
from popolo.models import OtherName, ContactDetail, Area, Person, RoleType
from popolo.tests.factories import (
    PersonFactory,
    AreaFactory,
    ProfessionFactory,
    EducationLevelFactory,
    OriginalProfessionFactory,
    OriginalEducationLevelFactory,
    RoleTypeFactory,
    ClassificationFactory,
    OrganizationFactory,
    IdentifierFactory,
)
from rest_framework import status

from project.api_v1.tests.utils import mixins, get_random_cf
from . import faker, locale


class PersonTestCase(
    mixins.APIResourceWithOtherNamesTestCase,
    mixins.APIResourceWithLinksTestCase,
    mixins.APIResourceWithSourcesTestCase,
    mixins.APIResourceWithIdentifiersTestCase,
    mixins.APIResourceWithContactDetailsTestCase,
    mixins.APIResourceTimestampableTestCase,
    mixins.APIResourceCRUDTestCase,
    mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/persons"
    model_factory_class = PersonFactory

    @staticmethod
    def get_basic_data():
        gender = random.choice(["M", "F"])
        given_name = (
            faker.first_name_female() if gender == "F" else faker.first_name_male()
        )
        family_name = faker.last_name()
        return {
            "given_name": given_name,
            "family_name": family_name,
            "birth_date": faker.date(pattern="%Y-%m-%d", end_datetime="-27y"),
            "death_date": None,
            "additional_name": faker.first_name(),
            "patronymic_name": None,
            "honorific_prefix": None,
            "honorific_suffix": None,
            "email": faker.ascii_safe_email(),
            "gender": gender,
            "summary": faker.paragraph(
                nb_sentences=2, variable_nb_sentences=True, ext_word_list=None
            ),
            "biography": faker.paragraph(
                nb_sentences=7, variable_nb_sentences=True, ext_word_list=None
            ),
            "national_identity": faker.pystr(),
            "birth_location": faker.city(),
            "image": faker.uri(),
        }

    @staticmethod
    def get_extended_data():
        province = AreaFactory(
            istat_classification=Area.ISTAT_CLASSIFICATIONS.provincia,
            classification="ADM2",
            identifier=factory.Faker("pystr", max_chars=2),
        )
        area = AreaFactory(
            istat_classification=Area.ISTAT_CLASSIFICATIONS.comune,
            classification="ADM3",
            parent=province,
        )

        return {
            "identifiers": [
                {"scheme": "OP_ID", "identifier": str(faker.pyint())},
                {"scheme": "CF", "identifier": get_random_cf()},
            ],
            "contact_details": [
                {
                    "label": faker.pystr(),
                    "contact_type": random.choice(
                        [t[0] for t in ContactDetail.CONTACT_TYPES]
                    ),
                    "value": faker.pystr(),
                    "note": faker.pystr(),
                }
                for _ in range(0, 3)
            ],
            "other_names": [
                {
                    "name": faker.pystr(),
                    "othername_type": random.choice(
                        [t[0] for t in OtherName.NAME_TYPES]
                    ),
                    "note": faker.pystr(),
                    "source": faker.uri(),
                }
                for _ in range(0, 3)
            ],
            "links": [
                {"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 5)
            ],
            "sources": [
                {"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 5)
            ],
            "birth_location_area": area.id,
            "related_persons": [
                PersonFactory().id,
                PersonFactory().id,
                PersonFactory().id,
            ],
        }

    def test_base_filters_active_status(self):
        """Test fetching of persons, filtered by active_status (alive, deceased)
        :return:
        """

        PersonFactory.create(
            birth_date=faker.date_between(start_date="-30y", end_date="-20y").strftime(
                "%Y-%m-%d"
            ),
            death_date=None,
        )
        PersonFactory.create(
            birth_date=faker.date_between(start_date="-30y", end_date="-20y").strftime(
                "%Y-%m-%d"
            ),
            death_date=faker.date_between(start_date="-10y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
        )

        response = self.client.get(
            "{0}?active_status=alive".format(self.endpoint), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"], Person.objects.filter(death_date__isnull=True).count()
        )

        response = self.client.get(
            "{0}?active_status=deceased".format(self.endpoint), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"], Person.objects.filter(death_date__isnull=False).count()
        )

    def test_create_person_with_area(self):
        """Test person creation, with a birth_location_area.

        This tests Foreign Key sent by POSTs as simple ID.

        :return:
        """
        province = AreaFactory(
            istat_classification=Area.ISTAT_CLASSIFICATIONS.provincia,
            classification="ADM2",
            identifier=factory.Faker("pystr", max_chars=2),
        )
        area = AreaFactory(
            istat_classification=Area.ISTAT_CLASSIFICATIONS.comune,
            classification="ADM3",
            parent=province,
        )

        person = self.get_basic_data()
        birth_location_area_id = area.id
        person["birth_location_area"] = birth_location_area_id
        response = self.client.post(self.endpoint, person, format="json")
        self.assertEquals(response.status_code, 201, response.content)
        content = json.loads(response.content)
        self.assertEquals(content["birth_location_area"]["id"], birth_location_area_id)
        self.assertEquals(
            content["birth_location"],
            "{0} ({1})".format(area.name, area.parent.identifier),
        )

    def test_create_person_full(self):
        """
        Test person creation
        """
        request_body = self.get_basic_data()
        request_body.update(self.get_extended_data())
        response = self.client.post(self.endpoint, request_body, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )
        content = json.loads(response.content)
        # Test on flat fields
        for field in ["given_name", "family_name", "birth_date"]:
            self.assertEquals(content[field], request_body[field])
        self.assertEqual(
            content["name"],
            "{0} {1}".format(request_body["given_name"], request_body["family_name"]),
        )
        self.assertEqual(
            content["sort_name"],
            "{0} {1}".format(
                request_body["family_name"], request_body["given_name"]
            ).lower(),
        )
        # Test collections
        for field in [
            "identifiers",
            "contact_details",
            "other_names",
            "links",
            "sources",
        ]:
            self.assertIsInstance(content[field], list)
            self.assertEquals(len(request_body[field]), len(content[field]))
        # Test many-to-one relationships
        for field in ["birth_location_area"]:
            self.assertIsInstance(content[field], dict)
            self.assertEquals(request_body[field], content[field]["id"])

    def test_update_part_person_simple(self):
        """Test basic person partial update (PATCH)
        """
        with factory.Faker.override_default_locale(locale):
            person = PersonFactory()
        patched_data = {
            "family_name": faker.last_name(),
            "email": faker.ascii_safe_email(),
            "biography": faker.paragraph(
                nb_sentences=7, variable_nb_sentences=True, ext_word_list=None
            ),
        }
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(person.id)), patched_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        for k in patched_data.keys():
            self.assertEquals(content[k], patched_data[k])
        self.assertEqual(
            content["name"],
            "{0} {1}".format(content["given_name"], content["family_name"]),
        )
        self.assertEqual(
            content["sort_name"],
            "{0} {1}".format(content["family_name"], content["given_name"]).lower(),
        )

    def test_update_part_person_with_area(self):
        """Test basic person partial update of FK field (PATCH)
        """
        with factory.Faker.override_default_locale(locale):
            person = PersonFactory()
            province = AreaFactory(
                istat_classification=Area.ISTAT_CLASSIFICATIONS.provincia,
                classification="ADM2",
                identifier=factory.Faker("pystr", max_chars=2),
            )
            area = AreaFactory(
                istat_classification=Area.ISTAT_CLASSIFICATIONS.comune,
                classification="ADM3",
                parent=province,
            )
        patched_data = {"birth_location_area": area.id}
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(person.id)), patched_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEquals(content["birth_location_area"]["id"], area.id)
        self.assertEquals(
            content["birth_location"],
            "{0} ({1})".format(area.name, area.parent.identifier),
        )

    def test_update_part_person_with_inners(self):
        with factory.Faker.override_default_locale(locale):
            person = PersonFactory()

        person.add_links(
            [{"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 3)]
        )
        person.add_sources(
            [{"note": faker.paragraph(2), "url": faker.uri()} for _ in range(0, 3)]
        )
        patched_data = {
            "family_name": faker.last_name(),
            "email": faker.ascii_safe_email(),
            "biography": faker.paragraph(
                nb_sentences=7, variable_nb_sentences=True, ext_word_list=None
            ),
        }
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(person.id)), patched_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        for k in patched_data.keys():
            self.assertEquals(content[k], patched_data[k])
        self.assertEquals(len(content["links"]), 3)
        self.assertEquals(len(content["sources"]), 3)

    def test_search_existing_person_exact(self):
        """Test search an existing person with exact parameters
        """

        with factory.Faker.override_default_locale(locale):
            person = PersonFactory()
            person.birth_location = faker.city()
            person.save()

        search_params = "?family_name={}&given_name={}&birth_date={}&birth_location={}".format(
            person.family_name,
            person.given_name,
            person.birth_date,
            person.birth_location,
        )

        # mock solr exact response
        self.mock_sqs_patcher = patch("pysolr.Solr._select")
        self.mock_sqs = self.mock_sqs_patcher.start()
        self.mock_sqs.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": '(family_name:("{}") AND given_name:("{}") '
                        'AND birth_date_s:("{}") '
                        'AND birth_location:("{}"))'.format(
                            person.family_name,
                            person.given_name,
                            person.birth_date,
                            person.birth_location,
                        ),
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 1,
                    "start": 0,
                    "maxScore": 31.469349,
                    "docs": [
                        {
                            "id": "popolo.person.{}".format(person.id),
                            "django_ct": "popolo.person",
                            "django_id": "{}".format(person.id),
                            "family_name": person.family_name,
                            "given_name": person.given_name,
                            "birth_date": person.birth_date,
                            "birth_date_s": person.birth_date,
                            "birth_location": person.birth_location,
                            "_version_": 1600543405957447692,
                            "score": 31.469349,
                        }
                    ],
                },
            }
        )

        response = self.client.get(
            "{}/search{}".format(self.endpoint, search_params), format="json"
        )
        self.mock_sqs_patcher.stop()
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEquals(content["count"], 1)
        self.assertNotEqual(content["results"][0]["score"], None)

    def test_search_person_non_exact(self):
        """Test search a person with partial parameters,
           returns similarities
        """
        family_name_a = "Franchi"
        family_name_b = "Bianchi"
        with factory.Faker.override_default_locale(locale):
            person_a = PersonFactory()
            person_a.family_name = family_name_a
            person_a.birth_location = faker.city()
            person_a.save()

            person_b = PersonFactory()
            person_b.family_name = family_name_b
            person_b.birth_location = faker.city()
            person_b.save()

            person_c = PersonFactory()

        search_params = "?family_name={}&given_name={}".format(
            person_a.family_name, person_a.given_name
        )

        # mock solr exact response
        self.mock_sqs_patcher = patch("pysolr.Solr._select")
        self.mock_sqs = self.mock_sqs_patcher.start()
        self.mock_sqs.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 11,
                    "params": {
                        "q": '(family_name:("{}") AND given_name:("{}") '.format(
                            person_a.family_name, person_a.given_name
                        ),
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 2,
                    "start": 0,
                    "maxScore": 81.469349,
                    "docs": [
                        {
                            "id": "popolo.person.{}".format(person_a.id),
                            "django_ct": "popolo.person",
                            "django_id": "{}".format(person_a.id),
                            "family_name": person_a.family_name,
                            "given_name": person_a.given_name,
                            "birth_date": person_a.birth_date,
                            "birth_date_s": person_a.birth_date,
                            "birth_location": person_a.birth_location,
                            "score": 81.469349,
                        },
                        {
                            "id": "popolo.person.{}".format(person_b.id),
                            "django_ct": "popolo.person",
                            "django_id": "{}".format(person_b.id),
                            "family_name": person_b.family_name,
                            "given_name": person_b.given_name,
                            "birth_date": person_b.birth_date,
                            "birth_date_s": person_b.birth_date,
                            "birth_location": person_b.birth_location,
                            "score": 57.349502,
                        },
                        {
                            "id": "popolo.person.{}".format(person_c.id),
                            "django_ct": "popolo.person",
                            "django_id": "{}".format(person_c.id),
                            "family_name": person_c.family_name,
                            "given_name": person_c.given_name,
                            "birth_date": person_c.birth_date,
                            "birth_date_s": person_c.birth_date,
                            "birth_location": person_c.birth_location,
                            "score": 7.349502,
                        },
                    ],
                },
            }
        )

        response = self.client.get(
            "{}/search{}".format(self.endpoint, search_params), format="json"
        )
        self.mock_sqs_patcher.stop()
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEquals(content["count"], 2)

    def test_search_person_non_exact_hires(self):
        """Test search a person with partial parameters,
           returns similarities
        """
        family_name_a = "Franchi"
        family_name_b = "Bianchi"
        with factory.Faker.override_default_locale(locale):
            person_a = PersonFactory()
            person_a.family_name = family_name_a
            person_a.birth_location = faker.city()
            person_a.save()

            person_b = PersonFactory()
            person_b.family_name = family_name_b
            person_b.birth_location = faker.city()
            person_b.save()

            person_c = PersonFactory()

        search_params = "?family_name={}&given_name={}".format(
            person_a.family_name, person_a.given_name
        )

        # mock solr exact response
        self.mock_sqs_patcher = patch("pysolr.Solr._select")
        self.mock_sqs = self.mock_sqs_patcher.start()
        self.mock_sqs.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 11,
                    "params": {
                        "q": '(family_name:("{}") AND given_name:("{}") '.format(
                            person_a.family_name, person_a.given_name
                        ),
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 2,
                    "start": 0,
                    "maxScore": 171.469349,
                    "docs": [
                        {
                            "id": "popolo.person.{}".format(person_a.id),
                            "django_ct": "popolo.person",
                            "django_id": "{}".format(person_a.id),
                            "family_name": person_a.family_name,
                            "given_name": person_a.given_name,
                            "birth_date": person_a.birth_date,
                            "birth_date_s": person_a.birth_date,
                            "birth_location": person_a.birth_location,
                            "score": 171.469349,
                        },
                        {
                            "id": "popolo.person.{}".format(person_b.id),
                            "django_ct": "popolo.person",
                            "django_id": "{}".format(person_b.id),
                            "family_name": person_b.family_name,
                            "given_name": person_b.given_name,
                            "birth_date": person_b.birth_date,
                            "birth_date_s": person_b.birth_date,
                            "birth_location": person_b.birth_location,
                            "score": 57.349502,
                        },
                        {
                            "id": "popolo.person.{}".format(person_c.id),
                            "django_ct": "popolo.person",
                            "django_id": "{}".format(person_c.id),
                            "family_name": person_c.family_name,
                            "given_name": person_c.given_name,
                            "birth_date": person_c.birth_date,
                            "birth_date_s": person_c.birth_date,
                            "birth_location": person_c.birth_location,
                            "score": 7.349502,
                        },
                    ],
                },
            }
        )

        response = self.client.get(
            "{}/search{}".format(self.endpoint, search_params), format="json"
        )
        self.mock_sqs_patcher.stop()
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEquals(content["count"], 1)
        self.assertEqual(content["results"][0]["family_name"], person_a.family_name)

    def test_search_person_no_given_name(self):
        """Test search a person with only family_name (validation)
        """
        search_params = "?family_name={}".format(faker.last_name())
        response = self.client.get(
            "{}/search{}".format(self.endpoint, search_params), format="json"
        )
        self.assertEquals(response.status_code, 500)
        content = json.loads(response.content)
        self.assertEquals("given_name field must be passed" in content["detail"], True)

    def test_search_person_no_family_name(self):
        """Test search a person with only given_name (validation)
        """
        search_params = "?given_name={}".format(faker.first_name())
        response = self.client.get(
            "{}/search{}".format(self.endpoint, search_params), format="json"
        )
        self.assertEquals(response.status_code, 500)
        content = json.loads(response.content)
        self.assertEquals("family_name field must be passed" in content["detail"], True)

    def test_filter_by_identifier_scheme_and_value(self):
        """Test fetching of persons filtered by identifier scheme and value.
        It should always return one record.
        :return:
        """
        i = IdentifierFactory()
        p = PersonFactory()
        p.add_identifier(scheme=i.scheme, identifier=i.identifier)
        PersonFactory.create()
        response = self.client.get(
            "{0}?identifier_scheme={1}&identifier_value={2}".format(
                self.endpoint, i.scheme, i.identifier
            ),
            format="json",
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Person.objects.filter(
                identifiers__scheme=i.scheme, identifiers__identifier=i.identifier
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_cf_id(self):
        i = IdentifierFactory(scheme="CF", identifier=faker.pystr(max_chars=11))
        p = PersonFactory()
        p.add_identifier(scheme=i.scheme, identifier=i.identifier)
        PersonFactory.create()
        response = self.client.get(
            "{0}?cf_id={1}".format(self.endpoint, i.identifier), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Person.objects.filter(
                identifiers__scheme=i.scheme, identifiers__identifier=i.identifier
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_op_id(self):
        i = IdentifierFactory(scheme="OP_ID")
        p = PersonFactory()
        p.add_identifier(scheme=i.scheme, identifier=i.identifier)
        PersonFactory.create()
        response = self.client.get(
            "{0}?op_id={1}".format(self.endpoint, i.identifier), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            Person.objects.filter(
                identifiers__scheme=i.scheme, identifiers__identifier=i.identifier
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_base_filters_birth_date(self):
        """Test filtering items by birth_date
        :return:
        """

        birth_date = faker.date_between(start_date="-33y", end_date="-17y").strftime(
            "%Y-%m-%d"
        )
        death_date = faker.date_between(start_date="-13y", end_date="-2y").strftime(
            "%Y-%m-%d"
        )
        self.model_factory_class.create(birth_date=birth_date, death_date=death_date)
        self.model_factory_class.create(birth_date=birth_date, death_date=death_date)
        self.model_factory_class.create(birth_date=birth_date, death_date=death_date)

        response = self.client.get(
            "{0}?birth_date={1}".format(self.endpoint, birth_date), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(content["count"], 3)

    def test_base_filters_death_date(self):
        """Test filtering items by death_date
        :return:
        """

        birth_date = faker.date_between(start_date="-33y", end_date="-17y").strftime(
            "%Y-%m-%d"
        )
        death_date = faker.date_between(start_date="-13y", end_date="-2y").strftime(
            "%Y-%m-%d"
        )
        self.model_factory_class.create(birth_date=birth_date, death_date=death_date)
        self.model_factory_class.create(birth_date=birth_date, death_date=death_date)
        self.model_factory_class.create(birth_date=birth_date, death_date=death_date)

        response = self.client.get(
            "{0}?death_date={1}".format(self.endpoint, death_date), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(content["count"], 3)

    def test_base_filters_death_date_literal(self):
        """Test filtering items by death_date literal
        :return:
        """

        birth_date = faker.date_between(start_date="-33y", end_date="-17y").strftime(
            "%Y-%m-%d"
        )
        today = datetime.strftime(datetime.today(), "%Y-%m-%d")
        self.model_factory_class.create(birth_date=birth_date, death_date=today)

        response = self.client.get(
            "{0}?death_date={1}".format(self.endpoint, today), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(content["count"], 1)

    def test_create_person_with_classifications(self):
        """ Test creation of a person with three different classifications

        :return:
        """
        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()

        # test various methods to add a classification
        classifications = [
            {"classification": classification_a.id},  # existing classification id
            {
                "scheme": faker.word(),
                "code": faker.ssn(),
                "descr": faker.word(),
            },  # non-existing classification by dict
            {
                "scheme": classification_b.scheme,
                "code": classification_b.code,
            },  # existing classification by dict
            {
                "scheme": classification_b.scheme,
                "code": faker.word(),
            },  # no duplication of same-scheme-class
        ]

        person = self.get_basic_data()
        person["classifications"] = classifications
        response = self.client.post(self.endpoint, person, format="json")

        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 3)

        response = self.client.get(self.endpoint + "/classifications", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 3)

        response = self.client.get(self.endpoint + "/classification_types", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 3)

    def test_create_persons_classifications_donot_repeat(self):
        """ Test creation of 2 organizations with the same 2 classifications.

        The total amount of classifications is 2.

        :return:
        """
        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()
        classifications = [
            {"classification": classification_a.id},
            {"classification": classification_b.id},
        ]

        person = self.get_basic_data()
        person["classifications"] = classifications
        response = self.client.post(self.endpoint, person, format="json")

        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 2)

        response = self.client.get(self.endpoint + "/classifications", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 2)

        person_b = self.get_basic_data()
        person_b["classifications"] = classifications
        response = self.client.post(self.endpoint, person_b, format="json")
        self.assertEquals(
            response.status_code, status.HTTP_201_CREATED, response.content
        )

        r = json.loads(response.content)
        self.assertEquals(len(r["classifications"]), 2)

        response = self.client.get(self.endpoint + "/classifications", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 2)

        response = self.client.get(self.endpoint + "/classification_types", format="json")
        r = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        self.assertEquals(r["count"], 2)


class ProfessionsTestCase(
    mixins.APIResourceWithIdentifiersTestCase,
    mixins.APIResourceCRUDTestCase,
    mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/persons/professions"
    model_factory_class = ProfessionFactory

    @staticmethod
    def get_basic_data():
        return {"name": faker.pystr()}


class OriginalProfessionsTestCase(
    mixins.APIResourceCRUDTestCase, mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/persons/original_professions"
    model_factory_class = OriginalProfessionFactory

    @staticmethod
    def get_basic_data():
        return {"name": faker.pystr(), "normalized_profession": ProfessionFactory().id}

    def test_update_part_original_profession_simple(self):
        """Test original profession partial update (PATCH)
        """
        with factory.Faker.override_default_locale(locale):
            profession = self.model_factory_class()

        patched_data = {"name": "TEST VALUE"}
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(profession.id)),
            patched_data,
            format="json",
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        for k in patched_data.keys():
            self.assertEquals(content[k], patched_data[k])


class EducationLevelsTestCase(
    mixins.APIResourceWithIdentifiersTestCase,
    mixins.APIResourceCRUDTestCase,
    mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/persons/education_levels"
    model_factory_class = EducationLevelFactory

    @staticmethod
    def get_basic_data():
        return {"name": faker.pystr()}


class OriginalEducationLevelsTestCase(
    mixins.APIResourceCRUDTestCase, mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/persons/original_education_levels"
    model_factory_class = OriginalEducationLevelFactory

    @staticmethod
    def get_basic_data():
        return {
            "name": faker.pystr(),
            "normalized_education_level": EducationLevelFactory().id,
        }

    def test_update_part_original_education_level_simple(self):
        """Test original profession partial update (PATCH)
        """
        with factory.Faker.override_default_locale(locale):
            edlevel = self.model_factory_class()

        patched_data = {"name": "TEST VALUE"}
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(edlevel.id)), patched_data, format="json"
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        for k in patched_data.keys():
            self.assertEquals(content[k], patched_data[k])


class RoleTypesTestCase(
    mixins.APIResourceCRUDTestCase, mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/persons/role_types"
    model_factory_class = RoleTypeFactory

    @staticmethod
    def get_basic_data():
        c = ClassificationFactory()
        c.scheme = "FORMA_GIURIDICA_OP"
        c.save()

        return {
            "label": faker.pystr(),
            "classification": c.id,
            "priority": faker.pyint(),
        }

    def test_filter_organization_id(self):
        """Test fetching of role_types filtered by organization_id
        :return:
        """
        cl = ClassificationFactory.create()
        org = OrganizationFactory.create(classification=cl.descr)
        org.add_classification_rel(cl)
        RoleTypeFactory.create(classification=cl)
        RoleTypeFactory.create()
        response = self.client.get(
            "{0}?organization={1}".format(self.endpoint, org.id), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            RoleType.objects.filter(
                classification__descr__iexact=org.classification
            ).count(),
            content,
        )

    def test_filter_non_existing_organization_id_returns_zero_results(self):
        """Test fetching of role_types filtered by organization_id

        when the organization_id points to a non-existing Organization

        :return:
        """
        cl = ClassificationFactory.create()
        org = OrganizationFactory.create(classification=cl.descr)
        org.add_classification_rel(cl)
        RoleTypeFactory.create(classification=cl)
        RoleTypeFactory.create()
        response = self.client.get(
            "{0}?organization={1}".format(self.endpoint, 99), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(content["count"], 0)

    def test_filter_forma_giuridica(self):
        """Test fetching of role_types filtered by forma_giuridica
        :return:
        """
        cl = ClassificationFactory.create(scheme="FORMA_GIURIDICA_OP")
        RoleTypeFactory.create(classification=cl)
        RoleTypeFactory.create()
        response = self.client.get(
            "{0}?forma_giuridica={1}".format(self.endpoint, cl.descr), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            RoleType.objects.filter(classification__descr__iexact=cl.descr).count(),
        )
