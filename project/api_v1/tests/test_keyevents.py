import json

from popolo.models import KeyEvent
from popolo.tests.factories import LegislatureEventFactory
from rest_framework import status

from project.api_v1.tests.utils import mixins
from . import faker


class KeyEventTestCase(
    mixins.APIResourceCRUDTestCase, mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/keyevents"
    model_factory_class = LegislatureEventFactory

    @staticmethod
    def get_basic_data():
        return {
            "name": faker.sentence(nb_words=3),
            "identifier": faker.pystr(max_chars=11),
            "event_type": "ITL",
            "start_date": faker.date(pattern="%Y-%m-%d", end_datetime="-27y"),
        }

    def test_partial_update(self):
        e = self.model_factory_class.create()
        request_body = {"name": "test_name"}
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=e.id),
            request_body,
            format="json",
        )
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        content = json.loads(response.content)
        self.assertEquals(content["name"], request_body["name"])
        self.assertEquals(content["identifier"], e.identifier)
        self.assertEquals(content["event_type"], e.event_type)

    def test_filter_by_name(self):
        name = faker.pystr(max_chars=12)
        LegislatureEventFactory.create(name=name)
        LegislatureEventFactory.create()
        response = self.client.get(
            "{0}?name={1}".format(self.endpoint, name), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"], KeyEvent.objects.filter(name__icontains=name).count()
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_name_contained(self):
        name = faker.pystr(max_chars=12)
        LegislatureEventFactory.create(name=name)
        partial_name = name[: int(len(name) / 2)]
        LegislatureEventFactory.create()
        response = self.client.get(
            "{0}?name={1}".format(self.endpoint, partial_name), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            KeyEvent.objects.filter(name__icontains=partial_name).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_identifier(self):
        identifier = faker.pystr(max_chars=12)
        LegislatureEventFactory.create(identifier=identifier)
        LegislatureEventFactory.create()
        response = self.client.get(
            "{0}?identifier={1}".format(self.endpoint, identifier), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            KeyEvent.objects.filter(identifier__iexact=identifier).count(),
        )
        self.assertEqual(content["count"], 1)
