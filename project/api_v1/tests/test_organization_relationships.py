import json

import factory
from popolo.models import OrganizationRelationship
from popolo.tests.factories import (
    OrganizationFactory,
    ClassificationFactory,
    OrganizationRelationshipFactory)

from project.api_v1.tests.utils import mixins
from . import faker, locale


class OrganizationRelationshipTestCase(
    mixins.APIResourceWithSourcesTestCase,
    mixins.APIResourceTimestampableTestCase,
    mixins.APIResourceDateframeableTestCase,
    mixins.APIResourceCRUDTestCase,
    mixins.APIResourceTestCase,
):
    endpoint = "/api-mappepotere/v1/organization_relationships"
    model_factory_class = OrganizationRelationshipFactory

    @staticmethod
    def get_basic_data():
        source_organization = OrganizationFactory.create()
        dest_organization = OrganizationFactory.create()
        classification = ClassificationFactory.create(
            scheme='OP_TIPO_RELAZIONE_ORG',
            code='OT_01',
            descr='Vigilanza'
        )

        return {
            "source_organization": source_organization.id,
            "dest_organization": dest_organization.id,
            "weight": faker.pyint(min_value=-2, max_value=2),
            "descr": faker.sentence(nb_words=8),
            "classification": classification.id,
            "start_date": faker.date_between(start_date="-3y", end_date="-2y").strftime(
                "%Y-%m-%d"
            ),
            "end_date": faker.date_between(start_date="-2y", end_date="-1y").strftime(
                "%Y-%m-%d"
            ),
        }

    def test_create_relationship(self):
        """Test membership creation, with a post.

        post is sent as simple ID.

        :return:
        """
        relationship = self.get_basic_data()
        response = self.client.post(self.endpoint, relationship, format="json")
        self.assertEquals(response.status_code, 201, response.content)

    def test_update_part_relationship_with_person_and_post(self):
        """Test basic relationship partial update (PATCH)
        """
        with factory.Faker.override_default_locale(locale):
            relationship = OrganizationRelationshipFactory()

        patched_data = {
            "weight": relationship.weight * -1,
            "descr": faker.sentence(nb_words=10),
        }
        response = self.client.patch(
            "{}/{}".format(self.endpoint, str(relationship.id)),
            patched_data,
            format="json",
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        for k in patched_data.keys():
            if isinstance(content[k], dict):
                c = content[k].pop("id")
            else:
                c = content[k]
            self.assertEquals(c, patched_data[k])

    def test_get_relations_from_organizations(self):
        """Test endpoint to read relations having the given organization as a destination

        :return:
        """
        with factory.Faker.override_default_locale(locale):
            relationship = OrganizationRelationshipFactory()

            dest_organization = relationship.dest_organization
            n_sources = faker.pyint(max_value=10)
            for _ in range(n_sources):
                OrganizationRelationship.objects.create(
                    source_organization=OrganizationFactory.create(),
                    dest_organization=dest_organization,
                    classification=relationship.classification,
                    weight=faker.pyint(min_value=-2, max_value=2)
                )

        response = self.client.get(
            f"/api-mappepotere/v1/organizations/{dest_organization.id}/relations_from_organizations",
            format="json",
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEquals(content['count'], n_sources + 1, "Wrong number of sources")

    def test_get_relations_to_organizations(self):
        """Test endpoint to read relations having the given organization as a source

        :return:
        """
        with factory.Faker.override_default_locale(locale):
            relationship = OrganizationRelationshipFactory()

            source_organization = relationship.source_organization
            n_dests = faker.pyint(max_value=10)
            for _ in range(n_dests):
                OrganizationRelationship.objects.create(
                    source_organization=source_organization,
                    dest_organization=OrganizationFactory.create(),
                    classification=relationship.classification,
                    weight=faker.pyint(min_value=-2, max_value=2)
                )

        response = self.client.get(
            f"/api-mappepotere/v1/organizations/{source_organization.id}/relations_to_organizations",
            format="json", no_page="true"
        )
        self.assertEquals(response.status_code, 200, response.content)
        content = json.loads(response.content)
        self.assertEquals(content['count'], n_dests + 1, "Wrong number of dests")
