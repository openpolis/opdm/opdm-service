# coding=utf-8
import datetime
import json
import random
from unittest.mock import patch

from django.contrib.contenttypes.models import ContentType
import factory
from popolo.models import Person, Membership, ContactDetail
from popolo.tests.factories import (
    PersonFactory,
    OrganizationFactory,
    PostFactory,
    ElectoralEventFactory,
    RoleTypeFactory,
)
from rest_framework import status

from project.akas.models import AKA
from . import faker
from .utils import mixins


class AKAFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AKA

    n_similarities = faker.pyint()

    @factory.lazy_attribute
    def loader_context(self):
        given_name = faker.first_name()
        family_name = faker.last_name()
        role_type = RoleTypeFactory.create()
        return {
            "item": {
                "family_name": family_name,
                "given_name": given_name,
                "name": "{0} {1}".format(given_name, family_name),
                "honorific_prefix": "",
                "birth_date": faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
                "birth_location": faker.city(),
                "gender": "M" if faker.boolean() else "F",
                "profession": faker.job(),
                "education_level": faker.sentence(nb_words=3, variable_nb_words=False),
                "start_date": faker.date_between(
                    start_date="-10y", end_date="-7y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-6y", end_date="-4y"
                ).strftime("%Y-%m-%d"),
                "role_type_label": role_type.label,
            },
            "post_id": PostFactory.create(
                organization=OrganizationFactory.create(), role_type=role_type
            ).id,
            "election_id": ElectoralEventFactory.create().id,
        }

    @factory.lazy_attribute
    def search_params(self):
        item = self._Resolver__values["loader_context"]["item"]
        return {
            "family_name": item["family_name"].lower(),
            "given_name": item["given_name"].lower(),
            "birth_date": item["birth_date"],
            "birth_location": item["birth_location"].lower(),
        }


class PersonAKATestCase(mixins.APIResourceTestCase):
    endpoint = "/api-mappepotere/v1/persons/akas"
    model_factory_class = AKAFactory

    # solr communications are mocked and silenced, to avoid output chaos on tests

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.mock_sqs_select_patcher = patch("pysolr.Solr._select")
        cls.mock_sqs_select = cls.mock_sqs_select_patcher.start()
        cls.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "OP_ID_s:(1234)",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        cls.mock_sqs_update_patcher = patch("pysolr.Solr._update")
        cls.mock_sqs_update = cls.mock_sqs_update_patcher.start()
        cls.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

    @classmethod
    def tearDownClass(cls):
        cls.mock_sqs_select_patcher.stop()
        cls.mock_sqs_update_patcher.stop()
        super().tearDownClass()

    @staticmethod
    def get_basic_data():
        return {
            "search_params": json.dumps(
                {
                    "given_name": faker.first_name(),
                    "family_name": faker.last_name(),
                    "birth_date": faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
                    "birth_location": faker.city(),
                }
            ),
            "n_similarities": faker.random_int(min=0, max=30),
            "loader_context": json.dumps({"test": "uoz"}),
            "content_type_id": None,
            "object_id": None,
            "is_resolved": False,
        }

    def test_create_aka_fails(self):
        """
        Test aka creation failure
        """
        request_body = self.get_basic_data()
        response = self.client.post(self.endpoint, request_body, format="json")
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_full_update_fails(self):
        aka = self.model_factory_class.create()
        request_body = dict()
        request_body.update(self.get_basic_data())
        request_body.update({"is_resolved": True})
        response = self.client.put(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_partial_update_create_new_person(self):
        aka = self.model_factory_class.create()
        request_body = dict()
        request_body.update({"is_resolved": True})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertNotEqual(content["object_id"], 0)

        # test that the correct Person instance was created
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(
            Person.objects.first().family_name,
            aka.loader_context["item"]["family_name"],
        )

    def test_partial_update_with_object_id_updates_person(self):
        person = PersonFactory.create()
        organization = OrganizationFactory.create()
        election = ElectoralEventFactory.create()
        role_type = RoleTypeFactory.create()
        updated_family_name = person.family_name + " " + faker.last_name()
        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": updated_family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, updated_family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": faker.date_between(
                        start_date="-10y", end_date="-7y"
                    ).strftime("%Y-%m-%d"),
                    "end_date": faker.date_between(
                        start_date="-6y", end_date="-4y"
                    ).strftime("%Y-%m-%d"),
                    "role_type_label": role_type.label,
                },
                "post_id": PostFactory.create(
                    organization=organization, role_type=role_type
                ).id,
                "election_id": election.id,
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)

        # test that the correct Person instance was updated
        # AKA name is added to other_names automatically
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Person.objects.first().family_name, person.family_name)
        self.assertEqual(
            Person.objects.first()
            .other_names.filter(othername_type="AKA")
            .first()
            .name,
            "{0} {1}".format(person.given_name, updated_family_name),
        )
        self.assertEqual(
            Person.objects.first().original_profession.name,
            aka.loader_context["item"]["profession"],
        )

    def test_partial_update_with_post_id_add_role(self):
        person = PersonFactory.create()
        organization = OrganizationFactory.create()
        election = ElectoralEventFactory.create()
        role_type = RoleTypeFactory.create()
        post = PostFactory.create(organization=organization, role_type=role_type)

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": faker.date_between(
                        start_date="-10y", end_date="-7y"
                    ).strftime("%Y-%m-%d"),
                    "end_date": faker.date_between(
                        start_date="-6y", end_date="-4y"
                    ).strftime("%Y-%m-%d"),
                    "role_type_label": role_type.label,
                },
                "post_id": post.id,
                "election_id": election.id,
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)

        # test that a Membership was added, and that it has the correct data
        self.assertEqual(Membership.objects.count(), 1)
        self.assertEqual(Membership.objects.first().post_id, post.id)
        self.assertEqual(
            Membership.objects.first().post.organization_id, organization.id
        )
        self.assertEqual(Membership.objects.first().electoral_event_id, election.id)
        self.assertEqual(
            Membership.objects.first().label,
            aka.loader_context["item"]["role_type_label"],
        )

    def test_partial_update_with_organization_id_add_membership(self):
        person = PersonFactory.create()
        organization = OrganizationFactory.create()
        election = ElectoralEventFactory.create()

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": faker.date_between(
                        start_date="-10y", end_date="-7y"
                    ).strftime("%Y-%m-%d"),
                    "end_date": faker.date_between(
                        start_date="-6y", end_date="-4y"
                    ).strftime("%Y-%m-%d"),
                    "role_type_label": faker.sentence(
                        nb_words=5, variable_nb_words=False
                    ),
                    "org_id": organization.id,
                },
                "election_id": election.id,
            }
        )

        # test AKA str representation (coverage)
        self.assertEquals("similarities" in str(aka), True)

        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)
        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)

        # test that a Membership was added, and that it has the correct data
        self.assertEqual(Membership.objects.count(), 1)
        self.assertEqual(Membership.objects.first().organization_id, organization.id)
        self.assertEqual(Membership.objects.first().electoral_event_id, election.id)

    def test_partial_update_with_overlapping_role_more_than_6_months(self):
        person = PersonFactory.create()
        organization = OrganizationFactory.create()
        election = ElectoralEventFactory.create()
        post = PostFactory.create(organization=organization)

        start_date = faker.date_between(start_date="-12y", end_date="-12y")
        end_date = faker.date_between(start_date="-3y", end_date="-1y")

        start_date_old = start_date.strftime("%Y-%m-%d")
        end_date_old = end_date.strftime("%Y-%m-%d")
        start_date_new = (start_date + datetime.timedelta(weeks=50)).strftime(
            "%Y-%m-%d"
        )
        end_date_new = (end_date - datetime.timedelta(weeks=50)).strftime("%Y-%m-%d")
        label = faker.sentence(nb_words=3)

        person.add_role(
            post, label=label, start_date=start_date_old, end_date=end_date_old
        )

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name + " TEST",
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": start_date_new,
                    "end_date": end_date_new,
                    "role_type_label": label,
                    "org_id": organization.id,
                },
                "post_id": post.id,
                "election_id": election.id,
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)

        # test that Person and Membership were not created anew
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)

        self.assertEqual("TEST" in Person.objects.first().family_name, False)

        # test that the membership start and end dates are not changed
        self.assertEqual(Membership.objects.first().start_date, start_date_old)
        self.assertEqual(Membership.objects.first().end_date, end_date_old)

    def test_partial_update_with_overlapping_role_less_than_n_months(self):
        person = PersonFactory.create()
        organization = OrganizationFactory.create()
        election = ElectoralEventFactory.create()
        post = PostFactory.create(organization=organization)
        label = faker.sentence(nb_words=3)

        start_date = faker.date_between(start_date="-12y", end_date="-12y")
        end_date = faker.date_between(start_date="-3y", end_date="-1y")

        start_date_old = start_date.strftime("%Y-%m-%d")
        end_date_old = end_date.strftime("%Y-%m-%d")
        start_date_new = (start_date + datetime.timedelta(weeks=3)).strftime("%Y-%m-%d")
        end_date_new = (end_date - datetime.timedelta(weeks=10)).strftime("%Y-%m-%d")

        person.add_role(
            post, label=label, start_date=start_date_old, end_date=end_date_old
        )

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": start_date_new,
                    "end_date": end_date_new,
                    "role_type_label": label,
                    "org_id": organization.id,
                },
                "post_id": post.id,
                "election_id": election.id,
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)

        # test that a Person was created and a new Membership was not created
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)

        # test that the membership start and end dates are not changed
        self.assertEqual(Membership.objects.first().start_date, start_date_new)
        self.assertEqual(Membership.objects.first().end_date, end_date_new)

    def test_partial_update_with_overlapping_role_less_than_6_months_keep_old_strategy(
        self,
    ):
        person = PersonFactory.create()
        organization = OrganizationFactory.create()
        election = ElectoralEventFactory.create()
        post = PostFactory.create(organization=organization)

        start_date = faker.date_between(start_date="-12y", end_date="-12y")
        end_date = faker.date_between(start_date="-3y", end_date="-1y")

        start_date_old = start_date.strftime("%Y-%m-%d")
        end_date_old = end_date.strftime("%Y-%m-%d")
        start_date_new = (start_date + datetime.timedelta(weeks=10)).strftime(
            "%Y-%m-%d"
        )
        end_date_new = (end_date - datetime.timedelta(weeks=3)).strftime("%Y-%m-%d")

        label = faker.sentence(nb_words=3)

        person.add_role(
            post, label=label, start_date=start_date_old, end_date=end_date_old
        )

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": start_date_new,
                    "end_date": end_date_new,
                    "role_type_label": label,
                    "org_id": organization.id,
                },
                "memberships_update_strategy": "keep_old",
                "post_id": post.id,
                "election_id": election.id,
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)

        # test that a Person was created and a new Membership was not created
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)

        # test that the membership start and end dates are not changed
        self.assertEqual(Membership.objects.first().start_date, start_date_old)
        self.assertEqual(Membership.objects.first().end_date, end_date_old)

    def test_partial_update_with_overlapping_roles_less_than_6_months_consecutive_roles(
        self,
    ):
        """When a role starting near another has the starting date equal to the ending date (consecutive roles)
        both roles are imported.

        :return:
        """
        person = PersonFactory.create()
        organization = OrganizationFactory.create()
        election = ElectoralEventFactory.create()
        post = PostFactory.create(organization=organization)

        start_date = faker.date_between(start_date="-12y", end_date="-12y")
        end_date = faker.date_between(start_date="-3y", end_date="-1y")

        start_date_old = start_date.strftime("%Y-%m-%d")
        end_date_old = end_date.strftime("%Y-%m-%d")
        start_date_new = end_date_old
        end_date_new = None

        label = faker.sentence(nb_words=3)

        person.add_role(
            post, label=label, start_date=start_date_old, end_date=end_date_old
        )

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": start_date_new,
                    "end_date": end_date_new,
                    "role_type_label": label,
                    "org_id": organization.id,
                },
                "memberships_update_strategy": "keep_old",
                "post_id": post.id,
                "election_id": election.id,
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)

        # test that a Person was created and the new membership was indeed created
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 2)

        # test that the membership start and end dates are not changed
        # self.assertEqual(Membership.objects.first().start_date, start_date_old)
        # self.assertEqual(Membership.objects.first().end_date, end_date_old)

    def test_partial_update_with_embedded_memberships(self):
        person = PersonFactory.create()
        organization = OrganizationFactory.create()
        role_type = RoleTypeFactory.create(label=faker.sentence(nb_words=2))
        post = PostFactory.create(
            organization=organization, role_type=role_type, role=role_type.label
        )

        contact_details = [
            {
                "label": " ".join(faker.words()),
                "contact_type": random.choice(
                    [a[0] for a in ContactDetail.CONTACT_TYPES]
                ),
                "note": faker.paragraph(2),
                "value": faker.pystr(max_chars=32),
            }
        ]
        memberships = [
            {
                "organization_id": organization.id,
                "label": post.label,
                "role": post.role,
                "start_date": faker.date_between(
                    start_date="-3y", end_date="-2y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-1y", end_date="-6m"
                ).strftime("%Y-%m-%d"),
            },
            {
                "organization_id": organization.id,
                "label": post.label,
                "role": post.role,
                "start_date": faker.date_between(
                    start_date="-3m", end_date="-2m"
                ).strftime("%Y-%m-%d"),
                "contact_details": [
                    {
                        "label": " ".join(faker.words()),
                        "contact_type": random.choice(
                            [a[0] for a in ContactDetail.CONTACT_TYPES]
                        ),
                        "note": faker.paragraph(2),
                        "value": faker.pystr(max_chars=32),
                    }
                ],
                "sources": [{"note": faker.paragraph(2), "url": faker.uri()}],
            },
        ]

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": faker.date_between(
                        start_date="-10y", end_date="-7y"
                    ).strftime("%Y-%m-%d"),
                    "end_date": faker.date_between(
                        start_date="-6y", end_date="-4y"
                    ).strftime("%Y-%m-%d"),
                    "contact_details": contact_details,
                    "memberships": memberships,
                }
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)
        self.assertEqual(Person.objects.first().contact_details.count(), 1)

        # test that a Membership was added, and that it has the correct data
        self.assertEqual(Membership.objects.count(), 2)
        self.assertEqual(
            Membership.objects.filter(contact_details__isnull=False).count(), 1
        )
        self.assertEqual(Membership.objects.filter(label=post.label).count(), 2)

    def test_partial_update_with_embedded_memberships_same_post_different_labels_check_label_false(
        self,
    ):
        person = PersonFactory.create()
        post_role = faker.sentence(nb_words=2)
        organization = OrganizationFactory.create()
        role_type = RoleTypeFactory.create(label=post_role)
        post = PostFactory.create(
            organization=organization, role_type=role_type, role=post_role
        )

        contact_details = [
            {
                "label": " ".join(faker.words()),
                "contact_type": random.choice(
                    [a[0] for a in ContactDetail.CONTACT_TYPES]
                ),
                "note": faker.paragraph(2),
                "value": faker.pystr(max_chars=32),
            }
        ]
        memberships = [
            {
                "organization_id": organization.id,
                "label": faker.sentence(nb_words=3),
                "role": post.role,
                "start_date": faker.date_between(
                    start_date="-3y", end_date="-2y"
                ).strftime("%Y-%m-%d"),
            },
            {
                "organization_id": organization.id,
                "label": faker.sentence(nb_words=3),
                "role": post.role,
                "start_date": faker.date_between(
                    start_date="-2y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-1y", end_date="-6m"
                ).strftime("%Y-%m-%d"),
                "contact_details": [
                    {
                        "label": " ".join(faker.words()),
                        "contact_type": random.choice(
                            [a[0] for a in ContactDetail.CONTACT_TYPES]
                        ),
                        "note": faker.paragraph(2),
                        "value": faker.pystr(max_chars=32),
                    }
                ],
                "sources": [{"note": faker.paragraph(2), "url": faker.uri()}],
            },
        ]

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": faker.date_between(
                        start_date="-10y", end_date="-7y"
                    ).strftime("%Y-%m-%d"),
                    "end_date": faker.date_between(
                        start_date="-6y", end_date="-4y"
                    ).strftime("%Y-%m-%d"),
                    "contact_details": contact_details,
                    "memberships": memberships,
                }
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)
        self.assertEqual(Person.objects.first().contact_details.count(), 1)

        # test that a Membership was added,  no duplication even if different labels, because they were not checked
        self.assertEqual(Membership.objects.count(), 1)

    def test_partial_update_with_embedded_memberships_same_post_different_labels_check_label_true(
        self,
    ):
        person = PersonFactory.create()
        post_role = faker.sentence(nb_words=2)
        organization = OrganizationFactory.create()
        role_type = RoleTypeFactory.create(label=post_role)
        post = PostFactory.create(
            organization=organization, role_type=role_type, role=post_role
        )

        contact_details = [
            {
                "label": " ".join(faker.words()),
                "contact_type": random.choice(
                    [a[0] for a in ContactDetail.CONTACT_TYPES]
                ),
                "note": faker.paragraph(2),
                "value": faker.pystr(max_chars=32),
            }
        ]
        memberships = [
            {
                "organization_id": organization.id,
                "label": faker.sentence(nb_words=3),
                "role": post.role,
                "start_date": faker.date_between(
                    start_date="-3y", end_date="-2y"
                ).strftime("%Y-%m-%d"),
            },
            {
                "organization_id": organization.id,
                "label": faker.sentence(nb_words=3),
                "role": post.role,
                "start_date": faker.date_between(
                    start_date="-2y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-1y", end_date="-6m"
                ).strftime("%Y-%m-%d"),
                "contact_details": [
                    {
                        "label": " ".join(faker.words()),
                        "contact_type": random.choice(
                            [a[0] for a in ContactDetail.CONTACT_TYPES]
                        ),
                        "note": faker.paragraph(2),
                        "value": faker.pystr(max_chars=32),
                    }
                ],
                "sources": [{"note": faker.paragraph(2), "url": faker.uri()}],
            },
        ]

        aka = self.model_factory_class.create(
            loader_context={
                "item": {
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "name": "{0} {1}".format(person.given_name, person.family_name),
                    "honorific_prefix": person.honorific_prefix,
                    "birth_date": person.birth_date,
                    "birth_location": person.birth_location,
                    "gender": person.gender,
                    "profession": faker.job(),
                    "education_level": faker.sentence(
                        nb_words=3, variable_nb_words=False
                    ),
                    "start_date": faker.date_between(
                        start_date="-10y", end_date="-7y"
                    ).strftime("%Y-%m-%d"),
                    "end_date": faker.date_between(
                        start_date="-6y", end_date="-4y"
                    ).strftime("%Y-%m-%d"),
                    "contact_details": contact_details,
                    "check_membership_label": True,
                    "memberships": memberships,
                }
            }
        )
        request_body = dict()
        request_body.update({"is_resolved": True, "object_id": person.id})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        # test correct response
        self.assertEquals(response.status_code, status.HTTP_200_OK, response.content)

        # test correct content was sent as response
        content = json.loads(response.content)
        self.assertEqual(content["is_resolved"], True)
        self.assertNotEqual(content["content_type_id"], 0)
        self.assertEqual(content["object_id"], person.id)
        self.assertEqual(Person.objects.first().contact_details.count(), 1)

        # test that a Membership was added, and that it has the correct data
        self.assertEqual(Membership.objects.count(), 2)
        self.assertEqual(
            Person.objects.first().memberships.values("post_id").distinct().count(), 1
        )
        self.assertEqual(
            Person.objects.first()
            .memberships.values("organization_id")
            .distinct()
            .count(),
            1,
        )
        self.assertEqual(
            Person.objects.first().memberships.values("label").distinct().count(), 2
        )
        self.assertEqual(
            Membership.objects.filter(contact_details__isnull=False).count(), 1
        )

    def test_partial_update_wrong_is_resolved_fails(self):
        aka = self.model_factory_class.create()
        request_body = dict()
        request_body.update({"is_resolved": "pippo"})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        self.assertEquals(response.status_code, 422, response.content)

    def test_partial_update_is_resolved_false_with_object_id_fails(self):
        aka = self.model_factory_class.create()
        request_body = dict()
        request_body.update({"is_resolved": False, "object_id": 10})
        response = self.client.patch(
            "{endpoint}/{id}".format(endpoint=self.endpoint, id=aka.id),
            request_body,
            format="json",
        )
        self.assertEquals(response.status_code, 422, response.content)

    def test_list(self):
        obj_list = self.model_factory_class.create_batch(10)
        response = self.client.get(self.endpoint, format="json")
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(len(obj_list), len(content["results"]))
        self.assertEquals(len(obj_list), content["count"])

    def test_retrieve(self):
        obj = self.model_factory_class.create()
        response = self.client.get(
            "{0}/{1}".format(self.endpoint, obj.id), format="json"
        )
        content = json.loads(response.content)
        self.assertEquals(response.status_code, status.HTTP_200_OK)
        self.assertEquals(obj.id, content["id"])

    def test_retrieve_non_existing(self):
        response = self.client.get(
            self.endpoint + "/probably-non-existing-resource-id", format="json"
        )
        self.assertEquals(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_fails(self):
        aka = self.model_factory_class.create()
        response = self.client.delete(self.endpoint + "/" + str(aka.id), format="json")
        self.assertEquals(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_filter_by_search_params_family_name(self):
        search_params = {
            "given_name": faker.first_name(),
            "family_name": faker.last_name(),
            "birth_date": faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
            "birth_location": faker.city(),
        }
        AKAFactory(search_params=search_params)
        AKAFactory.create()
        response = self.client.get(
            "{0}?search_family_name={1}".format(
                self.endpoint, search_params["family_name"]
            ),
            format="json",
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            AKA.objects.filter(
                search_params__family_name__iexact=search_params["family_name"]
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_search_params_given_name(self):
        search_params = {
            "given_name": faker.first_name(),
            "family_name": faker.last_name(),
            "birth_date": faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
            "birth_location": faker.city(),
        }
        AKAFactory(search_params=search_params)
        AKAFactory.create()
        response = self.client.get(
            "{0}?search_given_name={1}".format(
                self.endpoint, search_params["given_name"]
            ),
            format="json",
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            AKA.objects.filter(
                search_params__given_name__iexact=search_params["given_name"]
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_op_context(self):
        given_name = faker.first_name()
        family_name = faker.last_name()
        role_type = RoleTypeFactory.create()
        loader_context = {
            "item": {
                "family_name": family_name,
                "given_name": given_name,
                "name": "{0} {1}".format(given_name, family_name),
                "honorific_prefix": "",
                "birth_date": faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
                "birth_location": faker.city(),
                "gender": "M" if faker.boolean() else "F",
                "profession": faker.job(),
                "education_level": faker.sentence(nb_words=3, variable_nb_words=False),
                "start_date": faker.date_between(
                    start_date="-10y", end_date="-7y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-6y", end_date="-4y"
                ).strftime("%Y-%m-%d"),
                "role_type_label": role_type.label,
            },
            "post_id": PostFactory.create(
                organization=OrganizationFactory.create(), role_type=role_type
            ).id,
            "election_id": ElectoralEventFactory.create().id,
            "context": faker.pystr(max_chars=12),
        }
        AKAFactory(loader_context=loader_context)
        AKAFactory.create()
        response = self.client.get(
            "{0}?op_context={1}".format(self.endpoint, loader_context["context"]),
            format="json",
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            AKA.objects.filter(
                loader_context__context__iexact=loader_context["context"]
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_loc_desc(self):
        given_name = faker.first_name()
        family_name = faker.last_name()
        loader_context = {
            "item": {
                "family_name": family_name,
                "given_name": given_name,
                "name": "{0} {1}".format(given_name, family_name),
                "honorific_prefix": "",
                "birth_date": faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
                "birth_location": faker.city(),
                "gender": "M" if faker.boolean() else "F",
                "profession": faker.job(),
                "education_level": faker.sentence(nb_words=3, variable_nb_words=False),
                "start_date": faker.date_between(
                    start_date="-10y", end_date="-7y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-6y", end_date="-4y"
                ).strftime("%Y-%m-%d"),
                "loc_desc": faker.sentence(nb_words=3, variable_nb_words=False),
            }
        }
        AKAFactory(loader_context=loader_context)
        AKAFactory.create()
        response = self.client.get(
            "{0}?loc_desc={1}".format(
                self.endpoint, loader_context["item"]["loc_desc"]
            ),
            format="json",
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            AKA.objects.filter(
                loader_context__item__loc_desc__iexact=loader_context["item"][
                    "loc_desc"
                ]
            ).count(),
        )
        self.assertEqual(content["count"], 1)

    def test_filter_by_object_id(self):
        p = PersonFactory()
        AKAFactory(
            content_type_id=ContentType.objects.get(model="person").id, object_id=p.id
        )
        AKAFactory.create()
        response = self.client.get(
            "{0}?person_id={1}".format(self.endpoint, p.id), format="json"
        )
        content = json.loads(response.content)
        self.assertEqual(
            content["count"],
            AKA.objects.filter(
                content_type_id=ContentType.objects.get(model="person").id,
                object_id=p.id,
            ).count(),
        )
        self.assertEqual(content["count"], 1)
