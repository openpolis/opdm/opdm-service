# noinspection PyUnresolvedReferences
from rest_framework.serializers import ModelSerializer

from project.api_v1.serializers import BulkListSerializer


class NestedFieldsWriterMixin(ModelSerializer):
    """
    Mixin with `create` and `update` methods that strips nested fields and
    call dedicated ORM add_* and update_* methods of entities
    """

    def create(self, validated_data):
        # strip nested fields' data
        nested_data = {}
        for nested_field in self.nested_fields:
            nested_data[nested_field] = validated_data.pop(nested_field, [])

        # create flat object instance using ModelSerializer's create() method
        obj = super(NestedFieldsWriterMixin, self).create(validated_data)

        # loop over org.add_FIELD for all nested fields
        # i.e.: org.add_classifications(nested_data['classification']) ...
        for field, values in nested_data.items():
            class_method = "add_{0}".format(field)
            if hasattr(obj, class_method) and values:
                getattr(obj, class_method)(values)

        return obj

    def update(self, instance, validated_data):

        # strip nested fields' data
        nested_data = {}
        for nested_field in self.nested_fields:
            nested_data[nested_field] = validated_data.pop(nested_field, None)

        # create flat Organization using ModelSerializer's create() method
        obj = super(NestedFieldsWriterMixin, self).update(instance, validated_data)

        # loop over org.update_FIELD for all nested fields
        # i.e.: org.add_classifications(nested_data['classification']) ...
        for field, values in nested_data.items():
            class_method = "update_{0}".format(field)
            if hasattr(obj, class_method) and values is not None:
                getattr(obj, class_method)(values)

        return obj


class BulkSerializerMixin(ModelSerializer):
    """
    Mixin to be mixed to any ModelSerializer class which defines BulkListSerializer as Meta.list_serializer_class.

    Based off the BulkSerializerMixin from the djangorestframework-bulk package.

    Copyright (c) 2014-2015, Miroslav Shubernetskiy
    """

    def to_internal_value(self, data):
        """
        Add update_lookup_field field back to validated data.

        Add update_lookup_field field back to validated data
        since super by default strips out read-only fields
        hence id will no longer be present in validated_data.

        :param data:
        :return:
        """
        ret = super(BulkSerializerMixin, self).to_internal_value(data)

        id_attr = getattr(self.Meta, "update_lookup_field", "id")
        request_method = getattr(
            getattr(self.context.get("view"), "request"), "method", ""
        )

        if (
            isinstance(self.root, BulkListSerializer)
            and id_attr
            and request_method in ("PUT", "PATCH")
        ):
            id_field = self.fields[id_attr]
            id_value = id_field.get_value(data)
            ret[id_attr] = id_value

        return ret
