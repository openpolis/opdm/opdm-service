from popolo.models import (
    Area,
    AreaRelationship,
    Classification,
    ContactDetail,
    Identifier,
    Organization,
    Person,
    Post,
    ClassificationRel,
    OtherName,
    Membership,
    KeyEvent,
    Ownership,
)
from rest_framework import serializers
from rest_framework.fields import empty
from rest_framework.validators import UniqueTogetherValidator

from project.api_v1.serializers import GenericRelatableSerializer


class ClassificationInlineSerializer(serializers.ModelSerializer):
    code = serializers.CharField(required=False)
    descr = serializers.CharField(required=False)

    def run_validation(self, data=empty):
        """
        We override the default `run_validation`, and intercept the case in which `id` is passed.

        This use case means that the classification we want to associate already exists and the UniqueTogetherValidator,
        should not report an exception (duplicates are avoided by the get_or_create method used to add the
        key_events).
        """
        (is_empty_value, data) = self.validate_empty_values(data)
        if is_empty_value:
            return data

        if "classification" in data and data["classification"]:
            return data

        # validation passes if one of 'code' and 'descr' has value; 'scheme' must always be specified
        if data.get("scheme", None) and (
            data.get("code", None) or data.get("descr", None)
        ):
            return data

        return super().run_validation(data)

    class Meta:
        model = Classification
        ref_name = "Classification (inline)"
        fields = ("id", "scheme", "code", "descr")


class ClassificationRelInlineSerializer(serializers.ModelSerializer):
    classification = ClassificationInlineSerializer()

    class Meta(GenericRelatableSerializer.Meta):
        model = ClassificationRel
        ref_name = "ClassificationRel (inline)"
        exclude = ("content_type", "object_id")


class ContactDetailInlineSerializer(serializers.ModelSerializer):
    class Meta(GenericRelatableSerializer.Meta):
        model = ContactDetail
        ref_name = "ContactDetail (inline)"
        exclude = (
            "content_type",
            "object_id",
            "start_date",
            "end_date",
            "end_reason",
            "created_at",
            "updated_at",
        )


class IdentifierInlineSerializer(serializers.ModelSerializer):
    class Meta(GenericRelatableSerializer.Meta):
        model = Identifier
        ref_name = "Identifier (inline)"
        exclude = (
            "content_type",
            "object_id",
            "end_reason",
            "source",
        )


class OtherNameInlineSerializer(serializers.ModelSerializer):
    class Meta(GenericRelatableSerializer.Meta):
        model = OtherName
        ref_name = "OtherName (inline)"


class AreaInlineSerializer(serializers.HyperlinkedModelSerializer):
    identifier = serializers.CharField()

    class Meta:
        model = Area
        ref_name = "Area (inline)"
        fields = (
            "id",
            "url",
            "name",
            "identifier",
            "classification",
            "istat_classification",
            "inhabitants",
        )


class MembershipInlineSerializer(serializers.HyperlinkedModelSerializer):
    person_name = serializers.CharField(source="person.name")

    class Meta:
        model = Membership
        ref_name = "Membership (inline)"
        fields = (
            "id",
            "url",
            "person_name",
            "label",
            "role",
            "start_date",
            "end_date",
            "end_reason",
            "constituency_descr_tmp",
            "electoral_list_descr_tmp",
        )


class OrganizationInlineSerializer(serializers.HyperlinkedModelSerializer):
    forma_giuridica = serializers.CharField(source="classification")

    class Meta:
        model = Organization
        ref_name = "Organization (inline)"
        fields = ("id", "url", "name", "forma_giuridica")


class OrganizationByIdentifierInlineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Organization
        ref_name = "Organization by identifier (inline)"
        fields = ("identifier",)


class PersonOwnershipInlineSerializer(serializers.HyperlinkedModelSerializer):
    owned_organization = OrganizationInlineSerializer()

    class Meta:
        model = Ownership
        ref_name = "Ownership"
        fields = read_only_fields = (
            "id",
            "url",
            "owned_organization",
            "percentage",
            "start_date",
            "end_date",
        )


class PersonInlineSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Person
        ref_name = "Person (inline)"
        fields = ("id", "url", "name", "sort_name", "birth_date", "birth_location")


class PostInlineSerializer(serializers.HyperlinkedModelSerializer):
    organization = OrganizationInlineSerializer()
    area = AreaInlineSerializer()

    class Meta:
        model = Post
        ref_name = "Post (inline)"
        fields = ("id", "url", "label", "other_label", "role", "organization", "area")


class KeyEventInlineSerializer(serializers.HyperlinkedModelSerializer):
    def run_validation(self, data=empty):
        """
        We override the default `run_validation`, and intercept the case in which `id` are passed
        along with the `event_type` and `start_date` fields.

        This use case means that the event we want to associate already exists and the UniqueTogetherValidator,
        should not report an exception (duplicates are avoided by the get_or_create method used to add the
        key_events).
        """
        (is_empty_value, data) = self.validate_empty_values(data)
        if is_empty_value:
            return data

        if (
            "key_event" in data
            and data["key_event"]
            and isinstance(self.validators[0], UniqueTogetherValidator)
        ):
            return data

        return super().run_validation(data)

    class Meta:
        model = KeyEvent
        ref_name = "KeyEvent (inline)"
        fields = (
            "id",
            "url",
            "name",
            "event_type",
            "identifier",
            "start_date",
            "end_date",
        )


class FormerParentsSerializer(serializers.HyperlinkedModelSerializer):
    area = AreaInlineSerializer(source="dest_area")

    class Meta:
        model = AreaRelationship
        fields = ("area", "start_date", "end_date")


class AreaListResultSerializer(serializers.HyperlinkedModelSerializer):
    parent = AreaInlineSerializer()
    identifiers = IdentifierInlineSerializer(many=True)
    other_names = OtherNameInlineSerializer(many=True)
    former_parents = FormerParentsSerializer(
        source="get_former_parents", many=True, read_only=True
    )

    gps_lat = serializers.SerializerMethodField()
    gps_lon = serializers.SerializerMethodField()

    @staticmethod
    def get_gps_lat(obj):
        geometry = obj.geometry
        if geometry:
            pos = geometry.point_on_surface
            if pos:
                return pos.y

    @staticmethod
    def get_gps_lon(obj):
        geometry = obj.geometry
        if geometry:
            pos = geometry.point_on_surface
            if pos:
                return pos.x

    class Meta:
        model = Area
        ref_name = "Area (list result)"
        fields = (
            "id",
            "url",
            "name",
            "identifier",
            "identifiers",
            "gps_lat",
            "gps_lon",
            "other_names",
            "classification",
            "istat_classification",
            "start_date",
            "end_date",
            "inhabitants",
            "parent",
            "former_parents",
            "created_at",
            "updated_at",
        )


class OwnershipListResultSerializer(serializers.HyperlinkedModelSerializer):
    owner_organization = OrganizationInlineSerializer()
    owner_person = PersonInlineSerializer()
    owned_organization = OrganizationInlineSerializer()

    class Meta:
        model = Ownership
        ref_name = "Ownership (list result)"
        fields = (
            "id",
            "url",
            "owner_organization",
            "owned_organization",
            "owner_person",
            "percentage",
            "start_date",
            "end_date",
            "created_at",
            "updated_at",
        )


class MembershipListResultSerializer(serializers.HyperlinkedModelSerializer):
    person = PersonInlineSerializer()
    organization = OrganizationInlineSerializer()
    post = PostInlineSerializer()
    area = AreaInlineSerializer()

    class Meta:
        model = Membership
        ref_name = "Membership (list result)"
        fields = (
            "id",
            "url",
            "label",
            "role",
            "person",
            "organization",
            "post",
            "start_date",
            "end_date",
            "end_reason",
            "area",
            "constituency_descr_tmp",
            "electoral_list_descr_tmp",
            "created_at",
            "updated_at",
        )


class OrganizationListResultSerializer(serializers.HyperlinkedModelSerializer):
    parent_uri = serializers.HyperlinkedRelatedField(
        queryset=Organization.objects.all(),
        view_name="organization-detail",
        source="parent",
    )
    identifier = serializers.CharField()
    identifiers = IdentifierInlineSerializer(many=True)
    other_names = OtherNameInlineSerializer(many=True)
    area = AreaInlineSerializer()
    children = OrganizationInlineSerializer(many=True)
    classifications = ClassificationRelInlineSerializer(many=True)
    n_memberships = serializers.SerializerMethodField()
    n_current_memberships = serializers.SerializerMethodField()
    n_past_memberships = serializers.SerializerMethodField()
    employees = serializers.IntegerField(source="economics.employees", read_only=True)
    revenue = serializers.IntegerField(source="economics.revenue", read_only=True)
    capital_stock = serializers.IntegerField(
        source="economics.capital_stock", read_only=True
    )

    @staticmethod
    def get_n_memberships(obj):
        return obj.memberships.count()

    @staticmethod
    def get_n_current_memberships(obj):
        return len([m for m in obj.memberships.all() if m.end_date is None])

    @staticmethod
    def get_n_past_memberships(obj):
        return len([m for m in obj.memberships.all() if m.end_date is not None])

    class Meta:
        model = Organization
        ref_name = "Organization (list result)"
        fields = (
            "id",
            "url",
            "name",
            "parent_uri",
            "identifier",
            "identifiers",
            "other_names",
            "classification",
            "founding_date",
            "dissolution_date",
            "n_memberships",
            "n_current_memberships",
            "n_past_memberships",
            "employees",
            "revenue",
            "capital_stock",
            "area",
            "children",
            "classifications",
            "created_at",
            "updated_at",
        )


class OrganizationSearchResultSerializer(OrganizationListResultSerializer):
    score = serializers.FloatField()

    class Meta(OrganizationListResultSerializer.Meta):
        model = Organization
        ref_name = "Organization (search result)"
        fields = (
            "score",
            "id",
            "url",
            "name",
            "identifier",
            "classification",
            "founding_date",
            "dissolution_date",
        )


class PersonListResultSerializer(serializers.HyperlinkedModelSerializer):
    identifiers = IdentifierInlineSerializer(many=True)
    other_names = OtherNameInlineSerializer(many=True)
    contact_details = ContactDetailInlineSerializer(many=True)
    classifications = ClassificationRelInlineSerializer(many=True)
    birth_location_area = AreaInlineSerializer()

    class Meta:
        model = Person
        ref_name = "Person (list result)"
        fields = (
            "id",
            "url",
            "slug",
            "name",
            "family_name",
            "given_name",
            "sort_name",
            "gender",
            "birth_date",
            "death_date",
            "birth_location",
            "birth_location_area",
            "email",
            "identifiers",
            "classifications",
            "other_names",
            "contact_details",
            "created_at",
            "updated_at",
        )


class PersonSearchResultSerializer(PersonListResultSerializer):
    score = serializers.FloatField()
    identifiers = IdentifierInlineSerializer(many=True)
    other_names = OtherNameInlineSerializer(many=True)
    memberships = MembershipInlineSerializer(many=True)
    birth_location_area = AreaInlineSerializer()

    class Meta(PersonListResultSerializer.Meta):
        model = Person
        ref_name = "Person (search result)"
        fields = (
            "score",
            "id",
            "url",
            "family_name",
            "given_name",
            "gender",
            "birth_date",
            "death_date",
            "birth_location",
            "birth_location_area",
            "email",
            "identifiers",
            "other_names",
            "memberships",
            "created_at",
            "updated_at",
        )


class PostListResultSerializer(serializers.HyperlinkedModelSerializer):
    organization = OrganizationInlineSerializer()
    area = AreaInlineSerializer()

    class Meta:
        model = Post
        ref_name = "Post (list result)"
        fields = (
            "id",
            "url",
            "label",
            "other_label",
            "role",
            "organization",
            "area",
            "created_at",
            "updated_at",
        )
