class GenericRelatableSerializer:
    """
    Base Meta to exclude structural fields from generic relations.
    Used when serializing models inheriting the GenericRelatable behavior.
    """

    class Meta:
        abstract = True
        exclude = ()
