from popolo.models import (
    Area,
    ContactDetail,
    Link,
    Source,
    Identifier,
    Organization,
    Classification,
    ClassificationRel,
    Membership,
    Person,
    LinkRel,
    SourceRel,
    Post,
    AreaRelationship,
    Profession,
    EducationLevel,
    OtherName,
    OriginalProfession,
    OriginalEducationLevel,
    RoleType,
    KeyEvent,
    KeyEventRel,
    Ownership,
    OrganizationRelationship)
from rest_framework import serializers
from rest_framework.reverse import reverse

from project.akas.models import AKA
from project.api_v1.serializers import (
    GenericRelatableSerializer,
    NestedFieldsWriterMixin,
    MembershipInlineSerializer,
    PostInlineSerializer,
    ClassificationInlineSerializer,
    OrganizationInlineSerializer,
    AreaInlineSerializer,
    PersonInlineSerializer,
    ClassificationRelInlineSerializer,
    ContactDetailInlineSerializer,
)
from project.api_v1.serializers.inlines import (
    KeyEventInlineSerializer,
    OrganizationByIdentifierInlineSerializer,
    PersonOwnershipInlineSerializer,
)
from project.api_v1.serializers.lists import BulkListSerializer
from project.api_v1.serializers.mixins import BulkSerializerMixin
from project.api_v1.validators import Interval


class ContactDetailSerializer(serializers.ModelSerializer):
    class Meta(GenericRelatableSerializer.Meta):
        model = ContactDetail
        ref_name = "ContactDetail"
        exclude = ("id", "content_type", "object_id")


class IdentifierSerializer(serializers.ModelSerializer):
    class Meta(GenericRelatableSerializer.Meta):
        model = Identifier
        ref_name = "Identifier"
        fields = (
            "identifier",
            "scheme",
            "start_date",
            "end_date",
            "end_reason",
            "source",
        )


class OtherNameSerializer(serializers.ModelSerializer):
    class Meta(GenericRelatableSerializer.Meta):
        model = OtherName
        ref_name = "OtherName"
        exclude = ("id", "content_type", "object_id")


class LinkSerializer(serializers.ModelSerializer):
    class Meta(GenericRelatableSerializer.Meta):
        model = Link
        ref_name = "Link"
        fields = ("url", "note")


class LinkRelSerializer(serializers.ModelSerializer):
    link_id = serializers.IntegerField(source="link.id", required=False)
    url = serializers.CharField(source="link.url")
    note = serializers.CharField(source="link.note")

    class Meta(GenericRelatableSerializer.Meta):
        model = LinkRel
        ref_name = "LinkRel"
        exclude = ("id", "link", "content_type", "object_id")


class SourceSerializer(serializers.ModelSerializer):
    class Meta(GenericRelatableSerializer.Meta):
        model = Source
        ref_name = "Source"
        fields = ("url", "note")


class SourceRelSerializer(serializers.ModelSerializer):
    source_id = serializers.IntegerField(source="source.id", required=False)
    url = serializers.CharField(source="source.url")
    note = serializers.CharField(source="source.note")

    class Meta(GenericRelatableSerializer.Meta):
        model = SourceRel
        ref_name = "SourceRel"
        exclude = ("id", "source", "content_type", "object_id")


class ClassificationSerializer(serializers.HyperlinkedModelSerializer):
    parent = ClassificationInlineSerializer()

    class Meta:
        model = Classification
        ref_name = "Classification"
        fields = ("id", "scheme", "code", "descr", "parent")
        read_only_fields = ("id",)


class ClassificationWriteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Classification
        ref_name = "Classification (write)"
        fields = ("scheme", "code", "descr")


class ClassificationRelSerializer(serializers.ModelSerializer):
    classification = ClassificationInlineSerializer()

    class Meta(GenericRelatableSerializer.Meta):
        model = ClassificationRel
        ref_name = "ClassificationRel"
        fields = ("id", "classification", "start_date", "end_date", "end_reason")


class ClassificationRelWriteSerializer(serializers.ModelSerializer):
    classification = serializers.PrimaryKeyRelatedField(
        queryset=Classification.objects.all(), required=True
    )

    class Meta(GenericRelatableSerializer.Meta):
        model = ClassificationRel
        ref_name = "ClassificationRel (write)"
        fields = ("classification", "start_date", "end_date", "end_reason")


class KeyEventSerializer(serializers.HyperlinkedModelSerializer):
    """Serializer used to read/write KeyEvents object (/keyevents endpoint)
    """

    class Meta:
        model = KeyEvent
        ref_name = "KeyEvent"
        fields = (
            "id",
            "url",
            "name",
            "event_type",
            "identifier",
            "start_date",
            "end_date",
            "end_reason",
            "created_at",
            "updated_at",
        )
        read_only_fields = ("id", "url", "created_at", "updates_at")


class KeyEventRelSerializer(serializers.ModelSerializer):
    """Read serializer for the key_events element"""

    key_event = KeyEventInlineSerializer()

    class Meta(GenericRelatableSerializer.Meta):
        model = KeyEventRel
        ref_name = "KeyEventRel"
        fields = ("id", "key_event")


class KeyEventRelWriteSerializer(serializers.ModelSerializer):
    """Write serializer for the key_events element (accepts {'key_event': 0})"""

    key_event = serializers.PrimaryKeyRelatedField(
        queryset=KeyEvent.objects.all(), required=True
    )

    class Meta(GenericRelatableSerializer.Meta):
        model = KeyEventRel
        ref_name = "KeyEventRel (write)"
        fields = ("key_event",)


class FormerParentsSerializer(serializers.HyperlinkedModelSerializer):
    area = AreaInlineSerializer(source="dest_area")

    class Meta:
        model = AreaRelationship
        fields = ("area", "start_date", "end_date")


class FormerChildrenSerializer(serializers.HyperlinkedModelSerializer):
    area = AreaInlineSerializer(source="source_area")
    identifiers = IdentifierSerializer(many=True, source="source_area.identifiers")

    class Meta:
        model = AreaRelationship
        fields = ("area", "start_date", "end_date", "identifiers")


class OrganizationSerializer(serializers.HyperlinkedModelSerializer):
    area = AreaInlineSerializer()
    parent = OrganizationInlineSerializer()
    children = OrganizationInlineSerializer(many=True)
    new_orgs = OrganizationInlineSerializer(many=True)
    key_events = KeyEventRelSerializer(many=True)
    contact_details = ContactDetailSerializer(many=True)
    classifications = ClassificationRelInlineSerializer(many=True)
    identifiers = IdentifierSerializer(many=True)
    other_names = OtherNameSerializer(many=True)
    links = LinkRelSerializer(many=True)
    sources = SourceRelSerializer(many=True)
    n_memberships = serializers.SerializerMethodField()
    n_current_memberships = serializers.SerializerMethodField()
    n_past_memberships = serializers.SerializerMethodField()
    employees = serializers.IntegerField(source="economics.employees", read_only=True)
    revenue = serializers.IntegerField(source="economics.revenue", read_only=True)
    capital_stock = serializers.IntegerField(
        source="economics.capital_stock", read_only=True
    )

    @staticmethod
    def get_n_memberships(obj):
        return obj.memberships.count()

    @staticmethod
    def get_n_current_memberships(obj):
        return len([m for m in obj.memberships.all() if m.end_date is None])

    @staticmethod
    def get_n_past_memberships(obj):
        return len([m for m in obj.memberships.all() if m.end_date is not None])

    class Meta:
        model = Organization
        ref_name = "Organization"
        fields = read_only_fields = (
            "id",
            "url",
            "slug",
            "name",
            "identifier",
            "classification",
            "image",
            "area",
            "parent",
            "children",
            "new_orgs",
            "founding_date",
            "dissolution_date",
            "end_reason",
            "n_memberships",
            "n_current_memberships",
            "n_past_memberships",
            "employees",
            "revenue",
            "capital_stock",
            "key_events",
            "contact_details",
            "identifiers",
            "classifications",
            "other_names",
            "links",
            "sources",
            "created_at",
            "updated_at",
        )


class OrganizationWriteSerializer(
    NestedFieldsWriterMixin, serializers.HyperlinkedModelSerializer
):
    parent = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(), required=False, allow_null=True
    )
    area = serializers.PrimaryKeyRelatedField(
        queryset=Area.objects.all(), required=False, allow_null=True
    )
    classifications = ClassificationInlineSerializer(
        many=True, required=False, allow_null=True
    )
    new_orgs = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Organization.objects.all(), required=False, allow_empty=True
    )
    key_events = KeyEventInlineSerializer(many=True, required=False, allow_null=True)
    contact_details = ContactDetailSerializer(
        many=True, required=False, allow_empty=True
    )
    identifiers = IdentifierSerializer(many=True, required=False, allow_empty=True)
    other_names = OtherNameSerializer(many=True, required=False, allow_empty=True)
    links = LinkRelSerializer(many=True, required=False, allow_empty=True)
    sources = SourceRelSerializer(many=True, required=False, allow_empty=True)
    nested_fields = [
        "classifications",
        "links",
        "sources",
        "other_names",
        "identifiers",
        "contact_details",
        "key_events",
    ]

    class Meta:
        model = Organization
        ref_name = "Organization (write)"
        fields = (
            "name",
            "identifier",
            "classification",
            "image",
            "area",
            "parent",
            "new_orgs",
            "founding_date",
            "dissolution_date",
            "end_reason",
            "key_events",
            "contact_details",
            "identifiers",
            "classifications",
            "other_names",
            "links",
            "sources",
        )


class OrganizationRelationshipSerializer(BulkSerializerMixin, serializers.HyperlinkedModelSerializer):
    source_organization = OrganizationInlineSerializer()
    dest_organization = OrganizationInlineSerializer()
    classification = ClassificationInlineSerializer()
    sources = SourceRelSerializer(many=True)

    class Meta:
        model = OrganizationRelationship
        ref_name = "OrganizationRelationship"
        fields = (
            "id",
            "url",
            "source_organization",
            "dest_organization",
            "weight",
            "descr",
            "classification",
            "start_date",
            "end_date",
            "end_reason",
            "sources",
        )


class OrganizationRelationshipWriteSerializer(
    NestedFieldsWriterMixin, BulkSerializerMixin, serializers.HyperlinkedModelSerializer
):
    source_organization = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(), required=False, allow_null=True
    )
    dest_organization = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(), required=False, allow_null=True
    )
    classification = serializers.PrimaryKeyRelatedField(
        queryset=Classification.objects.filter(scheme='OP_TIPO_RELAZIONE_ORG'),
        required=False, allow_null=True
    )
    sources = SourceRelSerializer(many=True, required=False, allow_empty=True)
    nested_fields = ["sources", ]

    class Meta:
        model = OrganizationRelationship
        ref_name = "Organization Relationship (write)"
        fields = (
            "id",
            "url",
            "source_organization",
            "dest_organization",
            "weight",
            "descr",
            "classification",
            "start_date",
            "end_date",
            "end_reason",
            "sources",
        )
        validators = [Interval()]
        list_serializer_class = BulkListSerializer


class AreaSerializer(serializers.HyperlinkedModelSerializer):
    slug = serializers.CharField(read_only=True)
    parent = AreaInlineSerializer()
    former_parents = FormerParentsSerializer(
        source="get_former_parents", many=True, read_only=True
    )
    new_places = AreaInlineSerializer(many=True, read_only=True)
    old_places = AreaInlineSerializer(many=True, read_only=True)
    identifiers = IdentifierSerializer(many=True, read_only=True)
    other_names = OtherNameSerializer(many=True, read_only=True)
    links = LinkRelSerializer(many=True, read_only=True)
    sources = SourceRelSerializer(many=True, read_only=True)
    related_areas = AreaInlineSerializer(many=True, read_only=True)

    class Meta:
        model = Area
        ref_name = "Area"
        fields = (
            "id",
            "url",
            "slug",
            "name",
            "identifier",
            "classification",
            "istat_classification",
            "is_provincial_capital",
            "inhabitants",
            # "geometry",
            "gps_lat",
            "gps_lon",
            "parent",
            "former_parents",
            "related_areas",
            "new_places",
            "old_places",
            "start_date",
            "end_date",
            "end_reason",
            "identifiers",
            "other_names",
            "links",
            "sources",
            "created_at",
            "updated_at",
        )


class AreaWriteSerializer(NestedFieldsWriterMixin, serializers.ModelSerializer):
    parent = serializers.PrimaryKeyRelatedField(
        queryset=Area.objects.all(), required=False, allow_null=True
    )
    new_places = serializers.PrimaryKeyRelatedField(
        queryset=Area.objects.all(), many=True, required=False, allow_empty=True
    )
    # TODO: Implement serialization `related_areas` field.
    # It's a many-to-many relationship with through model (AreaRelationship).
    related_areas = serializers.ListField(
        child=serializers.IntegerField(), read_only=True
    )
    identifiers = IdentifierSerializer(many=True, required=False, allow_null=True)
    other_names = OtherNameSerializer(many=True, required=False, allow_null=True)
    links = LinkRelSerializer(many=True, required=False, allow_null=True)
    sources = SourceRelSerializer(many=True, required=False, allow_null=True)

    nested_fields = ["identifiers", "other_names", "links", "sources", "related_areas"]

    class Meta:
        model = Area
        ref_name = "Area (write)"
        fields = (
            "name",
            "identifier",
            "classification",
            "istat_classification",
            "is_provincial_capital",
            "inhabitants",
            "parent",
            "related_areas",
            "new_places",
            "start_date",
            "end_date",
            "end_reason",
            "identifiers",
            "other_names",
            "links",
            "sources",
            "created_at",
            "updated_at",
        )
        validators = [Interval()]


class ProfessionSerializer(
    NestedFieldsWriterMixin, serializers.HyperlinkedModelSerializer
):
    identifiers = IdentifierSerializer(many=True, required=False)
    nested_fields = ["identifiers"]

    class Meta:
        model = Profession
        ref_name = "Profession"
        fields = ("id", "url", "name", "identifiers")
        read_only_fields = ("id", "url")


class EducationLevelSerializer(
    NestedFieldsWriterMixin, serializers.HyperlinkedModelSerializer
):
    identifiers = IdentifierSerializer(many=True, required=False)
    nested_fields = ["identifiers"]

    class Meta:
        model = EducationLevel
        ref_name = "EducationLevel"
        fields = ("id", "url", "name", "identifiers")
        read_only_fields = ("id", "url")


class OriginalProfessionSerializer(serializers.HyperlinkedModelSerializer):
    normalized_profession = ProfessionSerializer()

    class Meta:
        model = OriginalProfession
        ref_name = "OriginalProfession"
        fields = read_only_fields = ("id", "url", "name", "normalized_profession")


class OriginalProfessionWriteSerializer(serializers.ModelSerializer):
    normalized_profession = serializers.PrimaryKeyRelatedField(
        queryset=Profession.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the normalized Profession. ",
    )

    class Meta:
        model = OriginalProfession
        ref_name = "OriginalProfession (write)"
        fields = ("name", "normalized_profession")


class OriginalEducationLevelSerializer(serializers.HyperlinkedModelSerializer):
    normalized_education_level = EducationLevelSerializer()

    class Meta:
        model = OriginalEducationLevel
        ref_name = "OriginalEducationLevel"
        fields = read_only_fields = ("id", "url", "name", "normalized_education_level")


class OriginalEducationLevelWriteSerializer(serializers.ModelSerializer):
    normalized_education_level = serializers.PrimaryKeyRelatedField(
        queryset=EducationLevel.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the normalized Education level. ",
    )

    class Meta:
        model = OriginalEducationLevel
        ref_name = "OriginalEducationLevel (write)"
        fields = ("name", "normalized_education_level")


class PersonSerializer(serializers.HyperlinkedModelSerializer):
    """
    Serializer class used for a detailed view on a Person instance/resource.
    This is a read only serializer, it does not meant to handle deserialization.
    """

    identifiers = IdentifierSerializer(many=True)
    contact_details = ContactDetailSerializer(many=True)
    other_names = OtherNameSerializer(many=True)
    links = LinkRelSerializer(many=True)
    sources = SourceRelSerializer(many=True)
    memberships = MembershipInlineSerializer(many=True)
    ownerships = PersonOwnershipInlineSerializer(many=True)
    classifications = ClassificationRelInlineSerializer(many=True)
    related_persons = PersonInlineSerializer(many=True)
    birth_location_area = AreaInlineSerializer(read_only=True)
    original_profession = OriginalProfessionSerializer(read_only=True, allow_null=True)
    profession = ProfessionSerializer(read_only=True, allow_null=True)
    original_education_level = OriginalEducationLevelSerializer(
        read_only=True, allow_null=True
    )
    education_level = EducationLevelSerializer(read_only=True, allow_null=True)

    class Meta:
        model = Person
        ref_name = "Person"
        fields = read_only_fields = (
            "id",
            "url",
            "slug",
            "name",
            "family_name",
            "given_name",
            "additional_name",
            "honorific_prefix",
            "honorific_suffix",
            "patronymic_name",
            "sort_name",
            "gender",
            "birth_date",
            "death_date",
            "national_identity",
            "birth_location",
            "birth_location_area",
            "profession",
            "original_profession",
            "education_level",
            "original_education_level",
            "summary",
            "biography",
            "image",
            "email",
            "contact_details",
            "identifiers",
            "other_names",
            "links",
            "sources",
            "memberships",
            "ownerships",
            "classifications",
            "related_persons",
            "created_at",
            "updated_at",
        )


class PersonWriteSerializer(
    NestedFieldsWriterMixin, serializers.HyperlinkedModelSerializer
):
    birth_location_area = serializers.PrimaryKeyRelatedField(
        queryset=Area.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the Person's birth location Area. "
        "Must be an id of an existing Area.",
    )
    original_profession = serializers.PrimaryKeyRelatedField(
        queryset=OriginalProfession.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the Person's known non-normalized Profession. ",
    )
    education_level = serializers.PrimaryKeyRelatedField(
        queryset=EducationLevel.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the Person's known non-normalized EducationLevel. ",
    )
    original_education_level = serializers.PrimaryKeyRelatedField(
        queryset=OriginalEducationLevel.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the Person's known non-normalized EducationLevel. ",
    )
    # TODO: Implement serialization `related_persons` field.
    # It's a many-to-many relationship with through model (PersonalRelationship)
    related_persons = serializers.ListField(
        child=serializers.IntegerField(), read_only=True
    )
    identifiers = IdentifierSerializer(many=True, required=False, allow_empty=True)
    contact_details = ContactDetailSerializer(
        many=True, required=False, allow_empty=True
    )
    classifications = ClassificationInlineSerializer(
        many=True, required=False, allow_null=True
    )
    other_names = OtherNameSerializer(many=True, required=False, allow_empty=True)
    links = LinkRelSerializer(many=True, required=False, allow_empty=True)
    sources = SourceRelSerializer(many=True, required=False, allow_empty=True)

    nested_fields = [
        "links",
        "sources",
        "other_names",
        "identifiers",
        "contact_details",
        "classifications",
    ]

    class Meta:
        model = Person
        ref_name = "Person (write)"
        fields = (
            "name",
            "family_name",
            "given_name",
            "additional_name",
            "honorific_prefix",
            "honorific_suffix",
            "patronymic_name",
            "sort_name",
            "gender",
            "birth_date",
            "death_date",
            "national_identity",
            "birth_location",
            "birth_location_area",
            "profession",
            "original_profession",
            "education_level",
            "original_education_level",
            "summary",
            "biography",
            "image",
            "email",
            "contact_details",
            "identifiers",
            "classifications",
            "other_names",
            "links",
            "sources",
            "related_persons",
        )


class OwnershipSerializer(serializers.HyperlinkedModelSerializer):
    owned_organization = OrganizationInlineSerializer()
    owner_organization = OrganizationInlineSerializer()
    owner_person = PersonInlineSerializer()
    sources = SourceRelSerializer(many=True)

    class Meta:
        model = Ownership
        ref_name = "Ownership"
        fields = read_only_fields = (
            "id",
            "url",
            "owned_organization",
            "owner_organization",
            "owner_person",
            "start_date",
            "end_date",
            "sources",
        )


class OwnershipOrgByIdentifierSerializer(serializers.HyperlinkedModelSerializer):
    owned_organization = OrganizationByIdentifierInlineSerializer()
    owner_organization = OrganizationByIdentifierInlineSerializer()
    owner_person = PersonInlineSerializer()
    sources = SourceRelSerializer(many=True)

    class Meta:
        model = Ownership
        ref_name = "Ownership"
        fields = read_only_fields = (
            "id",
            "owned_organization",
            "owner_organization",
            "owner_person",
            "percentage",
            "start_date",
            "end_date",
            "sources",
        )


class OwnershipWriteSerializer(
    NestedFieldsWriterMixin, serializers.HyperlinkedModelSerializer
):
    owned_organization = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(), required=False, allow_null=True
    )
    owner_organization = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(), required=False, allow_null=True
    )
    owner_person = serializers.PrimaryKeyRelatedField(
        queryset=Person.objects.all(), required=False, allow_null=True
    )
    sources = SourceRelSerializer(many=True, required=False, allow_empty=True)
    nested_fields = ["sources"]

    class Meta:
        model = Ownership
        ref_name = "Ownership (write)"
        fields = (
            "owned_organization",
            "owner_organization",
            "owner_person",
            "start_date",
            "end_date",
            "sources",
        )


class MembershipSerializer(BulkSerializerMixin, serializers.HyperlinkedModelSerializer):
    person = PersonInlineSerializer()
    member_organization = OrganizationInlineSerializer()
    organization = OrganizationInlineSerializer()
    electoral_event = KeyEventInlineSerializer(many=False)
    post = PostInlineSerializer()
    on_behalf_of = OrganizationInlineSerializer()
    appointed_by = MembershipInlineSerializer()
    appointees = MembershipInlineSerializer(many=True)
    area = AreaInlineSerializer()
    classifications = ClassificationRelInlineSerializer(many=True)
    contact_details = ContactDetailInlineSerializer(many=True)
    links = LinkRelSerializer(many=True)
    sources = SourceRelSerializer(many=True)

    class Meta:
        model = Membership
        ref_name = "Membership"
        fields = (
            "id",
            "url",
            "label",
            "role",
            "person",
            "member_organization",
            "on_behalf_of",
            "organization",
            "electoral_event",
            "post",
            "start_date",
            "end_date",
            "end_reason",
            "area",
            "classifications",
            "appointed_by",
            "is_appointment_locked",
            "appointment_note",
            "appointees",
            "constituency_descr_tmp",
            "electoral_list_descr_tmp",
            "contact_details",
            "links",
            "sources",
        )


class MembershipWriteSerializer(
    NestedFieldsWriterMixin, BulkSerializerMixin, serializers.HyperlinkedModelSerializer
):
    person = serializers.PrimaryKeyRelatedField(
        queryset=Person.objects.all(), required=False, allow_null=True
    )
    member_organization = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(), required=False, allow_null=True
    )
    organization = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(), required=False, allow_null=True
    )
    post = serializers.PrimaryKeyRelatedField(
        queryset=Post.objects.all(), required=False, allow_null=True
    )
    electoral_event = serializers.PrimaryKeyRelatedField(
        queryset=KeyEvent.objects.filter(event_type__icontains="ele"),
        required=False,
        allow_null=True,
    )

    on_behalf_of = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(), required=False, allow_null=True
    )
    appointed_by = serializers.PrimaryKeyRelatedField(
        queryset=Membership.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the Membership that has appointed this membership. "
        "ex: Secr. of Defence Mac Namara is appointed by POTUS JFK. ",
    )
    appointees = MembershipInlineSerializer(many=True, required=False, allow_empty=True)

    area = serializers.PrimaryKeyRelatedField(
        queryset=Area.objects.all(), required=False, allow_null=True
    )
    classifications = ClassificationInlineSerializer(
        many=True, required=False, allow_null=True
    )
    contact_details = ContactDetailInlineSerializer(
        many=True, required=False, allow_empty=True
    )
    links = LinkRelSerializer(many=True, required=False, allow_empty=True)
    sources = SourceRelSerializer(many=True, required=False, allow_empty=True)
    nested_fields = ["classifications", "links", "sources", "contact_details", "appointees"]

    class Meta:
        model = Membership
        ref_name = "Membership (write)"
        fields = (
            "id",
            "label",
            "role",
            "person",
            "member_organization",
            "on_behalf_of",
            "appointed_by",
            "is_appointment_locked",
            "appointment_note",
            "appointees",
            "organization",
            "post",
            "electoral_event",
            "start_date",
            "end_date",
            "end_reason",
            "area",
            "classifications",
            "constituency_descr_tmp",
            "electoral_list_descr_tmp",
            "contact_details",
            "links",
            "sources",
        )
        validators = [Interval()]
        list_serializer_class = BulkListSerializer


class RoleTypeSerializer(serializers.HyperlinkedModelSerializer):
    classification = ClassificationInlineSerializer()

    class Meta:
        model = RoleType
        ref_name = "RoleType"
        fields = ("id", "label", "other_label", "classification", "priority", "is_appointer", "is_appointable")
        read_only_fields = ("id",)


class RoleTypeWriteSerializer(serializers.HyperlinkedModelSerializer):
    classification = serializers.PrimaryKeyRelatedField(
        queryset=Classification.objects.filter(scheme="FORMA_GIURIDICA_OP"),
        required=True,
    )

    class Meta:
        model = RoleType
        ref_name = "RoleType (write)"
        fields = ("label", "other_label", "classification", "priority")


class PostSerializer(serializers.HyperlinkedModelSerializer):
    organization = OrganizationInlineSerializer()
    area = AreaInlineSerializer()
    role_type = RoleTypeSerializer()
    appointed_by = PostInlineSerializer()
    holders = PersonInlineSerializer(many=True)

    class Meta:
        model = Post
        ref_name = "Post"
        fields = read_only_fields = (
            "id",
            "url",
            "label",
            "other_label",
            "slug",
            "role",
            "role_type",
            "organization",
            "area",
            "appointed_by",
            "is_appointment_locked",
            "appointment_note",
            "holders",
            "start_date",
            "end_date",
            "end_reason",
            "created_at",
            "updated_at",
        )


class PostWriteSerializer(serializers.HyperlinkedModelSerializer):
    organization = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the Organization in which the Post is held. "
        "Must be an id of an existing Organization. ",
    )
    area = serializers.PrimaryKeyRelatedField(
        queryset=Area.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the geographic Area to which the Post is related. "
        "Must be an id of an existing Area. ",
    )
    appointed_by = serializers.PrimaryKeyRelatedField(
        queryset=Post.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the Post that officially appoints members to this Post. "
        "ex: Secr. of Defence is appointed by POTUS. ",
    )
    role_type = serializers.PrimaryKeyRelatedField(
        queryset=RoleType.objects.all(),
        required=False,
        allow_null=True,
        help_text="The id of the role_type.",
    )

    # TODO: Implement serialization `holders` field.
    # It's a many-to-many relationship with through model (Membership).
    holders = serializers.ListField(child=serializers.IntegerField(), read_only=True)

    class Meta:
        model = Post
        ref_name = "Post (write)"
        fields = (
            "label",
            "other_label",
            "slug",
            "role",
            "role_type",
            "organization",
            "area",
            "appointed_by",
            "is_appointment_locked",
            "appointment_note",
            "holders",
            "start_date",
            "end_date",
            "end_reason",
        )


class AKASerializer(serializers.HyperlinkedModelSerializer):
    search_params = serializers.JSONField()
    search_url = serializers.SerializerMethodField()

    loader_context = serializers.JSONField()
    n_similarities = serializers.IntegerField()
    is_resolved = serializers.BooleanField()

    def get_search_url(self, obj):
        params = "&".join("{}={}".format(k, v) for k, v in obj.search_params.items())
        return self.context["request"].build_absolute_uri(
            f"{reverse('mappepotere_v1:person-search')}?{params}"
        )

    class Meta(GenericRelatableSerializer.Meta):
        model = AKA
        ref_name = "AKA"
        fields = (
            "id",
            "url",
            "search_url",
            "search_params",
            "n_similarities",
            "loader_context",
            "is_resolved",
            "content_type_id",
            "object_id",
            "created_at",
            "updated_at",
        )
