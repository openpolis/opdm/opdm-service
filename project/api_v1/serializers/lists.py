from rest_framework.exceptions import ValidationError
from rest_framework.serializers import ListSerializer


class BulkListSerializer(ListSerializer):
    """
    Custom ListSerializer that handles the update of multiple objects.

    Based off the BulkListSerializer from the `djangorestframework-bulk` package.

    Copyright (c) 2014-2015, Miroslav Shubernetskiy
    """

    def update(self, queryset, validated_data):
        id_attr = getattr(self.child.Meta, "update_lookup_field", "id")

        validated_data_by_id = {i.pop(id_attr): i for i in validated_data}

        # since this method is given a queryset which can have many
        # model instances, first find all objects to update
        # and only then update the models
        objects_to_update = queryset.filter(
            **{f"{id_attr}__in": validated_data_by_id.keys()}
        )

        if set(validated_data_by_id.keys()) != set(
            objects_to_update.values_list(id_attr, flat=True)
        ):
            raise ValidationError("Could not find all objects to update.")

        updated_objects = []

        for obj in objects_to_update:
            obj_id = getattr(obj, id_attr)
            obj_validated_data = validated_data_by_id.get(obj_id)

            # use model serializer to actually update the model
            # in case that method is overwritten
            updated_objects.append(self.child.update(obj, obj_validated_data))

        return updated_objects
