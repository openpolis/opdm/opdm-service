import re
from datetime import datetime

from haystack import indexes
from popolo.models import Person, Organization


class PersonIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True, stored=False)
    family_name = indexes.CharField(stored=True, model_attr="family_name")
    given_name = indexes.CharField(stored=True, model_attr="given_name")
    birth_date = indexes.FacetCharField(stored=True, null=True, model_attr="birth_date")
    birth_date_s = indexes.CharField(stored=True, null=True, model_attr="birth_date")
    birth_location = indexes.CharField(
        stored=True, null=True, model_attr="birth_location"
    )
    birth_location_area = indexes.IntegerField(
        stored=True, null=True, model_attr="birth_location_area__id"
    )

    def get_model(self):
        return Person

    def index_queryset(self, using=None):
        return self.get_model().objects.only(
            "family_name", "given_name", "birth_date", "birth_location"
        )

    @staticmethod
    def prepare_family_name(obj):
        return obj.family_name.lower()

    @staticmethod
    def prepare_given_name(obj):
        return obj.given_name.lower()

    @staticmethod
    def prepare_birth_location(obj):
        """Strip province from birth location to try decreasing similarities number

        :param obj:
        :return:
        """
        if obj.birth_location:
            return re.sub(r"(.*) \(.*\)", r"\1", obj.birth_location.lower())
        else:
            return None

    @staticmethod
    def prepare_birth_location_area(obj):
        if obj.birth_location_area:
            return obj.birth_location_area.id
        else:
            return None

    @staticmethod
    def prepare_birth_date(obj):
        if obj.birth_date:
            try:
                return datetime.strptime(obj.birth_date, "%Y")
            except ValueError:
                try:
                    return datetime.strptime(obj.birth_date, "%Y-%m")
                except ValueError:
                    try:
                        return datetime.strptime(obj.birth_date, "%Y-%m-%d")
                    except ValueError:
                        return None

    @staticmethod
    def prepare_birth_date_s(obj):
        if obj.birth_date:
            return obj.birth_date.replace("-", "")

    def prepare(self, obj):
        self.prepared_data = super().prepare(obj)
        for i in obj.identifiers.values("scheme", "identifier"):
            self.prepared_data["%s_s" % i["scheme"]] = str(i["identifier"])
        return self.prepared_data


class OrganizationIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True, stored=False)

    name = indexes.CharField(stored=True, model_attr="name")
    identifier = indexes.FacetCharField(stored=True, null=True, model_attr="identifier")
    classification = indexes.FacetCharField(
        stored=True, null=True, model_attr="classification"
    )

    def get_model(self):
        return Organization

    def index_queryset(self, using=None):
        return self.get_model().objects.only("name", "identifier", "classification",)
