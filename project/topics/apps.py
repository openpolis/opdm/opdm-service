from django.apps import AppConfig
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class TopicsConfig(AppConfig):
    """
    Default "topics" app configuration.

    Only connect signals when specified in settings (can be disabled in tests).
    """

    name = 'project.topics'
    verbose_name = _("Topics")

    def ready(self):
        from . import admin
        from . import signals

        admin.register()  # Register models in admin site
        if settings.TOPICS_CONNECT_SIGNALS:
            signals.connect()  # Connect the signals
