from django.db.models.signals import post_save, pre_delete

from popolo.models import ClassificationRel, Organization


def _classification_to_topic(org):
    mapping = org.classifications.get(classification__scheme='ATOKA_ATECO').\
        classification.topic_mapping.first()
    if mapping:
        return mapping.topic_classification
    else:
        return None


def add_topic_to_org(sender, instance: ClassificationRel, **kwargs):
    """
    Assign an OPDM_ORGANIZATION_LABEL classification to an Organization that
    just received a FORMA_GIURIDICA_OP classification.

    :param sender: The model class
    :param instance: The actual instance being saved.
    :param kwargs: Other args. See: https://docs.djangoproject.com/en/dev/ref/signals/#post-save
    """
    if instance.classification.scheme == 'ATOKA_ATECO':
        try:
            org = Organization.objects.\
                exclude(classification__in=['Regione', 'Provincia', 'Comune']).\
                get(id=instance.object_id)
        except Organization.DoesNotExist:
            pass
        else:
            topic_classification = _classification_to_topic(org)
            if topic_classification:
                org.add_classification_rel(topic_classification)


def remove_topic_from_org(sender, instance: ClassificationRel, **kwargs):
    """
    Remove an OPDM_TOPIC_TAG classification from an Organization after
    removal of a ATOKA_ATECO classification.

    :param sender: The model class
    :param instance: The actual instance being saved.
    :param kwargs: Other args. See: https://docs.djangoproject.com/en/dev/ref/signals/#post-save
    """
    if instance.classification.scheme == 'ATOKA_ATECO':
        try:
            org = Organization.objects.get(id=instance.object_id)
        except Organization.DoesNotExist:
            pass
        else:
            topic_classification = _classification_to_topic(org)
            if topic_classification:
                org.classifications.filter(classification=topic_classification).delete()


def connect():
    """
    Connect all the signals.
    """
    post_save.connect(receiver=add_topic_to_org, sender=ClassificationRel)
    pre_delete.connect(receiver=remove_topic_from_org, sender=ClassificationRel)


def disconnect():
    """
    Connect all the signals.
    """
    post_save.disconnect(receiver=add_topic_to_org, sender=ClassificationRel)
    pre_delete.disconnect(receiver=remove_topic_from_org, sender=ClassificationRel)
