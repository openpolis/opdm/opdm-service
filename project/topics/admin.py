from django.contrib import admin

from project.topics.models import OrgMapping


class OrgMappingAdmin(admin.ModelAdmin):
    model = OrgMapping
    list_display = ('original_classification', 'original_code', 'topic')
    raw_id_fields = ('classification', )
    list_filter = ('topic_classification', )
    search_fields = ('classification__descr', 'classification__code')

    def original_classification(self, obj):
        return f"{obj.classification.descr}"
    original_classification.short_description = 'Original ATECO descr'

    def original_code(self, obj):
        return f"{obj.classification.code}"
    original_code.short_description = 'Original ATECO code'

    def topic(self, obj):
        return f"{obj.topic_classification.descr}"
    topic.short_description = 'Topic'


def register():
    admin.site.register(OrgMapping, OrgMappingAdmin)
