from popolo.behaviors.models import Timestampable
from popolo.models import Classification

from django.utils.translation import ugettext_lazy as _
from django.db import models


class OrgMapping(Timestampable, models.Model):
    """Maps an ATOKA_ATEKO Classification instance to a OPDM_TOPIC_TAG one
    """
    classification = models.ForeignKey(
        to=Classification,
        related_name="topic_mapping",
        limit_choices_to={"scheme": "ATOKA_ATECO"},
        help_text=_("The original ATOKA_ATECO classification"),
        on_delete=models.CASCADE,
    )

    topic_classification = models.ForeignKey(
        to=Classification,
        related_name="original_ateko_classifications_mapping",
        limit_choices_to={"scheme": "OPDM_TOPIC_TAG"},
        help_text=_("The topic classification (or tag)"),
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        return f"(ATOKA_ATECO:{self.classification.descr}) -> " \
            f"(OPDM_TOPIC_TAG:{self.topic_classification.descr})"
