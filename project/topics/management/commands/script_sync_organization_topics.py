# coding=utf-8
from django.contrib.contenttypes.models import ContentType
from popolo.models import Classification, ClassificationRel, Organization

from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Synchronize organization topics, from the existing mappings"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument(
            "--reset",
            dest="reset",
            action='store_true',
            help="Reset all organization topics, by deleting all of them before re-creating them.",
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        try:
            chunk_size = 1000
            batch_size = 100
            reset = options['reset']

            org_content_type = ContentType.objects.filter(model='organization').first()
            self.logger.info("Start loading procedure")

            if reset:
                # only reset classification relations
                topics = ClassificationRel.objects.filter(
                    content_type=org_content_type,
                    classification__scheme='OPDM_TOPIC_TAG',
                    classification__original_ateko_classifications_mapping__classification__scheme='ATOKA_ATECO'
                ).distinct()

                self.logger.info(
                    f"Removing all {topics.count()} topics coming from atoka, to perform a reset, as requested."
                )
                topics._raw_delete(topics.db)

            for ateco_cl in Classification.objects.filter(
                scheme="ATOKA_ATECO",
            ):
                if ateco_cl.topic_mapping.count():
                    # read the one topic mapped to this ateco classification
                    topic_cl = ateco_cl.topic_mapping.first().topic_classification
                    diff_ids = list(set(
                        Organization.objects.filter(
                            classifications__classification=ateco_cl
                        ).exclude(
                            classification__in=['Regione', 'Provincia', 'Comune']
                        ).values_list('id', flat=True)
                    ) - set(
                        Organization.objects.filter(classifications__classification=topic_cl).
                        values_list('id', flat=True)
                    ))

                    self.logger.info(f"{ateco_cl.descr} => {topic_cl.descr} = {len(diff_ids)}")
                    for n, c in enumerate(range(0, len(diff_ids), chunk_size)):
                        chunk_ids = diff_ids[c:c + chunk_size]
                        to_add_ids = list(set(chunk_ids))
                        items_objects = [
                            ClassificationRel(
                                content_type=org_content_type,
                                object_id=oid,
                                classification=topic_cl)
                            for oid in to_add_ids
                        ]
                        ClassificationRel.objects.filter(
                            content_type=org_content_type,
                            object_id__in=to_add_ids,
                            classification__scheme="OPDM_TOPIC_TAG"
                        ).delete()
                        ClassificationRel.objects.bulk_create(items_objects, batch_size=batch_size)

                        if n > 0:
                            self.logger.info(
                                "{0}/{1}".format(
                                    n * chunk_size, len(diff_ids)
                                )
                            )
                else:
                    self.logger.warning(f"{ateco_cl.descr} => No topic assigned")

            self.logger.info("End of procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes
