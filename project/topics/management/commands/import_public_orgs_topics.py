# coding=utf-8
from ooetl.extractors import CSVExtractor
from popolo.models import Classification, Organization
from numpy import int64, nan

from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """This command should only be launched once, to add given topic to oprganizations.
    Future topic classification can be done using the UI, with the scheme: OPDM_TOPIC_TAG
    """
    help = "Import organizations topic, from a csv source"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument(
            "--criteria-file",
            dest="criteria_file",
            help="Criteria to assign topics to organizations, starting from their IDs"
            "(path or URL to file)",
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        try:
            criteria_file = options['criteria_file']

            self.logger.info("Starting procedure")

            # extract criteria list from file
            if criteria_file is None:
                self.logger.error("criteria-file must be given: use a local path or a remote url to a CSV file")
                return "Done!"
            criteria_df = CSVExtractor(
                criteria_file,
                sep=",",
                dtype={
                    'id': int64,
                }
            ).extract()

            for crit in criteria_df.to_dict(orient="records"):
                org_id = crit['org_id']
                try:
                    o = Organization.objects.get(id=org_id)
                except Organization.DoesNotExist:
                    self.logger.error(f"Could not find Organization for {crit['org_id']}.")
                    continue

                for i in range(1, 5):
                    topic = crit.get(f'topic_{i}', None)
                    if topic and topic is not nan:
                        topic_cl, created = Classification.objects.get_or_create(
                            scheme="OPDM_TOPIC_TAG",
                            descr=topic
                        )
                        if created:
                            self.logger.debug(f"Topic {topic} added to OPDM_TOPIC_TAG Classifications")

                        o.add_classification_rel(topic_cl, allow_same_scheme=True)
                        self.logger.info(f"{topic} added to {o}")

            self.logger.info("End of procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes
