# coding=utf-8
from ooetl.extractors import CSVExtractor
from popolo.models import Classification

from project.topics.models import OrgMapping
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Import organization classification to topics mappings, from a csv source"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument(
            "--criteria-file",
            dest="criteria_file",
            help="Criteria to assign topics to organizations, classified with given atoka classifications"
            "(path or URL to file)",
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        try:
            criteria_file = options['criteria_file']

            self.logger.info("Starting procedure")

            # extract criteria list from file
            if criteria_file is None:
                self.logger.error("criteria-file must be given: use a local path or a remote url to a CSV file")
                return "Done!"
            criteria_df = CSVExtractor(
                criteria_file,
                sep=",",
            ).extract()

            for crit in criteria_df.to_dict(orient="records"):
                topic = crit['topic']
                ateco_code = crit['ateco_code']

                atoka_cls = Classification.objects.filter(
                    scheme="ATOKA_ATECO",
                    code__istartswith=ateco_code
                )
                if atoka_cls.count() == 0:
                    self.logger.error(f"Could not find ATOKA_ATECO classification for {ateco_code}.")
                    continue

                topic_cl, created = Classification.objects.get_or_create(
                    scheme="OPDM_TOPIC_TAG",
                    descr=topic
                )
                if created:
                    self.logger.debug(f"Topic {topic} added to OPDM_TOPIC_TAG Classifications")

                for cl in atoka_cls:
                    m, created = OrgMapping.objects.get_or_create(
                        classification=cl,
                        topic_classification=topic_cl
                    )
                    if created:
                        self.logger.info(f"{m} created.")
                    else:
                        self.logger.info(f"{m} already existing.")

            self.logger.info("End of procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes
