import logging

import django.test
from io import StringIO
from django.core.management import call_command
from popolo.models import Classification
from popolo.tests.factories import OrganizationFactory


class ImportAtecoTopicMappingTest(django.test.TestCase):
    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.topics.management.commands.test_logging_command"
        )
        logger.handlers = []

    def test_criteriafile_required(self):
        out = StringIO()
        call_command(
            "import_ateco_topics_mappings", verbosity=2, stdout=out
        )
        self.assertTrue("ERROR" in out.getvalue() and "criteria-file must be given" in out.getvalue())

    def test_topics_assigned_correctly(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        org_a.add_classification(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        org_b.add_classification(scheme='ATOKA_ATECO', code='C-2', descr='Ateco 2')
        org_c.add_classification(scheme='ATOKA_ATECO', code='C-3', descr='Ateco 3')

        out = StringIO()
        call_command(
            "import_ateco_topics_mappings",
            verbosity=2,
            criteria_file='project/topics/tests/data/ateco_topics_A.csv',
            stdout=out
        )
        self.assertTrue("ERROR" not in out.getvalue())
        self.assertTrue(
            all([
                'Topic A' in Classification.objects.filter(
                    scheme='OPDM_TOPIC_TAG'
                ).values_list('descr', flat=True),
                'Topic B' in Classification.objects.filter(
                    scheme='OPDM_TOPIC_TAG'
                ).values_list('descr', flat=True),
                'Topic C' not in Classification.objects.filter(
                    scheme='OPDM_TOPIC_TAG'
                ).values_list('descr', flat=True)
            ])
        )

    def test_missing_classification(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        org_a.add_classification(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        org_b.add_classification(scheme='ATOKA_ATECO', code='C-2', descr='Ateco 2')
        org_c.add_classification(scheme='ATOKA_ATECO', code='C-2', descr='Ateco 2')

        out = StringIO()
        call_command(
            "import_ateco_topics_mappings",
            verbosity=2,
            criteria_file='project/topics/tests/data/ateco_topics_A.csv',
            stdout=out
        )
        self.assertTrue("ERROR" in out.getvalue())
        self.assertTrue("Could not find ATOKA_ATECO classification" in out.getvalue())

    def test_handle_double_mapping(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()

        org_a.add_classification(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        org_b.add_classification(scheme='ATOKA_ATECO', code='C-2', descr='Ateco 2')

        out = StringIO()
        call_command(
            "import_ateco_topics_mappings",
            verbosity=2,
            criteria_file='project/topics/tests/data/ateco_topics_B.csv',
            stdout=out
        )
        self.assertTrue("ERROR" not in out.getvalue())
        self.assertTrue("already existing" in out.getvalue())


class ImportOrganizationsTopicsTest(django.test.TestCase):
    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.topics.management.commands.test_logging_command"
        )
        logger.handlers = []

    def test_criteriafile_required(self):
        out = StringIO()
        call_command(
            "import_public_orgs_topics", verbosity=2, stdout=out
        )
        self.assertTrue("ERROR" in out.getvalue() and "criteria-file must be given" in out.getvalue())

    def test_topics_assigned_correctly(self):
        org_a = OrganizationFactory.create(id=1)
        org_b = OrganizationFactory.create(id=2)
        org_c = OrganizationFactory.create(id=3)

        out = StringIO()
        call_command(
            "import_public_orgs_topics",
            verbosity=2,
            criteria_file='project/topics/tests/data/organization_topics_A.csv',
            stdout=out
        )
        self.assertTrue("ERROR" not in out.getvalue())
        self.assertTrue(org_a.classifications.count() == 4)
        self.assertTrue(org_b.classifications.count() == 2)
        self.assertTrue(org_c.classifications.count() == 0)

    def test_missing_organization(self):
        OrganizationFactory.create(id=1)
        OrganizationFactory.create(id=2)
        OrganizationFactory.create(id=4)

        out = StringIO()
        call_command(
            "import_public_orgs_topics",
            verbosity=2,
            criteria_file='project/topics/tests/data/organization_topics_A.csv',
            stdout=out
        )

        self.assertTrue("ERROR" in out.getvalue())
        self.assertTrue("Could not find Organization" in out.getvalue())
