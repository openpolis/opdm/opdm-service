import logging

import django.test
from io import StringIO
from django.core.management import call_command
from popolo.tests.factories import OrganizationFactory, ClassificationFactory

from project.topics.models import OrgMapping


class SyncOrgMappingTest(django.test.TestCase):
    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.topics.management.commands.test_logging_command"
        )
        logger.handlers = []

    def test_topics_synched_correctly(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        cl_b = ClassificationFactory(scheme='ATOKA_ATECO', code='C-2', descr='Ateco 2')
        cl_c = ClassificationFactory(scheme='ATOKA_ATECO', code='C-3', descr='Ateco 3')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)
        org_c.add_classification_rel(cl_c)

        topic_1 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 1')
        topic_2 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 2')

        org_c.add_classification_rel(topic_1)

        OrgMapping.objects.create(classification=cl_a, topic_classification=topic_1)
        OrgMapping.objects.create(classification=cl_b, topic_classification=topic_2)
        OrgMapping.objects.create(classification=cl_c, topic_classification=topic_2)

        out = StringIO()
        call_command(
            "script_sync_organization_topics", verbosity=2, stdout=out
        )
        self.assertTrue(
            all([
                org_a.classifications.filter(classification=topic_1).count() == 1,
                org_b.classifications.filter(classification=topic_2).count() == 1,
                org_c.classifications.filter(classification=topic_2).count() == 1,
            ])
        )

    def test_topics_sync_does_not_remove(self):
        """Organization already labeled are not reset when mapping changes, if --reset is not used
        """
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()

        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        cl_b = ClassificationFactory(scheme='ATOKA_ATECO', code='C-2', descr='Ateco 2')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)

        topic_1 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 1')
        topic_2 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 2')

        org_a.add_classification_rel(topic_1)
        org_b.add_classification_rel(topic_2)

        OrgMapping.objects.create(classification=cl_a, topic_classification=topic_1)

        out = StringIO()
        call_command(
            "script_sync_organization_topics", verbosity=2, stdout=out
        )
        self.assertTrue(
            all([
                org_a.classifications.filter(classification=topic_1).count() == 1,
                org_b.classifications.filter(classification=topic_2).count() == 1,
            ])
        )

    def test_topics_sync_with_reset_does_remove(self):
        """Organization already labeled are reset when mapping changes, if --reset is being used
        """
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()

        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        cl_b = ClassificationFactory(scheme='ATOKA_ATECO', code='C-2', descr='Ateco 2')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)

        topic_1 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 1')
        topic_2 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 2')

        org_b.add_classification_rel(topic_2)

        OrgMapping.objects.create(classification=cl_a, topic_classification=topic_1)

        out = StringIO()
        call_command(
            "script_sync_organization_topics", verbosity=2, reset=True, stdout=out
        )
        self.assertTrue(
            all([
                org_a.classifications.filter(classification=topic_1).count() == 1,
                org_b.classifications.filter(classification=topic_2).count() == 1,
            ])
        )

    def test_missing_classification_mapping(self):
        """A ATOKA_ATECO classification has no mapping to a topic"""
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        org_a.add_classification_rel(cl_a)

        out = StringIO()
        call_command(
            "script_sync_organization_topics", verbosity=2, stdout=out
        )
        self.assertTrue('WARNING' in out.getvalue() and 'No topic assigned')

    def test_sync_local_institution_with_ateco_not_classified(self):
        """An organization of type Regione, Provincia or Comune,
        with a mapped ATOKA_ATECO classification does not get a topic tag"""
        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        topic_1 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 1')
        OrgMapping.objects.create(classification=cl_a, topic_classification=topic_1)

        for inst_classification in ['Regione', 'Provincia', 'Comune']:
            org = OrganizationFactory.create(classification=inst_classification)
            org.add_classification_rel(cl_a)
            out = StringIO()
            call_command(
                "script_sync_organization_topics", verbosity=2, reset=True, stdout=out
            )
            self.assertTrue(
                all([
                    org.classifications.filter(classification=topic_1).count() == 0,
                ])
            )
