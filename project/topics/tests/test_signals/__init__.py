import logging
import django.test
from popolo.tests.factories import OrganizationFactory, ClassificationFactory
from project.topics.models import OrgMapping
from project.topics import signals


class OrgSignalsTest(django.test.TestCase):

    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.topics"
        )
        logger.handlers = []
        signals.connect()

    @classmethod
    def tearDownClass(cls):
        signals.disconnect()
        super().tearDownClass()

    def test_add_topic_to_org(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')

        topic_1 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 1')
        OrgMapping.objects.create(classification=cl_a, topic_classification=topic_1)

        org_a.add_classification_rel(cl_a)
        self.assertTrue(
            org_a.classifications.filter(classification__scheme='OPDM_TOPIC_TAG').count() == 1
        )
        self.assertTrue(
            org_a.classifications.filter(classification=topic_1).count() == 1
        )

    def test_remove_topic_from_org(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        topic_1 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 1')
        OrgMapping.objects.create(classification=cl_a, topic_classification=topic_1)
        org_a.add_classification_rel(cl_a)
        self.assertTrue(
            org_a.classifications.filter(classification__scheme='OPDM_TOPIC_TAG').count() == 1
        )
        self.assertTrue(
            org_a.classifications.filter(classification=topic_1).count() == 1
        )

        org_a.classifications.filter(classification=cl_a).delete()
        self.assertTrue(
            org_a.classifications.filter(classification__scheme='OPDM_TOPIC_TAG').count() == 0
        )
        self.assertTrue(
            org_a.classifications.filter(classification=topic_1).count() == 0
        )

    def test_add_classification_for_missing_topic(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        org_a.add_classification_rel(cl_a)
        self.assertTrue(
            org_a.classifications.count() == 1
        )
        self.assertTrue(
            org_a.classifications.filter(classification__scheme='OPDM_TOPIC_TAG').count() == 0
        )

    def test_remove_classification_for_missing_topic(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')
        org_a.add_classification_rel(cl_a)
        org_a.classifications.filter(classification=cl_a).delete()
        self.assertTrue(
            org_a.classifications.count() == 0
        )

    def test_dont_add_topic_to_local_institution_org(self):
        cl_a = ClassificationFactory(scheme='ATOKA_ATECO', code='C-1', descr='Ateco 1')

        topic_1 = ClassificationFactory(scheme='OPDM_TOPIC_TAG', descr='Topic 1')
        OrgMapping.objects.create(classification=cl_a, topic_classification=topic_1)

        org = OrganizationFactory.create(classification='Regione')
        org.add_classification_rel(cl_a)
        self.assertTrue(
            org.classifications.filter(classification__scheme='OPDM_TOPIC_TAG').count() == 0
        )
        self.assertTrue(
            org.classifications.filter(classification=topic_1).count() == 0
        )
