from project.elections.models import ElectoralResult, ElectoralListResult, ElectoralCandidateResult, \
    ElectoralSubCandidateResult
from popolo.models import Membership, Organization, KeyEvent
import time
from django.db.models import Q
from datetime import datetime
from thefuzz import fuzz
from thefuzz import process
from django.db.models import Field
from django.db.models import Lookup
from io import StringIO
import pandas as pd
import re
import io
import boto3
from taskmanager.management.base import LoggingBaseCommand


def upload_file(dataframe, bucket, key):
    """dat=DataFrame, bucket=bucket name in AWS S3, key=key name in AWS S3"""
    client = boto3.client("s3")
    csv_buffer = io.BytesIO()
    w = io.TextIOWrapper(csv_buffer)
    dataframe.to_csv(w)
    w.seek(0)
    client.upload_fileobj(csv_buffer, bucket, key)


def download_file(bucket, key):
    """bucket=bucket name in AWS S3, key=key name in AWS S3"""
    client = boto3.client("s3")
    s3_object = client.get_object(Bucket=bucket, Key=key)
    csv_string = s3_object['Body'].read().decode('utf-8')

    return pd.read_csv(StringIO(csv_string), sep=";")


def clean_name(x):
    x = x.upper()
    regex = r"(( DETTO | NOTO | DETTA | NOTA ).*)"
    x = re.sub(regex, '', x, 0)
    x = x.replace('\'', '')

    return x


class PersonName(Lookup):
    lookup_name = 'person_name'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return "LEFT(REGEXP_REPLACE(REPLACE(REGEXP_REPLACE(upper(%s), " \
               "'(( DETTO | NOTO | DETTA | NOTA ).*)', ''),'''',''),'(. )', ' ','g'),-1) " \
               "= LEFT(REGEXP_REPLACE(REPLACE(REGEXP_REPLACE(upper(%s), " \
               "'(( DETTO | NOTO | DETTA | NOTA ).*)', ''),'''',''),'(. )', ' ','g'),-1)" \
               % (lhs, rhs), params


Field.register_lookup(PersonName)


class ExtractElected:

    def __init__(self, electoral_event_id, institution_id):
        self.election = KeyEvent.objects.get(id=electoral_event_id)
        self.institution = Organization.objects.get(id=institution_id)
        if self.election.event_type in ['ELE-EU', 'ELE-POL']:
            self.er = ElectoralResult.objects.filter(electoral_event=self.election,
                                                     institution__parent=self.institution)
        else:
            self.er = ElectoralResult.objects.filter(electoral_event=self.election,
                                                     institution=self.institution)
        self.event_type = self.election.event_type

    def popolo_memberships(self):
        return Membership.objects.filter(electoral_event=self.election,
                                         organization__parent=self.institution)

    def electoral_candidates(self):
        return ElectoralCandidateResult.objects.filter(result__in=self.er)

    def electoral_list_results(self):
        return ElectoralListResult.objects.filter(result__in=self.er)

    def electoral_subcandidates(self):
        return ElectoralSubCandidateResult.objects.filter(list_result__result__in=self.er)


class ConnectEligendo:

    def __init__(self, membership):
        self.membership = Membership.objects.get(id=membership)
        self.electoral_event = self.membership.electoral_event
        self.organization = self.membership.organization.parent
        self.constituency_desc = self.membership.constituency_descr_tmp
        self.list_desc = self.membership.electoral_list_descr_tmp
        self.event_type = self.membership.electoral_event.event_type
        self.role = self.membership.role
        self.person = self.membership.person
        self.election_area = f'{self.electoral_event.name} in {self.organization.name}'

    def check_subcandidates(self, extraction, df_membershipsmanual):

        filt_list = []
        if self.event_type == 'ELE-REG':
            filt_list = ['ELECT_CIRC_REG', 'ADM1']
        elif self.event_type == 'ELE-EU':
            filt_list = ['RGNE']
        elif self.event_type == 'ELE-POL':
            filt_list = ['CONT', 'ELECT_COLL']

        subcandidates = extraction.electoral_subcandidates().filter(
            list_result__result__constituency__classification__in=filt_list)
        df_subcandidates = pd.DataFrame(subcandidates.values('name',
                                                             'id',
                                                             'birth_place',
                                                             'birth_date',
                                                             'is_elected',
                                                             'list_result__name',
                                                             'list_result__result__constituency__name',
                                                             'list_result__result__constituency__classification'))

        subcandidate_chosen = process.extract(clean_name(self.person.sort_name),
                                              df_subcandidates['name'].apply(lambda x: clean_name(x.upper())),
                                              scorer=fuzz.WRatio, limit=5)
        try:
            birth_date = datetime.strptime(self.person.birth_date, '%Y-%m-%d').date()
            for index, i in enumerate(subcandidate_chosen):
                chosen = df_subcandidates.loc[i[2]]['birth_date']
                is_elected = df_subcandidates.loc[i[2]]['is_elected']
                if chosen == birth_date:
                    ls = list(subcandidate_chosen[index])
                    ls[1] += 30
                    subcandidate_chosen[index] = tuple(ls)
                if is_elected is True:
                    ls = list(subcandidate_chosen[index])
                    ls[1] += 5
                    subcandidate_chosen[index] = tuple(ls)
            subcandidate_chosen.sort(key=lambda x: x[1], reverse=True)
        except Exception:
            pass
        if (subcandidate_chosen[0][1] > 88) and ((subcandidate_chosen[0][1]) - (subcandidate_chosen[1][1])) > 5:
            chosen = df_subcandidates.loc[subcandidate_chosen[0][2]]
            return subcandidates.get(id=chosen.id)

        elif (subcandidate_chosen[0][1] - subcandidate_chosen[1][1]) < 6 and subcandidate_chosen[0][1] > 88:
            chosen = df_subcandidates.loc[[subcandidate_chosen[0][2], subcandidate_chosen[1][2]]]
            check_unique_elected_true = chosen[chosen['is_elected'] is True]
            check_unique_adm1 = check_unique_elected_true[
                check_unique_elected_true['list_result__result__constituency__classification'] == 'ADM1']
            if check_unique_elected_true.shape[0] == 1:
                return subcandidates.get(id=check_unique_elected_true.id)

            elif check_unique_adm1.shape[0] == 1:
                return subcandidates.get(id=check_unique_adm1.id)
            else:
                manual_choice = df_membershipsmanual[(df_membershipsmanual['name'] == self.person.name)
                                                     & (df_membershipsmanual['elezione'] == self.electoral_event.name)]
                if manual_choice.shape[0] == 1:
                    const = manual_choice.iloc[0]['constituency_chosen']
                    manual_id = chosen[chosen['list_result__result__constituency__name'] == const]
                    return subcandidates.get(id=manual_id.id)
                return None
        else:
            return None

    def check_candidates(self, extraction):
        return extraction.electoral_candidates().filter(result__constituency__classification='ADM1')

    def connect(self, df_membershipsmanual, variable_maps):
        if self.event_type != 'ELE-COM':
            global global_count
            global_count += 1
            print(global_count)
            checkcandidate = ElectoralCandidateResult.objects.filter(elected_membership=self.membership.id)
            checksubcandidate = ElectoralSubCandidateResult.objects.filter(elected_membership=self.membership.id)
            if checksubcandidate or checkcandidate:
                return None
            extraction = ExtractElected(self.membership.electoral_event.id,
                                        self.membership.organization.parent.id
                                        )
            if not extraction.er:
                logging = {'electoral_event': self.electoral_event.id,
                           'election_name': self.electoral_event.name,
                           'institution_id': self.organization.id,
                           'institution_name': self.organization.name,
                           'membership_id': None,
                           'membership_name': None,
                           'role': None,
                           'event_type': self.event_type
                           }
                return logging
                # return None#False, self.election_area
            if self.role != variable_maps['role']:
                candidate = self.check_candidates(extraction).filter(name=self.person.sort_name.upper(),
                                                                     is_counselor=True)

                if candidate:
                    return candidate[0].elected_membership.add(self.membership)

                subcandidate_chosen = self.check_subcandidates(extraction, df_membershipsmanual)
                if subcandidate_chosen:
                    return subcandidate_chosen.elected_membership.add(self.membership)
                else:
                    candidate = self.check_candidates(extraction)\
                        .filter(Q(name__person_name=self.person.sort_name.upper()) |
                                Q(name__person_name=self.person.name.upper()))
                    if len(candidate) == 1:
                        return candidate[0].elected_membership.add(self.membership)
                    else:
                        logging = {'electoral_event': self.electoral_event.id,
                                   'election_name': self.electoral_event.name,
                                   'institution_id': self.organization.id,
                                   'institution_name': self.organization.name,
                                   'membership_id': self.membership.id,
                                   'membership_name': self.person.sort_name,
                                   'role': self.membership.role,
                                   'event_type': self.event_type
                                   }
                        return logging
            else:
                candidate = self.check_candidates(extraction).get(is_top=True)
                candidates = Membership.objects.filter(person=self.membership.person,
                                                       electoral_event=self.electoral_event)
                for i in candidates:
                    candidate.elected_membership.add(i)
                return None

        else:
            pass  # TODO


def processInput(persona_id, df_membershipsmanual, variable_maps):
    return ConnectEligendo(persona_id).connect(df_membershipsmanual, variable_maps)


class Command(LoggingBaseCommand):
    """Command to merge electoral results lists and candidate with popolo_membership references."""

    help = "Merge electoral results lists and candidate from Eligendo to popolo_membership references."

    def handle(self, *args, **kwargs):

        super(Command, self).handle(__name__, *args, formatter_key="simple", **kwargs)
        typology = 'POL'
        variable_maps = {
            'COM': {'event_type': 'ELE-COM',
                    'role': 'Sindaco',
                    'name': 'comunali'},
            'REG': {'event_type': 'ELE-REG',
                    'role': 'Presidente di Regione',
                    'name': 'regionali'},
            'POL': {'event_type': 'ELE-POL',
                    'role': None,
                    'name': 'politiche'},
            'EU': {'event_type': 'ELE-EU',
                   'role': None,
                   'name': 'europee'}
        }

        # if kwargs["all"]:
        ElectoralCandidateResult.elected_membership.through.objects\
            .filter(electoralcandidateresult__result__electoral_event__event_type=variable_maps[typology]
                    ['event_type']).delete()
        ElectoralSubCandidateResult\
            .elected_membership\
            .through\
            .objects\
            .filter(electoralsubcandidateresult__list_result__result__electoral_event__event_type=variable_maps
                    [typology]['event_type']).delete()
        ElectoralListResult.elected_memberships.through.objects.filter(
            electorallistresult__result__electoral_event__event_type=variable_maps[typology]['event_type']).delete()
        regionali = Membership.objects.filter(
            electoral_event__event_type=variable_maps[typology]['event_type'],
            electoral_event__start_date__gt='2012-01-01')

        start_time = time.time()

        global global_count
        global_count = 0
        print("--- %s seconds ---" % (start_time))
        name = variable_maps[typology]['name']
        df_membershipsmanual = download_file('openpolis-drop', f'eligendo/{name}/memberships_manual.csv')

        results = []
        for i in regionali:
            to_attach = [processInput(i.id, df_membershipsmanual, variable_maps[typology])]
            if to_attach != [None]:
                [processInput(i.id, df_membershipsmanual, variable_maps[typology])]
                print(to_attach)
            results += [to_attach]
        print("--- %s seconds ---" % (time.time() - start_time))

        test = [x for x in results if x is not None]
        df_report = pd.DataFrame(list(test)).drop_duplicates()
        file_name = typology + '_anomalies' + '.csv'

        upload_file(df_report, 'openpolis-drop', f'eligendo/{name}/' + file_name)
