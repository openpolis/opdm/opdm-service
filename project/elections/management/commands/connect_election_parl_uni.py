from popolo.models import KeyEvent, Membership as OPDMMembership
from taskmanager.management.base import LoggingBaseCommand

from django.db.models.functions import Upper
from django.db.models import F
from django.db.models import Q
from rest_framework import filters
from project.elections.models import ElectoralResult, ElectoralCandidateResult


class Command(LoggingBaseCommand):
    """Assegna alle memberships di tipo parlamentare, quindi senatori e deputati eletti (eccetto dunque senatori a vita)
    l'area id di elezione.
    L'assegnazione viene fatta sulla base dei record risultati elettorali e va a vedere in primis i candidati
    uninominali per cui non c'è dubbio per l'assegnazione dell'area.
    Più ostica la decisione sui candidati plurinominali nel caso in cui risultino eletti in più circoscrizioni,
    in questo caso Eligendo fornisce solo il dato della prima vittoria e non anche degli 'scorrimenti', in quel caso
    c'è necessità di un controllo manuale post
    """

    help = "Associazione area_id per mmemberships di tipo Deputato e Senatore"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--date',
            dest='date',
            default="2018-03-04",
            help='Data elezione politiche.'
        )

    def handle(self, *args, **options):
        """
        Execute the task.
        """
        super().handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Starting procedure")
        self.date = options['date']

        electoral_event = KeyEvent.objects.get(
            start_date=self.date,
            event_type='ELE-POL',
        )

        self.logger.info(f"Analyzing {electoral_event.name}")
        memberships = OPDMMembership.objects.filter(electoral_event=electoral_event) \
            .annotate(upper_role=Upper(F("role"))).filter(
            upper_role__in=['DEPUTATO', 'SENATORE'])
        ele_uni = ElectoralResult.objects.filter(electoral_event=electoral_event,
                                                 constituency__classification='ELECT_UNI')
        cnt = 0
        for ee in ele_uni:
            try:
                winner = ee.candidate_results.get(is_top=True)
                name_parts = winner.name.split(' ')
                control = True
                while name_parts and control:
                    if len(name_parts) == 1:
                        raise Exception
                    try:

                        queries_1 = [
                            Q(**{"person__name__icontains": x})
                            for x in name_parts
                        ]
                        queries_2 = [
                            Q(**{"person__other_names__name__icontains": x})
                            for x in name_parts
                        ]
                        condition_1 = filters.reduce(filters.operator.and_, queries_1)
                        condition_2 = filters.reduce(filters.operator.and_, queries_2)
                        all_conditions = filters.reduce(filters.operator.or_, [condition_1, condition_2])
                        memb = memberships.filter(all_conditions).values('id').distinct()
                        memb = memberships.get(id__in=memb)
                        if not memb.area_id:
                            memberships = memberships.exclude(id=memb.id)
                            print(f"{winner.name} -> {memb.person.name}")
                            cnt += 1
                            print(cnt)
                        control = False
                        winner.membership = memb
                        winner.save()
                        ElectoralCandidateResult.objects.filter(is_top=True,
                                                                result__electoral_event=winner.result.electoral_event,
                                                                name=winner.name,
                                                                result__institution=winner.result.institution).update(
                            membership=memb)
                    except Exception:
                        name_parts = name_parts[:-1]
            except:
                print(f"Error for {winner.name}")
        return "Done!"
