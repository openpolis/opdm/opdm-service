"""Elections command to import electoral results JSON file."""

from pathlib import Path

from taskmanager.management.base import LoggingBaseCommand

from project.elections.models import ElectoralResult
import os


class Command(LoggingBaseCommand):
    """Command to import electoral results JSON file."""

    help = "Import electoral results, from a JSON file."
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        """Add arguments to the command."""
        parser.add_argument(
            "--filepath",
            dest="filepath",
            type=Path,
            help="Path to the JSON file.",
        )
        parser.add_argument(
            "--d",
            type=bool,
            dest='directory',
            default=False,
            help="If True, it truncates and reloads all connections",
        )

        parser.add_argument(
            "--t",
            type=str,
            dest='typology',
            help="Typology election",
        )

    def handle(self, *args, **options):
        """
        Execute the import function.

        Returned value will be printed to stdout when command finishes.
        """
        super().handle(__name__, *args, formatter_key="simple", **options)
        typology = options["typology"]
        try:
            if options["directory"]:
                for filepath in os.listdir(options["filepath"]):
                    self.logger.info(f"Starting procedure {filepath}")
                    stats = ElectoralResult.load_results(options["filepath"]/filepath, typology, self.logger)
                    imports = ", ".join(f"{v} {k}" for k, v in stats.items() if k != "errors")
                    self.logger.info(f"Imported: {imports}")
                    for error in stats["errors"]:
                        self.logger.error(error)
                    self.logger.info("End of procedure")
            else:
                filepath = options["filepath"]
                self.logger.info("Starting procedure")
                stats = ElectoralResult.load_results(filepath,typology, self.logger)
                imports = ", ".join(f"{v} {k}" for k, v in stats.items() if k != "errors")
                self.logger.info(f"Imported: {imports}")
                for error in stats["errors"]:
                    self.logger.error(error)
                self.logger.info("End of procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"
