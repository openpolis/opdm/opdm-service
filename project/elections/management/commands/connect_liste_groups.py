import pandas as pd
import re
from django.conf import settings
from django.db.models import Func, F, Value, JSONField, Q
from taskmanager.management.base import LoggingBaseCommand
from popolo.models import Organization
from project.elections.models import ElectoralResult, ElectoralListResult
from tqdm import tqdm


def find_name(x):
    return f'^{x}[^A-Z0-9]|[^A-Z0-9]{x}[^A-Z0-9]|[^A-Z0-9]{x}$|^{x}$'


class Command(LoggingBaseCommand):
    """Load political colour data into table."""

    help = "Load political colour data into table."

    def handle(self, *args, **options):
        super().handle(__name__, *args, formatter_key="simple", **options)
        self.logger.info("Starting procedure for tagging party to OPDM_PARTY_COLOUR")
        path_file = settings.ROOT_PATH / "data" / "political_colour" / 'liste_da_associare.csv'
        organizations = Organization.objects.filter(classification='Partito/Movimento politico')

        df = pd.read_csv(path_file, sep=",")
        df = df.fillna('0')
        df = df[df['list_1'] != '0']

        columns_list = [f'list_{i}' for i in range(1, 6)]

        for index, row in df.iterrows():
            print(index)
            # comunali = ElectoralListResult.objects.filter(result__electoral_event__event_type='ELE-COM')
            all_listresults = ElectoralListResult.objects.filter(name__iexact=row['name']).filter(
                Q(result__parent__isnull=True)|
                Q(result__constituency__classification__in=['ELECT_UNI', 'ELECT_COLL', 'ADM1'])
            )

            for i in all_listresults:
                a = i.dati_specifici
                a['association_group_loose'] = True
                # i.dati_specifici = i.dati_specifici['association_group_loose'] = True
            ElectoralListResult.objects.bulk_update(all_listresults, ['dati_specifici'])
            # all_listresults.update(dati_specifici=Func(
            #     F("dati_specifici"),
            #     Value(["association_group_loose"]),
            #     Value(True, JSONField()),
            #     function="jsonb_set"
            # ))
            if row['list_1'] == 0:
                continue
            for i in columns_list:
                if row[i] == '0':
                    break
                name = row[i]
                if row[i] == '=Europa':
                    name = '+Europa'

                print(i)
                print(name)
                if name == 'Forza Italia':
                    org = organizations.get(name__iexact=name, start_date='2013-09-18')
                elif name == 'Partito Liberale Italiano':
                    org = organizations.get(name__iexact=name, start_date='1997-07-04')
                elif name == 'Democrazia Cristiana':
                    org = organizations.get(name__iexact=name, start_date='2002-06-09')
                else:
                    regex = r'(\d{4})'
                    result = re.findall(regex, row[i])
                    if result:
                        name = re.sub(regex, '', name, 0, re.MULTILINE)
                        name = re.sub(r' - $', '', name, 0, re.MULTILINE)
                        name = re.sub(r' \(\)$', '', name, 0, re.MULTILINE)
                        org = Organization.objects.get(name__iexact=name, start_date__icontains=result[0])

                    else:
                        try:
                            name = name.replace('Articolo 1', 'Articolo Uno')
                            name = name.replace('Südtiroler Volkspartei', 'Südtiroler Volkspartei - SVP')
                            name = name.replace('Democrazia Solidale', 'Democrazia Solidale - Demos')
                            org = Organization.objects.get(name__iexact=name)
                        except:
                            try:
                                custom_error = True
                                print('a')
                                if custom_error:
                                    continue
                            except:
                                continue
                print(org.name)
                [x.parties.add(org) for x in all_listresults]

        path_file = settings.ROOT_PATH / "data" / "political_colour" / 'update_2024.csv'

        df = pd.read_csv(path_file, sep=",")
        df = df.fillna('0')
        df = df[df['list_1'] != '0']

        columns_list = [f'list_{i}' for i in range(1, 6)]

        for index, row in df.iterrows():
            print(index)
            # comunali = ElectoralListResult.objects.filter(result__electoral_event__event_type='ELE-COM')
            all_listresults = ElectoralListResult.objects.filter(name__iexact=row['name']).filter(
                Q(result__parent__isnull=True)|
                Q(result__constituency__classification__in=['ELECT_UNI', 'ELECT_COLL', 'ADM1'])
            )

            for i in all_listresults:
                a = i.dati_specifici
                a['association_group_loose'] = True
                # i.dati_specifici = i.dati_specifici['association_group_loose'] = True
            ElectoralListResult.objects.bulk_update(all_listresults, ['dati_specifici'])
            # all_listresults.update(dati_specifici=Func(
            #     F("dati_specifici"),
            #     Value(["association_group_loose"]),
            #     Value(True, JSONField()),
            #     function="jsonb_set"
            # ))
            if row['list_1'] == 0:
                continue
            for i in columns_list:
                if row[i] == '0':
                    break
                name = row[i]
                if row[i] == '=Europa':
                    name = '+Europa'

                print(i)
                print(name)
                if name == 'Forza Italia':
                    org = organizations.get(name__iexact=name, start_date='2013-09-18')
                elif name == 'Partito Liberale Italiano':
                    org = organizations.get(name__iexact=name, start_date='1997-07-04')
                elif name == 'Democrazia Cristiana':
                    org = organizations.get(name__iexact=name, start_date='2002-06-09')
                else:
                    regex = r'(\d{4})'
                    result = re.findall(regex, row[i])
                    if result:
                        name = re.sub(regex, '', name, 0, re.MULTILINE)
                        name = re.sub(r' - $', '', name, 0, re.MULTILINE)
                        name = re.sub(r' \(\)$', '', name, 0, re.MULTILINE)
                        org = Organization.objects.get(name__iexact=name, start_date__icontains=result[0])

                    else:
                        try:
                            name = name.replace('Articolo 1', 'Articolo Uno')
                            name = name.replace('Südtiroler Volkspartei', 'Südtiroler Volkspartei - SVP')
                            name = name.replace('Democrazia Solidale', 'Democrazia Solidale - Demos')
                            org = Organization.objects.get(name__iexact=name)
                        except:
                            try:
                                custom_error = True
                                print('a')
                                if custom_error:
                                    continue
                            except:
                                continue
                print(org.name)
                [x.parties.add(org) for x in all_listresults]

        path_file = settings.ROOT_PATH / "data" / "political_colour" / 'liste_senza_partiti.csv'
        df = pd.read_csv(path_file, sep=",")
        df = df.fillna('0')
        for index, row in df.iterrows():
            print(index)
            # comunali = ElectoralListResult.objects.filter(result__electoral_event__event_type='ELE-COM')
            all_listresults = ElectoralListResult.objects.filter(name__iexact=row['name']).filter(
                Q(result__parent__isnull=True)|
                Q(result__constituency__classification__in=['ELECT_UNI', 'ELECT_COLL', 'ADM1'])
            )
            for i in all_listresults:
                a = i.dati_specifici
                a['missing_sure'] = True
                # i.dati_specifici = i.dati_specifici['missing_sure'] = True
                # i.dati_specifici = {'missing_sure': True}
            ElectoralListResult.objects.bulk_update(all_listresults, ['dati_specifici'])


        path_file = settings.ROOT_PATH / "data" / "political_colour" / 'scorporo_liste_omonime.csv'
        organizations = Organization.objects.filter(classification='Partito/Movimento politico')
        df = pd.read_csv(path_file, sep=",")
        df = df.fillna('0')
        df = df[df['list_1'] != '0']

        columns_list = [f'list_{i}' for i in range(1, 6)]

        for index, row in df.iterrows():

            print(index)
            elections = row['elezione'].split(";")
            for election in elections:
                regex = r"(.+) - (.+) \((.+)\)"
                matches = re.findall(regex, election, re.MULTILINE)[0]
                er = ElectoralResult.objects.get(
                    electoral_event__name=matches[0],
                    constituency__name=matches[1],
                    constituency__parent__name=matches[2]
                )
                # print(matches)
                # print(er)

                all_listresults = ElectoralListResult.objects.filter(name__iexact=row['lista'],
                                                                     result=er).filter(
                    Q(result__parent__isnull=True)|
                    Q(result__constituency__classification__in=['ELECT_UNI', 'ELECT_COLL', 'ADM1'])
            )
                for i in all_listresults:
                    a = i.dati_specifici
                    a['homonym_lists'] = True
                    # i.dati_specifici = i.dati_specifici['homonym_lists'] = True
                    # i.dati_specifici = {'homonym_lists': True}
                ElectoralListResult.objects.bulk_update(all_listresults, ['dati_specifici'])

                for i in columns_list:

                    if row[i] == '0':
                        break
                    name = row[i]
                    if row[i] == '=Europa':
                        name = '+Europa'

                    # print(i)
                    # print(name)
                    if name == 'Forza Italia':
                        org = organizations.get(name__iexact=name, start_date='2013-09-18')
                    elif name == 'Partito Liberale Italiano':
                        org = organizations.get(name__iexact=name, start_date='1997-07-04')
                    elif name == 'Democrazia Cristiana':
                        org = organizations.get(name__iexact=name, start_date='2002-06-09')
                    else:
                        regex = r'(\d{4})'
                        result = re.findall(regex, row[i])
                        try:
                            if result:
                                name = re.sub(regex, '', name, 0, re.MULTILINE)
                                name = re.sub(r' - $', '', name, 0, re.MULTILINE)
                                name = re.sub(r' \(\)$', '', name, 0, re.MULTILINE)
                                name = name.replace('Articolo 1', 'Articolo Uno')
                                name = name.replace('Südtiroler Volkspartei', 'Südtiroler Volkspartei - SVP')
                                name = name.replace('Democrazia Solidale', 'Democrazia Solidale - Demos')

                                org = Organization.objects.get(name__iexact=name, start_date__icontains=result[0])

                            else:
                                name = name.replace('Articolo 1', 'Articolo Uno')
                                name = name.replace('Südtiroler Volkspartei', 'Südtiroler Volkspartei - SVP')
                                name = name.replace('Democrazia Solidale', 'Democrazia Solidale - Demos')

                                org = Organization.objects.get(name__iexact=name)
                        except:
                            try:
                                custom_error = True
                                print('a')
                                if custom_error:
                                    continue
                            except:
                                continue
                    print(all_listresults)
                    print(org.name)
                    [x.parties.add(org) for x in all_listresults]

        path_file = settings.ROOT_PATH / "data" / "political_colour" / 'mapping_liste_partiti_part_2_ettore.csv'
        df = pd.read_csv(path_file, sep=",")
        for index, row in df.iterrows():
            try:
                lista = ElectoralListResult.objects.get(result__electoral_event_id=row['id_keyevent'],
                                    result__constituency__name=row['area'],
                                    name=row['list_name'])
            except:
                # print(f"{row['list_name'].decode('utf-8')}")
                continue
            org = Organization.objects.get(id=row['org_id'])
            lista.parties.add(org)

        ORG_OTHER_NAME = {
            'Alleanza di Centro (AdC)': ['ALLEANZA DI CENTRO-DEMOCRAZIA CRISTIANA',
                                         'ALLEANZA DI CENTRO - GRANDE SUD',
                                         'CRISTIANO POPOLARI - ALLEANZA DI CENTRO',
                                         'MODERATI POPOLARI - ALLEANZA DI CENTRO'
                                         ],
            'Articolo 1 - Movimento Democratico e Progressista': [find_name('ARTICOLO 1')],
            'Centro Democratico': ['CENTRO DEMOCRATICO-LIBERAL DEMOCRATICI',
                                   'INNOVIAMO MATERA-CENTRO DEMOCRATICO',
                                   'ITALIA VIVA-PARTITO SOCIALISTA ITALIANO-CENTRO DEMOCRATICO',
                                   'NOI TUTTI LIBERI E PARTECIPI-CENTRO DEMOCRATICO'],
            'Coraggio Italia': [],
            'Democrazia Autonomia (DemA)': [find_name('DEMA'),
                                            'DEMA DEMOCRAZIA AUTONOMIA'
                                            'SINISTRA ANTICAPITALISTA-RIFONDAZIONE COMUNISTA-DEMA DEMOCRAZIA AUTONOMIA'
                                            ],
            'Democrazia Solidale': [],
            'Die Freiheitlichen': [],
            'Direzione Italia': [],
            'Energie per l\'Italia': [],
            '+Europa': [],
            'Europa Verde': [],
            'Fare per Fermare il Declino': [],
            'Federazione dei Verdi': ['Federazione dei Verdi',
                                      r'FED\.VERDI',
                                      'ALLEANZA VERDI E SINISTRA',],
            'Fortza Paris': [],
            'Forza Italia': ['Forza Italia', find_name('FI -'),
                             'FORZA ITALIA'],
            'Forza Nuova': [],
            'Fronte Nazionale': [],
            'Futuro e Libertà per l\'Italia': [],
            'Grande Nord': [],
            'Fratelli d\'Italia': ['Fratelli d\'Italia',
                                   r'FRAT\. D\'IT\.',
                                   'FRATELLI D’ITALIA'],
            'Il Megafono - Lista Crocetta': [],
            'Il Popolo della Famiglia': [],
            'Il Popolo della Libertà': ['Il Popolo della Libert*'],
            'Indipendenza Veneta': [],
            'Io Sud': [],
            'Italexit': [],
            'Italia dei Valori': ['Italia dei Valori',
                                  find_name('IDV'),
                                  r'IT\.VALORI'],
            'Italia in Comune': [],
            'Italia Viva': [],
            'La Destra': [],
            'La Puglia in Più': ['La Puglia in Pi*'],
            'Lega per Salvini Premier': ['Lega per Salvini Premier',
                                         'lega salvini',
                                         'LEGA',
                                         'PRIMA L\'ITALIA SALVINI PREMIER'],
            'Liga Veneta Repubblica': [],
            'Movimento 5 Stelle': ['Movimento 5 Stelle', 'Movimento 5Stelle', 'Movimento Cinque Stelle', 'MOVIMENTO CINQUESTELLE',
                                   'MOVIMENTO V STELLE', 'MOVIMENTO V STELLE 2050',
                                   'Movimento 5 stelle (M5S)'],
            'Movimento Associativo Italiani all\'Estero': ['Movimento Associativo Italiani all\'Estero',
                                                           'MOV.ASSOCIATIVO ITALIANI ALL\'ESTERO'],
            'Movimento Nazionale per la Sovranità': ['Movimento Nazionale per la Sovranit*'],
            'Movimento per le Autonomie': [],
            'Movimento Sociale-Fiamma Tricolore': ['fiamma tricolore'],
            'Noi con l\'Italia': [],
            'Partito Animalista Italiano': ['partito animalissta'],
            'Partito Comunista dei Lavoratori': [],
            'Partito comunista italiano': ['Partito comunista italiano', find_name('PCI')],
            'Partito dei Sardi': [],
            'Partito della Rifondazione Comunista': ['Partito della Rifondazione Comunista',
                                                     'rifondazione comunista',
                                                     r'RIF\.COMUNISTA',
                                                     r'RIF\.COM'],
            'Partito Democratico': ['Partito Democratico',
                                    find_name('PD'),
                                    'Partito Democratico (PD)',
                                    'partito democratico'],
            'Partito di Alternativa Comunista': [],
            'Partito Liberale Italiano': [],
            'Partito Pensionati': [],
            'Partito Pirata Italiano': [],
            'Partito Repubblicano Italiano': ['Partito Repubblicano Italiano',
                                              find_name('PRI')],
            'Partito Sardo d\'Azione': [],
            'Partito Socialista Democratico Italiano': ['Partito Socialista Democratico Italiano',
                                                        find_name('PSDI'),
                                                        find_name(r'P\.S\.I\.')],
            'Partito Socialista Italiano': ['Partito Socialista Italiano',
                                            find_name('PSI'),
                                            r'PART\. SOC\. IT'],
            'PMLI - Partito marxista-leninista italiano': ['PMLI - Partito marxista-leninista italiano',
                                                           find_name(r'P\.COM\.MARX-LEN\.')],
            'Popolari per l\'Italia': [],
            'Popolari UDEUR': ['Popolari UDEUR',
                               'UDEUR'],
            'Potere al Popolo!': [],
            'Riformatori Sardi': [],
            'Rivoluzione cristiana': [],
            'Scelta Civica': [],
            'Sinistra Ecologia Libertà': ['Sinistra Ecologia Libert*',
                                          find_name('SEL'),
                                          r'SIN\.ECOL\. LIBERTA'],
            'Sinistra Italiana': ['ALLEANZA VERDI E SINISTRA',],
            'Stella Alpina': [],
            'Südtiroler Volkspartei': ['S*dtiroler Volkspartei',
                                       find_name('SVP'),
                                       'SVP SÜDTIROLER VOLKSPARTEI',
                                       'Südtiroler Volkspartei',
                                       'Südtiroler Volkspartei (Partito popolare sudtirolese) (SVP)'],
            'Südtiroler Volkspartei - SVP': ['S*dtiroler Volkspartei',
                                       find_name('SVP'),
                                       'SVP SÜDTIROLER VOLKSPARTEI',
                                       'Südtiroler Volkspartei'],
            'Unione per il Trentino': [],
            'Union Valdôtaine Progressiste': ['Union Vald*taine Progressiste'],
            'Volt Italia': [find_name('volt')]
        }

        organizations = Organization.objects.filter(classification='Partito/Movimento politico')

        for org in organizations:
            print(f'{org} - {org.start_date}\n')
            res = ORG_OTHER_NAME.get(org.name)
            if res is not None:
                if len(res) == 0:
                    org_elect = ElectoralListResult.objects.filter(name__iregex=org.name.replace('+', '\\+'),
                                                                   result__electoral_event__start_date__gte=(
                                                                           org.start_date or '1900-01-01'),
                                                                   result__electoral_event__start_date__lte=(
                                                                       org.end_date or '9999-12-31')) \
                        .exclude(parties__id=org.id).filter(    Q(result__parent__isnull=True)|
                                                                Q(result__constituency__classification__in=['ELECT_UNI', 'ELECT_COLL', 'ADM1'])
            )
                    for i in tqdm(org_elect):
                        i.parties.add(org)
                if len(res) > 0:
                    for name in res:
                        org_elect = ElectoralListResult\
                            .objects\
                            .filter(name__iregex=name,
                                    result__electoral_event__start_date__gte=org.start_date,
                                    result__electoral_event__start_date__lte=(org.end_date or '9999-12-31'))\
                            .exclude(parties__id=org.id).filter(    Q(result__parent__isnull=True)|
                Q(result__constituency__classification__in=['ELECT_UNI', 'ELECT_COLL', 'ADM1'])
            )
                        for i in tqdm(org_elect):
                            i.parties.add(org)
