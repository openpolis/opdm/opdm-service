from popolo.models import KeyEvent, Membership
from taskmanager.management.base import LoggingBaseCommand

from django.db.models.functions import Concat, Coalesce
from django.db.models import F, Value, CharField
from django.db.models import Q

from project.elections.models import ElectoralResult


class Command(LoggingBaseCommand):
    """Connect electoral results to popolo entities
    Candidates' results are connected to persons
    Lists' results are connected to a membership's list
    """

    help = "Reconciliate electoral candidates or list to persons or temporary lists in memberships"
    requires_migrations_checks = True
    requires_system_checks = True
    start_date = None

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--start-date',
            dest='start_date',
            default="2010-01-01",
            help='Date to start reconciliation from (up to now). Format YYYY-MM-DD.'
        )

    def handle(self, *args, **options):
        """
        Execute the task.
        """
        super().handle(__name__, *args, formatter_key="simple", **options)

        try:

            self.logger.info("Starting procedure")
            self.start_date = options['start_date']

            electoral_events = KeyEvent.objects.filter(
                start_date__gte=self.start_date,
                id__in=ElectoralResult.objects.values_list('electoral_event__id', flat=True).distinct()
            ).order_by('-start_date')

            self.logger.info(f"Looping over {electoral_events.count()} electoral events")
            for ee in electoral_events:
                results = ElectoralResult.objects.filter(electoral_event=ee)
                self.logger.info(f" {results.count()} electoral results for {ee}")

                name_pattern = 'family_first'
                if 'politiche' in ee.name.lower() and ee.start_date < '2017':
                    name_pattern = 'given_first'

                for r in results:
                    institution = r.institution

                    if 'assemblea' not in institution.name.lower():
                        opdm_memberships = Membership.objects.filter(
                            organization__parent=institution,
                            electoral_event=ee
                        )
                    else:
                        opdm_memberships = Membership.objects.filter(
                            organization=institution,
                            electoral_event=ee
                        )

                    self.connect_persons(opdm_memberships, r, name_pattern=name_pattern)
                    self.connect_lists(opdm_memberships, r)

            self.logger.info("End of procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"

    def connect_persons(self, opdm_memberships, result: ElectoralResult, name_pattern='family_first'):
        """Connect persons in result to persons in opdm membership"""
        n_linked = n_unlinked = 0

        opdm_memberships_with_names = opdm_memberships.annotate(
                name_family_first=Concat(
                    F('person__family_name'), Value(' '), F('person__given_name'),
                    output_field=CharField()
                ),
                name_given_first=Concat(
                    F('person__given_name'), Value(' '), F('person__family_name'),
                    output_field=CharField()
                ),
                other_name=Coalesce(F('person__other_names__name'), Value(''))
            )
        candidates_results = result.candidate_results.filter(
            Q(is_counselor=True) | Q(is_top=True)
        ).exclude(elected_membership__isnull=False)
        n_candidates_results = candidates_results.count()
        if n_candidates_results:
            for candidate_result in candidates_results:
                self.logger.debug(
                    f"   {candidate_result.name} "
                    f"(top: {candidate_result.is_top}, counselor: {candidate_result.is_counselor})"
                )
                try:
                    membership_id = next(
                        filter(
                            lambda x: (
                                x[f'name_{name_pattern}'].lower() == candidate_result.name.lower()
                                or
                                x['other_name'].lower() == candidate_result.name.lower()
                            ),
                            opdm_memberships_with_names.values(
                                f'name_{name_pattern}', 'other_name', 'id'
                            ).distinct()
                        )
                    )['id']
                    candidate_result.elected_membership_id = membership_id
                    candidate_result.save()
                    self.logger.debug(
                        f"    linked membership found: {candidate_result.elected_membership_id}"
                    )
                    n_linked += 1
                except StopIteration:
                    self.logger.warning(
                        f"   unable to find link for {candidate_result} "
                        f"(top: {candidate_result.is_top}, counselor: {candidate_result.is_counselor})"
                    )
                    n_unlinked += 1

            self.logger.info(
                f"  {n_linked}/{n_candidates_results} candidates "
                f"linked for {result.constituency}, {result.institution}"
            )

    def connect_lists(self, opdm_memberships, result: ElectoralResult):
        """Connect lists in result to lists in opdm membership"""

        n_linked = n_unlinked = 0
        list_results = result.list_results.all()
        n_list_results = result.list_results.count()
        if n_list_results:
            for list_result in list_results:
                self.logger.debug(
                    f"   {list_result.name} "
                )

                memberships = opdm_memberships.filter(
                    electoral_list_descr_tmp__icontains=list_result.name
                )
                n_memberships = memberships.count()
                if n_memberships:
                    for m in memberships:
                        list_result.elected_memberships.add(m.id)
                    self.logger.debug(
                        f"   {n_memberships} linked memberships found for {list_result} "
                    )
                    n_linked += 1
                else:
                    self.logger.debug(
                        f"   unable to find link for {list_result.name} "
                    )
                    n_unlinked += 1

            self.logger.info(
                f"  {n_linked}/{n_list_results} lists "
                f"linked for {result.constituency}, {result.institution}"
            )
