import pandas as pd
from popolo.models import KeyEvent, Membership as OPDMMembership
from taskmanager.management.base import LoggingBaseCommand

from django.db.models import Q
from rest_framework import filters
from project.elections.models import ElectoralResult, ElectoralCandidateResult


class Command(LoggingBaseCommand):


    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--date',
            dest='date',
            default="2018-03-04",
            help='Data elezione politiche.'
        )

    def handle(self, *args, **options):
        """
        Execute the task.
        """
        super().handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Starting procedure")
        self.date = options['date']

        electoral_event = KeyEvent.objects.filter(
            event_type='ELE-COM',
        ).filter(start_date__gte=self.date).order_by('-start_date')
        rows_list = []#pd.DataFrame(columns=["name", "is_mayor", "is_counselor", "comune"])

        for event in electoral_event:
            self.logger.info(f"Analyzing {event.name}")
            memberships = OPDMMembership.objects.filter(electoral_event=event)
            ele_com = ElectoralResult.objects.filter(electoral_event=event,
                                                     constituency__classification='ADM3')

            for ee in ele_com:
                # try:
                if not ee.is_valid:
                    continue
                candidates = ee.candidate_results.all()
                submemberships = memberships.filter(organization__parent=ee.institution)
                for candidate in candidates:
                    if candidate.membership:
                        continue
                    try:

                        name_parts = candidate.name.split(' ')
                        control = True
                        while name_parts and control:
                            if len(name_parts) == 1:
                                raise Exception
                            try:

                                queries_1 = [
                                    Q(**{"person__name__icontains": x})
                                    for x in name_parts
                                ]
                                queries_2 = [
                                    Q(**{"person__other_names__name__icontains": x})
                                    for x in name_parts
                                ]
                                condition_1 = filters.reduce(filters.operator.and_, queries_1)
                                condition_2 = filters.reduce(filters.operator.and_, queries_2)
                                all_conditions = filters.reduce(filters.operator.or_, [condition_1, condition_2])
                                if candidate.is_top:
                                    memb = submemberships.filter(role='Sindaco').filter(all_conditions).values('id').distinct()
                                else:
                                    memb = submemberships.exclude(role='Sindaco').filter(all_conditions).values(
                                        'id').distinct()
                                memb = submemberships.get(id__in=memb)
                                # memberships = memberships.exclude(id=memb.id)
                                # print(f"{candidate.name} -> {memb.person.name}")
                                control = False
                                if memb.id in ElectoralCandidateResult.objects.filter(membership_id__isnull=False).values_list('membership_id', flat=True):
                                    continue
                                candidate.membership = memb
                                candidate.save()
                            except Exception:
                                name_parts = name_parts[:-1]
                    except:
                        try:
                            if candidate.is_top:
                                try:
                                    memb = submemberships.filter(role='Sindaco').order_by('start_date').first()
                                    if not memb:
                                        raise Exception
                                    if memb.id in ElectoralCandidateResult.objects.filter(
                                        membership_id__isnull=False).values_list('membership_id', flat=True):
                                        continue
                                    candidate.membership = memb
                                    candidate.save()
                                except:
                                    dict_1 = {
                                    "electoral_event": event.name,
                                    "name": candidate.name,
                                     "is_top": candidate.is_top,
                                     "is_counselor": candidate.is_counselor,
                                     "comune": candidate.result.institution.name,
                                    "comune_id": candidate.result.institution.id}
                                    rows_list.append(dict_1)
                                    # candidate.membership = None
                                    # candidate.save()
                                    print(
                                        f"Error for {candidate.name}; {candidate.is_top}; {candidate.is_counselor}; {candidate.result.institution}")
                            elif candidate.is_counselor:
                                dict_1 = {
                                    "electoral_event": event.name,
                                    "name": candidate.name,
                                     "is_top": candidate.is_top,
                                     "is_counselor": candidate.is_counselor,
                                     "comune": candidate.result.institution.name,
                                    "comune_id": candidate.result.institution.id}
                                rows_list.append(dict_1)
                                # candidate.membership = None
                                # candidate.save()
                                print(f"Error for {candidate.name}; {candidate.is_top}; {candidate.is_counselor}; {candidate.result.institution}")
                        except UnicodeEncodeError:
                            print(
                                f"UnicodeEncodeError Error for ;{candidate.id}; {candidate.is_top}; {candidate.result.institution}")
        df_errors = pd.DataFrame(rows_list)
        df_errors.to_csv("election_errors.csv", index=False)
        return "Done!"
