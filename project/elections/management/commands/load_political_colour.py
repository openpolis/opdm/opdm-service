import datetime
import pandas as pd
from django.conf import settings
from django.db.models import F, Value
from django.db.models.functions import Coalesce
from taskmanager.management.base import LoggingBaseCommand
from popolo.models import Organization, ClassificationRel, Classification


class Command(LoggingBaseCommand):
    """Load political colour data into table."""

    help = "Load political colour data into table."

    def handle(self, *args, **options):
        super().handle(__name__, *args, formatter_key="simple", **options)
        self.logger.info("Starting procedure for tagging party to OPDM_PARTY_COLOUR")
        path_file = settings.ROOT_PATH / "data" / "political_colour" / 'mapping_party_pc_new.csv'
        organizations = Organization.objects.filter(classification='Partito/Movimento politico')
        df = pd.read_csv(path_file, sep=";")
        df = df.where(pd.notnull(df), None)

        for index, row in df.iterrows():
            start_date_ap = datetime.datetime.strftime(
                datetime.datetime.strptime(row['start_date_ap'], '%d/%m/%Y'), '%Y-%m-%d')\
                if row['start_date_ap'] is not None and pd.notna(row['start_date_ap']) else None
            end_date_ap = datetime.datetime.strftime(
                datetime.datetime.strptime(row['start_date_ap'], '%d/%m/%Y'), '%Y-%m-%d')\
                if row['end_date_ap'] is not None and pd.notna(row['end_date_ap']) else None
            org = organizations.filter(name=row['partito']).annotate(
                new_start_date=Coalesce(F('start_date'), Value('1900-01-01'))).get(
                new_start_date=datetime.datetime.strftime(datetime.datetime.strptime(row['start_date'], '%d/%m/%Y'),
                                                          '%Y-%m-%d'))
            classification = Classification.objects.get(scheme='OPDM_PARTY_COLOUR', descr=row['area'])

            ClassificationRel.objects.filter(classification__scheme='OPDM_PARTY_COLOUR').update_or_create(
                                                    object_id=org.id,
                                                    start_date=start_date_ap,
                                                    end_date=end_date_ap,
                                                    content_type_id=28,
                                                    defaults={'classification': classification}
                                                    )
