"""Merging list elections data with popolo_memberships."""

from popolo.models import Area
from taskmanager.management.base import LoggingBaseCommand
from django.db.models import Case, Value, When, FilteredRelation, Q
from django.contrib.postgres.aggregates import ArrayAgg
from django.contrib.gis.db.models import Union
from slugify import slugify
from django.contrib.gis.geos import GEOSGeometry, Polygon, MultiPolygon
from django.db.models import F
from project.elections.models import AreaMapConstituency


class Command(LoggingBaseCommand):
    """"""

    help = "Merge electoral results lists and candidate from Eligendo to popolo_membership references."

    def handle(self, *args, **kwargs):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **kwargs)
        self.logger.info(f"START: Area count: {Area.objects.all().count()}")
        ###
        areas = Area.objects.filter(
            classification='ADM2',
            parent__classification='ADM1').exclude(name__in=[
                'Firenze',
                'Udine']).exclude(parent__name__in=['Sardegna'])

        first_part = areas.annotate(
            new_name=Case(
                When(name__in=['Catanzaro', 'Crotone', 'Vibo Valentia'], then=Value('Centro')),
                When(name__in=['Cosenza'], then=Value('Nord')),
                When(name__in=['Reggio di Calabria'], then=Value('Sud')),
                When(name__in=['Perugia', 'Terni'], then=Value('Circoscrizione Umbria')),
                default='name'
            )).values('new_name', 'geometry', 'parent_id', 'id')

        gb = first_part.values('new_name', 'parent_id').annotate(Union('geometry'),
                                                                 list_ids=ArrayAgg('id',
                                                                                   delimiter=';')
                                                                 )

        for i, r in enumerate(gb):
            geom = GEOSGeometry(r['geometry__union'])

            if isinstance(geom, MultiPolygon):
                pass
            elif isinstance(geom, Polygon):
                geom = MultiPolygon([geom])
            else:
                raise TypeError(
                    '{} not acceptable for this model'.format(geom.geom_type)
                )

            rip, created = Area.objects.update_or_create(
                classification='ELECT_CIRC_REG',
                name=r['new_name'],
                defaults={
                    'identifier': 'ELECT_CIRC_REG_' + r['new_name'],
                    'slug': slugify('ELECT_CIRC_REG_'.lower() + r['new_name']),
                    'geometry': geom,
                    'parent_id': r['list_ids'][0] if len(r['list_ids']) == 1 else r['parent_id']
                }

            )
            self.logger.info(f'{i}- {rip.name} created {created} on Area')

            if len(r['list_ids']) > 1 and len(gb.filter(parent_id=r['parent_id'])) > 1:  # il primo è per individuare
                # se ci sono più aree associate, il secondo per escludere quelle zone con più aree ma degeneri perché
                # comprendenti una area amministrativa già esistente come la regione Umbria
                const = Area.objects.get(name=r['new_name'], classification='ELECT_CIRC_REG')
                for i in r['list_ids']:
                    sub_area = Area.objects.get(id=i)
                    rip, created = AreaMapConstituency.objects.update_or_create(
                        constituency=const,
                        sub_areas=sub_area,
                        defaults={'typology': sub_area.classification}
                                                                                )
                    self.logger.info(f'connected {sub_area.name} to {const.name} created {created}')

        # Firenze
        areas = Area.objects.filter(
            classification='ADM3',
            parent__classification='ADM2').filter(parent__name__in=['Firenze'])
        map_firenze = {
            'Firenze 1': [
                'Firenze'
            ],
            'Firenze 2': [
                'Bagno a Ripoli',
                'Barberino di Mugello',
                'Barberino Tavarnelle',
                'Barberino Val d\'Elsa',
                'Borgo San Lorenzo',
                'Dicomano',
                'Fiesole',
                'Figline e Incisa Valdarno',
                'Firenzuola',
                'Greve in Chianti',
                'Incisa in Val d\'Arno',
                'Figline Valdarno',
                'Impruneta',
                'Londa',
                'Marradi',
                'Palazzuolo sul Senio',
                'Pelago',
                'Pontassieve',
                'Reggello',
                'Rignano sull\'Arno',
                'Rufina',
                'San Casciano in Val di Pesa',
                'San Godenzo',
                'San Piero a Sieve',
                'Scarperia',
                'Scarperia e San Piero',
                'Tavarnelle Val di Pesa',
                'Vaglia',
                'Vicchio'

            ],
            'Firenze 3': [
                'Capraia e Limite',
                'Castelfiorentino',
                'Cerreto Guidi',
                'Certaldo',
                'Empoli',
                'Fucecchio',
                'Gambassi Terme',
                'Montaione',
                'Montelupo Fiorentino',
                'Montespertoli',
                'Vinci'

            ],
            'Firenze 4': [
                'Calenzano',
                'Campi Bisenzio',
                'Lastra a Signa',
                'Scandicci',
                'Sesto Fiorentino',
                'Signa'

            ]
        }
        first_part = areas.annotate(
            new_name=Case(
                When(name__in=map_firenze['Firenze 1'], then=Value('Firenze 1')),
                When(name__in=map_firenze['Firenze 2'], then=Value('Firenze 2')),
                When(name__in=map_firenze['Firenze 3'], then=Value('Firenze 3')),
                When(name__in=map_firenze['Firenze 4'], then=Value('Firenze 4'))
            )).values('new_name', 'geometry', 'parent_id', 'parent__parent_id', 'id')

        gb = first_part.values('new_name', 'parent__parent_id').annotate(Union('geometry'),
                                                                         list_ids=ArrayAgg('id', delimiter=';'))
        for i, r in enumerate(gb):

            geom = GEOSGeometry(r['geometry__union'])

            if isinstance(geom, MultiPolygon):
                pass
            elif isinstance(geom, Polygon):
                geom = MultiPolygon([geom])
            else:
                raise TypeError(
                    '{} not acceptable for this model'.format(geom.geom_type)
                )

            rip, created = Area.objects.update_or_create(
                classification='ELECT_CIRC_REG',
                name=r['new_name'],
                defaults={
                    'identifier': 'ELECT_CIRC_REG_' + r['new_name'],
                    'slug': slugify('ELECT_CIRC_REG_'.lower() + r['new_name']),
                    'geometry': geom,
                    'parent_id': r['parent__parent_id']
                }

            )
            self.logger.info(f'{i}- {rip.name} created {created} on Area')

            const = Area.objects.get(name=r['new_name'], classification='ELECT_CIRC_REG')
            for i in r['list_ids']:
                sub_area = Area.objects.get(id=i)
                rip, created = AreaMapConstituency.objects.get_or_create(
                    constituency=const,
                    sub_areas=sub_area,
                    defaults={'typology': sub_area.classification}
                )
                self.logger.info(f'connected {sub_area.name} to {const.name} created {created}')

        # Friuli
        areas = Area.objects.filter(
            classification='ADM3',
            parent__classification='ADM2').filter(parent__name__in=[
                'Udine'
            ])
        map_tolmezzo = {
            'Tolmezzo': [
                'Sappada',
                'Resia',
                'Resiutta',
                'Rigolato',
                'Sauris',
                'Socchieve',
                'Sutrio',
                'Tarvisio',
                'Tolmezzo',
                'Trasaghis',
                'Treppo Carnico',
                'Treppo Ligosullo',
                'Venzone',
                'Verzegnis',
                'Villa Santina',
                'Zuglio',
                'Gemona del Friuli',
                'Lauco',
                'Ligosullo',
                'Malborghetto Valbruna',
                'Moggio Udinese',
                'Montenars',
                'Osoppo',
                'Ovaro',
                'Paluzza',
                'Paularo',
                'Pontebba',
                'Prato Carnico',
                'Preone',
                'Ravascletto',
                'Raveo',
                'Amaro',
                'Ampezzo',
                'Arta Terme',
                'Artegna',
                'Bordano',
                'Buja',
                'Cavazzo Carnico',
                'Cercivento',
                'Chiusaforte',
                'Comeglians',
                'Dogna',
                'Enemonzo',
                'Forni Avoltri',
                'Forni di Sopra',
                'Forni di Sotto'

            ],
        }
        first_part = areas.annotate(
            new_name=Case(
                When(name__in=map_tolmezzo['Tolmezzo'], then=Value('Tolmezzo')),
                default='parent__name'
            )).values('new_name', 'geometry', 'parent_id', 'parent__parent_id', 'id')

        gb = first_part.values('new_name', 'parent__parent_id').annotate(
            Union('geometry'),
            list_ids=ArrayAgg('id', delimiter=';')
                                                    )
        for i, r in enumerate(gb):

            geom = GEOSGeometry(r['geometry__union'])

            if isinstance(geom, MultiPolygon):
                pass
            elif isinstance(geom, Polygon):
                geom = MultiPolygon([geom])
            else:
                raise TypeError(
                    '{} not acceptable for this model'.format(geom.geom_type)
                )

            rip, created = Area.objects.update_or_create(
                classification='ELECT_CIRC_REG',
                name=r['new_name'],
                defaults={
                    'identifier': 'ELECT_CIRC_REG_' + r['new_name'],
                    'slug': slugify('ELECT_CIRC_REG_'.lower() + r['new_name']),
                    'geometry': geom,
                    'parent_id': r['parent__parent_id']
                }

            )
            self.logger.info(f'{i}- {rip.name} created {created} on Area')
            const = Area.objects.get(name=r['new_name'], classification='ELECT_CIRC_REG')
            for rl in r['list_ids']:
                sub_area = Area.objects.get(id=rl)
                AreaMapConstituency.objects.get_or_create(constituency=const,
                                                          sub_areas=sub_area,
                                                          defaults={'typology': sub_area.classification}
                                                          )
                self.logger.info(f'connected {sub_area.name} to {const.name} created {created}')

            print(created)
            print(rip)

        # Sardegna
        area_sardegna = Area.objects.annotate(
            has_tag=FilteredRelation(
                'from_relationships',
                condition=Q(from_relationships__end_date__gt='2014-04-01'),
            ))\
            .filter(
            Q(
                from_relationships__end_date__gt='2014-01-01',
                parent__parent__name='Sardegna') |
            Q(
                classification='ADM3',
                parent__classification='ADM2',
                parent__parent__classification='ADM1',
                parent__name__in=[
                    'Sassari',
                    'Oristano',
                    'Nuoro',
                    'Cagliari'
                ],
                has_tag__isnull=True,))\
            .values('id',
                    'name',
                    'parent__parent__name',
                    new_name=Case(
                        When(has_tag__dest_area__name__isnull=True,
                             then='parent__name'),
                        default=F('has_tag__dest_area__name')),
                    new_geometry=F('geometry'),
                    )

        gb = area_sardegna.values('new_name', 'parent__parent__id').annotate(Union('new_geometry'),
                                                                             list_ids=ArrayAgg('id', delimiter=';')
                                                                             )
        for i, r in enumerate(gb):
            print(i)
            geom = GEOSGeometry(r['new_geometry__union'])

            if isinstance(geom, MultiPolygon):
                pass
            elif isinstance(geom, Polygon):
                geom = MultiPolygon([geom])
            else:
                raise TypeError(
                    '{} not acceptable for this model'.format(geom.geom_type)
                )

            rip, created = Area.objects.update_or_create(
                classification='ELECT_CIRC_REG',
                name=r['new_name'],
                defaults={
                    'identifier': 'ELECT_CIRC_REG_' + r['new_name'],
                    'slug': slugify('ELECT_CIRC_REG_'.lower() + r['new_name']),
                    'geometry': geom,
                    'parent_id': r['parent__parent__id']
                }

            )
            self.logger.info(f'{i}- {rip.name} created {created} on Area')
            const = Area.objects.get(name=r['new_name'], classification='ELECT_CIRC_REG')
            for i in r['list_ids']:
                sub_area = Area.objects.get(id=i)
                AreaMapConstituency.objects.get_or_create(constituency=const,
                                                          sub_areas=sub_area,
                                                          defaults={'typology': sub_area.classification}
                                                          )
                self.logger.info(f'connected {sub_area.name} to {const.name} created {created}')
            print(created)
            print(rip)

        # Connections for RGNE
        connect_to_italia = Area.objects.filter(name__in=[
            'Centro',
            'Sud',
            'Isole',
            'Nord-ovest',
            'Nord-est'])
        italia = Area.objects.get(name='Italia')
        for i in connect_to_italia:
            i.parent = italia
            self.logger.info(f'connected {i.name} to {italia.name}')
            i.save()

        self.logger.info(f"END: Area count: {Area.objects.all().count()}")
