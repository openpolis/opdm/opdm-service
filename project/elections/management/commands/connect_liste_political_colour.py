from django.db.models import F, Value, Q
from django.db.models.functions import Coalesce
from taskmanager.management.base import LoggingBaseCommand
from tqdm import tqdm

from project.elections.models import ElectoralListResult, PoliticalColour, ElectoralCandidateResult

missing_parties = []


def get_political_colours(parties_color):
    parties_color = list(
        map(lambda x: x.replace('Centrodestra', 'Centro destra').replace('Centrosinistra', 'Centro sinistra').replace(
            'M5S',
            'Movimento 5 stelle'),
            parties_color))
    if parties_color in [['Liberali'],
                         ['Altro', 'Liberali']]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Liberali",
            defaults={
                'colour': "#DB7093"
            }
        )

    elif parties_color in [['Centro sinistra'],
                           ['Altro', 'Centro', 'Centro sinistra', 'Sinistra', 'Verdi'],
                           ['Altro', 'Centro sinistra', 'Liberali'],
                           ['Altro', 'Centro sinistra', 'Liberali', 'Verdi'],
                           ['Altro', 'Centro', 'Centro sinistra', 'Sinistra'],
                           ['Centro sinistra', 'Liberali'],
                           ['Centro sinistra', 'Sinistra'],
                           ['Centro', 'Centro sinistra', 'Liberali'],
                           ['Centro sinistra', 'Verdi'],
                           ['Centro sinistra', 'Liberali', 'Verdi'],
                           ['Centro', 'Centro sinistra'],
                           ['Altro', 'Centro sinistra', 'Sinistra'],
                           ['Altro', 'Centro sinistra'],
                           ['Centro sinistra', 'Liberali', 'Sinistra'],
                           ['Altro', 'Centro sinistra', 'Sinistra', 'Verdi'],
                           ['Centro', 'Centro sinistra', 'Verdi'],
                           ['Centro sinistra', 'Sinistra', 'Verdi'],
                           ['Centro', 'Centro sinistra', 'Sinistra'],
                           ['Altro', 'Centro', 'Centro sinistra'],
                           ['Altro', 'Centro sinistra', 'Verdi'],
                           ['Altro', 'Centro sinistra', 'Liberali', 'Sinistra'],
                           ['Centro sinistra', 'Liberali', 'Sinistra', 'Verdi'],
                           ['Altro', 'Liberali', 'Verdi'],
                           ['Liberali', 'Sinistra'],
                           ['Altro', 'Centro sinistra', 'Liberali', 'Sinistra', 'Verdi'],
                           ['Altro', 'Centro', 'Centro sinistra', 'Liberali', 'Sinistra'],
                           ['Centro', 'Centro sinistra', 'Sinistra', 'Verdi'],
                           ['Liberali', 'Verdi'],
                           ['Altro', 'Centro', 'Sinistra'],
                           ['Centro', 'Sinistra', 'Verdi'],
                           ['Altro', 'Centro', 'Centro sinistra', 'Liberali'],
                           ['Centro', 'Centro sinistra', 'Liberali', 'Sinistra'],
                           ['Centro', 'Sinistra'],
                           ['Altro', 'Centro destra', 'Destra', 'Lega', 'Sinistra'],
                           ['Centro', 'Centro destra', 'Destra', 'Sinistra'],
                           ['Altro', 'Centro', 'Sinistra', 'Verdi'],
                           ['Centro', 'Liberali', 'Verdi'],
                           ['Centro', 'Verdi'],
                           ['Altro', 'Centro', 'Liberali', 'Verdi'],
                           ['Altro', 'Centro', 'Centro sinistra', 'Liberali', 'Verdi'],
                           ['Altro', 'Centro', 'Centro sinistra', 'Liberali', 'Sinistra', 'Verdi'],
                           ['Altro', 'Centro', 'Centro sinistra', 'Verdi'],
                           ['Centro', 'Centro sinistra', 'Liberali', 'Sinistra', 'Verdi'],
                           ['Centro', 'Centro sinistra', 'Liberali', 'Verdi'],
                           ['Altro', 'Centro', 'Liberali', 'Sinistra']
                           ]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Centrosinistra",
            defaults={
                'colour': "#800000"
            }
        )

    elif parties_color in [['Centro'],
                           ['Centro', 'Liberali'],
                           ['Altro', 'Centro'],
                           ['Altro', 'Centro', 'Liberali']
                           ]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Centro",
            defaults={
                'colour': "#20B2AA"
            }
        )

    elif parties_color in [['Sinistra'],
                           ['Altro', 'Sinistra'],
                           ['Sinistra', 'Verdi'],
                           ['Altro', 'Sinistra', 'Verdi']]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Sinistra",
            defaults={
                'colour': "#B22222"
            }
        )

    elif parties_color in [['Destra'],
                           ['Destra', 'Lega'],
                           ['Altro', 'Destra']]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Destra",
            defaults={
                'colour': "#00008B"
            }
        )

    elif parties_color in [
        ['Altro', 'Centro', 'Centro destra'],
        ['Altro', 'Centro', 'Centro destra', 'Liberali'],
        ['Centro destra'],
        ['Centro', 'Centro destra'],
        ['Centro destra'],
        ['Centro destra', 'Destra', 'Lega'],
        ['Centro destra', 'Liberali'],
        ['Centro destra', 'Lega'],
        ['Centro', 'Lega'],
        ['Centro', 'Destra'],
        ['Centro destra', 'Destra'],
        ['Centro', 'Centro destra', 'Destra', 'Lega'],
        ['Centro', 'Destra', 'Liberali'],
        ['Destra', 'Liberali'],
        ['Altro', 'Centro destra'],
        ['Centro destra', 'Destra', 'Liberali'],
        ['Destra', 'Lega', 'Liberali'],
        ['Altro', 'Centro destra', 'Lega'],
        ['Centro', 'Centro destra', 'Lega'],
        ['Altro', 'Centro destra', 'Destra'],
        ['Centro', 'Centro destra', 'Destra'],
        ['Centro', 'Destra', 'Lega'],
        ['Centro', 'Centro destra', 'Destra', 'Liberali'],
        ['Altro', 'Centro destra', 'Liberali'],
        ['Centro', 'Centro destra', 'Liberali'],
        ['Centro destra', 'Destra', 'Lega', 'Liberali'],
        ['Altro', 'Centro destra', 'Destra', 'Liberali'],
        ['Centro destra', 'Lega', 'Liberali'],
        ['Altro', 'Centro destra', 'Destra', 'Lega'],
        ['Lega', 'Liberali'],
        ['Centro', 'Destra', 'Lega', 'Liberali'],
        ['Altro', 'Centro', 'Destra'],
        ['Altro', 'Destra', 'Lega'],
        ['Altro', 'Centro', 'Centro destra', 'Destra', 'Lega'],
        ['Altro', 'Centro destra', 'Destra', 'Lega', 'Liberali'],
        ['Altro', 'Centro', 'Centro destra', 'Destra'],
        ['Centro', 'Centro destra', 'Liberali', 'Verdi'],
        ['Centro', 'Centro destra', 'Destra', 'Lega', 'Liberali'],
        ['Altro', 'Centro', 'Centro destra', 'Lega'],
        ['Altro', 'Centro', 'Centro destra', 'Destra', 'Lega', 'Liberali'],
        ['Altro', 'Centro', 'Centro destra', 'Centro sinistra', 'Sinistra', 'Verdi'],
        ['Centro', 'Centro destra', 'Lega', 'Liberali'],
        ['Centro', 'Centro destra', 'Lega', 'Liberali']

    ]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Centrodestra",
            defaults={
                'colour': "#4169E1"
            }
        )

    elif parties_color in [
        ['Lega'],
        ['Altro', 'Lega']
    ]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Lega",
            defaults={
                'colour': "#228B22"
            }
        )

    elif parties_color in [
        ['Altro'],
        ['Centro destra', 'Centro sinistra'],
        ['Destra', 'Sinistra'],
        ['Altro', 'Centro destra', 'Centro sinistra'],
        ['Centro destra', 'Centro sinistra', 'Destra'],
        ['Centro', 'Centro destra', 'Centro sinistra'],
        ['Altro', 'Centro', 'Centro destra', 'Centro sinistra', 'Sinistra'],
        ['Altro', 'Centro destra', 'Centro sinistra', 'Sinistra'],
        ['Centro', 'Centro destra', 'Centro sinistra', 'Liberali'],
        ['Centro destra', 'Centro sinistra', 'Sinistra'],
        ['Centro destra', 'Centro sinistra', 'Destra', 'Lega'],
        ['Centro destra', 'Centro sinistra', 'Liberali'],
        ['Centro', 'Centro destra', 'Centro sinistra', 'Destra'],
        ['Centro destra', 'Centro sinistra', 'Liberali', 'Sinistra'],
        ['Altro', 'Centro destra', 'Centro sinistra', 'Verdi'],
        ['Altro', 'Centro destra', 'Centro sinistra', 'Liberali', 'Sinistra'],
        ['Altro', 'Centro', 'Centro destra', 'Centro sinistra', 'Liberali'],
        ['Centro', 'Centro destra', 'Verdi'],
        ['Altro', 'Centro destra', 'Centro sinistra', 'Liberali'],
        ['Altro', 'Centro destra', 'Centro sinistra', 'Sinistra', 'Verdi'],
        ['Altro', 'Centro', 'Centro destra', 'Centro sinistra', 'Liberali', 'Sinistra', 'Verdi'],
        ['Centro destra', 'Verdi'],
        ['Centro destra', 'Centro sinistra', 'Liberali', 'Sinistra', 'Verdi'],
        ['Altro', 'Centro', 'Centro destra', 'Centro sinistra', 'Liberali', 'Sinistra'],
        ['Centro destra', 'Liberali', 'Sinistra'],
        ['Centro destra', 'Centro sinistra', 'Verdi'],
        ['Altro', 'Centro', 'Centro destra', 'Centro sinistra'],
        ['Centro destra', 'Sinistra'],
        ['Centro destra', 'Destra', 'Verdi'],
        ['Altro', 'Centro', 'Centro destra', 'Centro sinistra', 'Liberali', 'Verdi'],
        ['Centro', 'Centro destra', 'Centro sinistra', 'Sinistra'],
        ['Altro', 'Centro destra', 'Centro sinistra', 'Liberali', 'Verdi'],
        ['Altro', 'Centro destra', 'Centro sinistra', 'Liberali', 'Sinistra', 'Verdi'],
        ['Centro destra', 'Centro sinistra', 'Liberali', 'Verdi'],
        ['Centro destra', 'Destra', 'Sinistra'],
        ['Centro destra', 'Sinistra', 'Verdi'],
        ['Centro destra', 'Destra', 'Lega', 'Sinistra'],
        ['Centro', 'Centro destra', 'Centro sinistra', 'Verdi'],
        ['Centro', 'Centro destra', 'Sinistra'],
        ['Centro', 'Centro destra', 'Destra', 'Lega', 'Sinistra'],
        ['Centro', 'Destra', 'Verdi'],
        ['Centro', 'Centro destra', 'Centro sinistra', 'Verdi'],
        ['Centro', 'Centro destra', 'Sinistra'],
        ['Centro', 'Centro destra', 'Destra', 'Lega', 'Sinistra'],
        ['Centro', 'Destra', 'Verdi']
    ]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Altro",
            defaults={
                'colour': "#808080"
            }
        )

    elif parties_color in [
        ['Movimento 5 stelle'],
        ['M5S'],
        ['Altro', 'Movimento 5 stelle'],
        ['Centro', 'Movimento 5 stelle']
    ]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="M5S",
            defaults={
                'colour': "#C49102"
            }
        )

    elif parties_color in [
        ['Verdi'],
        ['Altro', 'Verdi'],
        ['Altro', 'Centro', 'Verdi']]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Verdi",
            defaults={
                'colour': "#006400"
            }
        )

    elif parties_color in [
        ['Movimento 5 stelle', 'Sinistra'],
        ['Centro sinistra', 'Movimento 5 stelle', 'Sinistra'],
        ['Centro sinistra', 'Movimento 5 stelle', 'Sinistra', 'Verdi'],
        ['Centro sinistra', 'Movimento 5 stelle'],
        ['Centro sinistra', 'Liberali', 'Movimento 5 stelle'],
        ['Giallorossa'],
        ['Centro', 'Centro sinistra', 'Movimento 5 stelle'],
        ['Centro sinistra', 'Movimento 5 stelle', 'Verdi'],
        ['Centro sinistra', 'Gialloverdi'],
        ['Altro', 'Centro sinistra', 'Movimento 5 stelle'],
        ['Altro', 'Centro sinistra', 'Movimento 5 stelle', 'Verdi'],
        ['Altro', 'Centro sinistra', 'Movimento 5 stelle', 'Sinistra'],
        ['Centro', 'Centro sinistra', 'Movimento 5 stelle', 'Sinistra'],
        ['Centro sinistra', 'Liberali', 'Movimento 5 stelle', 'Sinistra'],
        ['Movimento 5 stelle', 'Verdi'],
        ['Centro sinistra', 'Giallorossa'],
        ['Altro', 'Centro', 'Centro sinistra', 'Movimento 5 stelle'],
        ['Altro', 'Centro', 'Centro sinistra', 'Movimento 5 stelle', 'Sinistra']
    ]:
        pc, created = PoliticalColour.objects.get_or_create(
            name="Giallorossa",
            defaults={
                'colour': "#FF8C00"
            }
        )

    else:
        if parties_color not in missing_parties:
            missing_parties.append(parties_color)
            print(parties_color)
        return None
    return pc


class Command(LoggingBaseCommand):
    """Load political colour data into table."""

    help = "Load political colour data into table."

    def handle(self, *args, **options):
        super().handle(__name__, *args, formatter_key="simple", **options)
        self.logger.info("Starting procedure for tagging liste to colour")
        els = ElectoralListResult.objects.filter(parties__isnull=False)
        missing_ass = []
        for el in tqdm(els):
            parties = el.parties.filter(classifications__classification__scheme='OPDM_PARTY_COLOUR').annotate(
                sd=Coalesce(F('classifications__classification__start_date'), Value('1900-01-01')),
                ed=Coalesce(F('classifications__classification__end_date'), Value('9999-12-31'))
            ).filter(sd__lte=el.result.electoral_event.start_date, ed__gte=el.result.electoral_event.start_date)
            parties_color = list(parties.values_list('classifications__classification__descr', flat=True))
            parties_color = sorted(list(set(parties_color)))
            # print(parties_color)
            pc = get_political_colours(parties_color)
            if pc:
                el.political_colour = pc
                el.save()
                ElectoralListResult.objects.filter(result__electoral_event=el.result.electoral_event,
                                                   result__institution=el.result.institution, name=el.name).update(
                    political_colour=pc
                )
            else:
                if parties_color not in missing_ass:
                    missing_ass.append(parties_color)
                    print(parties_color)
                # print('a')
        sinistra = PoliticalColour.objects.get(name='Sinistra')
        destra = PoliticalColour.objects.get(name='Sinistra')
        centrosinistra = PoliticalColour.objects.get(name='Centrosinistra')
        centrodestra = PoliticalColour.objects.get(name='Centrodestra')
        ElectoralListResult.objects.filter(political_colour__isnull=True).filter(
            Q(name__icontains='sinistra') & Q(name__icontains='centro')).update(
            political_colour=centrosinistra
        )
        ElectoralListResult.objects.filter(political_colour__isnull=True).filter(
            Q(name__icontains='destra') & Q(name__icontains='centro')).update(
            political_colour=centrodestra
        )
        ElectoralListResult.objects.filter(political_colour__isnull=True).filter(name__icontains='destra').exclude(
            name__icontains='centro').update(
            political_colour=destra
        )
        ElectoralListResult.objects.filter(political_colour__isnull=True).filter(name__icontains='sinistra').exclude(
            name__icontains='centro').update(
            political_colour=sinistra
        )
        eds = ElectoralCandidateResult.objects.filter(list_results__political_colour__isnull=False)
        eds = ElectoralCandidateResult.objects.filter(id__in=eds.values('id').distinct()).distinct()
        eds = eds.values_list('result__electoral_event', 'result__institution', 'name').distinct()
        # ElectoralCandidateResult.objects.update(political_colour=None)
        for el_id, inst, name in tqdm(eds):
            ed = ElectoralCandidateResult.objects.filter(result__electoral_event_id=el_id, result__institution_id=inst,
                                                         name=name).first()
            parties_color = ed.list_results.filter(political_colour__isnull=False).values_list(
                'political_colour__name', flat=True).order_by('political_colour__name').distinct()
            pc = get_political_colours(list(parties_color))
            ed.political_colour = pc
            ed.save()
            ElectoralCandidateResult.objects.filter(result__electoral_event=ed.result.electoral_event,
                                                    result__institution=ed.result.institution, name=ed.name).update(
                political_colour=pc
            )
