from popolo.models import KeyEvent
from taskmanager.management.base import LoggingBaseCommand

from project.elections.models import ElectoralResult


class Command(LoggingBaseCommand):
    """Da eligendo non ci arriva il dettaglio per tutti i livelli territoriali di chi è diventato presidente.
    Lo script spalma l'is_top nella gerarchia per le elezioni regionale
    """

    help = "Associazione area_id per mmemberships di tipo Deputato e Senatore"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--date',
            dest='date',
            default="2018-03-04",
            help='Data elezione politiche.'
        )

    def handle(self, *args, **options):
        """
        Execute the task.
        """
        super().handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Starting procedure")
        self.date = options['date']

        electoral_event = KeyEvent.objects.get(
            start_date=self.date,
            event_type='ELE-REG',
        )

        self.logger.info(f"Analyzing {electoral_event.name}")
        ees = ElectoralResult.objects.filter(electoral_event=electoral_event)

        def update_is_top_candidate(ers, candidate_name):
            for er in ers:
                cand_results = er.candidate_results.all()
                cand_results.filter(name=candidate_name).update(is_top=True)
                self.logger.info(f"Update {er.constituency.name}")
                if er.children.all():
                    update_is_top_candidate(er.children.all(), candidate_name)
        for ee in ees:
            candidate = ee.candidate_results.get(is_top=True)
            candidate_name = candidate.name
            update_is_top_candidate(ee.children.all(), candidate_name)

        return "Done!"
