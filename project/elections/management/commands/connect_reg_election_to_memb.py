import pandas as pd
from popolo.models import KeyEvent, Membership as OPDMMembership
from taskmanager.management.base import LoggingBaseCommand

from django.db.models import Q
from rest_framework import filters
from project.elections.models import ElectoralResult, ElectoralCandidateResult


class Command(LoggingBaseCommand):
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--date',
            dest='date',
            default="2018-03-04",
            help='Data elezione politiche.'
        )

    def handle(self, *args, **options):
        """
        Execute the task.
        """
        super().handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Starting procedure")
        self.date = options['date']

        electoral_event = KeyEvent.objects.filter(
            event_type__in=['ELE-REG', 'ELE-PROV'],
        ).filter(start_date__gte='2010-01-01').order_by('-start_date')
        rows_list = []  # pd.DataFrame(columns=["name", "is_mayor", "is_counselor", "comune"])

        for event in electoral_event:
            self.logger.info(f"Analyzing {event.name}")
            memberships = OPDMMembership.objects.filter(electoral_event=event)
            ele_reg_prov = ElectoralResult.objects.filter(electoral_event=event,
                                                          constituency__classification__in=['ADM1', 'ADM2'])

            for ee in ele_reg_prov:
                # try:
                candidates = ee.candidate_results.all()
                submemberships = memberships.filter(organization__parent=ee.institution)
                for candidate in candidates:
                    try:

                        candidate_name = candidate.name
                        candidate_name = candidate_name.replace('ROSSI E.', 'ROSSI ENRICO')
                        candidate_name = candidate_name.replace('SPACCA', 'SPACCA GIAN MARIO')
                        name_parts = candidate_name.split(' ')
                        control = True
                        while name_parts and control:
                            if len(name_parts) == 1:
                                raise Exception
                            try:

                                queries_1 = [
                                    Q(**{"person__name__icontains": x})
                                    for x in name_parts
                                ]
                                queries_2 = [
                                    Q(**{"person__other_names__name__icontains": x})
                                    for x in name_parts
                                ]
                                condition_1 = filters.reduce(filters.operator.and_, queries_1)
                                condition_2 = filters.reduce(filters.operator.and_, queries_2)
                                all_conditions = filters.reduce(filters.operator.or_, [condition_1, condition_2])
                                if candidate.is_top:
                                    memb = submemberships.filter(Q(role__istartswith='Presidente di Regione') |
                                                                 Q(role__istartswith='Presidente di Provincia')
                                                                 ).filter(all_conditions).values('id').distinct()
                                else:
                                    memb = submemberships.exclude(Q(role__istartswith='Presidente di Regione') &
                                                                  Q(role__istartswith='Presidente di Provincia')).filter(
                                        all_conditions).values(
                                        'id').distinct()
                                memb = submemberships.get(id__in=memb)
                                # memberships = memberships.exclude(id=memb.id)
                                print(f"{candidate.name} -> {memb.person.name}")
                                control = False
                                candidate.membership = memb
                                candidate.save()
                                ElectoralCandidateResult.objects.filter(result__electoral_event=ee.electoral_event,
                                                                        result__institution=ee.institution,
                                                                        name=candidate.name).update(membership=memb)
                            except Exception:
                                name_parts = name_parts[:-1]
                    except:
                        try:
                            if candidate.is_top:
                                try:
                                    memb = submemberships.filter(role='Sindaco').order_by('start_date').first()
                                    if not memb:
                                        raise Exception
                                    candidate.membership = memb
                                    candidate.save()
                                except:
                                    dict_1 = {
                                        "electoral_event": event.name,
                                        "name": candidate.name,
                                        "is_top": candidate.is_top,
                                        "is_counselor": candidate.is_counselor,
                                        "comune": candidate.result.institution.name,
                                        "comune_id": candidate.result.institution.id}
                                    rows_list.append(dict_1)
                                    candidate.membership = None
                                    candidate.save()
                                    ElectoralCandidateResult.objects.filter(result__electoral_event=ee.electoral_event,
                                                                            result__institution=ee.institution,
                                                                            name=candidate.name).update(membership=None)
                                    print(
                                        f"Error for {candidate.name}; {candidate.is_top}; {candidate.is_counselor}; {candidate.result.institution}")
                            elif candidate.is_counselor:
                                dict_1 = {
                                    "electoral_event": event.name,
                                    "name": candidate.name,
                                    "is_top": candidate.is_top,
                                    "is_counselor": candidate.is_counselor,
                                    "comune": candidate.result.institution.name,
                                    "comune_id": candidate.result.institution.id}
                                rows_list.append(dict_1)
                                candidate.membership = None
                                candidate.save()
                                ElectoralCandidateResult.objects.filter(result__electoral_event=ee.electoral_event,
                                                                        result__institution=ee.institution,
                                                                        name=candidate.name).update(membership=None)
                                print(
                                    f"Error for {candidate.name}; {candidate.is_top}; {candidate.is_counselor}; {candidate.result.institution}")
                        except UnicodeEncodeError:
                            print(
                                f"UnicodeEncodeError Error for ;{candidate.id}; {candidate.is_top}; {candidate.result.institution}")
        df_errors = pd.DataFrame(rows_list)
        df_errors.to_csv("election_errors.csv", index=False)
        return "Done!"
