"""Merging list elections data with popolo_memberships."""

from project.elections.models import ElectoralResult, ElectoralListResult, ElectoralCandidateResult,\
    ElectoralSubCandidateResult
from popolo.models import Membership, Organization, Area, KeyEvent
from taskmanager.management.base import LoggingBaseCommand

from django.db import models as dmodels
from django.db.models import Field
from django.db.models import Lookup, Q
from django.db.models.functions import Upper

from thefuzz import fuzz
from thefuzz import process
from tqdm import tqdm

import boto3
import copy
import io
import pandas as pd
import re
import unidecode


class PersonName(Lookup):
    lookup_name = 'person_name'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return "LEFT(REGEXP_REPLACE(REPLACE(REGEXP_REPLACE(upper(%s), " \
               "'(( DETTO | NOTO | DETTA | NOTA ).*)', ''),'''',''),'(. )', ' ','g'),-1) " \
               "= LEFT(REGEXP_REPLACE(REPLACE(REGEXP_REPLACE(upper(%s), " \
               "'(( DETTO | NOTO | DETTA | NOTA ).*)', ''),'''',''),'(. )', ' ','g'),-1)" \
               % (lhs, rhs), params


Field.register_lookup(PersonName)


class ConstituencyLU(Lookup):
    lookup_name = 'constituency_lookup'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        # print("REPLACE('-',' ',(%s)) = REPLACE('-',' ',(%s))" % (lhs, rhs), params)
        return "REPLACE(upper(%s),'-',' ') =" \
               "REPLACE(upper(%s),'-',' ')"\
               % (lhs, rhs), params


Field.register_lookup(ConstituencyLU)


class ConstituencyLUPOL(Lookup):
    lookup_name = 'constituency_lookup_politiche'

    def as_sql(self, compiler, connection):
        lhs, lhs_params = self.process_lhs(compiler, connection)
        rhs, rhs_params = self.process_rhs(compiler, connection)
        params = lhs_params + rhs_params
        return r"REPLACE(REGEXP_REPLACE(upper(%s),'(\d+_)',''),'-',' ') =" \
               r"REPLACE(REGEXP_REPLACE(upper(%s),'(\d+_)',''),'-',' ')"\
               % (lhs, rhs), params


Field.register_lookup(ConstituencyLUPOL)


def map_area_european(constityency_descr):
    mapping = {
        'ITALIA NORD-ORIENTALE': 'NORD-EST',
        'ITALIA NORD-OCCIDENTALE': 'NORD-OVEST',
        'ITALIA CENTRALE': 'CENTRO',
        'ITALIA MERIDIONALE': 'SUD',
        'ITALIA INSULARE': 'ISOLE'
    }
    return mapping[constityency_descr]


def is_null_list_acceptable(x):
    return None if any(ext in x for ext in ['Assessore',
                                            'Vicesindaco',
                                            'Vicepresidente di Regione',
                                            'Sottosegretario',
                                            'Presidente vicario di Regione']) is False else True


def clean_name(x):
    x = x.upper()
    regex = r"(( DETTO | NOTO | DETTA | NOTA ).*)"
    x = re.sub(regex, '', x, 0)
    x = x.replace('\'', '')
    regex = r"(. )"
    x = re.sub(regex, ' ', x, 0)
    x = x[:-1]
    return x


def remove_last_letter_string(x):
    new_list = list(map(lambda f: f[:-1] if len(f) > 1 else f, x.split()))
    return ' '.join(new_list)


def cleaning_liste(x):
    if x is not None:
        x = x.upper()
        x = x.replace('Lista civica CEN-DESTRA (CEN-DES (LS CIVICA))'.upper(), '')
        x = x.replace('RIF.COM-COM.IT', 'RIFONDAZIONE COMUNITA-COMUNISTI ITALINI')
        x = x.replace('LIBERAL DEMOCRATICI', 'LIBERALDEMOCRATICI')
        x = x.replace('LISTA CIVICA - CEN-SIN ()', '')
        x = x.replace('LISTA CIVICA - CEN-DES ()', '')
        x = x.replace('LISTA CIVICA ', '')
        x = x.replace('LISTA CIVICA: ', '')
        x = x.replace('SINISTRA |', '')
        x = x.replace('DESTRA |', '')
        x = x.replace('LISTA COMPOSITA', '')
        x = x.replace('L.NORD', 'LEGA NORD ')
        x = x.replace('-', ' ')
        x = x.replace('CEN DES(LS.CIVICHE)', ' ')
        x = x.replace('CEN SIN(LS.CIVICHE)', ' ')
        x = x.replace('CEN DES(CONTR.UFF.)', ' ')
        x = x.replace('CEN SIN(CONTR.UFF.)', ' ')
        x = x.replace('S�AMO', 'SIAMO')
        x = x.replace('�', 'E')
        x = unidecode.unidecode(x)
        x = x.replace('|', ' ')
        x = x.replace(':', ' ')
        x = x.replace('(', ' ')
        x = x.replace(')', ' ')
        x = x.replace('+', ' ')
        x = x.replace('LISTA CIVICA', ' ')
        x = x.strip()
        x = x.replace('\'', ' ')
        x = remove_last_letter_string(x)
        x = re.sub('[ ]+', ' ', x).strip()
        if x in ['NO PERVENUT',
                 'COLOR POLITIC NO PERVENUT',
                 'NO SPECIFICAT',
                 '',
                 'CE SI',
                 'CE DE',
                 'NONCH MOTIV D ODIN PUBBLICO']:
            x = None
        return x
    else:
        return None


def upload_file(dataframe, bucket, key):
    """dat=DataFrame, bucket=bucket name in AWS S3, key=key name in AWS S3"""
    client = boto3.client("s3")
    csv_buffer = io.BytesIO()
    w = io.TextIOWrapper(csv_buffer)
    dataframe.to_csv(w)
    w.seek(0)
    client.upload_fileobj(csv_buffer, bucket, key)


class ExtractElected:
    def __init__(self, electoral_event_id, institution_id, constituency_id):
        self.election = KeyEvent.objects.get(id=electoral_event_id)
        self.institution = Organization.objects.get(id=institution_id)
        self.constituency = Area.objects.get(id=constituency_id)
        self.er = ElectoralResult.objects.get(electoral_event_id=electoral_event_id,
                                              institution_id=institution_id,
                                              constituency_id=constituency_id)
        self.event_type = self.election.event_type

    def elected(self):
        if self.event_type in ['ELE-COM', 'ELE-REG']:
            return Membership.objects.filter(electoral_event_id=self.election.id,
                                             organization__parent_id=self.institution,
                                             organization__parent__area_id=self.constituency)
        elif self.event_type in ['ELE-POL']:
            filters = dmodels.Q(electoral_event_id=self.election) & dmodels.Q(organization=self.institution)
            if self.institution.parent.name == 'CAMERA DEI DEPUTATI':
                if self.election.name == "Elezioni politiche 24/02/2013":
                    constituency_name = self.constituency.name.partition('/')[0]
                else:
                    constituency_name = self.constituency.parent.name
                filters &= dmodels.Q(constituency_descr_tmp__constituency_lookup=constituency_name)
            elif self.institution.parent.name == 'SENATO DELLA REPUBBLICA':
                filters &= dmodels.Q(constituency_descr_tmp__constituency_lookup=self.constituency.parent.parent.name
                                     .replace('/Südtirol', ''))
            else:
                Exception('Error')
            return Membership.objects.filter(filters)


class Command(LoggingBaseCommand):
    """Command to merge electoral results lists and candidate with popolo_membership references."""

    help = "Merge electoral results lists and candidate from Eligendo to popolo_membership references."

    def add_arguments(self, parser):
        parser.add_argument(
            "--l",
            type=str,
            dest='level',
            choices=["COM", "REG", "POL", "EU"],
            help="COM for Comuni level; REG for Regioni level; POL per general elections and EU per european elections",
        )
        parser.add_argument(
            "--truncate",
            dest='all',
            action='store_true',
            default=False,
            help="If True, it truncates all connections",
        )

    def handle(self, *args, **kwargs):
        try:
            super(Command, self).handle(__name__, *args, formatter_key="simple", **kwargs)
            variable_maps = {'COM': {'event_type': 'ELE-COM',
                                     'role': 'Sindaco'},
                             'REG': {'event_type': 'ELE-REG',
                                     'role': 'Presidente di Regione'},
                             'POL': {'event_type': 'ELE-POL',
                                     'role': ['DEPUTATO', 'SENATORE']},
                             'EU':  {'event_type': 'ELE-EU',
                                     'role': ['PARLAMENTARE EUROPEO']}
                             }
            typology = kwargs["level"]
            self.logger.info(f"Starting procedure for {typology}")
            if kwargs["all"]:
                ElectoralCandidateResult\
                    .elected_membership\
                    .through\
                    .objects\
                    .filter(
                      electoralcandidateresult__result__electoral_event__event_type=variable_maps
                      [typology]['event_type'])\
                    .delete()
                ElectoralSubCandidateResult\
                    .elected_membership\
                    .through\
                    .objects\
                    .filter(
                         electoralsubcandidateresult__list_result__result__electoral_event__event_type=variable_maps
                         [typology]['event_type'])\
                    .delete()
                ElectoralListResult\
                    .elected_memberships\
                    .through\
                    .objects\
                    .filter(
                            electorallistresult__result__electoral_event__event_type=variable_maps
                            [typology]['event_type'])\
                    .delete()
                self.logger.info(f"Removed all connections to popolo_memberships for {typology}")
            if typology in ['COM', 'REG']:
                ''' Estrazione dei candidati eletti sindaco da fonte eligendo'''
                candidate_elett = ElectoralCandidateResult \
                    .objects \
                    .filter(
                            result__electoral_event__event_type=variable_maps[typology]['event_type'],
                            result__is_valid=True,
                            is_top=True)

                if typology == 'REG':
                    '''
                    Per le elezioni regionali prendiamo solamente le informazione sulla sezione regionale,
                    in quanto esistono più sezione: circoscrizionale, provinciale e comunale.
                    La connessione della membership, quindi, avverrà solamente ad un unico livello e a front-end verrà
                    calcolata come derivazione main connection.
                    '''
                    candidate_elett = candidate_elett.filter(result__constituency__classification='ADM1')

                list_logging = []
                self.logger.info(f"Starting procedure for {typology} winners")

                for index, candidate in enumerate(candidate_elett):
                    if candidate.elected_membership.filter(role='Sindaco').count() >= 1:
                        continue
                    if (index+1) % 100 == 0:
                        self.logger.info(f"{index}/{len(candidate_elett)}")

                    variables = (candidate.result.electoral_event_id,
                                 candidate.result.institution_id,
                                 candidate.result.constituency_id)

                    extraction = ExtractElected(*variables)
                    elected = extraction.elected().filter(role=variable_maps[typology]['role'])

                    logging = {'electoral_event_id': extraction.election.id,
                               'election_name': extraction.election.name,
                               'institution_id': extraction.institution.id,
                               'institution_name': extraction.institution.name,
                               'area_id': extraction.constituency.id,
                               'area': extraction.constituency.name,
                               'area_abitanti': extraction.constituency.inhabitants,
                               'provincia': extraction.constituency.parent.name if typology == 'COM' else None,
                               'regione': extraction.constituency.parent.parent.name if typology == 'COM' else None,
                               'candidate_id': candidate.id,
                               'nome_eletto': candidate.name.upper(),
                               'type': None,
                               'membership_id': None,
                               'membership_name': None
                               }
                    if len(elected) == 0:
                        ''' Non esistono candidati nel ruolo per l'elezione indicata'''
                        logging['type'] = 'not found'
                        list_logging.append(logging)
                    elif len(elected) > 1:
                        '''Il candidato esiste ed è più di uno'''
                        '''Ricerca tramite nome'''
                        multiple_candidates = elected.filter(person__sort_name__person_name=candidate.name)

                        if len(multiple_candidates) > 0:
                            if len(multiple_candidates.values('person__id').distinct()) == 1:
                                for single_candidate in multiple_candidates:
                                    logging_dett = copy.deepcopy(logging)
                                    logging_dett['type'] = 'found'
                                    logging_dett['membership_id'] = single_candidate.id
                                    logging_dett['membership_name'] = single_candidate.person.sort_name.upper()
                                    list_logging.append(logging_dett)

                                    memb_to_add = Membership.objects.get(id=single_candidate.id)
                                    ElectoralCandidateResult.objects.get(id=candidate.id).elected_membership\
                                        .add(memb_to_add)
                            elif len(multiple_candidates.values('person__id').distinct()) > 1:
                                logging['type'] = 'multiple person id for same name'

                                list_logging.append(logging)
                        else:
                            '''Non è stato trovato tramite il nome'''
                            logging['type'] = 'mayor exists but not found the name'
                            list_logging.append(logging)
                    elif len(elected) == 1:
                        '''Esiste il candidato per il ruolo ed è unico'''
                        memb_to_add = elected[0]
                        ElectoralCandidateResult.objects.get(id=candidate.id).elected_membership.add(memb_to_add)
                        logging['type'] = 'found'
                        logging['membership_id'] = elected[0].id
                        logging['membership_name'] = elected[0].person.sort_name.upper()

                        list_logging.append(logging)
                df_report = pd.DataFrame(list_logging)
                file_name = typology + '_winner_elected' + '.csv'
                upload_file(df_report, 'openpolis-drop', file_name)

                cand_sindac_elett = ElectoralCandidateResult \
                    .objects \
                    .filter(result__electoral_event__event_type=variable_maps[typology]['event_type'],
                            result__is_valid=True,
                            is_top=False,
                            is_counselor=True)
                if typology == 'REG':
                    cand_sindac_elett = cand_sindac_elett.filter(result__constituency__classification='ADM1')
                cand_sindac_elett = cand_sindac_elett\
                    .values('id',
                            'name',
                            'result__electoral_event_id',
                            'result__institution_id',
                            'result__constituency_id',
                            'is_counselor')

                count_not_found = 0
                count_dubbi = 0
                count_found = 0
                lista_joined_membership = []
                list_logging = []
                self.logger.info(f"Starting procedure for {typology} not winners")
                n = 0
                for cand_sindac in tqdm(cand_sindac_elett):
                    variables = cand_sindac['result__electoral_event_id'], \
                                cand_sindac['result__institution_id'], \
                                cand_sindac['result__constituency_id']
                    n += 1
                    if n % 100 == 0:
                        self.logger.info(f"{n}/{len(cand_sindac_elett)}")
                    extraction = ExtractElected(*variables)
                    all_elected = extraction.elected().exclude(role=variable_maps[typology]['role'])
                    elected = all_elected.filter(person__sort_name__person_name=cand_sindac['name'])
                    logging = {'electoral_event_id': extraction.election.id,
                               'election_name': extraction.election.name,
                               'institution_id': extraction.institution.id,
                               'institution_name': extraction.institution.name,
                               'area_id': extraction.constituency.id,
                               'area': extraction.constituency.name,
                               'area_abitanti': extraction.constituency.inhabitants,
                               'provincia': extraction.constituency.parent.name if typology == 'COM' else None,
                               'regione': extraction.constituency.parent.parent.name if typology == 'COM' else None,
                               'candidate_id': cand_sindac['id'],
                               'nome_eletto': cand_sindac['name'].upper(),
                               'type': None,
                               'membership_id': None,
                               'membership_name': None
                               }
                    if len(elected) > 1:
                        if len(elected.values('person__id').distinct()) > 1:
                            logging['type'] = 'dubbio'
                            logging['list_to_find'] = [{x['person__id']: x['person__name']}
                                                       for x in elected.values('person__id',
                                                                               'person__name').distinct()]
                            list_logging.append(logging)
                            count_dubbi += 1
                        elif len(elected.values('person__id').distinct()) == 1:
                            count_found += 1
                            lista_joined_membership += [x.id for x in elected]
                            for i in elected:
                                memb_to_add = Membership.objects.get(id=i.id)
                                ElectoralCandidateResult.objects.get(id=cand_sindac['id']).elected_membership\
                                    .add(memb_to_add)
                                logging_dett = copy.deepcopy(logging)
                                logging_dett['type'] = 'found'
                                logging_dett['membership_id'] = memb_to_add.id
                                logging_dett['membership_name'] = memb_to_add.person.sort_name.upper()
                                list_logging.append(logging_dett)
                    elif len(elected) == 0:
                        '''Non trovata esatta corrispondenza tra candidato
                        eligendo e possibili eletti in quell'elezione'''
                        elected = all_elected.values('id',
                                                     'person__sort_name')
                        if len(elected) > 0:
                            df_elected = pd.DataFrame(elected)
                            chosen = process.extract(clean_name(cand_sindac['name']), df_elected['person__sort_name']
                                                     .apply(lambda x: clean_name(x.upper())),
                                                     scorer=fuzz.token_set_ratio, limit=2)

                            if len(chosen) == 1:
                                if chosen[0][1] > 90:
                                    lista_joined_membership += [df_elected.loc[chosen[0][2]]['id']]
                                    memb_to_add = Membership.objects.get(id=df_elected.loc[chosen[0][2]]['id'])
                                    ElectoralCandidateResult.objects.get(id=cand_sindac['id']).\
                                        elected_membership.add(memb_to_add)
                                    logging['type'] = 'found'
                                    logging['membership_id'] = elected[0].id
                                    logging['membership_name'] = memb_to_add.person.sort_name.upper()

                                    count_found += 1
                                else:
                                    logging['type'] = 'no matching'
                                    logging['list_to_find'] = [{df_elected.loc[x[2]]['id']:
                                                                (df_elected.loc[x[2]]['person__sort_name'].upper(),
                                                                 x[1])} for x in chosen]
                                    list_logging.append(logging)
                                    count_not_found += 1

                            elif (chosen[0][1] > 90) and (chosen[0][1] - chosen[1][1]) > 19:
                                lista_joined_membership += [df_elected.loc[chosen[0][2]]['id']]
                                memb_to_add = Membership.objects.get(id=df_elected.loc[chosen[0][2]]['id'])
                                ElectoralCandidateResult.objects.get(id=cand_sindac['id']).elected_membership.\
                                    add(memb_to_add)
                                count_found += 1
                                logging['type'] = 'found'
                                logging['membership_id'] = memb_to_add.id
                                logging['membership_name'] = memb_to_add.person.sort_name.upper()
                                list_logging.append(logging)
                            else:
                                if chosen[0][1] > 90:
                                    type_ = 'dubbio'
                                else:
                                    type_ = 'no matching'
                                logging['type'] = type_
                                logging['list_to_find'] = [{df_elected.loc[x[2]]['id']:
                                                            (df_elected.loc[x[2]]['person__sort_name'].upper(), x[1])}
                                                           for x in chosen]
                                list_logging.append(logging)
                                count_not_found += 1
                        else:
                            logging['type'] = 'no records for popolo memberships'
                            list_logging.append(logging)
                            count_not_found += 1
                    elif len(elected) == 1:
                        memb_to_add = Membership.objects.get(id=elected[0].id)
                        ElectoralCandidateResult.objects.get(id=cand_sindac['id']).elected_membership.add(memb_to_add)

                        logging['type'] = 'found'
                        logging['membership_id'] = elected[0].id
                        logging['membership_name'] = memb_to_add.person.sort_name.upper()
                        list_logging.append(logging)

                        count_found += 1
                        lista_joined_membership += [x.id for x in elected]
                df_report = pd.DataFrame(list_logging)
                file_name = typology + '_candidate_not_winner_elected_' + 'counselor.csv'
                # df_report.to_csv(file_name)
                upload_file(df_report, 'openpolis-drop', file_name)
                # bucket.upload_file(file_name, ExtraArgs={'ACL': 'public-read'})

                # Consiglieri/assessori
                self.logger.info(f"Starting procedure for {typology} counselors and others")
                elections = ElectoralResult.objects.filter(is_valid=True,
                                                           electoral_event__event_type=variable_maps[typology]
                                                           ['event_type'])

                rows_list = []
                rows_list_not_found = []
                n = 0
                if typology not in ['REG']:
                    for er in tqdm(elections):
                        # n += 1
                        # if n % 100 == 0:
                        #     self.logger.info(f"{n}/{len(elections)}")
                        liste_not_joined = []
                        variables = er.electoral_event_id, er.institution_id, er.constituency_id
                        extraction = ExtractElected(*variables)

                        if (extraction.election.event_type == variable_maps[typology]['event_type']
                                and extraction.election.event_type != 'ELE-REG'):

                            logging = {'electoral_event_id': extraction.election.id,
                                       'election_name': extraction.election.name,
                                       'institution_id': extraction.institution.id,
                                       'institution_name': extraction.institution.name,
                                       'area_id': extraction.constituency.id,
                                       'area': extraction.constituency.name,
                                       'area_abitanti': extraction.constituency.inhabitants,
                                       'provincia': extraction.constituency.parent.name if typology == 'COM' else None,
                                       'regione': extraction.constituency.parent.parent.name if typology == 'COM'
                                       else None,
                                       'lista_omonima': []
                                       }
                            logging_not_found = copy.deepcopy(logging)
                            liste_elezioni = ElectoralListResult.objects \
                                .filter(result_id=er,
                                        ballot_candidate_result_id=None) \
                                .values('id',
                                        'name',
                                        'seats')
                            if len(liste_elezioni) == 0:
                                logging['type'] = 'no liste associate per questa elezione su eligendo'
                                rows_list.append(logging)
                                continue

                            df_list_eligendo = pd.DataFrame(liste_elezioni)
                            df_list_eligendo['new_name'] = df_list_eligendo['name'].apply(lambda x: cleaning_liste(x))

                            # Popolo membership

                            elected = extraction.elected() \
                                .exclude(label__contains='Commissario') \
                                .exclude(role=variable_maps[typology]['role']) \
                                .exclude(id__in=lista_joined_membership) \
                                .values('id',
                                        'electoral_list_descr_tmp',
                                        'role',
                                        'person__name',
                                        'end_reason')

                            if len(elected) == 0:
                                logging['type'] = 'non presenti record in popolo_membership'
                                rows_list.append(logging)
                                continue

                            df_elected = pd.DataFrame(elected)
                            df_elected = df_elected.rename(columns={'electoral_list_descr_tmp': 'list'})
                            df_elected['list_new'] = df_elected['list'].apply(lambda x: cleaning_liste(x))
                            df_elected['may_be_null'] = df_elected['role'].apply(lambda x: is_null_list_acceptable(x))

                            contatore_agganci = 0
                            contatore_null = 0
                            contatore_null_acceptable = 0
                            count_candidate = 0
                            count_lista_omonima = 0
                            count_list_not_joined = 0
                            grouped = df_elected.groupby(by=['list', 'list_new'], dropna=False)
                            single_list_new = grouped.count()
                            single_list_new['ids'] = grouped.agg({'id': lambda x: list(x)})
                            single_list_new = single_list_new.reset_index()

                            for index, row in single_list_new.iterrows():

                                if pd.isna(row['list_new']):
                                    contatore_null_acceptable += row['may_be_null']
                                    contatore_null += row['id']
                                    continue

                                res = df_list_eligendo[df_list_eligendo['new_name'] == row['list_new']]
                                if res.shape[0] > 1:
                                    lista_omonima = row['list']
                                    logging['lista_omonima'] += [lista_omonima]
                                    count_lista_omonima += row['id']
                                    continue
                                elif res.shape[0] == 1:
                                    contatore_agganci += row['id']
                                    for memb in row['ids']:
                                        memb_to_add = Membership.objects.get(id=memb)
                                        ElectoralListResult.objects.get(id=res['id'])\
                                            .elected_memberships.add(memb_to_add)
                                elif res.shape[0] == 0:
                                    if row['list'].replace(':', '|').count('|') > 1:
                                        if typology == 'REG':
                                            memb_to_add = Membership.objects.filter(id__in=row['ids'])
                                            single_persons = list(set([x.person.id for x in memb_to_add]))
                                            if len(single_persons) == 1:
                                                ecr = ElectoralCandidateResult.objects.get(
                                                    elected_membership__person__id=single_persons[0],
                                                    result__electoral_event__id=variables[0])
                                                for i in memb_to_add:
                                                    ecr.elected_membership.add(i)
                                                    contatore_agganci += 1
                                            else:
                                                logging['maybe_candidate'] = {row['list']: row['ids']}
                                                count_candidate += 1
                                        else:
                                            logging['maybe_candidate'] = {row['list']: row['ids']}
                                            count_candidate += 1
                                    else:
                                        chosen = process.extract(row['list_new'], df_list_eligendo['new_name'],
                                                                 scorer=fuzz.token_set_ratio,
                                                                 limit=2)
                                        if len(chosen) == 1:
                                            if chosen[0][1] > 50:
                                                contatore_agganci += row['id']
                                                for memb in row['ids']:
                                                    memb_to_add = Membership.objects.get(id=memb)
                                                    ElectoralListResult.objects.get(id=df_list_eligendo
                                                                                    .loc[chosen[0][2]]['id'])\
                                                        .elected_memberships.add(memb_to_add)
                                            else:
                                                count_list_not_joined += row['id']
                                                liste_not_joined += [{row['list']: row['id']}]
                                                logging_not_found['list_not_found'] = copy.deepcopy(row['list'])
                                                rows_list_not_found.append(copy.deepcopy(logging_not_found))

                                        elif ((chosen[0][1] > 70) and (chosen[0][1] - chosen[1][1]) > 20) or (
                                              (chosen[0][1] > 90) and (chosen[0][1] - chosen[1][1]) > 10):
                                            contatore_agganci += row['id']
                                            for memb in row['ids']:
                                                pass
                                                memb_to_add = Membership.objects.get(id=memb)
                                                ElectoralListResult.objects.get(id=df_list_eligendo
                                                                                .loc[chosen[0][2]]['id'])\
                                                    .elected_memberships.add(memb_to_add)

                                        else:
                                            count_list_not_joined += row['id']
                                            liste_not_joined += [{row['list']: row['id']}]
                                            list_row = copy.deepcopy(row['list'])
                                            logging_not_found['list_not_found'] = copy.deepcopy(list_row)
                                            rows_list_not_found.append(copy.deepcopy(logging_not_found))

                            logging['list_not_joined'] = '' if len(liste_not_joined) == 0 else liste_not_joined
                            logging['count_list_not_joined'] = count_list_not_joined
                            logging['num_elected'] = df_elected.shape[0]
                            logging['null_list_elected'] = contatore_null
                            logging['null_list_elected_acceptable'] = contatore_null_acceptable
                            logging['perfect_joins'] = contatore_agganci
                            logging['count_candidate'] = count_candidate
                            logging['count_lista_omonima'] = count_lista_omonima

                            rows_list.append(logging)

                df_report = pd.DataFrame(rows_list)
                file_name = typology+'_analisysis_'+'.csv'
                # df_report.to_csv(file_name)
                upload_file(df_report, 'openpolis-drop', file_name)
                df_report = pd.DataFrame(rows_list_not_found)
                file_name = typology+'_analisysis_not_found'+'.csv'
                # df_report.to_csv(file_name)
                upload_file(df_report, 'openpolis-drop', file_name)
                # bucket.upload_file(file_name, ExtraArgs={'ACL': 'public-read'})
            if typology in ['POL', 'EU']:
                self.logger.info(f"Starting procedure for {typology} uninomal")
                uninomal_elected = ElectoralCandidateResult \
                    .objects \
                    .filter(result__electoral_event__event_type=variable_maps[typology]['event_type'],
                            result__is_valid=True).filter(Q(is_top=True) | Q(is_counselor=True)) \
                    .values('id',
                            'name',
                            'result__electoral_event_id',
                            'result__institution_id',
                            'result__constituency_id',
                            'is_counselor')
                n = 0
                list_logging = []
                for el in uninomal_elected:
                    variables = el['result__electoral_event_id'], \
                                el['result__institution_id'], \
                                el['result__constituency_id']

                    n += 1
                    if n % 100 == 0:
                        self.logger.info(f"{n}/{len(uninomal_elected)}")
                    extraction = ExtractElected(*variables)
                    logging = {'electoral_event_id': extraction.election.id,
                               'election_name': extraction.election.name,
                               'institution_id': extraction.institution.id,
                               'institution_name': extraction.institution.name,
                               'area_id': extraction.constituency.id,
                               'area': extraction.constituency.name,
                               'candidate_id': el['id'],
                               'nome_eletto': el['name'].upper(),
                               'type': None,
                               'membership_id': None,
                               'membership_name': None
                               }
                    all_elected = extraction.elected()
                    elected = all_elected.filter(person__sort_name__person_name=el['name'])
                    if len(elected) != 1:
                        if len(all_elected) > 0:
                            df_elected = pd.DataFrame(all_elected.values('id', 'person__sort_name'))
                            chosen = process.extract(clean_name(el['name']), df_elected['person__sort_name']
                                                     .apply(lambda x: clean_name(x.upper())),
                                                     scorer=fuzz.token_set_ratio, limit=2)
                            if (chosen[0][1] > 75) and (chosen[0][1] - chosen[0][2] > 20):
                                memb_to_add = Membership.objects.get(id=df_elected.loc[chosen[0][2]]['id'])
                                ElectoralCandidateResult.objects.get(id=el['id']). \
                                    elected_membership.add(memb_to_add)
                                logging['type'] = 'found'
                                logging['membership_id'] = memb_to_add.id
                                logging['membership_name'] = memb_to_add.person.sort_name.upper()
                                list_logging.append(logging)
                            else:
                                logging['type'] = 'not found'
                                print(20 * '-')
                                print(el['name'])
                                print(extraction.election)
                                print(extraction.constituency)
                                print(extraction.institution)
                                list_logging.append(logging)
                        else:
                            logging['type'] = 'not found'
                            print(20 * '-')
                            print(el['name'])
                            print(extraction.election)
                            print(extraction.constituency)
                            print(extraction.institution)
                            list_logging.append(logging)

                    elif len(elected) == 1:
                        memb_to_add = elected[0]
                        ElectoralCandidateResult.objects.get(id=el['id']). \
                            elected_membership.add(memb_to_add)
                        logging['type'] = 'found'
                        logging['membership_id'] = elected[0].id
                        logging['membership_name'] = memb_to_add.person.sort_name.upper()
                        list_logging.append(logging)
                    else:
                        logging['type'] = 'not found'
                        list_logging.append(logging)
                df_report = pd.DataFrame(list_logging)
                file_name = typology + '_uninominal_elected.csv'

                upload_file(df_report, 'openpolis-drop', file_name)

                self.logger.info(f"Starting procedure for {typology} plurinomal CAMERA 2018")

                event = KeyEvent.objects.filter(event_type__in=[variable_maps[typology]['event_type']],
                                                start_date__gt='2012-01-01',
                                                start_date__lt='2020-10-01')\
                    .exclude(name__contains='suppletive')

                for electoral_event in event:
                    self.logger.info(f"Starting procedure for {electoral_event.name} plurinominal")
                    parlamento = Membership.objects.annotate(role_upper=Upper('role'))\
                        .filter(role_upper__in=variable_maps[typology]['role'],
                                electoral_event=electoral_event)

                    list_logging = []
                    for deputato in parlamento:

                        check_uni = ElectoralCandidateResult.objects.filter(elected_membership=deputato)
                        if len(check_uni) > 0:
                            continue
                        list_to_find = (deputato.electoral_list_descr_tmp.upper()
                                        if deputato.electoral_list_descr_tmp is not None
                                        else None)

                        logging = {'electoral_event_id': electoral_event.id,
                                   'election_name': electoral_event.name,
                                   'institution_id': deputato.organization.parent.id,
                                   'institution_name': deputato.organization.parent.name,
                                   'constituency_desc': deputato.constituency_descr_tmp,
                                   'membership_id': deputato.id,
                                   'membership_name': deputato.person.name.upper(),
                                   'list_desc': list_to_find
                                   }
                        if electoral_event.event_type == 'ELE-EU':
                            if electoral_event.name == 'Elezioni europee 25/05/2014':
                                subcand = ElectoralSubCandidateResult.objects.filter(
                                    name__person_name=deputato.person.sort_name.upper(),
                                    list_result__result__electoral_event=deputato.electoral_event,
                                    list_result__result__constituency__classification='RGNE').distinct()
                            else:
                                subcand = ElectoralSubCandidateResult.objects.filter(
                                    name__person_name=deputato.person.sort_name.upper(),
                                    is_elected=True,
                                    list_result__result__electoral_event=deputato.electoral_event,
                                    list_result__result__constituency__classification='RGNE').distinct()
                        else:
                            subcand = ElectoralSubCandidateResult\
                                .objects\
                                .filter(name__person_name=deputato.person.sort_name.upper(),
                                        is_elected=True,
                                        list_result__result__electoral_event=deputato.electoral_event).distinct()
                        if subcand.count() == 1:
                            ElectoralSubCandidateResult.objects.get(id=subcand.first().id).elected_membership\
                                .add(deputato)
                        else:
                            if electoral_event.event_type == 'ELE-EU':
                                if electoral_event.name == 'Elezioni europee 25/05/2014':
                                    subcandidates = ElectoralSubCandidateResult\
                                        .objects\
                                        .filter(list_result__result__electoral_event=deputato.electoral_event,
                                                list_result__result__constituency__classification='RGNE').distinct()
                                else:
                                    subcandidates = ElectoralSubCandidateResult.objects.filter(
                                        is_elected=True,
                                        list_result__result__electoral_event=deputato.electoral_event,
                                        list_result__result__constituency__classification='RGNE').distinct()

                            else:
                                subcandidates = ElectoralSubCandidateResult\
                                    .objects\
                                    .filter(is_elected=True,
                                            list_result__result__electoral_event=deputato.electoral_event).distinct()
                            df_elected = pd.DataFrame(subcandidates.values('id', 'name'))
                            chosen = process.extract(clean_name(deputato.person.sort_name),
                                                     df_elected['name'].apply(lambda x: clean_name(x.upper())),
                                                     scorer=fuzz.token_set_ratio, limit=2)
                            if (chosen[0][1] > 89) and (chosen[0][1] - chosen[1][1]) > 13:
                                # print(subcandidates[chosen[0][2]])
                                subcandidates[chosen[0][2]].elected_membership.add(deputato)
                            else:
                                if electoral_event.event_type == 'ELE-EU':
                                    subcandidates = ElectoralSubCandidateResult.objects.filter(
                                        list_result__result__electoral_event=deputato.electoral_event,
                                        list_result__result__constituency__classification='RGNE').distinct()
                                else:
                                    subcandidates = ElectoralSubCandidateResult\
                                        .objects\
                                        .filter(list_result__result__electoral_event=deputato.electoral_event, )\
                                        .distinct()
                                df_elected = pd.DataFrame(subcandidates.values('id', 'name'))
                                chosen = process.extract(clean_name(deputato.person.sort_name),
                                                         df_elected['name'].apply(lambda x: clean_name(x.upper())),
                                                         scorer=fuzz.token_set_ratio, limit=2)
                                if (chosen[0][1] > 89) and (chosen[0][1] - chosen[1][1]) > 13:
                                    # print(subcandidates[chosen[0][2]])
                                    subcandidates[chosen[0][2]].elected_membership.add(deputato)
                                else:
                                    if electoral_event.event_type == 'ELE-EU':
                                        subcandidates = ElectoralSubCandidateResult\
                                            .objects\
                                            .filter(
                                                    list_result__result__electoral_event=deputato.electoral_event,
                                                    list_result__result__constituency__classification='RGNE',
                                                    list_result__result__constituency__name__icontains=map_area_european
                                                    ((deputato.constituency_descr_tmp or 'None').upper())).distinct()
                                        df_elected = pd.DataFrame(subcandidates.values('id', 'name'))
                                        chosen = process.extract(clean_name(deputato.person.sort_name),
                                                                 df_elected['name'].apply(
                                                                     lambda x: clean_name(x.upper())),
                                                                 scorer=fuzz.token_set_ratio, limit=2)
                                        if (chosen[0][1] > 89) and (chosen[0][1] - chosen[1][1]) > 13:
                                            # print(subcandidates[chosen[0][2]])
                                            subcandidates[chosen[0][2]].elected_membership.add(deputato)
                                        else:
                                            print('not connected')
                                    print('not connected')

                    df_report = pd.DataFrame(list_logging)
                    file_name = (f'{electoral_event.event_type}_' + electoral_event.start_date
                                 + '_plurinominal_elected.csv')

                    upload_file(df_report, 'openpolis-drop', file_name)
            if typology in ['REG']:
                self.logger.info(f"Starting procedure for {typology} elected memberships")

                event = KeyEvent.objects.filter(event_type__in=['ELE-REG'],
                                                start_date__gt='2012-01-01',
                                                start_date__lt='2022-10-01')

                for electoral_event in event:
                    # self.logger.info(f"Starting procedure for {electoral_event.name} plurinominal")
                    parlamento = Membership.objects.annotate(role_upper=Upper('role'))\
                        .filter(electoral_event=electoral_event)

                    for index, deputato in enumerate(parlamento):
                        check_uni = ElectoralCandidateResult.objects.filter(elected_membership=deputato)
                        if len(check_uni) > 0:
                            continue
                        list_to_find = (deputato.electoral_list_descr_tmp.upper()
                                        if deputato.electoral_list_descr_tmp is not None
                                        else None)

                        logging = {'electoral_event_id': electoral_event.id,
                                   'election_name': electoral_event.name,
                                   'institution_id': deputato.organization.parent.id,
                                   'institution_name': deputato.organization.parent.name,
                                   'constituency_desc': deputato.constituency_descr_tmp
                                   if deputato.constituency_descr_tmp is not None else None,
                                   'membership_id': deputato.id,
                                   'membership_name': deputato.person.name.upper(),
                                   'list_desc': list_to_find
                                   }
                        liste = ElectoralListResult\
                            .objects\
                            .filter(
                                    result__constituency__classification__in=['ELECT_CIRC_REG', 'ADM1'],
                                    result__electoral_event_id=deputato.electoral_event_id,
                                    result__institution=deputato.organization.parent)
                        if liste:
                            subcandidates = ElectoralSubCandidateResult.objects.filter(list_result__in=liste)
                            subcandidate_first_choice = subcandidates\
                                .filter(name__person_name=deputato.person.sort_name.upper())
                            if len(subcandidate_first_choice) == 1:
                                subcandidate_first_choice[0].elected_membership.add(deputato)
                                print(subcandidate_first_choice[0].list_result)
                                # print('found')

                            elif len(subcandidate_first_choice) == 0:
                                check = deputato.person.id == Membership\
                                    .objects\
                                    .get(electoral_event=deputato.electoral_event,
                                         organization__parent=deputato.organization.parent,
                                         role='Presidente di Regione').person.id
                                if check:
                                    cand_res = ElectoralCandidateResult\
                                        .objects\
                                        .get(
                                             id__in=ElectoralCandidateResult.objects.filter(
                                                 elected_membership__person__id=deputato.person.id,
                                                 result__electoral_event__id=deputato.electoral_event.id
                                             ).distinct().values_list('id'))
                                    cand_res.elected_membership.add(deputato)
                                else:
                                    df_elected = pd.DataFrame(subcandidates.values('id', 'name', 'is_elected'))
                                    chosen = process.extract(
                                        clean_name(deputato.person.sort_name),
                                        df_elected['name'].apply(lambda x:
                                                                 clean_name(
                                                                     x.upper())),
                                        scorer=fuzz.token_set_ratio,
                                        limit=2)
                                    if (chosen[0][1] > 89) and (chosen[0][1] - chosen[1][1]) > 13:
                                        # print(subcandidates[chosen[0][2]])
                                        subcandidates[chosen[0][2]].elected_membership.add(deputato)
                                    else:
                                        list_of_words = ['Assessore',
                                                         'Sottosegretario',
                                                         'Presidente',
                                                         'Vicepresidente']
                                        words_re = re.compile("|".join(list_of_words))
                                        if words_re.search(deputato.role):
                                            if deputato.electoral_list_descr_tmp:
                                                print('not a conselour')
                                            else:
                                                continue
                                        else:

                                            check = ElectoralCandidateResult\
                                                .objects\
                                                .filter(name=deputato.person.sort_name.upper(),
                                                        result__electoral_event__event_type=variable_maps
                                                        [typology]['event_type'])\
                                                .filter(
                                                        result__constituency__classification='ADM1',
                                                        result__electoral_event=electoral_event)
                                            if check:
                                                check.first().elected_membership.add(deputato)
                                                # print('connect with candidate')

                                            else:
                                                print('no found')

                            else:
                                is_elected_sub = subcandidate_first_choice.filter(is_elected=True)
                                if len(is_elected_sub) == 1:
                                    is_elected_sub.first().elected_membership.add(deputato)
                                else:
                                    names_list = [x.name for x in subcandidates]
                                    extr = process.extract(deputato.person.sort_name.upper(),
                                                           names_list,
                                                           scorer=fuzz.token_set_ratio,
                                                           limit=2)
                                    if extr[0][1] > 90:
                                        subcandidates.filter(name__person_name=extr[0][0])
                                    else:
                                        print(f'{index} {deputato.organization.parent.name} '
                                              f'exists elections but no found {deputato.person.sort_name.upper()}')
                        else:
                            print(f'{index} {deputato.organization.parent.name} no election in {electoral_event.name}')

            self.logger.info(f"End procedure for {typology}")
        except Exception as e:
            print(e)
