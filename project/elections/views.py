"""Define elections views."""
from api_v1.serializers import KeyEventSerializer, OrganizationInlineSerializer, AreaInlineSerializer, \
    MembershipInlineSerializer
from api_v1.views import MembershipViewSet
from popolo.models import KeyEvent, Organization, Area
from rest_framework import filters
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet, ReadOnlyModelViewSet
from drf_rw_serializers import viewsets as rw_viewsets
from drf_yasg.utils import swagger_auto_schema

from django_filters import rest_framework as extra_filters

from api_v1.filters import NullsLastOrderingFilter, FiltersInListOnlySchema
from api_v1.views.DetailActionsMixin import DetailActionsMixin
from .filtersets import ElectoralResultFilterSet, KeyEventElectoralResultFilterSet, \
    OrganizationElectoralResultFilterSet, AreaElectoralResultFilterSet, ElectoralListResultFilterSet, \
    ElectoralCandidateResultFilterSet
from .models import (
    ElectoralCandidateResult,
    ElectoralListResult,
    ElectoralResult,
    PoliticalColour,
)
from .serializers import (
    ElectoralCandidateResultSerializer,
    ElectoralListResultSerializer,
    ElectoralResultSerializer,
    PoliticalColourSerializer, ElectoralCandidateResultWriteSerializer, ElectoralListResultWriteSerializer,
)


class PoliticalColourViewSet(RetrieveModelMixin, ListModelMixin, GenericViewSet):
    """A serializer for PoliticalColour model."""

    queryset = PoliticalColour.objects.all().order_by("name", "pk")
    serializer_class = PoliticalColourSerializer


class ElectoralResultViewSet(DetailActionsMixin, ReadOnlyModelViewSet):
    """A serializer for ElectoralResult model."""

    queryset = ElectoralResult.objects.all().prefetch_related(
        "electoral_event", "institution", "constituency"
    )

    serializer_class = ElectoralResultSerializer
    filterset_class = ElectoralResultFilterSet

    search_fields = ("electoral_event__name", "institution__name", "constituency__name")
    ordering_fields = (
        "electoral_event__start_date",
        "constituency__inhabitants",
        "votes_cast_perc"
    )
    ordering = ("-electoral_event__start_date",)

    filter_backends = (
        filters.SearchFilter,
        NullsLastOrderingFilter,
        extra_filters.DjangoFilterBackend,
    )
    filter_class = ElectoralResultFilterSet

    @action(
        detail=False,
        queryset=KeyEvent.objects.filter(
            id__in=ElectoralResult.objects.values_list('electoral_event__id', flat=True).distinct()
        ),
        serializer_class=KeyEventSerializer,
        ordering=("-start_date", ),
        ordering_fields=("start_date", "name", ),
        search_fields=("name", ),
        filterset_class=KeyEventElectoralResultFilterSet,
    )
    def events(self, request, *args, **kwargs):
        """ All events for the existing electoral results """
        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)

    @action(
        detail=False,
        queryset=Organization.objects.filter(
            id__in=ElectoralResult.objects.values_list('institution__id', flat=True).distinct()
        ),
        serializer_class=OrganizationInlineSerializer,
        ordering=("name", ),
        ordering_fields=("name", ),
        search_fields=("name", ),
        filterset_class=OrganizationElectoralResultFilterSet,
    )
    def institutions(self, request, *args, **kwargs):
        """ All institutions for the existing electoral results """
        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)

    @action(
        detail=False,
        queryset=Area.objects.filter(
            id__in=ElectoralResult.objects.values_list('constituency__id', flat=True).distinct()
        ),
        serializer_class=AreaInlineSerializer,
        ordering=("-inhabitants", ),
        ordering_fields=("name", "inhabitants",),
        search_fields=("name", ),
        filterset_class=AreaElectoralResultFilterSet,
    )
    def constituencies(self, request, *args, **kwargs):
        """ All constituencies for the existing electoral results """
        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)

    @action(
        detail=True,
        ordering=("-votes",),
        ordering_fields=("name", "candidate_result__name", "votes"),
        filterset_class=ElectoralListResultFilterSet
    )
    def list_results(self, request, **kwargs):
        """Return electoral list results for the event."""
        instance = self.get_object_no_filter()

        self.serializer_class = ElectoralListResultSerializer
        listresults_viewset = ElectoralListResultViewSet(request=request)
        self.search_fields = []
        self.queryset = (
            instance.list_results.all()
        )
        self.queryset = listresults_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)

    @action(
        detail=True,
        ordering=("-votes",),
        ordering_fields=("name", "votes"),
        filterset_class=ElectoralCandidateResultFilterSet
    )
    def candidate_results(self, request, **kwargs):
        """Return electoral candidate results for the event."""
        instance = self.get_object_no_filter()

        self.serializer_class = ElectoralCandidateResultSerializer
        candidateresults_viewset = ElectoralCandidateResultViewSet(request=request)
        self.search_fields = []
        self.queryset = (
            instance.candidate_results.all()
        )
        self.queryset = candidateresults_viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)


class ElectoralListResultViewSet(DetailActionsMixin, rw_viewsets.ModelViewSet):
    """A serializer for ElectoralListResult model."""

    queryset = ElectoralListResult.objects.all().order_by("pk")

    # use a custom AutoSchema class to show filters only in list action
    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = ElectoralListResultSerializer
    write_serializer_class = ElectoralListResultWriteSerializer

    @swagger_auto_schema(request_body=ElectoralListResultWriteSerializer)
    def update(self, request, *args, **kwargs):
        return super(ElectoralListResultViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=ElectoralListResultWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        return super(ElectoralListResultViewSet, self).partial_update(request, *args, **kwargs)

    @action(detail=True)
    def memberships(self, request, **kwargs):
        """Return memberships connected to this electoral list."""
        instance = self.get_object_no_filter()
        self.serializer_class = MembershipInlineSerializer
        _viewset = MembershipViewSet(request=request)
        self.queryset = (
            instance.get_elected_memberships
        )
        self.queryset = _viewset.filter_queryset(self.queryset)

        page = self.paginate_queryset(self.filter_queryset(self.get_queryset()))
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)
        serializer = self.get_serializer(
            self.filter_queryset(self.get_queryset()), many=True
        )
        return Response(serializer.data)


class ElectoralCandidateResultViewSet(rw_viewsets.ModelViewSet):
    """A serializer for ElectoralCandidateResult model."""

    queryset = ElectoralCandidateResult.objects.all().order_by("pk")

    # use a custom AutoSchema class to show filters only in list action
    schema = FiltersInListOnlySchema()

    read_serializer_class = serializer_class = ElectoralCandidateResultSerializer
    write_serializer_class = ElectoralCandidateResultWriteSerializer

    @swagger_auto_schema(request_body=ElectoralCandidateResultWriteSerializer)
    def update(self, request, *args, **kwargs):
        return super(ElectoralCandidateResultViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(request_body=ElectoralCandidateResultWriteSerializer)
    def partial_update(self, request, *args, **kwargs):
        return super(ElectoralCandidateResultViewSet, self).partial_update(request, *args, **kwargs)
