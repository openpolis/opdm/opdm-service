"""Define elections models tests."""

from decimal import Decimal

from api_v1.tests.utils.mixins import APIResourceTestCase
from popolo.models import Area, KeyEvent, Organization, Person, Membership

from project.elections.models import (
    ElectoralCandidateResult,
    ElectoralListResult,
    ElectoralResult,
    PoliticalColour,
)


class PoliticalColourViewSetTest(APIResourceTestCase):
    """Define political colours endpoints tests."""

    endpoint = "/api-mappepotere/v1/political_colours"

    @classmethod
    def setUpTestData(cls):
        """Initialize test data."""
        cls.political_colour = PoliticalColour.objects.create(pk=1, name="test colour")

    def test_list(self):
        """Test if return political colours list."""
        response = self.client.get(self.endpoint)
        self.assertDictEqual(
            response.json(),
            {
                "count": 1,
                "next": None,
                "previous": None,
                "results": [{"id": 1, "name": "test colour"}],
            },
        )


class ElectoralResultViewSetTest(APIResourceTestCase):
    """Define electoral results endpoints tests."""

    endpoint = "/api-mappepotere/v1/electoral_results"

    @classmethod
    def setUpTestData(cls):
        """Initialize test data."""
        cls.electoral_event = KeyEvent.objects.create(pk=1, name="test event")
        cls.institution = Organization.objects.create(pk=1, name="test organization")
        cls.constituency = Area.objects.create(pk=1, name="test area")
        cls.electoral_result = ElectoralResult.objects.create(
            pk=1,
            registered_voters=0,
            votes_cast=0,
            votes_cast_perc=Decimal("0"),
            invalid_votes=0,
            blank_votes=0,
            valid_votes=0,
            is_valid=True,
            is_total=True,
            electoral_event=cls.electoral_event,
            institution=cls.institution,
            constituency=cls.constituency,
        )
        cls.candidate = Person.objects.create(
            pk=1, family_name="Test", given_name="Name"
        )
        cls.elected_membership = Membership.objects.create(
            pk=1, person=cls.candidate, organization=cls.institution,
            role="Test role", post=None, label="Test label"
        )
        cls.candidate_result = ElectoralCandidateResult.objects.create(
            pk=1,
            votes=1,
            votes_perc=Decimal("0"),
            is_counselor=False,
            is_top=False,
            result=cls.electoral_result,
        )
        # cls.candidate_result.elected_membership.set([cls.elected_membership, ])
        cls.political_colour = PoliticalColour.objects.create(
            pk=1, name="political colour"
        )
        cls.electoral_list_result = ElectoralListResult.objects.create(
            pk=1,
            name="test list result",
            political_colour=cls.political_colour,
            result=cls.electoral_result,
            candidate_result=cls.candidate_result,
        )
        cls.electoral_list_result.parties.set([cls.institution, ])

    def test_list(self):
        """Test if return electoral results."""
        response = self.client.get(self.endpoint)
        self.maxDiff = None
        self.assertDictEqual(
            response.json(),
            {
                "count": 1,
                "next": None,
                "previous": None,
                "results": [
                    {
                        "blank_votes": 0,
                        "constituency": {
                            'classification': '',
                            'id': 1,
                            'identifier': '',
                            'inhabitants': None,
                            'istat_classification': None,
                            'name': 'test area',
                            'url': 'http://testserver/api-mappepotere/v1/areas/1'
                        },
                        "electoral_event": {
                            'end_date': None,
                            'event_type': 'ELE',
                            'id': 1,
                            'identifier': None,
                            'name': 'test event',
                            'start_date': None,
                            'url': 'http://testserver/api-mappepotere/v1/keyevents/1'
                        },
                        "id": 1,
                        "url": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                        "institution": {
                            'forma_giuridica': None,
                            'id': 1,
                            'name': 'test organization',
                            'url': 'http://testserver/api-mappepotere/v1/organizations/1'
                        },
                        "invalid_votes": 0,
                        "has_second_round": False,
                        "is_valid": True,
                        "is_total": True,
                        "registered_voters": 0,
                        "valid_votes": 0,
                        "votes_cast": 0,
                        "votes_cast_perc": "0.00"
                    }
                ],
            },
        )

    def test_retrieve(self):
        """Test if return an electoral result."""
        endpoint = f"{self.endpoint}/1"
        self.maxDiff = None
        response = self.client.get(endpoint)
        self.assertDictEqual(
            response.json(),
            {
                "blank_votes": 0,
                "constituency": {
                    'classification': '',
                    'id': 1,
                    'identifier': '',
                    'inhabitants': None,
                    'istat_classification': None,
                    'name': 'test area',
                    'url': 'http://testserver/api-mappepotere/v1/areas/1'
                },
                "electoral_event": {
                    'end_date': None,
                    'event_type': 'ELE',
                    'id': 1,
                    'identifier': None,
                    'name': 'test event',
                    'start_date': None,
                    'url': 'http://testserver/api-mappepotere/v1/keyevents/1'
                  },
                "id": 1,
                "url": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                "institution": {
                    'forma_giuridica': None,
                    'id': 1,
                    'name': 'test organization',
                    'url': 'http://testserver/api-mappepotere/v1/organizations/1'
                },
                "invalid_votes": 0,
                "has_second_round": False,
                "is_valid": True,
                "is_total": True,
                "registered_voters": 0,
                "valid_votes": 0,
                "votes_cast": 0,
                "votes_cast_perc": "0.00"
            },
        )

    def test_list_has_second_round_filtered(self):
        """Test if return electoral results filtered by has_second_round."""
        response = self.client.get(self.endpoint, data={"has_second_round": False})
        response_json = response.json()
        self.assertEqual(response_json["count"], 1)

        response = self.client.get(self.endpoint, data={"has_second_round": True})
        response_json = response.json()
        self.assertEqual(response_json["count"], 0)

    def test_list_constituency_filter(self):
        """Test if return electoral results filtered by constituency."""
        response = self.client.get(self.endpoint, data={"constituency__id": 1})
        response_json = response.json()
        self.assertEqual(response_json["count"], 1)

        response = self.client.get(self.endpoint, data={"constituency__id": 2})
        response_json = response.json()
        self.assertEqual(response_json["count"], 0)

    def test_list_electoral_event_filter(self):
        """Test if return electoral results filtered by electoral_event."""
        response = self.client.get(self.endpoint, data={"electoral_event__id": 1})
        response_json = response.json()
        self.assertEqual(response_json["count"], 1)

        response = self.client.get(self.endpoint, data={"electoral_event__id": 2})
        response_json = response.json()
        self.assertEqual(response_json["count"], 0)

    def test_list_institution_filter(self):
        """Test if return electoral results filtered by institution."""
        response = self.client.get(self.endpoint, data={"institution__id": 1})
        response_json = response.json()
        self.assertEqual(response_json["count"], 1)

        response = self.client.get(self.endpoint, data={"institution__id": 2})
        response_json = response.json()
        self.assertEqual(response_json["count"], 0)

    def test_list_results(self):
        """Test if return electoral result list results."""
        self.maxDiff = None
        endpoint = f"{self.endpoint}/1/list_results"
        response = self.client.get(endpoint).json()
        self.assertEqual(response['count'], 1)
        self.assertListEqual(
            response['results'],
            [
                {
                    "candidate_result": {
                        # 'elected_membership': [{
                        #     'constituency_descr_tmp': None,
                        #     'electoral_list_descr_tmp': None,
                        #     'end_date': None,
                        #     'end_reason': None,
                        #     'id': 1,
                        #     'label': 'Test label',
                        #     'person_name': 'Name Test',
                        #     'role': 'Test role',
                        #     'start_date': None,
                        #     'url': 'http://testserver/api-mappepotere/v1/memberships/1'
                        # }],
                        'id': 1,
                        'is_counselor': False,
                        'is_top': False,
                        'result': 'http://testserver/api-mappepotere/v1/electoral_results/1',
                        'tmp_candidate': {},
                        'url': 'http://testserver/api-mappepotere/v1/candidate_results/1',
                        'name': '',
                        'votes': 1,
                        'votes_perc': '0.00'
                    },
                    "id": 1,
                    "url": 'http://testserver/api-mappepotere/v1/list_results/1',
                    "image": None,
                    "name": "test list result",
                    "parties": [{
                        'id': 1,
                        'name': 'test organization',
                        'forma_giuridica': None,
                        'url': 'http://testserver/api-mappepotere/v1/organizations/1'
                    }],
                    "political_colour": {
                        'id': self.political_colour.id,
                        'name': self.political_colour.name,
                    },
                    "tmp_lists": [],
                    # 'elected_memberships': [],
                    "result": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                    "seats": None,
                    "votes": None,
                    "votes_perc": None
                }
            ],
        )

    def test_candidate_results(self):
        """Test if return electoral result candidate results."""
        endpoint = f"{self.endpoint}/1/candidate_results"
        response = self.client.get(endpoint).json()
        self.maxDiff = None
        self.assertEqual(response['count'], 1)
        self.assertListEqual(
            response['results'],
            [
                {
                    "id": 1,
                    "url": 'http://testserver/api-mappepotere/v1/candidate_results/1',
                    "name": '',
                    "votes": 1,
                    "votes_perc": "0.00",
                    "is_counselor": False,
                    "is_top": False,
                    # "elected_membership": [{
                    #     "constituency_descr_tmp": None,
                    #     "electoral_list_descr_tmp": None,
                    #     "end_date": None,
                    #     "end_reason": None,
                    #     "id": 1,
                    #     "label": "Test label",
                    #     "person_name": "Name Test",
                    #     "role": "Test role",
                    #     "start_date": None,
                    #     "url": "http://testserver/api-mappepotere/v1/memberships/1"
                    # }],
                    "result": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                    "tmp_candidate": {},
                }
            ],
        )


class ElectoralListResultTest(APIResourceTestCase):
    """Define electorallistresult endpoints tests."""

    endpoint = "/api-mappepotere/v1/list_results"

    @classmethod
    def setUpTestData(cls):
        """Initialize test data."""
        cls.electoral_event = KeyEvent.objects.create(pk=1, name="test event")
        cls.institution = Organization.objects.create(
            pk=1, name="test organization",
            classification='Partito/Movimento politico'
        )
        cls.constituency = Area.objects.create(pk=1, name="test area")
        cls.electoral_result = ElectoralResult.objects.create(
            pk=1,
            registered_voters=0,
            votes_cast=0,
            votes_cast_perc=Decimal("0"),
            invalid_votes=0,
            blank_votes=0,
            valid_votes=0,
            is_valid=True,
            is_total=True,
            electoral_event=cls.electoral_event,
            institution=cls.institution,
            constituency=cls.constituency,
        )
        cls.candidate = Person.objects.create(
            pk=1, family_name="Test", given_name="Name"
        )
        cls.elected_membership = Membership.objects.create(
            pk=1, person=cls.candidate, organization=cls.institution,
            role="Test role", post=None, label="Test label"
        )
        cls.candidate_result = ElectoralCandidateResult.objects.create(
            pk=1,
            votes=1,
            votes_perc=Decimal("0"),
            is_counselor=False,
            is_top=False,
            result=cls.electoral_result,
        )
        # cls.candidate_result.elected_membership.set([cls.elected_membership, ])
        cls.political_colour = PoliticalColour.objects.create(
            pk=1, name="political colour"
        )
        cls.electoral_list_result = ElectoralListResult.objects.create(
            pk=1,
            name="test list result",
            political_colour=cls.political_colour,
            result=cls.electoral_result,
            candidate_result=cls.candidate_result,
            tmp_lists=['list_1', 'list_2']
        )
        cls.electoral_list_result.parties.set([cls.institution, ])

    def test_retrieve(self):
        """Test if return an electoral list result."""
        endpoint = f"{self.endpoint}/1"
        self.maxDiff = None
        response = self.client.get(endpoint)
        self.assertDictEqual(
            response.json(),
            {
                "id": 1,
                "url": 'http://testserver/api-mappepotere/v1/list_results/1',
                "image": None,
                "name": "test list result",
                "votes": None,
                "votes_perc": None,
                "seats": None,
                "result": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                "candidate_result": {
                    # 'elected_membership': [{
                    #     'constituency_descr_tmp': None,
                    #     'electoral_list_descr_tmp': None,
                    #     'end_date': None,
                    #     'end_reason': None,
                    #     'id': 1,
                    #     'label': 'Test label',
                    #     'person_name': 'Name Test',
                    #     'role': 'Test role',
                    #     'start_date': None,
                    #     'url': 'http://testserver/api-mappepotere/v1/memberships/1'
                    # }, ],
                    'id': 1,
                    'is_counselor': False,
                    'is_top': False,
                    'result': 'http://testserver/api-mappepotere/v1/electoral_results/1',
                    'tmp_candidate': {},
                    'url': 'http://testserver/api-mappepotere/v1/candidate_results/1',
                    'name': '',
                    'votes': 1,
                    'votes_perc': '0.00'
                },
                "political_colour": {
                    'id': self.political_colour.id,
                    'name': self.political_colour.name,
                },
                "parties": [{
                    'id': 1,
                    'name': 'test organization',
                    'forma_giuridica': 'Partito/Movimento politico',
                    'url': 'http://testserver/api-mappepotere/v1/organizations/1'
                }],
                "tmp_lists": ["list_1", "list_2"],
                # 'elected_memberships': []
            },
        )

    def test_patch_political_colour(self):
        """Test partial update of list result

        :return:
        """
        endpoint = f"{self.endpoint}/1"
        response = self.client.patch(
            endpoint,
            {'political_colour': None},
            format="json"
        )
        self.maxDiff = None
        self.assertDictEqual(
            response.json(),
            {
                "id": 1,
                "url": 'http://testserver/api-mappepotere/v1/list_results/1',
                "image": None,
                "name": "test list result",
                "votes": None,
                "votes_perc": None,
                "seats": None,
                "result": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                "candidate_result": {
                    # 'elected_membership': [{
                    #     'constituency_descr_tmp': None,
                    #     'electoral_list_descr_tmp': None,
                    #     'end_date': None,
                    #     'end_reason': None,
                    #     'id': 1,
                    #     'label': 'Test label',
                    #     'person_name': 'Name Test',
                    #     'role': 'Test role',
                    #     'start_date': None,
                    #     'url': 'http://testserver/api-mappepotere/v1/memberships/1'
                    # }],
                    'id': 1,
                    'is_counselor': False,
                    'is_top': False,
                    'result': 'http://testserver/api-mappepotere/v1/electoral_results/1',
                    'tmp_candidate': {},
                    'url': 'http://testserver/api-mappepotere/v1/candidate_results/1',
                    'name': '',
                    'votes': 1,
                    'votes_perc': '0.00'
                },
                "political_colour": None,
                "parties": [{
                    'id': 1,
                    'name': 'test organization',
                    'forma_giuridica': 'Partito/Movimento politico',
                    'url': 'http://testserver/api-mappepotere/v1/organizations/1'
                }],
                "tmp_lists": ["list_1", "list_2"],
                # 'elected_memberships': []
            },
        )

    def test_patch_parties(self):
        """Test partial update of list result

        :return:
        """
        not_a_party = Organization.objects.create(
            pk=2, name="test organization",
            classification='Something else'
        )

        self.maxDiff = None
        endpoint = f"{self.endpoint}/1"
        response = self.client.patch(
            endpoint,
            {'parties': [not_a_party.id]},
            format="json"
        )
        self.assertDictEqual(
            response.json(),
            {'parties': ['Pk "2" non valido - l\'oggetto non esiste.']},
        )

        response = self.client.patch(
            endpoint,
            {'parties': [1]},
            format="json"
        )
        self.assertDictEqual(
            response.json(),
            {
                "id": 1,
                "url": 'http://testserver/api-mappepotere/v1/list_results/1',
                "image": None,
                "name": "test list result",
                "votes": None,
                "votes_perc": None,
                "seats": None,
                "result": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                "candidate_result": {
                    # 'elected_membership': [{
                    #     'constituency_descr_tmp': None,
                    #     'electoral_list_descr_tmp': None,
                    #     'end_date': None,
                    #     'end_reason': None,
                    #     'id': 1,
                    #     'label': 'Test label',
                    #     'person_name': 'Name Test',
                    #     'role': 'Test role',
                    #     'start_date': None,
                    #     'url': 'http://testserver/api-mappepotere/v1/memberships/1'
                    # }],
                    'id': 1,
                    'is_counselor': False,
                    'is_top': False,
                    'result': 'http://testserver/api-mappepotere/v1/electoral_results/1',
                    'tmp_candidate': {},
                    'url': 'http://testserver/api-mappepotere/v1/candidate_results/1',
                    'name': '',
                    'votes': 1,
                    'votes_perc': '0.00'
                },
                "political_colour": {
                    'id': self.political_colour.id,
                    'name': self.political_colour.name,
                },
                "parties": [{
                    'id': 1,
                    'name': 'test organization',
                    'forma_giuridica': 'Partito/Movimento politico',
                    'url': 'http://testserver/api-mappepotere/v1/organizations/1'
                }],
                "tmp_lists": ["list_1", "list_2"],
                # 'elected_memberships': []
            },
        )

    def test_patch_tmp_lists(self):
        """Test partial update of tmp_list field

        :return:
        """
        self.maxDiff = None
        endpoint = f"{self.endpoint}/1"
        response = self.client.patch(
            endpoint,
            {'tmp_lists': []},
            format="json"
        )
        self.assertDictEqual(
            response.json(),
            {
                "id": 1,
                "url": 'http://testserver/api-mappepotere/v1/list_results/1',
                "image": None,
                "name": "test list result",
                "votes": None,
                "votes_perc": None,
                "seats": None,
                "result": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                "candidate_result": {
                    # 'elected_membership': [{
                    #     'constituency_descr_tmp': None,
                    #     'electoral_list_descr_tmp': None,
                    #     'end_date': None,
                    #     'end_reason': None,
                    #     'id': 1,
                    #     'label': 'Test label',
                    #     'person_name': 'Name Test',
                    #     'role': 'Test role',
                    #     'start_date': None,
                    #     'url': 'http://testserver/api-mappepotere/v1/memberships/1'
                    # }],
                    'id': 1,
                    'is_counselor': False,
                    'is_top': False,
                    'result': 'http://testserver/api-mappepotere/v1/electoral_results/1',
                    'tmp_candidate': {},
                    'url': 'http://testserver/api-mappepotere/v1/candidate_results/1',
                    'name': '',
                    'votes': 1,
                    'votes_perc': '0.00'
                },
                "political_colour": {
                    'id': self.political_colour.id,
                    'name': self.political_colour.name,
                },
                "parties": [{
                    'id': 1,
                    'name': 'test organization',
                    'forma_giuridica': 'Partito/Movimento politico',
                    'url': 'http://testserver/api-mappepotere/v1/organizations/1'
                }],
                "tmp_lists": [],
                # 'elected_memberships': []
            },
        )

        response = self.client.patch(
            endpoint,
            {'tmp_lists': ['list_A', 'list_B']},
            format="json"
        )
        self.assertDictEqual(
            response.json(),
            {
                "id": 1,
                "url": 'http://testserver/api-mappepotere/v1/list_results/1',
                "image": None,
                "name": "test list result",
                "votes": None,
                "votes_perc": None,
                "seats": None,
                "result": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                "candidate_result": {
                    # 'elected_membership': [{
                    #     'constituency_descr_tmp': None,
                    #     'electoral_list_descr_tmp': None,
                    #     'end_date': None,
                    #     'end_reason': None,
                    #     'id': 1,
                    #     'label': 'Test label',
                    #     'person_name': 'Name Test',
                    #     'role': 'Test role',
                    #     'start_date': None,
                    #     'url': 'http://testserver/api-mappepotere/v1/memberships/1'
                    # }],
                    'id': 1,
                    'is_counselor': False,
                    'is_top': False,
                    'result': 'http://testserver/api-mappepotere/v1/electoral_results/1',
                    'tmp_candidate': {},
                    'url': 'http://testserver/api-mappepotere/v1/candidate_results/1',
                    'name': '',
                    'votes': 1,
                    'votes_perc': '0.00'
                },
                "political_colour": {
                    'id': self.political_colour.id,
                    'name': self.political_colour.name,
                },
                "parties": [{
                    'id': 1,
                    'name': 'test organization',
                    'forma_giuridica': 'Partito/Movimento politico',
                    'url': 'http://testserver/api-mappepotere/v1/organizations/1'
                }],
                "tmp_lists": ['list_A', 'list_B'],
                # 'elected_memberships': []
            },
        )


class ElectoraCandidateResultViewSetTest(APIResourceTestCase):
    """Define electoralcandidateresult endpoints tests."""

    endpoint = "/api-mappepotere/v1/candidate_results"

    @classmethod
    def setUpTestData(cls):
        """Initialize test data."""
        cls.electoral_event = KeyEvent.objects.create(pk=1, name="test event")
        cls.institution = Organization.objects.create(pk=1, name="test organization")
        cls.party = Organization.objects.create(pk=2, name="test party")
        cls.political_colour = PoliticalColour.objects.create(pk=1, name="test colour")
        cls.constituency = Area.objects.create(pk=1, name="test area")
        # cls.elected_membership = Membership.objects.create(pk=1, name="test elected_membership")
        cls.electoral_result = ElectoralResult.objects.create(
            pk=1,
            registered_voters=0,
            votes_cast=0,
            votes_cast_perc=Decimal("0"),
            invalid_votes=0,
            blank_votes=0,
            valid_votes=0,
            is_valid=True,
            is_total=True,
            electoral_event=cls.electoral_event,
            institution=cls.institution,
            constituency=cls.constituency,
        )
        cls.candidate = Person.objects.create(
            pk=1, family_name="Test", given_name="Name"
        )
        cls.elected_membership = Membership.objects.create(
            pk=1, person=cls.candidate, organization=cls.institution,
            role="Test role", post=None, label="Test label"
        )
        cls.candidate_result = ElectoralCandidateResult.objects.create(
            pk=1,
            votes=1,
            votes_perc=Decimal("0"),
            is_counselor=False,
            is_top=False,
            result=cls.electoral_result,
        )
        # cls.candidate_result.elected_membership.set([cls.elected_membership, ])

    def test_retrieve(self):
        """Test if return a candidate result."""
        endpoint = f"{self.endpoint}/1"
        self.maxDiff = None
        response = self.client.get(endpoint)
        self.assertDictEqual(
            response.json(),
            {
                "id": 1,
                "url": 'http://testserver/api-mappepotere/v1/candidate_results/1',
                "name": '',
                "votes": 1,
                "votes_perc": "0.00",
                "is_counselor": False,
                "is_top": False,
                # 'elected_membership': [{
                #     'constituency_descr_tmp': None,
                #     'electoral_list_descr_tmp': None,
                #     'end_date': None,
                #     'end_reason': None,
                #     'id': 1,
                #     'label': 'Test label',
                #     'person_name': 'Name Test',
                #     'role': 'Test role',
                #     'start_date': None,
                #     'url': 'http://testserver/api-mappepotere/v1/memberships/1'
                # }],
                "result": 'http://testserver/api-mappepotere/v1/electoral_results/1',
                "tmp_candidate": {},
            },
        )
