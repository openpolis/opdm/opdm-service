"""Define load elections tests."""

from pathlib import Path

from django.test import SimpleTestCase

from project.elections.load import Importazione, load_json_items


class ImportazioneTest(SimpleTestCase):
    """Define importazione tests."""

    maxDiff = None

    def test_json_schema(self):
        """Test importazione json schema."""
        data_dir = Path(__file__).parent / "data"
        self.assertJSONEqual(
            Importazione.schema_json(),
            Path(data_dir / "electoral_results.schema.json").read_text(),
        )


class LoadElectionsTest(SimpleTestCase):
    """Define load elections tests."""

    maxDiff = None

    def test_path_attribute(self):
        """Test path attribute."""
        data_dir = Path(__file__).parent / "data"
        empty_file = str(data_dir / "empty")
        self.assertTupleEqual(
            load_json_items(empty_file),
            (None, "Expecting value: line 1 column 1 (char 0)"),
        )
        empty_text = str(data_dir / "empty.txt")
        self.assertTupleEqual(
            load_json_items(empty_text),
            (None, "Expecting value: line 1 column 1 (char 0)"),
        )
        empty_gzip = str(data_dir / "empty.txt.gz")
        self.assertTupleEqual(
            load_json_items(empty_gzip),
            (
                None,
                "'utf-8' codec can't decode byte 0x8b in position 1: "
                "invalid start byte",
            ),
        )
        empty_json = str(data_dir / "empty.json")
        self.assertTupleEqual(
            load_json_items(empty_json),
            (
                None,
                "1 validation error for Importazione\n"
                "eventi\n  field required (type=value_error.missing)",
            ),
        )
        wrong_json = str(data_dir / "wrong.json")
        self.assertTupleEqual(
            load_json_items(wrong_json),
            (None,
             "1 validation error for Importazione\neventi -> 0 -> tipo\n  "
             "value is not a valid enumeration member; permitted: 'ELE-EU', "
             "'ELE-POL', 'ELE-REG', 'ELE-COM', 'ELE-PROV', 'ELE-SUP' "
             "(type=type_error.enum; enum_values=[<TipoEvento.europee: 'ELE-EU'>, "
             "<TipoEvento.politiche: 'ELE-POL'>, <TipoEvento.regionali: 'ELE-REG'>, "
             "<TipoEvento.comunali: 'ELE-COM'>, <TipoEvento.provinciali: 'ELE-PROV'>, "
             "<TipoEvento.suppletive: 'ELE-SUP'>])"),
        )
