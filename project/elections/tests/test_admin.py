"""Define common tests."""

from django.contrib.admin import site as default_site
from django.test import RequestFactory, SimpleTestCase
from django.utils.translation import gettext_lazy as _

from project.elections.admin import (
    ElectoralCandidateResultAdmin,
    ElectoralListResultAdmin,
    ElectoralResultAdmin,
)
from project.elections.models import (
    ElectoralCandidateResult,
    ElectoralListResult,
    ElectoralResult,
)


class ElectoralListResultAdminTest(SimpleTestCase):
    """Tests for admin."""

    def setUp(self):
        """Initialize tests data."""
        self.admin_instance = ElectoralListResultAdmin(
            model=ElectoralListResult, admin_site=default_site
        )
        self.request = RequestFactory().get("/")

    def test_has_add_permission(self):
        """Test if return queryset."""
        self.assertFalse(self.admin_instance.has_add_permission(self.request))

    def test_has_change_permission(self):
        """Test if return queryset."""
        self.assertTrue(self.admin_instance.has_change_permission(self.request))

    def test_has_delete_permission(self):
        """Test if return queryset."""
        self.assertFalse(self.admin_instance.has_delete_permission(self.request))

    def test_get_fieldsets(self):
        """Test get_fieldsets."""
        result = ElectoralResult()
        list_result = ElectoralListResult(result=result)
        fieldsets = self.admin_instance.get_fieldsets(self.request, obj=list_result)
        list_result_fieldsets = (
            (None, {"fields": ("name", "candidate_result", "image", "political_colour", "parties")}),
            (_("Results"), {"fields": (("votes", "votes_perc", "seats"),)}),
            (_("Elected"), {"fields": ("elected_memberships",)})
        )
        self.assertTupleEqual(tuple(fieldsets), list_result_fieldsets)
        result.ballot_votes_cast = 1
        fieldsets = self.admin_instance.get_fieldsets(self.request, obj=list_result)
        ballot_list_result_fieldsets = (
            (None, {"fields": ("name", "candidate_result", "image", "political_colour", "parties")}),
            (_("Results"), {"fields": (("votes", "votes_perc", "seats"),)}),
            (_("Second turn"), {"fields": ("ballot_candidate_result",)}),
            (_("Elected"), {"fields": ("elected_memberships",)})
        )
        self.assertTupleEqual(tuple(fieldsets), ballot_list_result_fieldsets)


class ElectoralCandidateResultAdminTest(SimpleTestCase):
    """Tests for admin."""

    def setUp(self):
        """Initialize tests data."""
        self.admin_instance = ElectoralCandidateResultAdmin(
            model=ElectoralCandidateResult, admin_site=default_site
        )
        self.request = RequestFactory().get("/")

    def test_has_add_permission(self):
        """Test if return queryset."""
        self.assertFalse(self.admin_instance.has_add_permission(self.request))

    def test_has_change_permission(self):
        """Test if return queryset."""
        self.assertTrue(self.admin_instance.has_change_permission(self.request))

    def test_has_delete_permission(self):
        """Test if return queryset."""
        self.assertFalse(self.admin_instance.has_delete_permission(self.request))

    def test_get_fieldsets(self):
        """Test get_fieldsets."""
        result = ElectoralResult()
        candidate_result = ElectoralCandidateResult(result=result)
        fieldsets = self.admin_instance.get_fieldsets(
            self.request, obj=candidate_result
        )
        candidate_result_fieldsets = (
            (None, {"fields": ("name", "result", "is_counselor", "is_top")}),
            (_("Results"), {"fields": ("votes", "votes_perc")}),
            (_("Elected"), {"fields": ("elected_membership",)})
        )
        self.assertTupleEqual(tuple(fieldsets), candidate_result_fieldsets)
        result.ballot_votes_cast = 1
        fieldsets = self.admin_instance.get_fieldsets(
            self.request, obj=candidate_result
        )
        ballot_candidate_result_fieldsets = (
            (None, {"fields": ("name", "result", "is_counselor", "is_top")}),
            (_("Results (First Turn)"), {"fields": ("votes", "votes_perc")}),
            (
                _("Results (Second Turn)"),
                {"fields": ("ballot_votes", "ballot_votes_perc")},
            ),
            (_("Elected"), {"fields": ("elected_membership",)})
        )
        self.assertTupleEqual(tuple(fieldsets), ballot_candidate_result_fieldsets)


class ElectoralResultAdminTest(SimpleTestCase):
    """Tests for admin."""

    def setUp(self):
        """Initialize tests data."""
        self.admin_instance = ElectoralResultAdmin(
            model=ElectoralResult, admin_site=default_site
        )
        self.request = RequestFactory().get("/")

    def test_has_add_permission(self):
        """Test if return queryset."""
        self.assertFalse(self.admin_instance.has_add_permission(self.request))

    def test_has_change_permission(self):
        """Test if return queryset."""
        self.assertFalse(self.admin_instance.has_change_permission(self.request))

    def test_has_delete_permission(self):
        """Test if return queryset."""
        self.assertFalse(self.admin_instance.has_delete_permission(self.request))

    def test_get_fieldsets(self):
        """Test get_fieldsets."""
        result = ElectoralResult()
        fieldsets = self.admin_instance.get_fieldsets(self.request, obj=result)
        result_fieldsets = (
            (
                None,
                {"fields": ("__str__", "is_total", "is_valid", "registered_voters")},
            ),
            (
                _("Results"),
                {
                    "fields": (
                        ("votes_cast", "votes_cast_perc"),
                        ("blank_votes", "invalid_votes", "valid_votes"),
                    )
                },
            ),
        )
        self.assertTupleEqual(tuple(fieldsets), result_fieldsets)
        result.ballot_votes_cast = 1
        fieldsets = self.admin_instance.get_fieldsets(self.request, obj=result)
        ballot_result_fieldsets = (
            (
                None,
                {"fields": ("__str__", "is_total", "is_valid", "registered_voters")},
            ),
            (
                _("Results (First Turn)"),
                {
                    "fields": (
                        ("votes_cast", "votes_cast_perc"),
                        ("blank_votes", "invalid_votes", "valid_votes"),
                    )
                },
            ),
            (
                _("Results (Second Turn)"),
                {
                    "fields": (
                        ("ballot_votes_cast", "ballot_votes_cast_perc"),
                        (
                            "ballot_blank_votes",
                            "ballot_invalid_votes",
                            "ballot_valid_votes",
                        ),
                    )
                },
            ),
        )
        self.assertTupleEqual(tuple(fieldsets), ballot_result_fieldsets)
