"""Define elections models tests."""

from decimal import Decimal
from pathlib import Path

from django.test import SimpleTestCase, TestCase
from popolo.models import Area, KeyEvent, Organization

from project.elections.models import (
    ElectoralCandidateResult,
    ElectoralListResult,
    ElectoralResult,
    PoliticalColour,
)


class PoliticalColourTest(SimpleTestCase):
    """Define political colour model tests."""

    def test_str(self):
        """Test instance string representation."""
        politicalcolour = PoliticalColour(name="Giallo")
        self.assertEqual(str(politicalcolour), "Giallo")


class ElectoralResultTest(TestCase):
    """Define electoral result model tests."""

    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        """Initialize test data."""
        cls.event_regional_elections_1970 = KeyEvent.objects.create(
            name="Elezioni regionali 01/01/1970"
        )
        cls.area_abruzzo = Area.objects.create(
            id=8040,
            identifier=13,
            istat_classification="REG",
            name="ABRUZZO",
        )
        cls.organization_region_abruzzo = Organization.objects.create(
            id=31386,
            area=cls.area_abruzzo,
            classification="Regione",
            name="REGIONE ABRUZZO",
        )

    def test_has_second_round(self):
        """Test Electoral Result has_second_round."""
        one_round = ElectoralResult.objects.create(
            registered_voters=100,
            votes_cast=80,
            votes_cast_perc=Decimal("80"),
            invalid_votes=10,
            blank_votes=5,
            valid_votes=70,
            is_valid=True,
            is_total=True,
            electoral_event=self.event_regional_elections_1970,
            institution=self.organization_region_abruzzo,
            constituency=self.area_abruzzo,
        )
        two_round = ElectoralResult.objects.create(
            registered_voters=100,
            votes_cast=80,
            votes_cast_perc=Decimal("80"),
            invalid_votes=10,
            blank_votes=5,
            valid_votes=70,
            ballot_votes_cast=90,
            ballot_votes_cast_perc=Decimal("90"),
            ballot_invalid_votes=15,
            ballot_blank_votes=10,
            ballot_valid_votes=75,
            is_valid=True,
            is_total=True,
            electoral_event=self.event_regional_elections_1970,
            institution=self.organization_region_abruzzo,
            constituency=self.area_abruzzo,
        )
        self.assertFalse(one_round.has_second_round)
        self.assertTrue(two_round.has_second_round)

    def test_str(self):
        """Test instance string representation."""
        electoral_result = ElectoralResult(
            registered_voters=0,
            votes_cast=0,
            votes_cast_perc=Decimal("0"),
            invalid_votes=0,
            blank_votes=0,
            valid_votes=0,
            is_valid=True,
            is_total=True,
            electoral_event=self.event_regional_elections_1970,
            institution=self.organization_region_abruzzo,
            constituency=self.area_abruzzo,
        )
        self.assertEqual(
            str(electoral_result),
            "Elezioni regionali 01/01/1970 - REGIONE ABRUZZO - ABRUZZO",
        )

    def test_load_electoral_results(self):
        """Test load electoral results."""
        data_path = Path(__file__).parent / "data"
        event_args = {"start_date": "2020-01-01", "event_type": "ELE-REG"}
        key_event_qs = KeyEvent.objects.filter(**event_args)
        electoral_result_qs = ElectoralResult.objects.all()
        electoral_candidate_result_qs = ElectoralCandidateResult.objects.all()
        electoral_list_result_qs = ElectoralListResult.objects.all()
        self.assertFalse(electoral_result_qs.exists())
        self.assertFalse(electoral_candidate_result_qs.exists())
        self.assertFalse(electoral_list_result_qs.exists())
        self.assertFalse(key_event_qs.exists())
        wrong_stats = ElectoralResult.load_results(data_path / "wrong.json")
        self.assertDictEqual(
            wrong_stats,
            {'events': 0, 'results': 0, 'candidates': 0, 'lists': 0, 'errors': [
                "1 validation error for Importazione\neventi -> 0 -> tipo\n  value is not a"
                " valid enumeration member; permitted: 'ELE-EU', 'ELE-POL',"
                " 'ELE-REG', 'ELE-COM', 'ELE-PROV', 'ELE-SUP'"
                " (type=type_error.enum; enum_values=[<TipoEvento.europee: 'ELE-EU'>, "
                "<TipoEvento.politiche: 'ELE-POL'>, <TipoEvento.regionali: 'ELE-REG'>, "
                "<TipoEvento.comunali: 'ELE-COM'>, <TipoEvento.provinciali: 'ELE-PROV'>,"
                " <TipoEvento.suppletive: 'ELE-SUP'>])"]},
        )
        self.assertFalse(electoral_result_qs.exists())
        self.assertFalse(electoral_candidate_result_qs.exists())
        self.assertFalse(electoral_list_result_qs.exists())
        self.assertFalse(key_event_qs.exists())
        good_stats = ElectoralResult.load_results(data_path / "ele-reg_results.json")
        self.assertDictEqual(
            good_stats,
            {
                "candidates": 2,
                "events": 1,
                "lists": 4,
                "results": 1,
                "errors": [
                    "Area 0 does not exists.",
                    "Organization 0 does not exists.",
                ],
            },
        )
        self.assertTrue(key_event_qs.exists())
        self.assertEqual(electoral_result_qs.count(), 1)
        self.assertEqual(electoral_candidate_result_qs.count(), 2)
        self.assertEqual(electoral_list_result_qs.count(), 4)
        electoral_result_qs_values = electoral_result_qs.values(
            "registered_voters",
            "votes_cast",
            "votes_cast_perc",
            "invalid_votes",
            "blank_votes",
            "valid_votes",
            "is_valid",
            "is_total",
            "electoral_event__name",
            "institution_id",
            "constituency_id",
        )
        electoral_result_values = (
            {
                "registered_voters": 1000000,
                "votes_cast": 800000,
                "votes_cast_perc": Decimal("80.00"),
                "invalid_votes": 8000,
                "blank_votes": 8000,
                "valid_votes": 792000,
                "is_valid": True,
                "is_total": True,
                "electoral_event__name": "Regional election 01/01/2020",
                "institution_id": 31386,
                "constituency_id": 8040,
            },
        )
        self.assertCountEqual(electoral_result_qs_values, electoral_result_values)
        electoral_candidate_result_qs_values = electoral_candidate_result_qs.values(
            "name",
            "votes",
            "votes_perc",
            "is_counselor",
            "is_top",
            # "elected_membership_id",
            "result__electoral_event__name",
            "political_colour_id",
            "tmp_candidate",
        )
        electoral_candidate_result_values = (
            {
                "name": "SILONE IGNAZIO",
                "votes": 600000,
                "votes_perc": Decimal("60.00"),
                "is_counselor": False,
                "is_top": True,
                # "elected_membership_id": None,
                "result__electoral_event__name": "Regional election 01/01/2020",
                "political_colour_id": None,
                "tmp_candidate": {},
            },
            {
                "name": "D'ANNUNZIO GABRIELE",
                "votes": 500000,
                "votes_perc": Decimal("50.00"),
                "is_counselor": True,
                "is_top": False,
                # "elected_membership_id": None,
                "result__electoral_event__name": "Regional election 01/01/2020",
                "political_colour_id": None,
                "tmp_candidate": {},
            },
        )
        self.assertCountEqual(
            electoral_candidate_result_qs_values, electoral_candidate_result_values
        )
        electoral_list_result_qs_values = electoral_list_result_qs.values(
            "name",
            "image",
            "votes",
            "votes_perc",
            "seats",
            "result__electoral_event__name",
            "candidate_result__name",
            "political_colour_id",
            "parties",
            "tmp_lists",
        )
        electoral_list_result_values = (
            {
                "name": "Partito Socialista Italiano",
                "image": "https://elezionistorico.interno.gov.it/contrassegni_a/"
                         "images/4/489FE5D9A5D927567EFEB54AD37B751D.jpg",
                "votes": 350000,
                "votes_perc": Decimal("35.00"),
                "seats": 10,
                "result__electoral_event__name": "Regional election 01/01/2020",
                "candidate_result__name": "SILONE IGNAZIO",
                "political_colour_id": None,
                "parties": None,
                "tmp_lists": [],
            },
            {
                "name": "Partito Socialista Democratico Italiano",
                "image": "https://elezionistorico.interno.gov.it/contrassegni_a/"
                         "images/4/489FE5D9A5D927567EFEB54AD37B751D.jpg",
                "votes": 250000,
                "votes_perc": Decimal("25.00"),
                "seats": 7,
                "result__electoral_event__name": "Regional election 01/01/2020",
                "candidate_result__name": "SILONE IGNAZIO",
                "political_colour_id": None,
                "parties": None,
                "tmp_lists": [],
            },
            {
                "name": "Estrema destra storica",
                "image": "https://elezionistorico.interno.gov.it/contrassegni_a/"
                         "images/4/489FE5D9A5D927567EFEB54AD37B751D.jpg",
                "votes": 300000,
                "votes_perc": Decimal("30.00"),
                "seats": 9,
                "result__electoral_event__name": "Regional election 01/01/2020",
                "candidate_result__name": "D'ANNUNZIO GABRIELE",
                "political_colour_id": None,
                "parties": None,
                "tmp_lists": [],
            },
            {
                "name": "Associazione Nazionalista Italiana",
                "image": "https://elezionistorico.interno.gov.it/contrassegni_a/"
                         "images/4/489FE5D9A5D927567EFEB54AD37B751D.jpg",
                "votes": 200000,
                "votes_perc": Decimal("20.00"),
                "seats": 5,
                "result__electoral_event__name": "Regional election 01/01/2020",
                "candidate_result__name": "D'ANNUNZIO GABRIELE",
                "political_colour_id": None,
                "parties": None,
                "tmp_lists": [],
            },
        )
        self.assertCountEqual(
            electoral_list_result_qs_values, electoral_list_result_values
        )


class ElectoralListResultTest(TestCase):
    """Define electoral list result model tests."""

    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        """Initialize test data."""
        cls.event_regional_elections_1970 = KeyEvent.objects.create(
            name="Elezioni regionali 01/01/1970"
        )
        cls.area_abruzzo = Area.objects.create(
            id=8040,
            identifier=13,
            istat_classification="REG",
            name="ABRUZZO",
        )
        cls.organization_region_abruzzo = Organization.objects.create(
            id=31386,
            area=cls.area_abruzzo,
            classification="Regione",
            name="REGIONE ABRUZZO",
        )
        cls.electoral_results_abruzzo_regional_elections_1970 = ElectoralResult(
            registered_voters=0,
            votes_cast=0,
            votes_cast_perc=Decimal("0"),
            invalid_votes=0,
            blank_votes=0,
            valid_votes=0,
            is_valid=True,
            is_total=True,
            electoral_event=cls.event_regional_elections_1970,
            institution=cls.organization_region_abruzzo,
            constituency=cls.area_abruzzo,
        )

    def test_str(self):
        """Test instance string representation."""
        electoral_list_result = ElectoralListResult(
            name="Partito Socialista Italiano",
            result=self.electoral_results_abruzzo_regional_elections_1970,
        )
        self.assertEqual(
            str(electoral_list_result),
            "Partito Socialista Italiano - Elezioni regionali 01/01/1970 - "
            "REGIONE ABRUZZO - ABRUZZO",
        )


class ElectoralCandidateResultTest(TestCase):
    """Define electoral candidate result model tests."""

    maxDiff = None

    @classmethod
    def setUpTestData(cls):
        """Initialize test data."""
        cls.event_regional_elections_1970 = KeyEvent.objects.create(
            name="Elezioni regionali 01/01/1970"
        )
        cls.area_abruzzo = Area.objects.create(
            id=8040,
            identifier=13,
            istat_classification="REG",
            name="ABRUZZO",
        )
        cls.organization_region_abruzzo = Organization.objects.create(
            id=31386,
            area=cls.area_abruzzo,
            classification="Regione",
            name="REGIONE ABRUZZO",
        )
        cls.electoral_results_abruzzo_regional_elections_1970 = ElectoralResult(
            registered_voters=0,
            votes_cast=0,
            votes_cast_perc=Decimal("0"),
            invalid_votes=0,
            blank_votes=0,
            valid_votes=0,
            is_valid=True,
            is_total=True,
            electoral_event=cls.event_regional_elections_1970,
            institution=cls.organization_region_abruzzo,
            constituency=cls.area_abruzzo,
        )

    def test_str(self):
        """Test instance string representation."""
        electoral_candidate_result = ElectoralCandidateResult(
            name="SILONE IGNAZIO",
            result=self.electoral_results_abruzzo_regional_elections_1970,
        )
        self.assertEqual(
            str(electoral_candidate_result),
            "SILONE IGNAZIO - Elezioni regionali 01/01/1970 - "
            "REGIONE ABRUZZO - ABRUZZO",
        )
