"""Define elections filtersets."""

from django.utils.translation import gettext_lazy as _
from django_filters import rest_framework as filters
from django_filters.rest_framework import FilterSet, BooleanFilter
from popolo.models import KeyEvent, Organization, Area

from project.elections.models import ElectoralResult, ElectoralListResult, ElectoralCandidateResult


class AreaElectoralResultFilterSet(FilterSet):

    electoral_event__id = filters.NumberFilter(field_name="results__electoral_event__id", distinct=True)
    institution__id = filters.NumberFilter(field_name="results__institution__id", distinct=True)

    class Meta:
        """Define filterset Meta."""
        model = Area
        fields = [
            "id",
            "electoral_event__id",
            "institution__id"
        ]


class OrganizationElectoralResultFilterSet(FilterSet):

    electoral_event__id = filters.NumberFilter(field_name="results__electoral_event__id", distinct=True)
    constituency__id = filters.NumberFilter(field_name="results__constituency__id", distinct=True)

    class Meta:
        """Define filterset Meta."""
        model = Organization
        fields = [
            "id",
            "electoral_event__id",
            "constituency__id"
        ]


class KeyEventElectoralResultFilterSet(FilterSet):

    institution__id = filters.NumberFilter(field_name="results__institution__id", distinct=True)
    constituency__id = filters.NumberFilter(field_name="results__constituency__id", distinct=True)

    class Meta:
        """Define filterset Meta."""
        model = KeyEvent
        fields = [
            "id",
            "event_type",
            "institution__id",
            "constituency__id"
        ]


class ElectoralResultFilterSet(FilterSet):
    """A filterset for ElectoralResultViewSet."""

    has_second_round = BooleanFilter(
        field_name="ballot_votes_cast",
        label=_("Has second round"),
        help_text=_("Select Yes for results with second round."),
        lookup_expr="isnull",
        exclude=True,
    )

    class Meta:
        """Define filterset Meta."""
        model = ElectoralResult
        fields = [
            "electoral_event__id",
            "electoral_event__event_type",
            "has_second_round",
            "institution__id",
            "constituency__id"
        ]


class ElectoralListResultFilterSet(FilterSet):
    """A filterset for ElectoralListResultViewSet."""

    class Meta:
        """Define filterset Meta."""
        model = ElectoralListResult
        fields = []


class ElectoralCandidateResultFilterSet(FilterSet):
    """A filterset for ElectoralListResultViewSet."""

    class Meta:
        """Define filterset Meta."""
        model = ElectoralCandidateResult
        fields = []
