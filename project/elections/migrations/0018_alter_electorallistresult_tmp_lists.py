# Generated by Django 3.2 on 2021-06-04 14:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('elections', '0017_auto_20210604_1305'),
    ]

    operations = [
        migrations.AlterField(
            model_name='electorallistresult',
            name='tmp_lists',
            field=models.JSONField(blank=True, default=list, help_text='A list of electoral list names stored in the electoral_list_descr_tmp field, containing all lists connected to this electoral result.', verbose_name='connected temp lists'),
        ),
    ]
