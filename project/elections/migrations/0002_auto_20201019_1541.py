import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("elections", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="electoralcandidateresult",
            name="candidate",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="candidate_results",
                to="popolo.Person",
                verbose_name="candidate",
            ),
        ),
        migrations.AlterField(
            model_name="electoralcandidateresult",
            name="political_colour",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="candidate_results",
                to="elections.PoliticalColour",
                verbose_name="political colour",
            ),
        ),
        migrations.AlterField(
            model_name="electoralcandidateresult",
            name="result",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="candidate_results",
                to="elections.ElectoralResult",
                verbose_name="result",
            ),
        ),
        migrations.AlterField(
            model_name="electorallistresult",
            name="candidate_result",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="list_results",
                to="elections.ElectoralCandidateResult",
                verbose_name="candidate result",
            ),
        ),
        migrations.AlterField(
            model_name="electorallistresult",
            name="party",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="list_results",
                to="popolo.Organization",
                verbose_name="party",
            ),
        ),
        migrations.AlterField(
            model_name="electorallistresult",
            name="political_colour",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="list_results",
                to="elections.PoliticalColour",
                verbose_name="political colour",
            ),
        ),
        migrations.AlterField(
            model_name="electorallistresult",
            name="result",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="list_results",
                to="elections.ElectoralResult",
                verbose_name="result",
            ),
        ),
        migrations.AlterField(
            model_name="electoralresult",
            name="constituency",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="results",
                to="popolo.Area",
                verbose_name="costituency",
            ),
        ),
        migrations.AlterField(
            model_name="electoralresult",
            name="electoral_event",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="results",
                to="popolo.KeyEvent",
                verbose_name="electoral event",
            ),
        ),
        migrations.AlterField(
            model_name="electoralresult",
            name="institution",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                related_name="results",
                to="popolo.Organization",
                verbose_name="institution",
            ),
        ),
    ]
