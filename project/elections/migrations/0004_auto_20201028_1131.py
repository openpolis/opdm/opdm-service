import django.contrib.postgres.fields.jsonb
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("elections", "0003_auto_20201026_1116"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="electoralresult",
            name="abstentions",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="abstentions_perc",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="blank_votes_perc",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="invalid_votes_perc",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="list_votes",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="list_votes_perc",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="registered_voters_perc",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="turnout",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="turnout_perc",
        ),
        migrations.RemoveField(
            model_name="electoralresult",
            name="valid_votes_perc",
        ),
        migrations.AddField(
            model_name="electoralcandidateresult",
            name="name",
            field=models.CharField(default="", max_length=512, verbose_name="name"),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="electorallistresult",
            name="image",
            field=models.URLField(default="", verbose_name="image"),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="electorallistresult",
            name="tmp_list",
            field=django.contrib.postgres.fields.jsonb.JSONField(
                blank=True, default=dict, verbose_name="temp list"
            ),
        ),
        migrations.AlterField(
            model_name="electoralcandidateresult",
            name="is_elected",
            field=models.BooleanField(blank=True, null=True, verbose_name="elected"),
        ),
        migrations.AlterField(
            model_name="electoralcandidateresult",
            name="is_top",
            field=models.BooleanField(blank=True, null=True, verbose_name="top"),
        ),
        migrations.AlterField(
            model_name="electoralcandidateresult",
            name="votes",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="votes"
            ),
        ),
        migrations.AlterField(
            model_name="electoralcandidateresult",
            name="votes_perc",
            field=models.DecimalField(
                blank=True,
                decimal_places=2,
                max_digits=5,
                null=True,
                verbose_name="votes percent",
            ),
        ),
        migrations.AlterField(
            model_name="electorallistresult",
            name="name",
            field=models.CharField(max_length=512, verbose_name="name"),
        ),
        migrations.AlterField(
            model_name="electoralresult",
            name="constituency",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="popolo.Area",
                verbose_name="constituency",
            ),
        ),
        migrations.AlterField(
            model_name="electoralresult",
            name="is_total",
            field=models.BooleanField(default=True, verbose_name="total"),
        ),
    ]
