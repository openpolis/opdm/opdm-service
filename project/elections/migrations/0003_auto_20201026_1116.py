import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("elections", "0002_auto_20201019_1541"),
    ]

    operations = [
        migrations.AddField(
            model_name="electoralresult",
            name="is_total",
            field=models.BooleanField(default=False, verbose_name="total"),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name="electorallistresult",
            name="candidate_result",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="list_results",
                to="elections.ElectoralCandidateResult",
                verbose_name="candidate result",
            ),
        ),
        migrations.AlterField(
            model_name="electoralresult",
            name="constituency",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                to="popolo.Area",
                verbose_name="costituency",
            ),
        ),
    ]
