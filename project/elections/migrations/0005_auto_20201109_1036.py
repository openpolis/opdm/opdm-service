from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("elections", "0004_auto_20201028_1131"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="electoralcandidateresult",
            name="is_elected",
        ),
        migrations.RemoveField(
            model_name="electorallistresult",
            name="seats_perc",
        ),
        migrations.AddField(
            model_name="electoralcandidateresult",
            name="is_counselor",
            field=models.BooleanField(blank=True, null=True, verbose_name="counselor"),
        ),
    ]
