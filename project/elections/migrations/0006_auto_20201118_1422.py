import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("elections", "0005_auto_20201109_1036"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="electoralresult",
            name="first_round_result",
        ),
        migrations.AddField(
            model_name="electoralcandidateresult",
            name="ballot_votes",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="votes (second round)"
            ),
        ),
        migrations.AddField(
            model_name="electoralcandidateresult",
            name="ballot_votes_perc",
            field=models.DecimalField(
                blank=True,
                decimal_places=2,
                max_digits=5,
                null=True,
                verbose_name="votes percent (second round)",
            ),
        ),
        migrations.AddField(
            model_name="electorallistresult",
            name="ballot_candidate_result",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.PROTECT,
                related_name="ballot_list_results",
                to="elections.ElectoralCandidateResult",
                verbose_name="candidate result (second round)",
            ),
        ),
        migrations.AddField(
            model_name="electoralresult",
            name="ballot_blank_votes",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="blank votes (second round)"
            ),
        ),
        migrations.AddField(
            model_name="electoralresult",
            name="ballot_invalid_votes",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="invalid votes (second round)"
            ),
        ),
        migrations.AddField(
            model_name="electoralresult",
            name="ballot_valid_votes",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="valid votes (second round)"
            ),
        ),
        migrations.AddField(
            model_name="electoralresult",
            name="ballot_votes_cast",
            field=models.PositiveIntegerField(
                blank=True, null=True, verbose_name="votes cast (second round)"
            ),
        ),
        migrations.AddField(
            model_name="electoralresult",
            name="ballot_votes_cast_perc",
            field=models.DecimalField(
                blank=True,
                decimal_places=2,
                max_digits=5,
                null=True,
                verbose_name="votes cast percent (second round)",
            ),
        ),
    ]
