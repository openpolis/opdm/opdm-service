# Generated by Django 3.2.16 on 2024-03-14 11:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        # ('popolo', '0005_alter_keyevent_event_type'),
        ('elections', '0031_auto_20240310_2228'),
    ]

    operations = [
        migrations.AlterField(
            model_name='electoralcandidateresult',
            name='membership',
            field=models.ForeignKey(blank=True, help_text='The membership linked to this election result', null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='electoral_candidate_results', to='popolo.membership', verbose_name='candidate membership'),
        ),
        migrations.DeleteModel(
            name='AreaMapConstituency',
        ),
    ]
