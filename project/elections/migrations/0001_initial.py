import django.contrib.postgres.fields.jsonb
import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ("popolo", "0001_initial"),
    ]

    operations = [
        migrations.CreateModel(
            name="ElectoralCandidateResult",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="creation time"
                    ),
                ),
                (
                    "updated_at",
                    models.DateTimeField(
                        auto_now=True, verbose_name="last modification time"
                    ),
                ),
                ("votes", models.PositiveIntegerField(verbose_name="votes")),
                (
                    "votes_perc",
                    models.DecimalField(
                        decimal_places=2, max_digits=5, verbose_name="votes percent"
                    ),
                ),
                ("is_elected", models.BooleanField(verbose_name="elected")),
                ("is_top", models.BooleanField(verbose_name="top")),
                (
                    "tmp_candidate",
                    django.contrib.postgres.fields.jsonb.JSONField(
                        blank=True, default=dict, verbose_name="temp candidate"
                    ),
                ),
                (
                    "candidate",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="popolo.Person",
                        verbose_name="candidate",
                    ),
                ),
            ],
            options={
                "verbose_name": "Electoral candidate result",
                "verbose_name_plural": "Electoral candidate results",
            },
        ),
        migrations.CreateModel(
            name="PoliticalColour",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="creation time"
                    ),
                ),
                (
                    "updated_at",
                    models.DateTimeField(
                        auto_now=True, verbose_name="last modification time"
                    ),
                ),
                (
                    "name",
                    models.CharField(
                        help_text="Definition of the political colour.",
                        max_length=255,
                        unique=True,
                        verbose_name="name",
                    ),
                ),
            ],
            options={
                "verbose_name": "Political colour",
                "verbose_name_plural": "Political colours",
            },
        ),
        migrations.CreateModel(
            name="ElectoralResult",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="creation time"
                    ),
                ),
                (
                    "updated_at",
                    models.DateTimeField(
                        auto_now=True, verbose_name="last modification time"
                    ),
                ),
                (
                    "registered_voters",
                    models.PositiveIntegerField(verbose_name="registered voters"),
                ),
                (
                    "registered_voters_perc",
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=5,
                        verbose_name="registered voters percent",
                    ),
                ),
                ("votes_cast", models.PositiveIntegerField(verbose_name="votes cast")),
                (
                    "votes_cast_perc",
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=5,
                        verbose_name="votes cast percent",
                    ),
                ),
                (
                    "invalid_votes",
                    models.PositiveIntegerField(verbose_name="invalid votes"),
                ),
                (
                    "invalid_votes_perc",
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=5,
                        verbose_name="invalid votes percent",
                    ),
                ),
                (
                    "blank_votes",
                    models.PositiveIntegerField(verbose_name="blank votes"),
                ),
                (
                    "blank_votes_perc",
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=5,
                        verbose_name="blank votes percent",
                    ),
                ),
                ("list_votes", models.PositiveIntegerField(verbose_name="list votes")),
                (
                    "list_votes_perc",
                    models.DecimalField(
                        decimal_places=2, max_digits=5, verbose_name="list percent"
                    ),
                ),
                (
                    "valid_votes",
                    models.PositiveIntegerField(verbose_name="valid votes"),
                ),
                (
                    "valid_votes_perc",
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=5,
                        verbose_name="valid votes percent",
                    ),
                ),
                ("turnout", models.PositiveIntegerField(verbose_name="turnout")),
                (
                    "turnout_perc",
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=5,
                        verbose_name="turnout votes percent",
                    ),
                ),
                (
                    "abstentions",
                    models.PositiveIntegerField(verbose_name="abstentions"),
                ),
                (
                    "abstentions_perc",
                    models.DecimalField(
                        decimal_places=2,
                        max_digits=5,
                        verbose_name="abstentions percent",
                    ),
                ),
                ("is_valid", models.BooleanField(verbose_name="valid")),
                (
                    "constituency",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="popolo.Area",
                        verbose_name="costituency",
                    ),
                ),
                (
                    "electoral_event",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="popolo.KeyEvent",
                        verbose_name="electoral event",
                    ),
                ),
                (
                    "first_round_result",
                    models.OneToOneField(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.PROTECT,
                        to="elections.ElectoralResult",
                    ),
                ),
                (
                    "institution",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="popolo.Organization",
                        verbose_name="institution",
                    ),
                ),
            ],
            options={
                "verbose_name": "Electoral result",
                "verbose_name_plural": "Electoral results",
            },
        ),
        migrations.CreateModel(
            name="ElectoralListResult",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "created_at",
                    models.DateTimeField(
                        auto_now_add=True, verbose_name="creation time"
                    ),
                ),
                (
                    "updated_at",
                    models.DateTimeField(
                        auto_now=True, verbose_name="last modification time"
                    ),
                ),
                (
                    "name",
                    models.CharField(max_length=255, unique=True, verbose_name="name"),
                ),
                (
                    "votes",
                    models.PositiveIntegerField(
                        blank=True, null=True, verbose_name="votes"
                    ),
                ),
                (
                    "votes_perc",
                    models.DecimalField(
                        blank=True,
                        decimal_places=2,
                        max_digits=5,
                        null=True,
                        verbose_name="votes percent",
                    ),
                ),
                (
                    "seats",
                    models.PositiveIntegerField(
                        blank=True, null=True, verbose_name="seats"
                    ),
                ),
                (
                    "seats_perc",
                    models.DecimalField(
                        blank=True,
                        decimal_places=2,
                        max_digits=5,
                        null=True,
                        verbose_name="seats percent",
                    ),
                ),
                (
                    "candidate_result",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="elections.ElectoralCandidateResult",
                        verbose_name="candidate result",
                    ),
                ),
                (
                    "party",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="popolo.Organization",
                    ),
                ),
                (
                    "political_colour",
                    models.ForeignKey(
                        blank=True,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="elections.PoliticalColour",
                        verbose_name="political colour",
                    ),
                ),
                (
                    "result",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.PROTECT,
                        to="elections.ElectoralResult",
                        verbose_name="result",
                    ),
                ),
            ],
            options={
                "verbose_name": "Electoral list result",
                "verbose_name_plural": "Electoral list results",
            },
        ),
        migrations.AddField(
            model_name="electoralcandidateresult",
            name="political_colour",
            field=models.ForeignKey(
                blank=True,
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                to="elections.PoliticalColour",
                verbose_name="political colour",
            ),
        ),
        migrations.AddField(
            model_name="electoralcandidateresult",
            name="result",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.PROTECT,
                to="elections.ElectoralResult",
                verbose_name="result",
            ),
        ),
    ]
