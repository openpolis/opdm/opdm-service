# Generated by Django 3.2 on 2021-04-08 10:24

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('elections', '0009_electoralmembership'),
    ]

    operations = [
        migrations.AlterField(
            model_name='electoralcandidateresult',
            name='name',
            field=models.CharField(db_index=True, max_length=512, verbose_name='name'),
        ),
        migrations.AlterField(
            model_name='electorallistresult',
            name='name',
            field=models.CharField(db_index=True, max_length=512, verbose_name='name'),
        ),
    ]
