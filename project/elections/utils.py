from typing import List, Union
from datetime import datetime
import requests
import unidecode
from bs4 import BeautifulSoup
USER_AGENT = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) "
              "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36")
from pathlib import Path
import environ
from django.core.cache import caches
cache = caches['default']
db_cache = caches['db']
env = environ.Env()
dot_env_file = Path(".env")
if dot_env_file.is_file():
    env.read_env(str(dot_env_file))

import re
OPDM_URL = env('OPDM_URL', default="https://serivce.opdm.openpolis.it")
OPDM_USER = env('OPDM_USER', default="opdm_user")
OPDM_PASSWORD = env('OPDM_PASSWORD', default="change-me")
USER_AGENT = env('USER_AGENT', default=(
    "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) "
    "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36"
))
from requests.auth import HTTPBasicAuth

def get_opdm_organizations() -> list:
    """Fetch all organizations of type 1205 (national assemblies)

    :return: a list
    """
    r = requests.get(
        f'{OPDM_URL}/organizations',
        params={
            'classification_id': 1205,
        },
        auth=HTTPBasicAuth(OPDM_USER, OPDM_PASSWORD),
        headers={'User-Agent': USER_AGENT}
    )
    jr = r.json()
    if jr['count'] == 0:
        raise Exception("Could not find any organization.")

    if r.status_code != 200:
        raise Exception(f"{r.status_code} while fetching organizations.")

    print(f"\t{r.request.url}")
    return [
        {
            'id': o['id'],
            'name': o['name'],
            'identifier': o['identifier'],
            'founding_date': o['founding_date'],
            'dissolution_date': o['dissolution_date']
        }
        for o in jr['results']
    ]


def get_opdm_areas() -> list:
    """Fetch all areas of type REG, PROV, NAZ, ELECT_*, to associate electoral results to constituencies
    """
    # if db_cache.get("election_areas"):
    #     return db_cache.get("election_areas")
    areas = []
    url = f'{OPDM_URL}/areas?page_size=200' \
        f'&classification=CONT,PCL,ADM1,ADM2,ADM3,ELECT_CIRC,ELECT_COLL,ELECT_COLL_ESTERO,ELECT_RIP'
    while True:
        r = requests.get(
            url,
            auth=HTTPBasicAuth(OPDM_USER, OPDM_PASSWORD),
            headers={'User-Agent': USER_AGENT}
        )
        print(f"\t{url}")
        if r.status_code != 200:
            raise Exception(f"Error while fetching areas")
        jr = r.json()
        if jr['count'] == 0:
            raise Exception(f"ERR: Could not find any area")

        url = jr['next']

        areas.extend([
            {
                'id': a['id'],
                'name': a['name'],
                'identifier': a['identifier'],
                'classification': a['classification'],
                'start_date': a['start_date'],
                'end_date': a['end_date'],
                'other_names': [x['name'] for x in a['other_names'] if len(x) > 0],
                'parent': a['parent']['name'] if a['parent'] is not None else None,
                'former_parents': [x['area']['name'] for x in a['former_parents'] if len(x) > 0]
            }
            for a in jr['results']
        ])

        if url is None:
            break
    db_cache.set("election_areas", areas,timeout=60*60*24*30)
    return areas


def clean_comune(comune):
    # if comune.upper() in ['CERRINA', 'FUBINE']:
    #     comune += ' MONFERRATO'
    if comune.upper() in ['VIALE']:
        comune += ' D\'ASTI'
    if comune.upper() == 'PUEGNAGO DEL GARDA':
        comune = 'PUEGNAGO SUL GARDA'
    if comune.upper() == 'COSIO DI ARROSCIA':
        comune = 'COSIO D\'ARROSCIA'
    if comune.upper() == 'IOLANDA DI SAVOIA':
        comune = 'JOLANDA DI SAVOIA'
    if comune.upper() == 'SAN DORLIGO DELLA VALLE-DOLINA':
        comune = 'SAN DORLIGO DELLA VALLE'
    if comune.upper() == 'MONTEBELLO IONICO':
        comune = 'MONTEBELLO JONICO'
    if comune.upper() == 'San Giovanni di FassaSèn Jan'.upper():
        comune = 'SAN GIOVANNI DI FASSA'
    if comune.upper() == 'NEGRAR DI VALPOLICELLA':
        comune = 'NEGRAR'
    comune = comune.split('/')[0]
    comune = comune.replace('\'', '')
    comune = unidecode.unidecode(comune)
    comune = comune.replace('-', ' ')
    comune = re.sub('[ ]+', ' ', comune).strip()
    return comune

def clean_provincia(provincia):
    provincia = provincia.replace('\'', '')
    provincia = provincia.split('/')[0]
    provincia = unidecode.unidecode(provincia)
    provincia = provincia.replace('-', ' ')
    if provincia.upper() in ['Valle dAosta'.upper()]:
        provincia = 'AOSTA'
    if provincia.upper() == 'REGGIO DI CALABRIA':
        provincia = 'REGGIO CALABRIA'
    if provincia.upper() == 'REGGIO NELL EMILIA':
        provincia = 'REGGIO EMILIA'
    if provincia.upper() == 'REGGIO NELLEMILIA':
        provincia = 'REGGIO EMILIA'

    return provincia


def clean_regione(regione):
    regione = regione.replace('\'', '')
    regione = regione.split('/')[0]
    regione = unidecode.unidecode(regione)
    regione = regione.replace('-', ' ')
    return regione


def url_links_matrioska(url, level, urls=None, pol=False):
    if urls is None:
        urls = []
    page = requests.get(
        url,
        headers={"User-Agent": USER_AGENT}
    )

    area_soup = BeautifulSoup(page.text, "lxml")
    tree = area_soup.find_all('label', {'class': "select-des"})
    tree = [x.string for x in tree]
    missing_circ = ['Data', 'Area', 'Regione', 'Provincia', 'Comune'] == tree
    count_levels = len(area_soup.find_all('select', {'name': re.compile(r'sel_sezione\d')})) + 1
    area_panels = area_soup.find(attrs={"name": f"sel_sezione{level}"})  # findAll('div', {'class': 'sezione_panel'})
    if pol:
        area_panels = area_soup.find(attrs={"name": f"sel_aree"})
    onchange = area_panels.get("onchange").split(',')
    first_part = onchange[0]
    regex = r"carica_pagina\('(.*)'"
    new_path = re.findall(regex, first_part)[0] + "&" + onchange[1].replace('\'', '') + '='

    sub_area = area_panels.find_all("option")
    for area_panel in sub_area:
        if 'Scegli' not in area_panel.text.strip():
            new_url = new_path + area_panel.get("value").replace('-', '&').replace(f'lev{level-1}', f'lev{level-1}=')
            if area_panels.get("id") == 'sel_aree':
                new_url = new_url.replace(f'levsut{level-1}', f'levsut{level-1}=').replace("msS",
                                                                                           "ms=S")\
                    .replace("tpeA", "tpe=A")
            new_url = 'https://elezionistorico.interno.gov.it/' + new_url
            urls.append(new_url)
            if next(area_panels.previous_siblings).string == 'Provincia' and missing_circ:
                urls.append(new_url)
            print(level*5*' '+area_panel.text)
            if level < count_levels:
                url_links_matrioska(new_url, level+1, urls)
    return urls


def get_comune_from_url(url):
    page = requests.get(
        url,
        headers={"User-Agent": USER_AGENT}
    )
    soup = BeautifulSoup(page.text, "lxml")
    area = soup.find("h3")
    valArea = None
    valCircoscrizione = None
    valPluri = None
    valCollegio = None
    valProvincia = None
    valComune = None
    if hasattr(area, 'contents'):
        for k, a in enumerate(area.contents):
            if k % 2 == 0:
                if a.strip().startswith('Area'):
                    valArea = a.replace('Area', '').replace("(escl. Valle d'Aosta e Trentino-Alto Adige)", "").strip()
                elif (
                    a.strip().startswith('Circoscrizione') or
                    a.strip().startswith('Regione') or
                    a.strip().startswith('Ripartizione')
                ):
                    valCircoscrizione = a.replace('Circoscrizione', '') \
                        .replace('Regione', '') \
                        .replace('Ripartizione', '').strip()
                elif (a.strip().startswith('Collegio Plurinominale') or
                      a.strip().startswith('Nazione')):
                    valPluri = a.replace('Collegio Plurinominale', '') \
                        .replace('Nazione', '').strip()
                elif a.strip().startswith('Collegio'):
                    valCollegio = a.replace('Collegio', '').strip()
                elif a.strip().startswith('Provincia'):
                    valProvincia = a.replace('Provincia', '').strip()
                elif a.strip().startswith('Comune'):
                    valComune = a.replace('Comune', '').strip()
    return valComune, valProvincia, valPluri, valCollegio


def get_opdm_data_from_context(opdm_organizations: List, opdm_areas: List, **context: Union[str, datetime]) -> tuple:
    """Torna area_id e istituzione_id, partendo da dati di contesto.

    :param opdm_organizations: Lista delle legislature
    :param opdm_areas: List delle constituency
    :param context: un dict contenente i parametri di contesto
      tipo_elsezione e data_elezione sono obbligatori
      gli altri non sono obbligatori
    :return:
    """
    data_elezione = context['data_elezione']

    area = context.get('area', None)
    circoscrizione = context.get('circoscrizione', None)
    provincia = context.get('provincia', None)
    regione = context.get('regione', None)
    comune = context.get('comune', None)
    if comune == 'JONADI':
        comune='IONADI'
    #
    # Look up among organizations, to find the correct institution
    # National politicians are bound to instance of the parliament (legislature)
    #
    try:
        istituzione_id = next(
            filter(
                lambda o:
                   regione in o['name']
                ,
                opdm_organizations
            )
        )['id']
    except:
        print(f'missing institution {data_elezione} {area} {circoscrizione} {provincia} {regione} {comune}')
        istituzione_id = None

    #
    # Look up among areas, to find a constituency.
    # When no constituency is passed, it is assumed that the record pertains to the whole nation
    # (national result)
    #
    if comune is not None:
        def area_lookup(a):
            return (a['classification'] == 'ADM3' and (clean_comune(a['name']).lower() == clean_comune(comune).lower()
                                                       or clean_comune(comune).lower() in
                                                       [clean_comune(x).lower() for x in a['other_names']])
                    and (clean_provincia(a['parent']).lower() == clean_provincia(provincia).lower()
                         or clean_provincia(provincia).lower() in [clean_provincia(x).lower() for x in a['former_parents']])
                    and datetime.strptime((a['start_date'] or '1900-01-01'), '%Y-%m-%d') <= data_elezione
                    < datetime.strptime((a['end_date'] or '2100-12-31'), '%Y-%m-%d'))
    elif provincia is not None:
        def area_lookup(a):
            return a['classification'] == 'ADM2' and clean_provincia(a['name']).lower() == clean_provincia(provincia)\
                .lower()
    elif circoscrizione is not None:

        def area_lookup(a):
            return a['classification'] == 'ELECT_CIRC_REG' and clean_provincia(a['name']).lower() == clean_provincia(circoscrizione).lower()
    elif regione is not None:
        def area_lookup(a):
            return a['classification'] == 'ADM1' and clean_regione(a['name']).lower() == clean_regione(regione).lower()



    elif area is not None:
        if area.lower() == 'italia':
            def area_lookup(a):
                return a['name'].lower() == area.lower()
        elif area.lower() == 'estero':
            def area_lookup(a):
                return a['name'].lower() == 'europa'
        else:
            def area_lookup():
                return False
    else:
        def area_lookup():
            return False

    try:
        area_id = next(
            filter(area_lookup, opdm_areas)
        )['id']
    except StopIteration:
        print(f'missing area {data_elezione} {area} {circoscrizione} {provincia} {regione} {comune}')
        area_id = None

    return area_id, istituzione_id
