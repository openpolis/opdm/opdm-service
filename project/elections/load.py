"""Load elections module."""

from datetime import date
from decimal import Decimal
from enum import Enum
from typing import List, Optional, Union, Tuple

from pydantic import BaseModel, HttpUrl


class SubCandidati(BaseModel):
    """Lista candidati/eletti per singola lista elettorale."""

    candidati: Optional[str]
    data_di_nascita: Optional[str]
    luogo_di_nascita: Optional[str]
    preferenze: Optional[int]
    eletto: Optional[str]


class Lista(BaseModel):
    """Risultato lista elettorale."""

    candidato: Optional[str]
    candidato_ballot: Optional[str]
    denominazione: str
    seggi: Optional[int]
    simbolo: Optional[HttpUrl]
    perc_voti: Optional[Decimal]
    voti: Optional[int]
    candidati_preferenze: Optional[List[SubCandidati]]


class Candidato(BaseModel):
    """Risultato candidato."""

    nome: str
    voti: Optional[int]
    voti_ballot: Optional[int]
    perc_voti: Optional[Decimal]
    perc_voti_ballot: Optional[Decimal]
    eletto_chief: bool
    eletto_consigliere: bool
    liste: List[Lista]
    name_listino: Optional[str]
    listino: Optional[List[SubCandidati]]


class Risultato(BaseModel):
    """Risultato elettorale."""

    area: Optional[int]
    regione: Optional[str]
    circoscrizione: Optional[str]
    provincia: Optional[str]
    comune: Optional[str]
    tipo_elezione: Optional[str]
    bianche: Optional[int]
    bianche_ballot: Optional[int]
    elettori: int
    elezione_valida: bool
    fonte: HttpUrl
    istituzione: int
    nulle: Optional[int]
    nulle_ballot: Optional[int]
    perc_votanti: Optional[Decimal]
    perc_votanti_ballot: Optional[Decimal]
    totale: bool
    votanti: Optional[int]
    votanti_ballot: Optional[int]
    candidati: Optional[List[Candidato]]
    liste: Optional[List[Lista]]


class TipoEvento(str, Enum):
    """Tipo evento elettorale."""

    europee = "ELE-EU"
    politiche = "ELE-POL"
    regionali = "ELE-REG"
    comunali = "ELE-COM"
    provinciali = 'ELE-PROV'
    suppletive = 'ELE-SUP'


class Evento(BaseModel):
    """Evento elettorale."""

    data: date
    tipo: TipoEvento
    risultati: List[Risultato]


class Importazione(BaseModel):
    """Importazione eventi elettorali."""

    eventi: List[Evento]


def load_json_items(path: str) -> Tuple[Union[Importazione, None], str]:
    """Load elections from a file."""
    try:
        importazione = Importazione.parse_file(path)
    except Exception as e:
        return None, str(e)
    else:
        return importazione, ""
