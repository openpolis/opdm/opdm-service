"""Define elections admins."""
from copy import deepcopy

from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from project.elections.models import (
    ElectoralCandidateResult,
    ElectoralListResult,
    ElectoralResult,
    PoliticalColour,
)


@admin.register(PoliticalColour)
class PoliticalColourAdmin(admin.ModelAdmin):
    """Define political colour admin."""

    list_display = ("name",)
    ordering = ("name", "pk")


@admin.register(ElectoralListResult)
class ElectoralListResultAdmin(admin.ModelAdmin):
    """Define electoral list result admin."""

    @admin.display()
    def parties_list(self, obj):
        return ",".join(obj.parties.values_list('name', flat=True))

    autocomplete_fields = (
        "candidate_result",
        "result",
        "parties",
        "elected_memberships"
    )
    fieldsets = (
        (None, {"fields": ("name", "candidate_result", "image", "political_colour", "parties")}),
        (_("Results"), {"fields": (("votes", "votes_perc", "seats"),)}),
        (_("Second turn"), {"fields": ("ballot_candidate_result",)}),
        (_("Elected"), {"fields": ("elected_memberships",)})
    )
    list_editable = ("political_colour", )
    list_display = (
        "name",
        "result",
        "political_colour",
        "parties_list",
        "votes",
        "votes_perc",
        "seats",
    )
    list_filter = (
        ("political_colour", admin.RelatedOnlyFieldListFilter),
    )
    autocomplete_list_filter = (
        'result', 'parties', 'elected_memberships'
    )
    ordering = ("result", "name", "pk")
    search_fields = ("name", "result__institution__name", )
    readonly_fields = ("name", "candidate_result", "image", "votes", "votes_perc", "seats", "ballot_candidate_result")
    list_select_related = (
        "political_colour", "result", "result__constituency", "result__institution", "result__electoral_event",
    )
    list_per_page = 25

    def formfield_for_dbfield(self, db_field, **kwargs):
        request = kwargs['request']
        formfield = super().formfield_for_dbfield(db_field, **kwargs)

        if db_field.name == 'political_colour':
            choices = getattr(request, '_political_colour_choices_cache', None)
            if choices is None:
                request._political_colour_choices_cache = choices = list(formfield.choices)
            formfield.choices = choices

        return formfield

    def has_add_permission(self, request):
        """Inhibit object add."""
        return False

    def has_change_permission(self, request, obj=None):
        """Inhibit object change."""
        return True

    def has_delete_permission(self, request, obj=None):
        """Inhibit object delete."""
        return False

    def get_fieldsets(self, request, obj=None):
        """Return fieldset."""
        fieldsets = super().get_fieldsets(request, obj=None)
        if not getattr(obj.result, "has_second_round", False):
            fieldsdict = dict(deepcopy(fieldsets))
            del fieldsdict[_("Second turn")]
            return fieldsdict.items()
        return fieldsets


class ElectoralListResultInline(admin.TabularInline):
    """Define ElectoralListResult inline."""

    autocomplete_fields = (
        "candidate_result",
        "result",
    )
    can_delete = False
    extra = 0
    fields = (
        "name",
        "result",
        "image",
        "votes",
        "votes_perc",
        "seats",
        "political_colour",
    )
    fk_name = "candidate_result"
    model = ElectoralListResult
    show_change_link = True


@admin.register(ElectoralCandidateResult)
class ElectoralCandidateResultAdmin(admin.ModelAdmin):
    """Define electoral candidate result admin."""

    autocomplete_fields = (
        "result",
        # "elected_membership",
    )
    fieldsets = (
        (None, {"fields": ("name", "result", "is_counselor", "is_top")}),
        (_("Results (First Turn)"), {"fields": ("votes", "votes_perc")}),
        (_("Results (Second Turn)"), {"fields": ("ballot_votes", "ballot_votes_perc")}),
        (_("Elected"), {"fields": ("elected_membership",)})
    )
    inlines = (ElectoralListResultInline,)
    list_display = (
        "name",
        "result",
        "votes",
        "votes_perc",
        "is_counselor",
        "is_top",
        "political_colour",
    )
    list_editable = ('political_colour', )
    list_filter = (
        "is_counselor",
        "is_top",
        ("political_colour", admin.RelatedOnlyFieldListFilter),
        ("result__electoral_event", admin.RelatedOnlyFieldListFilter),

    )
    ordering = ("result", "name", "pk")
    search_fields = ("name",)

    def has_add_permission(self, request):
        """Inhibit object add."""
        return False

    def has_change_permission(self, request, obj=None):
        """Inhibit object change."""
        return True

    def has_delete_permission(self, request, obj=None):
        """Inhibit object delete."""
        return False

    def get_fieldsets(self, request, obj=None):
        """Return fieldset."""
        fieldsets = super().get_fieldsets(request, obj=None)
        if not getattr(obj.result, "has_second_round", False):
            fieldsdict = dict(deepcopy(fieldsets))
            del fieldsdict[_("Results (Second Turn)")]
            del fieldsdict[_("Elected")]
            fieldsdict[_("Results")] = fieldsdict.pop(_("Results (First Turn)"))
            fieldsdict[_("Elected")] = {"fields": ("elected_membership",)}
            return fieldsdict.items()
        return fieldsets


class ElectoralCandidateResultInline(admin.TabularInline):
    """Define ElectoralCandidateResult inline."""

    autocomplete_fields = (
        "result",
    )
    can_delete = False
    extra = 0
    fields = (
        "name",
        "result",
        "votes",
        "votes_perc",
        "is_counselor",
        "is_top",
        "political_colour",
    )
    model = ElectoralCandidateResult
    show_change_link = True


@admin.register(ElectoralResult)
class ElectoralResultAdmin(admin.ModelAdmin):
    """Define electoral result admin."""

    autocomplete_fields = (
        "institution",
        "constituency",
    )
    fieldsets = (
        (None, {"fields": ("__str__", "is_total", "is_valid", "registered_voters")}),
        (
            _("Results (First Turn)"),
            {
                "fields": (
                    ("votes_cast", "votes_cast_perc"),
                    ("blank_votes", "invalid_votes", "valid_votes"),
                )
            },
        ),
        (
            _("Results (Second Turn)"),
            {
                "fields": (
                    ("ballot_votes_cast", "ballot_votes_cast_perc"),
                    (
                        "ballot_blank_votes",
                        "ballot_invalid_votes",
                        "ballot_valid_votes",
                    ),
                )
            },
        ),
    )
    inlines = (ElectoralCandidateResultInline,)
    list_display = (
        "electoral_event",
        "institution",
        "constituency",
        "registered_voters",
        "votes_cast",
        "votes_cast_perc",
        "invalid_votes",
        "blank_votes",
        "valid_votes",
        "is_valid",
        "is_total",
    )
    list_filter = (
        "is_valid",
        "is_total",
        ("electoral_event", admin.RelatedOnlyFieldListFilter),
        # ("institution", admin.RelatedOnlyFieldListFilter),
        # ("constituency", admin.RelatedOnlyFieldListFilter),
    )
    ordering = ("electoral_event", "pk")
    readonly_fields = ("__str__",)
    search_fields = (
        "electoral_event__name",
        "institution__name",
        "constituency__name",
    )

    def has_add_permission(self, request):
        """Inhibit object add."""
        return False

    def has_change_permission(self, request, obj=None):
        """Inhibit object change."""
        return False

    def has_delete_permission(self, request, obj=None):
        """Inhibit object delete."""
        return False

    def get_fieldsets(self, request, obj=None):
        """Return fieldset."""
        fieldsets = super().get_fieldsets(request, obj=None)
        if not getattr(obj, "has_second_round", False):
            fieldsdict = dict(deepcopy(fieldsets))
            del fieldsdict[_("Results (Second Turn)")]
            fieldsdict[_("Results")] = fieldsdict.pop(_("Results (First Turn)"))
            return fieldsdict.items()
        return fieldsets
