"""Define elections serializers."""
from popolo.models import Organization, Membership
from rest_framework import serializers

from api_v1.serializers import KeyEventInlineSerializer, OrganizationInlineSerializer, AreaInlineSerializer
from .models import (
    ElectoralCandidateResult,
    ElectoralListResult,
    ElectoralResult,
    PoliticalColour,
)


class PoliticalColourSerializer(serializers.ModelSerializer):
    """A serializer for PoliticalColour model."""

    class Meta:
        """Define serializer Meta."""

        model = PoliticalColour
        fields = (
            "id",
            "name",
        )


class ElectoralResultSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for ElectoralResult model."""

    electoral_event = KeyEventInlineSerializer(many=False)
    institution = OrganizationInlineSerializer(many=False)
    constituency = AreaInlineSerializer(many=False)

    class Meta:
        """Define serializer Meta."""

        model = ElectoralResult
        ref_name = "Electoral Result"
        fields = (
            "id",
            "url",
            "registered_voters",
            "votes_cast",
            "votes_cast_perc",
            "invalid_votes",
            "blank_votes",
            "valid_votes",
            "is_valid",
            "is_total",
            "electoral_event",
            "institution",
            "constituency",
            "has_second_round",
        )


class ElectoralCandidateResultSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for ElectoralCandidateResult model."""
    # elected_membership = MembershipInlineSerializer(many=True)

    class Meta:
        """Define serializer Meta."""

        model = ElectoralCandidateResult
        fields = (
            "id",
            "url",
            "name",
            "result",
            "votes",
            "votes_perc",
            "is_counselor",
            "is_top",
            # "elected_membership",
            "tmp_candidate",
        )


class ElectoralCandidateResultWriteSerializer(serializers.ModelSerializer):
    class Meta:
        """Define serializer Meta."""

        model = ElectoralCandidateResult
        ref_name = "ElectoralCandidateResult (write)"
        fields = (
            "elected_membership",
        )


class ElectoralCandidateResultInlineSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for ElectoralCandidateResult model."""

    class Meta:
        """Define serializer Meta."""

        model = ElectoralCandidateResult
        fields = (
            "id",
            "url",
            "name",
            "result_id",
        )


class ElectoralListResultSerializer(serializers.HyperlinkedModelSerializer):
    """A serializer for ElectoralListResult model."""
    candidate_result = ElectoralCandidateResultSerializer(many=False)
    political_colour = PoliticalColourSerializer()
    parties = OrganizationInlineSerializer(many=True)
    # elected_memberships = MembershipInlineSerializer(many=True)
    nested_fields = ["parties", ]

    class Meta:
        """Define serializer Meta."""

        model = ElectoralListResult
        fields = (
            "id",
            "url",
            "result",
            "name",
            "image",
            "votes",
            "votes_perc",
            "seats",
            "candidate_result",
            "political_colour",
            "parties",
            "tmp_lists",
            # "elected_memberships",
        )


class ElectoralListResultWriteSerializer(serializers.ModelSerializer):
    political_colour = serializers.PrimaryKeyRelatedField(
        queryset=PoliticalColour.objects.all(),
        required=False, allow_null=True
    )
    parties = serializers.PrimaryKeyRelatedField(
        queryset=Organization.objects.filter(classification='Partito/Movimento politico'),
        many=True, required=False, allow_empty=True
    )

    class Meta:
        """Define serializer Meta."""

        model = ElectoralListResult
        ref_name = "ElectoralListResult (write)"
        fields = (
            "political_colour",
            "parties",
            "tmp_lists",
        )


class ElectoralListResultInlineSerializer(serializers.HyperlinkedModelSerializer):
    """An inline serializer for ElectoralListResult model."""

    class Meta:
        """Define serializer Meta."""

        model = ElectoralListResult
        fields = (
            "id",
            "url",
            "result_id",
            "name",
        )


def get_electoral_list_result(self):
    if hasattr(self, 'electoralmembership'):
        return self.electoralmembership.list_result
    else:
        return None


def get_electoral_candidate_result(self):
    if hasattr(self, 'electoralmembership'):
        return self.electoralmembership.candidate_result
    else:
        return None


# inject these methods inside the Membership model
Membership.get_electoral_list_result = get_electoral_list_result
Membership.get_electoral_candidate_result = get_electoral_candidate_result
