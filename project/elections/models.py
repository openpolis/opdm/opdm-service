"""Define elections models."""
import datetime
import pickle
import re

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models import F, Value
from django.db.models.functions import Coalesce
from django.utils.translation import gettext_lazy as _
from popolo.behaviors.models import Timestampable
from popolo.mixins import SourceShortcutsMixin
from popolo.models import Area, KeyEvent, Organization, Membership

from project.elections.load import load_json_items
from project.elections.utils import url_links_matrioska, get_comune_from_url, get_opdm_data_from_context, \
    get_opdm_organizations, get_opdm_areas


class PoliticalColour(Timestampable, models.Model):
    """Political colour of candidates and lists."""

    name = models.CharField(
        _("name"),
        max_length=255,
        unique=True,
        help_text=_("Definition of the political colour."),
    )

    colour = models.CharField(
        _("colour"),
        max_length=255,
        blank=True, null=True,
        help_text=_("Colour"),
    )

    class Meta:
        """Define model Meta."""

        verbose_name = _("Political colour")
        verbose_name_plural = _("Political colours")

    def __str__(self):
        """Return string representation."""
        return self.name


def remove_multi_space(nome):
    try:
        return re.sub(' +', ' ', nome)
    except Exception:
        return nome


class ElectoralResult(SourceShortcutsMixin, Timestampable, models.Model):
    """Electoral result."""

    registered_voters = models.PositiveIntegerField(_("registered voters"))
    # first round
    votes_cast = models.PositiveIntegerField(_("votes cast"), null=True, blank=True)
    votes_cast_perc = models.DecimalField(
        _("votes cast percent"), max_digits=5, decimal_places=2, null=True, blank=True
    )
    invalid_votes = models.PositiveIntegerField(_("invalid votes"), null=True, blank=True)
    blank_votes = models.PositiveIntegerField(_("blank votes"), null=True, blank=True)
    valid_votes = models.PositiveIntegerField(_("valid votes"), null=True, blank=True)
    # second round
    ballot_votes_cast = models.PositiveIntegerField(
        _("votes cast (second round)"), null=True, blank=True
    )
    ballot_votes_cast_perc = models.DecimalField(
        _("votes cast percent (second round)"),
        max_digits=5,
        decimal_places=2,
        null=True,
        blank=True,
    )
    ballot_invalid_votes = models.PositiveIntegerField(
        _("invalid votes (second round)"), null=True, blank=True
    )
    ballot_blank_votes = models.PositiveIntegerField(
        _("blank votes (second round)"), null=True, blank=True
    )
    ballot_valid_votes = models.PositiveIntegerField(
        _("valid votes (second round)"), null=True, blank=True
    )
    # election
    is_valid = models.BooleanField(_("valid"))
    is_total = models.BooleanField(_("total"), default=True)
    electoral_event = models.ForeignKey(
        "popolo.KeyEvent",
        on_delete=models.PROTECT,
        verbose_name=_("electoral event"),
        related_name="results",
    )
    institution = models.ForeignKey(
        "popolo.Organization",
        on_delete=models.PROTECT,
        verbose_name=_("institution"),
        related_name="results",
    )
    constituency = models.ForeignKey(
        "popolo.Area",
        on_delete=models.PROTECT,
        verbose_name=_("constituency"),
        null=True,
        related_name="results",
    )
    # array of items referencing "http://popoloproject.com/schemas/source.json#"
    sources = GenericRelation(
        to="popolo.SourceRel",
        blank=True,
        null=True,
        help_text=_("URLs to source documents about the Electoral Result"),
    )

    parent = models.ForeignKey(
        to="ElectoralResult",
        blank=True, null=True,
        related_name="children",
        help_text="Elezione padre",
        on_delete=models.CASCADE,
    )

    class Meta:
        """Define model Meta."""

        verbose_name = _("Electoral result")
        verbose_name_plural = _("Electoral results")

    def __str__(self):
        """Return string representation."""
        return (
            f"{self.electoral_event.name} - "
            f"{self.institution.name} - "
            f"{self.constituency.name}"
        )

    @property
    def has_second_round(self):
        """Return true if it has the second round."""
        return self.ballot_votes_cast is not None

    @classmethod
    def load_results(cls, path: str, typology: str=None, logger=None):
        """Load electoral results from a path."""
        stats = {"events": 0, "results": 0, "candidates": 0, "lists": 0, "errors": []}
        importazione, error_str = load_json_items(path)
        if error_str:
            stats["errors"].append(error_str)
        # opdm_organizations = get_opdm_organizations()
        # opdm_areas = get_opdm_areas()

        for event in getattr(importazione, "eventi", []):
            event_type = f"{event.tipo}"
            event_type_text = dict(KeyEvent.EVENT_TYPES).get(event_type, event_type)
            key_event, ke_created = KeyEvent.objects.get_or_create(
                start_date=f"{event.data}",
                event_type=f"{event.tipo}",
                defaults={
                    "name": f"{event_type_text} {event.data:%d/%m/%Y}",
                    "identifier": f"{event_type}_{event.data}",
                },
            )
            if logger:
                logger.info(f"Event: {key_event} (created: {ke_created})")
            if typology in ['S', 'C']:
                ramo_path = path.name[0].split('/')[-1][0]
            else:
                ramo_path = typology
            if typology in ['C', 'S']:
                url_elezione = ('https://elezionistorico.interno.gov.it' +
                                '/index.php?tpel=' + ramo_path + '&dtel=' + event.data.strftime('%d/%m/%Y'))
                # uri_list = list(dict.fromkeys(url_links_matrioska(url_elezione, 1, pol=True)))
                # with open(f"{path.parent}/uri_list_{ramo_path}_{event.data.strftime('%d_%m_%Y')}.pickle", 'wb') as handle:
                #     pickle.dump(uri_list, handle, protocol=pickle.HIGHEST_PROTOCOL)
                with open(f"{path.parent}/uri_list_{ramo_path}_{event.data.strftime('%d_%m_%Y')}.pickle", 'rb') as handle:
                    uri_list = pickle.load(handle)
            stats["events"] += 1

            mapping_area_hier={
                'main': None,
                'circ': [],
                'pluri': [],
                'uni': []
            }
            for index, result in enumerate(event.risultati):
                area_id = result.area
                if index ==0 and typology in ['S', 'C'] and event.data.strftime('%Y-%m-%d')!='2013-02-24':
                    area = Area.objects.get(name='Italia')
                elif index !=0 and typology in ['S', 'C'] and (area_id is None or not str(area_id).isnumeric()):
                    try:
                        comune_name, provincia_name, pluri, collegio = get_comune_from_url(uri_list[index])
                        if comune_name and comune_name[0] == 'J':
                            comune_name = 'I'+comune_name[1:]
                        elif (comune_name or pluri) == 'STATO CITTA\' DEL VATICANO':
                            comune_name = 'Vaticano'
                        elif (comune_name or pluri) == 'ESWATINI':
                            comune_name = 'Swaziland'
                        elif (comune_name or pluri) == 'TIMOR ORIENTALE':
                            comune_name = 'TIMOR EST'
                        try:
                            area = Area.objects.annotate(end_date_coalesce=Coalesce(F('end_date'), Value('9999-12-31'))).get(name__iexact=(comune_name or pluri),
                                                    start_date__lte=event.data.strftime('%Y-%m-%d'), end_date__gte=event.data.strftime('%Y-%m-%d'))
                        except:
                            area = Area.objects.annotate(
                                end_date_coalesce=Coalesce(F('end_date'), Value('9999-12-31'))).get(
                                name__istartswith=(comune_name or pluri),
                                start_date__lte=event.data.strftime('%Y-%m-%d'),
                                end_date__gte=event.data.strftime('%Y-%m-%d'))
                    except:
                        stats["errors"].append(
                            f"{comune_name} does not exists."
                        )
                        continue
                else:
                    try:
                        # context = {
                        #     'data_elezione':event.data,
                        #     'tipo_elezione': result.tipo_elezione,
                        #     'area': 'ITALIA',
                        #     'regione': result.regione,
                        #     'circoscrizione': result.circoscrizione,
                        #     'provincia': result.provincia,
                        #     'comune': result.comune
                        # }
                        # recalc_area, recalc_istituzione = get_opdm_data_from_context(opdm_organizations, opdm_areas, **context)
                        area = Area.objects.get(id=area_id)
                        # if recalc_area and area_id != recalc_area:
                        #     stats["errors"].append(f"Area {area_id} {area.name} bad id.")
                        #     continue
                        # if typology != 'COM':
                        #     if result.comune:
                        #         if area.classification != 'ADM3':
                        #             stats["errors"].append(f"Area {area_id} bad classification.")
                        #             break
                        #     else:
                        #         if area.classification == 'ADM3':
                        #             stats["errors"].append(f"Area {area_id} bad classification.")
                        #             break
                    except Area.DoesNotExist:
                        stats["errors"].append(f"Area {area_id} does not exists.")
                        continue
                organization_id = result.istituzione
                try:
                    organization = Organization.objects.get(id=organization_id)
                except Organization.DoesNotExist:
                    stats["errors"].append(
                        f"Organization {organization_id} does not exists."
                    )
                    continue
                parent_elect_event = None
                if typology in ['C', 'S']:
                    if area.name=='ESTERO':
                        parent_elect_event = None
                    elif area.name in ['Trentino-Alto Adige/Südtirol', "Valle d'Aosta/Vallée d'Aoste"] and event.data.strftime('%Y-%m-%d')=='2013-02-24' and ramo_path=='S':
                        parent_elect_event = None
                    elif area.classification=='ELECT_RIP':
                        parent_elect_event = mapping_area_hier['estero']
                    elif area.classification=='ADM1' or area.parent.classification=='ELECT_CIRC':
                        parent_elect_event = mapping_area_hier['main']
                    elif area.classification=='ADM3' and event.data.strftime('%Y-%m-%d')!='2013-02-24':
                        parent_elect_event=mapping_area_hier['uni'][-1]
                    elif area.classification=='ADM3' and event.data.strftime('%Y-%m-%d')=='2013-02-24' and ramo_path=='S' and area.parent.parent.name not in ["Trentino-Alto Adige/Südtirol"] and area.parent.name not in ["Valle d'Aosta/Vallée d'Aoste"]:
                        parent_elect_event = mapping_area_hier['prov']
                    elif area.classification=='ADM3' and event.data.strftime('%Y-%m-%d')=='2013-02-24' and ramo_path=='C' and  area.parent.name not in ["Valle d'Aosta/Vallée d'Aoste"]:
                        parent_elect_event = mapping_area_hier['prov']
                    elif area.classification=='ADM3' and event.data.strftime('%Y-%m-%d')=='2013-02-24' and ramo_path=='S' and area.parent.parent.name in ["Trentino-Alto Adige/Südtirol", "Valle d'Aosta/Vallée d'Aoste"]:
                        parent_elect_event = mapping_area_hier['uni'][-1]
                    elif area.classification=='ADM3' and event.data.strftime('%Y-%m-%d')=='2013-02-24' and ramo_path=='C' and area.parent.parent.name in ["Valle d'Aosta/Vallée d'Aoste"]:
                        parent_elect_event = mapping_area_hier['uni'][-1]
                    elif  area.classification in ['ELECT_COLL'] and area.parent.classification=='ELECT_CIRC':
                        parent_elect_event=mapping_area_hier['circ'][-1]
                    elif (area.classification in ['ELECT_UNI'] and area.parent.classification=='ELECT_COLL'
                          and ramo_path=='C' and area.name =="Valle d'Aosta/Vallée d'Aoste - 01 Aosta"):
                        parent_elect_event = None
                    elif  area.classification in ['ELECT_UNI'] and area.parent.classification=='ELECT_COLL':
                        parent_elect_event=mapping_area_hier['pluri'][-1]
                    elif area.classification=='ADM1' and ramo_path=='S':
                        parent_elect_event = mapping_area_hier['main']
                    elif area.classification in ['ELECT_COLL'] and area.parent.classification=='ADM1' and ramo_path=='S':
                        parent_elect_event = mapping_area_hier['circ'][-1]
                    elif area.classification=='ELECT_UNI' and ramo_path=='S' and 'trentino' in area.parent.name.lower():
                        parent_elect_event = mapping_area_hier['main']
                    elif area.classification =='PCL' and area.name.lower() !='italia' and ramo_path=='S':
                        parent_elect_event=mapping_area_hier['pluri'][-1]
                    elif area.classification =='PCL' and area.name.lower() !='italia' and ramo_path=='C' and event.data.strftime('%Y-%m-%d')=='2013-02-24':
                        parent_elect_event=mapping_area_hier['pluri'][-1]
                    elif area.classification =='PCL' and area.name.lower() !='italia' and ramo_path=='C':
                        parent_elect_event=mapping_area_hier['estero']
                    elif area.classification == 'ELECT_CIRC' and ramo_path=='C':
                        parent_elect_event = mapping_area_hier['main']
                    elif area.classification == 'ADM2' and ramo_path and event.data.strftime('%Y-%m-%d')=='2013-02-24':
                        parent_elect_event = mapping_area_hier['circ'][-1]
                    else:
                        print('a')
                        pass
                elif typology == 'R':
                    if area.classification =='ADM1':
                        parent_elect_event=None
                    elif area.classification =='ELECT_CIRC_REG':
                        parent_elect_event = mapping_area_hier['reg']
                    elif area.classification =='ADM3':
                        parent_elect_event = mapping_area_hier['elect_circ_reg']
                elif typology == 'P':
                    if area.classification =='ADM2':
                        parent_elect_event=None
                    elif area.classification =='ELECT_CIRC_REG':
                        parent_elect_event = mapping_area_hier['prov']
                    elif area.classification =='ADM3':
                        parent_elect_event = mapping_area_hier['elect_circ_prov']
                elif typology == 'E':
                    if area.classification=='CONT':
                        parent_elect_event = None
                    elif area.classification=='PCL' and area.name.lower() != 'italia':
                        parent_elect_event = mapping_area_hier['main']
                    elif area.classification=='PCL' and area.name.lower() == 'italia':
                        parent_elect_event = None
                    elif area.classification == 'RGNE':
                        parent_elect_event = mapping_area_hier['main']
                    elif area.classification == 'ADM1':
                        parent_elect_event = mapping_area_hier['rgne']
                    elif area.classification == 'ADM2':
                        parent_elect_event = mapping_area_hier['adm1']
                    elif area.classification == 'ADM3':
                        parent_elect_event = mapping_area_hier['adm2']
                elif typology == 'SUP':
                    if area.classification=='ELECT_UNI':
                        parent_elect_event=None
                    else:
                        parent_elect_event = mapping_area_hier['elect_uni']

                electoral_res, er_created = ElectoralResult.objects.update_or_create(
                    electoral_event=key_event,
                    institution=organization,
                    constituency=area,
                    parent=parent_elect_event,
                    defaults={
                        "blank_votes": result.bianche,
                        "ballot_blank_votes": result.bianche_ballot,
                        "registered_voters": 0 if result.elettori < 0 else result.elettori,
                        "is_valid": result.elezione_valida,
                        "invalid_votes": result.nulle,
                        "ballot_invalid_votes": result.nulle_ballot,
                        "votes_cast_perc": result.perc_votanti,
                        "ballot_votes_cast_perc": result.perc_votanti_ballot,
                        "is_total": result.totale,
                        "votes_cast": result.votanti,
                        "ballot_votes_cast": result.votanti_ballot,
                        "valid_votes": (
                            None
                            if (
                                result.nulle is None
                                or result.votanti is None
                            )
                            else result.votanti - result.nulle
                        ),
                        "ballot_valid_votes": (
                            None
                            if (
                                result.nulle_ballot is None
                                or result.votanti_ballot is None
                            )
                            else result.votanti_ballot - result.nulle_ballot
                        ),
                    },
                )
                if typology in ['C','S']:
                    if index==0 and event.data.strftime('%Y-%m-%d')!='2013-02-24':
                        mapping_area_hier['main'] = electoral_res
                    elif area.name == 'ESTERO':
                        mapping_area_hier['estero']=electoral_res
                    elif area.classification in ['ELECT_CIRC']:
                        mapping_area_hier['circ'].append(electoral_res)
                    elif area.classification in ['ELECT_COLL'] and area.parent.classification=='ELECT_CIRC':
                        mapping_area_hier['pluri'].append(electoral_res)
                    elif area.classification in ['ELECT_UNI'] and area.parent.classification=='ELECT_COLL':
                        mapping_area_hier['uni'].append(electoral_res)
                    elif ((area.classification=='ADM1' and area.name=='Valle d\'Aosta/Vallée d\'Aoste' and ramo_path=='C') or
                        (area.classification=='ADM1' and ('trentino' in area.name.lower() or 'aosta' in area.name.lower()) and ramo_path=='C')):
                        mapping_area_hier['uni'].append(electoral_res)
                    elif area.classification=='ADM1' and area.name=='Valle d\'Aosta/Vallée d\'Aoste' and event.data.strftime('%Y-%m-%d')!='2013-02-24':
                        mapping_area_hier['uni'].append(electoral_res)

                    elif ramo_path == 'S' and area.classification == 'ADM1' and area.name in ['Valle d\'Aosta/Vallée d\'Aoste',
                                                                                              'Trentino-Alto Adige/Südtirol'] and event.data.strftime('%Y-%m-%d')=='2013-02-24':
                        mapping_area_hier['pluri'].append(electoral_res)
                    elif ramo_path=='S' and area.classification=='ADM1':
                        mapping_area_hier['circ'].append(electoral_res)
                    elif area.classification in ['ELECT_COLL'] and area.parent.classification == 'ADM1' and ramo_path=='S':
                        mapping_area_hier['pluri'].append(electoral_res)
                    elif area.classification in ['ELECT_RIP'] and ramo_path=='S':
                        mapping_area_hier['pluri'].append(electoral_res)
                    elif area.classification in ['ELECT_RIP'] and ramo_path=='C' and event.data.strftime('%Y-%m-%d')=='2013-02-24':
                        mapping_area_hier['pluri'].append(electoral_res)
                    elif area.classification == 'PCL' and area.name.lower()=='italia' and event.data.strftime('%Y-%m-%d')=='2013-02-24':
                        mapping_area_hier['main'] = electoral_res
                    elif area.classification == 'ADM2' and event.data.strftime('%Y-%m-%d')=='2013-02-24':
                        mapping_area_hier['prov'] = electoral_res
                elif typology == 'R':
                    if area.classification =='ADM1':
                        mapping_area_hier['reg'] = electoral_res
                    elif area.classification =='ELECT_CIRC_REG':
                        mapping_area_hier['elect_circ_reg'] = electoral_res
                elif typology == 'P':
                    if area.classification =='ADM2':
                        mapping_area_hier['prov'] = electoral_res
                    elif area.classification =='ELECT_CIRC_REG':
                        mapping_area_hier['elect_circ_prov'] = electoral_res
                elif typology == 'E':
                    if area.classification == 'CONT':
                        mapping_area_hier['main'] = electoral_res
                    elif area.classification == 'PCL' and area.name.lower() == 'italia':
                        mapping_area_hier['main'] = electoral_res
                    elif area.classification == 'RGNE':
                        mapping_area_hier['rgne'] = electoral_res
                    elif area.classification == 'ADM1':
                        mapping_area_hier['adm1'] = electoral_res
                    elif area.classification == 'ADM2':
                        mapping_area_hier['adm2'] = electoral_res
                elif typology == 'SUP':
                    if area.classification == 'ELECT_UNI':
                        mapping_area_hier['elect_uni'] = electoral_res
                if logger:
                    cu = "created" if er_created else "updated"
                    logger.info(f"  result {electoral_res} ({cu})")
                stats["results"] += 1

                # import candidati e liste associate
                if result.candidati:
                    for candidate in result.candidati:
                        candidate_res, _ = ElectoralCandidateResult.objects.update_or_create(
                            result=electoral_res,
                            name=remove_multi_space(candidate.nome),
                            defaults={
                                "votes": candidate.voti,
                                "ballot_votes": candidate.voti_ballot,
                                "votes_perc": candidate.perc_voti,
                                "ballot_votes_perc": candidate.perc_voti_ballot,
                                "is_top": candidate.eletto_chief,
                                "is_counselor": candidate.eletto_consigliere,
                            },
                        )
                        stats["candidates"] += 1
                        if candidate.listino:
                            cand_list_obj, created = ElectoralListResult.objects.update_or_create(
                                result=electoral_res,
                                candidate_result=candidate_res,
                                name=candidate.name_listino,
                                defaults={
                                    "image": '',
                                    "votes": candidate.voti,
                                    "votes_perc": candidate.perc_voti,
                                    "seats": None,
                                    "ballot_candidate_result": None,
                                    "tmp_lists": '',
                                },
                            )
                            for list_cand in candidate.listino:
                                ElectoralSubCandidateResult.objects.update_or_create(
                                    name=remove_multi_space(list_cand.candidati.upper()),
                                    birth_date=datetime.datetime.strptime(list_cand.data_di_nascita,
                                                                          '%d/%m/%Y') if
                                    list_cand.data_di_nascita != '' else None,
                                    birth_place=list_cand.luogo_di_nascita,
                                    list_result=cand_list_obj,
                                    defaults={
                                        'is_elected': True if list_cand.eletto.upper() == 'Eletto'.upper()
                                        else False,
                                        'votes': list_cand.preferenze
                                    })

                        for lista in candidate.liste:
                            ballot_cand_res = None
                            candidate_result = candidate_res
                            tmp_lists = []
                            if lista.candidato:
                                try:
                                    candidate_result = ElectoralCandidateResult.objects.get(
                                        name=remove_multi_space(lista.candidato),
                                        result=electoral_res,
                                    )
                                except ElectoralCandidateResult.DoesNotExist:
                                    candidate_result = None
                                else:
                                    ballot_cand_res = candidate_res
                            if candidate_result is not None:
                                if lista.candidato_ballot:
                                    try:
                                        ballot_cand_res = (
                                            ElectoralCandidateResult.objects.get(
                                                name=remove_multi_space(lista.candidato_ballot),
                                                result=electoral_res,
                                            )
                                        )
                                    except ElectoralCandidateResult.DoesNotExist:
                                        ballot_cand_res = None

                                list_obj, created = ElectoralListResult.objects.update_or_create(
                                    result=electoral_res,
                                    candidate_result=candidate_result,
                                    name=lista.denominazione,
                                    defaults={
                                        "image": lista.simbolo,
                                        "votes": lista.voti,
                                        "votes_perc": lista.perc_voti,
                                        "seats": lista.seggi,
                                        "ballot_candidate_result": ballot_cand_res,
                                        "tmp_lists": tmp_lists,
                                    },
                                )
                                if lista.candidati_preferenze:
                                    for candidati_pef in lista.candidati_preferenze:
                                        ElectoralSubCandidateResult.objects.update_or_create(
                                            name=remove_multi_space(candidati_pef.candidati.upper()),
                                            birth_date=datetime.datetime.strptime(candidati_pef.data_di_nascita,
                                                                                  '%d/%m/%Y') if
                                            candidati_pef.data_di_nascita != '' else None,
                                            birth_place=candidati_pef.luogo_di_nascita,
                                            list_result=list_obj,
                                            defaults={
                                                'is_elected': True if candidati_pef.eletto.upper() == 'Eletto'.upper()
                                                else False,
                                                'votes': candidati_pef.preferenze
                                            })
                                stats["lists"] += 1

                # import risultati di lista svincolati da candidati (politiche riepiloghi e collegi non uni)
                if hasattr(result, 'liste') and result.liste:
                    for lista in result.liste:
                        list_obj, created = ElectoralListResult.objects.update_or_create(
                            result=electoral_res,
                            candidate_result=None,
                            name=lista.denominazione,
                            defaults={
                                "image": lista.simbolo,
                                "votes": lista.voti,
                                "votes_perc": lista.perc_voti,
                                "seats": lista.seggi,
                                "ballot_candidate_result": None,
                            },
                        )
                        if lista.candidati_preferenze:
                            for candidati in lista.candidati_preferenze:
                                ElectoralSubCandidateResult.objects.update_or_create(
                                    name=remove_multi_space(candidati.candidati.upper()),
                                    birth_date=datetime.datetime.strptime(candidati.data_di_nascita,
                                                                          '%d/%m/%Y') if
                                    candidati.data_di_nascita != '' else None,
                                    birth_place=candidati.luogo_di_nascita,
                                    list_result=list_obj,
                                    defaults={
                                        'is_elected': True if candidati.eletto.upper() == 'Eletto'.upper()
                                        else False,
                                        'votes': candidati.preferenze
                                    })
                        stats["lists"] += 1

        return stats

    # @property
    # def get_memberships(self):
    #     elr = ElectoralListResult.objects.filter(result=self)
    #     return [{x.name: x.get_elected_memberships} for x in elr if len(x.get_elected_memberships) > 0]


class ElectoralCandidateResult(Timestampable, models.Model):
    """Electoral candidate result."""

    name = models.CharField(_("name"), max_length=512, db_index=True)
    # first round
    votes = models.PositiveIntegerField(_("votes"), blank=True, null=True)
    votes_perc = models.DecimalField(
        _("votes percent"), max_digits=5, decimal_places=2, blank=True, null=True
    )
    # second round
    ballot_votes = models.PositiveIntegerField(
        _("votes (second round)"), blank=True, null=True
    )
    ballot_votes_perc = models.DecimalField(
        _("votes percent (second round)"),
        max_digits=5,
        decimal_places=2,
        blank=True,
        null=True,
    )
    # election
    is_counselor = models.BooleanField(_("counselor"), blank=True, null=True)
    is_top = models.BooleanField(_("top"), blank=True, null=True)

    membership = models.ForeignKey(
        Membership,
        verbose_name=_("candidate membership"),
        related_name="electoral_candidate_results",
        blank=True, null=True,
        help_text=_(
            "The membership linked to this election result"
        ),
        on_delete=models.SET_NULL
    )

    result = models.ForeignKey(
        ElectoralResult,
        on_delete=models.CASCADE,
        verbose_name=_("result"),
        related_name="candidate_results",
    )
    political_colour = models.ForeignKey(
        PoliticalColour,
        on_delete=models.SET_NULL,
        verbose_name=_("political colour"),
        related_name="candidate_results",
        blank=True,
        null=True,
    )
    tmp_candidate = models.JSONField(_("temp candidate"), default=dict, blank=True)

    class Meta:
        """Define model Meta."""

        verbose_name = _("Electoral candidate result")
        verbose_name_plural = _("Electoral candidate results")

    def __str__(self):
        """Return string representation."""
        return f"{self.name} - {self.result}"


class ElectoralListResult(Timestampable, models.Model):
    """Electoral list result."""

    name = models.CharField(_("name"), max_length=512, db_index=True)
    image = models.URLField(_("image"),blank=True, null=True)
    votes = models.PositiveIntegerField(_("votes"), blank=True, null=True)
    votes_perc = models.DecimalField(
        _("votes percent"), max_digits=5, decimal_places=2, blank=True, null=True
    )
    seats = models.PositiveIntegerField(_("seats"), blank=True, null=True)
    result = models.ForeignKey(
        ElectoralResult,
        on_delete=models.CASCADE,
        verbose_name=_("result"),
        related_name="list_results",
    )
    candidate_result = models.ForeignKey(
        ElectoralCandidateResult,
        on_delete=models.CASCADE,
        verbose_name=_("candidate result"),
        related_name="list_results",
        null=True,
    )
    ballot_candidate_result = models.ForeignKey(
        ElectoralCandidateResult,
        on_delete=models.CASCADE,
        verbose_name=_("candidate result (second round)"),
        related_name="ballot_list_results",
        null=True,
        blank=True,
    )
    political_colour = models.ForeignKey(
        PoliticalColour,
        on_delete=models.SET_NULL,
        verbose_name=_("political colour"),
        related_name="list_results",
        blank=True,
        null=True,
    )
    parties = models.ManyToManyField(
        Organization,
        related_name="electoral_list_results",
        limit_choices_to={'classification': 'Partito/Movimento politico'},
        blank=True,
        help_text=_("Parties organization connected to this list result")
    )
    elected_memberships = models.ManyToManyField(
        Membership,
        related_name="electoral_list_results",
        blank=True,
        help_text=_(
            "Memberships elected having this list among the supporting lists, "
            "with respect to this list result"
        )
    )
    tmp_lists = models.JSONField(
        _("connected temp lists"),
        default=list, blank=True,
        help_text=_(
            "A list of electoral list names stored in the electoral_list_descr_tmp field, "
            "containing all lists connected to this electoral result."
        )
    )
    dati_specifici = models.JSONField(
        blank=True,
        null=True,
        default=dict
    )

    class Meta:
        """Define model Meta."""

        verbose_name = _("Electoral list result")
        verbose_name_plural = _("Electoral list results")

    def __str__(self):
        """Return string representation."""
        return f"{self.name} - {self.result}"

    @property
    def get_elected_memberships(self):
        def flatten(t):
            q = t[0]
            for index, i in enumerate(t):
                if index == 0:
                    continue
                q = q | i
            return q

        def find_root_tree(ar: Area, class_election: str) -> [list, None]:
            if ar is None:
                return None

            def check_up(up):
                if up is None:
                    return None
                if up.classification == class_election:
                    return Area.objects.filter(id=up.id)
                else:
                    return check_up(up.parent)

            res = check_up(ar)
            if res is None:
                def check_below(below):
                    if below is None:
                        return None
                    res = flatten([Area.objects.filter(parent=x) for x in below])
                    ls = set([x.classification for x in res])
                    if class_election in ls:
                        return res
                    if len(ls) == 0:
                        return None
                    else:
                        return check_below(res)

                ar = [ar]
                res = check_below(ar)
            return res

        constituency = self.result.constituency
        event = self.result.electoral_event
        map_event_master = {'ELE-EU': 'RGNE',
                            'ELE-REG': 'ELECT_CIRC_REG'}
        if event.event_type in ['ELE-EU', 'ELE-REG']:
            master_area = find_root_tree(constituency, map_event_master[event.event_type])
            if master_area is not None:
                master_list = ElectoralListResult.objects.filter(result__constituency__in=master_area,
                                                                 result__electoral_event=event,
                                                                 name=self.name)
                return flatten([x.elected_memberships.all() for x in master_list])
        return self.elected_memberships.all()  # .values_list('person__name', flat=True)


# class AreaMapConstituency(Timestampable, models.Model):
#
#
#     constituency = models.ForeignKey(
#         "popolo.Area",
#         on_delete=models.PROTECT,
#         verbose_name=_("constituency"),
#         null=True,
#         related_name="constituency_area",
#     )
#
#     sub_areas = models.ForeignKey(
#         "popolo.Area",
#         on_delete=models.PROTECT,
#         verbose_name=_("sub_areas"),
#         null=True,
#         related_name="sub_areas",
#     )
#     typology = models.CharField(
#         _("typology"),
#         max_length=255,
#         unique=False,
#         help_text=_("Definition of administrative level of sub_areas."),
#     )
#
#     class Meta:
#         """Define model Meta."""
#
#         verbose_name = _("Mapping new area")
#         verbose_name_plural = _("Mapping new areas")
#
#     def __str__(self):
#         """Return string representation."""
#         return f"{self.sub_areas.name} is a part of {self.constituency.name}"


class ElectoralSubCandidateResult(Timestampable, models.Model):
    """Electoral candidate result."""

    name = models.CharField(_("name"), max_length=512, db_index=True)
    birth_date = models.DateField(max_length=8, null=True)
    birth_place = models.CharField(max_length=512, null=True)
    votes = models.PositiveIntegerField(_("votes"), blank=True, null=True)

    is_elected = models.BooleanField(_("counselor"), blank=True, null=True)
    elected_membership = models.ManyToManyField(
        Membership,
        verbose_name=_("sub candidate membership"),
        related_name="electoral_sub_candidate_results",
        blank=True,
        help_text=_(
            "The membership linked to this election list result"
        )
    )
    list_result = models.ForeignKey(
        ElectoralListResult,
        on_delete=models.CASCADE,
        verbose_name=_("list_result"),
        related_name="sub_candidate_results",
    )

    class Meta:
        """Define model Meta."""

        verbose_name = _("Electoral sub candidate result")
        verbose_name_plural = _("Electoral sub candidate results")

    def __str__(self):
        """Return string representation."""
        return f"{self.name} - {self.list_result}"
