"""Configure the elections app."""

from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _


class ElectionsConfig(AppConfig):
    """The elections app configuration."""

    name = "project.elections"
    verbose_name = _("Elections")
