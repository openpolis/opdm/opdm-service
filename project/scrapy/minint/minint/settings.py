# -*- coding: utf-8 -*-

# Scrapy settings for minint project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://doc.scrapy.org/en/latest/topics/settings.html
#     https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://doc.scrapy.org/en/latest/topics/spider-middleware.html
import environ
from pathlib import Path
ROOT_PATH = Path(__file__).parents[4]  # ROOT/project/scrapy/minint/minint/settings.py - 4 = ROOT/
RESOURCES_PATH = ROOT_PATH / 'resources'
CONFIG_PATH = ROOT_PATH / "config"

FETCH_ROLES = False

# Parse .env file
# -----------------------------------------------------------------------------
env = environ.Env()
# .env file, should load only in development environment
# Operating System Environment variables have precedence over variables defined
# in the .env file, that is to say variables from the .env files will only be
# used if not defined as environment variables.
if env.str("DJANGO_ENV_FILE", default=True):
    environ.Env.read_env(str(CONFIG_PATH / ".env"))

BOT_NAME = 'minint'

SPIDER_MODULES = ['minint.spiders']
NEWSPIDER_MODULE = 'minint.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = env(
    "SCRAPY_USER_AGENT",
    default="Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:109.0) Gecko/20100101 Firefox/109.0"
)

# Proxy to connect as if from Italy
HTTP_PROXY_HOST = env("HTTP_PROXY_HOST", default=None)
HTTP_PROXY_HOST_PORT = env("HTTP_PROXY_HOST_PORT", default="8080")
HTTP_PROXY_USERNAME = env("HTTP_PROXY_USERNAME", default=None)
HTTP_PROXY_PASSWORD = env("HTTP_PROXY_PASSWORD", default=None)

# Obey robots.txt rules
ROBOTSTXT_OBEY = True

FEED_EXPORT_INDENT = 2

# Configure logging
# LOG_FILE = os.path.normpath(str(RESOURCES_PATH) + "/logs/scrapy.log")

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 16

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
DOWNLOAD_DELAY = 1

# The download delay setting will honor only one of:
# CONCURRENT_REQUESTS_PER_DOMAIN = 16
# CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
# COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
# TELNETCONSOLE_ENABLED = False

# Override the default request headers:
# DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
# }

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
# SPIDER_MIDDLEWARES = {
#     'minint.middlewares.MinintSpiderMiddleware': 543,
# }

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
DOWNLOADER_MIDDLEWARES = {
    'minint.middlewares.MinintProxyMiddleware': 350,
    'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 400,
}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
# EXTENSIONS = {
#     'scrapy.extensions.telnet.TelnetConsole': None,
# }

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
    'minint.pipelines.histadmin.TransformerPipeline': 300,
    'minint.pipelines.histadmin.OPDMLoaderPipeline':  400,
    'minint.pipelines.histadmin.JSONEmitterPipeline': 500,
}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
AUTOTHROTTLE_ENABLED = True
# # The initial download delay
# AUTOTHROTTLE_START_DELAY = 5
# # The maximum download delay to be set in case of high latencies
# AUTOTHROTTLE_MAX_DELAY = 20
# # The average number of requests Scrapy should be sending in parallel to
# # each remote server
AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# # Enable showing throttling stats for every response received:
# AUTOTHROTTLE_DEBUG = True

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
# HTTPCACHE_ENABLED = True
# HTTPCACHE_EXPIRATION_SECS = 0
# HTTPCACHE_DIR = 'httpcache'
# HTTPCACHE_IGNORE_HTTP_CODES = []
# HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'
