# -*- coding: utf-8 -*-

# core functions used in pipeline to transform items received from scraper
# and load into OPDM after comparison with existing data
from datetime import datetime


def convert_date(date: str) -> str:
    """Convert date into sortable form

    :param date: string in  %d/%m/%Y format
    :return: string in %Y-%m-%d format
    """
    return datetime.strptime(date, '%d/%m/%Y').strftime('%Y-%m-%d')


def normalized_role(role):
    d = {
        'assessore comunale': 'assessore',
        'assessore provinciale': 'assessore',
        'assessore regionale': 'assessore',
        'assessore anziano': 'assessore',
        'assessore effettivo': 'assessore',
        'assessore non di origine elettiva': 'assessore',
        'assessore supplente': 'assessore',
        'commissario prefettizio': 'commissario',
        'commissario prefettizio art.19': 'commissario',
        'commissario regionale': 'commissario',
        'commissario regionaleassessore supplente': 'commissario',
        'commissario straordinario': 'commissario',
        'commissione straordinaria': 'commissario',
        'consigliere comunale': 'consigliere',
        'consigliere provinciale': 'consigliere',
        'consigliere regionale': 'consigliere',
        'consigliere candidato sindaco': 'consigliere',
        'consigliere - candidato sindaco': 'consigliere',
        'consigliere -  candidato sindaco': 'consigliere',
        'consigliere candidato presidente': 'consigliere',
        'consigliere - candidato presidente': 'consigliere',
        'consigliere - candidato presidenteassessore anziano': 'consigliere',
        'consigliere - candidato presidentevicepresidente della provincia di origine elettiva': 'consigliere',
        'consigliere straniero': 'consigliere',
        'consigliere straniero aggiunto': 'consigliere',
        'consigliere supplente': 'consigliere',
        'delega funzioni da parte del sindaco': 'consigliere',
        'delega funzioni da parte del sindaco - vicesindaco': 'consigliere',
        'delega funzioni da parte del sindaco - vicesindaco reggente': 'consigliere',
        'delega funzioni da parte del sindaco - vicesindaco supplente': 'consigliere',
        'questore': 'consigliere',
        'segretario del consiglio': 'consigliere',
        'sub commissario prefettizio': 'commissario',
        'sub commissario straordinario': 'commissatrio',
        'presidente di consiglio regionale': 'presidente di consiglio',
        'presidente di consiglio provinciale': 'presidente di consiglio',
        'presidente di consiglio comunale': 'presidente di consiglio',
        'presidente del consiglio': 'presidente di consiglio',
        'presidente della provincia': 'presidente di provincia',
        'presidente della regione': 'presidente di regione',
        'vicepresidente reggente della provincia di origine elettiva': 'vicepresidente di provincia',
        'vicepresidente reggente della provincia non di origine elettiva': 'vicepresidente di provincia',
        'vicepresidente supplente della provincia di origine elettiva': 'vicepresidente di provincia',
        'vicepresidente della provincia': 'vicepresidente di provincia',
        'vicepresidente della provincia di origine elettiva': 'vicepresidente di provincia',
        'vicepresidente della provincia non di origine elettiva': 'vicepresidente di provincia',
        'vicepresidente della regione': 'vicepresidente di regione',
        'vicepresidente della regione di origine elettiva': 'vicepresidente di regione',
        'vicepresidente reggente della regione non di origine elettiva': 'vicepresidente di regione',
        'vicepresidente della regione non di origine elettiva': 'vicepresidente di regione',
        'vicepresidente del consiglio': 'vicepresidente di consiglio',
        'vicepresidente di consiglio regionale': 'vicepresidente di consiglio',
        'vicepresidente di consiglio provinciale': 'vicepresidente di consiglio',
        'vicepresidente di consiglio comunale': 'vicepresidente di consiglio',
        'vicesindaco reggente non di origine elettiva': 'vicesindaco',
        'vicesindaco supplente non di origine elettiva': 'vicesindaco',
        "vicesindaco elettivo in valle d'Aosta": 'vicesindaco',
        'vicesindaco metropolitano': 'vicesindaco',
        'vicesindaco non di origine elettiva': 'vicesindaco',
        'vicesindaco reggente': 'vicesindaco',
        'vicesindaco supplente': 'vicesindaco',
    }

    return d.get(role.lower().strip(), role.lower().strip())


def normalized_location_name(name):
    loc = {
        "VALLE D'AOSTA": "Valle d'Aosta/Vallée d'Aoste",
        "TRENTINO-ALTO ADIGE": "Trentino-Alto Adige/Südtirol",
        "REGGIO CALABrIA": "Reggio di Calabria",
    }
    return loc.get(name.upper().strip(), name.upper().strip())


def get_or_create_electoral_event(loc_type, election_date):
    from popolo.models import KeyEvent
    from project.core import labels_utils

    context = loc_type

    if context == 'CM':
        context = 'METRO'

    election_name = "Elezioni {0} {1}".format(
        labels_utils.context2adj(context)
        .replace("ale", "ali")
        .replace("ana", "ane")
        .replace("comunal", "comunali")
        .replace("provincial", "provinciali")
        .replace("metropolitan", "metropolitane")
        .replace("regional", "regionali"),
        election_date,
    )

    if context:
        identifier = "ELE-{0}_{1}".format(context, election_date)
    else:
        identifier = None

    election, created = KeyEvent.objects.get_or_create(
        start_date=election_date,
        event_type="ELE-{0}".format(context),
        defaults={
            'name': election_name,
            'end_date': election_date,
            'identifier': identifier
        }
    )

    return election
