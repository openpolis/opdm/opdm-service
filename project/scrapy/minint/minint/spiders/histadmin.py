# -*- coding: utf-8 -*-
import logging
import re
import urllib.parse

import scrapy
logger = logging.getLogger(__name__)


class HistAdminSpiderException(Exception):
    pass


class HistAdminSpider(scrapy.Spider):
    """Spider to crawl the administrative history of institutions @ minint
    Accepts two parameters:

    The complete duration for the crawling of 8000 pages
    depends on the `DOWNLOAD_DELAY` settings parameter,
    according approximately to the formula::

        OPERATION_DURATION_MIN = DOWNLOAD_DELAY * 133

    So, if DOWNLOAD_DELAY is set to 5secs, then the duration in minutes is 133x5 = 665 mins (11h5m),
    while setting the DOWNLOAD_DELAY to 3 secs, the overall duration reduces to 399mins (6h39m).

    The parameter must be set to the minimum value that avoid IP blocking from the minint servers.


    loc_type: (REG|PROV|CM|COM)
    locations: an identifier for the location, depending on loc_type
      - REG: a name, case insensitive (ex: Toscana, toscana, TOSCANA, ...)
      - CM: a name, case insensitive (ex: Milano, MILANO,
      - PROV,CM: the prov identifier, case insensitive (ex: ROMA; roma, ...)
      - COM:
        - the prov, for all cities in that prov (ex: RM, rm)
        - the prov-code combination, for just that city (ex: RM-0900, rm-0900)
    """

    name = 'histadmin'
    allowed_domains = ['amministratori.interno.gov.it']
    start_urls = []
    min_electoral_date = None
    loc_type = None
    locations = None
    json_file = None

    def __init__(self, name=None, **kwargs):
        """based on arguments passed to scrapy spider,
        initialize loc_type, locations and start_urls,
        raising exceptions if arguments validation fails.
        """
        if 'min_electoral_date' not in kwargs:
            kwargs['min_electoral_date'] = '2012'

        if 'loc_type' not in kwargs:
            raise HistAdminSpiderException("The loc_type parameter must be specified")

        kwargs['loc_type'] = kwargs['loc_type'].upper()
        if kwargs['loc_type'] not in ['REG', 'PROV', 'CM', 'COM']:
            raise HistAdminSpiderException(
                "The loc_type parameter must take one of these values: REG, PROV, CM, COM"
            )

        if 'locations' in kwargs:
            kwargs['locations'] = kwargs['locations'].upper().split(":")

        super().__init__(self.name, **kwargs)

        if self.loc_type == 'REG':
            self.start_urls = ['https://amministratori.interno.gov.it/amministratori/ServletStoriaEnteR1']
        elif self.loc_type == 'CM':
            self.start_urls = ['https://amministratori.interno.gov.it/amministratori/ServletStoriaEnteM1']
        elif self.loc_type == 'PROV':
            self.start_urls = ['https://amministratori.interno.gov.it/amministratori/ServletStoriaEnteP1']
        else:
            self.start_urls = ['https://amministratori.interno.gov.it/amministratori/ServletStoriaEnte1']

    def parse(self, response):
        """Based on loc_type invokes the correct parsing logic

        All logics have the same pattern, except 'com' that is sligthly more complicated:
        - parse the URL,
        - read the values in the select html element
        - filter these values with the `location` argument
        - build a generator of post requests to the pages containing administrators details

        The administrators are actually parsed in the static `parse_administrators` callback,
        invoked when the request is answered.

        :param response: the response to self.start_urls, to be parsed
        :return: a generator containing all requests to parse,
        """
        if self.loc_type == 'REG':
            # fetch options values in the select widget
            select_values = response.css('select[name="regione"] > option::attr(value)').extract()

            # filter according to locationsparameter, if passed
            if getattr(self, 'locations', None):
                select_values = list(filter(
                    lambda v:
                        re.sub(r'[\d/]', '', v).replace('_', ' ').
                        replace('EMILIA ROMAGNA', 'EMILIA-ROMAGNA')
                        in getattr(self, 'locations').split(","),
                    select_values
                ))

            # build the generator
            for select_value in select_values:
                url = self.start_urls[0].replace('ServletStoriaEnteR1', 'ServletStoriaEnteR2')
                yield scrapy.FormRequest(
                    url=url, formdata={'regione': select_value, 'SubmitP': 'Conferma'},
                    callback=self.parse_administrators
                )
        elif self.loc_type == 'CM':
            # fetch options values in the select widget
            select_values = response.css('select[name="provincia"] > option::attr(value)').extract()

            # filter according to locations parameter, if passed
            if getattr(self, 'locations', None):
                select_values = list(filter(
                    lambda v:
                        re.sub(r'[\d/]', '', v).replace('_', ' ')
                        in getattr(self, 'locations').split(","),
                    select_values
                ))

            # build the generator
            for select_value in select_values:
                url = self.start_urls[0].replace('ServletStoriaEnteM1', 'ServletStoriaEnteM2')
                self.logger.info('Scraping {0} with regione {1}'.format(url, select_value))
                yield scrapy.FormRequest(
                    url=url, formdata={'provincia': select_value, 'SubmitP': 'Conferma'},
                    callback=self.parse_administrators
                )
        elif self.loc_type == 'PROV':
            # fetch options values in the select widget
            select_values = response.css('select[name="provincia"] > option::attr(value)').extract()

            # filter according to locationsparameter, if passed
            if getattr(self, 'locations', None):
                select_values = list(filter(
                    lambda v: v.split("*")[2] in getattr(self, 'locations').split(","), select_values
                ))

            # build the generator
            for select_value in select_values:
                url = self.start_urls[0].replace('ServletStoriaEnteP1', 'ServletStoriaEnteP2')
                self.logger.info('Scraping {0} with provincia {1}'.format(url, select_value))
                yield scrapy.FormRequest(
                    url=url, formdata={'provincia': select_value, 'SubmitP': 'Conferma'},
                    callback=self.parse_administrators
                )
        else:
            # This parser has a different behavior than the other three.
            #
            # It:
            # - reads the list of provinces from the ServletStoriaEnte1 page.
            # - filters the provinces, according to the locationsparameter
            # - builds a generator of post requests to the ServletStoriaEnte1,
            #   containing the selection of the comune in the list of all comuni in the province;
            #   the callback for this is parse_comuni_provincia

            # fetch options values for provinces, in the select widget
            select_values = response.css('select[name="provincia"] > option::attr(value)').extract()

            # filter according to locations parameter, if passed
            provs, codes, = [], []
            for location in getattr(self, 'locations', []):
                prov_code = location.upper().split('-')
                prov, code = prov_code[0], prov_code[1] if len(prov_code) > 1 else None
                provs.append(prov)
                codes.append(code)

            select_values = list(filter(
                lambda v: v.split("*")[2] in set(provs), select_values
            ))

            # build the intermediate generator, for comuni of a province lists
            for select_value in select_values:
                url = self.start_urls[0].replace('ServletStoriaEnte1', 'ServletStoriaEnte2')
                self.logger.info('Scraping {0} with provincia {1}'.format(url, select_value))
                form_request = scrapy.FormRequest(
                    url=url, formdata={'provincia': select_value, 'SubmitP': 'Conferma'},
                    callback=self.parse_comuni_provincia
                )
                form_request.meta['provs'] = provs
                form_request.meta['codes'] = codes
                yield form_request

    def parse_comuni_provincia(self, response):
        """Utility callback for parse_com,
        to generate the list of requests for all comuni in a province.

        All comuni pages are put in the generator to be parsed by `parse_administrators`.

        prov and code parameters are passed along in the response.meta attribute, as dict
        :param response:
        :return:
        """

        codes = response.meta['codes']

        # fetch options values in the select widget
        select_values = response.css('select[name="comune"] > option::attr(value)').extract()

        # filter according to locationsparameter, if passed
        if codes:
            select_values = list(filter(
                lambda v: v.split("*")[2] in set(codes), select_values
            ))

        # build the generator of administrators page to be requested
        for select_value in select_values:
            url = self.start_urls[0].replace('ServletStoriaEnte1', 'ServletStoriaEnte3')
            self.logger.info('Scraping {0} with comune {1}'.format(url, select_value))
            yield scrapy.FormRequest(
                url=url, formdata={
                    'comune': select_value, 'SubmitP': 'Conferma'
                },
                callback=self.parse_administrators
            )

    def parse_administrators(self, response):
        """The actual parser of the administrators page.

        A generator is built, by yealding a dictionary that contains
        the locations key and a list of administrators memberships, grouped by election date.

        :param response: the response to be parsed
        :return: a generator of a list of dictionaries
        """
        select_value_dict = urllib.parse.parse_qs(response.request.body.decode("utf-8"))
        if 'regione' in select_value_dict:
            select_value = select_value_dict['regione'][0]
            select_value = re.sub(r'[\d/]', '', select_value).replace('_', ' ').\
                replace('EMILIA ROMAGNA', 'EMILIA-ROMAGNA')
        elif 'provincia' in select_value_dict:
            if self.loc_type.upper() == 'PROV':
                select_values = select_value_dict['provincia'][0].split("*")
                select_value = ",".join([select_values[3], select_values[2]])
            else:
                select_value = select_value_dict['provincia'][0]
                select_value = re.sub(r'[\d/]', '', select_value).replace('_', ' ').\
                    replace('EMILIA ROMAGNA', 'EMILIA-ROMAGNA')
        else:
            select_values = select_value_dict['comune'][0].split("*")
            select_value = ",".join([select_values[1], select_values[2], select_values[4]])

        administrators = []
        headers = response.css("div.n_scrut_a")
        tables = response.css("div.t_afflscrut_a + div.raccoltadati table, div.t_afflscrut_a + br")
        elements = [
            dict(zip(('header', 'table'), element)) for element in zip(headers, tables)
        ]

        logger.info(select_value)

        for element in elements:
            location, election_date = element['header'].\
                css('div::text').extract_first().\
                replace(' - Data Elezione: ', '|').strip().split('|')
            electoral_event = {'election_date': election_date, 'administrators': []}
            rows_elements = element['table'].css('tr')
            logger.info("  election: {0} - {1} roles".format(election_date, len(rows_elements)))
            if len(rows_elements):
                labels = rows_elements[0].css('th::text').extract()
                for row_element in rows_elements[1:]:
                    cells_contents = row_element.css('td a::text').extract() + row_element.css('td::text').extract()
                    detail_url = row_element.css('td a::attr(href)').extract_first()
                    row_dict = dict(zip(map(lambda x: x.strip(), labels), cells_contents))
                    row_dict['person__name'] = "{0} {1}".format(
                        row_dict.pop('Nome'), row_dict.pop('Cognome')
                    )
                    row_dict['role'] = row_dict.pop('Carica')
                    row_dict['electoral_list_descr_tmp'] = row_dict.pop('Lista/Partito', None)
                    row_dict['start_date'] = row_dict.pop('Data entrata in carica')
                    row_dict['end_date'] = row_dict.pop('Data cessazione', None)
                    row_dict['end_reason'] = row_dict.pop('Stato carica', None)
                    row_dict['minint_detail_url'] = response.urljoin(detail_url)
                    row_dict['minint_identifier'] = urllib.parse.parse_qs(detail_url.split("?")[1])['campo1'][0]
                    electoral_event['administrators'].append(row_dict)
            administrators.append(electoral_event)

        yield {select_value: administrators}


class RolesHistAdminSpider(HistAdminSpider):
    """Spider used to scrape all roles in the Historic pages at MinInt

    Scraped results are sent to a DistinctRolesPipeline, that gather all roles,
    extract the distinct values and send them to a txt file.
    """
    name = 'roles_histadmin'

    custom_settings = {
        'ITEM_PIPELINES': {
            'minint.pipelines.histadmin.DistinctRolesPipeline': 100,
        }
    }
