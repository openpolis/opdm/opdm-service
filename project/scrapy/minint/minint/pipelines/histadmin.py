# -*- coding: utf-8 -*-

# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import copy
from copy import deepcopy
import csv
from datetime import datetime, timedelta
import io
import json
import os
from operator import itemgetter
from os.path import dirname, abspath
import re
import sys
import unicodedata

from django.db.models import Q

from ..core import convert_date, normalized_role, normalized_location_name

LOC_TYPES = {
    'reg':  'Regione',
    'prov': 'Provincia',
    'cm':   'Città Metropolitana',
    'com':  'Comune',
}


def days_between(d1, d2):
    d1 = datetime.strptime(d1, "%Y-%m-%d")
    d2 = datetime.strptime(d2, "%Y-%m-%d")
    return abs((d2 - d1).days)


class DistinctRolesPipeline(object):
    """
    Extract distinct roles from all pages, wirte them to a txt file
    """
    roles_seen = None
    file = None

    def open_spider(self):
        self.roles_seen = set()
        self.file = open('roles.txt', 'w')

    def close_spider(self):
        self.file.write("\n".join(list(self.roles_seen)))
        self.file.close()

    def process_item(self, item):
        self.roles_seen = self.roles_seen | set(
            [a['role'] for e in item[next(iter(item))] for a in e['administrators']]
        )
        return item


class TransformerPipeline(object):
    """
    Sort electoral groups in parsed item by electoral dates,
    change the structure adding information needed down the pipelines,
    group by electoral events,
    retrieve location info
    transform memberships, by:
     - removing accents
     - normalising roles
     - converting dates into proper format,
    ...
    """
    @staticmethod
    def process_item(item, spider):
        loc = next(iter(item))

        el_groups = list(
            filter(
                lambda x: convert_date(x['election_date']) > spider.min_electoral_date,
                sorted(
                    item[loc],
                    key=lambda x: convert_date(x['election_date'])
                )
            )
        )

        def get_location_info(loc_type: str, loc_identifier: str) -> tuple:
            prov, _loc_code, _loc_name = None, None, None

            if loc_type.upper() in ['REG', 'CM']:
                _loc_name = loc_identifier
            elif loc_type.upper() == 'PROV':
                _loc_name, prov = loc_identifier.split(',')
            else:
                _loc_prov, _loc_code, _loc_name = loc_identifier.split(',')
                m = re.match(r'.*\((.+)\)', _loc_prov)
                if len(m.groups()):
                    prov = m.group(1)
                else:
                    return None, None, None

            return prov, _loc_code, _loc_name

        def membership_transform(membership, _election_date):
            membership['election_date'] = _election_date

            # convert dates
            for k in ['start_date', 'end_date']:
                if membership[k].strip() == '-':
                    membership[k] = None
                else:
                    membership[k] = convert_date(membership[k])

            # lower text fields
            for k in [
                'person__name',
                'end_reason', 'electoral_list_descr_tmp'
            ]:
                if membership[k]:
                    membership[k] = membership[k].lower()

            # remove accents from names
            membership['person__name'] = unicodedata.normalize(
                'NFKD', membership['person__name']
            ).encode('ASCII', 'ignore').decode().\
                replace("a'", "a").replace("o'", "o").replace("i'", "i").replace("u'", "u").replace("e'", "e")

            # normalise role
            membership['role'] = normalized_role(membership['role'])

            # add empty fields or remove unneeded ones
            membership['membership_id'] = None
            membership['opdm_identifier'] = None
            membership.pop('minint_identifier', None)

            if not membership['end_date']:
                membership['end_reason'] = None

            return membership

        loc_prov, loc_code, loc_name = get_location_info(spider.loc_type, loc)

        processed_item = {
            'loc_identifier': loc,
            'loc_prov': loc_prov,
            'loc_code': loc_code,
            'loc_name': loc_name,
            'loc_type': spider.loc_type,
            'el_groups': []
        }

        for n, el_group in enumerate(el_groups):
            election_date = convert_date(el_group['election_date'])
            if n + 1 < len(el_groups):
                next_election_date = el_groups[n+1]['election_date']
                election_date_plus_term = convert_date(next_election_date)
            else:
                election_date_plus_term = (
                    datetime.strptime(
                        el_group['election_date'], '%d/%m/%Y'
                    ) + timedelta(days=5*365)
                ).strftime('%Y-%m-%d')

            # add duplicates records to minint_administrators, to account for duplicate roles
            # that may not be considered in the minint source
            # issue https://gitlab.depp.it/openpolis/opdm/opdm-project/issues/118
            roles_to_duplicate = {
                'Vicesindaco': 'Assessore comunale',
                'Vicepresidente del consiglio': 'Consigliere',
                'Presidente della regione': 'Consigliere'
            }
            group_administrators = el_group['administrators']
            for admin in el_group['administrators']:
                if admin['role'] in roles_to_duplicate.keys():
                    duplicate_admin = copy.deepcopy(admin)
                    duplicate_admin['role'] = roles_to_duplicate[admin['role']]
                    group_administrators.append(duplicate_admin)

            processed_item['el_groups'].append({
                'election_date': election_date,
                'election_date_plus_term': election_date_plus_term,
                'minint_administrators': [
                    membership_transform(administrator, election_date) for administrator in group_administrators
                ]
            })

        return processed_item


class OPDMLoaderPipeline(object):
    """Process items scraped from Minint, comparing them with items in OPDM.
    Modify when needed, logs discrpeancies ad records to be added or to be removed.
    """

    @staticmethod
    def process_item(item, spider):
        """Process a single scraped page (a location), after it has been transformed by TransformerPipeline.

        :param item:
        :param spider:
        :return:
        """

        # must hack sys.path, in order to setup django and import modules from the project
        sys.path.append(dirname(dirname(dirname(abspath('.')))))
        os.environ['DJANGO_SETTINGS_MODULE'] = 'config.settings'

        import django
        django.setup()

        from popolo.models import Membership, Area
        from project.scrapy.minint.minint.core import get_or_create_electoral_event
        from project.core.diff_utils import DiffHelper

        def remove_duplicates(minint_memberships, opdm_memberships):
            # filter out duplicated person + role
            minint_names_roles_sd = [(m['person__name'], m['role'], m['start_date']) for m in minint_memberships]
            opdm_names_roles_sd = [(m['person__name'], m['role'], m['start_date']) for m in opdm_memberships]

            d = {
                x: minint_names_roles_sd.count(x)
                for x in minint_names_roles_sd
                if minint_names_roles_sd.count(x) > 1 or opdm_names_roles_sd.count(x) > 1
            }
            if d != {}:
                minint_memberships = list(filter(
                    lambda x: (x['person__name'], x['role']) not in d,
                    minint_memberships
                ))
                spider.logger.warning(
                    "Duplicated records found in minint source. "
                    "May point to multiple charges in the same context, needs manual check."
                )
                for k in d.keys():
                    spider.logger.warning(" name: {0}, role: {1}".format(k[0], k[1]))

            d = {
                x: opdm_names_roles_sd.count(x)
                for x in opdm_names_roles_sd
                if opdm_names_roles_sd.count(x) > 1 or minint_names_roles_sd.count(x) > 1
            }
            if d != {}:
                opdm_memberships = list(filter(
                    lambda x: (x['person__name'], x['role'], x['start_date']) not in d,
                    opdm_memberships
                ))
                spider.logger.warning(
                    "Duplicated records found in opdm source. "
                    "May point to multiple charges in the same context, needs manual check."
                )
                for k in d.keys():
                    spider.logger.warning(" name: {0}, role: {1}, start_date: {2}".format(k[0], k[1], k[2]))

            return minint_memberships, opdm_memberships

        def get_memberships_minint_detail_urls(_opdm_memberships_qs):
            # create a dictionary of the source_url for each membership's id
            opdm_memberships_sources = _opdm_memberships_qs.filter(
                sources__source__note='amministratori.interno.gov.it'
            ).values('id', 'sources__source__url').distinct()
            return {
                m['id']: m['sources__source__url']
                for m in opdm_memberships_sources
            }

        def get_persons_other_names(_opdm_memberships_qs):
            # create a dictionary of all othernames for each person's id
            opdm_persons_with_other_names = _opdm_memberships_qs.filter(
                person__other_names__isnull=False
            ).values('person_id', 'person__other_names__name').distinct()
            _persons_other_names = {}
            for p in opdm_persons_with_other_names:
                person_id = p['person_id']
                if person_id not in _persons_other_names:
                    _persons_other_names[person_id] = []
                _persons_other_names[person_id].append(p['person__other_names__name'])
            return _persons_other_names

        def membership_transform(membership, _memberships_minint_detail_urls):
            membership['election_date'] = membership.pop('electoral_event__start_date')
            membership['membership_id'] = membership.pop('id')
            for k in [
                'person__name',
                'end_reason', 'role', 'electoral_list_descr_tmp'
            ]:
                if membership[k]:
                    membership[k] = membership[k].lower()

            # remove accents from names
            membership['person__name'] = unicodedata.normalize(
                'NFKD', membership['person__name']
            ).replace("\u2019", "'").encode('ASCII', 'ignore').decode().\
                replace("a'", "a").replace("o'", "o").replace("i'", "i").replace("u'", "u").replace("e'", "e")

            membership['role'] = normalized_role(membership['role'])
            membership['opdm_identifier'] = membership.pop('person_id')
            membership['minint_detail_url'] = _memberships_minint_detail_urls.get(membership['membership_id'], None)

            return membership

        def emit_csv(_io, rows, _fieldnames, delimiter=',', _persons_other_names=None):
            writer = csv.DictWriter(_io, delimiter=delimiter, fieldnames=_fieldnames)
            writer.writeheader()
            for row in rows:
                writer.writerow(row)

                # duplicate a row with other names (opdm case)
                if _persons_other_names and row.get('opdm_identifier', None) in _persons_other_names:
                    for name in _persons_other_names[row['opdm_identifier']]:
                        row['person__name'] = name.lower()
                        writer.writerow(row)

        def get_location(loc_type: str, loc_name: str = None, prov: str = None, loc_code: str = None) -> Area:
            """Fetch the correct location object given the right location info for the context"""
            normalized_loc_name = normalized_location_name(loc_name)
            if loc_type.upper() in ['REG', 'CM']:
                location = Area.objects.get(
                    istat_classification__iexact=loc_type,
                    name__iexact=normalized_loc_name
                )
            elif loc_type.upper() == 'PROV':
                location = Area.objects.filter(
                    Q(istat_classification__iexact='PROV') | Q(istat_classification__iexact='CM')
                ).get(identifier=prov)
            else:
                location = Area.objects.get(
                    istat_classification__iexact='COM',
                    parent__identifier=prov,
                    identifiers__scheme='MININT_ELETTORALE',
                    identifiers__identifier__endswith=loc_code
                )
            return location

        def update_opdm_record(l_update):
            """Verify that possible update is a real one.
            Do the update, if needed.
            Log what updates were performed and return the message to be logged.

            :param l_update: possible update
            :return: message
            """
            if opdm_identifier_field not in l_update:
                spider.logger.warning("could not find person id field in this update: {0}".format(
                    l_update
                ))
                return
            # person_id = int(l_update[opdm_identifier_field][0])
            membership_id = int(l_update[membership_id_field][0])
            # person = Person.objects.get(id=person_id)
            membership = Membership.objects.get(id=membership_id)

            # will have the URL where the information has been read
            update_source = None

            # copy likely updates into r_update, removing non-updateable fields
            r_update = deepcopy(l_update)
            r_update.pop(opdm_identifier_field)
            r_update.pop(membership_id_field)

            idx = "-"
            for k, v in l_update.items():

                # set idx value, for logging purposes
                if k == 'idx':
                    idx = v
                    r_update.pop('idx')
                    continue

                # get field label, from positional index
                field = fieldnames[k]

                # get possible update value
                v_new = v[1].strip()

                # skip updates for fields used to fetch person and membership from OPDM
                if field in ['opdm_identifier', 'membership_id']:
                    continue

                # avoid overwriting existing values (valid for all fields)
                if getattr(membership, field, None) is not None:
                    r_update.pop(fieldnames.index(field))
                    continue

                # set start or end dates for memberships
                if field in ['start_date', 'end_date']:

                    # avoid writing empty or undefined values, leaving it to None
                    if field == 'end_date' and (v_new == '-' or v_new.strip() == ''):
                        r_update.pop(fieldnames.index(field))
                        continue

                    setattr(membership, field, v_new)

                # set end_reason if different from proclamazione (it's a status in the minint page)
                if field == 'end_reason':

                    # avoid writing proclamazione as an end_reason
                    if v_new == 'proclamazione':
                        r_update.pop(fieldnames.index(field))
                        continue

                    membership.end_reason = v_new

                # set electoral list, if not null
                if field == 'electoral_list_descr_tmp':

                    # avoid writing empty or undefined values, leaving it to None
                    if v_new == '-' or v_new.strip() == '':
                        r_update.pop(fieldnames.index(field))
                        continue

                    membership.electoral_list_descr_tmp = v_new

                # set the update_source apart, to use it in case of a real update
                if field == 'minint_detail_url':
                    update_source = v_new.replace(" ", "+")
                    r_update.pop(fieldnames.index(field))

                # add electoral event
                if field == 'election_date':

                    membership.electoral_event = electoral_event

            # log real updates
            if r_update:
                messages = [
                    f"{fieldnames[k]}: {v[0].strip() if v[0] else '-'} => {v[1].strip() if v[1].strip() else '-'}"
                    for k, v in r_update.items()
                    if fieldnames[k] != 'minint_detail_url'
                ]
                if update_source:
                    messages.append("histadmin source added for this membership")
                    membership.add_source(
                        update_source,
                        note="Storia amministrativa dell'Ente, da amministratori.interno.gov.it"
                    )

                changes_str = "; ".join(messages)

                # person.save()
                membership.save()

                return f"{idx}; {changes_str}"

            return None

        processed_item = {
            'loc_identifier': item['loc_identifier'],
            'loc_prov': item['loc_prov'],
            'loc_code': item['loc_code'],
            'loc_name': item['loc_name'],
            'loc_type': item['loc_type'],
            'location_id': get_location(
                item['loc_type'],
                loc_name=item['loc_name'],
                prov=item['loc_prov'],
                loc_code=item['loc_code']
            ).id,
            'el_groups': item['el_groups']
        }

        for n, el_group in enumerate(processed_item['el_groups']):
            electoral_event = get_or_create_electoral_event(processed_item['loc_type'], el_group['election_date'])

            # extract context's memberships
            # only from consigli and giunte regionali
            opdm_memberships_qs = Membership.objects.filter(
                organization__parent__area_id=processed_item['location_id'],
                organization__classification__icontains=processed_item['loc_type'],
                start_date__gte=el_group['election_date'],
                start_date__lte=el_group['election_date_plus_term']
            ).select_related()

            el_group['opdm_administrators'] = list(opdm_memberships_qs.values(
                'electoral_event__start_date', 'id',
                'person__name', 'person_id',
                'role', 'start_date', 'end_date', 'end_reason',
                'electoral_list_descr_tmp'
            ))

            spider.logger.info("Results for {0} - election {1}".format(
                processed_item['loc_identifier'], el_group['election_date'])
            )

            # filter out memberships for same person and role in the context
            el_group['minint_administrators'], el_group['opdm_administrators'] = remove_duplicates(
                el_group['minint_administrators'], el_group['opdm_administrators']
            )

            # add duplicates records to opdm_administrators, to account for duplicate roles
            # that may not be considered in the minint source
            # issue https://gitlab.depp.it/openpolis/opdm/opdm-project/issues/118
            roles_to_duplicate = {
                'Vicesindaco': 'Assessore comunale',
                'Vicepresidente del consiglio': 'Consigliere',
                'Presidente della regione': 'Consigliere'
            }
            group_administrators = el_group['opdm_administrators']
            for admin in el_group['opdm_administrators']:
                if admin['role'] in roles_to_duplicate.keys():
                    duplicate_admin = copy.deepcopy(admin)
                    duplicate_admin['role'] = roles_to_duplicate[admin['role']]
                    group_administrators.append(duplicate_admin)

            el_group['opdm_administrators'] = group_administrators

            memberships_minint_detail_urls = get_memberships_minint_detail_urls(opdm_memberships_qs)
            persons_other_names = get_persons_other_names(opdm_memberships_qs)

            fieldnames = [
                'election_date', 'membership_id',
                'person__name',
                'opdm_identifier', 'minint_detail_url',
                'role', 'start_date', 'end_date', 'end_reason',
                'electoral_list_descr_tmp'
            ]

            minint_io = io.StringIO()
            emit_csv(
                minint_io,
                sorted(
                    el_group['minint_administrators'],
                    key=lambda x: ",".join([x['person__name'], x['role'], x['start_date']])
                ),
                fieldnames
            )

            opdm_io = io.StringIO()
            emit_csv(
                opdm_io,
                sorted(
                    [membership_transform(administrator, memberships_minint_detail_urls)
                     for administrator in el_group['opdm_administrators']],
                    key=lambda x: ",".join([x['person__name'], x['role'], x['start_date']])
                ),
                fieldnames,
                _persons_other_names=persons_other_names
            )

            index_columns = (
                fieldnames.index('person__name'),
                fieldnames.index('role'),
                fieldnames.index('start_date'),
            )
            adds, removes, updates = DiffHelper.csv_streams_diff(
                new=opdm_io, old=minint_io, sep=",",
                index_colums=index_columns, returns_deltas=True
            )

            # assign correct numerical indexes to fields used later
            opdm_identifier_field = fieldnames.index('opdm_identifier')
            membership_id_field = fieldnames.index('membership_id')

            # updates
            el_group['likely_updates'] = updates
            real_updates = []
            for update in updates:
                real_update = update_opdm_record(update)
                if real_update:
                    real_updates.append(real_update)

            if len(real_updates):
                spider.logger.info(f"{len(real_updates)} items were UPDATED:")
                for ru in real_updates:
                    spider.logger.info(f"  {ru}")
            else:
                spider.logger.info("no items were UPDATED")
            el_group['real_updates'] = real_updates

            # exclude adds and removes differing for just few days in the start_date field
            if len(adds) and len(removes):
                adds_indexes = [itemgetter(2, 5, 6)(i.split(",")) for i in adds]
                removes_indexes = [itemgetter(2, 5, 6)(i.split(",")) for i in removes]

                for add_index in adds_indexes:
                    for remove_index in removes_indexes:
                        if itemgetter(0, 1)(add_index) == itemgetter(0, 1)(remove_index):
                            if days_between(add_index[2], remove_index[2]) < 30:
                                # keep only other items (filter out fake differences)
                                adds = list(
                                    filter(lambda x: itemgetter(2, 5, 6)(x.split(",")) != add_index, adds)
                                )
                                removes = list(
                                    filter(lambda x: itemgetter(2, 5, 6)(x.split(",")) != remove_index, removes)
                                )

            # log adds and removes as warnings
            if len(adds):
                spider.logger.info("{0} items found in minint, not in opdm (should be ADDED)".format(len(adds)))
                for add in adds:
                    spider.logger.info("  {0}".format(add))
            el_group['adds'] = adds

            updated_persons_ids = set()
            for u in updates:
                try:
                    updated_persons_ids.add(int(u[membership_id_field][0]))
                except KeyError as err:
                    spider.logger.warning(f"{err} while adding {membership_id_field}")

            removes = list(filter(lambda r: int(r.split(",")[membership_id_field]) not in updated_persons_ids, removes))
            if len(removes):
                spider.logger.info("{0} items found in opdm, not in minint (should be REMOVED)".format(len(removes)))
                for remove in removes:
                    spider.logger.info("  {0}".format(remove))
            el_group['removes'] = removes

        return processed_item


class JSONEmitterPipeline(object):
    file = None

    def open_spider(self, spider):
        if hasattr(spider, 'json_file') and spider.json_file:
            self.file = open(spider.json_file, 'w')
            self.file.write("[\n")

    def close_spider(self, spider):
        if hasattr(spider, 'json_file') and spider.json_file:
            self.file.write("]\n")
            self.file.close()

    def process_item(self, item, spider):
        if hasattr(spider, 'json_file') and spider.json_file:
            line = json.dumps(dict(item), indent=4) + ",\n"
            self.file.write(line)
        return item
