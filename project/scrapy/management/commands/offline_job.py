# coding=utf-8
from copy import deepcopy
import csv
from datetime import datetime, timedelta
import io
import json
import logging
import re

from django.core.management.base import BaseCommand
from popolo.models import Membership, Person

from project.scrapy.minint.minint.core import convert_date, get_or_create_electoral_event, normalized_role
from project.core.diff_utils import DiffHelper


class Command(BaseCommand):
    """This script tests the logic of extraction and comparison between data scraped from MinInt,
    "Storia amministrativa dell'ente" and the OPDM data

    It is used to avoid making continuous requests to MinInt, resulting in IP blocking.
    """
    help = 'Work offline on items parsed by scrapy'
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            '--items-file',
            dest='items_file',
            default="./resources/data/items.json",
            help='Control whether to drop existing rows from database',
        )
        parser.add_argument(
            'location',
            nargs="?",
            help='Select a location. If not selected, then a list of locations is printed.'
        )
        parser.add_argument(
            '--loc-type',
            dest='loc_type',
            required=True,
            help="Select a location type (regione|provincia|metropoli|comune)"
        )
        parser.add_argument(
            '--min-electoral-date',
            dest='min_electoral_date',
            default="1900-01-01",
            help="Define a minimum electoral date to parse. If not set, then parse all."
        )
        parser.add_argument(
            '--dump-csv-tables',
            action='store_true',
            dest='dump_csv_tables',
            help="Dump csv tables to files for debugging purposes"
        )

    def handle(self, *args, **options):
        verbosity = options['verbosity']
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        location = options.get('location', None)
        loc_type = options.get('loc_type')
        dump_csv_tables = options.get('dump_csv_tables')
        min_electoral_date = options.get('min_electoral_date')

        items_file = options['items_file']
        with open(items_file) as json_items:
            items = json.load(json_items)

        self.logger.info("Starting")

        if not location:
            self.logger.info("Choose among these {0} items".format(len(items)))

        for item in items:
            k = list(item.keys())[0]
            loc = re.sub(r'[\d/]', '', k).replace('_', ' ').replace('EMILIA ROMAGNA', 'EMILIA-ROMAGNA')
            if not location:
                self.logger.info(loc)
            else:
                if location != loc:
                    continue

                self.logger.info("Processing location: {0}".format(location))
                fieldnames = [
                    'election_date', 'membership_id',
                    'person__name',
                    'opdm_identifier', 'minint_detail_url',
                    'role', 'start_date', 'end_date', 'end_reason',
                    'electoral_list_descr_tmp'
                ]

                # sort electoral events by ascending date
                # and filters only electoral dates after given one
                el_groups = list(
                    filter(
                        lambda x: convert_date(x['election_date']) > min_electoral_date,
                        sorted(
                            item[k],
                            key=lambda x: convert_date(x['election_date'])
                        )
                    )
                )
                for n, el_group in enumerate(el_groups):

                    election_date = datetime.strptime(
                        el_group['election_date'], '%d/%m/%Y'
                    ).strftime('%Y-%m-%d').strip()

                    if n + 1 < len(el_groups):
                        next_election_date = el_groups[n+1]['election_date']
                        election_date_plus_term = datetime.strptime(
                            next_election_date, '%d/%m/%Y'
                        ).strftime('%Y-%m-%d').strip()
                    else:
                        election_date_plus_term = datetime.strptime(
                            el_group['election_date'], '%d/%m/%Y'
                        ) + timedelta(days=5*365)

                    electoral_event = get_or_create_electoral_event(loc_type, election_date)

                    #
                    # build minint data table, as CSV, into minint_io stream
                    #
                    minint_memberships = []
                    for membership in el_group['administrators']:

                        membership['election_date'] = election_date
                        membership['opdm_identifier'] = None
                        for k in ['start_date', 'end_date']:
                            if membership[k].strip() == '-':
                                membership[k] = None
                            else:
                                membership[k] = datetime.strptime(
                                    membership[k], '%d/%m/%Y'
                                ).strftime('%Y-%m-%d').strip()
                        for k in [
                            'person__name',
                            'end_reason', 'electoral_list_descr_tmp'
                        ]:
                            if membership[k]:
                                membership[k] = membership[k].lower()

                        membership['role'] = normalized_role(membership['role'])
                        membership['membership_id'] = None
                        membership.pop('minint_identifier', None)

                        if not membership['end_date']:
                            membership['end_reason'] = None

                        minint_memberships.append(membership)

                    #
                    # build opdm data table, as CSV, into minint_io stream
                    #

                    # extract context's memberships
                    opdm_memberships_qs = Membership.objects.filter(
                        organization__parent__name__icontains=loc,
                        organization__parent__classification__iexact=loc_type,
                        start_date__gte=election_date, start_date__lte=election_date_plus_term
                    ).select_related()

                    opdm_memberships = opdm_memberships_qs.values(
                        'electoral_event__start_date', 'id',
                        'person__name', 'person_id',
                        'role', 'start_date', 'end_date', 'end_reason',
                        'electoral_list_descr_tmp'
                    )

                    # filter out duplicated person + role
                    minint_names_roles = [(m['person__name'], m['role']) for m in minint_memberships]
                    opdm_names_roles = [(m['person__name'], m['role']) for m in opdm_memberships]

                    d = {
                        x: minint_names_roles.count(x)
                        for x in minint_names_roles
                        if minint_names_roles.count(x) > 1 or opdm_names_roles.count(x) > 1
                    }
                    if d != {}:
                        minint_memberships = list(filter(
                            lambda x: (x['person__name'], x['role']) not in d,
                            minint_memberships
                        ))
                        self.logger.warning(
                            "Duplicated records found in minint source. "
                            "May point to multiple charges in the same context, needs manual check."
                        )
                        for k in d.keys():
                            self.logger.warning(" name: {0}, role: {1}".format(k[0], k[1]))

                    d = {
                        x: opdm_names_roles.count(x)
                        for x in opdm_names_roles
                        if opdm_names_roles.count(x) > 1 or minint_names_roles.count(x) > 1
                    }
                    if d != {}:
                        opdm_memberships = list(filter(
                            lambda x: (x['person__name'], x['role']) not in d,
                            opdm_memberships
                        ))
                        self.logger.warning(
                            "Duplicated records found in opdm source. "
                            "May point to multiple charges in the same context, needs manual check."
                        )
                        for k in d.keys():
                            self.logger.warning(" name: {0}, role: {1}".format(k[0], k[1]))

                    # create a dictionary of the source_url for each membership's id
                    opdm_memberships_sources = opdm_memberships_qs.filter(
                        sources__source__note='amministratori.interno.gov.it'
                    ).values('id', 'sources__source__url').distinct()
                    memberships_minint_detail_urls = {
                        m['id']: m['sources__source__url']
                        for m in opdm_memberships_sources
                    }

                    # create a dictionary of all othernames for each person's id
                    opdm_persons_with_other_names = opdm_memberships_qs.filter(
                        person__other_names__isnull=False
                    ).values('person_id', 'person__other_names__name').distinct()
                    persons_other_names = {}
                    for p in opdm_persons_with_other_names:
                        person_id = p['person_id']
                        if person_id not in persons_other_names:
                            persons_other_names[person_id] = []
                        persons_other_names[person_id].append(p['person__other_names__name'])

                    # generate the actual CSV tables,
                    # injecting detail_url and duplicating rows when other names are present
                    minint_io = io.StringIO()
                    writer = csv.DictWriter(minint_io, delimiter=",", fieldnames=fieldnames)
                    writer.writeheader()
                    for membership in minint_memberships:
                        writer.writerow(membership)

                    opdm_io = io.StringIO()
                    writer = csv.DictWriter(opdm_io, delimiter=",", fieldnames=fieldnames)
                    writer.writeheader()
                    for membership in opdm_memberships:
                        membership['election_date'] = membership.pop('electoral_event__start_date')
                        membership['membership_id'] = membership.pop('id')
                        for k in [
                            'person__name',
                            'end_reason', 'role', 'electoral_list_descr_tmp'
                        ]:
                            if membership[k]:
                                membership[k] = membership[k].lower()

                        membership['role'] = normalized_role(membership['role'])
                        membership['opdm_identifier'] = membership.pop('person_id')
                        membership['minint_detail_url'] = memberships_minint_detail_urls.get(
                            membership['membership_id'], None
                        )

                        writer.writerow(membership)

                        # duplicate a row with other names
                        if membership['opdm_identifier'] in persons_other_names:
                            for name in persons_other_names[membership['opdm_identifier']]:
                                membership['person__name'] = name.lower()
                                writer.writerow(membership)

                    index_columns = (
                        fieldnames.index('person__name'),
                        fieldnames.index('role'),
                    )

                    def sortkey_by_index_columns(row):
                        """

                        :param row: the row to be sorted
                        :return: the sort key
                        """
                        fields = row.split(",")
                        return ",".join([fields[index_columns[0]], fields[index_columns[1]]])

                    # create sorted IO
                    sorted_minint_io = io.StringIO()
                    sorted_opdm_io = io.StringIO()
                    minint_io.seek(0)
                    opdm_io.seek(0)
                    sorted_minint_io.writelines(sorted(minint_io.readlines(), key=sortkey_by_index_columns))
                    sorted_opdm_io.writelines(sorted(opdm_io.readlines(), key=sortkey_by_index_columns))

                    del minint_io
                    del opdm_io

                    # set this to True to dump content of minint and opdm tables, as csv files
                    # it may help in debugging
                    if dump_csv_tables:
                        sorted_minint_io.seek(0)
                        sorted_opdm_io.seek(0)
                        with open('resources/data/minint.csv', 'w') as f:
                            f.writelines(sorted(sorted_minint_io.readlines()))
                        with open('resources/data/opdm.csv', 'w') as f:
                            f.writelines(sorted(sorted_opdm_io.readlines()))

                    sorted_minint_io.seek(0)
                    sorted_opdm_io.seek(0)
                    adds, removes, updates = DiffHelper.csv_streams_diff(
                        new=sorted_opdm_io, old=sorted_minint_io,
                        sep=",", index_colums=index_columns, returns_deltas=True
                    )

                    self.logger.info("Results for {0} - {1}".format(location, election_date))

                    self.logger.info(
                        "{0} items found to be likely updated:".format(len(updates))
                    )

                    # assign correct numerical indexes to fields used later
                    opdm_identifier_field = fieldnames.index('opdm_identifier')
                    membership_id_field = fieldnames.index('membership_id')

                    # proceeding to updates
                    for update in updates:
                        if opdm_identifier_field not in update:
                            self.logger.warning("could not find person id field in this update: {0}".format(
                                update
                            ))
                            continue
                        person_id = int(update[opdm_identifier_field][0])
                        membership_id = int(update[membership_id_field][0])
                        person = Person.objects.get(id=person_id)
                        membership = Membership.objects.get(id=membership_id)

                        # copy likely updates into real_update, removing non-updateable fields
                        real_update = deepcopy(update)
                        real_update.pop(opdm_identifier_field)
                        real_update.pop(membership_id_field)

                        idx = "-"
                        for k, v in update.items():

                            # set idx value, for logging purposes
                            if k == 'idx':
                                idx = v
                                real_update.pop('idx')
                                continue

                            # get field label, from positional index
                            field = fieldnames[k]

                            # get possible update value
                            v_new = v[1].strip()

                            # skip updates for fields used to fetch person and membership from OPDM
                            if field in ['opdm_identifier', 'membership_id']:
                                continue

                            # set start or end dates for memberships
                            if field in ['start_date', 'end_date']:
                                if field == 'end_date' and v_new == '-':
                                    continue
                                setattr(membership, field, v_new)

                            # set electoral list, if not null
                            if field == 'electoral_list_descr_tmp':
                                if v_new != '-':
                                    membership.electoral_list_descr_tmp = v_new
                                else:
                                    real_update.pop(fieldnames.index('electoral_list_descr_tmp'))
                                    continue

                            # set end_reason if different from proclamazione (it's a status in the minint page)
                            if field == 'end_reason':
                                if v_new != 'proclamazione':
                                    membership.end_reason = v_new
                                else:
                                    real_update.pop(fieldnames.index('end_reason'))
                                    continue

                            # add minint_identifier to the person
                            if field == 'minint_detail_url':
                                membership.add_source(v_new, note='amministratori.interno.gov.it')

                            # add electoral event
                            if field == 'election_date':
                                membership.electoral_event = electoral_event

                        # log real updates
                        if real_update:
                            changes_str = "; ".join(
                                "{0}: {1} => {2}".format(
                                    fieldnames[k], v[0].strip() if v[0] else "-", v[1].strip() if v[1].strip() else "-"
                                ) for k, v in real_update.items()
                                if fieldnames[k] != 'minint_detail_url'
                            )
                            self.logger.info("{0}; {1}".format(idx, changes_str))

                        person.save()
                        membership.save()

                    updated_persons_ids = set(int(u[membership_id_field][0]) for u in updates)

                    # log adds and removes as warnings
                    if len(adds):
                        self.logger.warning(
                            "\n{0} items found in minint, not in opdm:\n{1}\n".format(len(adds), "\n".join(adds)))

                    removes = list(filter(
                        lambda r: int(r.split(",")[membership_id_field]) not in updated_persons_ids, removes
                    ))
                    if len(removes):
                        self.logger.warning(
                            "{0} items found in opdm, not in minint:\n{1}\n\n".format(
                                len(removes), "\n".join(removes)
                            )
                        )

                break

        self.logger.info("Finished!")
