# coding=utf-8

import logging
import subprocess

from django.conf import settings

from popolo.models import Area

from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """Scrape hitoric of administrative institutions pages from minint

    Ues scrapy crawl hist_admin.

    Warning: it is theoretically possible to launch this task using COM as loc_type argument,
    and it will start parsing the totality of pages for all municipalities in Italy, but that will
    take like forever, so it is bettere to limit it with some filters.

    """
    help = 'Scrape hitory of administrative institutions pages from minint'
    requires_migrations_checks = True
    requires_system_checks = True
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            'loc_type',
            nargs="?",
            help='The type of location to scrape (REG|PROV|CM|COM).',
        )
        parser.add_argument(
            '--json-file',
            dest='json_file',
            help='Complete JSON file path.'
        )
        parser.add_argument(
            '--log-file',
            dest='logfile',
            help='Scrapy log file, complete path.'
        )
        parser.add_argument(
            '--locations',
            dest='locations',
            help="The locations, expressed as case insensitive comma-separated strings: "
            "names (Sicilia,Roma,MILANO), for REG and CM; "
            "prov initials (RM,mi) for PROV and COM;"
            "single prov-code (RM-0900) for COM"
        )
        parser.add_argument(
            "--inhabitants-range",
            dest="inhabitants_range",
            default='0+',
            help="Inhabitants range. Express it as 100k-1M, 500k+, 50k-, 0-50k",
        )
        parser.add_argument(
            "--min-electoral-date",
            dest="min_electoral_date",
            default='2012',
            help="Only electoral events after this date will be cosidered.",
        )

    def handle(self, *args, **options):
        """Contains the command logic.
        Launch the scrapy crawl histadmin subprocess and log the output.

        :param args:
        :param options:
        :return:
        """
        self.setup_logger(__name__, formatter_key="simple", **options)

        loc_type = options['loc_type']
        locations = options.get('locations', None)
        inhabitants_range = self.parse_range(options['inhabitants_range'])
        min_electoral_date = options['min_electoral_date']

        loglevel = logging.getLevelName(self.logger.level)
        logfile = options.get('logfile', None)
        json_file = options.get('json_file', None)

        if logfile and logfile[-4:] != '.log':
            raise Exception("logfile must end in .log")

        if json_file and json_file[-5:] != '.json':
            raise Exception("json_file must end in .json")

        try:
            cmd = [
                "scrapy", "crawl", "histadmin",
                f"--loglevel={loglevel}",
                "-a", f"min_electoral_date={min_electoral_date}",
                "-a", f"loc_type={loc_type}",
            ]
            cwd = f"{settings.ROOT_PATH}/project/scrapy/minint/"
            if loc_type != "COM" or locations:
                if locations:
                    cmd.extend(["-a", f"locations={locations}"])

                if json_file:
                    cmd.extend(["-a", f"json_file={json_file}"])

                if logfile:
                    cmd.append(f"--logfile={logfile}")

                self.logger.info(f"Executing scrapy command: {cmd}")
                for log_line in self.execute_subprocess(cmd, cwd):
                    self.emit_scrapy_log_line(log_line)
                self.logger.info("Finished!")

            else:
                # extract locations from areas, by inhabitants
                min_inh, max_inh = inhabitants_range
                self.logger.info(f"Scraping history for cities in this range: {inhabitants_range}\n")

                locs = Area.objects.filter(
                    istat_classification=Area.ISTAT_CLASSIFICATIONS.comune,
                    inhabitants__gte=min_inh, inhabitants__lte=max_inh,
                    identifiers__scheme="MININT_ELETTORALE"
                )\
                    .select_related("parent")\
                    .order_by('-inhabitants')\
                    .values("name", "parent__identifier", "identifiers__identifier", "inhabitants")

                grouped_locs = {}
                for loc in locs:
                    grouped_locs.setdefault(loc['parent__identifier'], []).append(
                        {
                            'name': loc['name'],
                            'code': f"{loc['parent__identifier']}-{loc['identifiers__identifier'][-4:]}"
                        }
                    )

                for prov, cities in grouped_locs.items():
                    prov_cmd = cmd.copy()
                    prov_cmd.extend(["-a", f"locations={':'.join([x['code'] for x in cities])}"])
                    if json_file:
                        prov_json_file = json_file.replace(".json", f"_{prov}.json")
                        prov_cmd.extend(["-a", f"json_file={prov_json_file}"])
                    if logfile:
                        prov_logfile = logfile.replace(".log", f"_{prov}.log")
                        cmd.append(f"--logfile={prov_logfile}")

                    self.logger.info(f"Executing scrapy command for province {prov}: {prov_cmd}")
                    for log_line in self.execute_subprocess(prov_cmd, cwd):
                        self.emit_scrapy_log_line(log_line)
                    self.logger.info("Finished!\n----")

        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."

    @staticmethod
    def execute_subprocess(cmd: list, cwd: str):
        """Generator that executes a command and yields output and error lines
        Raises CalledProcessError exception if return_code is not 0

        :param cmd: the command and arguments list
        :param cwd: the current working directory, as absolute path
        :return: a generator containing all log lines of processed items
        """
        popen = subprocess.Popen(
            cmd, cwd=cwd,
            stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
            universal_newlines=True
        )
        for stdout_line in iter(popen.stdout.readline, ""):
            yield stdout_line
        popen.stdout.close()
        return_code = popen.wait()
        if return_code:
            raise subprocess.CalledProcessError(return_code, cmd)

    def emit_scrapy_log_line(self, log_line: str):
        """Parse a log line produced by scrapy and re-log it using the internal logger

        :param self:     the Command instance, where the logger is defined
        :param log_line: the line to be parsed
        """
        line = log_line.strip()
        if 'DEBUG:' in line:
            self.logger.debug(line)
        elif 'INFO:' in line:
            self.logger.info(line)
        elif 'WARNING:' in line:
            self.logger.warning(line)
        elif 'ERROR:' in line:
            self.logger.info(line)

    @staticmethod
    def parse_range(range_str):
        """Transform a string into a proper range.

        Possible formats are:
        - min:max - from min to max
        - min+    - more than min
        - max-    - less than max

        Available values multipliers are:
        - k, K (*1 thousand)
        - m, M (*1 million)

        The returned range is verified, min must be less than max.

        If format could not be parsed or values are not sequentials,
        an exception is raised.

        :param range_str: the string, in one of the accepted formats
        :return: a tuple, with two integers (min, max)
        """
        def suffix_parser(v):
            if v.lower()[-1] == 'k':
                return int(v.lower().strip('k+-')) * 1000
            elif v.lower()[-1] == 'm':
                return int(v.lower().strip('m+-')) * 1000000
            else:
                return int(v)

        if ":" in range_str:
            r_min, r_max = map(suffix_parser, range_str.split(":"))
        elif "+" in range_str:
            r_min, r_max = suffix_parser(range_str.strip("+")), 1e+18
        elif "-" in range_str:
            r_min, r_max = 0, suffix_parser(range_str.strip("-"))
        else:
            raise Exception("Bad range format, use one of: min:max, min+, max-")

        if r_min >= r_max:
            raise Exception("Bad range, min must be less than max.")

        return r_min, r_max
