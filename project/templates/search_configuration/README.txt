The schema template for Solr 7 (haystack's default is not compatible).

Used in order to generate the schema.xml file to be used by the solr instance
of this application.

Solr ``solrconfig.xml`` and ``schema.xml`` file need to be compatible,
the files that need to be used are under ``config/solr/``.
