from rest_framework_simplejwt.serializers import TokenObtainSlidingSerializer
from rest_framework_simplejwt.views import TokenObtainSlidingView


class CustomTokenObtainSlidingSerializer(TokenObtainSlidingSerializer):
    """Custom ``TokenObtainSlidingSerializer`` specialization,
    that adds opdm and hasura *claims* to the token.

    The keys used are:
    - https://openpolis.io/jwt/claims
    - https:://hasura.io/jwt/claims

    These claims help identifying authenticated users' roles and permissions in client applications.
    """
    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)

        # Add hasura-* claims
        roles = list(user.groups.values_list('name', flat=True))
        if len(roles):
            default_role = roles[0]
        else:
            default_role = None

        hasura_claims = {
            "x-hasura-allowed-roles": roles,
            "x-hasura-default-role": default_role,
          }
        token["https://hasura.io/jwt/claims"] = hasura_claims

        opdm_claims = {
            "groups": roles
        }
        token["https://openpolis.io/jwt/claims"] = opdm_claims

        return token


class CustomTokenObtainSlidingView(TokenObtainSlidingView):
    """Custom ``TokenObtainSlidingView`` spcialization,
    that uses the ``CustomTokenObtainSlidingSerializer`` as serializer."""
    serializer_class = CustomTokenObtainSlidingSerializer
