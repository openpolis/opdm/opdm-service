import inspect

from ooetl.loaders import Loader
import pandas as pd


class DummyLoader(Loader):
    def load(self, **kwargs):
        """works if self.etl.processed_data is an iterator or a pandas.DataFrame

        :param kwargs:
        :return:
        """

        if isinstance(self.etl.processed_data, pd.DataFrame):
            items = self.etl.processed_data.to_dict("records")
        else:
            items = self.etl.processed_data
        for item in items:
            self.etl.logger.info(item)


class PopoloLoader(Loader):
    """:class:`Loader` that stores  data in ``popolo`` models

    This is the base class, that needs to be subclassed.

    It defines a generic `load` method that loops over the
    `etl.processed_data` and apply `load_item` to each of the records.

    The `load_item` method needs to be subclassed, in order to
    take into consideration the specifics of the destination model.
    """

    log_step = None

    def __init__(self, **kwargs):
        self.log_step = kwargs.pop("log_step", 500)
        super(PopoloLoader, self).__init__()

    def load(self, **kwargs):
        """Load data."""

        # TODO: just assume `etl.processed_data` is an iterable (?);
        #   there is no need to create a list...
        if isinstance(self.etl.processed_data, pd.DataFrame):
            items = self.etl.processed_data.to_dict(orient="records")
        elif inspect.isgenerator(self.etl.processed_data):
            items = self.etl.processed_data
        else:
            items = list(self.etl.processed_data)

        for n, item in enumerate(items):

            try:
                self.load_item(item)
            except Exception as e:
                self.logger.error(f"{e} catched while importing {item}")

            if n % self.log_step == 0:
                if inspect.isgenerator(items):
                    self.logger.info("{0}".format(n))
                else:
                    self.logger.info("{0}/{1}".format(n, len(items)))

    def load_item(self, item, **kwargs):
        raise Exception("Need to be subclassed")
