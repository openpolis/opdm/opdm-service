from typing import Dict

from project.core import keyevent_utils
from project.tasks.etl.loaders import PopoloLoader


class PopoloKeyEventLoader(PopoloLoader):
    """:class:`Loader` that stores  data in ``popolo.KeyEvent`` instances

    The generic `load` method can still be overridden in subclasses,
    should peculiar necessities arise (as bulk loading).
    """

    def __init__(self, **kwargs):
        """Create a new instance of the loader

        Args:
            lookup_strategy: anagraphical, identifier, mixed select the lookup strategy to use
            update_strategy: how to update the Organization
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
        """
        super(PopoloKeyEventLoader, self).__init__(**kwargs)
        self.lookup_strategy = kwargs.get("lookup_strategy", "mixed")
        self.update_strategy = kwargs.get("update_strategy", "keep_old")

    def load_item(self, item: Dict, **kwargs):
        """
        Load item.

        :param item: a dictionary representing a `KeyEvent`
        :param kwargs:
        :return:
        """

        ke_id = keyevent_utils.keyevent_lookup(item, self.lookup_strategy)

        if ke_id >= 0:
            try:
                # invoke class method to create or update a person from a dict (item)
                ke_id, created = keyevent_utils.update_or_create_keyevent_from_item(
                    item, ke_id, update_strategy=self.update_strategy
                )
            except Exception as e:
                self.logger.error(f"{e} when loading organization {item}")
            else:
                # TODO: KeyEventsUtils.sync_solr_index(o)
                if created:
                    self.logger.info(f"New key event created: {ke_id}")
                else:
                    self.logger.info(f"Key event {ke_id} updated.")
        else:
            self.logger.warning(f"{ke_id} organizations found for item {item}")

        return ke_id
