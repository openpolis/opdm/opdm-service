from io import StringIO
import re

from django.conf import settings
import numpy as np
from popolo.models import (
    RoleType,
    Classification,
    Organization,
    Post,
    KeyEvent,
    Person,
)

from project.core import organization_utils
from project.core import person_utils
from project.core.exceptions import UnprocessableEntitytAPIException
from project.tasks.etl.loaders import PopoloLoader


class PopoloRoleTypeLoader(PopoloLoader):
    """:class:`Loader` that stores records in ``popolo.RoleType`` instances

    """

    def load_item(self, item, **kwargs):
        descr_classification = item["forma_giuridica_op"]

        # transform processed data into a list of dicts
        self.logger.debug("{0} in {1}".format(item["role_label"], descr_classification))

        # get or create classification
        try:
            c = Classification.objects.get(
                scheme="FORMA_GIURIDICA_OP", descr=descr_classification
            )
        except Classification.DoesNotExist:
            self.logger.error(
                "Could not find classification FORMA_GIURIDICA_OP: {0}".format(
                    descr_classification
                )
            )
        else:
            # get_or_create the Organization
            r, created = RoleType.objects.get_or_create(
                label=item["role_label"],
                classification=c,
                defaults={
                    "priority": item["priority"],
                    "other_label": item["role_short_label"]
                    if item["role_short_label"] is not np.nan
                    else None,
                },
            )
            if created:
                self.logger.debug("RoleType {0} created".format(r))
            else:
                self.logger.debug("RoleType {0} found and updated".format(r))


class PopoloPersonLoader(PopoloLoader):
    """:class:`Loader` that stores persons into Popolo models

    This is an instance of a Popolo Loader, that perform loading of Person's related data
    Each item that is processed has a well defined structure.

    A strategy to lookup the person needs to be specified when instantiating the loader.
    If not specified, the default strategy will be to look the Person using the anagraphical data
    i.e. family_name, given_name, birth_date, birth_location, and generating AKAS when more than one results are found

    If the `lookup_strategy` is set to *identifier*, then the lookup is performed precisely, using the
    specified `identifier_scheme` parameter. If not specifie, then a lookup on the internal OPDM result is performed.

    All lookups are performed through calls to solr.

    In order to minimize the calls to solr,
    the scritps using this loader need to be called with the HAYSTACK_SIGNAL_PROCESSOR
    variable set to haystack.signals.BaseSignalProcessor,
    as the index updating is performed at low-level, only after a Person is correctly created,
    with all her associated values.
    """

    def __init__(self, **kwargs):
        """Create a new instance of the loader

        Args:
            context: a loader context to be written into the AKAS, when created
            lookup_strategy: anagraphical or identifier, select the lookup strategy to use
            identifier_scheme: the scheme to use whith the identifier lookup strategy (OP_ID, CF, ...)
            update_strategy: how to update the Person
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
            aka_lo: threshold below which a similarity is discarded
            aka_hi: threshold above which a similarity is considered identical

        """
        super(PopoloPersonLoader, self).__init__(**kwargs)
        self.context = kwargs.get("context", "")
        self.lookup_strategy = kwargs.get("lookup_strategy", "anagraphical")
        self.identifier_scheme = kwargs.get("identifier_scheme", None)
        self.update_strategy = kwargs.get("update_strategy", "keep_old")
        self.aka_lo = kwargs.get("aka_lo", settings.AKA_LO_THRESHOLD)
        self.aka_hi = kwargs.get("aka_lo", settings.AKA_HI_THRESHOLD)

    def load_item(self, item, **kwargs):
        """load Person into the Popolo models
        lookup a person, with strategy defined in self.lookup_strategy
        invoke update_or_create_from_item (anagraphical data plus identifiers, contacts, ...)

        :param item: the item to be loaded
        :return:
        """

        loader_context = {
            "module": __name__,
            "class": self.__class__.__name__,
            "data_source": self.etl.source,
            "context": self.context,
            "item": item,
        }
        person_id = person_utils.person_lookup(
            item,
            self.lookup_strategy,
            loader_context,
            identified_scheme=self.identifier_scheme,
            aka_lo=self.aka_lo,
            aka_hi=self.aka_hi,
            logger=self.logger,
        )
        if person_id >= 0:
            try:
                # invoke class method to create or update a person from a dict (item)
                p, created = person_utils.update_or_create_person_from_item(
                    item, person_id, update_strategy=self.update_strategy
                )
            except Exception as e:
                self.logger.error("{0} when loading person {1}".format(e, item))
            else:
                person_utils.sync_solr_index(p)


class PopoloMembershipLoader(PopoloLoader):
    """:class:`Loader` that stores memberships instances into the OPDM DB
    TODO: WARNING! this loader does not handle AKAs creation

    """

    def __init__(self, **kwargs):
        self.update_strategy = kwargs.get("update_strategy", "keep_old")
        self.check_membership_label = kwargs.get("check_membership_label", False)

        super().__init__()

    def load(self, **kwargs):
        """Overrides default load, by fetching the Areas,
        used to translate cadraste codes into city names or
        accessing the parents.

        Areas' information are stored in a dict, so that a single complex
        query is performed and no further accesses are required during
        import operations.
        """
        # processed data are grouped by region and
        if 'election_classification' in self.etl.processed_data.columns:
            grouped_df = self.etl.processed_data.groupby(
                [
                    "loc_code",
                    "loc_desc",
                    "election_date",
                    "election_name",
                    "election_classification",
                    "charge_desc",
                    "org_id",
                    "org_scheme",
                    "org_classification",
                    "org_name",
                    "org_valid_at",
                    "role_type_label",
                    "post_label",
                ]
            )
        else:
            grouped_df = self.etl.processed_data.groupby(
                [
                    "loc_code",
                    "loc_desc",
                    "election_date",
                    "election_name",
                    "charge_desc",
                    "org_id",
                    "org_scheme",
                    "org_classification",
                    "org_name",
                    "org_valid_at",
                    "role_type_label",
                    "post_label",
                ]
            )

        for loc_charge, group in grouped_df:

            if 'election_classification' in self.etl.processed_data.columns:
                (
                    loc_code,
                    loc_desc,
                    election_date,
                    election_name,
                    election_classification,
                    charge_desc,
                    org_id,
                    org_scheme,
                    org_classification,
                    org_name,
                    org_valid_at,
                    role_type_label,
                    post_label,
                ) = loc_charge
            else:
                (
                    loc_code,
                    loc_desc,
                    election_date,
                    election_name,
                    charge_desc,
                    org_id,
                    org_scheme,
                    org_classification,
                    org_name,
                    org_valid_at,
                    role_type_label,
                    post_label,
                ) = loc_charge
                election_classification = None

            if election_name != "":
                if election_classification is None:
                    # get or create a minimal KeyEvent instance
                    if "comunali" in election_name:
                        context = "COM"
                    elif "metropolitane" in election_name:
                        context = "METRO"
                    elif "provinciali" in election_name:
                        context = "PROV"
                    elif "regionali" in election_name:
                        context = "REG"
                    else:
                        context = ""
                else:
                    if election_classification == 'CM':
                        context = 'METRO'
                    else:
                        context = election_classification

                if context:
                    identifier = "ELE-{0}_{1}".format(context, election_date)
                else:
                    identifier = None

                election, created = KeyEvent.objects.get_or_create(
                    start_date=election_date,
                    event_type="ELE-{0}".format(context),
                    defaults={
                        "name": election_name,
                        "end_date": election_date,
                        "identifier": identifier,
                    },
                )
            else:
                election = None

            # get the Organization the posts refer to
            if org_id:
                if org_scheme == "":
                    try:
                        org = Organization.objects.current(moment=org_valid_at).get(
                            pk=org_id
                        )
                    except Organization.DoesNotExist:
                        self.logger.warning(
                            "organization {org_id} valid at {org_valid_at} could not be found for "
                            "{charge_desc} {loc_desc} ({loc_code})".format(
                                org_id=org_id,
                                charge_desc=charge_desc,
                                loc_desc=loc_desc,
                                loc_code=loc_code,
                                org_valid_at=org_valid_at,
                            )
                        )
                        continue
                    except Organization.MultipleObjectsReturned:
                        self.logger.warning(
                            "more than one organizations found for {0} {1}".format(
                                charge_desc, loc_desc
                            )
                        )
                        continue
                else:
                    try:
                        org = Organization.objects.current(moment=org_valid_at).get(
                            identifiers__scheme=org_scheme,
                            odentifiers__identifier=org_id,
                        )
                    except Organization.DoesNotExist:
                        self.logger.warning(
                            "organization {org_id} valid at {org_valid_at} could not be found for "
                            "{charge_desc} {loc_desc} ({loc_code})".format(
                                org_id=org_id,
                                charge_desc=charge_desc,
                                loc_desc=loc_desc,
                                loc_code=loc_code,
                                org_valid_at=org_valid_at,
                            )
                        )
                        continue
                    except Organization.MultipleObjectsReturned:
                        self.logger.warning(
                            "more than one organizations found for {0} {1}".format(
                                charge_desc, loc_desc
                            )
                        )
                        continue

            elif org_name:
                try:
                    org = Organization.objects.current(moment=org_valid_at).get(
                        name__iexact=org_name
                    )
                except Organization.DoesNotExist:
                    self.logger.warning(
                        "organization {org_name} valid at {org_valid_at} could not be found for "
                        "{charge_desc} {loc_desc} ({loc_code})".format(
                            org_name=org_name,
                            charge_desc=charge_desc,
                            loc_desc=loc_desc,
                            loc_code=loc_code,
                            org_valid_at=org_valid_at,
                        )
                    )
                    continue
                except Organization.MultipleObjectsReturned:
                    self.logger.warning(
                        "more than one organizations found for {0} {1}".format(
                            charge_desc, loc_desc
                        )
                    )
                    continue
            else:
                self.logger.warning(
                    "neither org_id nor org_name were defined for {0} {1}".format(
                        charge_desc, loc_desc
                    )
                )
                continue

            # get the RoleType
            try:
                role_type = RoleType.objects.get(
                    label__iexact=role_type_label,
                    classification__descr__iexact=org_classification,
                )
            except RoleType.DoesNotExist:
                self.logger.warning(
                    " role_type could not be found for {0} {1}".format(
                        role_type_label, org_classification
                    )
                )
                continue
            except RoleType.MultipleObjectsReturned:
                self.logger.warning(
                    "more than one role types found for {0} {1}".format(
                        role_type_label, org_classification
                    )
                )
                continue

            # update or create the Post, fixing the role/role_type duality
            try:
                post, created = Post.objects.update_or_create(
                    organization=org,
                    role_type=role_type,
                    defaults={"label": post_label, "role": role_type.label},
                )
                if created:
                    self.logger.info(" {0} created".format(post))
                else:
                    self.logger.debug(" {0} updated".format(post))
            except Post.MultipleObjectsReturned:
                self.logger.warning(
                    "more than one post found for {0} {1}".format(
                        role_type, org_classification
                    )
                )

            # loop over all persons with posts in given organization
            item_dicts = group.to_dict("records")
            n_items = len(item_dicts)

            loc_code_clean = loc_code
            if "(" in loc_code:
                m = re.search(r"^.* \((.*)\)", loc_code)
                if m:
                    loc_code_clean = m.group(1)

            if hasattr(self, "year") and getattr(self, "year"):
                self.logger.info(
                    "{8:3d} recs - Y: {7:4.4}, EL:{6}, {0:24.24} ({1:>6.6}), {2}, {3} ({4}), {5}".format(
                        loc_desc,
                        loc_code_clean,
                        charge_desc,
                        org_name,
                        org_valid_at,
                        post_label,
                        election_date,
                        getattr(self, "year"),
                        n_items,
                    )
                )
            else:
                self.logger.info(
                    "{7:3d} recs - EL:{6}, {0:24.24} ({1:>6.6}), {2}, {3} ({4}), {5}".format(
                        loc_desc,
                        loc_code_clean,
                        charge_desc,
                        org_name,
                        org_valid_at,
                        post_label,
                        election_date,
                        n_items,
                    )
                )

            for item in item_dicts:
                self.load_item(item, post=post, election=election)

    def load_item(self, item, **kwargs):
        """update or create person's information (with search and similarities)

        :param item:
        :param kwargs:
        :return:
        """
        post = kwargs.get("post")
        election = kwargs.get("election", None)
        person = None

        # get the Person object
        if item["person_id_scheme"]:
            try:
                person = Person.objects.get(
                    identifiers__scheme=item["person_id_scheme"],
                    identifiers__identifier=item["person_id"],
                )
            except Person.DoesNotExist:
                self.logger.error(
                    "Person could not be found: {0}={1}".format(
                        item["person_id_scheme"], item["person_id"]
                    )
                )
            except Person.MultipleObjectsReturned:
                self.logger.error(
                    "More than one Person found: {0}={1}".format(
                        item["person_id_scheme"], item["person_id"]
                    )
                )
        elif item["person_id_scheme"] is None:
            try:
                person = Person.objects.get(pk=item["person_id"])
            except Person.DoesNotExist:
                self.logger.error(
                    "Person could not be found: pk={0}".format(item["person_id"])
                )
            except Person.MultipleObjectsReturned:
                self.logger.error(
                    "More than one Person found: pk={0}".format(item["person_id"])
                )

        if person is None:
            return

        if item.get("charge_desc", None) and any(
            d in item["charge_desc"].lower() for d in ["assessore", "commissario"]
        ):
            label = item["charge_desc"]
        else:
            label = None

        try:
            _, _ = person_utils.update_or_create_role(
                item,
                person,
                post,
                election,
                label=label,
                logger=self.logger,
                check_label=self.check_membership_label,
                update_strategy=self.update_strategy,
            )
        except Exception as e:
            self.logger.error("{0} when loading membership {1}".format(e, item))


class PopoloPersonMembershipLoader(PopoloMembershipLoader):
    """:class:`Loader` that stores persons, posts and memberships instances at once into OPDM

    Used only in Minint imports

    """

    def __init__(self, context="", year=None, **kwargs):
        """Create a new instance of the loader

        Args:
            context: a loader context to be written into the AKAS, when created
            year: the year this import refers to
            lookup_strategy: anagraphical or identifier, select the lookup strategy to use
            identifier_scheme: the scheme to use whith the identifier lookup strategy (OP_ID, CF, ...)
            persons_update_strategy: how to update the Person
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            memberships_update_strategy: how to update the Person's membership
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            check_membership_label: whether to check labels in role.update_or_create method
               (defaults to False)
            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
            aka_lo: threshold below which a similarity is discarded
            aka_hi: threshold above which a similarity is considered identical

        """
        self.context = context
        self.year = year
        self.lookup_strategy = kwargs.get("lookup_strategy", "anagraphical")
        self.identifier_scheme = kwargs.get("identifier_scheme", None)
        self.persons_update_strategy = kwargs.get("persons_update_strategy", "keep_old")
        self.memberships_update_strategy = kwargs.get(
            "memberships_update_strategy", "keep_old"
        )
        self.check_membership_label = kwargs.get("check_membership_label", False)
        self.aka_lo = kwargs.get("aka_lo", settings.AKA_LO_THRESHOLD)
        self.aka_hi = kwargs.get("aka_lo", settings.AKA_HI_THRESHOLD)

        self.istat_code_field = "ISTAT_CODE_" + self.context[:3].upper()
        if self.context in [
            "provincia",
            "province",
            "provincie",
            "prov",
        ] or self.context in ["metro", "metropoli", "cm"]:
            self.istat_code_field = "ISTAT_CODE_PROV"

        super().__init__()

    def load_item(self, item, **kwargs):
        """update or create person's information (with search and similarities)

        :param item:
        :param kwargs:
        :return:
        """
        post = kwargs.get("post")
        election = kwargs.get("election")

        # lookup person and return id (0 if not found, < 0 if similarities were found)
        loader_context = {
            "module": __name__,
            "class": self.__class__.__name__,
            "data_source": getattr(self.etl.extractor, "remote_url", self.etl.source),
            "context": self.context,
            "item": item,
            "post_id": post.id,
            "election_id": election.id,
            "persons_update_strategy": self.persons_update_strategy,
            "memberships_update_strategy": self.memberships_update_strategy,
            "check_membership_label": self.check_membership_label,
        }
        # patch to remove StringIO as source in CompositeETL,
        # to avoid exception serializing StringIO objects, and
        # set the correct URL
        if isinstance(loader_context["data_source"], StringIO):
            loader_context["data_source"] = getattr(self.etl, "source", "-")

        person_id = person_utils.person_lookup(
            item,
            self.lookup_strategy,
            loader_context,
            identified_scheme=self.identifier_scheme,
            aka_lo=self.aka_lo,
            aka_hi=self.aka_hi,
            logger=self.logger,
        )

        # add person and membership only if no similarities were found
        if person_id >= 0:
            try:
                # invoke class method to create or update a person from a dict (item)
                person, created = person_utils.update_or_create_person_from_item(
                    item, person_id, update_strategy=self.persons_update_strategy
                )
            except Exception as e:
                person, created = None, False
                self.logger.error("{0} when loading person {1}".format(e, item))
            else:
                if person is None:
                    self.logger.error(
                        "Could not update or create person with id: {0}, "
                        "when loading person {1}".format(person_id, item)
                    )
                    return
                person_utils.sync_solr_index(person)

            if created:
                self.logger.debug("    {0} created".format(person))
            else:
                self.logger.debug("    {0} updated".format(person))

            label = None
            if item.get("charge_desc"):
                if post.role_type.label == 'Consigliere comunale' and 'delega' in item["charge_desc"].lower():
                        label = f"{post.label} - {item.get('charge_desc')}"
                elif any(
                    d in item["charge_desc"].lower()
                    for d in [
                        "assessore", "commissario", "segretario",
                        "consigliere candidato", "consigliere supplente", "vicesindaco"
                    ]
                ):
                    if post.role_type.label == 'Commissario di giunta comunale':
                        label = post.label.replace('Commissario della giunta comunale', item["charge_desc"])
                    elif post.role_type.label == 'Commissario di giunta regionale':
                        label = post.label.replace('Commissario della giunta regionale', item["charge_desc"])
                    elif "segretario" in item["charge_desc"].lower():
                        label = f"{post.label} - {item.get('charge_desc')}"
                    else:
                        label = post.label.replace(post.role_type.label, item["charge_desc"])
            try:
                _, _ = person_utils.update_or_create_role(
                    item,
                    person,
                    post,
                    election,
                    label=label,
                    logger=self.logger,
                    check_label=self.check_membership_label,
                    update_strategy=self.memberships_update_strategy,
                )
            except Exception as e:
                self.logger.error("{0} when loading membership {1}".format(e, item))


class PopoloPersonWithRelatedDetailsLoader(PopoloPersonLoader):
    """:class:`Loader` that stores a Person having nested memberships/ownerships (JSON files)

    """

    def __init__(self, **kwargs):
        """Create a new instance of the loader

        Args:
            context: a loader context to be written into the AKAS, when created
            lookup_strategy: anagraphical or identifier, select the lookup strategy to use
            identifier_scheme: the scheme to use whith the identifier lookup strategy (OP_ID, CF, ...)
            update_strategy: how to update the Person
            memberships_update_strategy: how to update the Person's memberships
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            ownerships_update_strategy: how to update the Persons' ownerships
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            check_membership_label: whether to check labels in role.update_or_create method
               (defaults to False)
            check_near_start_dates: whether to check for neighbouring dates to update instead of create new membership
            near_start_date_lo: range of the past-neighborhood
            near_start_date_hi: range of the future-neighborhood

            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
            aka_lo: threshold below which a similarity is discarded
            aka_hi: threshold above which a similarity is considered identical
        """
        super(PopoloPersonWithRelatedDetailsLoader, self).__init__(**kwargs)
        self.context = kwargs.get("context", None)
        self.check_membership_label = kwargs.get("check_membership_label", False)

        self.check_near_start_dates = kwargs.get("check_near_start_dates", True)
        self.near_start_date_lo = kwargs.get("near_start_date_lo", 60)
        self.near_start_date_hi = kwargs.get("near_start_date_hi", 180)

        self.memberships_update_strategy = kwargs.get(
            "memberships_update_strategy", "keep_old"
        )
        self.ownerships_update_strategy = kwargs.get(
            "ownerships_update_strategy", "keep_old"
        )
        self.aka_lo = kwargs.get("aka_lo", settings.AKA_LO_THRESHOLD)
        self.aka_hi = kwargs.get("aka_hi", settings.AKA_HI_THRESHOLD)

    def load_item(self, item, **kwargs):
        """load Person and her memberships into the Popolo models
        - lookup a person, with strategy defined in self.lookup_strategy
        - invoke update_or_create_from_item (anagraphical data plus identifiers, contacts, ...)
        - loop over all item['memberships'], and update_or_create memberships,
          using strategy defined in self.memberships_update_strategy

        :param item: the item to be loaded
        :param kwargs: other parameters, not used currently
        :return:
        """
        loader_context = {
            "module": __name__,
            "class": self.__class__.__name__,
            "data_source": getattr(self.etl.extractor, "remote_url", self.etl.source),
            "context": self.context,
            "check_membership_label": self.check_membership_label,
            "item": item,
        }

        # patch to remove StringIO as source in CompositeETL,
        # to avoid exception serializing StringIO objects, and
        # set the correct URL
        if isinstance(loader_context["data_source"], StringIO):
            loader_context["data_source"] = getattr(self.etl, "source", "-")

        person_id = person_utils.person_lookup(
            item,
            self.lookup_strategy,
            loader_context,
            identified_scheme=self.identifier_scheme,
            aka_lo=self.aka_lo,
            aka_hi=self.aka_hi,
            logger=self.logger,
        )
        if person_id >= 0:
            try:
                _, _ = person_utils.update_or_create_person_and_details_from_item(
                    person_id,
                    item,
                    loader_context=loader_context,
                    persons_update_strategy=None,
                    memberships_update_strategy=self.memberships_update_strategy,
                    ownerships_update_strategy=self.ownerships_update_strategy,
                    check_membership_label=self.check_membership_label,
                    check_near_start_dates=self.check_near_start_dates,
                    near_start_date_lo=self.near_start_date_lo,
                    near_start_date_hi=self.near_start_date_hi,
                    logger=self.logger,
                )

            except UnprocessableEntitytAPIException as e:
                self.logger.warning("{0} while loading {1}".format(e, item))


class PopoloPersonOwnershipLoader(PopoloLoader):
    """:class:`Loader` that stores  data in ``popolo.Ownership`` instances

    The generic `load` method can still be overridden in subclasses,
    should peculiar necessities arise (as bulk loading).
    """

    areas_dict = None

    def __init__(self, **kwargs):
        """Create a new instance of the loader

        Args:
            lookup_strategy: anagraphical, identifier, mixed select the lookup strategy to use
            identifier_scheme: the scheme to use whith the mixer/identifier lookup strategies (OP_ID, CF, ...)
            update_strategy: how to update the Organization
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            - private_company_verification (enable verification of private
              orgs before anagraphical search in mixed strategy)
            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
        """
        super(PopoloPersonOwnershipLoader, self).__init__(**kwargs)
        self.lookup_strategy = kwargs.get("lookup_strategy", "identifier")
        self.identifier_scheme = kwargs.get("identifier_scheme", None)
        self.update_strategy = kwargs.get("update_strategy", "keep_old")
        self.private_company_verification = kwargs.get('private_company_verification', False)

    def load_item(self, item, **kwargs):
        """load Ownership into the Popolo models
        lookup the owning and owned organizations,
        invoke update_or_create to create the instance

        :param item: the item to be loaded
        :return:
        """

        # organization lookup: if not found a 0 ID is returned and
        # the exception is raised within the update_or_create_org_ownership_from_item
        owner_person_id = person_utils.person_lookup(
            item["owning_person"], "identifier", logger=self.logger
        )

        owned_org_id = organization_utils.org_lookup(
            item["owned_org"],
            self.lookup_strategy,
            identifier_scheme=self.identifier_scheme,
            private_company_verification=self.private_company_verification,
            logger=self.logger,
        )
        if owner_person_id == 0:
            self.logger.error(
                "Could not find owner person in {0}".format(item["owning_person"])
            )
            return
        elif owner_person_id < 0:
            self.logger.error(
                "More than one owner org found for tax_id {0}".format(
                    item["owning_person"]
                )
            )
            return
        else:
            try:
                owner_person = Person.objects.get(id=owner_person_id)
            except Organization.DoesNotExist:
                self.logger.error(
                    "Owner person ID found but Person missing: {0}".format(
                        owner_person_id
                    )
                )
                return

        if owned_org_id == 0:
            self.logger.error(
                "Could not find owned org in {0}".format(item["owned_org"])
            )
            return
        elif owned_org_id < 0:
            self.logger.error(
                "More than one owned org found for tax_id {0}".format(item["owned_org"])
            )
            return
        else:
            try:
                owned_org = Organization.objects.get(id=owned_org_id)
            except Organization.DoesNotExist:
                self.logger.error(
                    "Owner organization ID found but Organization missing: {0}".format(
                        owned_org_id
                    )
                )
                return

        try:
            # invoke class method to create or update an ownership out of an item
            o, created = organization_utils.update_or_create_ownership_from_item(
                owner_person, owned_org, item, update_strategy=self.update_strategy
            )
            if created:
                self.logger.info("Ownership {0} created".format(o))
            else:
                self.logger.debug("Ownership {0} updated".format(o))
        except Exception as e:
            self.logger.error(
                "Error when loading ownership for {0} owning {1}% of {2} created".format(
                    owner_person_id, float(item["percentage"]), owned_org_id
                )
            )
            self.logger.error("{0} when loading ownership from {1}".format(e, item))


class PopoloPersonalRelationshipLoader(PopoloLoader):

    areas_dict = None

    def __init__(self, **kwargs):
        """Create a new instance of the loader

        Args:
            lookup_strategy: anagraphical, identifier, mixed select the lookup strategy to use
            identifier_scheme: the scheme to use whith the mixer/identifier lookup strategies (OP_ID, CF, ...)
            update_strategy: how to update the Organization
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
        """
        super().__init__(**kwargs)
        self.lookup_strategy = kwargs.get("lookup_strategy", "identifier")
        self.update_strategy = kwargs.get("update_strategy", "keep_old")

    def load_item(self, item, **kwargs):

        try:
            person1 = Person.objects.get(id=item["person1_id"])
        except Person.DoesNotExist:
            self.logger.error(f"Could not find person with id {item['person1_id']}")
            return

        try:
            person2 = Person.objects.get(id=item["person2_id"])
        except Person.DoesNotExist:
            self.logger.error(f"Could not find person with id {item['person2_id']}")
            return

        try:
            # invoke class method to create or update an ownership out of an item
            # source and dest must be reversed in order for the parent relationship to  have meaning
            o, created = person_utils.update_or_create_personal_relationship(
                source_person=person1,
                dest_person=person2,
                item=item,
                update_strategy=self.update_strategy
            )
            if created:
                self.logger.debug("Personal relationship {0} created".format(o))
            else:
                self.logger.debug("Personal relationship {0} updated".format(o))
        except Exception as e:
            self.logger.error(
                f"Error loading relationship: "
                f"{item['person1_id']} -> Kinship ({item.get('description', '')}) -> {item['person2_id']}"
            )
            self.logger.error(f"{e} when loading personal relationship from {item}")
