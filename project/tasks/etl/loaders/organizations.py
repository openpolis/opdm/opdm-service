# -*- coding: utf-8 -*-
from popolo.models import Organization

from project.core import organization_utils
from project.tasks.etl.loaders import PopoloLoader

descr_stato_partecipazione = {
    'N': 'Non partecipata',
    'S': 'Partecipata',
    'E': 'Precedentemente partecipata'
}


class PopoloOrgOwnershipLoader(PopoloLoader):
    """:class:`Loader` that stores  data in ``popolo.Ownership`` instances

    The generic `load` method can still be overridden in subclasses,
    should peculiar necessities arise (as bulk loading).
    """
    areas_dict = None

    def __init__(self, **kwargs):
        """Create a new instance of the loader

        Args:
            lookup_strategy: anagraphical, identifier, mixed select the lookup strategy to use
            identifier_scheme: the scheme to use whith the mixer/identifier lookup strategies (OP_ID, CF, ...)
            update_strategy: how to update the Organization
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `overwrite_minint_opdm`: partial overwrite
            - private_company_verification (enable verification of private
              orgs before anagraphical search in mixed strategy)
            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
        """
        super(PopoloOrgOwnershipLoader, self).__init__(**kwargs)
        self.lookup_strategy = kwargs.get('lookup_strategy', 'identifier')
        self.identifier_scheme = kwargs.get('identifier_scheme', 'CF')
        self.update_strategy = kwargs.get('update_strategy', 'keep_old')
        self.private_company_verification = kwargs.get('private_company_verification', False)

    def load_item(self, item, **kwargs):
        """load Ownership into the Popolo models
        lookup the owning and owned organizations,
        invoke update_or_create to create the instance

        :param item: the item to be loaded
        :return:
        """

        # organization lookup: if not found a 0 ID is returned and
        # the exception is raised within the update_or_create_org_ownership_from_item
        owner_org_id = organization_utils.org_lookup(
            item["owning_org"], self.lookup_strategy,
            identifier_scheme=self.identifier_scheme,
            private_company_verification=self.private_company_verification,
            logger=self.logger
        )

        owned_org_id = organization_utils.org_lookup(
            item["owned_org"], self.lookup_strategy,
            identifier_scheme=self.identifier_scheme,
            private_company_verification=self.private_company_verification,
            logger=self.logger
        )
        if owner_org_id == 0:
            self.logger.error("Could not find owner org in {0}".format(item["owning_org"]))
            return
        elif owner_org_id < 0:
            self.logger.error("More than one owner org found for tax_id {0}".format(item["owning_org"]))
            return
        else:
            try:
                owner_org = Organization.objects.get(id=owner_org_id)
            except Organization.DoesNotExist:
                self.logger.error("Owner organization ID found but Organization missing: {0}".format(owner_org_id))
                return

        if owned_org_id == 0:
            self.logger.error("Could not find owned org in {0}".format(item["owned_org"]))
            return
        elif owned_org_id < 0:
            self.logger.error("More than one owned org found for tax_id {0}".format(item["owned_org"]))
            return
        else:
            try:
                owned_org = Organization.objects.get(id=owned_org_id)
            except Organization.DoesNotExist:
                self.logger.error("Owner organization ID found but Organization missing: {0}".format(owned_org_id))
                return

        try:
            # invoke class method to create or update an ownership out of an item
            o, created = organization_utils.update_or_create_ownership_from_item(
                owner_org, owned_org, item, update_strategy=self.update_strategy
            )
            if created:
                self.logger.info('Ownership {0} created'.format(o))
            else:
                self.logger.debug('Ownership {0} updated'.format(o))
        except Exception as e:
            self.logger.error('Error when loading ownership for {0} owning {1}% of {2} created'.format(
                owner_org_id, round(float(item['percentage']), 2), owned_org_id
            ))
            self.logger.error("{0} when loading ownership from {1}".format(e, item))


class PopoloOrgLoader(PopoloLoader):
    """:class:`Loader` that stores  data in ``popolo.Organization`` instances

    The generic `load` method can still be overridden in subclasses,
    should peculiar necessities arise (as bulk loading).
    """
    areas_dict = None

    def __init__(self, **kwargs):
        """Create a new instance of the loader

        Args:
            lookup_strategy: anagraphical, identifier, mixed select the lookup strategy to use
            identifier_scheme: the scheme to use whith the mixer/identifier lookup strategies (OP_ID, CF, ...)
            update_strategy: how to update the Organization
            - `keep_old`: only write fields that are empty, keeping old values
            - `overwrite`: overwrite all fields
            - `keep_old_overwrite_cf`: keep_old but overwrite CF identifier
            - private_company_verification (enable verification of private
              orgs before anagraphical search in mixed strategy)

            see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
        """
        super(PopoloOrgLoader, self).__init__(**kwargs)
        self.lookup_strategy = kwargs.get('lookup_strategy', 'identifier')
        self.identifier_scheme = kwargs.get('identifier_scheme', 'CF')
        self.update_strategy = kwargs.get('update_strategy', 'keep_old')
        self.private_company_verification = kwargs.get('private_company_verification', False)

    def load_item(self, item, **kwargs):
        """load Organization into the Popolo models
        lookup an organization, with strategy defined in self.lookup_strategy
        invoke update_or_create_from_item (anagraphical data plus identifiers, contacts, ...)

        :param item: the item to be loaded
        :return:
        """

        org_id = organization_utils.org_lookup(
            item, self.lookup_strategy,
            identifier_scheme=self.identifier_scheme,
            private_company_verification=self.private_company_verification,
            logger=self.logger
        )
        if org_id >= 0:
            try:
                # invoke class method to create or update a person from a dict (item)
                o, created = organization_utils.update_or_create_org_from_item(
                    item, org_id, update_strategy=self.update_strategy
                )
                if created:
                    self.logger.info("    {0} created".format(o))
                else:
                    self.logger.debug("    {0} updated".format(o))

            except Exception as e:
                self.logger.error("{0} when loading organization {1}".format(e, item))
            else:
                organization_utils.sync_solr_index(o)
        else:
            self.logger.warning("{0} organizations found for item {1}".format(org_id, item))


class PopoloOrgBulkCreateLoader(PopoloOrgLoader):
    """:class:`Loader` that stores new data in ``popolo.Organization`` instances

    This is the BulkCreate loader, used to fastly insert areas in the DB.

    It needs an empty model.
    """

    def load(self, **kwargs):
        """Stores data to ``popolo.Organization` instances,
        """
        chunk_size = kwargs.get('chunk_size', 1000)
        batch_size = kwargs.get('chunk_size', 100)

        # transform processed data into a list of dicts
        items_dicts = self.etl.processed_data.to_dict('records')

        for n, c in enumerate(range(0, len(items_dicts), chunk_size)):
            # first, fast insertion
            items_objects = (
                Organization(**{
                    'name': i['DENOMINAZIONE'],
                    'identifier': i['CF'],
                    'founding_date': i['DATA_ISTITUZIONE'],
                    'slug': i['SLUG']
                }) for i in items_dicts[c:c + chunk_size]
            )
            Organization.objects.bulk_create(
                items_objects, batch_size=batch_size
            )

            self.logger.info(
                "{0}/{1}".format(
                    n * chunk_size, len(items_dicts)
                )
            )

        self.logger.info("Bulk insert finished")
