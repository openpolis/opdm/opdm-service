import numpy as np
from popolo.models import Area, Language
from slugify import slugify

from project.tasks.etl.loaders import PopoloLoader


class PopoloAreaLoader(PopoloLoader):
    """:class:`Loader` that stores  data in ``popolo.Area`` instances

    The generic `load` method can still be overridden in subclasses,
    should peculiar necessities arise (as bulk loading).
    """

    it_language = None
    de_language = None
    sl_language = None

    def __init__(self, csv_source=None):
        """Create a new instance of the loader

        Args:
            csv_source: To be used in Source model

        """
        super().__init__()
        self.csv_source = csv_source

    def load(self, **kwargs):
        """Stores data to ``popolo.Area`` instances,
        """
        # get or create langiages
        self.it_language, _dummy = Language.objects.get_or_create(
            iso639_1_code='it',
            defaults={'name': 'Italiano'}
        )
        self.de_language, _dummy = Language.objects.get_or_create(
            iso639_1_code='de',
            defaults={'name': 'Tedesco'}
        )
        self.sl_language, _dummy = Language.objects.get_or_create(
            iso639_1_code='sl',
            defaults={'name': 'Sloveno'}
        )

        super(PopoloAreaLoader, self).load(**kwargs)

    def add_nuts_identifier(self, area, scheme, start_year, identifier):
        """  add NUTS identifiers, only if not already there
        Extract the current identifier and, if the value to add is different, then
        closes the validity of the old one and add the new one.

        :param area:       the area the identifiers refer to
        :param scheme:     NUTS[123]_CODE
        :param start_year: the year identifying the identifier (2006, 2010, 2021)
        :param identifier: the identifier value (IT1, ITC, IT23, ...)
        :return:           void
        """
        nuts_current_identifiers = area.identifiers.filter(scheme=scheme, end_date__isnull=True)
        n_current_identifiers = nuts_current_identifiers.count()
        current_identifier = nuts_current_identifiers.first()
        if n_current_identifiers > 1:
            self.logger.warn(
                "Found multiple current {0} identifiers for {1}".format(scheme, area)
            )
            return

        if current_identifier is None or current_identifier.identifier != identifier:
            # close current identifier if started before the new one (replacement)
            # delete it if started on the same day, or after (substitution)
            # do nothing (return) if the current identifier starts later than the new one
            if current_identifier:
                if (
                    current_identifier.start_date < "{0}-01-01".format(start_year) or
                    current_identifier.start_date is None
                ):
                    nuts_current_identifiers.update(end_date="{0}-01-01".format(start_year))
                elif current_identifier.start_date == "{0}-01-01".format(start_year):
                    nuts_current_identifiers.delete()
                else:
                    return

            try:
                area.add_identifier(
                    scheme=scheme,
                    identifier=identifier,
                    source=self.csv_source,
                    start_date='{0}-01-01'.format(start_year)
                )
            except Exception as e:
                self.logger.warn(
                    "While importing {0} identifier for {1}, "
                    "got the following exceptions: {2}".format(scheme, area, e)
                )

    def load_item(self, area, **kwargs):
        self.logger.debug(area['den_full'])

        # get_or_create the Ripartizione Geografica
        rip, created = Area.objects.get_or_create(
            identifier=area['code_rip'],
            defaults={
                'name': area['den_rip'],
                'classification': 'RGNE',
                'istat_classification':
                    Area.ISTAT_CLASSIFICATIONS.ripartizione,
                'slug': slugify(
                    Area.ISTAT_CLASSIFICATIONS.ripartizione + " " +
                    str(area['code_rip'])
                )

            }
        )

        # self.add_nuts_identifier(rip, 'NUTS1_CODE', 2010, area['code_nuts1_2010'])
        self.add_nuts_identifier(rip, 'NUTS1_CODE', 2021, area['code_nuts1_2021'])
        self.add_nuts_identifier(rip, 'NUTS1_CODE', 2024, area['code_nuts1_2024'])

        # get_or_create the Regione
        reg, created = Area.objects.get_or_create(
            identifier=area['code_reg'],
            defaults={
                'name': area['den_reg'],
                'classification': 'ADM1',
                'istat_classification':
                    Area.ISTAT_CLASSIFICATIONS.regione,
                'slug': slugify(
                    Area.ISTAT_CLASSIFICATIONS.regione + " " +
                    str(area['code_reg'])
                )

            }
        )
        if created:
            reg.parent = rip
            reg.save()

        # add NUTS identifiers, only if not already there
        # self.add_nuts_identifier(reg, 'NUTS2_CODE', 2010, area['code_nuts2_2010'])
        self.add_nuts_identifier(reg, 'NUTS2_CODE', 2021, area['code_nuts2_2021'])
        self.add_nuts_identifier(reg, 'NUTS2_CODE', 2024, area['code_nuts2_2024'])

        # get_or_create the Città Metropolitana or Provinca
        den = area['den_prov']
        if area['code_cm'] is not np.nan:
            istat_class = Area.ISTAT_CLASSIFICATIONS.metro
        else:
            istat_class = Area.ISTAT_CLASSIFICATIONS.provincia

        cp, created = Area.objects.get_or_create(
            identifier=area['sigla_auto'],
            defaults={
                'name': den,
                'classification': 'ADM2',
                'istat_classification': istat_class,
                'slug': slugify(
                    istat_class + " " + area['sigla_auto']
                )

            }
        )
        if created:
            cp.parent = reg
            cp.save()

            # add ISTAT_CODE_PROV identifier
            try:
                cp.add_identifier(
                    identifier=area['code_prov'],
                    scheme='ISTAT_CODE_PROV',
                    source='self.csv_source'
                )
            except Exception as e:
                self.logger.error(
                    "While importing ISTAT_CODE_PROV identifier for {0}, "
                    "got the following exceptions: {1}".format(cp, e)
                )

            # add ISTAT_CODE_CM identifier if Meropolitan Area
            if area['code_cm'] is not np.nan:
                try:
                    cp.add_identifier(
                        identifier=area['code_cm'],
                        scheme='ISTAT_CODE_CM',
                        source='self.csv_source'
                    )
                except Exception as e:
                    self.logger.error(
                        "While importing ISTAT_CODE_CM identifier for {0}, "
                        "got the following exceptions: {1}".format(cp, e)
                    )

            cp.add_identifier(
                scheme='ISTAT_CODE_PROV',
                identifier=area['code_prov'],
                source=self.csv_source
            )

        # self.add_nuts_identifier(cp, 'NUTS3_CODE', 2010, area['code_nuts3_2010'])
        self.add_nuts_identifier(cp, 'NUTS3_CODE', 2021, area['code_nuts3_2021'])
        self.add_nuts_identifier(cp, 'NUTS3_CODE', 2024, area['code_nuts3_2024'])

        # get_or_create the Comune
        c, created = Area.objects.get_or_create(
            identifier=area['code_catasto'],
            defaults={
                'name': area['den_full'],
                'classification': 'ADM3',
                'istat_classification': Area.ISTAT_CLASSIFICATIONS.comune,
                'is_provincial_capital': area['is_cap'],
                # 'inhabitants': area['pop_2011'],
                'slug': slugify(
                    Area.ISTAT_CLASSIFICATIONS.comune + " " +
                    str(area['code_catasto'])
                )
            }

        )
        c.parent = cp

        try:
            c.add_identifier(
                identifier=area['code_com'],
                scheme='ISTAT_CODE_COM',
                source=self.csv_source,
                extend_overlapping=False
            )
        except Exception as e:
            self.logger.debug(
                "While importing identifier for {0}, "
                "got the following exceptions: {1}".format(c, e)
            )

        if area['den_xx'] is not np.nan:
            c.add_i18n_name(area['den_it'], self.it_language)

            if area['code_reg'] == '04':
                c.add_i18n_name(area['den_xx'], self.de_language)

            elif area['code_reg'] == '06':
                c.add_i18n_name(area['den_xx'], self.sl_language)

            else:
                self.logger.warn(
                    "Unknown  {0}".format(c)
                )

        c.save()


class PopoloAreaBulkCreateLoader(PopoloAreaLoader):
    """:class:`Loader` that stores new data in ``popolo.Area`` instances

    This is the BulkCreate loader, used to fastly insert areas in the DB.

    It needs an empty model.
    """

    def load(self, **kwargs):
        """Stores data to ``popolo.Area`` instances,
        """
        chunk_size = kwargs.get('chunk_size', 1000)
        batch_size = kwargs.get('chunk_size', 100)

        # transform processed data into a list of dicts
        items_dicts = self.etl.processed_data.to_dict('records')

        for n, c in enumerate(range(0, len(items_dicts), chunk_size)):
            items_objects = [
                Area(**{
                    'name': a['den_full'],
                    'identifier': a['code_catasto'],
                    'classification': 'ADM3',
                    'istat_classification': Area.ISTAT_CLASSIFICATIONS.comune,
                    'slug': slugify(
                        Area.ISTAT_CLASSIFICATIONS.comune + " " +
                        str(a['code_catasto'])
                    )
                }) for a in items_dicts[c:c + chunk_size]
            ]
            Area.objects.bulk_create(
                items_objects, batch_size=batch_size
            )

            self.logger.info(
                "{0}/{1}".format(
                    n * chunk_size, len(items_dicts)
                )
            )

        self.logger.info("Bulk insert finished")
