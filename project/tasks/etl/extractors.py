import csv
from io import BytesIO, StringIO
import json
from typing import Union, Optional, Dict
from urllib.request import urlopen, Request

from dpath.exceptions import PathNotFound
import dpath.util
from jsonschema import validate, ValidationError, SchemaError
from ooetl.extractors import CSVExtractor, RemoteExtractor
from ooetl.extractors import Extractor
import pandas as pd
import requests


class ListExtractor(Extractor):
    """Wraps a list into an Extractor, in order for it to be used in the ETL

    """

    def __init__(self, items: list):
        """Create a new instance, setting the items attribute

        Args:
            items: the items to be extracted

        Returns:
            instance of an :class:`Extractor` subclass
        """
        self.items = items
        super().__init__()

    def extract(self, **kwargs):
        """Extract content
        """
        return self.items


class JsonExtractor(Extractor):
    """Extract JSON records from a stream"""

    in_stream = None

    def __init__(self, in_stream: StringIO, source=None, encoding="utf8"):
        """Create a new instance, setting the valuo for the IO of the JSON content
        in_stream may be a StringIO or a File pointer

        Args:
            in_stream (StringIO): JSON data stream or file pointer

        Returns:
            instance of a :class:`RemoteExtractor` subclass
        """
        self.in_stream = in_stream
        self.encoding = encoding

        if source is None:
            self.source = "unknown"
        else:
            self.source = source

    def extract(self, **kwargs):
        """Extract content from json source, be it a remote url or a local file
        """

        self.logger.info("Fetching data from given stream")

        try:
            results = json.load(self.in_stream)
        except Exception as e:
            self.logger.info("{0}. Data could not be fetched.".format(e))
            results = []

        return results


class JsonRemoteExtractor(Extractor):
    """
    An extractor JSON data from a url.

    JSON Schema validation included.
    """

    def __init__(
        self,
        url: Union[str, Request],
        schema: Union[dict, str, Request] = None,
        encoding: str = "utf8",
        **kwargs
    ):
        """
        :param url: An URL which points to the JSON document to be retrieved.
        :param schema:
            The JSON Schema which will be used to validate the retrieved JSON document.
            Can be either a `dict` defining the JSON Schema or a `str` representing
            an URL pointing to the JSON Schema document.
        :param encoding:
            A string representing the encoding to use when parsing the JSON.
            Defaults to ‘utf-8’.
        """
        self.url = url
        self.encoding = encoding
        self.schema = self._load_schema(schema)

        super().__init__(**kwargs)

    def extract(self, **kwargs) -> Optional[dict]:
        """
        Extract JSON data from a url.

        :raises: `jsonschema.exceptions.ValidationError`
        :raises: `jsonschema.exceptions.SchemaError`
        """

        data = self._load_json_from_url(self.url)

        if self.schema:
            try:
                validate(data, self.schema)
            except (ValidationError, SchemaError) as e:
                raise e

        return data

    def _load_json_from_url(self, url: Union[str, Request],) -> Optional[dict]:
        with urlopen(url) as response:
            raw = response.read()
            encoding = response.headers.get_content_charset() or self.encoding
            content = raw.decode(encoding=encoding)
            return json.loads(content)

    def _load_schema(self, schema: Union[str, Dict]) -> Optional[dict]:
        if isinstance(schema, dict):
            return schema
        elif isinstance(schema, str) or isinstance(Request, str):
            return self._load_json_from_url(schema)
        else:
            return None


class JsonArrayExtractor(RemoteExtractor):
    """:class:`Extractor` for remote JSON data

    """

    def __init__(
        self, url: Union[str, StringIO], source=None, encoding="utf8", dpath=None
    ):
        """Create a new instance, setting public URL of the JSON content

        Args:
            url: JSON data public URL or file-like pointer
            source: back-compatible parameters (use url now)
            encoding: the encoding of the json source (utf8, latin1)
            dpath: a sting representing the path to the data (ie: /children, /results/valid, ...)

        Returns:
            instance of a :class:`RemoteExtractor` subclass
        """
        self.encoding = encoding
        self.dpath = dpath

        if source is None:
            self.source = url
        else:
            self.source = source
        super().__init__(url)

    def extract(self, **kwargs):
        """Extract content from json source, both in case of a remote url, a local file, or a generic stream
        """
        if self.logger:
            self.logger.debug("Fetching data from {0}".format(self.source))

        if isinstance(self.remote_url, str):
            protocol = self.remote_url[: self.remote_url.rfind("//") - 1]
            if "http" in protocol:
                try:
                    if "headers" in kwargs:
                        headers = kwargs["headers"]
                    else:
                        headers = {
                            "User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) "
                            "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537."
                        }
                    resp = requests.get(self.remote_url, headers=headers)
                    if resp.status_code == 200:
                        if hasattr(resp, "content"):
                            data = json.loads(resp.content)
                        elif hasattr(resp, "json_data"):
                            data = resp.json_data
                        else:
                            data = []

                        if self.dpath and isinstance(data, dict):
                            try:
                                data = dpath.util.get(data, self.dpath)
                            except PathNotFound:
                                raise Exception(
                                    "Could not find path {0} in json response".format(
                                        self.dpath
                                    )
                                )

                        results = data
                    else:
                        results = []
                except requests.exceptions.MissingSchema as e:
                    if self.logger:
                        self.logger.info("{0}. Data could not be fetched.".format(e))
                    results = []
            else:
                try:
                    with open(self.remote_url.replace("file://", ""), "r") as json_file:
                        results = json.load(json_file)
                except Exception as e:
                    if self.logger:
                        self.logger.info("{0}. Data could not be fetched.".format(e))
                    results = []
        elif hasattr(self.remote_url, "read"):
            try:
                results = json.load(self.remote_url)
            except Exception as e:
                if self.logger:
                    self.logger.info("{0}. Data could not be fetched.".format(e))
                results = []
        else:
            if self.logger:
                self.logger.info(
                    "remote_url must be either a string or a file-like variable. Data could not be fetched."
                )
            results = []

        return results


class OPAPIPagedExtractor(RemoteExtractor):
    """:class:`Extractor` for remote OPAPI data, exposed as json, and
    retrievable as paged results.

    """

    def __init__(self, remote_url, **kwargs):
        """Create a new instance, setting the the `page` and url to point the extraction

        Args:
            url (string): api endpoint
            kwargs: parameters
              page: n. of page to extract
              page_size: n. of results per page
              n_pages: n. of pages maximum
              api_filters: other api filters

        Returns:
            instance of a :class:`RemoteExtractor` subclass
        """
        super().__init__(remote_url)
        self.starting_page = kwargs.pop("starting_page", 1)
        self.page_size = kwargs.pop("page_size", 50)
        self.n_pages = kwargs.pop("n_pages", 0)
        self.api_filters = kwargs.pop("api_filters")

    def extract(self, **kwargs):
        """Extract one or more pages from API endpoint
        """
        complete_url = "{0}.json?&page={1}&page_size={2}&n_pages={3}".format(
            self.remote_url, self.starting_page, self.page_size, self.n_pages
        )
        if self.api_filters:
            complete_url += "&{0}".format(self.api_filters)

        results = []
        pages_fetched = 0

        cond = True

        while cond:

            self.logger.info("Fetching data from {0}".format(complete_url))
            resp = requests.get(complete_url)

            if resp.status_code == 200:
                data = resp.json()
                results.extend(data["results"])
            else:
                break

            if data["next"] is None:
                break

            complete_url = data["next"]
            pages_fetched = pages_fetched + 1

            if self.n_pages > 0:
                cond = pages_fetched < self.n_pages

        return results


class UACSVExtractor(CSVExtractor):
    """:class:`Extractor`  for remote data, exposed in an
    uncompressed csv format, requested with a User-Agent
    """

    def __init__(self, url, **kwargs):
        """Create a new instance of the extractor

        Needs a remote url and may accept other named arguments,
        the arguments used here are a short list of arguments that can be
        found in the :function:`pandas.read_csv()` method.

        Args:
            url: (string)
            kwargs: see super class
        """
        self.user_agent = kwargs.pop("user_agent", "Mozilla/5.0")
        super(UACSVExtractor, self).__init__(url, **kwargs)

    def extract(self, **kwargs):
        """Extracts data from a remote, csv source

        Args:
            **kwargs: Arbitrary keyword arguments.

        Returns:
            :class:`pandas.DataFrame`: The dataframe containing extracted items
        """

        csv_stream = BytesIO(
            requests.get(
                self.remote_url,
                **kwargs
            ).content
        )

        df = pd.read_csv(
            csv_stream,
            index_col=False,
            sep=self.sep,
            skiprows=self.skiprows,
            header=self.header,
            encoding=self.encoding,
            dtype=self.dtype,
            converters=self.converters,
            na_filter=self.na_filter,
            na_values=self.na_values,
            keep_default_na=self.keep_default_na,
        )

        return df


class UAProxyCSVExtractor(CSVExtractor):
    """:class:`Extractor`  for remote data, exposed in an
    uncompressed csv format, requested with a User-Agent
    """

    def __init__(self, url, **kwargs):
        """Create a new instance of the extractor

        Needs a remote url and may accept other named arguments,
        the arguments used here are a short list of arguments that can be
        found in the :function:`pandas.read_csv()` method.

        Args:
            url: (string)
            kwargs: see super class
        """
        self.remote_kwargs = {
            'headers': kwargs.pop("headers", "Mozilla/5.0"),
            'proxies': kwargs.pop("proxies", None),
        }
        super(UAProxyCSVExtractor, self).__init__(url, **kwargs)

    def extract(self, **kwargs):
        """Extracts data from a remote, csv source

        Args:
            **kwargs: Arbitrary keyword arguments.

        Returns:
            :class:`pandas.DataFrame`: The dataframe containing extracted items
        """
        csv_stream = BytesIO(
            requests.get(
                self.remote_url,
                **self.remote_kwargs
            ).content
        )

        df = pd.read_csv(
            csv_stream,
            index_col=False,
            sep=self.sep,
            skiprows=self.skiprows,
            header=self.header,
            encoding=self.encoding,
            dtype=self.dtype,
            converters=self.converters,
            na_filter=self.na_filter,
            na_values=self.na_values,
            keep_default_na=self.keep_default_na,
            quoting=csv.QUOTE_ALL
        )

        return df
