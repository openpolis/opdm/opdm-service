# -*- coding: utf-8 -*-
from ooetl.transformations import Transformation
import pandas as pd


class BdapOwnership2OPDMTransformation(Transformation):
    """Instance of ``ooetl.Transformation`` class that handles
    transforming ownerships data from BDAP into OPDM-Django-Popolo compatible data
    """

    def transform(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns,
        transforming into a nested python data-structure

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original dataframe
        od = self.etl.original_data.copy()

        # columns renaming (uppercase)
        od = od.rename(columns={col: col.upper() for col in list(od.columns)})

        # select only useful columns
        od = od.filter(
            items=[
                "ID_ENTE_PARTECIPANTE",
                "ID_ENTE_PARTECIPATO",
                "CF_ENTE_PARTECIPANTE",
                "CF_ENTE_PARTECIPATO",
                "DENOM_ENTE_PARTECIPANTE",
                "DENOM_ENTE_PARTECIPATO",
                "DATA_RILEVAZIONE",
                "DATA_RILEVAZIONE_FINE_RELAZ",
                "QUOTA_DIRETTA",
            ]
        )

        # rename ID_ENTE into CODICE_ENTE_BDAP
        od = od.rename(columns={
            "ID_ENTE_PARTECIPANTE": "CODICE_ENTE_BDAP_PARTECIPANTE",
            "ID_ENTE_PARTECIPATO": "CODICE_ENTE_BDAP_PARTECIPATO",
            "DATA_RILEVAZIONE": "DATA_INIZIO_RELAZIONE",
            "DATA_RILEVAZIONE_FINE_RELAZ": "DATA_FINE_RELAZIONE"
        })

        # remove empty rows
        od = od.dropna(
            how="any",
            subset=[
                "CODICE_ENTE_BDAP_PARTECIPANTE",
                "CODICE_ENTE_BDAP_PARTECIPATO",
                "DATA_INIZIO_RELAZIONE",
                "QUOTA_DIRETTA"
            ],
        )

        # do some more filtering here (test and debug)
        # od = od[
        #   (od.DESCR_FORMA_GIURIDICA == 'Regione') |
        #   (od.DESCR_TIPOLOGIA_SIOPE == 'CONSIGLI REGIONALI')
        #   (od.DESCR_FORMA_GIURIDICA == 'Comune') &
        #   (
        #     (od.DENOMINAZIONE == 'COMUNE DI PIAN DI SCO') |
        #     (od.DENOMINAZIONE == 'COMUNE DI SANT OMOBONO TERME')
        #   )
        # ]

        # convert nulls into None
        od = od.where((pd.notnull(od)), None)

        od['DATA_INIZIO_RELAZIONE'] = od['DATA_INIZIO_RELAZIONE'].apply(
            lambda d: d[:4]
        )
        od['DATA_FINE_RELAZIONE'] = od['DATA_FINE_RELAZIONE'].apply(
            lambda d: d[:4] if d is not None else None
        )

        def transform_item(item):
            """Transform item in a nested python structure, ready to be processed by a generic loader.

            :param item:
            :return:
            """
            processed_item = {
                "owning_org": {
                    "name": item["DENOM_ENTE_PARTECIPANTE"],
                    "identifier": item["CF_ENTE_PARTECIPANTE"],
                    "identifiers": [{
                        "scheme": "CODICE_ENTE_BDAP",
                        "identifier": item["CODICE_ENTE_BDAP_PARTECIPANTE"]
                    }]
                },
                "owned_org": {
                    "name": item["DENOM_ENTE_PARTECIPATO"],
                    "identifier": item["CF_ENTE_PARTECIPATO"],
                    "identifiers": [{
                        "scheme": "CODICE_ENTE_BDAP",
                        "identifier": item["CODICE_ENTE_BDAP_PARTECIPATO"]
                    }]
                },
                "percentage": 100 * float(item.get("QUOTA_DIRETTA", 0.0)),
                "start_date": item.get("DATA_INIZIO_RELAZIONE", None),
                "end_date": item.get("DATA_FINE_RELAZIONE", None),
                "sources": [{
                    "url": "https://bdap-opendata.mef.gov.it/content/anagrafica-enti-partecipazioni-ente",
                    "note": "OpenBDAP - ANAGRAFE ENTI - PARTECIPAZIONI ENTE"
                }]
            }

            return processed_item

        self.etl.logger.info("Starting transform_item")
        # define transformation for each item as a generator
        # to be passed along to the loader, without resolution
        od = (transform_item(item) for item in [i[1] for i in od.iterrows()])
        self.etl.logger.info("transform_item finished")

        # store processed data into the ETL instance
        self.etl.processed_data = od
