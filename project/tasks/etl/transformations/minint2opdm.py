from datetime import datetime
import logging
import re

from ooetl.transformations import Transformation
import pandas as pd

from project.core import labels_utils
from project.core.dicts_functions import (
    get_organs_dict_by_minint_code, get_organs_dict_by_istat_code,
    get_organs_dict_by_sigla_prov, get_organs_dict_by_city_prov,
)


class Minint2OpdmTransformation(Transformation):
    """Instance of ``ooetl.ETL`` class that handles
    importing historical minint data into Django-Popolo model
    """

    def __init__(self, *args, **kwargs):

        self.verbosity = kwargs.pop("verbosity", False)
        self.log_level = kwargs.pop("log_level", logging.INFO)

        super().__init__()

        self.loc_code_filter = kwargs.pop("loc_code_filter", None)
        self.loc_desc_filter = kwargs.pop("loc_desc_filter", None)

        self.filtered_columns = []
        self.renamed_columns = {}
        self.context = ""

    def filter_rows(self, df):
        """override this to apply some filtering to the dataframe

        :param df:
        :return:
        """
        return df

    def get_organs_dict(self):
        """Default method to extract organs dict, can be overridden"""
        if hasattr(self, "year") and self.year == "2013":
            return get_organs_dict_by_minint_code(self.context)
        else:
            return get_organs_dict_by_istat_code(self.context)

    def transform(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns.

        :return: the ETL instance (to chain methods)
        """

        self.logger.info("start of transform")

        self.logger.debug("  reading organ's dict")
        organs_dict = self.get_organs_dict()

        # fix source in items, use remote_url for back-compatibility
        # etl should have a source field for this purpose
        if self.etl.source:
            etl_source = self.etl.source
        else:
            etl_source = self.etl.extractor.remote_url

        self.logger.info("  get a copy of the original dataframe")
        od = self.etl.original_data.copy()

        # convert nulls into None
        od = od.where((pd.notnull(od)), None)

        # dates conversions
        def convert_date(d):
            if d:
                return datetime.strptime(d, "%d/%m/%Y").strftime("%Y-%m-%d")
            else:
                return None

        if 'incarico' in od.columns:
            od['charge_desc'] = od.incarico.fillna(od.descrizione_carica)
        elif 'descrizione_carica' in od.columns:
            od['charge_desc'] = od.descrizione_carica
        elif 'DESCRIZIONE_CARICA' in od.columns:
            od['charge_desc'] = od.DESCRIZIONE_CARICA
        else:
            raise Exception("No incarico, descrizione_carica or DESCRIZIONE_CARICA found in source CSV file")

        # select only useful columns
        od = od.filter(items=self.filtered_columns)

        # columns renaming
        od = od.rename(columns=self.renamed_columns)

        # apply subclass-specific filters (metro/provinces separation)
        od = self.filter_rows(od)

        if hasattr(self, "year") and self.year == "2013":
            od = od.rename(columns={"DATA_TEMPO_ELETTORALE": "DATA_ELEZIONE"})

            if self.context == "com":
                od["loc_code"] = (
                    od["COD_REGIONE"] + od["COD_PROVINCIA"] + od["loc_code"]
                )
                del od["COD_PROVINCIA"]
                del od["COD_REGIONE"]

        # filter rows based on loc_code_filter
        # may use jolly (as in '1%')
        if self.loc_code_filter:
            self.logger.info("  apply loc_code filter")
            if "%" in self.loc_code_filter:
                cond = od.loc_code.str.startswith(self.loc_code_filter.strip("%"))
            else:
                cond = od.loc_code == self.loc_code_filter
            od = od[cond]
        if len(od) == 0:
            raise Exception(
                "No data found for loc_code {0}!".format(self.loc_code_filter)
            )

        # filter rows based on loc_desc_filter
        # may use jolly (as in '1%')
        if self.loc_desc_filter:
            self.logger.info("  apply loc_desc filter")
            if "%" in self.loc_desc_filter:
                cond = od.loc_desc.str.startswith(self.loc_desc_filter.strip("%"))
            else:
                cond = od.loc_desc == self.loc_desc_filter
            od = od[cond]
        if len(od) == 0:
            raise Exception(
                "No data found for loc_desc {0}!".format(self.loc_desc_filter)
            )

        self.logger.info("  convert dates")
        for date_field in ["election", "birth", "start", "end"]:
            full_date_field = date_field + "_date"
            if full_date_field in list(od.columns):
                od[full_date_field] = od[full_date_field].apply(
                    lambda d: convert_date(d)
                )

        # capitalize (title) names
        od["given_name"] = od["given_name"].map(str.title)
        od["family_name"] = od["family_name"].map(str.title)
        od["name"] = od["given_name"] + " " + od["family_name"]

        self.logger.info("  split_birth_location")

        if "birth_location" in list(od.columns):

            def split_birth_location(loc):
                if not loc:
                    return ""
                m = re.search(r"(.+) \((\w+)\)", loc)
                if m:
                    return "{0} ({1})".format(m.group(1).title(), m.group(2).upper())
                else:
                    return loc.title()

            od["birth_location"] = od["birth_location"].apply(split_birth_location)
        else:
            od["birth_location"] = None

        self.logger.info("  substitute accents in locations")

        def substitute_accents_in_locs(x):
            m_de = re.match(".* de' .*", x.lower())
            m_ne = re.match(".* ne' .*", x.lower())
            m_ca = re.match("^ca' .*", x.lower())
            m_vo = re.match("^vo'$", x.lower())

            if not m_de and not m_ne and not m_vo and not m_ca:
                return (
                    x.title()
                    .replace("i'", "ì")
                    .replace("e'", "è")
                    .replace("a'", "à")
                    .replace("o'", "ò")
                    .replace("u'", "ù")
                )
            else:
                return x.title().replace(" De' ", " de' ")

        od["loc_desc"] = od["loc_desc"].apply(substitute_accents_in_locs)

        if self.context == "com" and self.is_current:

            def get_loc_code_for_com(r):
                """Transform loc_code column into <name (pv)>,
                for current minint ammcom records"""
                return "{0} ({1})".format(r["loc_desc"], r["loc_code"])

            od["loc_code"] = od.apply(get_loc_code_for_com, axis=1)

        # get election's data
        self.logger.debug("  get election's data")

        def get_election_name(r):
            election_date = r.get("election_date", "")
            return "Elezioni {0} {1}".format(
                labels_utils.context2adj(self.context)
                .replace("ale", "ali")
                .replace("ana", "ane")
                .replace("comunal", "comunali")
                .replace("provincial", "provinciali")
                .replace("metropolitan", "metropolitane")
                .replace("regional", "regionali"),
                datetime.strptime(election_date, "%Y-%m-%d").strftime("%d/%m/%Y") if election_date else None,
            )

        od["election_name"] = od.apply(get_election_name, axis=1)
        od["election_classification"] = self.context.upper()

        # get organization's data

        self.logger.info("  get organization's classification")

        def get_organization_classification(r):
            normalized_charge = labels_utils.normalize_charge(r["charge_desc"])
            organ = labels_utils.charge2organ(normalized_charge).title()
            adjective = labels_utils.context2adj(self.context).replace("al", "ale")
            if organ == "Conferenza":
                adjective = adjective.replace("tan", "tana")
            else:
                adjective = adjective.replace("tan", "tano")
            return "{0} {1}".format(organ, adjective)

        od["org_classification"] = od.apply(get_organization_classification, axis=1)

        self.logger.info("  get organizations' pk")

        def get_org_pk(r):
            normalized_charge = labels_utils.normalize_charge(r["charge_desc"])
            organ = labels_utils.charge2organ(normalized_charge).lower()

            # exception for consigli metropolitani
            if r["org_classification"] == "Consiglio metropolitano":
                organ = "consiglio_metropolitano"

            item = organs_dict.get(r["loc_code"].lower(), None)
            if item:
                organ_in_dict = item.get(organ, None)
                if organ_in_dict:
                    return organ_in_dict.get("pk", "")
            return ""

        od["org_id"] = od.apply(get_org_pk, axis=1)
        od["org_scheme"] = ""

        self.logger.info("  get organization's name")

        def get_organization_name(r):
            # prepos = labels_utils.area_prep(r['loc_desc'], self.context[0].upper())
            # return "{0} {1} {2}".format(
            #     r['org_classification'],
            #     prepos,
            #     r['loc_desc']
            # )
            normalized_charge = labels_utils.normalize_charge(r["charge_desc"])
            organ = labels_utils.charge2organ(normalized_charge).lower()

            # exception for consigli metropolitani
            if r["org_classification"] == "Consiglio metropolitano":
                organ = "consiglio_metropolitano"

            item = organs_dict.get(r["loc_code"].lower(), None)
            if item:
                organ_in_dict = item.get(organ, None)
                if organ_in_dict:
                    return organ_in_dict.get("name", "")
            return ""

        od["org_name"] = od.apply(get_organization_name, axis=1)
        od["org_valid_at"] = od["start_date"]

        # get role type's data
        self.logger.info("  get role type's data")

        def get_role_type_label(r):
            return labels_utils.build_role_for_local_adm(
                r["charge_desc"], self.context
            )

        od["role_type_label"] = od.apply(get_role_type_label, axis=1)

        # get post's data
        self.logger.info("  get post's data")

        def get_post_label(r):
            return labels_utils.build_label_for_local_adm(
                r["charge_desc"], r["org_name"]
            )

        od["post_label"] = od.apply(get_post_label, axis=1)

        def transform_item(item):
            """Enrich each item with sources info

            :param item:
            :return:
            """
            processed_item = item
            processed_item["sources"] = [
                {"url": etl_source, "note": "Dati Amministratori locali Ministero dell'Interno"}
            ]
            return processed_item

        # transform each item
        od = od.apply(transform_item, axis=1)

        # store processed data into the ETL instance
        self.etl.processed_data = od

        self.logger.info("end of transform")

        # return ETL instance
        return self.etl


class HistMinint2OpdmTransformation(Minint2OpdmTransformation):
    def __init__(self, *args, **kwargs):
        self.verbosity = kwargs.pop("verbosity", False)
        self.log_level = kwargs.pop("log_level", logging.INFO)

        super().__init__(
            *args, verbosity=self.verbosity, log_level=self.log_level, **kwargs
        )

        self.loc_code_filter = kwargs.get("loc_code_filter", None)
        self.loc_desc_filter = kwargs.get("loc_desc_filter", None)
        self.year = kwargs.get("year", None)

        if self.year == "2013":
            self.filtered_columns = [
                "DATA_TEMPO_ELETTORALE",
                "SIGLA_TITOLO_ACCADEMICO",
                "COGNOME",
                "NOME",
                "SESSO",
                "DATA_NASCITA",
                "DESC_SEDE_NASCITA",
                "TITOLO_DI_STUDIO",
                "PROFESSIONE",
                "DESCRIZIONE_CARICA",
                "SEQ_ORDINA",
                "DATA_NOMINA",
                "DESC_PARTITO",
                "DATA_CESSAZIONE",
            ]

            self.renamed_columns = {
                "SIGLA_TITOLO_ACCADEMICO": "honorific_prefix",
                "COGNOME": "family_name",
                "NOME": "given_name",
                "SESSO": "gender",
                "DATA_NASCITA": "birth_date",
                "DESC_SEDE_NASCITA": "birth_location",
                "TITOLO_DI_STUDIO": "education_level",
                "PROFESSIONE": "profession",
                "SEQ_ORDINA": "charge_code",
                "DESCRIZIONE_CARICA": "charge_desc",
                "DATA_NOMINA": "start_date",
                "DATA_TEMPO_ELETTORALE": "election_date",
                "DESC_PARTITO": "party_list_coalition",
                "DATA_CESSAZIONE": "end_date",
            }
        else:
            self.filtered_columns = [
                "DATA_ELEZIONE",
                "SIGLA_TITOLO_ACCADEMICO",
                "COGNOME",
                "NOME",
                "SESSO",
                "DATA_NASCITA",
                "SEDE_NASCITA",
                "TITOLO_DI_STUDIO",
                "PROFESSIONE",
                "DESCRIZIONE_CARICA",
                "LIVELLO_CARICA",
                "DATA_NOMINA",
                "PARTITO_LISTA_COALIZIONE",
                "DATA_CESSAZIONE",
            ]

            self.renamed_columns = {
                "SIGLA_TITOLO_ACCADEMICO": "honorific_prefix",
                "COGNOME": "family_name",
                "NOME": "given_name",
                "SESSO": "gender",
                "DATA_NASCITA": "birth_date",
                "SEDE_NASCITA": "birth_location",
                "TITOLO_DI_STUDIO": "education_level",
                "PROFESSIONE": "profession",
                "LIVELLO_CARICA": "charge_code",
                "DESCRIZIONE_CARICA": "charge_desc",
                "DATA_NOMINA": "start_date",
                "DATA_ELEZIONE": "election_date",
                "PARTITO_LISTA_COALIZIONE": "party_list_coalition",
                "DATA_CESSAZIONE": "end_date",
            }

        self.context = ""
        self.is_current = False


class HistMinintReg2OpdmTransformation(HistMinint2OpdmTransformation):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.year == "2013":
            self.filtered_columns = [
                "DESC_REGIONE",
                "COD_REGIONE",
            ] + self.filtered_columns

            self.renamed_columns.update(
                {"DESC_REGIONE": "loc_desc", "COD_REGIONE": "loc_code"}
            )
        else:
            self.filtered_columns = [
                "DESCRIZIONE_REGIONE",
                "ISTAT_CODICE_REGIONE",
            ] + self.filtered_columns

            self.renamed_columns.update(
                {"DESCRIZIONE_REGIONE": "loc_desc", "ISTAT_CODICE_REGIONE": "loc_code"}
            )

        self.context = "reg"


class HistMinintProv2OpdmTransformation(HistMinint2OpdmTransformation):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.year == "2013":
            self.filtered_columns = [
                "DESC_PROVINCIA",
                "COD_PROVINCIA",
            ] + self.filtered_columns

            self.renamed_columns.update(
                {"DESC_PROVINCIA": "loc_desc", "COD_PROVINCIA": "loc_code"}
            )
        else:
            self.filtered_columns = [
                "DESRIZIONE_PROVINCIA",
                "DESCRIZIONE_PROVINCIA",
                "ISTAT_CODICE_PROVINCIA",
            ] + self.filtered_columns

            self.renamed_columns.update(
                {
                    "DESRIZIONE_PROVINCIA": "loc_desc",
                    "DESCRIZIONE_PROVINCIA": "loc_desc",
                    "ISTAT_CODICE_PROVINCIA": "loc_code",
                }
            )

        self.context = "prov"

    def filter_rows(self, df):
        """keep only rows not related to metropolitan cities

        :param df:
        :return:
        """

        df = super().filter_rows(df)

        cond = True

        # exclude metropolitan areas when needed
        if int(self.year) > 2014:
            for loc_desc in [
                "ROMA",
                "MILANO",
                "NAPOLI",
                "TORINO",
                "GENOVA",
                "BOLOGNA",
                "FIRENZE",
                "VENEZIA",
                "BARI",
            ]:
                cond = cond & (df.loc_desc != loc_desc)

        if int(self.year) > 2015:
            for loc_desc in ["REGGIO CALABRIA", "CAGLIARI"]:
                cond = cond & (df.loc_desc != loc_desc)

        if cond is not True:
            df = df[cond]
        return df


class HistMinintMetro2OpdmTransformation(HistMinint2OpdmTransformation):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.year == "2013":
            self.filtered_columns = [
                "DESC_PROVINCIA",
                "COD_PROVINCIA",
            ] + self.filtered_columns

            self.renamed_columns.update(
                {"DESC_PROVINCIA": "loc_desc", "COD_PROVINCIA": "loc_code"}
            )
        else:
            self.filtered_columns = [
                "DESRIZIONE_PROVINCIA",
                "DESCRIZIONE_PROVINCIA",
                "ISTAT_CODICE_PROVINCIA",
            ] + self.filtered_columns

            self.renamed_columns.update(
                {
                    "DESRIZIONE_PROVINCIA": "loc_desc",
                    "DESCRIZIONE_PROVINCIA": "loc_desc",
                    "ISTAT_CODICE_PROVINCIA": "loc_code",
                }
            )

        self.context = "cm"

    def filter_rows(self, df):
        """keep only rows related to metropolitan cities

        :param df:
        :return:
        """

        df = super().filter_rows(df)
        cond = False

        # include metropolitan area when needed
        if int(self.year) > 2014:
            for loc_desc in [
                "ROMA",
                "MILANO",
                "NAPOLI",
                "TORINO",
                "GENOVA",
                "BOLOGNA",
                "FIRENZE",
                "VENEZIA",
                "BARI",
            ]:
                cond = cond | (df.loc_desc == loc_desc)

        if int(self.year) > 2015:
            for loc_desc in ["REGGIO CALABRIA", "CAGLIARI"]:
                cond = cond | (df.loc_desc == loc_desc)

        if cond is not False:
            df = df[cond]
        return df


class HistMinintCom2OpdmTransformation(HistMinint2OpdmTransformation):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.year == "2013":
            self.filtered_columns = [
                "DESC_COMUNE",
                "COD_COMUNE",
                "COD_PROVINCIA",
                "COD_REGIONE",
            ] + self.filtered_columns

            self.renamed_columns.update(
                {"DESC_COMUNE": "loc_desc", "COD_COMUNE": "loc_code"}
            )
        else:
            self.filtered_columns = [
                "DESCRIZIONE_COMUNE",
                "ISTAT_CODICE_COMUNE",
            ] + self.filtered_columns

            self.renamed_columns.update(
                {"DESCRIZIONE_COMUNE": "loc_desc", "ISTAT_CODICE_COMUNE": "loc_code"}
            )

        self.context = "com"


class CurrentMinint2OpdmTransformation(Minint2OpdmTransformation):
    def __init__(self, *args, **kwargs):
        self.verbosity = kwargs.pop("verbosity", False)
        self.log_level = kwargs.pop("log_level", logging.INFO)

        super().__init__(
            *args, verbosity=self.verbosity, log_level=self.log_level, **kwargs
        )

        self.loc_code_filter = kwargs.get("loc_code_filter", None)
        self.loc_desc_filter = kwargs.get("loc_desc_filter", None)

        self.filtered_columns = [
            "cognome",
            "nome",
            "sesso",
            "data_nascita",
            "luogo_nascita",
            "titolo_studio",
            "professione",
            "charge_desc",
            "data_entrata_in_carica",
            "data_elezione",
            "lista_appartenenza/collegamento",
        ]

        self.renamed_columns = {
            "cognome": "family_name",
            "nome": "given_name",
            "sesso": "gender",
            "data_nascita": "birth_date",
            "luogo_nascita": "birth_location",
            "titolo_studio": "education_level",
            "professione": "profession",
            "data_entrata_in_carica": "start_date",
            "data_elezione": "election_date",
            "lista_appartenenza/collegamento": "party_list_coalition",
        }
        self.context = ""
        self.is_current = True


class CurrentMinintReg2OpdmTransformation(CurrentMinint2OpdmTransformation):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filtered_columns = [
            "denominazione_regione",
            "codice_regione",
        ] + self.filtered_columns

        self.renamed_columns.update(
            {"denominazione_regione": "loc_desc", "codice_regione": "loc_code"}
        )

        self.context = "reg"


class CurrentMinintProv2OpdmTransformation(CurrentMinint2OpdmTransformation):
    def get_organs_dict(self):
        return get_organs_dict_by_sigla_prov("prov")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filtered_columns = [
            "denominazione_provincia",
            "sigla_provincia",
        ] + self.filtered_columns

        self.renamed_columns.update(
            {"denominazione_provincia": "loc_desc", "sigla_provincia": "loc_code"}
        )

        self.context = "prov"


class CurrentMinintMetro2OpdmTransformation(CurrentMinint2OpdmTransformation):
    def get_organs_dict(self):
        return get_organs_dict_by_sigla_prov("metro")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filtered_columns = [
            "denominazione_provincia",
            "sigla_provincia",
        ] + self.filtered_columns

        self.renamed_columns.update(
            {"denominazione_provincia": "loc_desc", "sigla_provincia": "loc_code"}
        )

        self.context = "cm"


class CurrentMinintCom2OpdmTransformation(CurrentMinint2OpdmTransformation):
    def get_organs_dict(self):
        return get_organs_dict_by_city_prov()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filtered_columns = [
            "denominazione_comune",
            "sigla_provincia",
        ] + self.filtered_columns

        self.renamed_columns.update(
            {"denominazione_comune": "loc_desc", "sigla_provincia": "loc_code"}
        )

        self.context = "com"


class CurrentMinintComm2OpdmTransformation(CurrentMinint2OpdmTransformation):
    def get_organs_dict(self):
        return get_organs_dict_by_city_prov()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.filtered_columns = [
            "cognome",
            "nome",
            "descrizione_carica",
            "data_elezione",
            "data_entrata_in_carica",
            "denominazione_comune",
            "sigla_provincia",
        ]

        self.renamed_columns = {
            "cognome": "family_name",
            "nome": "given_name",
            "descrizione_carica": "charge_desc",
            "data_entrata_in_carica": "start_date",
            "data_elezione": "election_date",
            "denominazione_comune": "loc_desc",
            "sigla_provincia": "loc_code",
        }

        self.context = "com"
