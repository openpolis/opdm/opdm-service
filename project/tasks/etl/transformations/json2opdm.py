from ooetl.transformations import Transformation
from popolo.models import Organization

from project.core.dicts_functions import get_orgs_dict_by_org_id, get_roletypes_dict_by_role


class JsonPersonsMemberships2OpdmTransformation(Transformation):
    """Transform persons and memberships from a JSON source
    for the PersonsWithMemberships loader
    """

    @staticmethod
    def filter_rows(od):
        # return filter(lambda x:x['family_name'] == 'Forteleoni', od)
        return od

    def transform(self):
        """ Transform a list of dicts parsed from the JSON file,
        annotating the memberships with electoral KeyEvent, Organization, and RoleType instances,
        in order to re-use the PersonsWithMemberships loader
        """
        self.logger.info("start of transform")
        classification_labels = [
            'Assemblea parlamentare',
            'Governo della Repubblica',
            'Presidenza del Consiglio',
            'Organo legislativo internazionale o europeo',
            'Partito/Movimento politico',
            'Autorità indipendente',
            'Organo costituzionale o a rilevanza costituzionale',
        ]
        self.logger.debug("  reading organ's dict")
        orgs_dict = get_orgs_dict_by_org_id(
            classifications_labels=classification_labels
        )
        roletypes_dict = get_roletypes_dict_by_role(
            classifications_labels=classification_labels
        )

        self.logger.info("  get a copy of the original dataframe")
        od = self.etl.original_data

        # apply subclass-specific filters (metro/provinces separation)
        od = self.filter_rows(od)

        # annotate organization and membership
        def annotate_memberships(r):
            for m in r.get('memberships', []):
                election = None
                try:
                    m['organization'] = orgs_dict[m['organization_id']]['organization']
                    election = orgs_dict[m['organization_id']]['election']
                    m['role_type'] = roletypes_dict[m['role'].lower()]

                except KeyError:

                    # populate orgs_dict and roletypes_dict from DB, and warn about possible optimisation
                    org = Organization.objects.get(pk=m['organization_id'])
                    orgs_dict.update(
                        get_orgs_dict_by_org_id(classifications_labels=[org.classification])
                    )
                    roletypes_dict.update(
                        get_roletypes_dict_by_role(classifications_labels=[org.classification])
                    )

                    # emit warning to optimise (not really needed)
                    self.logger.warning(
                        f"Add [{org.classification}] to classification_labels in import script to optimize this script."
                    )

                    # re-assign variables
                    m['organization'] = orgs_dict[m['organization_id']]['organization']
                    election = orgs_dict[m['organization_id']]['election']
                    m['role_type'] = roletypes_dict[m['role'].lower()]

                finally:
                    # complete assignments
                    if 'electoral_event_id' not in m:
                        m['electoral_event_id'] = election.id if election else None
                    m['organization_name'] = m['organization'].name
                    m['role_type_label'] = m['role_type'].label

            return r

        od = map(annotate_memberships, od)

        # store processed data into the Transformation instance
        self.etl.processed_data = od
