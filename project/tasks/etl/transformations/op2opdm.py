import logging

from ooetl import ETL
import pandas as pd
from popolo.models import ContactDetail

from project.core import labels_utils
from project.core.dicts_functions import get_organs_dict_by_op_id


class Op2OpdmETL(ETL):
    """Instance of ``ooetl.ETL`` class that handles
    importing persons and memberships coming from OPAPI, into OPDM
    """
    RESOURCES_TYPES_MAP = {
        'email ufficiale': ContactDetail.CONTACT_TYPES.email,
        'altra email': ContactDetail.CONTACT_TYPES.email,
        'url ufficiale': ContactDetail.CONTACT_TYPES.url,
        'altra url': ContactDetail.CONTACT_TYPES.url,
        'twitter': ContactDetail.CONTACT_TYPES.twitter,
        'facebook': ContactDetail.CONTACT_TYPES.facebook,
    }

    def __init__(self, extractor, loader, **kwargs):

        self.log_level = kwargs.get('log_level', logging.INFO)
        self.loc_code_filter = kwargs.get('loc_code_filter', None)
        super().__init__(extractor, loader, log_level=self.log_level)

    def transform_memberships(self):
        """ Transform a list of dicts parsed from OPAPI,
        into a dataframe, suitable for use with the PopoloMembershipLoader

        :return: the ETL instance (to chain methods)
        """
        self.logger.info("start of transform")

        self.logger.info("  reading organ's dict")
        organs_dict = get_organs_dict_by_op_id()

        # get a copy of the original data
        self.logger.info("  get a copy of the original dataframe")
        od = pd.DataFrame(self.original_data)

        # select only useful columns
        od = od.filter(items=[
            'location', 'location_descr', 'charge_type_descr', 'politician', 'date_start', 'date_end',
            'institution_descr', 'description', 'party'
        ])

        # add source
        od['source'] = 'http://api3.openpolis.it'

        # person's ID used in this ETL is the OP unique identifier
        od['person_id'] = od.politician.apply(lambda x: x['content']['id'])
        od['person_id_scheme'] = 'OP_ID'

        def get_party(item):
            if item['acronym'] is None:
                return "{0}".format(item['name'])
            else:
                return "{0} ({1})".format(item['name'], item['acronym'])

        od['party_list_coalition'] = od.party.apply(get_party)

        od['loc_code'] = od.location.apply(lambda x: x.split('/')[-1].split('.')[0])
        od['loc_desc'] = od.location_descr.apply(
            lambda x: x.replace('Comune di ', '').replace('Regione ', '').replace('Provincia di ', ''))

        # no info on elections for this ETL
        od['election_date'] = ''
        od['election_name'] = ''

        od['org_classification'] = od.institution_descr.apply(
            lambda x: x.replace('Comunale', 'comunale').
            replace('Provinciale', 'provinciale').
            replace('Metropolitano', 'metropolitano').
            replace('Regionale', 'regionale')
            )
        od['org_valid_at'] = od['date_start']

        od['organ'] = od['institution_descr'].apply(
            lambda x: x.split(' ')[0].lower() if x.lower() != 'consiglio metropolitano' else 'consiglio_metropolitano'
        )
        od['context'] = od['institution_descr'].apply(lambda x: x.split(' ')[1].lower()[:3])

        def get_organization_info(r, field):
            item = organs_dict.get(r['loc_code'], None)
            if item:
                organ_in_dict = item.get(r['organ'], None)
                if organ_in_dict:
                    return organ_in_dict.get(field, '')
            return ''

        def get_organization_pk(r):
            return get_organization_info(r, 'pk')

        def get_organization_name(r):
            return get_organization_info(r, 'name')

        od['org_name'] = od.apply(get_organization_name, axis=1)
        od['org_id'] = od.apply(get_organization_pk, axis=1)
        od['org_scheme'] = ''

        def get_charge_descr(r):
            return labels_utils.build_charge_descr_from_opapi_data(
                r['charge_type_descr'], r['institution_descr'], r['description']
            )

        od['charge_desc'] = od.apply(get_charge_descr, axis=1)

        def get_role_type_label(r):
            return labels_utils.build_role_for_local_adm(r['charge_desc'], r['context'])

        od['role_type_label'] = od.apply(get_role_type_label, axis=1)

        def get_post_label(r):
            return labels_utils.build_label_for_local_adm(r['charge_desc'], r['org_name'])

        od['post_label'] = od.apply(get_post_label, axis=1)

        # drop non-used columns
        od.drop(columns=[
            'politician', 'party', 'location', 'location_descr',
            'institution_descr', 'organ'
        ], inplace=True)

        # columns renaming
        od = od.rename(
            columns={
                'date_start': 'start_date',
                'date_end': 'end_date',
            }
        )

        # store processed data into the ETL instance
        self.processed_data = od

        # return ETL instance
        return self

    def transform_persons(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns.

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original data
        od = self.original_data.copy()

        def pre_filter_item(item):
            """define a filter here, if needed (dev/debug/...)

            :param item: the item to be processed
            :return: boolean, whether to pass or not
            """
            if item:
                return True

        def transform_item(item):
            """Transform each person from an op-api structure into an opdm structure

            :param item:
            :return:
            """
            politician = item['politician']

            family_name = politician['last_name'].title()
            given_name = politician['first_name'].title()
            name = "{0} {1}".format(
                given_name,
                family_name
            )
            sort_name = "{0} {1}".format(
                family_name.lower(),
                given_name.lower()
            )

            processed_item = {
                'name': name,
                'given_name': given_name,
                'family_name': family_name,
                'sort_name': sort_name,
                'created_at': politician['content']['created_at'] + "+0100",
                'updated_at': politician['content']['updated_at'] + "+0100"
            }
            if 'birth_date' in politician and politician['birth_date']:
                processed_item['birth_date'] = politician['birth_date'].split('T')[0]
            if 'death_date' in politician and politician['death_date']:
                processed_item['death_date'] = politician['death_date'].split('T')[0]
            if 'birth_location' in politician and politician['birth_location']:
                processed_item['birth_location'] = politician['birth_location']
            if 'sex' in politician and politician['sex']:
                processed_item['gender'] = politician['sex']

            processed_item['identifiers'] = [
                {'scheme': 'OP_ID', 'identifier': politician['content']['id']}
            ]
            processed_item['links'] = [
                {'url': politician['self_uri'], 'note': "api3.openpolis.it URI"},
                {'url': politician['openparlamento_uri'], 'note': "parlamento.openpolis.it URI"},
                {'url': politician['image_uri'], 'note': "image URI"},
            ]

            processed_item['sources'] = [
                {'url': "http://api3.openpolis.it", 'note': "api3.openpolis.it"},
            ]

            if (
                'profession' in politician and politician['profession'] and
                'description' in politician['profession'] and politician['profession']['description']
            ):
                processed_item['profession'] = politician['profession']['description']

            if (
                'education_levels' in politician and len(politician['education_levels']) > 0 and
                'education_level' in politician['education_levels'][0] and
                politician['education_levels'][0]['education_level']['description']
            ):
                processed_item['education_level'] = politician['education_levels'][0]['education_level']['description']

                if (
                    'description' in politician['education_levels'][0] and
                    politician['education_levels'][0]['description']
                ):
                    processed_item['education_level'] = "{0} ({1})".format(
                        processed_item['education_level'],
                        politician['education_levels'][0]['description']
                    )

            if 'resources' in politician and len(politician['resources']) > 0:
                processed_item['contact_details'] = []
                for resource in politician['resources']:
                    if 'url' in resource['resource_type'].lower():
                        processed_item['links'].append({
                            'note': resource['resource_type'],
                            'url': resource['value']
                        })
                    else:
                        processed_item['contact_details'].append({
                            'label': resource['resource_type'],
                            'value': resource['value'],
                            'contact_type': self.RESOURCES_TYPES_MAP[
                                resource['resource_type'].lower()
                            ],
                            'created_at': politician['content']['created_at'] + "+0100",
                            'updated_at': resource['updated_at'] + "+0100"
                        })

            return processed_item

        # pre-filter elements, accordinc to defined logic
        od = filter(pre_filter_item, od)

        # remove duplicates using a dict-comprehension
        od = {x['politician']['content']['id']: x for x in od}.values()

        # transform each item
        od = map(transform_item, od)

        # store processed data into the ETL instance
        self.processed_data = od

        # return ETL instance
        return self

    def etl(self):
        """ Shortcut for ``extract().transform().load()``

        May be used when there are no parameters to be passed to the
        intermediate methods
        """
        return self.extract().transform().load()
