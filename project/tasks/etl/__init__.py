from ooetl.transformations import Transformation
import pandas as pd


class RemoveCRTransformation(Transformation):
    def transform(self, **kwargs):
        # get a copy of the original dataframe
        od = self.etl.original_data.copy()

        # strip \n from headers and rows
        if isinstance(self.etl.original_data, pd.DataFrame):
            od.columns = [x.strip().replace('\n', '') for x in od.columns]
            od = od.replace('\r\n|\r|\n', ' ', regex=True)

        # store processed data into the ETL instance
        self.etl.processed_data = od

        # return ETL instance
        return self


# functions returning unique tuples from a dict containing values


class ETLException(Exception):
    pass
