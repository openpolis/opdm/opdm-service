import logging
import os

from ooetl import ETL, DummyTransformation
from ooetl.extractors import CSVExtractor
from ooetl.loaders import CSVLoader, JsonLoader

from project.tasks.etl import ETLException, RemoveCRTransformation
from project.tasks.etl.composites.diff_logics import JsonOutRemovesDiffLogic, ShallowUpdatesDiffLogic
from project.tasks.etl.extractors import JsonArrayExtractor


class CompositeETL(ETL):
    """
    Class extending ETL used for procedures made out of complex compositions of other ETLs.

    The normal etl() flux is blocked, and only the __call__() method can be used.
    """

    def etl(self):
        raise ETLException("Not available in Composite ETL classes")

    def extract(self, **kwargs):
        raise ETLException("Not available in Composite ETL classes")

    def transform(self, **kwargs):
        raise ETLException("Not available in Composite ETL classes")

    def load(self, **kwargs):
        raise ETLException("Not available in Composite ETL classes")


class CSVDiffCompositeETL(CompositeETL):
    """

    """
    def __init__(self, *args, **kwargs):
        self.local_cache_path = kwargs.pop('local_cache_path', './data/cache')
        self.local_out_path = kwargs.pop('local_out_path', './data/out')
        self.filename = kwargs.pop('filename', 'out')
        self.clear_cache = kwargs.pop('clear_cache', False)
        self.unique_idx_cols = kwargs.pop('unique_idx_cols', None)
        self.extended_attributes = kwargs.pop('extended_attributes', {})
        super().__init__(*args, **kwargs)
        self.sep = self.extractor.sep
        self.encoding = self.extractor.encoding

        # create dirs if not existing
        if not os.path.isdir(self.local_cache_path):
            os.mkdir(self.local_cache_path)
        if not os.path.isdir(self.local_out_path):
            os.mkdir(self.local_out_path)

    def __call__(self):
        from project.core.diff_utils import DiffHelper
        with DiffHelper(
            local_cache_path=self.local_cache_path,
            filename=self.filename, clear_cache=self.clear_cache,
                fmt="csv"
        ) as diff_helper:
            label = ".".join(diff_helper.new.split('/')[-1].split('.')[:-1])

            self.logger.info("Downloading new version into cache from {0}".format(self.extractor.source))
            ETL(
                extractor=self.extractor,
                transformation=RemoveCRTransformation(),
                loader=CSVLoader(
                    diff_helper.local_cache_path,
                    label=label,
                    sep=self.sep,
                    encoding=self.encoding
                ),
                log_level=self.logger.level
            )()

            self.logger.info("Extracting differences")
            diff_helper.extract_differences(
                encoding=self.encoding, sep=self.sep, index_cols=self.unique_idx_cols
            )

            if diff_helper.adds_and_updates['n']:
                self.logger.info(
                    "Starting proper ETL process for {0} records".format(
                        diff_helper.adds_and_updates['n']
                    )
                )

                class TEST(CSVExtractor):

                     def extract(self, **kwargs):
                        import pandas as pd
                        """Extracts data from a remote, csv source

                        Args:
                            **kwargs: Arbitrary keyword arguments.

                        Returns:
                            :class:`pandas.DataFrame`: The dataframe containing extracted items
                        """

                        df = pd.read_csv(
                            self.source,
                            index_col=False,
                            sep=self.sep,
                            skiprows=self.skiprows,
                            skipfooter=self.skipfooter,
                            header=self.header,
                            encoding=self.encoding,
                            dtype=self.dtype,
                            converters=self.converters,
                            na_filter=self.na_filter,
                            na_values=self.na_values,
                            keep_default_na=self.keep_default_na,
                        )

                        return df

                original_etl = ETL(
                    extractor=TEST(
                        diff_helper.adds_and_updates['io'],
                        sep=self.sep,
                        na_values=['', '-'], keep_default_na=False
                    ),
                    transformation=self.transformation,
                    loader=self.loader,
                    logger=self.logger,
                    log_level=self.logger.level
                )
                original_etl.source = self.extractor.source
                for k, v in self.extended_attributes.items():
                    setattr(original_etl, k, v)

                original_etl()

                original_etl.extractor.source = original_etl.source
            else:
                self.logger.info("No records to add or update")

            if diff_helper.removes['n']:
                self.logger.info(
                    "Storing {0} records to be removed into {1}".format(
                        diff_helper.removes['n'], self.local_out_path
                    )
                )
                ETL(
                    extractor=CSVExtractor(
                        diff_helper.removes['io'],
                        sep=self.sep
                    ),
                    transformation=self.transformation,
                    loader=CSVLoader(
                        self.local_out_path,
                        label='{0}_removes'.format(label.replace('_new', ''))
                    ),
                    logger=self.logger
                )()
            else:
                self.logger.info("No records to remove")


class JsonDiffCompositeETL(CompositeETL):
    """A CompositeETL instance that extract and process only the differences between a remote Json and the local cache,
    if any, in order to reduce the processing time.

    Diff analysis, through the DiffHelper context's ``diff_helper.extract_differences``, populates both the
    ``adds_and_updates`` and the ``removes`` dictionaries.

    A ``key_getter`` function (or lambda) must be passed in the constructor, so that records can be identified uniquely.
    Diff is only performed on the keys, and all records that require update or creation are bundled together into the
    ``adds_and_updates`` dictionary. Record appearing in the old, cached file, but not in the new one,
    are marked for removals, by being added to the ``removes`` dictionary.

    Records in the ``removes`` dictionary are applied a parametric removes_diff_logic,
    which, by default (``JsonOutRemovesDiffLogic``), writes these records to a json file in a given location.

    Different behaviors can be implemented in subclasses of ``RemovesDiffLogic``, by overriding the ``apply`` method.

    See ``CloseInnerMembershipsOwnershipsRemovesDiffLogic``, for an example that modifies the inner
    memberships and ownershps, by setting
    the ``end_date`` and ``end_reason`` fields to some defined values, and feed them back into the ``adds_and_updates``
    records, to be processed later on.

    Records in the ``adds_and_updates`` dictionary are transformed and loaded according to the original ETL
    procedure.
    """
    def __init__(self, *args, **kwargs):
        self.local_cache_path = kwargs.pop('local_cache_path', './data/cache')
        self.local_out_path = kwargs.pop('local_out_path', './data/out')
        self.filename = kwargs.pop('filename', 'out')
        if "." in self.filename:
            self.filename, self.extension = ".".join(self.filename.split(".")[:-1]), self.filename.split(".")[-1]
        else:
            self.filename, self.extension = self.filename, ''

        self.extended_attributes = kwargs.pop('extended_attributes', {})
        self.clear_cache = kwargs.pop('clear_cache', False)

        self.key_getter = kwargs.pop('key_getter', None)
        if self.key_getter is None:
            raise ETLException("A key_getter function must be passed to identify records uniquely")

        super().__init__(*args, **kwargs)
        self.encoding = self.extractor.encoding

        self.fmt = kwargs.pop('fmt', None)
        if self.extension.lower() in ('csv', 'json'):
            self.fmt = self.extension.lower()

        self.updates_diff_logic = kwargs.pop('updates_diff_logic', ShallowUpdatesDiffLogic())
        self.removes_diff_logic = kwargs.pop('removes_diff_logic', JsonOutRemovesDiffLogic(self.local_out_path))

        self.logger = kwargs.pop('logger', logging.getLogger(__name__))
        self.logger.setLevel(kwargs.pop('log_level', logging.INFO))

        # create dirs if not existing
        if not os.path.isdir(self.local_cache_path):
            os.mkdir(self.local_cache_path)
        if not os.path.isdir(self.local_out_path):
            os.mkdir(self.local_out_path)

    def __call__(self):
        from project.core.diff_utils import DiffHelper
        with DiffHelper(
            local_cache_path=self.local_cache_path,
            filename=self.filename,
            clear_cache=self.clear_cache,
            key_getter=self.key_getter,
            updates_diff_logic=self.updates_diff_logic,
            fmt=self.fmt,
        ) as diff_helper:
            self.logger.info("Downloading new version into cache from {0}".format(self.extractor.source))
            ETL(
                extractor=self.extractor,
                transformation=DummyTransformation(),
                loader=JsonLoader(
                    diff_helper.new, encoding=self.encoding
                ),
                log_level=self.logger.level
            )()

            self.logger.info("Extracting differences")
            diff_helper.extract_differences(
                encoding=self.encoding
            )

            # apply removal logic
            if diff_helper.removes['n']:
                self.removes_diff_logic.apply(diff_helper, etl=self)
            else:
                self.logger.info("No records found to apply removal logic")

            # goes on with the composite logic, and transform and load adds_and_updates records
            if diff_helper.adds_and_updates['n']:
                self.logger.info(
                    "{0} records to be processed for update or creation".format(
                        diff_helper.adds_and_updates['n']
                    )
                )
                original_etl = ETL(
                    extractor=JsonArrayExtractor(diff_helper.adds_and_updates['io']),
                    transformation=self.transformation,
                    loader=self.loader,
                    logger=self.logger,
                    log_level=self.logger.level
                )
                original_etl.source = self.extractor.source
                for k, v in self.extended_attributes.items():
                    setattr(original_etl, k, v)

                original_etl()
            else:
                self.logger.info("No records to add")
