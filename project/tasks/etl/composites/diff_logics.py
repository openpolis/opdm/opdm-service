from abc import ABC, abstractmethod
from datetime import datetime
from io import StringIO
import json
from typing import Tuple, Any

from ooetl import ETL
from ooetl.loaders import JsonLoader

from project.tasks.etl import ETLException
from project.tasks.etl.extractors import JsonArrayExtractor


def person_key_from_dict(x):
    """Returns a tuple with a unique index, when x is a dict, containing Person's fields

    :param x:
    :return:
    """
    if "id" in x:
        return x["id"]
    else:
        return (
            x["family_name"],
            x["given_name"],
            x.get("birth_date", None),
            x.get("gender", None),
            x.get("birth_location", None),
        )


def organization_key_from_dict(org_dict):
    """Return unique hashable tuple for organization

    :param org_dict: dict containing organization
    :return: tuple
    """
    try:
        identifier = next(
            filter(lambda x: x["scheme"] == "CF", org_dict.get("identifiers", []))
        )["identifier"]
    except StopIteration:
        identifier = org_dict.get("identifier", None)

    return org_dict["name"], identifier, org_dict.get("founding_date", None)


def membership_key_from_dict(membership):
    return membership["organization_id"], membership["role"], membership["start_date"]


def ownership_key_from_dict(ownership):
    return (ownership["organization_id"],)


def generic_key_from_dict(_dict: dict) -> Tuple[Any]:
    pass


class RemovesDiffLogic(ABC):
    def apply(self, diff_helper, etl):
        pass


class JsonOutRemovesDiffLogic(RemovesDiffLogic):
    def __init__(self, local_out_path):
        super().__init__()
        self.local_out_path = local_out_path

    def apply(self, diff_helper, etl):
        """Default removal logic: write json records to a file.

        :param diff_helper:
        :param etl:
        :return:
        """
        if etl.logger:
            etl.logger.info(
                "Storing {0} records to be removed into {1}".format(
                    diff_helper.removes["n"], self.local_out_path
                )
            )
        ETL(
            extractor=JsonArrayExtractor(diff_helper.removes["io"]),
            transformation=etl.transformation,
            loader=JsonLoader(
                "{0}/{1}_removes.json".format(self.local_out_path, etl.filename)
            ),
            logger=etl.logger,
        )()


class CloseItemRemovesDiffLogic(RemovesDiffLogic):
    def __init__(self, **kwargs):
        super().__init__()
        self.end_field = kwargs.pop("end_field", "end_date")
        self.end_date = kwargs.pop("end_date", None)
        self.end_reason = kwargs.pop("end_reason", None)

    def apply(self, diff_helper, etl):
        """Find and closes memberships in records marked for removal.
        Will modify the ``diff_helper.removes`` dictionary, accordingly, and
        feed it into the ``diff_helper.adds_and_updates`` dictionary.

        :param diff_helper:
        :param etl:
        :return:
        """
        if self.end_date is None:
            raise ETLException("end_date attribute needs to be not null")

        if etl.logger:
            etl.logger.info(
                "Applying removal logic to {0} records".format(diff_helper.removes["n"])
            )

        for item in diff_helper.removes["records"]:
            item[self.end_field] = self.end_date
            if self.end_reason:
                item["end_reason"] = self.end_reason

        # add modified removes records to adds_and_updates, to be processed later
        diff_helper.adds_and_updates["records"].extend(diff_helper.removes["records"])
        diff_helper.adds_and_updates["n"] += diff_helper.removes["n"]
        diff_helper.adds_and_updates["io"] = StringIO(
            json.dumps(diff_helper.adds_and_updates["records"])
        )


class CloseInnerMembershipsOwnershipsRemovesDiffLogic(RemovesDiffLogic):
    def __init__(self, **kwargs):
        super().__init__()
        self.end_field = kwargs.pop("end_field", "end_date")
        self.end_date = kwargs.pop("end_date", None)
        self.end_reason = kwargs.pop("end_reason", None)

    def apply(self, diff_helper, etl):
        """Find and closes memberships in records marked for removal.
        Will modify the ``diff_helper.removes`` dictionary, accordingly, and
        feed it into the ``diff_helper.adds_and_updates`` dictionary.

        :param diff_helper:
        :param etl:
        :return:
        """
        from datetime import timedelta

        if self.end_date is None:
            raise ETLException("end_date attribute needs to be not null")

        if etl.logger:
            etl.logger.info(
                "Applying removal logic to {0} records".format(diff_helper.removes["n"])
            )
        for rec in diff_helper.removes["records"]:
            # close memberships and ownerships (updating end_date and end_reason fields)
            for item in rec.get("memberships", []):
                # normal, old case
                item[self.end_field] = self.end_date

                # special case, overwrite if last start_date in org memberships
                # is lesser than 6 months ago
                org_id = item.get("organization_id", None)
                if org_id:
                    last_start_date = sorted(
                        (
                            m.get("start_date", "")
                            for r in diff_helper.adds_and_updates["records"]
                            for m in r.get("memberships", [])
                            if m.get("organization_id", None) == org_id
                        ),
                        reverse=True,
                    )
                    if len(last_start_date) > 0:
                        last_start_date = last_start_date[0]
                    else:
                        last_start_date = None
                    six_months_ago = (datetime.now() - timedelta(days=182)).strftime(
                        "%Y-%m-%d"
                    )

                    if last_start_date and last_start_date > six_months_ago:
                        item[self.end_field] = last_start_date

                if self.end_reason:
                    item["end_reason"] = self.end_reason

            for item in rec.get("ownerships", []):
                item[self.end_field] = self.end_date
                if self.end_reason:
                    item["end_reason"] = self.end_reason

        # add modified removes records to adds_and_updates, to be processed later
        diff_helper.adds_and_updates["records"].extend(diff_helper.removes["records"])
        diff_helper.adds_and_updates["n"] += diff_helper.removes["n"]
        diff_helper.adds_and_updates["io"] = StringIO(
            json.dumps(diff_helper.adds_and_updates["records"])
        )


class UpdatesDiffLogic(ABC):
    @abstractmethod
    def compute_diffs(self, olds, news):
        pass


class ShallowUpdatesDiffLogic(UpdatesDiffLogic):
    def __init__(self):
        super().__init__()

    def compute_diffs(self, olds, news):
        """Default logic used to compare intersecting keys.

        A shallow comparison is performed, without considering inner details

        This can and should be overridden in case deeper analysis is required.

        :param olds:
        :param news:
        :return: the list of values that differ
        """
        return list(dict(filter(lambda x: x[1] != olds[x[0]], news.items())).values())


class CheckInnerDetailsUpdatesDiffLogic(ShallowUpdatesDiffLogic):
    def __init__(
        self,
        person_key_getter,
        membership_key_getter=membership_key_from_dict,
        ownership_key_getter=ownership_key_from_dict,
    ):
        super().__init__()
        self.person_key_getter = person_key_getter
        self.membership_key_getter = membership_key_getter
        self.ownership_key_getter = ownership_key_getter

    def compute_diffs(self, olds, news):
        """Compute updates, checking inner details (memberships and ownerships),
        and adding closed ownerships and memberships to the records to be updated.

        Memberships and Ownerships are identified through the *_getter functions, returning a unique tuple from a dict

        :param olds:
        :param news:

        :return: the list of items to be updated
        """

        different_items = super().compute_diffs(olds, news)
        for new_item in different_items:
            old_item = olds[self.person_key_getter(new_item)]

            for m in old_item.get("memberships", []):
                if self.membership_key_getter(m) not in [
                    self.membership_key_getter(new_m)
                    for new_m in new_item.get("memberships", [])
                ]:
                    m["end_date"] = datetime.strftime(datetime.now(), "%Y-%m-%d")
                    m["end_reason"] = "record missing from source"
                    new_item.setdefault("memberships", []).append(m)

            for o in old_item.get("ownerships", []):
                if self.ownership_key_getter(o) not in [
                    self.ownership_key_getter(new_o)
                    for new_o in new_item.get("ownerships", [])
                ]:
                    o["end_date"] = datetime.strftime(datetime.now(), "%Y-%m-%d")
                    o["end_reason"] = "record missing from source"
                    new_item.setdefault("ownerships", []).append(o)

        return different_items
