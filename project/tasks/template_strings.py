ORG_GRUPPO_POLITICO_CAMERA_NAME = (
    'Gruppo parlamentare "{name}" alla Camera ({roman} legislatura)'
)
"""
Template string per gruppo parlamentare alla Camera.
"""
ORG_GRUPPO_POLITICO_SENATO_NAME = (
    'Gruppo parlamentare "{name}" al Senato ({roman} legislatura)'
)
"""
Template string per gruppo parlamentare al Senato.
"""

MEMBERSHIP_GRUPPO_POLITICO_LABEL = "{role} {org_name}"
