import unittest

from project.tasks.parsing.utils import carve_date_interval, \
    DateIntervalException, carve_date_intervals


class UtilsTestCase(unittest.TestCase):
    # def test_sort_overlapping_memberships_A(self):
    #     unsorted_json = [
    #         {
    #             "role": "Membro",
    #             "start_date": "2020-01-01",
    #             "end_date": "2020-12-31",
    #         },
    #         {
    #             "role": "Portavoce",
    #             "start_date": "2020-02-01",
    #             "end_date": "2020-02-28",
    #         },
    #         {
    #             "role": "Presidente",
    #             "start_date": "2020-06-01",
    #             "end_date": "2020-06-31",
    #         },
    #     ]
    #     sorted_json = [
    #         {
    #             "role": "Membro",
    #             "start_date": "2020-01-01",
    #             "end_date": "2020-02-01",
    #         },
    #         {
    #             "role": "Portavoce",
    #             "start_date": "2020-02-01",
    #             "end_date": "2020-02-28",
    #         },
    #         {
    #             "role": "Membro",
    #             "start_date": "2020-02-28",
    #             "end_date": "2020-06-01",
    #         },
    #         {
    #             "role": "Presidente",
    #             "start_date": "2020-06-01",
    #             "end_date": "2020-06-31",
    #         },
    #         {
    #             "role": "Membro",
    #             "start_date": "2020-06-31",
    #             "end_date": "2020-12-31",
    #         },
    #     ]
    #
    #     ret = sort_overlapping_memberships(unsorted_json)
    #
    #     self.assertEqual(ret, sorted_json)
    #
    # def test_sort_overlapping_memberships_B(self):
    #     unsorted_json = [
    #         {
    #             "role": "Membro",
    #             "organization_id": 1,
    #             "start_date": "2020-01-01",
    #             "end_date": "2020-12-31",
    #         },
    #         {
    #             "role": "Portavoce",
    #             "organization_id": 1,
    #             "start_date": "2020-02-01",
    #             "end_date": "2020-02-28",
    #         },
    #         {
    #             "role": "Presidente",
    #             "organization_id": 1,
    #             "start_date": "2020-06-01",
    #             "end_date": "2020-06-31",
    #         },
    #         {
    #             "role": "Membro",
    #             "organization_id": 2,
    #             "start_date": "2020-01-01",
    #             "end_date": "2020-06-01",
    #         },
    #         {
    #             "role": "Vicepresidente",
    #             "organization_id": 2,
    #             "start_date": "2020-03-01",
    #             "end_date": "2020-05-01",
    #         },
    #         {
    #             "role": "Membro",
    #             "organization_id": 3,
    #             "start_date": "2020-01-01",
    #             "end_date": None,
    #         },
    #     ]
    #     sorted_json = [
    #         {
    #             "role": "Membro",
    #             "organization_id": 1,
    #             "start_date": "2020-01-01",
    #             "end_date": "2020-02-01",
    #         },
    #         {
    #             "role": "Portavoce",
    #             "organization_id": 1,
    #             "start_date": "2020-02-01",
    #             "end_date": "2020-02-28",
    #         },
    #         {
    #             "role": "Membro",
    #             "organization_id": 1,
    #             "start_date": "2020-02-28",
    #             "end_date": "2020-06-01",
    #         },
    #         {
    #             "role": "Presidente",
    #             "organization_id": 1,
    #             "start_date": "2020-06-01",
    #             "end_date": "2020-06-31",
    #         },
    #         {
    #             "role": "Membro",
    #             "organization_id": 1,
    #             "start_date": "2020-06-31",
    #             "end_date": "2020-12-31",
    #         },
    #         {
    #             "role": "Membro",
    #             "organization_id": 2,
    #             "start_date": "2020-01-01",
    #             "end_date": "2020-03-01",
    #         },
    #         {
    #             "role": "Vicepresidente",
    #             "organization_id": 2,
    #             "start_date": "2020-03-01",
    #             "end_date": "2020-05-01",
    #         },
    #         {
    #             "role": "Membro",
    #             "organization_id": 2,
    #             "start_date": "2020-05-01",
    #             "end_date": "2020-06-01",
    #         },
    #         {
    #             "role": "Membro",
    #             "organization_id": 3,
    #             "start_date": "2020-01-01",
    #             "end_date": None,
    #         },
    #     ]
    #
    #     ret = sort_overlapping_memberships(
    #         unsorted_json, group_by_key="organization_id"
    #     )
    #
    #     self.assertEqual(ret, sorted_json)
    #
    # def test_merge_intervals_A(self):
    #     intervals = [
    #         {"name": "A", "start_date": 0, "end_date": 10},
    #         {"name": "B", "start_date": 5, "end_date": 10},
    #     ]
    #     merged = [
    #         {"name": "A", "start_date": 0, "end_date": 5},
    #         {"name": "B", "start_date": 5, "end_date": 10},
    #     ]
    #     ret = merge_intervals(*intervals)
    #     self.assertEqual(ret, merged)
    #
    # def test_merge_intervals_B(self):
    #     intervals = [
    #         {"name": "A", "start_date": 0, "end_date": 10},
    #         {"name": "B", "start_date": 5, "end_date": 6},
    #     ]
    #     merged = [
    #         {"name": "A", "start_date": 0, "end_date": 5},
    #         {"name": "B", "start_date": 5, "end_date": 6},
    #         {"name": "A", "start_date": 6, "end_date": 10},
    #     ]
    #     ret = merge_intervals(*intervals)
    #     self.assertEqual(ret, merged)
    #
    # def test_merge_intervals_C(self):
    #     intervals = [
    #         {"name": "A", "start_date": 0, "end_date": 10},
    #         {"name": "B", "start_date": 0, "end_date": 5},
    #     ]
    #     merged = [
    #         {"name": "B", "start_date": 0, "end_date": 5},
    #         {"name": "A", "start_date": 5, "end_date": 10},
    #     ]
    #     ret = merge_intervals(*intervals)
    #     self.assertEqual(ret, merged)
    #
    # def test_merge_intervals_D(self):
    #     # a     |--------|
    #     # b           |-------|
    #     # =     |-----|-------|
    #     intervals = [
    #         {"name": "A", "start_date": 0, "end_date": 10},
    #         {"name": "B", "start_date": 6, "end_date": 12},
    #     ]
    #     merged = [
    #         {"name": "A", "start_date": 0, "end_date": 6},
    #         {"name": "B", "start_date": 6, "end_date": 12},
    #     ]
    #     ret = merge_intervals(*intervals)
    #     self.assertEqual(ret, merged)
    #
    # def test_merge_intervals_E(self):
    #     # a     |-------|
    #     # b             |-------|
    #     # =     |-------|-------|
    #     intervals = [
    #         {"name": "A", "start_date": 0, "end_date": 10},
    #         {"name": "B", "start_date": 10, "end_date": 20},
    #     ]
    #     merged = [
    #         {"name": "A", "start_date": 0, "end_date": 10},
    #         {"name": "B", "start_date": 10, "end_date": 20},
    #     ]
    #     ret = merge_intervals(*intervals)
    #     self.assertEqual(ret, merged)
    #
    # def test_merge_intervals_F(self):
    #     # a     |---------------- - - -
    #     # b             |-------|
    #     # =     |-------|-------| - - -
    #     intervals = [
    #         {"name": "A", "start_date": 0, "end_date": None},
    #         {"name": "B", "start_date": 10, "end_date": 20},
    #     ]
    #     merged = [
    #         {"name": "A", "start_date": 0, "end_date": 10},
    #         {"name": "B", "start_date": 10, "end_date": 20},
    #         {"name": "A", "start_date": 20, "end_date": None},
    #     ]
    #     ret = merge_intervals(*intervals)
    #     self.assertEqual(ret, merged)
    #
    # def test_merge_intervals_G(self):
    #     # a     |---------------- - - -
    #     # b             |-------- - - -
    #     # =     |-------|-------- - - -
    #     intervals = [
    #         {"name": "A", "start_date": 0, "end_date": None},
    #         {"name": "B", "start_date": 10, "end_date": None},
    #     ]
    #     merged = [
    #         {"name": "A", "start_date": 0, "end_date": 10},
    #         {"name": "B", "start_date": 10, "end_date": None},
    #     ]
    #     ret = merge_intervals(*intervals)
    #     self.assertEqual(ret, merged)

    def test_carve_internal_interval(self):
        original_interval = ("2020-01-01", "2020-12-31")
        interval = ("2020-04-01", "2020-04-30")
        intervals = carve_date_interval(
            original_interval, interval
        )
        self.assertEqual(len(intervals), 2)
        self.assertEqual(intervals[0][0], original_interval[0])
        self.assertEqual(intervals[0][1], interval[0])
        self.assertEqual(intervals[1][0], interval[1])
        self.assertEqual(intervals[1][1], original_interval[1])

    def test_carve_internal_interval_with_nones(self):
        original_interval = (None, None)
        interval = ("2020-04-01", "2020-04-30")
        intervals = carve_date_interval(
            original_interval, interval
        )
        self.assertEqual(len(intervals), 2)
        self.assertEqual(intervals[0][0], original_interval[0])
        self.assertEqual(intervals[0][1], interval[0])
        self.assertEqual(intervals[1][0], interval[1])
        self.assertEqual(intervals[1][1], original_interval[1])

    def test_carve_outer_interval_at_start(self):
        original_interval = ("2020-01-01", "2020-12-31")
        interval = ("2019-06-01", "2020-04-30")
        intervals = carve_date_interval(
            original_interval, interval
        )
        self.assertEqual(len(intervals), 1)
        self.assertEqual(intervals[0][0], interval[1])
        self.assertEqual(intervals[0][1], original_interval[1])

    def test_carve_outer_interval_at_end(self):
        original_interval = ("2020-01-01", "2020-12-31")
        interval = ("2020-06-01", "2021-06-30")
        intervals = carve_date_interval(
            original_interval, interval
        )
        self.assertEqual(len(intervals), 1)
        self.assertEqual(intervals[0][0], original_interval[0])
        self.assertEqual(intervals[0][1], interval[0])

    def test_carve_larger_interval(self):
        original_interval = ("2020-01-01", "2020-12-31")
        interval = ("2019-06-01", "2021-06-30")
        intervals = carve_date_interval(
            original_interval, interval
        )
        self.assertEqual(len(intervals), 0)

    def test_carve_wrong_interval(self):
        original_interval = ("2020-01-01", "2020-12-31")
        interval = ("2020-04-30", "2020-04-01")
        with self.assertRaises(DateIntervalException):
            _ = carve_date_interval(
                original_interval, interval
            )

    def test_carve_external_interval(self):
        original_interval = ("2020-01-01", "2020-12-31")
        interval = ("2021-06-30", "2021-12-31")
        intervals = carve_date_interval(
            original_interval, interval
        )
        self.assertEqual(len(intervals), 1)
        self.assertEqual(intervals[0][0], original_interval[0])
        self.assertEqual(intervals[0][1], original_interval[1])

    def test_carve_multiple_intervals(self):
        original_intervals = [("2020-01-01", "2020-12-31")]
        intervals = [
            ("2020-02-01", "2020-03-11"),
            ("2020-05-01", "2020-05-25"),
        ]
        resulting_intervals = carve_date_intervals(
            original_intervals, intervals
        )
        self.assertEqual(len(resulting_intervals), 3)
        self.assertEqual(resulting_intervals[0][0], original_intervals[0][0])
        self.assertEqual(resulting_intervals[0][1], intervals[0][0])
        self.assertEqual(resulting_intervals[1][0], intervals[0][1])
        self.assertEqual(resulting_intervals[1][1], intervals[1][0])
        self.assertEqual(resulting_intervals[2][0], intervals[1][1])
        self.assertEqual(resulting_intervals[2][1], original_intervals[0][1])
