# -*- coding: utf-8 -*-

"""
Implements tests specific to the CSVDiffCompositeETL class.
"""
import logging
import os
import shutil

from ooetl import DummyTransformation
from ooetl.loaders import CSVLoader
from ooetl.tests.django import ETLTest

from project.tasks.etl import ETLException
from project.tasks.etl.composites import CSVDiffCompositeETL
from project.tasks.etl.extractors import UACSVExtractor


class CSVDiffCompositeTest(ETLTest):

    new_csv_content = (
        "area_istat_com_code,area,area_id,institution_id,tot\r\n"
        '3045,"Cavaglio, d\'Agogna",425,50746,4.0\r\n'
        "80060,Platì,7073,42138,6.0\r\n"
        "57071,Turania,5156,39738,4.0\r\n"
        "11322,Pescocostanzo,1341,46341,5.0\r\n"
    )

    old_csv_content = (
        "area_istat_com_code,area,area_id,institution_id,tot\r\n"
        "43002,Apiro,4898,45900,6.0\r\n"
        "17012,Barghe,2025,36158,6.0\r\n"
        '3045,"Cavaglio d\'Agogna",425,50746,6.0\r\n'
        "80060,Platì,7073,42138,6.0\r\n"
        "57071,Turania,5156,39738,4.0\r\n"
    )

    test_cache_path = "resources/data/cachetest"
    test_filename = "test.csv"

    @classmethod
    def cleanUpCache(cls, rmdir=False):
        """Clean up cache folder content, may also remove dir

        Invoked after each test, to remove test files (sort of rollback, for files used in tests)

        If invoked at the end of the tests, then the folder is also removed.

        :param rmdir: whether to remove the folder or not
        :return:
        """
        folder = cls.test_cache_path

        # the folder path must contain 'cachetest', for security reasons
        if "cachetest" in folder and os.path.isdir(folder):
            for f in os.listdir(folder):
                f_path = os.path.join(folder, f)
                if os.path.isfile(f_path):
                    os.unlink(f_path)
            if rmdir:
                shutil.rmtree(folder)

    @classmethod
    def setUpClass(cls):
        super(CSVDiffCompositeTest, cls).setUpClass()
        if not os.path.isdir(cls.test_cache_path):
            os.mkdir(cls.test_cache_path)

    @classmethod
    def tearDownClass(cls):
        cls.cleanUpCache(rmdir=True)
        super(CSVDiffCompositeTest, cls).tearDownClass()

    def test_composite_etl_method_raises_exception(self):
        """Test scenario where cached file does not exist, and it's created"""
        with self.assertRaises(ETLException) as exc_ctx:
            CSVDiffCompositeETL(
                extractor=UACSVExtractor(
                    url="https://storage.opdm.io/test.csv",  # mocked, not a real URL
                    sep=",",
                    na_values=["", "-"],
                    keep_default_na=False,
                ),
                transformation=DummyTransformation(),
                loader=CSVLoader(self.test_cache_path),
                local_cache_path=self.test_cache_path,
                local_out_path=self.test_cache_path,
                filename=self.test_filename,
                unique_idx_cols=(0,),
                log_level=logging.INFO,
            ).etl()
        self.assertEqual(
            "Not available in Composite ETL classes",
            exc_ctx.exception.args[0]
        )

    def test_composite_etl_extract_method_raises_exception(self):
        """Test scenario where cached file does not exist, and it's created"""
        with self.assertRaises(ETLException) as exc_ctx:
            CSVDiffCompositeETL(
                extractor=UACSVExtractor(
                    url="https://storage.opdm.io/test.csv",  # mocked, not a real URL
                    sep=",",
                    na_values=["", "-"],
                    keep_default_na=False,
                ),
                transformation=DummyTransformation(),
                loader=CSVLoader(self.test_cache_path),
                local_cache_path=self.test_cache_path,
                local_out_path=self.test_cache_path,
                filename=self.test_filename,
                unique_idx_cols=(0,),
                log_level=logging.INFO,
            ).extract()
        self.assertEqual(
            "Not available in Composite ETL classes",
            exc_ctx.exception.args[0]
        )

    def test_composite_etl_transform_method_raises_exception(self):
        """Test scenario where cached file does not exist, and it's created"""
        with self.assertRaises(ETLException) as exc_ctx:
            CSVDiffCompositeETL(
                extractor=UACSVExtractor(
                    url="https://storage.opdm.io/test.csv",  # mocked, not a real URL
                    sep=",",
                    na_values=["", "-"],
                    keep_default_na=False,
                ),
                transformation=DummyTransformation(),
                loader=CSVLoader(self.test_cache_path),
                local_cache_path=self.test_cache_path,
                local_out_path=self.test_cache_path,
                filename=self.test_filename,
                unique_idx_cols=(0,),
                log_level=logging.INFO,
            ).transform()
        self.assertEqual(
            "Not available in Composite ETL classes",
            exc_ctx.exception.args[0]
        )

    def test_composite_etl_load_method_raises_exception(self):
        """Test scenario where cached file does not exist, and it's created"""
        with self.assertRaises(ETLException) as exc_ctx:
            CSVDiffCompositeETL(
                extractor=UACSVExtractor(
                    url="https://storage.opdm.io/test.csv",  # mocked, not a real URL
                    sep=",",
                    na_values=["", "-"],
                    keep_default_na=False,
                ),
                transformation=DummyTransformation(),
                loader=CSVLoader(self.test_cache_path),
                local_cache_path=self.test_cache_path,
                local_out_path=self.test_cache_path,
                filename=self.test_filename,
                unique_idx_cols=(0,),
                log_level=logging.INFO,
            ).load()
        self.assertEqual(
            "Not available in Composite ETL classes",
            exc_ctx.exception.args[0]
        )

    def test_no_old_csv(self):
        """Test scenario where cached file does not exist, and it's created"""

        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = self.new_csv_content.encode("utf8")

        CSVDiffCompositeETL(
            extractor=UACSVExtractor(
                url="https://storage.opdm.io/test.csv",  # mocked, not a real URL
                sep=",",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=DummyTransformation(),
            loader=CSVLoader(self.test_cache_path),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            unique_idx_cols=(0,),
            log_level=logging.INFO,
        )()

        # io streams contain the correct number of lines
        with open(
            "{0}/dati.csv".format(self.test_cache_path), "r", encoding="utf8"
        ) as f:
            lines = f.readlines()

        self.assertEqual(len(lines), len(self.new_csv_content.split("\r\n")) - 1)

        self.cleanUpCache()

    def test_no_changes(self):
        """Test scenario where new content and cached file are identical"""
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = self.new_csv_content.encode("utf8")

        # create old_csv file in cache with new_content
        with open("{0}/test.csv".format(self.test_cache_path), "wb") as f:
            f.write(self.new_csv_content.encode("utf8"))

        CSVDiffCompositeETL(
            extractor=UACSVExtractor(
                url="https://storage.opdm.io/test.csv",  # mocked, not a real URL
                sep=",",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=DummyTransformation(),
            loader=CSVLoader(self.test_cache_path),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            unique_idx_cols=(0,),
            log_level=logging.INFO,
        )()

        # no dati file was produced
        self.assertFalse(os.path.isfile("{0}/dati.csv".format(self.test_cache_path)))
        self.assertFalse(
            os.path.isfile("{0}/test_removes.csv".format(self.test_cache_path))
        )

        self.cleanUpCache()

    def test_changes(self):
        """Test scenario where new content and cached file are different"""
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = self.new_csv_content.encode("utf8")

        # create old_csv file in cache with new_content
        with open("{0}/test.csv".format(self.test_cache_path), "wb") as f:
            f.write(self.old_csv_content.encode("utf8"))

        CSVDiffCompositeETL(
            extractor=UACSVExtractor(
                url="https://storage.opdm.io/test.csv",  # mocked, not a real URL
                sep=",",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=DummyTransformation(),
            loader=CSVLoader(self.test_cache_path),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            unique_idx_cols=(0,),
            log_level=logging.INFO,
        )()

        # a dati file was produced
        # a removes file was produced
        self.assertTrue(os.path.isfile("{0}/dati.csv".format(self.test_cache_path)))
        self.assertTrue(
            os.path.isfile("{0}/test_removes.csv".format(self.test_cache_path))
        )

        # produced files contain the correct number of lines
        with open(
            "{0}/dati.csv".format(self.test_cache_path), "r", encoding="utf8"
        ) as f:
            lines = f.readlines()
        self.assertEqual(len(lines) - 1, 2)

        with open(
            "{0}/test_removes.csv".format(self.test_cache_path), "r", encoding="utf8"
        ) as f:
            lines = f.readlines()
        self.assertEqual(len(lines) - 1, 2)

        self.cleanUpCache()

    def test_clear_cache(self):
        """Test scenario where new content and cached file are identical"""
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = self.new_csv_content.encode("utf8")

        # create old_csv file in cache with new_content
        with open("{0}/test.csv".format(self.test_cache_path), "wb") as f:
            f.write(self.new_csv_content.encode("utf8"))

        CSVDiffCompositeETL(
            extractor=UACSVExtractor(
                url="https://storage.opdm.io/test.csv",  # mocked, not a real URL
                sep=",",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=DummyTransformation(),
            loader=CSVLoader(self.test_cache_path),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            clear_cache=True,
            unique_idx_cols=(0,),
            log_level=logging.INFO,
        )()

        # io streams contain the correct number of lines
        with open(
            "{0}/dati.csv".format(self.test_cache_path), "r", encoding="utf8"
        ) as f:
            lines = f.readlines()

        self.assertEqual(len(lines), len(self.new_csv_content.split("\r\n")) - 1)

        self.cleanUpCache()
