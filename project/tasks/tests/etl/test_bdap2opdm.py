# -*- coding: utf-8 -*-

"""
Implements tests specific to the etl classes that import from BDAP files into OPDM.
"""
import io
import logging
from unittest.mock import patch

from django.test import TestCase as djTestCase
from ooetl import ETL
from ooetl.extractors import CSVExtractor
import pandas as pd
from popolo.models import Ownership
from popolo.tests.factories import (
    OrganizationFactory,
)
import requests

from project.tasks.etl.loaders import DummyLoader
from project.tasks.etl.loaders.organizations import PopoloOrgOwnershipLoader
from project.tasks.etl.transformations import BdapOwnership2OPDMTransformation

bdap_source_url = "https://bdap-opendata.mef.gov.it/export/csv/Anagrafe-Enti---Partecipazioni-Ente.csv"


class BDAPETLTest(djTestCase):
    @classmethod
    def setUpClass(cls):
        super(BDAPETLTest, cls).setUpClass()
        setattr(cls, 'mock_get_patcher', patch('requests.get'))
        cls.mock_get = getattr(cls, 'mock_get_patcher').start()

    @classmethod
    def tearDownClass(cls):
        getattr(cls, 'mock_get_patcher').stop()
        super(BDAPETLTest, cls).tearDownClass()


class BDAP2OpdmETLTest(BDAPETLTest):
    def test_etl_dummy_loader(self):
        """Test DummyLoader class, tests extraction from CSV into pandas DataFrame and transformation
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"Id_Ente_Partecipante;Id_Ente_Partecipato;Data_Rilevazione;"
            b"Data_Rilevazione_Fine_Relaz;Denom_Ente_Partecipante;"
            b"CF_Ente_Partecipante;Denom_Ente_Partecipato;CF_Ente_Partecipato;"
            b"Quota_Diretta;Quota_Indiretta;Componente_Grafo_Partecipaz;\r\n"
            b"111142930451558302;123046528777613102;2014-12-31 00:00:00;;COMUNE DI VERMIGLIO;"
            b"00343510228;\"TRENTINO TRASPORTI S.P.A., IN SIGLA \"\"T.T. S.P.A.\"\"\";01807370224;0.00607;;2;\r\n"
            b"111142930451558302;125643113465280202;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;TRENTINO TRASPORTI ESERCIZIO S.P.A.;02084830229;0.019;;2;\r\n"
            b"111142930451558302;151446528778286902;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;DOLOMITI ENERGIA SOCIETA' PER AZIONI;01812630224;0.23;;2;\r\n"
        )

        resp = requests.get(bdap_source_url)
        source_io = io.BytesIO(resp.content)

        etl = ETL(
            extractor=CSVExtractor(
                source_io,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=DummyLoader(),
            transformation=BdapOwnership2OPDMTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 3)

    def test_etl_create_ownerships(self):
        """Tests PopoloOrgOwnershipLoader class: three new ownerships are created
        records with QUOTA_DIRETTA null are not created
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"Id_Ente_Partecipante;Id_Ente_Partecipato;Data_Rilevazione;"
            b"Data_Rilevazione_Fine_Relaz;Denom_Ente_Partecipante;"
            b"CF_Ente_Partecipante;Denom_Ente_Partecipato;CF_Ente_Partecipato;"
            b"Quota_Diretta;Quota_Indiretta;Componente_Grafo_Partecipaz;\r\n"
            b"111142930451558302;123046528777613102;2014-12-31 00:00:00;;COMUNE DI VERMIGLIO;"
            b"00343510228;\"TRENTINO TRASPORTI S.P.A., IN SIGLA \"\"T.T. S.P.A.\"\"\";01807370224;0.00607;;2;\r\n"
            b"111142930451558302;125643113465280202;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;TRENTINO TRASPORTI ESERCIZIO S.P.A.;02084830229;0.019;;2;\r\n"
            b"111142930451558302;151446528778286902;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;DOLOMITI ENERGIA SOCIETA' PER AZIONI;01812630224;0.23;;2;\r\n"
            b"111142930549694901;154146528905365002;2014-12-31 00:00:00;2015-12-31;"
            b"COMUNE DI RESANA;81000610261;ASCO TLC S.P.A.;03553690268;;0.455;2;\r\n"
            b"111142930549694901;154246528684472901;2014-12-31 00:00:00;2015-12-31;"
            b"COMUNE DI RESANA;81000610261;SEVEN CENTER S.R.L.;00344730288;;42.5;2;\r\n"
        )

        owning_org = OrganizationFactory.create(
            name="COMUNE DI VERMIGLIO", identifier="00343510228"
        )
        owning_org.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="111142930451558302")

        owned_org_a = OrganizationFactory.create(
            name="TRENTINO TRASPORTI S.P.A., IN SIGLA \"\"T.T. S.P.A.\"\"", identifier="01807370224"
        )
        owned_org_a.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="123046528777613102")

        owned_org_b = OrganizationFactory.create(
            name="TRENTINO TRASPORTI ESERCIZIO S.P.A.", identifier="02084830229"
        )
        owned_org_b.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="125643113465280202")

        owned_org_c = OrganizationFactory.create(
            name="DOLOMITI ENERGIA SOCIETA' PER AZIONI", identifier="01812630224"
        )
        owned_org_c.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="151446528778286902")

        resp = requests.get(bdap_source_url)
        source_io = io.BytesIO(resp.content)

        ETL(
            extractor=CSVExtractor(
                source_io,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=PopoloOrgOwnershipLoader(
                update_strategy='overwrite',
                lookup_strategy='mixed',
                identifier_scheme='CODICE_ENTE_BDAP',
            ),
            transformation=BdapOwnership2OPDMTransformation(),
            log_level=logging.INFO,
        ).etl()
        n_ownerships = Ownership.objects.count()
        self.assertEqual(n_ownerships, 3)

        o = Ownership.objects.first()
        self.assertEqual(o.owned_organization, owned_org_a)
        self.assertEqual(o.owner_organization, owning_org)

    def test_etl_update_ownerships_overwrite(self):
        """Tests PopoloOrgOwnershipLoader class: existing ownerships are updated, with default update_strategy
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"Id_Ente_Partecipante;Id_Ente_Partecipato;Data_Rilevazione;"
            b"Data_Rilevazione_Fine_Relaz;Denom_Ente_Partecipante;"
            b"CF_Ente_Partecipante;Denom_Ente_Partecipato;CF_Ente_Partecipato;"
            b"Quota_Diretta;Quota_Indiretta;Componente_Grafo_Partecipaz;\r\n"
            b"111142930451558302;123046528777613102;2016-12-31 00:00:00;;COMUNE DI VERMIGLIO;"
            b"00343510228;\"TRENTINO TRASPORTI S.P.A., IN SIGLA \"\"T.T. S.P.A.\"\"\";01807370224;0.001;;2;\r\n"
            b"111142930451558302;125643113465280202;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;TRENTINO TRASPORTI ESERCIZIO S.P.A.;02084830229;0.019;;2;\r\n"
            b"111142930451558302;151446528778286902;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;DOLOMITI ENERGIA SOCIETA' PER AZIONI;01812630224;0.23;;2;\r\n"
        )

        owning_org = OrganizationFactory.create(
            name="COMUNE DI VERMIGLIO", identifier="00343510228"
        )
        owning_org.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="111142930451558302")

        owned_org_a = OrganizationFactory.create(
            name="TRENTINO TRASPORTI S.P.A., IN SIGLA \"\"T.T. S.P.A.\"\"", identifier="01807370224"
        )
        owned_org_a.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="123046528777613102")

        owned_org_b = OrganizationFactory.create(
            name="TRENTINO TRASPORTI ESERCIZIO S.P.A.", identifier="02084830229"
        )
        owned_org_b.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="125643113465280202")

        owned_org_c = OrganizationFactory.create(
            name="DOLOMITI ENERGIA SOCIETA' PER AZIONI", identifier="01812630224"
        )
        owned_org_c.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="151446528778286902")

        old_start_date = '2014'
        old_percentage = 0.1
        Ownership.objects.create(
            owned_organization_id=owned_org_a.id,
            owner_organization_id=owning_org.id,
            start_date=old_start_date,
            percentage=old_percentage
        )

        resp = requests.get(bdap_source_url)
        source_io = io.BytesIO(resp.content)

        ETL(
            extractor=CSVExtractor(
                source_io,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=PopoloOrgOwnershipLoader(
                update_strategy='overwrite',
                lookup_strategy='mixed',
                identifier_scheme='CODICE_ENTE_BDAP',
            ),
            transformation=BdapOwnership2OPDMTransformation(),
            log_level=logging.INFO,
        ).etl()
        n_ownerships = Ownership.objects.count()
        self.assertEqual(n_ownerships, 3)

        o = Ownership.objects.get(
            owned_organization=owned_org_a, owner_organization=owning_org, percentage=old_percentage
        )
        self.assertEqual(o.percentage, old_percentage, f"{o.percentage}, {old_percentage}")
        self.assertNotEqual(o.start_date, old_start_date)

    def test_etl_update_ownerships_keep_old(self):
        """Tests PopoloOrgOwnershipLoader class: existing ownerships are updated, only None values are written
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"Id_Ente_Partecipante;Id_Ente_Partecipato;Data_Rilevazione;"
            b"Data_Rilevazione_Fine_Relaz;Denom_Ente_Partecipante;"
            b"CF_Ente_Partecipante;Denom_Ente_Partecipato;CF_Ente_Partecipato;"
            b"Quota_Diretta;Quota_Indiretta;Componente_Grafo_Partecipaz;\r\n"
            b"111142930451558302;123046528777613102;2014-12-31 00:00:00;;COMUNE DI VERMIGLIO;"
            b"00343510228;\"TRENTINO TRASPORTI S.P.A., IN SIGLA \"\"T.T. S.P.A.\"\"\";01807370224;0.00607;;2;\r\n"
            b"111142930451558302;125643113465280202;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;TRENTINO TRASPORTI ESERCIZIO S.P.A.;02084830229;0.003;;2;\r\n"
            b"111142930451558302;151446528778286902;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;DOLOMITI ENERGIA SOCIETA' PER AZIONI;01812630224;0.23;;2;\r\n"
        )

        owning_org = OrganizationFactory.create(
            name="COMUNE DI VERMIGLIO", identifier="00343510228"
        )
        owning_org.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="111142930451558302")

        owned_org_a = OrganizationFactory.create(
            name="TRENTINO TRASPORTI S.P.A., IN SIGLA \"\"T.T. S.P.A.\"\"", identifier="01807370224"
        )
        owned_org_a.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="123046528777613102")

        owned_org_b = OrganizationFactory.create(
            name="TRENTINO TRASPORTI ESERCIZIO S.P.A.", identifier="02084830229"
        )
        owned_org_b.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="125643113465280202")

        owned_org_c = OrganizationFactory.create(
            name="DOLOMITI ENERGIA SOCIETA' PER AZIONI", identifier="01812630224"
        )
        owned_org_c.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="151446528778286902")

        old_start_date = '2014'
        old_percentage = 0.3
        Ownership.objects.create(
            owned_organization_id=owned_org_b.id,
            owner_organization_id=owning_org.id,
            start_date=old_start_date,
            end_date=None,
            percentage=old_percentage
        )

        resp = requests.get(bdap_source_url)
        source_io = io.BytesIO(resp.content)

        ETL(
            extractor=CSVExtractor(
                source_io,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=PopoloOrgOwnershipLoader(
                update_strategy='keep_old',
                lookup_strategy='mixed',
                identifier_scheme='CODICE_ENTE_BDAP',
            ),
            transformation=BdapOwnership2OPDMTransformation(),
            log_level=logging.INFO,
        ).etl()
        n_ownerships = Ownership.objects.count()
        self.assertEqual(n_ownerships, 3)

        o = Ownership.objects.get(
            owned_organization=owned_org_b, owner_organization=owning_org, percentage=old_percentage
        )
        self.assertEqual(o.percentage, old_percentage)
        self.assertEqual(o.start_date, old_start_date)
        self.assertNotEqual(o.end_date, None)

    def test_etl_create_ownerships_missing_org(self):
        """Tests PopoloOrgOwnershipLoader class: ownerships are created, errors are handled
        Mixed strategy as a backup works,
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"Id_Ente_Partecipante;Id_Ente_Partecipato;Data_Rilevazione;"
            b"Data_Rilevazione_Fine_Relaz;Denom_Ente_Partecipante;"
            b"CF_Ente_Partecipante;Denom_Ente_Partecipato;CF_Ente_Partecipato;"
            b"Quota_Diretta;Quota_Indiretta;Componente_Grafo_Partecipaz;\r\n"
            b"111142930451558302;123046528777613102;2014-12-31 00:00:00;;COMUNE DI VERMIGLIO;"
            b"00343510228;\"TRENTINO TRASPORTI S.P.A., IN SIGLA \"\"T.T. S.P.A.\"\"\";01807370224;0.00607;;2;\r\n"
            b"111142930451558302;125643113465280202;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;TRENTINO TRASPORTI ESERCIZIO S.P.A.;02084830229;0.019;;2;\r\n"
            b"111142930451558302;151446528778286902;2014-12-31 00:00:00;2015-12-31;COMUNE DI VERMIGLIO;"
            b"00343510228;DOLOMITI ENERGIA SOCIETA' PER AZIONI;01812630224;0.23;;2;\r\n"
        )

        owning_org = OrganizationFactory.create(
            name="COMUNE DI VERMIGLIO", identifier="00343510228"
        )
        owning_org.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="111142930451558302")
        owning_org.add_identifier(scheme="CF", identifier="00343510228")

        owned_org_a = OrganizationFactory.create(
            name="TRENTINO TRASPORTI S.P.A., IN SIGLA \"T.T. S.P.A.\"", identifier="01807370224"
        )
        owned_org_a.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="123046528777613110")
        owned_org_a.add_identifier(scheme="CF", identifier="01807370224")

        owned_org_b = OrganizationFactory.create(
            name="TRENTINO TRASPORTI ESERCIZIO S.P.A.", identifier="02084830229"
        )
        owned_org_b.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="125643113465280202")
        owned_org_b.add_identifier(scheme="CF", identifier="02084830229")

        owned_org_c = OrganizationFactory.create(
            name="DOLOMITI ENERGIA SOCIETA' PER AZIONI", identifier="01812630999"
        )
        owned_org_c.add_identifier(scheme="CODICE_ENTE_BDAP", identifier="151446528778286999")
        owned_org_c.add_identifier(scheme="CF", identifier="01812630999")

        resp = requests.get(bdap_source_url)
        source_io = io.BytesIO(resp.content)

        ETL(
            extractor=CSVExtractor(
                source_io,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=PopoloOrgOwnershipLoader(
                update_strategy='keep_old',
                lookup_strategy='mixed',
                identifier_scheme='CODICE_ENTE_BDAP',
            ),
            transformation=BdapOwnership2OPDMTransformation(),
            log_level=logging.INFO,
        ).etl()
        n_ownerships = Ownership.objects.count()
        self.assertEqual(n_ownerships, 3)
