# -*- coding: utf-8 -*-

"""
Implements tests specific to the etl classes.
"""
import json
import logging

from ooetl import ETL
import pandas as pd
from popolo.models import Person, Membership
from popolo.tests.factories import (
    OrganizationFactory,
    RoleTypeFactory,
    AreaFactory,
    ClassificationFactory,
    PersonFactory,
    PostFactory,
)

from project.tasks.etl.extractors import UACSVExtractor
from project.tasks.etl.loaders import DummyLoader
from project.tasks.etl.loaders.persons import PopoloPersonMembershipLoader
from project.tasks.etl.transformations.minint2opdm import (
    HistMinintReg2OpdmTransformation,
    HistMinintProv2OpdmTransformation,
    HistMinintMetro2OpdmTransformation,
    HistMinintCom2OpdmTransformation,
)
from project.tasks.tests.etl import SolrETLTest


class HistMinint2OpdmETLTest(SolrETLTest):
    def test_etl_dummy_loader_pd(self):
        """Test DummyLoader class
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"CODICE_REGIONE;DESCRIZIONE_REGIONE;ISTAT_CODICE_REGIONE;ANNO_CENSIMENTO;POPOLAZIONE_CENSITA;"
            b"DESCRIZIONE_TEMPO_GESTIONE;DATA_ELEZIONE;DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;"
            b"ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;NOME;SESSO;DATA_NASCITA;SEDE_NASCITA;"
            b"LIVELLO_CARICA;DESCRIZIONE_CARICA;DATA_NOMINA;PARTITO_LISTA_COALIZIONE;DATA_CESSAZIONE;"
            b"TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;01;2011;4363916;ORDINARIA;25/05/2014;;51;11;ON;CHIAMPARINO;SERGIO;M;01/09/1948;"
            b"MONCALIERI (TO);10;Presidente della regione;25/05/2014;CHIAMPARINO PRESIDENTE;;LAUREA;"
            b"Pensionati e persone ritirate dal lavoro\r\n"
        )

        area = AreaFactory.create(name="Lombardia")
        area.add_identifier(scheme="ISTAT_CODE", identifier="01")
        reg = OrganizationFactory(
            name="Regione Lombardia", classification="Regione", area=area
        )
        OrganizationFactory.create(
            name="Giunta regionale della Lombardia",
            classification="Giunta regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta regionale")
        RoleTypeFactory.create(
            label="Presidente di regione", priority=1, classification=classification
        )

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=DummyLoader(),
            transformation=HistMinintReg2OpdmTransformation(year="2017"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)

    def test_etl_minint_reg(self):
        """Test import from MinInt, yearly summary, regione context
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"CODICE_REGIONE;DESCRIZIONE_REGIONE;ISTAT_CODICE_REGIONE;ANNO_CENSIMENTO;POPOLAZIONE_CENSITA;"
            b"DESCRIZIONE_TEMPO_GESTIONE;DATA_ELEZIONE;DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;"
            b"ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;NOME;SESSO;DATA_NASCITA;SEDE_NASCITA;"
            b"LIVELLO_CARICA;DESCRIZIONE_CARICA;DATA_NOMINA;PARTITO_LISTA_COALIZIONE;DATA_CESSAZIONE;"
            b"TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;01;2011;4363916;ORDINARIA;25/05/2014;;51;11;ON;CHIAMPARINO;SERGIO;M;01/09/1948;"
            b"MONCALIERI (TO);10;Presidente della regione;25/05/2014;CHIAMPARINO PRESIDENTE;;LAUREA;"
            b"Pensionati e persone ritirate dal lavoro\r\n"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": '(family_name:("chiamparino") AND given_name:("sergio") '
                        'AND birth_date_s:("19480901") '
                        'AND birth_location:("moncalieri (to)"))',
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Piemonte")
        area.add_identifier(scheme="ISTAT_CODE_REG", identifier="01")
        reg = OrganizationFactory(
            name="Regione Piemonte", classification="Regione", area=area
        )
        OrganizationFactory.create(
            name="Giunta regionale del Piemonte",
            classification="Giunta regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta regionale")
        RoleTypeFactory.create(
            label="Presidente di regione", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="reg",
            year="2017",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_regioni31122017_0.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintReg2OpdmTransformation(year="2017"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        p = Person.objects.first()
        self.assertEqual(p.family_name, "Chiamparino")
        self.assertEqual(
            p.original_profession.name, "Pensionati e persone ritirate dal lavoro"
        )
        self.assertEqual(p.original_education_level.name, "LAUREA")
        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Presidente della Regione Piemonte")
        self.assertEqual(m.role, "Presidente di regione")
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_reg_existing(self):
        """Test import from MinInt, yearly summary, regione context, existing person and membership
        """

        # create person with membership
        person = PersonFactory.create(
            family_name="Baccega",
            given_name="Mauro",
            gender="M",
            birth_date="1955-08-15",
            birth_location="Aosta (AO)",
        )

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"CODICE_REGIONE;DESCRIZIONE_REGIONE;ISTAT_CODICE_REGIONE;ANNO_CENSIMENTO;POPOLAZIONE_CENSITA;"
            b"DESCRIZIONE_TEMPO_GESTIONE;DATA_ELEZIONE;DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;"
            b"ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;NOME;SESSO;DATA_NASCITA;SEDE_NASCITA;"
            b"LIVELLO_CARICA;DESCRIZIONE_CARICA;DATA_NOMINA;PARTITO_LISTA_COALIZIONE;DATA_CESSAZIONE;"
            b"TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"02;VALLE D'AOSTA;02;2011;126806;ORDINARIA;26/05/2013;;35;6;;BACCEGA;MAURO;M;15/08/1955;"
            b"AOSTA (AO);120;Consigliere;01/07/2013;ALTRI;;LICENZA DI SCUOLA MEDIA INF. O TITOLI EQUIPOLLENTI;"
            b"PROFESSIONI NON ALTROVE CLASSIFICABILI\r\n"
        )

        # mock solr select responses (found exact match)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 1,
                    "start": 0,
                    "docs": [
                        {
                            "id": "popolo.person.{0}".format(person.id),
                            "django_ct": "popolo.person",
                            "django_id": "{0}".format(person.id),
                            "family_name": "baccega",
                            "given_name": "mauro",
                            "birth_date": "1955-08-15T00:00:00Z",
                            "birth_date_s": "19550815",
                            "birth_location": "aosta (ao)",
                            "birth_location_area": 1200,
                            "OP_ID_s": "16431",
                            "CF_s": "BCCMRA55M15A326X",
                            "score": 0.0,
                        }
                    ],
                },
            }
        )
        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Valle d'Aosta/Vallée d'Aoste")
        area.add_identifier(scheme="ISTAT_CODE_REG", identifier="02")
        reg = OrganizationFactory(
            name="REGIONE AUTONOMA VALLE D'AOSTA", classification="Regione", area=area
        )
        org = OrganizationFactory.create(
            name="CONSIGLIO REGIONALE DELLA VALLE D'AOSTA",
            classification="Consiglio regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio regionale")
        RoleTypeFactory.create(
            label="Consigliere regionale", priority=1, classification=classification
        )
        post = PostFactory.create(
            organization=org,
            label="Consigliere regionale della Valle D'Aosta",
            role="Consigliere regionale",
        )

        person.add_role(
            post=post, start_date="2018-06-26", label=post.label, role=post.role
        )
        person.add_role(
            post=post,
            start_date="2013-07-21",
            end_date="2018-06-26",
            label=post.label,
            role=post.role,
        )

        loader = PopoloPersonMembershipLoader(
            context="reg",
            year="2017",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_regioni31122017_0.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintReg2OpdmTransformation(year="2017"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        p = Person.objects.first()
        self.assertEqual(p.family_name, "Baccega")
        self.assertEqual(
            p.original_profession.name, "PROFESSIONI NON ALTROVE CLASSIFICABILI"
        )
        self.assertEqual(
            p.original_education_level.name,
            "LICENZA DI SCUOLA MEDIA INF. O TITOLI EQUIPOLLENTI",
        )
        self.assertEqual(Membership.objects.count(), 2)
        self.assertEqual(Membership.objects.filter(end_date__isnull=False).count(), 1)
        self.assertEqual(
            Membership.objects.filter(
                sources__source__url__icontains="dait.interno.gov.it"
            ).count(),
            1,
        )
        self.assertEqual(Membership.objects.filter(start_date="2013-07-01").count(), 1)

    def test_etl_minint_provincia(self):
        """Test import from MinInt, yearly summary, provincia context
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"CODICE_REGIONE;DESCRIZIONE_REGIONE;CODICE_PROVINCIA;DESRIZIONE_PROVINCIA;SIGLA_PROVINCIA;"
            b"ISTAT_CODICE_PROVINCIA;ANNO_CENSIMENTO;POPOLAZIONE_CENSITA;DESCRIZIONE_TEMPO_GESTIONE;"
            b"DATA_ELEZIONE;DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;ASSESSORI_ASSEGNATI;"
            b"SIGLA_TITOLO_ACCADEMICO;COGNOME;NOME;SESSO;DATA_NASCITA;SEDE_NASCITA;LIVELLO_CARICA;"
            b"DESCRIZIONE_CARICA;DATA_NOMINA;PARTITO_LISTA_COALIZIONE;DATA_CESSAZIONE;"
            b"TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;002;ALESSANDRIA;AL;006;2011;427229;ORDINARIA;18/12/2016;;12;0;;BALDI;GIANFRANCO;"
            b"M;10/08/1962;ACQUI TERME (AL);10;Presidente della provincia;25/09/2017;;;"
            b"LICENZA DI SCUOLA MEDIA SUP. O TITOLI EQUIPOLLENTI;COMMERCIANTI E ESERCENTI DI NEGOZIO\r\n"
            b"01;PIEMONTE;002;ALESSANDRIA;AL;006;2011;427229;ORDINARIA;18/12/2016;;12;0;PROF;BUSCAGLIA;CARLO;"
            b"M;17/07/1949;DERNICE (AL);110;Consigliere;19/12/2016;"
            b"LS METROPOLITANA O PROV.LE SECONDO GRADO: UNITI PER LA PROVINCIA DI ALESSANDRIA E IL SUO TERRITORIO;;"
            b"LAUREA;PRESIDI\r\n"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": '(family_name:("baldi") AND given_name:("gianfranco") '
                        'AND birth_date_s:("19620810") '
                        'AND birth_location:("acqui terme (al)"))',
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Alessandria")
        area.add_identifier(scheme="ISTAT_CODE_PROV", identifier="006")
        reg = OrganizationFactory(
            name="AMMINISTRAZIONE PROVINCIALE DI ALESSANDRIA",
            classification="Provincia",
            area=area,
        )
        OrganizationFactory.create(
            name="Giunta provinciale di Alessandria",
            classification="Giunta provinciale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta provinciale")
        RoleTypeFactory.create(
            label="Presidente di provincia", priority=1, classification=classification
        )

        OrganizationFactory.create(
            name="Consiglio provinciale di Alessandria",
            classification="Consiglio provinciale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio provinciale")
        RoleTypeFactory.create(
            label="Consigliere provinciale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="prov",
            year="2017",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_province31122017.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintProv2OpdmTransformation(year="2017"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 2)
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Membership.objects.count(), 2)

        p = Person.objects.get(family_name="Baldi")
        self.assertEqual(
            p.original_profession.name, "COMMERCIANTI E ESERCENTI DI NEGOZIO"
        )
        self.assertEqual(
            p.original_education_level.name,
            "LICENZA DI SCUOLA MEDIA SUP. O TITOLI EQUIPOLLENTI",
        )
        m = p.memberships.first()
        self.assertEqual(m.label, "Presidente della Provincia di Alessandria")
        self.assertEqual(m.role, "Presidente di provincia")
        self.assertEqual(m.organization.name, "Giunta provinciale di Alessandria")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

        p = Person.objects.get(family_name="Buscaglia")
        self.assertEqual(p.original_profession.name, "PRESIDI")
        self.assertEqual(p.original_education_level.name, "LAUREA")
        m = p.memberships.first()
        self.assertEqual(m.label, "Consigliere provinciale di Alessandria")
        self.assertEqual(m.role, "Consigliere provinciale")
        self.assertEqual(m.organization.name, "Consiglio provinciale di Alessandria")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_metro(self):
        """Test import from MinInt, yearly summary, città metropolitana context, membership in giunta and consiglio
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"CODICE_REGIONE;DESCRIZIONE_REGIONE;CODICE_PROVINCIA;DESRIZIONE_PROVINCIA;SIGLA_PROVINCIA;"
            b"ISTAT_CODICE_PROVINCIA;ANNO_CENSIMENTO;POPOLAZIONE_CENSITA;DESCRIZIONE_TEMPO_GESTIONE;"
            b"DATA_ELEZIONE;DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;ASSESSORI_ASSEGNATI;"
            b"SIGLA_TITOLO_ACCADEMICO;COGNOME;NOME;SESSO;DATA_NASCITA;SEDE_NASCITA;LIVELLO_CARICA;"
            b"DESCRIZIONE_CARICA;DATA_NOMINA;PARTITO_LISTA_COALIZIONE;DATA_CESSAZIONE;TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;081;TORINO;TO;001;2011;2247780;ORDINARIA;12/10/2014;;;;DOTT;APPENDINO;CHIARA;F;12/06/1984;"
            b"MONCALIERI (TO);10;Sindaco metropolitano;30/06/2016;;;LAUREA;Impiegati di aziende\r\n"
            b"01;PIEMONTE;081;TORINO;TO;001;2011;2247780;ORDINARIA;09/10/2016;;18;0;AVV;AVETTA;ALBERTO;"
            b"M;17/12/1969;IVREA (TO);110;Consigliere;10/10/2016;LS METROPOLITANA O PROV.LE SECONDO GRADO: "
            b"CITTA' DI CITTA' ;;LAUREA;AVVOCATI E PROCURATORI LEGALI\r\n"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Torino")
        area.add_identifier(scheme="ISTAT_CODE_PROV", identifier="001")
        reg = OrganizationFactory(
            name="CITTÀ METROPOLITANA DI TORINO",
            classification="Città metropolitana",
            area=area,
        )
        OrganizationFactory.create(
            name="Conferenza metropolitana di Torino",
            classification="Conferenza metropolitana",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Conferenza metropolitana")
        RoleTypeFactory.create(
            label="Sindaco metropolitano", priority=1, classification=classification
        )

        OrganizationFactory.create(
            name="Consiglio metropolitano di Torino",
            classification="Consiglio metropolitano",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio metropolitano")
        RoleTypeFactory.create(
            label="Consigliere metropolitano", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="metro",
            year="2017",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_province31122017.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintMetro2OpdmTransformation(year="2017"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 2)
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Membership.objects.count(), 2)

        p = Person.objects.get(family_name="Appendino")
        self.assertEqual(p.original_profession.name, "Impiegati di aziende")
        self.assertEqual(p.original_education_level.name, "LAUREA")
        m = p.memberships.first()
        self.assertEqual(m.label, "Sindaco metropolitano di Torino")
        self.assertEqual(m.role, "Sindaco metropolitano")
        self.assertEqual(m.organization.name, "Conferenza metropolitana di Torino")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

        p = Person.objects.get(family_name="Avetta")
        self.assertEqual(p.original_profession.name, "AVVOCATI E PROCURATORI LEGALI")
        self.assertEqual(p.original_education_level.name, "LAUREA")
        m = p.memberships.first()
        self.assertEqual(m.label, "Consigliere metropolitano di Torino")
        self.assertEqual(m.role, "Consigliere metropolitano")
        self.assertEqual(m.organization.name, "Consiglio metropolitano di Torino")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_com(self):
        """Test import from MinInt, yearly summary, comune context, membership in giunta and consiglio
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"CODICE_REGIONE;DESCRIZIONE_REGIONE;CODICE_PROVINCIA;DESCRIZIONE_PROVINCIA;CODICE_COMUNE;"
            b"DESCRIZIONE_COMUNE;SIGLA_PROVINCIA;ISTAT_CODICE_COMUNE;ANNO_CENSIMENTO;POPOLAZIONE_CENSITA;"
            b"MAGGIORITARIO_PROPORZIONALE;DESCRIZIONE_TEMPO_GESTIONE;DATA_ELEZIONE;DATA_BALLOTTAGGIO;"
            b"CONSIGLIERI_SPETTANTI;ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;NOME;SESSO;"
            b"DATA_NASCITA;SEDE_NASCITA;LIVELLO_CARICA;DESCRIZIONE_CARICA;DATA_NOMINA;PARTITO_LISTA_COALIZIONE;"
            b"DATA_CESSAZIONE;TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;002;ALESSANDRIA;0010;ACQUI TERME;AL;006001;2011;20054;P;ORDINARIA;11/06/2017;"
            b"25/06/2017;16;5;;LUCCHINI;LORENZO GIUSEPPE;M;18/08/1963;REGNO UNITO;10;Sindaco;26/06/2017;"
            b"MOVIMENTO 5 STELLE.IT;;LAUREA  BREVE;SPECIALISTI E TECNICI NELLE SCIENZE DELLA SALUTE E "
            b"ADDETTI A SERVIZI SANITARI\r\n"
            b"01;PIEMONTE;002;ALESSANDRIA;0010;ACQUI TERME;AL;006001;2011;20054;P;ORDINARIA;"
            b"11/06/2017;25/06/2017;16;5;;TRENTINI;ELENA;F;13/01/1967;BOLZANO (BZ);80;"
            b"Presidente del consiglio;13/07/2017;MOVIMENTO 5 STELLE.IT;;;"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Acqui Terme")
        area.add_identifier(scheme="ISTAT_CODE_COM", identifier="006001")
        reg = OrganizationFactory(
            name="COMUNE DI ACQUI TERME", classification="Comune", area=area
        )
        OrganizationFactory.create(
            name="Giunta comunale di Acqui Terme",
            classification="Giunta comunale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta comunale")
        RoleTypeFactory.create(
            label="Sindaco", priority=1, classification=classification
        )

        OrganizationFactory.create(
            name="Consiglio comunale di Acqui Terme",
            classification="Consiglio comunale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio comunale")
        RoleTypeFactory.create(
            label="Presidente di consiglio comunale",
            priority=1,
            classification=classification,
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            year="2017",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_comuni31122017.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintCom2OpdmTransformation(year="2017"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 2)
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Membership.objects.count(), 2)

        p = Person.objects.get(family_name="Lucchini")
        self.assertEqual(
            p.original_profession.name,
            "SPECIALISTI E TECNICI NELLE SCIENZE DELLA SALUTE E ADDETTI A SERVIZI SANITARI",
        )
        self.assertEqual(p.original_education_level.name, "LAUREA  BREVE")
        m = p.memberships.first()
        self.assertEqual(m.label, "Sindaco di Acqui Terme")
        self.assertEqual(m.role, "Sindaco")
        self.assertEqual(m.organization.name, "Giunta comunale di Acqui Terme")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

        p = Person.objects.get(family_name="Trentini")
        self.assertEqual(p.original_profession, None)
        self.assertEqual(p.original_education_level, None)
        m = p.memberships.first()
        self.assertEqual(m.label, "Presidente del consiglio comunale di Acqui Terme")
        self.assertEqual(m.role, "Presidente di consiglio comunale")
        self.assertEqual(m.organization.name, "Consiglio comunale di Acqui Terme")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_reg_2013(self):
        """Test import from MinInt, yearly summary, regione context, 2013 format
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"COD_REGIONE;DESC_REGIONE;COD_PROVINCIA;DESC_PROVINCIA;COD_COMUNE;DESC_COMUNE;SIGLA_PROVINCIA;"
            b"ANNO_CENSIMENTO;POPOLAZIONE_CENSITA_TOT;MAGG_PROP;DESC_TEMPO_GESTIONE;DATA_TEMPO_ELETTORALE;"
            b"DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;"
            b"NOME;SESSO;DATA_NASCITA;DESC_SEDE_NASCITA;SEQ_ORDINA;DESCRIZIONE_CARICA;DATA_NOMINA;DESC_PARTITO;"
            b"TIPO_MOVIMENTAZIONE;COD_MOVIMENTAZIONE;DESCRIZIONE;DATA_MOVIMENTAZIONE;DATA_CESSAZIONE;"
            b"TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;000;;0000;;;2001;4214677;;ORDINARIA;28/03/2010;;60;12;;COTA;ROBERTO;M;13/07/1968;"
            b"NOVARA (NO);10;Presidente della regione;09/04/2010;ROBERTO COTA PRESIDENTE;N;901;PRIMA NOMINA;"
            b"09/04/2010;06/06/2014;LAUREA;AVVOCATI E PROCURATORI LEGALI"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": '(family_name:("chiamparino") AND given_name:("sergio") '
                        'AND birth_date_s:("19480901") '
                        'AND birth_location:("moncalieri (to)"))',
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Piemonte")
        area.add_identifier(scheme="MININT_ELETTORALE", identifier="01")
        reg = OrganizationFactory(
            name="Regione Piemonte", classification="Regione", area=area
        )
        OrganizationFactory.create(
            name="Giunta regionale del Piemonte",
            classification="Giunta regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta regionale")
        RoleTypeFactory.create(
            label="Presidente di regione", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="reg",
            year="2013",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_regioni31122013_0.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintReg2OpdmTransformation(year="2013"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        p = Person.objects.first()
        self.assertEqual(p.family_name, "Cota")
        self.assertEqual(p.original_profession.name, "AVVOCATI E PROCURATORI LEGALI")
        self.assertEqual(p.original_education_level.name, "LAUREA")
        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Presidente della Regione Piemonte")
        self.assertEqual(m.role, "Presidente di regione")
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_provincia_2013(self):
        """Test import from MinInt, yearly summary, provincia context, year 2013
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"COD_REGIONE;DESC_REGIONE;COD_PROVINCIA;DESC_PROVINCIA;COD_COMUNE;DESC_COMUNE;SIGLA_PROVINCIA;"
            b"ANNO_CENSIMENTO;POPOLAZIONE_CENSITA_TOT;MAGG_PROP;DESC_TEMPO_GESTIONE;DATA_TEMPO_ELETTORALE;"
            b"DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;"
            b"NOME;SESSO;DATA_NASCITA;DESC_SEDE_NASCITA;SEQ_ORDINA;DESCRIZIONE_CARICA;DATA_NOMINA;DESC_PARTITO;"
            b"TIPO_MOVIMENTAZIONE;COD_MOVIMENTAZIONE;DESCRIZIONE;DATA_MOVIMENTAZIONE;DATA_CESSAZIONE;"
            b"TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;002;ALESSANDRIA;0000;;AL;2001;418231;;ORDINARIA;07/06/2009;21/06/2009;30;10;DOTT;"
            b"FILIPPI;PAOLO;M;15/09/1962;CASALE MONFERRATO (AL);10;Presidente della provincia;23/06/2009;"
            b"DI PIETRO ITALIA DEI VALORI | PARTITO DEMOCRATICO | LISTA LOCALE: IN PROVINCIA FILIPPI PRESIDENTE "
            b"| Colore Politico non pervenuto | LISTA LOCALE: CIVICA MOVIMENTO DEMOCRATICO CRESCERE INS"
            b"IEME PER LA PROVINCIA | CEN-SIN(LS.CIVICHE) | SINISTRA E LIBERTA' | COMUNISTI ITALIANI "
            b"| MODERATI;N;901;PRIMA NOMINA;23/06/2009;;LAUREA;"
            b"IMPIEGATI  AMMINISTRATIVI CON MANSIONI DIRETTIVE E DI CONCETTO\r\n"
            b"01;PIEMONTE;002;ALESSANDRIA;0000;;AL;2001;418231;;ORDINARIA;07/06/2009;21/06/2009;30;10;;"
            b"RIBOLDI;FEDERICO;M;05/02/1986;OMEGNA (NO);110;Consigliere;27/07/2009;IL POPOLO DELLA LIBERTA';"
            b"N;901;PRIMA NOMINA;27/07/2009;;LICENZA DI SCUOLA MEDIA SUP. O TITOLI EQUIPOLLENTI;SCOLARI E STUDENTI\r\n"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": '(family_name:("baldi") AND given_name:("gianfranco") '
                        'AND birth_date_s:("19620810") '
                        'AND birth_location:("acqui terme (al)"))',
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Alessandria")
        area.add_identifier(scheme="MININT_ELETTORALE", identifier="002")
        reg = OrganizationFactory(
            name="AMMINISTRAZIONE PROVINCIALE DI ALESSANDRIA",
            classification="Provincia",
            area=area,
        )
        OrganizationFactory.create(
            name="Giunta provinciale di Alessandria",
            classification="Giunta provinciale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta provinciale")
        RoleTypeFactory.create(
            label="Presidente di provincia", priority=1, classification=classification
        )

        OrganizationFactory.create(
            name="Consiglio provinciale di Alessandria",
            classification="Consiglio provinciale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio provinciale")
        RoleTypeFactory.create(
            label="Consigliere provinciale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="prov",
            year="2013",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_province31122013.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintProv2OpdmTransformation(year="2013"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 2)
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Membership.objects.count(), 2)

        p = Person.objects.get(family_name="Filippi")
        self.assertEqual(
            p.original_profession.name,
            "IMPIEGATI  AMMINISTRATIVI CON MANSIONI DIRETTIVE E DI CONCETTO",
        )
        self.assertEqual(p.original_education_level.name, "LAUREA")
        m = p.memberships.first()
        self.assertEqual(m.label, "Presidente della Provincia di Alessandria")
        self.assertEqual(m.role, "Presidente di provincia")
        self.assertEqual(m.organization.name, "Giunta provinciale di Alessandria")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

        p = Person.objects.get(family_name="Riboldi")
        self.assertEqual(p.original_profession.name, "SCOLARI E STUDENTI")
        self.assertEqual(
            p.original_education_level.name,
            "LICENZA DI SCUOLA MEDIA SUP. O TITOLI EQUIPOLLENTI",
        )
        m = p.memberships.first()
        self.assertEqual(m.label, "Consigliere provinciale di Alessandria")
        self.assertEqual(m.role, "Consigliere provinciale")
        self.assertEqual(m.organization.name, "Consiglio provinciale di Alessandria")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_com_2013(self):
        """Test import from MinInt, for year 2013 (differewnt format)
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"COD_REGIONE;DESC_REGIONE;COD_PROVINCIA;DESC_PROVINCIA;COD_COMUNE;DESC_COMUNE;SIGLA_PROVINCIA;"
            b"ANNO_CENSIMENTO;POPOLAZIONE_CENSITA_TOT;MAGG_PROP;DESC_TEMPO_GESTIONE;DATA_TEMPO_ELETTORALE;"
            b"DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;"
            b"NOME;SESSO;DATA_NASCITA;DESC_SEDE_NASCITA;SEQ_ORDINA;DESCRIZIONE_CARICA;DATA_NOMINA;DESC_PARTITO;"
            b"TIPO_MOVIMENTAZIONE;COD_MOVIMENTAZIONE;DESCRIZIONE;DATA_MOVIMENTAZIONE;DATA_CESSAZIONE;"
            b"TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;002;ALESSANDRIA;0010;ACQUI TERME;AL;2001;19184;P;ORDINARIA;06/05/2012;20/05/2012;16;5;;"
            b"BERTERO;ENRICO SILVIO;M;27/03/1959;ACQUI TERME (AL);10;Sindaco;21/05/2012;"
            b"LISTA CIVICA: ACQUINSIEME | IL POPOLO DELLA LIBERTA' | LISTA CIVICA: PER BERTERO SINDACO;N;"
            b"901;Proclamazione;21/05/2012;;"
            b"LICENZA DI SCUOLA MEDIA SUP. O TITOLI EQUIPOLLENTI;RAPPRESENTANTI DI COMMERCIO, "
            b"VIAGGIATORI, MEDIATORI E ASSIMILATI\r\n"
            b"01;PIEMONTE;002;ALESSANDRIA;0010;ACQUI TERME;AL;2001;19184;P;ORDINARIA;06/05/2012;20/05/2012;16;5;"
            b"ING;CANNITO;PIER PAOLO;M;04/07/1977;NIZZA MONFERRATO (AT);120;Consigliere -  Candidato Sindaco;"
            b"06/06/2012;MOVIMENTO 5 STELLE BEPPEGRILLO.IT;N;901;Proclamazione;06/06/2012;;LAUREA;"
            b"INGEGNERI, ARCHITETTI E ALTRI SPECIALISTI E TECNICI IN MATERIA DI SCIENZE INGEGNERISTICHE, "
            b"SICUREZZA E TUTELA DEL TERRITORIO, DEGLI EDIFICI E DELL'AMBIENTE, QUALITA' INDUSTRIALE\r\n"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Acqui Terme")
        area.add_identifier(scheme="MININT_ELETTORALE", identifier="1010020010")
        reg = OrganizationFactory(
            name="COMUNE DI ACQUI TERME", classification="Comune", area=area
        )
        OrganizationFactory.create(
            name="Giunta comunale di Acqui Terme",
            classification="Giunta comunale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta comunale")
        RoleTypeFactory.create(
            label="Sindaco", priority=1, classification=classification
        )

        OrganizationFactory.create(
            name="Consiglio comunale di Acqui Terme",
            classification="Consiglio comunale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio comunale")
        RoleTypeFactory.create(
            label="Consigliere comunale", priority=2, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            year="2013",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_comuni_31122013.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintCom2OpdmTransformation(year="2013"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 2)
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Membership.objects.count(), 2)

        p = Person.objects.get(family_name="Bertero")
        self.assertEqual(
            p.original_profession.name,
            "RAPPRESENTANTI DI COMMERCIO, VIAGGIATORI, MEDIATORI E ASSIMILATI",
        )
        self.assertEqual(
            p.original_education_level.name,
            "LICENZA DI SCUOLA MEDIA SUP. O TITOLI EQUIPOLLENTI",
        )
        m = p.memberships.first()
        self.assertEqual(m.label, "Sindaco di Acqui Terme")
        self.assertEqual(m.role, "Sindaco")
        self.assertEqual(m.organization.name, "Giunta comunale di Acqui Terme")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

        p = Person.objects.get(family_name="Cannito")
        self.assertEqual(
            p.original_profession.name,
            "INGEGNERI, ARCHITETTI E ALTRI SPECIALISTI E TECNICI IN MATERIA DI SCIENZE INGEGNERISTICHE, "
            "SICUREZZA E TUTELA DEL TERRITORIO, DEGLI EDIFICI E DELL'AMBIENTE, QUALITA' INDUSTRIALE",
        )
        self.assertEqual(p.original_education_level.name, "LAUREA")
        m = p.memberships.first()
        self.assertEqual(m.label, "Consigliere comunale di Acqui Terme")
        self.assertEqual(m.role, "Consigliere comunale")
        self.assertEqual(m.organization.name, "Consiglio comunale di Acqui Terme")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_commissario(self):
        """Test import from MinInt, yearly summary, comune context, membership for Commissario

        Membership label is kept (Commissario Prefettizio)
        Post label is normalized and aggiusted (Commissario della giunta comunale di ...)
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"CODICE_REGIONE;DESCRIZIONE_REGIONE;CODICE_PROVINCIA;DESCRIZIONE_PROVINCIA;CODICE_COMUNE;"
            b"DESCRIZIONE_COMUNE;SIGLA_PROVINCIA;ISTAT_CODICE_COMUNE;ANNO_CENSIMENTO;POPOLAZIONE_CENSITA;"
            b"MAGGIORITARIO_PROPORZIONALE;DESCRIZIONE_TEMPO_GESTIONE;DATA_ELEZIONE;DATA_BALLOTTAGGIO;"
            b"CONSIGLIERI_SPETTANTI;ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;NOME;SESSO;"
            b"DATA_NASCITA;SEDE_NASCITA;LIVELLO_CARICA;DESCRIZIONE_CARICA;DATA_NOMINA;PARTITO_LISTA_COALIZIONE;"
            b"DATA_CESSAZIONE;TITOLO_DI_STUDIO;PROFESSIONE\r\n"
            b"01;PIEMONTE;027;CUNEO;0830;ELVA;CN;004083;2001;114;M;SCIOGLIMENTO;"
            b"06/05/2012;;6;0;;BERGIA;CLAUDIA;;;;10;Commissario Prefettizio;12/06/2017;;;;\r\n"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Elva")
        area.add_identifier(scheme="ISTAT_CODE_COM", identifier="004083")
        reg = OrganizationFactory(
            name="COMUNE DI ELVA", classification="Comune", area=area
        )
        OrganizationFactory.create(
            name="Giunta comunale di Elva", classification="Giunta comunale", parent=reg
        )
        classification = ClassificationFactory.create(descr="Giunta comunale")
        RoleTypeFactory.create(
            label="Commissario di giunta comunale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            year="2017",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/storico_amministratori_comuni31122017.csv",
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=HistMinintCom2OpdmTransformation(year="2017"),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)

        p = Person.objects.get(family_name="Bergia")
        self.assertEqual(p.original_profession, None)
        self.assertEqual(p.original_education_level, None)
        m = p.memberships.first()
        self.assertEqual(m.label, "Commissario Prefettizio di Elva")
        self.assertEqual(m.role, "Commissario di giunta comunale")
        self.assertEqual(m.organization.name, "Giunta comunale di Elva")
        self.assertNotEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)
