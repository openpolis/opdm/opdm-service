# -*- coding: utf-8 -*-

"""
Implements tests specific to the CSVDiffCompositeETL class.
"""
import json
import logging
import os
import shutil

from ooetl import DummyTransformation
from ooetl.loaders import JsonLoader
from ooetl.tests.django import ETLTest

from project.tasks.etl import ETLException
from project.tasks.etl.composites import JsonDiffCompositeETL
from project.tasks.etl.composites.diff_logics import (
    person_key_from_dict, CloseItemRemovesDiffLogic,
    CloseInnerMembershipsOwnershipsRemovesDiffLogic, CheckInnerDetailsUpdatesDiffLogic,
)
from project.tasks.etl.extractors import JsonArrayExtractor


class JsonDiffCompositeTest(ETLTest):

    with open('project/tasks/tests/etl/files/json_test_new.json') as json_new:
        new_json_content = json.load(json_new)

    with open('project/tasks/tests/etl/files/json_test_old.json') as json_old:
        old_json_content = json.load(json_old)

    test_cache_path = "resources/data/cachetest"
    test_filename = "test.json"

    @classmethod
    def cleanUpCache(cls, rmdir=False):
        """Clean up cache folder content, may also remove dir

        Invoked after each test, to remove test files (sort of rollback, for files used in tests)

        If invoked at the end of the tests, then the folder is also removed.

        :param rmdir: whether to remove the folder or not
        :return:
        """
        folder = cls.test_cache_path

        # the folder path must contain 'cachetest', for security reasons
        if "cachetest" in folder and os.path.isdir(folder):
            for f in os.listdir(folder):
                f_path = os.path.join(folder, f)
                if os.path.isfile(f_path):
                    os.unlink(f_path)
            if rmdir:
                shutil.rmtree(folder)

    @classmethod
    def setUpClass(cls):
        super(JsonDiffCompositeTest, cls).setUpClass()
        if not os.path.isdir(cls.test_cache_path):
            os.mkdir(cls.test_cache_path)

    @classmethod
    def tearDownClass(cls):
        cls.cleanUpCache(rmdir=True)
        super(JsonDiffCompositeTest, cls).tearDownClass()

    def test_composite_etl_method_raises_exception_when_etl_method_invoked(self):
        """Test scenario fails when an ETL method etl(), extract(), ... is invoked on a CompositeETL"""
        with self.assertRaises(ETLException) as exc_ctx:
            JsonDiffCompositeETL(
                extractor=JsonArrayExtractor(
                    url="https://storage.opdm.io/test.json",  # mocked, not a real URL
                ),
                transformation=DummyTransformation(),
                loader=JsonLoader(self.test_cache_path),
                local_cache_path=self.test_cache_path,
                local_out_path=self.test_cache_path,
                filename=self.test_filename,
                log_level=logging.CRITICAL,
                key_getter=lambda x: 1
            ).etl()
        self.assertEqual(
            "Not available in Composite ETL classes",
            exc_ctx.exception.args[0]
        )

    def test_composite_etl_method_raises_exception_when_no_key_getter(self):
        """Test scenario fails when key_getter argument is not passed to JsonDiffCompositeETL"""
        with self.assertRaises(ETLException) as exc_ctx:
            JsonDiffCompositeETL(
                extractor=JsonArrayExtractor(
                    url="https://storage.opdm.io/test.json",  # mocked, not a real URL
                ),
                transformation=DummyTransformation(),
                loader=JsonLoader(self.test_cache_path),
                local_cache_path=self.test_cache_path,
                local_out_path=self.test_cache_path,
                filename=self.test_filename,
                log_level=logging.CRITICAL
            ).etl()
        self.assertEqual(
            "A key_getter function must be passed to identify records uniquely",
            exc_ctx.exception.args[0]
        )

    def test_no_old_json(self):
        """Test scenario where cached file does not exist, and it's created"""

        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(self.new_json_content)
        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(cache_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (
                x['family_name'], x['given_name'],
                x.get('birth_date', None), x.get('gender', None), x.get('birth_location', None)
            )
        )()

        # io streams contain the correct number of lines
        with open(cache_file, "r") as f:
            content = json.load(f)

        self.assertEqual(len(content), len(self.new_json_content))

        self.cleanUpCache()

    def test_no_changes(self):
        """Test scenario where new content and cached file are identical"""
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(self.new_json_content)
        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = self.new_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(cache_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (
                x['family_name'], x['given_name'],
                x.get('birth_date', None), x.get('gender', None), x.get('birth_location', None)
            )
        )()

        # cached content is not touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertEqual(cached_content, final_content)

        self.cleanUpCache()

    def test_changes(self):
        """Test scenario where new content and cached file are different"""
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(self.new_json_content)
        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = self.old_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(cache_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (
                x['family_name'], x['given_name'],
                x.get('birth_date', None), x.get('gender', None), x.get('birth_location', None)
            )
        )()

        # cached content is touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertNotEqual(cached_content, final_content)

        # produced files contain the correct number of lines
        self.assertEqual(len(final_content), len(self.new_json_content))

        # a removals file is produced
        with open(
            "{0}/test_removes.json".format(self.test_cache_path), "r"
        ) as f:
            removes_content = json.load(f)
            self.assertEqual(len(removes_content), 2)

        self.cleanUpCache()

    def test_changes_close_memberships_in_removals(self):
        from datetime import datetime

        """Test scenario where new content and cached file are different.
        Removals are transformed into updates with memberships closed"""
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(self.new_json_content)
        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)
        out_file = "{0}/adds_and_updates_{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with old content
        cached_content = self.old_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(out_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=person_key_from_dict,
            removes_diff_logic=CloseInnerMembershipsOwnershipsRemovesDiffLogic(
                end_field='end_date',
                end_date=datetime.strftime(datetime.now(), "%Y-%m-%d"),
                end_reason='record missing from source',
            ),
            updates_diff_logic=CheckInnerDetailsUpdatesDiffLogic(
                person_key_getter=person_key_from_dict
            )
        )()

        # cached content is touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertNotEqual(cached_content, final_content)

        # produced files contain the correct number of lines
        self.assertEqual(len(final_content), len(self.new_json_content))

        with open(out_file, "r") as f:
            out_content = json.load(f)

        # adds_and_updates contain the correct number of records
        self.assertEqual(len(out_content), 4)

        # memberships were correctly closed for removals
        conte = next(x for x in out_content if x['family_name'] == 'CONTE')
        self.assertEqual(len(conte['memberships']), 1)
        self.assertEqual(conte['memberships'][0]['end_reason'], 'record missing from source')

        dimaio = next(x for x in out_content if x['family_name'] == 'DI MAIO')
        self.assertEqual(len(dimaio['memberships']), 3)
        self.assertTrue(
            any(
                m.get('end_reason', None) == 'record missing from source'
                for m in dimaio["memberships"]
            )
        )

        # inner memberships of persons not removed were correctly removed
        bongiorno = next(x for x in out_content if x['family_name'] == 'BONGIORNO')
        self.assertEqual(len(bongiorno['memberships']), 3)
        self.assertTrue(
            any(
                m.get('end_reason', None) == 'record missing from source'
                for m in bongiorno["memberships"]
            )
        )

        # 4 ownerships are in the bongiorno updates record
        self.assertEqual(len(bongiorno['ownerships']), 4)

        # at least one was closed
        self.assertTrue(
            any(
                o.get('end_reason', None) == 'record missing from source'
                for o in bongiorno["ownerships"]
            )
        )

        self.cleanUpCache()

    def test_orgs_changes(self):
        """Test scenario where new content and cached file are different"""

        with open('project/tasks/tests/etl/files/json_test_orgs_new.json') as json_new:
            new_json_content = json.load(json_new)

        with open('project/tasks/tests/etl/files/json_test_orgs_old.json') as json_old:
            old_json_content = json.load(json_old)

        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(new_json_content)

        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = old_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(cache_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (x['name'], x['identifier'], x.get('founding_date', None)),
        )()

        # cached content is touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertNotEqual(cached_content, final_content)

        # produced files contain the correct number of lines
        self.assertEqual(len(final_content), len(new_json_content))

        # a removals file is produced
        with open(
            "{0}/test_removes.json".format(self.test_cache_path), "r"
        ) as f:
            removes_content = json.load(f)
            self.assertEqual(len(removes_content), 1)

        self.cleanUpCache()

    def test_orgs_changes_close_in_removals(self):
        """Test scenario where new content and cached file are different.
        Removals are transformed into updates with memberships closed"""
        from datetime import datetime

        with open('project/tasks/tests/etl/files/json_test_orgs_new.json') as json_new:
            new_json_content = json.load(json_new)

        with open('project/tasks/tests/etl/files/json_test_orgs_old.json') as json_old:
            old_json_content = json.load(json_old)

        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(new_json_content)
        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)
        out_file = "{0}/adds_and_updates_{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = old_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(out_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (x['name'], x['identifier'], x.get('founding_date', None)),
            removes_diff_logic=CloseItemRemovesDiffLogic(
                end_field='dissolution_date',
                end_date=datetime.strftime(datetime.now(), "%Y-%m-%d"),
                end_reason='record missing from source',
            ),
        )()

        # cached content is touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertNotEqual(cached_content, final_content)

        # produced files contain the correct number of lines
        self.assertEqual(len(final_content), len(new_json_content))

        # memberships were correctly closed for removals
        with open(out_file, "r") as f:
            out_content = json.load(f)

        # adds_and_updates contains 3 records:
        # - an addiction
        # - an update
        # - a removal, with dissolution_date set to today
        self.assertEqual(len(out_content), 3)

        # the removals reason has been set at least into one of the records
        self.assertTrue(
            any('end_reason' in o and o['end_reason'] == 'record missing from source'
                for o in out_content)
        )

        self.cleanUpCache()

    def test_ownerships_persons_changes(self):
        """Test scenario where new content and cached file are different"""

        with open('project/tasks/tests/etl/files/json_test_ownerships_persons_new.json') as json_new:
            new_json_content = json.load(json_new)

        with open('project/tasks/tests/etl/files/json_test_ownerships_persons_old.json') as json_old:
            old_json_content = json.load(json_old)

        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(new_json_content)

        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = old_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        owner_type = "person"
        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(cache_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (
                x['owning_{0}'.format(owner_type.replace('organization', 'org'))]['identifier'],
                x['owned_org']['identifier']
            ),
        )()

        # cached content is touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertNotEqual(cached_content, final_content)

        # produced files contain the correct number of lines
        self.assertEqual(len(final_content), len(new_json_content))

        # a removals file is produced
        with open(
            "{0}/test_removes.json".format(self.test_cache_path), "r"
        ) as f:
            removes_content = json.load(f)
            self.assertEqual(len(removes_content), 1)

        self.cleanUpCache()

    def test_ownerships_persons_close_in_removals(self):
        """Test scenario where new content and cached file are different.
        Removals are transformed into updates with memberships closed"""
        from datetime import datetime

        with open('project/tasks/tests/etl/files/json_test_ownerships_persons_new.json') as json_new:
            new_json_content = json.load(json_new)

        with open('project/tasks/tests/etl/files/json_test_ownerships_persons_old.json') as json_old:
            old_json_content = json.load(json_old)

        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(new_json_content)
        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)
        out_file = "{0}/adds_and_updates_{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = old_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        owner_type = "person"
        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(out_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (
                x['owning_{0}'.format(owner_type.replace('organization', 'org'))]['identifier'],
                x['owned_org']['identifier']
            ),
            removes_diff_logic=CloseItemRemovesDiffLogic(
                end_date=datetime.strftime(datetime.now(), "%Y-%m-%d"),
                end_reason='record missing from source',
            ),
        )()

        # cached content is touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertNotEqual(cached_content, final_content)

        # produced files contain the correct number of lines
        self.assertEqual(len(final_content), len(new_json_content))

        # memberships were correctly closed for removals
        with open(out_file, "r") as f:
            out_content = json.load(f)

        # adds_and_updates contains 3 records:
        # - an addiction
        # - an update
        # - a removal, with dissolution_date set to today
        self.assertEqual(len(out_content), 3)

        # the removals reason has been set at least into one of the records
        self.assertTrue(
            any('end_date' in o and o['end_date'] is not None
                for o in out_content)
        )

        self.cleanUpCache()

    def test_ownerships_orgs_changes(self):
        """Test scenario where new content and cached file are different"""

        with open('project/tasks/tests/etl/files/json_test_ownerships_orgs_new.json') as json_new:
            new_json_content = json.load(json_new)

        with open('project/tasks/tests/etl/files/json_test_ownerships_orgs_old.json') as json_old:
            old_json_content = json.load(json_old)

        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(new_json_content)

        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = old_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        owner_type = "organization"
        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(cache_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (
                x['owning_{0}'.format(owner_type.replace('organization', 'org'))]['identifier'],
                x['owned_org']['identifier']
            ),
        )()

        # cached content is touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertNotEqual(cached_content, final_content)

        # produced files contain the correct number of lines
        self.assertEqual(len(final_content), len(new_json_content))

        # a removals file is produced
        with open(
            "{0}/test_removes.json".format(self.test_cache_path), "r"
        ) as f:
            removes_content = json.load(f)
            self.assertEqual(len(removes_content), 1)

        self.cleanUpCache()

    def test_ownerships_orgs_changes_close_in_removals(self):
        """Test scenario where new content and cached file are different.
        Removals are transformed into updates with memberships closed"""
        from datetime import datetime

        with open('project/tasks/tests/etl/files/json_test_ownerships_orgs_new.json') as json_new:
            new_json_content = json.load(json_new)

        with open('project/tasks/tests/etl/files/json_test_ownerships_orgs_old.json') as json_old:
            old_json_content = json.load(json_old)

        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(new_json_content)
        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)
        out_file = "{0}/adds_and_updates_{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = old_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        owner_type = "organization"
        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(out_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (
                x['owning_{0}'.format(owner_type.replace('organization', 'org'))]['identifier'],
                x['owned_org']['identifier']
            ),
            removes_diff_logic=CloseItemRemovesDiffLogic(
                end_date=datetime.strftime(datetime.now(), "%Y-%m-%d"),
                end_reason='record missing from source',
            )
        )()

        # cached content is touched
        with open(cache_file, "r") as f:
            final_content = json.load(f)
        self.assertNotEqual(cached_content, final_content)

        # produced files contain the correct number of lines
        self.assertEqual(len(final_content), len(new_json_content))

        # memberships were correctly closed for removals
        with open(out_file, "r") as f:
            out_content = json.load(f)

        # adds_and_updates contains 3 records:
        # - an addiction
        # - an update
        # - a removal, with dissolution_date set to today
        self.assertEqual(len(out_content), 3)

        # the removals reason has been set at least into one of the records
        self.assertTrue(
            any('end_date' in o and o['end_date'] is not None
                for o in out_content)
        )

        self.cleanUpCache()

    def test_clear_cache(self):
        """Test scenario where new content and cached file are identical, but cache is cleared"""
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = json.dumps(self.new_json_content)
        cache_file = "{0}/{1}".format(self.test_cache_path, self.test_filename)

        # create old json file in cache with new content
        cached_content = self.new_json_content
        with open(cache_file, "w") as f:
            json.dump(cached_content, f)

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(
                url="https://storage.opdm.io/test.json",  # mocked, not a real URL
            ),
            transformation=DummyTransformation(),
            loader=JsonLoader(cache_file),
            local_cache_path=self.test_cache_path,
            local_out_path=self.test_cache_path,
            clear_cache=True,
            filename=self.test_filename,
            log_level=logging.CRITICAL,
            key_getter=lambda x: (
                x['family_name'], x['given_name'],
                x.get('birth_date', None), x.get('gender', None), x.get('birth_location', None)
            )
        )()

        # io streams contain the correct number of lines
        with open(cache_file, "r") as f:
            content = json.load(f)

        self.assertEqual(len(content), len(self.new_json_content))

        self.cleanUpCache()
