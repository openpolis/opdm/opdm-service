# -*- coding: utf-8 -*-

"""
Implements tests specific to the etl classes.
"""
import json
import logging

from popolo.models import Person, Membership
from popolo.tests.factories import (
    OrganizationFactory,
    RoleTypeFactory,
    AreaFactory,
    ClassificationFactory,
)

from project.tasks.etl.extractors import OPAPIPagedExtractor
from project.tasks.etl.loaders import DummyLoader
from project.tasks.etl.loaders.persons import (
    PopoloPersonLoader,
    PopoloMembershipLoader,
)
from project.tasks.etl.transformations import Op2OpdmETL
from project.tasks.tests.etl import SolrETLTest, MockResponse

json_resp = {
    "count": 1,
    "next": None,
    "previous": None,
    "results": [
        {
            "date_start": "2014-05-26",
            "date_end": None,
            "politician": {
                "content": {
                    "id": 160621,
                    "created_at": "2007-02-27T04:02:56",
                    "updated_at": "2017-07-11T16:51:05",
                },
                "first_name": "SERGIO",
                "last_name": "CHIAMPARINO",
                "birth_date": "1948-09-01T00:00:00",
                "death_date": None,
                "birth_location": "Moncalieri (TO)",
                "sex": "M",
                "self_uri": "http://api3.openpolis.it/politici/politicians/160621",
                "image_uri": "http://politici.openpolis.it/politician/picture?content_id=160621",
                "openparlamento_uri": "http://api3.openpolis.it/parlamento/18/parliamentarians/160621",
                "last_charge_update": "2017-07-11T16:51:05",
                "last_resource_update": "2014-07-25T11:33:56",
                "profession": {"description": "Impiegato"},
                "education_levels": [
                    {
                        "description": "Scienze Politiche",
                        "education_level": {"description": "Laurea"},
                    }
                ],
                "resources": [
                    {
                        "resource_type": "Altra Email",
                        "value": "infochiampa@gmail.com",
                        "description": "",
                        "updated_at": "2014-07-25T11:33:56",
                    }
                ],
                "institution_charges": [
                    {
                        "charge": "dal 31/05/2006 al 15/05/2011 Sindaco Giunta Comunale Torino (Partito: PD)",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/189282",
                    },
                    {
                        "charge": "dal 09/05/1996 al 29/05/2001 Deputato",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/591326",
                    },
                    {
                        "charge": "dal 12/07/1993 al 02/05/1996 Consigliere Consiglio Comunale Torino (Lista "
                        "elettorale: DS) ",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/656101",
                    },
                    {
                        "charge": "dal 28/05/2001 al 27/05/2006 Sindaco Giunta Comunale Torino (Partito: DS)",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/656103",
                    },
                    {
                        "charge": "dal 26/05/2014 Pres. Giunta Regionale Piemonte (Partito: PD)",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/720138",
                    },
                    {
                        "charge": "dal 30/06/2014 Consigliere Consiglio Regionale Piemonte (Lista elettorale: "
                        "CHIAMPARINO PRESIDENTE) ",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/781576",
                    },
                ],
            },
            "charge_type_descr": "Presidente",
            "institution_descr": "Giunta Regionale",
            "location_descr": "Regione Piemonte",
            "location": "http://api3.openpolis.it/territori/locations/3",
            "constituency_descr": None,
            "constituency_election_type": None,
            "description": "",
            "party": {
                "name": "Partito Democratico",
                "acronym": "PD",
                "oname": "Partito Democratico",
            },
            "group": {"name": "Non specificato", "acronym": None, "oname": None},
            "content": {
                "content": {
                    "id": 720138,
                    "created_at": "2014-05-28T12:35:41",
                    "updated_at": "2014-05-28T12:35:41",
                },
                "user": 28139,
                "deleted_at": None,
                "verified_at": None,
            },
        }
    ],
}


class Op2OpdmETLTest(SolrETLTest):
    def test_etl_dummy_loader_list(self):
        """Test DummyLoader class
        """
        self.mock_get.return_value = MockResponse(json_resp, 200)

        loader = DummyLoader()
        loader.logger = logging.getLogger(__name__)

        etl = Op2OpdmETL(
            extractor=OPAPIPagedExtractor(
                "http://api3.openpolis.it/politici/instcharges",
                starting_page=1,
                page_size=10,
                n_pages=1,
                api_filters="updated_at=1970-01-01&institution_id=6",
            ),
            loader=loader,
            log_level=0,
        )
        etl = etl.extract()

        df = etl.etl().original_data
        self.assertIsInstance(df, list)
        self.assertEqual(len(df), 1)

    def test_etl_opapi(self):
        """Test import from OpAPI
        """

        # mock response from OPAPI
        self.mock_get.return_value = MockResponse(json_resp, 200)

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "OP_ID_s:(1234)",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update response
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        loader = DummyLoader()
        loader.logger = logging.getLogger(__name__)

        etl = Op2OpdmETL(
            extractor=OPAPIPagedExtractor(
                "http://api3.openpolis.it/politici/instcharges",
                starting_page=1,
                page_size=10,
                n_pages=1,
                api_filters="updated_at=1970-01-01&institution_id=6",
            ),
            loader=loader,
            log_level=0,
        )
        etl = etl.extract()

        etl.set_loader(
            PopoloPersonLoader(
                context="OPAPI import",
                lookup_strategy="identifier",
                update_strategy="keep_old",
            )
        )
        etl.transform_persons().load()
        self.assertEqual(Person.objects.count(), 1)
        p = Person.objects.first()
        self.assertEqual(p.family_name, "Chiamparino")
        self.assertEqual(p.original_profession.name, "Impiegato")
        self.assertEqual(p.original_education_level.name, "Laurea (Scienze Politiche)")

        area = AreaFactory.create(name="Piemonte")
        area.add_identifier(scheme="ISTAT_CODE", identifier="01")
        area.add_identifier(scheme="OP_ID", identifier="3")
        reg = OrganizationFactory(
            name="Regione Piemonte", classification="Regione", area=area
        )
        OrganizationFactory.create(
            name="Giunta regionale del Piemonte",
            classification="Giunta regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta regionale")
        RoleTypeFactory.create(
            label="Presidente di regione", priority=1, classification=classification
        )

        etl.set_loader(PopoloMembershipLoader())
        etl.transform_memberships().load()

        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Presidente della Regione Piemonte")
        self.assertEqual(m.role, "Presidente di regione")
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("api3.openpolis.it" in m.sources.first().source.url, True)
