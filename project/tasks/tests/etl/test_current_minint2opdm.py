# -*- coding: utf-8 -*-

"""
Implements tests specific to the etl classes.
"""
import json
import logging

from ooetl import ETL
import pandas as pd
from popolo.models import Person, Membership
from popolo.tests.factories import (
    OrganizationFactory,
    RoleTypeFactory,
    AreaFactory,
    ClassificationFactory,
    PersonFactory,
    PostFactory,
)

from project.tasks.etl.extractors import UACSVExtractor
from project.tasks.etl.loaders import DummyLoader
from project.tasks.etl.loaders.persons import PopoloPersonMembershipLoader
from project.tasks.etl.transformations.minint2opdm import (
    CurrentMinintReg2OpdmTransformation,
    CurrentMinintProv2OpdmTransformation,
    CurrentMinintMetro2OpdmTransformation,
    CurrentMinintCom2OpdmTransformation,
    CurrentMinintComm2OpdmTransformation,
)
from project.tasks.tests.etl import SolrETLTest


class CurrentMinint2OpdmETLTest(SolrETLTest):
    def test_etl_dummy_loader_pd(self):
        """Test DummyLoader class
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Regionali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"denominazione_regione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita"
            "\";\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica"
            "\";\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"PIEMONTE\";\"CIRIO\";\"ALBERTO\";\"M\";\"06/12/1972\";\"TORINO (TO)\";\"Presidente della "
            "regione\";;\"26/05/2019\";\"27/05/2019\";\"FORZA ITALIA BERLUSCONI | GIORGIA MELONI FRATELLI D'ITALIA | "
            "LEGA SALVINI PIEMONTE | LIBERTAS UDC - PPE | SÌ TAV SÌ LAVORO PER IL PIEMONTE\";\"Laurea "
            "Magistrale\";\"Imprenditori, gestori e responsabili di piccole imprese\"\r\n"
        ).encode("utf-8")

        area = AreaFactory.create(name="Piemonte")
        area.add_identifier(scheme="ISTAT_CODE", identifier="01")
        reg = OrganizationFactory(
            name="Regione Piemonte", classification="Regione", area=area
        )
        OrganizationFactory.create(
            name="Giunta regionale del Piemonte",
            classification="Giunta regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta regionale")
        RoleTypeFactory.create(
            label="Presidente di regione", priority=1, classification=classification
        )

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=CurrentMinintReg2OpdmTransformation(),
            loader=DummyLoader(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)

    def test_etl_minint_reg(self):
        """Test import from MinInt, yearly summary, regione context
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Regionali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"denominazione_regione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita"
            "\";\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica"
            "\";\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"PIEMONTE\";\"CIRIO\";\"ALBERTO\";\"M\";\"06/12/1972\";\"TORINO (TO)\";\"Presidente della "
            "regione\";;\"26/05/2019\";\"27/05/2019\";\"FORZA ITALIA BERLUSCONI | GIORGIA MELONI FRATELLI D'ITALIA | "
            "LEGA SALVINI PIEMONTE | LIBERTAS UDC - PPE | SÌ TAV SÌ LAVORO PER IL PIEMONTE\";\"Laurea "
            "Magistrale\";\"Imprenditori, gestori e responsabili di piccole imprese\"\r\n"
        ).encode("utf-8")

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Piemonte")
        area.add_identifier(scheme="ISTAT_CODE_REG", identifier="01")
        reg = OrganizationFactory(
            name="Regione Piemonte", classification="Regione", area=area
        )
        OrganizationFactory.create(
            name="Giunta regionale del Piemonte",
            classification="Giunta regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta regionale")
        RoleTypeFactory.create(
            label="Presidente di regione", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="reg",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=CurrentMinintReg2OpdmTransformation(),
            loader=loader,
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        p = Person.objects.first()
        self.assertEqual(p.family_name, "Cirio")
        self.assertEqual(
            p.original_profession.name, "Imprenditori, gestori e responsabili di piccole imprese"
        )
        self.assertEqual(p.original_education_level.name, "Laurea Magistrale")
        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Presidente della Regione Piemonte")
        self.assertEqual(m.role, "Presidente di regione")
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_reg_existing(self):
        """Test import from MinInt, yearly summary, regione context, existing person and membership
        """

        # create person with membership
        person = PersonFactory.create(
            family_name="Baccega",
            given_name="Mauro",
            gender="M",
            birth_date="1955-08-15",
            birth_location="Aosta (AO)",
        )

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Regionali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"denominazione_regione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";"
            "\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"02\";\"VALLE D'AOSTA\";\"BACCEGA\";\"MAURO\";\"M\";\"15/08/1955\";\"AOSTA (AO)\";\"Consigliere\";;"
            "\"20/09/2020\";\"20/10/2020\";;\"Istruzione Secondaria di Primo Grado\";"
            "\"PROFESSIONI NON ALTROVE CLASSIFICABILI\"\r\n"
        ).encode("utf8")

        # mock solr select responses (found exact match)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 1,
                    "start": 0,
                    "docs": [
                        {
                            "id": "popolo.person.{0}".format(person.id),
                            "django_ct": "popolo.person",
                            "django_id": "{0}".format(person.id),
                            "family_name": "baccega",
                            "given_name": "mauro",
                            "birth_date": "1955-08-15T00:00:00Z",
                            "birth_date_s": "19550815",
                            "birth_location": "aosta (ao)",
                            "birth_location_area": 1200,
                            "OP_ID_s": "16431",
                            "CF_s": "BCCMRA55M15A326X",
                            "score": 0.0,
                        }
                    ],
                },
            }
        )
        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Valle d'Aosta/Vallée d'Aoste")
        area.add_identifier(scheme="ISTAT_CODE_REG", identifier="02")
        reg = OrganizationFactory(
            name="REGIONE AUTONOMA VALLE D'AOSTA", classification="Regione", area=area
        )
        org = OrganizationFactory.create(
            name="CONSIGLIO REGIONALE DELLA VALLE D'AOSTA",
            classification="Consiglio regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio regionale")
        RoleTypeFactory.create(
            label="Consigliere regionale", priority=1, classification=classification
        )
        post = PostFactory.create(
            organization=org,
            label="Consigliere regionale della Valle D'Aosta",
            role="Consigliere regionale",
        )

        person.add_role(
            post=post,
            start_date="2018-06-26",
            end_date="2020-09-20",
            label=post.label,
            role=post.role
        )
        person.add_role(
            post=post,
            start_date="2013-07-21",
            end_date="2018-06-26",
            label=post.label,
            role=post.role,
        )

        loader = PopoloPersonMembershipLoader(
            context="reg",
            persons_update_strategy="keep_old",
            memberships_update_strategy="keep_old",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintReg2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        p = Person.objects.first()
        self.assertEqual(p.family_name, "Baccega")
        self.assertEqual(
            p.original_profession.name, "PROFESSIONI NON ALTROVE CLASSIFICABILI"
        )
        self.assertEqual(
            p.original_education_level.name,
            "Istruzione Secondaria di Primo Grado",
        )
        self.assertEqual(Membership.objects.count(), 3)
        self.assertEqual(Membership.objects.filter(end_date__isnull=False).count(), 2)
        self.assertEqual(
            Membership.objects.filter(
                sources__source__url__icontains="dait.interno.gov.it"
            ).count(),
            1,
        )
        self.assertEqual(Membership.objects.filter(start_date="2013-07-01").count(), 0)

    def test_etl_minint_reg_consigliere_altro(self):
        """Test import from MinInt, current, regione context,
        new consigliere with incarico specified gets the role in incarico
        """

        # create person with membership
        person = PersonFactory.create(
            family_name="MARGUERETTAZ",
            given_name="AURELIO",
            gender="M",
            birth_date="1963-08-22",
            birth_location="Aosta (AO)",
        )

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Regionali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"denominazione_regione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"02\";\"VALLE D'AOSTA\";\"MARGUERETTAZ\";\"AURELIO\";\"M\";\"22/08/1963\";\"AOSTA (AO)\";\"Consigliere\";"
            "\"Vicepresidente del consiglio\";\"20/09/2020\";\"20/10/2020\";;\"Laurea Magistrale\";"
            "\"COMMERCIALISTI E ASSIMILATI\"\r\n"
        ).encode("utf8")

        # mock solr select responses (found exact match)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 1,
                    "start": 0,
                    "docs": [
                        {
                            "id": "popolo.person.{0}".format(person.id),
                            "django_ct": "popolo.person",
                            "django_id": "{0}".format(person.id),
                            "family_name": "marguerettaz",
                            "given_name": "aurelio",
                            "birth_date": "1955-08-15T00:00:00Z",
                            "birth_date_s": "19550815",
                            "birth_location": "aosta (ao)",
                            "birth_location_area": 1200,
                            "OP_ID_s": "16431",
                            "CF_s": "BCCMRA55M15A326X",
                            "score": 0.0,
                        }
                    ],
                },
            }
        )
        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Valle d'Aosta")
        area.add_identifier(scheme="ISTAT_CODE_REG", identifier="02")
        reg = OrganizationFactory(
            name="REGIONE Valle d'Aosta", classification="Regione", area=area
        )
        _ = OrganizationFactory.create(
            name="CONSIGLIO REGIONALE DELLA VALLE D'AOSTA",
            classification="Consiglio regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(
            descr="Consiglio regionale",
            scheme='FORMA_GIURIDICA_OP',
            code='3210'

        )
        _ = RoleTypeFactory.create(
            label="Vicepresidente di consiglio regionale", priority=1, classification=classification
        )
        _ = RoleTypeFactory.create(
            label="Consigliere regionale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="reg",
            persons_update_strategy="keep_old",
            memberships_update_strategy="keep_old",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintReg2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Vicepresidente del consiglio regionale della Valle D'Aosta")

    def test_etl_minint_reg_consigliere_segretario(self):
        """Test import from MinInt, current, regione context,
        new consigliere with incarico specified gets the role in incarico
        """
        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Regionali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"denominazione_regione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";"
            "\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"02\";\"VALLE D'AOSTA\";\"DISTORT\";\"LUCA\";\"M\";\"02/07/1964\";\"AOSTA (AO)\";\"Consigliere\";"
            "\"Segretario del consiglio\";\"20/09/2020\";\"20/10/2020\";;;\r\n"
        ).encode("utf8")

        # mock solr select responses (found exact match)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 0,
                    "start": 0,
                    "docs": [],
                },
            }
        )
        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(name="Valle d'Aosta")
        area.add_identifier(scheme="ISTAT_CODE_REG", identifier="02")
        reg = OrganizationFactory(
            name="REGIONE Valle d'Aosta", classification="Regione", area=area
        )
        org = OrganizationFactory.create(
            name="CONSIGLIO REGIONALE DELLA VALLE D'AOSTA",
            classification="Consiglio regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(
            descr="Consiglio regionale",
            scheme='FORMA_GIURIDICA_OP',
            code='3210'

        )
        vp_role = RoleTypeFactory.create(
            label="Consigliere regionale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="reg",
            persons_update_strategy="keep_old",
            memberships_update_strategy="keep_old",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintReg2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)

        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Consigliere regionale della Valle D'Aosta - Segretario del consiglio")

    def test_etl_minint_com_new(self):
        """Test import from MinInt, yearly summary, comune context, membership in giunta and consiglio
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Comunali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"codice_provincia\";\"codice_comune\";\"denominazione_comune\";\"sigla_provincia\";"
            "\"popolazione_censita_alla_data_elezione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";"
            "\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"002\";\"0010\";\"ACQUI TERME\";\"AL\";\"20054\";\"RAPETTI\";\"DANILO\";"
            "\"M\";\"05/03/1971\";\"ACQUI TERME (AL)\";"
            "\"Sindaco\";;\"12/06/2022\";\"26/06/2022\";"
            "\"CON BERTERO PER RAPETTI SINDACO | DANILO RAPETTI SINDACO ORGOGLIO ACQUESE |"
            " OBIETTIVO COMUNE RAPETTI SINDACO | PER ACQUI VORREI DANILO RAPETTI SINDACO\";\"Laurea Magistrale\";"
            "\"IMPRENDITORI TITOLARI E AMMIN. DELEGATI DI IMPRESE COMMERCIALI\"\r\n"
        ).encode("utf8")

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )
        prov = AreaFactory.create(
            name="Alessandria", identifier="AL", istat_classification="PROV"
        )
        area = AreaFactory.create(
            name="Acqui Terme", parent=prov, istat_classification="COM"
        )
        area.add_identifier(scheme="ISTAT_CODE_COM", identifier="0020010")
        reg = OrganizationFactory(
            name="COMUNE DI ACQUI TERME", classification="Comune", area=area
        )
        OrganizationFactory.create(
            name="Giunta comunale di Acqui Terme",
            classification="Giunta comunale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta comunale")
        RoleTypeFactory.create(
            label="Sindaco", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammcom.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintCom2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)

        p = Person.objects.get(family_name="Rapetti")
        self.assertEqual(
            p.original_profession.name,
            "IMPRENDITORI TITOLARI E AMMIN. DELEGATI DI IMPRESE COMMERCIALI",
        )
        self.assertEqual(p.original_education_level.name, "Laurea Magistrale")
        m = p.memberships.first()
        self.assertEqual(m.label, "Sindaco di Acqui Terme")
        self.assertEqual(m.role, "Sindaco")
        self.assertEqual(m.organization.name, "Giunta comunale di Acqui Terme")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_com_existing(self):
        """Test import of an existing person, with an existing role from MinInt, yearly summary, comune context,
            no new membership is created
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Comunali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"codice_provincia\";\"codice_comune\";\"denominazione_comune\";\"sigla_provincia\";"
            "\"popolazione_censita_alla_data_elezione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";"
            "\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"002\";\"0010\";\"ACQUI TERME\";\"AL\";\"20054\";\"RAPETTI\";\"DANILO\";"
            "\"M\";\"05/03/1971\";\"ACQUI TERME (AL)\";"
            "\"Sindaco\";;\"12/06/2022\";\"26/06/2022\";"
            "\"CON BERTERO PER RAPETTI SINDACO | DANILO RAPETTI SINDACO ORGOGLIO ACQUESE |"
            " OBIETTIVO COMUNE RAPETTI SINDACO | PER ACQUI VORREI DANILO RAPETTI SINDACO\";\"Laurea Magistrale\";"
            "\"IMPRENDITORI TITOLARI E AMMIN. DELEGATI DI IMPRESE COMMERCIALI\"\r\n"
        ).encode("utf8")

        person = PersonFactory.create(
            family_name="Rapetti",
            given_name="Danilo",
            gender="M",
            birth_date="1971-03-05",
            birth_location="Acqui Terme (AL)",
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 1,
                    "start": 0,
                    "docs": [
                        {
                            "id": "popolo.person.{0}".format(person.id),
                            "django_ct": "popolo.person",
                            "django_id": "{0}".format(person.id),
                            "family_name": "rapetti",
                            "given_name": "danilo",
                            "birth_date": "1971-03-05T00:00:00Z",
                            "birth_date_s": "19710305",
                            "birth_location": "acqui terme (al)",
                            "birth_location_area": 1200,
                            "OP_ID_s": "16431",
                            "CF_s": "RPTDNL71C05A513F",
                            "score": 0.0,
                        }
                    ],
                },
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        prov = AreaFactory.create(
            name="Alessandria", identifier="AL", istat_classification="PROV"
        )
        area = AreaFactory.create(
            name="Acqui Terme", parent=prov, istat_classification="COM"
        )
        area.add_identifier(scheme="ISTAT_CODE_COM", identifier="006001")
        reg = OrganizationFactory(
            name="COMUNE DI ACQUI TERME", classification="Comune", area=area
        )
        org = OrganizationFactory.create(
            name="Giunta comunale di Acqui Terme",
            classification="Giunta comunale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta comunale")
        sin_role = RoleTypeFactory.create(
            label="Sindaco", priority=1, classification=classification
        )
        sin_post = PostFactory.create(
            organization=org,
            role_type=sin_role,
            label="Sindaco di Acqui Terme",
            role=sin_role.label,
        )

        person.add_role(
            post=sin_post, start_date="2022-06-22", label=sin_post.label, role=sin_post.role
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammcom.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintCom2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)

        p = Person.objects.get(family_name="Rapetti")
        self.assertEqual(
            p.original_profession.name,
            "IMPRENDITORI TITOLARI E AMMIN. DELEGATI DI IMPRESE COMMERCIALI",
        )
        self.assertEqual(p.original_education_level.name, "Laurea Magistrale")
        m = p.memberships.first()
        self.assertEqual(m.label, "Sindaco di Acqui Terme")
        self.assertEqual(m.role, "Sindaco")
        self.assertEqual(m.organization.name, "Giunta comunale di Acqui Terme")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_com_import_assessore_on_existing_vicepresidente_closed(self):
        """Test risoluzione bug

        Inserisce l'incarico di assessore anche se la persona ha la carica di vicepresidente con stessa start_date
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Regionali - elenco completo Italia\r\n"
            "Aggiornato al 03/06/2024\r\n"
            "\"codice_regione\";\"denominazione_regione\";"
            "\"cognome\";\"nome\";\"sesso\";\"data_nascita\";\"luogo_nascita\";"
            "\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"PIEMONTE\";"
            "\"CAROSSO\";\"FABIO\";\"M\";\"06/09/1972\";\"ASTI (AT)\";"
            "\"Assessore\";;\"26/05/2019\";\"14/06/2019\""
            ";;\"Istruzione Secondaria di Secondo Grado\";"
            "\"Imprenditori, amministratori, direttori di aziende private di grande o media dimensione\"\r\n"
        ).encode("utf8")

        person = PersonFactory.create(
            family_name="Carosso",
            given_name="Fabio",
            gender="M",
            birth_date="1972-09-06",
            birth_location="Asti (AT)",
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {
                    "numFound": 1,
                    "start": 0,
                    "docs": [
                        {
                            "id": "popolo.person.{0}".format(person.id),
                            "django_ct": "popolo.person",
                            "django_id": "{0}".format(person.id),
                            "family_name": "carosso",
                            "given_name": "fabio",
                            "birth_date": "1972-09-06T00:00:00Z",
                            "birth_date_s": "19720906",
                            "birth_location": "torino (TO)",
                            "birth_location_area": 1200,
                            "OP_ID_s": "16431",
                            "CF_s": "RPTDNL71C05A513F",
                            "score": 0.0,
                        }
                    ],
                },
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(
            name="Piemonte", istat_classification="REG", classification="ADM1", identifier="01"
        )
        area.add_identifier(scheme="ISTAT_CODE_REG", identifier="01")
        reg = OrganizationFactory(
            name="Piemonte", classification="Regione", area=area
        )
        org = OrganizationFactory.create(
            name="Giunta regionale del Piemonte",
            classification="Giunta regionale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta regionale")
        vp_role = RoleTypeFactory.create(
            label="Vicepresidente di Regione", priority=2, classification=classification
        )
        vp_post = PostFactory.create(
            organization=org,
            role_type=vp_role,
            label="Vicepresidente della Regione Piemonte",
            start_date=None, end_date=None,
            role=vp_role.label,
        )
        assessore_role = RoleTypeFactory.create(
            label="Assessore regionale", priority=1, classification=classification
        )
        assessore_post = PostFactory.create(
            organization=org,
            role_type=assessore_role,
            label="Assessore regionale del Piemonte",
            start_date=None, end_date=None,
            role=assessore_role.label,
        )

        # aggiunta di una membership di vicepresidente
        person.add_role(
            post=vp_post, start_date="2019-06-14", label=vp_post.label, role=vp_post.role
        )

        # caricamento dati (una membership di assessore)
        loader = PopoloPersonMembershipLoader(
            context="reg",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintReg2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)

        p = Person.objects.get(family_name="Carosso")
        self.assertEqual(
            p.original_profession.name,
            "Imprenditori, amministratori, direttori di aziende private di grande o media dimensione",
        )
        self.assertEqual(p.original_education_level.name, "Istruzione Secondaria di Secondo Grado")
        m = p.memberships.first()
        self.assertEqual(m.label, "Vicepresidente della Regione Piemonte")
        self.assertEqual(m.role, "Vicepresidente di Regione")
        self.assertEqual(m.organization.name, "Giunta regionale del Piemonte")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_com_consigliere_candidato_sindaco(self):
        """Test import from MinInt, yearly summary, comune context,
        consigliere candidato sindaco
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Comunali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"codice_provincia\";\"codice_comune\";\"denominazione_comune\";\"sigla_provincia\";"
            "\"popolazione_censita_alla_data_elezione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";"
            "\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"002\";\"0010\";\"ACQUI TERME\";\"AL\";\"20054\";\"BAROSIO\";\"BRUNO\";\"M\";\"30/06/1973\";"
            "\"VESIME (AT)\";\"Consigliere candidato sindaco\";;\"12/06/2022\";\"17/07/2022\";;\r\n"
        ).encode("utf8")

        # mock solr select responses (found exact match)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )
        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        prov = AreaFactory.create(
            name="Alessandria", identifier="AL", istat_classification="PROV"
        )
        area = AreaFactory.create(
            name="Acqui Terme", parent=prov, istat_classification="COM"
        )
        area.add_identifier(scheme="ISTAT_CODE_COM", identifier="002001")
        com = OrganizationFactory(
            name="COMUNE DI ACQUI TERME", classification="Comune", area=area
        )
        org = OrganizationFactory.create(
            name="Consiglio comunale di Acqui Terme",
            classification="Consiglio comunale",
            parent=com,
        )
        classification = ClassificationFactory.create(descr="Consiglio comunale")
        RoleTypeFactory.create(
            label="Consigliere comunale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintCom2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Consigliere candidato sindaco di Acqui Terme")
        self.assertEqual(m.role, "Consigliere comunale")
        self.assertEqual(m.organization.name, "Consiglio comunale di Acqui Terme")
        self.assertEqual(m.role, m.post.role)

    def test_etl_minint_com_consigliere_con_deleghe(self):
        """Test import from MinInt, yearly summary, comune context,
        consigliere candidato sindaco
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Comunali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"codice_provincia\";\"codice_comune\";\"denominazione_comune\";\"sigla_provincia\";"
            "\"popolazione_censita_alla_data_elezione\";\"cognome\";\"nome\";\"sesso\";"
            "\"data_nascita\";\"luogo_nascita\";"
            "\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";"
            "\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"002\";\"0110\";\"BALZOLA\";\"AL\";\"1420\";\"GRAZIOTTO\";\"LUCA\";\"M\";"
            "\"23/03/1970\";\"CASALE MONFERRATO (AL)\";"
            "\"Consigliere\";\"Delega funzioni da parte del Sindaco - Vicesindaco\";\"26/05/2019\";\"27/05/2019\";"
            "\"FUTURA BALZOLA\";\"Istruzione Secondaria di Secondo Grado\";\"\"\r\n"
        ).encode("utf8")

        # mock solr select responses (found exact match)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )
        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        prov = AreaFactory.create(
            name="Alessandria", identifier="AL", istat_classification="PROV"
        )
        area = AreaFactory.create(
            name="Balzola", parent=prov, istat_classification="COM"
        )
        area.add_identifier(scheme="ISTAT_CODE_COM", identifier="0020110")
        com = OrganizationFactory(
            name="COMUNE DI BALZOLA", classification="Comune", area=area
        )
        org = OrganizationFactory.create(
            name="Consiglio comunale di Balzola",
            classification="Consiglio comunale",
            parent=com,
        )
        classification = ClassificationFactory.create(descr="Consiglio comunale")
        RoleTypeFactory.create(
            label="Consigliere comunale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintCom2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Consigliere comunale di Balzola - Delega funzioni da parte del Sindaco - Vicesindaco")
        self.assertEqual(m.role, "Consigliere comunale")
        self.assertEqual(m.organization.name, "Consiglio comunale di Balzola")
        self.assertEqual(m.role, m.post.role)

    def test_etl_minint_com_vicesindaco_reggente(self):
        """Test import from MinInt, yearly summary, comune context,
        vicesindaco reggente, supplente, ...
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Comunali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"codice_provincia\";\"codice_comune\";\"denominazione_comune\";\"sigla_provincia\";"
            "\"popolazione_censita_alla_data_elezione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";"
            "\"luogo_nascita\";"
            "\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";"
            "\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"096\";\"0250\";\"DORZANO\";\"BI\";\"508\";\"GRAZIAN\";\"GIULIANO\";\"M\";\"01/09/1957\";"
            "\"SANLURI (CA)\";"
            "\"Assessore\";\"Vicesindaco reggente\";\"03/10/2021\";\"03/02/2023\";;"
            "\"Istruzione Secondaria di Primo Grado\";"
            "\"Pensionati e persone ritirate dal lavoro\"\r\n"
        ).encode("utf8")

        # mock solr select responses (found exact match)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )
        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        prov = AreaFactory.create(
            name="Biella", identifier="BI", istat_classification="PROV"
        )
        area = AreaFactory.create(
            name="Dorzano", parent=prov, istat_classification="COM"
        )
        area.add_identifier(scheme="ISTAT_CODE_COM", identifier="0960250")
        com = OrganizationFactory(
            name="COMUNE DI DORZANO", classification="Comune", area=area
        )
        org = OrganizationFactory.create(
            name="Giunta comunale di Dorzano",
            classification="Giunta comunale",
            parent=com,
        )
        classification = ClassificationFactory.create(descr="Giunta comunale")
        RoleTypeFactory.create(
            label="Vicesindaco", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintCom2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Vicesindaco reggente di Dorzano")
        self.assertEqual(m.role, "Vicesindaco")
        self.assertEqual(m.organization.name, "Giunta comunale di Dorzano")
        self.assertEqual(m.role, m.post.role)

    def test_etl_minint_com_assessore_non_elettivo(self):
        """Test import from MinInt, yearly summary, comune context,
        assessore di origine non elettiva
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Comunali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"codice_provincia\";\"codice_comune\";\"denominazione_comune\";\"sigla_provincia\";"
            "\"popolazione_censita_alla_data_elezione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";"
            "\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"002\";\"0290\";\"CAPRIATA D'ORBA\";\"AL\";\"1926\";\"ARATA\";\"GIAMPIETRO\";\"M\";\"05/09/1967\";"
            "\"ALESSANDRIA (AL)\";\"Assessore non di origine elettiva\";;\"26/05/2019\";\"28/05/2019\";;"
            "\"Istruzione Secondaria di "
            "Secondo Grado\";"
            "\"IMPIEGATI DI AMMINISTRAZIONI, ORGANI ED ENTI PUBBLICI (NON RICOMPRESI IN ALTRE SPECIFICHE "
            "CLASSIFICAZIONI)\"\r\n"
        ).encode("utf8")

        # mock solr select responses (found exact match)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )
        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        prov = AreaFactory.create(
            name="Alessandria", identifier="AL", istat_classification="PROV"
        )
        area = AreaFactory.create(
            name="Capriata d'Orba", parent=prov, istat_classification="COM"
        )
        area.add_identifier(scheme="ISTAT_CODE_COM", identifier="0020290")
        com = OrganizationFactory(
            name="COMUNE DI CAPRIATA D'ORBA", classification="Comune", area=area
        )
        org = OrganizationFactory.create(
            name="Giunta comunale di Capriata D'Orba",
            classification="Giunta comunale",
            parent=com,
        )
        classification = ClassificationFactory.create(descr="Giunta comunale")
        RoleTypeFactory.create(
            label="Assessore comunale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammreg.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintCom2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)
        m = Membership.objects.first()
        self.assertEqual(m.label, "Assessore non di origine elettiva di Capriata D'Orba")
        self.assertEqual(m.role, "Assessore comunale")
        self.assertEqual(m.organization.name, "Giunta comunale di Capriata D'Orba")
        self.assertEqual(m.role, m.post.role)

    def test_etl_minint_provincia(self):
        """Test import from MinInt, yearly summary, provincia context
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"Amministratori Provinciali - elenco completo Italia\r\n"
            b"aggiornato al 4/6/2018\r\n"
            b"codice_regione;codice_provincia;denominazione_provincia;sigla_provincia;"
            b"titolo_accademico;cognome;nome;sesso;data_nascita;luogo_nascita;"
            b"descrizione_carica;data_elezione;data_elezione_max_carica;"
            b"data_entrata_in_carica;partito;titolo_studio;professione\r\n"
            b"01;002;ALESSANDRIA;AL;;BALDI;GIANFRANCO;M;10/08/1962;ACQUI TERME (AL);"
            b"Presidente della provincia;18/12/2016;24/09/2017;25/09/2017;;"
            b"LICENZA DI SCUOLA MEDIA SUP. O TITOLI EQUIPOLLENTI;"
            b"COMMERCIANTI E ESERCENTI DI NEGOZIO\r\n"
            b"01;002;ALESSANDRIA;AL;PROF;BUSCAGLIA;CARLO;M;17/07/1949;DERNICE (AL);"
            b"Consigliere;18/12/2016;12/10/2014;19/12/2016;"
            b"LS METROPOLITANA O PROV.LE SECONDO GRADO | UNITI PER LA PROVINCIA DI ALESSANDRIA E IL SUO TERRITORIO;"
            b"LAUREA;PRESIDI, CAPI DI ISTITUTI DI ISTRUZIONE E DIRETTORI DIDATTICI\r\n"
        )

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": '(family_name:("baldi") AND given_name:("gianfranco") '
                        'AND birth_date_s:("19620810") '
                        'AND birth_location:("acqui terme (al)"))',
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(
            name="Alessandria", istat_classification="PROV", identifier="AL"
        )
        area.add_identifier(scheme="ISTAT_CODE_PROV", identifier="006")
        reg = OrganizationFactory(
            name="AMMINISTRAZIONE PROVINCIALE DI ALESSANDRIA",
            classification="Provincia",
            area=area,
        )
        OrganizationFactory.create(
            name="Giunta provinciale di Alessandria",
            classification="Giunta provinciale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta provinciale")
        RoleTypeFactory.create(
            label="Presidente di provincia", priority=1, classification=classification
        )

        OrganizationFactory.create(
            name="Consiglio provinciale di Alessandria",
            classification="Consiglio provinciale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio provinciale")
        RoleTypeFactory.create(
            label="Consigliere provinciale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="prov",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammprov.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintProv2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 2)
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Membership.objects.count(), 2)

        p = Person.objects.get(family_name="Baldi")
        self.assertEqual(
            p.original_profession.name, "COMMERCIANTI E ESERCENTI DI NEGOZIO"
        )
        self.assertEqual(
            p.original_education_level.name,
            "LICENZA DI SCUOLA MEDIA SUP. O TITOLI EQUIPOLLENTI",
        )
        m = p.memberships.first()
        self.assertEqual(m.label, "Presidente della Provincia di Alessandria")
        self.assertEqual(m.role, "Presidente di provincia")
        self.assertEqual(m.organization.name, "Giunta provinciale di Alessandria")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

        p = Person.objects.get(family_name="Buscaglia")
        self.assertEqual(
            p.original_profession.name,
            "PRESIDI, CAPI DI ISTITUTI DI ISTRUZIONE E DIRETTORI DIDATTICI",
        )
        self.assertEqual(p.original_education_level.name, "LAUREA")
        m = p.memberships.first()
        self.assertEqual(m.label, "Consigliere provinciale di Alessandria")
        self.assertEqual(m.role, "Consigliere provinciale")
        self.assertEqual(m.organization.name, "Consiglio provinciale di Alessandria")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_metro(self):
        """Test import from MinInt, yearly summary, città metropolitana context, membership in giunta and consiglio
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            b"Amministratori Metropolitani - elenco completo Italia\r\n"
            b"aggiornato al 4/7/2018\r\n"
            b"codice_regione;codice_provincia;denominazione_provincia;sigla_provincia;"
            b"titolo_accademico;cognome;nome;sesso;data_nascita;luogo_nascita;descrizione_carica;"
            b"data_elezione;data_elezione_max_carica;data_entrata_in_carica;partito;titolo_studio;professione\r\n"
            b"01;081;TORINO;TO;DOTT;APPENDINO;CHIARA;F;12/06/1984;MONCALIERI (TO);"
            b"Sindaco metropolitano;09/10/2016;05/06/2016;30/06/2016;;"
            b"LAUREA;Impiegati di aziende, imprese, enti economici e soggetti di "
            b"diritto privato con qualifiche non direttive\r\n"
            b"01;081;TORINO;TO;AVV;AVETTA;ALBERTO;M;17/12/1969;IVREA (TO);"
            b"Consigliere;09/10/2016;05/06/2016;10/10/2016;"
            b"LS METROPOLITANA O PROV.LE SECONDO GRADO | CITTA' DI CITTA' ;"
            b"LAUREA;AVVOCATI E PROCURATORI LEGALI\r\n"
        )
        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        area = AreaFactory.create(
            name="Torino", identifier="TO", istat_classification="CM"
        )
        area.add_identifier(scheme="ISTAT_CODE_PROV", identifier="001")
        reg = OrganizationFactory(
            name="CITTÀ METROPOLITANA DI TORINO",
            classification="Città metropolitana",
            area=area,
        )
        OrganizationFactory.create(
            name="Conferenza metropolitana di Torino",
            classification="Conferenza metropolitana",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Conferenza metropolitana")
        RoleTypeFactory.create(
            label="Sindaco metropolitano", priority=1, classification=classification
        )

        OrganizationFactory.create(
            name="Consiglio metropolitano di Torino",
            classification="Consiglio metropolitano",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Consiglio metropolitano")
        RoleTypeFactory.create(
            label="Consigliere metropolitano", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="metro",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/ammmetropolitani.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintMetro2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 2)
        self.assertEqual(Person.objects.count(), 2)
        self.assertEqual(Membership.objects.count(), 2)

        p = Person.objects.get(family_name="Appendino")
        self.assertEqual(
            p.original_profession.name,
            "Impiegati di aziende, imprese, enti economici e soggetti di diritto privato con "
            "qualifiche non direttive",
        )
        self.assertEqual(p.original_education_level.name, "LAUREA")
        m = p.memberships.first()
        self.assertEqual(m.label, "Sindaco metropolitano di Torino")
        self.assertEqual(m.role, "Sindaco metropolitano")
        self.assertEqual(m.organization.name, "Conferenza metropolitana di Torino")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

        p = Person.objects.get(family_name="Avetta")
        self.assertEqual(p.original_profession.name, "AVVOCATI E PROCURATORI LEGALI")
        self.assertEqual(p.original_education_level.name, "LAUREA")
        m = p.memberships.first()
        self.assertEqual(m.label, "Consigliere metropolitano di Torino")
        self.assertEqual(m.role, "Consigliere metropolitano")
        self.assertEqual(m.organization.name, "Consiglio metropolitano di Torino")
        self.assertEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)

    def test_etl_minint_commissario(self):
        """Test import from MinInt, yearly summary, comune context, membership for Commissario

        Membership label is kept (Commissario Prefettizio)
        Post label is normalized and aggiusted (Commissario della giunta comunale di ...)
        """

        # mock csv download
        self.mock_get.return_value.ok = True
        self.mock_get.return_value.status_code = 200
        self.mock_get.return_value.content = (
            "Amministratori Comunali - elenco completo Italia\r\n"
            "Aggiornato al 03/05/2024\r\n"
            "\"codice_regione\";\"codice_provincia\";\"codice_comune\";\"denominazione_comune\";\"sigla_provincia\";"
            "\"popolazione_censita_alla_data_elezione\";\"cognome\";\"nome\";\"sesso\";\"data_nascita\";"
            "\"luogo_nascita\";\"descrizione_carica\";\"incarico\";\"data_elezione\";\"data_entrata_in_carica\";"
            "\"lista_appartenenza/collegamento\";\"titolo_studio\";\"professione\"\r\n"
            "\"01\";\"052\";\"0070\";\"ARONA\";\"NO\";\"14195\";\"TERRIBILE\";\"ALFONSO\";;;;"
            "\"Commissario Straordinario\";;\"20/09/2020\";\"31/10/2023\";;;\r\n"
        ).encode("utf8")

        # mock solr select responses (not found)
        self.mock_sqs_select.return_value = json.dumps(
            {
                "responseHeader": {
                    "status": 0,
                    "QTime": 1,
                    "params": {
                        "q": "*:*",
                        "df": "text",
                        "spellcheck": "true",
                        "fl": "* score",
                        "start": "0",
                        "spellcheck.count": "1",
                        "fq": "django_ct:(popolo.person)",
                        "rows": "1",
                        "wt": "json",
                        "spellcheck.collate": "true",
                    },
                },
                "response": {"numFound": 0, "start": 0, "maxScore": 0.0, "docs": []},
            }
        )

        # mock solr update responses
        self.mock_sqs_update.return_value = json.dumps(
            {"responseHeader": {"status": 0, "QTime": 1, "params": {}}, "response": {}}
        )

        prov = AreaFactory.create(
            name="Novara", identifier="NO", istat_classification="PROV"
        )
        area = AreaFactory.create(
            name="Arona", parent=prov, istat_classification="COM"
        )
        reg = OrganizationFactory(
            name="COMUNE DI ARONA", classification="Comune", area=area
        )
        OrganizationFactory.create(
            name="Giunta comunale di Arona",
            classification="Giunta comunale",
            parent=reg,
        )
        classification = ClassificationFactory.create(descr="Giunta comunale")
        RoleTypeFactory.create(
            label="Commissario di giunta comunale", priority=1, classification=classification
        )

        loader = PopoloPersonMembershipLoader(
            context="com",
            persons_update_strategy="overwrite_minint_opdm",
            memberships_update_strategy="overwrite_minint_opdm",
        )
        loader.logger = logging.getLogger(__name__)

        etl = ETL(
            extractor=UACSVExtractor(
                url="https://dait.interno.gov.it/documenti/organistraordinariincarica.csv",
                sep=";",
                encoding="latin1",
                skiprows=2,
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=loader,
            transformation=CurrentMinintComm2OpdmTransformation(),
            log_level=0,
        )
        df = etl.etl().original_data
        self.assertIsInstance(df, pd.DataFrame)
        self.assertEqual(len(df), 1)
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(Membership.objects.count(), 1)

        p = Person.objects.get(family_name="Terribile")
        self.assertEqual(p.original_profession, None)
        self.assertEqual(p.original_education_level, None)
        m = p.memberships.first()
        self.assertEqual(m.label, "Commissario Straordinario di Arona")
        self.assertEqual(m.role, "Commissario di giunta comunale")
        self.assertEqual(m.start_date, "2023-10-31")
        self.assertEqual(m.organization.name, "Giunta comunale di Arona")
        self.assertNotEqual(m.label, m.post.label)
        self.assertEqual(m.role, m.post.role)
        self.assertEqual(m.organization, m.post.organization)
        self.assertEqual(m.sources.count(), 1)
        self.assertEqual("dait.interno.gov.it" in m.sources.first().source.url, True)
