# -*- coding: utf-8 -*-

"""
Implements tests specific to the DiffHelper class.
"""
import json
import os
import shutil

import django.test

from project.core.diff_utils import DiffHelper, DiffHelperException


class DiffHelperTest(django.test.TestCase):

    new_csv_content = (
        "area_istat_com_code,area,area_id,institution_id,tot\r\n"
        '3045,"Cavaglio, d\'Agogna",425,50746,4.0\r\n'
        "80060,Platì,7073,42138,6.0\r\n"
        "57071,Turania,5156,39738,4.0\r\n"
        "11322,Pescocostanzo,1341,46341,5.0\r\n"
    )

    old_csv_content = (
        "area_istat_com_code,area,area_id,institution_id,tot\r\n"
        "43002,Apiro,4898,45900,6.0\r\n"
        "17012,Barghe,2025,36158,6.0\r\n"
        '3045,"Cavaglio d\'Agogna",425,50746,6.0\r\n'
        "80060,Platì,7073,42138,6.0\r\n"
        "57071,Turania,5156,39738,4.0\r\n"
    )

    with open('project/tasks/tests/etl/files/json_test_new.json') as json_new:
        new_json_content = json.load(json_new)

    with open('project/tasks/tests/etl/files/json_test_old.json') as json_old:
        old_json_content = json.load(json_old)

    test_cache_path = "resources/data/cachetest"
    test_filename = "test"

    @classmethod
    def cleanUpCache(cls, rmdir=False):
        """Clean up cache folder content, may also remove dir

        Invoked after each test, to remove test files (sort of rollback, for files used in tests)

        If invoked at the end of the tests, then the folder is also removed.

        :param rmdir: whether to remove the folder or not
        :return:
        """
        folder = cls.test_cache_path

        # the folder path must contain 'cachetest', for security reasons
        if "cachetest" in folder and os.path.isdir(folder):
            for f in os.listdir(folder):
                f_path = os.path.join(folder, f)
                if os.path.isfile(f_path):
                    os.unlink(f_path)
            if rmdir:
                shutil.rmtree(folder)

    @classmethod
    def setUpClass(cls):
        super(DiffHelperTest, cls).setUpClass()
        if not os.path.isdir(cls.test_cache_path):
            os.mkdir(cls.test_cache_path)

    @classmethod
    def tearDownClass(cls):
        cls.cleanUpCache(rmdir=True)
        super(DiffHelperTest, cls).tearDownClass()

    def test_csv_no_new_file(self):
        """Test scenario where new file has not been created in cache"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename=self.test_filename
        ) as diff_helper:
            with self.assertRaises(DiffHelperException) as exc_ctx:
                diff_helper.extract_differences_csv(
                    encoding="utf8", sep=";", index_cols=(0,)
                )
            self.assertTrue(
                "The new file is not present in the cache directory"
                in exc_ctx.exception.args[0]
            )

    def test_csv_no_old_file(self):
        """Test scenario where cached file does not exist, and it's created"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename=self.test_filename
        ) as diff_helper:
            # create new_csv file in cache with new_content
            with open(diff_helper.new, "wb") as f:
                f.write(self.new_csv_content.encode("utf8"))

            diff_helper.extract_differences_csv(
                encoding="utf8", sep=",", index_cols=(0,)
            )

            # transform io into lists, to perform multiple tests on them
            adds_and_updates = list(diff_helper.adds_and_updates["io"].readlines())

            # io streams contain the correct number of lines
            self.assertEqual(
                diff_helper.adds_and_updates["n"] + 2,
                len(self.new_csv_content.split("\r\n")),
            )
            self.assertEqual(diff_helper.removes["n"], 0)

            # the content is passed along correctly
            self.assertTrue("80060,Platì,7073,42138,6.0\n" in adds_and_updates)

            # an empty old_csv file is created in the cache
            self.assertTrue(os.path.isfile(diff_helper.old))

        self.cleanUpCache()

    def test_csv_no_changes(self):
        """Test scenario where new content and cached file are identical"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename=self.test_filename
        ) as diff_helper:

            # create nnew and old csv files in cache with new_content
            with open(diff_helper.new, "wb") as f:
                f.write(self.new_csv_content.encode("utf8"))
            with open(diff_helper.old, "wb") as f:
                f.write(self.new_csv_content.encode("utf8"))

            diff_helper.extract_differences_csv(
                encoding="utf8", sep=",", index_cols=(0,)
            )

            # io streams contain the correct number of lines
            self.assertEqual(diff_helper.adds_and_updates["n"], 0)
            self.assertEqual(diff_helper.removes["n"], 0)

        self.cleanUpCache()

    def test_csv_changes(self):
        """Test scenario where new content and cached file are changed"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename=self.test_filename
        ) as diff_helper:

            # create new and old csv files in cache
            with open(diff_helper.new, "wb") as f:
                f.write(self.new_csv_content.encode("utf8"))
            with open(diff_helper.old, "wb") as f:
                f.write(self.old_csv_content.encode("utf8"))

            diff_helper.extract_differences_csv(
                encoding="utf8", sep=",", index_cols=(0,)
            )

            # io streams contain the correct number of lines
            self.assertEqual(diff_helper.adds_and_updates["n"], 2)
            self.assertEqual(diff_helper.removes["n"], 2)

        self.cleanUpCache()

    def test_csv_wrong_sep(self):
        """Test scenario where separator was specified incorrectly"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename=self.test_filename
        ) as diff_helper:

            # create old_csv file in cache with old_content
            # create new and old csv files in cache
            with open(diff_helper.new, "wb") as f:
                f.write(self.new_csv_content.encode("utf8"))
            with open(diff_helper.old, "wb") as f:
                f.write(self.old_csv_content.encode("utf8"))

            with self.assertRaises(DiffHelperException) as exc_ctx:
                diff_helper.extract_differences_csv(
                    encoding="utf8", sep=";", index_cols=(0,)
                )
            self.assertTrue(
                "It seems the CSV file separator is not" in exc_ctx.exception.args[0]
            )

        self.cleanUpCache()

    def test_json_no_key_getter(self):
        with self.assertRaises(DiffHelperException) as exc_ctx:
            DiffHelper(
                local_cache_path=self.test_cache_path, filename="{0}.json".format(self.test_filename),
                key_getter=None
            )
            self.assertTrue(
                "A key_getter function must be passed to identify JSON records uniquely"
                in exc_ctx.exception.args[0]
            )

    def test_json_no_new_file(self):
        """Test scenario where new file has not been created in cache"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename="{0}.json".format(self.test_filename),
            key_getter=lambda x: 1
        ) as diff_helper:
            with self.assertRaises(DiffHelperException) as exc_ctx:
                diff_helper.extract_differences()
            self.assertTrue(
                "The new file is not present in the cache directory"
                in exc_ctx.exception.args[0]
            )

    def test_json_no_old_file(self):
        """Test scenario where cached file does not exist, and it's created"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename="{0}.json".format(self.test_filename),
            key_getter=lambda x: (
                x['family_name'], x['given_name'],
                x.get('birth_date', None), x.get('gender', None), x.get('birth_location', None)
            )
        ) as diff_helper:
            # create new_json file in cache with new_content
            with open(diff_helper.new, "w") as f:
                f.write(json.dumps(self.new_json_content))

            diff_helper.extract_differences()

            # transform io into python data (lists or dicts),
            # to perform multiple tests on them
            adds_and_updates = json.load(diff_helper.adds_and_updates["io"])

            # io streams contain the correct number of lines
            self.assertEqual(
                diff_helper.adds_and_updates["n"],
                len(self.new_json_content),
            )
            self.assertEqual(diff_helper.adds_and_updates["n"], 3)
            self.assertEqual(diff_helper.removes["n"], 0)

            # the content is passed along correctly
            self.assertTrue("SALVINI" in [add['family_name'] for add in adds_and_updates])

            # an empty old_csv file is created in the cache
            self.assertTrue(os.path.isfile(diff_helper.old))

        self.cleanUpCache()

    def test_json_no_changes(self):
        """Test scenario where new content and cached file are identical"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename="{0}.json".format(self.test_filename),
            key_getter=lambda x: (
                x['family_name'], x['given_name'],
                x.get('birth_date', None), x.get('gender', None), x.get('birth_location', None)
            )
        ) as diff_helper:

            # create nnew and old csv files in cache with new_content
            with open(diff_helper.new, "w") as f:
                f.write(json.dumps(self.new_json_content))
            with open(diff_helper.old, "w") as f:
                f.write(json.dumps(self.new_json_content))

            diff_helper.extract_differences()

            # io streams contain the correct number of lines
            self.assertEqual(diff_helper.adds_and_updates["n"], 0)
            self.assertEqual(diff_helper.removes["n"], 0)

        self.cleanUpCache()

    def test_json_changes(self):
        """Test scenario where new content and cached file are changed"""
        with DiffHelper(
            local_cache_path=self.test_cache_path, filename="{0}.json".format(self.test_filename),
            key_getter=lambda x: (
                x['family_name'], x['given_name'],
                x.get('birth_date', None), x.get('gender', None), x.get('birth_location', None)
            )
        ) as diff_helper:

            # create new and old csv files in cache
            with open(diff_helper.new, "w") as f:
                f.write(json.dumps(self.new_json_content))
            with open(diff_helper.old, "w") as f:
                f.write(json.dumps(self.old_json_content))

            diff_helper.extract_differences()

            # io streams contain the correct number of lines
            self.assertEqual(diff_helper.adds_and_updates["n"], 2)
            self.assertEqual(diff_helper.removes["n"], 2)

        self.cleanUpCache()

    def test_unsupported_format(self):
        """Test scenario where an unsupported format diff was requested"""
        with DiffHelper(
            local_cache_path=self.test_cache_path,
            filename=self.test_filename,
            fmt="xml",
        ) as diff_helper:

            # create old_csv file in cache with old_content
            # create new and old csv files in cache
            with open(diff_helper.new, "wb") as f:
                f.write(self.new_csv_content.encode("utf8"))
            with open(diff_helper.old, "wb") as f:
                f.write(self.old_csv_content.encode("utf8"))

            with self.assertRaises(DiffHelperException) as exc_ctx:
                diff_helper.extract_differences(encoding="utf8")
            self.assertTrue(
                "Format {0} is not (yet) supported.".format(diff_helper.fmt)
                in exc_ctx.exception.args[0]
            )

        self.cleanUpCache()
