# -*- coding: utf-8 -*-

"""
Implements tests specific to importing from JSON files into OPDM.
"""
from datetime import timedelta
import json
import logging
import random

from ooetl import ETL
from popolo.models import (
    KeyEvent,
    ContactDetail,
    OtherName,
    Organization,
    Person,
    RoleType,
)
from popolo.tests.factories import (
    ElectoralEventFactory,
    OrganizationFactory,
    AreaFactory,
    ClassificationFactory,
    LegislatureEventFactory,
    IdentifierFactory,
    PostFactory,
    RoleTypeFactory,
    PersonFactory,
    MembershipFactory,
)

from project.akas.models import AKA
from project.api_v1.tests import faker
from project.api_v1.tests.test_aka import AKAFactory
from project.tasks.etl.extractors import JsonArrayExtractor, ListExtractor
from project.tasks.etl.loaders.keyevents import PopoloKeyEventLoader
from project.tasks.etl.loaders.organizations import PopoloOrgLoader
from project.tasks.etl.loaders.persons import PopoloPersonWithRelatedDetailsLoader
from project.tasks.etl.transformations.json2opdm import (
    JsonPersonsMemberships2OpdmTransformation,
)
from project.tasks.tests.etl import SolrETLTest, MockResponse


class Json2OpdmETLTest(SolrETLTest):
    existing_ev = None
    existing_o = None

    def setup_mock(self, json_resp, **kwargs):

        # default solr select response to not found
        sqs_select_mock = kwargs.get(
            "sqs_select_mock",
            json.dumps(
                {
                    "responseHeader": {
                        "status": 0,
                        "QTime": 1,
                        "params": {
                            "q": "OP_ID_s:(1234)",
                            "df": "text",
                            "spellcheck": "true",
                            "fl": "* score",
                            "start": "0",
                            "spellcheck.count": "1",
                            "fq": "django_ct:(popolo.person)",
                            "rows": "1",
                            "wt": "json",
                            "spellcheck.collate": "true",
                        },
                    },
                    "response": {
                        "numFound": 0,
                        "start": 0,
                        "maxScore": 0.0,
                        "docs": [],
                    },
                }
            ),
        )

        # default solr update result to ok
        sqs_update_mock = kwargs.get(
            "sqs_update_mock",
            json.dumps(
                {
                    "responseHeader": {"status": 0, "QTime": 1, "params": {}},
                    "response": {},
                }
            ),
        )

        # mock response from JSON
        self.mock_get.return_value = MockResponse(json_resp, 200)

        # mock solr select response
        self.mock_sqs_select.return_value = sqs_select_mock

        # mock solr update response
        self.mock_sqs_update.return_value = sqs_update_mock

    def test_etl_legislatures_anagraphical_keep_old(self):
        """Test extraction of legislatures and loading into OPDM,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """
        json_resp = [
            {
                "name": "Legislatura 1",
                "event_type": "ITL",
                "identifier": "ITL_01",
                "start_date": "2000-03-01",
                "end_date": "2004-01-01",
            },
            {
                "name": "Elezioni politiche del 2000",
                "event_type": "ELE",
                "identifier": "ELE_01",
                "start_date": "2000-01-01",
                "end_date": "2010-01-01",
            },
        ]

        self.setup_mock(json_resp)

        # insert electoral event into DB,
        # to test lookup
        self.existing_ev = ElectoralEventFactory.create(
            name="Elezioni politiche del 2000",
            start_date="2000-01-01",
            end_date="2000-01-01",
            identifier=None,
        )

        loader = PopoloKeyEventLoader(
            lookup_strategy="anagraphical", update_strategy="keep_old"
        )
        loader.logger = logging.getLogger(__name__)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/legislatures.json"
            ),
            loader=loader,
            log_level=0,
        )()

        self.assertEqual(KeyEvent.objects.count(), 2)
        e = KeyEvent.objects.get(id=self.existing_ev.id)

        # end_date was not written, as it already existed
        self.assertEqual(e.end_date, self.existing_ev.end_date)

        # identifier was written, as it was null
        self.assertNotEqual(e.identifier, self.existing_ev.identifier)

    def test_etl_legislatures_identifier_keep_old(self):
        """Test import of legislatures and loading into OPDM,
           lookup_strategy: identifier
           update_strategy: keep_old
        """

        json_resp = [
            {
                "name": "Legislatura 1",
                "event_type": "ITL",
                "identifier": "ITL_01",
                "start_date": "2000-03-01",
                "end_date": "2004-01-01",
            },
            {
                "name": "Elezioni politiche del 2000",
                "event_type": "ELE",
                "identifier": "ELE_01",
                "start_date": "2000-01-01",
                "end_date": "2010-01-01",
            },
        ]

        self.setup_mock(json_resp)

        # insert electoral event into DB,
        # to test lookup
        self.existing_ev = ElectoralEventFactory.create(
            name="Elezioni politiche del 2000",
            start_date="2000-01-01",
            end_date="2000-01-01",
            identifier="ELE_01",
        )

        loader = PopoloKeyEventLoader(
            lookup_strategy="identifier", update_strategy="keep_old"
        )
        loader.logger = logging.getLogger(__name__)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/legislatures.json"
            ),
            loader=loader,
            log_level=0,
        )()

        self.assertEqual(KeyEvent.objects.count(), 2)
        e = KeyEvent.objects.get(id=self.existing_ev.id)

        # end_date was not written, as it already existed
        self.assertEqual(e.end_date, self.existing_ev.end_date)

    def test_etl_legislatures_anagraphical_overwrite(self):
        """Test import of legislatures and loading into OPDM,
           lookup_strategy: anagraphical
           update_strategy: overwrite
        """
        json_resp = [
            {
                "name": "Legislatura 1",
                "event_type": "ITL",
                "identifier": "ITL_01",
                "start_date": "2000-03-01",
                "end_date": "2004-01-01",
            },
            {
                "name": "Elezioni politiche del 2000",
                "event_type": "ELE",
                "identifier": "ELE_01",
                "start_date": "2000-01-01",
                "end_date": "2010-01-01",
            },
        ]

        self.setup_mock(json_resp)

        # insert electoral event into DB,
        # to test lookup
        self.existing_ev = ElectoralEventFactory.create(
            name="Elezioni politiche del 2000",
            start_date="2000-01-01",
            end_date="2000-01-01",
            identifier=None,
        )

        loader = PopoloKeyEventLoader(
            lookup_strategy="anagraphical", update_strategy="overwrite"
        )
        loader.logger = logging.getLogger(__name__)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/legislatures.json"
            ),
            loader=loader,
            log_level=0,
        )()

        self.assertEqual(KeyEvent.objects.count(), 2)
        e = KeyEvent.objects.get(id=self.existing_ev.id)

        # end_date was overwritten
        self.assertNotEqual(e.end_date, self.existing_ev.end_date)

        # identifier was overwritten, as it was null
        self.assertNotEqual(e.identifier, self.existing_ev.identifier)

    def test_etl_legislatures_identifier_overwrite(self):
        """Test import of legislatures and loading into OPDM,
           lookup_strategy: identifier
           update_strategy: overwrite
        """
        json_resp = [
            {
                "name": "Legislatura 1",
                "event_type": "ITL",
                "identifier": "ITL_01",
                "start_date": "2000-03-01",
                "end_date": "2004-01-01",
            },
            {
                "name": "Elezioni politiche del 2000",
                "event_type": "ELE",
                "identifier": "ELE_01",
                "start_date": "2000-01-01",
                "end_date": "2010-01-01",
            },
        ]

        self.setup_mock(json_resp)

        # insert electoral event into DB,
        # to test lookup
        self.existing_ev = ElectoralEventFactory.create(
            name="Elezioni politiche del 2000",
            start_date="2000-01-01",
            end_date="2000-01-01",
            identifier="ELE_01",
        )

        loader = PopoloKeyEventLoader(
            lookup_strategy="identifier", update_strategy="overwrite"
        )
        loader.logger = logging.getLogger(__name__)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/legislatures.json"
            ),
            loader=loader,
            log_level=0,
        )()

        self.assertEqual(KeyEvent.objects.count(), 2)
        e = KeyEvent.objects.get(id=self.existing_ev.id)

        # end_date was overwritten
        self.assertNotEqual(e.end_date, self.existing_ev.end_date)

    @staticmethod
    def get_organization_json_resp(existing_org: dict) -> list:
        """return a json_resp data structure with a list of 2 organizations,

        the first organization share some values with existing_org,
        in order for update_strategies to be tested

        :return: dict
        """

        cl = {'scheme': 'OP_CONTEXT', 'descr': 'TESTING'}

        classifications = [
            {"classification": ClassificationFactory().id},
            {"classification": ClassificationFactory().id},
            cl
        ]

        identifier_a = IdentifierFactory.create()
        identifier_b = IdentifierFactory.create()
        identifiers = [
            {
                "scheme": identifier_a.scheme,
                "identifier": identifier_a.identifier,
                "source": identifier_a.source,
            },
            {
                "scheme": identifier_b.scheme,
                "identifier": identifier_b.identifier,
                "source": identifier_b.source,
            },
        ]

        contact_details = [
            {
                "label": " ".join(faker.words()),
                "contact_type": random.choice(
                    [a[0] for a in ContactDetail.CONTACT_TYPES]
                ),
                "note": faker.paragraph(2),
                "value": faker.pystr(max_chars=32),
            }
        ]

        other_names = [
            {
                "name": faker.pystr(max_chars=256),
                "othername_type": random.choice([a[0] for a in OtherName.NAME_TYPES]),
                "note": faker.paragraph(2),
                "source": faker.uri(),
            }
        ]

        sources = [{"note": faker.paragraph(2), "url": faker.uri()}]

        ev_1 = LegislatureEventFactory()
        ev_2 = ElectoralEventFactory()
        ev_3 = LegislatureEventFactory()
        ev_4 = ElectoralEventFactory()

        json_resp = [
            {
                "name": existing_org["name"],
                "identifier": existing_org["identifier"],
                "classification": "TEST",
                "image": existing_org["image_url"],
                "area": existing_org["area"].id,
                "parent": existing_org["parent"].id,
                "founding_date": existing_org["founding_date"],
                "dissolution_date": faker.date_between(
                    start_date="-2y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
                "key_events": [{"key_event": ev_1.id}, {"key_event": ev_2.id}],
                "contact_details": contact_details,
                "identifiers": identifiers,
                "classifications": classifications,
                "other_names": other_names,
                "sources": sources,
            },
            {
                "name": faker.company(),
                "identifier": str(faker.pyint()),
                "classification": "TEST",
                "image": faker.url(),
                "area": AreaFactory().id,
                "parent": OrganizationFactory().id,
                "founding_date": faker.date_between(
                    start_date="-10y", end_date="-2y"
                ).strftime("%Y-%m-%d"),
                "dissolution_date": None,
                "key_events": [{"key_event": ev_3.id}, {"key_event": ev_4.id}],
                "contact_details": contact_details,
                "identifiers": identifiers,
                "classifications": classifications,
                "other_names": other_names,
                "sources": sources,
            },
        ]

        return json_resp

    def test_etl_organizations_anagraphical_keep_old(self):
        """Test import of organizations and loading into OPDM,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """

        existing_org = {
            "name": faker.company(),
            "identifier": str(faker.pyint()),
            "founding_date": faker.date_between(
                start_date="-30y", end_date="-2y"
            ).strftime("%Y-%m-%d"),
            "image_url": faker.url(),
            "area": AreaFactory(),
            "parent": OrganizationFactory(),
        }

        json_resp = self.get_organization_json_resp(existing_org)
        self.setup_mock(json_resp)

        # insert organization into DB,
        # to test lookup
        self.existing_o = OrganizationFactory.create(
            name=existing_org["name"],
            identifier=existing_org["identifier"],
            classification="TEST",
            founding_date=faker.date_between(
                start_date="-30y", end_date="-2y"
            ).strftime("%Y-%m-%d"),
            dissolution_date=None,
        )

        loader = PopoloOrgLoader(
            lookup_strategy="anagraphical", update_strategy="keep_old"
        )
        loader.logger = logging.getLogger(__name__)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/organizations.json"
            ),
            loader=loader,
            log_level=0,
        )()

        # only one of the two organizations in json file was imported
        # total number of orgs must be 2
        self.assertEqual(Organization.objects.filter(classification="TEST").count(), 2)
        o = Organization.objects.get(id=self.existing_o.id)

        # end_date was written, as it did not exist
        self.assertNotEqual(o.dissolution_date, self.existing_o.dissolution_date)

        # area and parent, passed by ID, were correctly created
        self.assertEqual(o.area.name, existing_org["area"].name)
        self.assertEqual(o.parent.name, existing_org["parent"].name)

        self.assertEqual(o.image, existing_org["image_url"])

        # classification was NOT written, as it was not null
        self.assertEqual(o.classification, self.existing_o.classification)

        # nested fields were imported correctly
        self.assertEqual(o.identifiers.count(), len(json_resp[0]["identifiers"]))
        self.assertEqual(
            o.contact_details.count(), len(json_resp[0]["contact_details"])
        )
        self.assertEqual(o.sources.count(), len(json_resp[0]["sources"]))
        self.assertEqual(
            o.classifications.count(), len(json_resp[0]["classifications"])
        )
        self.assertEqual(o.other_names.count(), len(json_resp[0]["other_names"]))
        self.assertEqual(o.key_events.count(), 2)

    def test_etl_load_organizations_same_other_cf_different_names(self):
        """Test import of organizations and loading into OPDM,
           when names are different and CF is among the ALTRI_CF_ATOKA identifiers
           lookup_strategy: mixed_curent
           update_strategy: keep_old_overwrite_cf
        """

        # insert organization into DB,
        # to test lookup
        o = OrganizationFactory.create(
            name="Ministero dell'Economia e delle Finanze",
            classification="TEST",
            founding_date=faker.date_between(
                start_date="-30y", end_date="-28y"
            ).strftime("%Y-%m-%d"),
            dissolution_date=None,
        )
        o.add_identifier(scheme='CF', identifier='80415740580')
        o.add_identifier(scheme='ALTRI_CF_ATOKA', identifier='80207790587,80226730580,80226750588,80228850584')
        o.add_source(url="http://bdap.it", note="BDAP")

        loader = PopoloOrgLoader(
            lookup_strategy="mixed_current", update_strategy="keep_old_overwrite_cf"
        )
        loader.logger = logging.getLogger(__name__)
        ETL(
            extractor=ListExtractor(
                [
                    {
                        "name": "Ministero Finanze",
                        "classification": "TEST",
                        "founding_date": None,
                        "dissolution_date": None,
                        "identifiers": [
                            {'scheme': 'CF', 'identifier': '80228850584'},

                        ],
                        "sources": [{"note": "ATOKA", "url": "http://api.atoka.io"}],
                    },
                ]
            ),
            loader=loader,
            log_level=0,
        )()

        # the loader recognizes the CF among the ALTRI_CF_ATOKA and does not create a new organization
        self.assertEqual(Organization.objects.filter(classification="TEST").count(), 1)

        # the source from the import has correctly been merged into the original org
        self.assertEqual(o.sources.count(), 2)

        # CFs in ALTRI_CF_ATOKA have been not touched
        altri_cf_atoka = o.identifiers.filter(scheme='ALTRI_CF_ATOKA').first().identifier.split(",")
        self.assertEqual(len(altri_cf_atoka), 4)

        # the original identifier has been changed (keep_old_overwrite_cf update_strategy)
        self.assertEqual(o.identifiers.filter(scheme='CF').first().identifier, '80228850584')

    def test_etl_load_public_organizations_same_name_different_cf_same_classification(self):
        """Test import of organizations and loading into OPDM,
           when names are the same and CFs are different
           Both orgs are public, and have the same classification

           lookup_strategy: mixed_curent
           update_strategy: keep_old_overwrite_cf
        """

        # insert organization into DB,
        # to test lookup

        classification = ClassificationFactory(
            scheme='FORMA_GIURIDICA_OP',
            code=2410,
            descr="Testing"
        )
        name = faker.company()
        main_identifier = faker.ssn()
        o = OrganizationFactory(
            name=name,
            classification=classification.descr,
            dissolution_date=None,
            identifier=main_identifier
        )
        o.add_classification(classification.scheme, code=classification.code)
        o.add_identifier(scheme='CF', identifier=faker.ssn())

        loader = PopoloOrgLoader(
            lookup_strategy="mixed_current", update_strategy="keep_old_overwrite_cf",
            private_company_verification=True
        )
        loader.logger = logging.getLogger(__name__)

        item_cf = faker.ssn()
        ETL(
            extractor=ListExtractor(
                [
                    {
                        "name": name,
                        "classification": classification.descr,
                        "founding_date": None,
                        "dissolution_date": None,
                        "identifier": item_cf,
                        "identifiers": [
                            {'scheme': 'CF', 'identifier': item_cf},

                        ],
                        "classifications": [{"classification": classification.id}],  # regione
                    },
                ]
            ),
            loader=loader,
            log_level=0,
        )()

        # the loader merges the organizations, as thy're public and have the same classification
        self.assertEqual(Organization.objects.filter(classification=classification.descr).count(), 1)

        # the original identifier has been changed (keep_old_overwrite_cf update_strategy)
        self.assertEqual(o.identifiers.filter(scheme='CF').first().identifier, item_cf)

    def test_etl_load_public_organizations_same_name_different_cf_different_classification(self):
        """Test import of organizations and loading into OPDM,
           when names are the same and CFs are different
           Both orgs are public, but have different classifications

           lookup_strategy: mixed_curent
           update_strategy: keep_old_overwrite_cf
        """

        # insert organization into DB,
        # to test lookup

        cl1 = ClassificationFactory(
            scheme='FORMA_GIURIDICA_OP',
            code=2410,
            descr="Testing1"
        )
        cl2 = ClassificationFactory(
            scheme='FORMA_GIURIDICA_OP',
            code=2420,
            descr="Testing2"
        )
        name = faker.company()
        main_identifier = faker.ssn()
        o = OrganizationFactory(
            name=name,
            classification=cl1.descr,
            dissolution_date=None,
            identifier=main_identifier
        )
        o.add_classification(cl1.scheme, code=cl1.code)
        o.add_identifier(scheme='CF', identifier=faker.ssn())

        loader = PopoloOrgLoader(
            lookup_strategy="mixed_current", update_strategy="keep_old_overwrite_cf",
            private_company_verification=True
        )
        loader.logger = logging.getLogger(__name__)

        item_cf = faker.ssn()
        ETL(
            extractor=ListExtractor(
                [
                    {
                        "name": name,
                        "classification": cl2.descr,
                        "founding_date": None,
                        "dissolution_date": None,
                        "identifier": item_cf,
                        "identifiers": [
                            {'scheme': 'CF', 'identifier': item_cf},

                        ],
                        "classifications": [{"classification": cl2.id}],  # regione
                    },
                ]
            ),
            loader=loader,
            log_level=0,
        )()

        # the loader does not merge the organizations, as they're public but have different classification
        self.assertEqual(Organization.objects.count(), 2)

    @staticmethod
    def get_person_memberships_json_resp() -> list:
        """return a json_resp data structure with a list of a person + memberships,

        :return: list
        """

        identifier_a = IdentifierFactory.create()
        identifier_b = IdentifierFactory.create()
        identifiers = [
            {
                "scheme": identifier_a.scheme,
                "identifier": identifier_a.identifier,
                "source": identifier_a.source,
            },
            {
                "scheme": identifier_b.scheme,
                "identifier": identifier_b.identifier,
                "source": identifier_b.source,
            },
        ]

        contact_details = [
            {
                "label": " ".join(faker.words()),
                "contact_type": random.choice(
                    [a[0] for a in ContactDetail.CONTACT_TYPES]
                ),
                "note": faker.paragraph(2),
                "value": faker.pystr(max_chars=32),
            }
        ]

        other_names = [
            {
                "name": faker.pystr(max_chars=256),
                "othername_type": random.choice([a[0] for a in OtherName.NAME_TYPES]),
                "note": faker.paragraph(2),
                "source": faker.uri(),
            }
        ]

        sources = [{"note": faker.paragraph(2), "url": faker.uri()}]

        classification_a = ClassificationFactory(
            scheme="FORMA_GIURIDICA_OP", descr="Assemblea parlamentare"
        )
        role_type_a = RoleTypeFactory(label="Deputato", classification=classification_a)
        election = ElectoralEventFactory(event_type="ELE-POL")
        organization_a = OrganizationFactory(classification=classification_a.descr)
        organization_a.add_classification_rel(classification_a)
        organization_a.add_key_event_rel(election)

        classification_b = ClassificationFactory(
            scheme="FORMA_GIURIDICA_OP", descr="Governo della Repubblica"
        )
        role_type_b = RoleTypeFactory(label="Ministro", classification=classification_b)
        organization_b = OrganizationFactory(classification=classification_b.descr)
        organization_b.add_classification_rel(classification_b.id)

        memberships = [
            {
                "organization_id": organization_a.id,
                "label": faker.sentence(nb_words=3),
                "role": role_type_a.label,
                "start_date": faker.date_between(
                    start_date="-3y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
                "end_date": faker.date_between(
                    start_date="-1y", end_date="-6m"
                ).strftime("%Y-%m-%d"),
            },
            {
                "organization_id": organization_b.id,
                "label": faker.sentence(nb_words=3),
                "role": role_type_b.label,
                "start_date": faker.date_between(
                    start_date="-2y", end_date="-1y"
                ).strftime("%Y-%m-%d"),
                "contact_details": [
                    {
                        "label": " ".join(faker.words()),
                        "contact_type": random.choice(
                            [a[0] for a in ContactDetail.CONTACT_TYPES]
                        ),
                        "note": faker.paragraph(2),
                        "value": faker.pystr(max_chars=32),
                    }
                ],
                "sources": [{"note": faker.paragraph(2), "url": faker.uri()}],
            },
        ]

        birth_parent_area = AreaFactory()
        birth_area = AreaFactory(parent=birth_parent_area)

        json_resp = [
            {
                "family_name": faker.last_name_male(),
                "given_name": faker.first_name_male(),
                "birth_date": faker.date_between(
                    start_date="-30y", end_date="-25y"
                ).strftime("%Y-%m-%d"),
                "birth_location": birth_area.name,
                "birth_location_area": birth_area,
                "image": faker.url(),
                "honorific_prefix": faker.prefix(),
                "gender": "M",
                "contact_details": contact_details,
                "identifiers": identifiers,
                "other_names": other_names,
                "sources": sources,
                "memberships": memberships,
            }
        ]

        return json_resp

    def test_etl_new_person_new_memberships(self):
        """Test import of non-existing person and membership,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """
        json_resp = self.get_person_memberships_json_resp()
        self.setup_mock(json_resp)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/incarichi_governo.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(context="governo"),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )()

        # total number of persons must be 1,
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(
            Person.objects.first().family_name, json_resp[0]["family_name"]
        )
        self.assertEqual(
            Person.objects.first().identifiers.count(), len(json_resp[0]["identifiers"])
        )

        # n of memberships for the person must be 2
        self.assertEqual(Person.objects.first().memberships.count(), 2)
        self.assertEqual(
            Person.objects.first()
            .memberships.filter(
                organization_id__in=Organization.objects.values_list("id", flat=True)
            )
            .count(),
            len(set(m["organization_id"] for m in json_resp[0]["memberships"])),
        )
        self.assertEqual(
            Person.objects.first()
            .memberships.filter(electoral_event=KeyEvent.objects.first())
            .count(),
            1,
        )
        self.assertEqual(
            Person.objects.first()
            .memberships.filter(contact_details__isnull=False)
            .count(),
            len(json_resp[0]["memberships"][1]["contact_details"]),
        )
        self.assertEqual(
            Person.objects.first().memberships.filter(sources__isnull=False).count(),
            len(json_resp[0]["memberships"][1]["sources"]),
        )

    def test_etl_existing_person_new_memberships(self):
        """Test import of existing person and new memberships,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """

        json_resp = self.get_person_memberships_json_resp()

        # create the person that is in the import snippet (test update)
        existing_p = PersonFactory(
            family_name=json_resp[0]["family_name"],
            given_name=json_resp[0]["given_name"],
            birth_date=json_resp[0]["birth_date"],
            birth_location=json_resp[0]["birth_location_area"].name,
            birth_location_area=json_resp[0]["birth_location_area"],
        )

        # setup mock responses with solr returning the only result
        self.setup_mock(
            json_resp,
            sqs_select_mock=json.dumps(
                {
                    "responseHeader": {
                        "status": 0,
                        "QTime": 1,
                        "params": {
                            "q": "family_name:{0} given_name:{1} birth_date_s:{2} birth_location_area:{3}".format(
                                json_resp[0]["family_name"],
                                json_resp[0]["given_name"],
                                json_resp[0]["birth_date"].replace("-", ""),
                                json_resp[0]["birth_location_area"].id,
                            ),
                            "df": "text",
                            "spellcheck": "true",
                            "fl": "* score",
                            "start": "0",
                            "spellcheck.count": "1",
                            "fq": "django_ct:(popolo.person)",
                            "rows": "10",
                            "wt": "json",
                            "spellcheck.collate": "true",
                        },
                    },
                    "response": {
                        "numFound": 1,
                        "start": 0,
                        "maxScore": 0.0,
                        "docs": [
                            {
                                "id": "popolo.person.{0}".format(existing_p.id),
                                "django_ct": "popolo.person",
                                "django_id": "{0}".format(existing_p.id),
                                "family_name": "{0}".format(existing_p.family_name),
                                "given_name": "{0}".format(existing_p.given_name),
                                "birth_date": "{0}".format(existing_p.birth_date),
                                "birth_date_s": "{0}".format(
                                    existing_p.birth_date
                                ).replace("-", ""),
                                "birth_location": "{0}".format(
                                    existing_p.birth_location_area.name
                                ),
                                "birth_location_area": existing_p.birth_location_area.id,
                                "CF_s": "GFFSNT54E13I600S",
                                "_version_": 1613292437310013440,
                                "score": 0.0,
                            }
                        ],
                    },
                }
            ),
        )

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/incarichi_governo.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(context="governo"),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )()

        # total number of persons must be 1,
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(
            Person.objects.first().family_name, json_resp[0]["family_name"]
        )
        self.assertEqual(Person.objects.first().image, json_resp[0]["image"])
        self.assertEqual(
            Person.objects.first().identifiers.count(), len(json_resp[0]["identifiers"])
        )

        # n of memberships for the person must be 2
        self.assertEqual(Person.objects.first().memberships.count(), 2)

    def test_etl_existing_person_existing_membership(self):
        """Test import of existing person and memberships,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """

        json_resp = self.get_person_memberships_json_resp()

        # create the person that is in the import snippet (test update)
        existing_p = PersonFactory(
            family_name=json_resp[0]["family_name"],
            given_name=json_resp[0]["given_name"],
            birth_date=json_resp[0]["birth_date"],
            birth_location=json_resp[0]["birth_location_area"].name,
            birth_location_area=json_resp[0]["birth_location_area"],
        )

        org = Organization.objects.get(
            id=json_resp[0]["memberships"][0]["organization_id"]
        )
        role_type = RoleType.objects.get(label="Deputato")
        post = PostFactory(
            organization=org,
            role_type=role_type,
            role=role_type.label,
            label=json_resp[0]["memberships"][0]["label"],
        )
        MembershipFactory.create(
            person=existing_p,
            organization=org,
            label=json_resp[0]["memberships"][0]["label"],
            post=post,
            start_date=json_resp[0]["memberships"][0]["start_date"],
            end_date=json_resp[0]["memberships"][0]["start_date"],
        )

        # setup mock responses with solr returning the only result
        self.setup_mock(
            json_resp,
            sqs_select_mock=json.dumps(
                {
                    "responseHeader": {
                        "status": 0,
                        "QTime": 1,
                        "params": {
                            "q": "family_name:{0} given_name:{1} birth_date_s:{2} birth_location_area:{3}".format(
                                json_resp[0]["family_name"],
                                json_resp[0]["given_name"],
                                json_resp[0]["birth_date"].replace("-", ""),
                                json_resp[0]["birth_location_area"].id,
                            ),
                            "df": "text",
                            "spellcheck": "true",
                            "fl": "* score",
                            "start": "0",
                            "spellcheck.count": "1",
                            "fq": "django_ct:(popolo.person)",
                            "rows": "10",
                            "wt": "json",
                            "spellcheck.collate": "true",
                        },
                    },
                    "response": {
                        "numFound": 1,
                        "start": 0,
                        "maxScore": 0.0,
                        "docs": [
                            {
                                "id": "popolo.person.{0}".format(existing_p.id),
                                "django_ct": "popolo.person",
                                "django_id": "{0}".format(existing_p.id),
                                "family_name": "{0}".format(existing_p.family_name),
                                "given_name": "{0}".format(existing_p.given_name),
                                "birth_date": "{0}".format(existing_p.birth_date),
                                "birth_date_s": "{0}".format(
                                    existing_p.birth_date
                                ).replace("-", ""),
                                "birth_location": "{0}".format(
                                    existing_p.birth_location_area.name
                                ),
                                "birth_location_area": existing_p.birth_location_area.id,
                                "CF_s": "GFFSNT54E13I600S",
                                "_version_": 1613292437310013440,
                                "score": 0.0,
                            }
                        ],
                    },
                }
            ),
        )

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/incarichi_governo.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(context="governo"),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )()

        # total number of persons must be 1, not 2
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(
            Person.objects.first().family_name, json_resp[0]["family_name"]
        )
        self.assertEqual(Person.objects.first().image, json_resp[0]["image"])
        self.assertEqual(
            Person.objects.first().identifiers.count(), len(json_resp[0]["identifiers"])
        )
        self.assertEqual(
            Person.objects.first().contact_details.count(),
            len(json_resp[0]["contact_details"]),
        )
        self.assertEqual(
            Person.objects.first().sources.count(), len(json_resp[0]["sources"])
        )

        # n of memberships for the person must be 2, not 3
        self.assertEqual(Person.objects.first().memberships.count(), 2)
        # end date must be updated in existing membership
        self.assertEqual(
            Person.objects.first().memberships.filter(end_date__isnull=False).count(), 1
        )

    def test_etl_existing_person_new_roles_same_post_different_labels_check_label_false(
        self
    ):
        """Test import of existing person with two new roles for the same post (i.e. Ministro),
           having only different labels,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """

        # change json_resp, so that two memberships in the same role,
        # different labels are included
        json_resp = self.get_person_memberships_json_resp()
        organization_id = json_resp[0]["memberships"][0]["organization_id"]
        role = json_resp[0]["memberships"][0]["role"]
        day_1 = faker.date_time_between("-2y", "-1y")

        memberships = [
            {
                "organization_id": organization_id,
                "role": role,
                "label": faker.sentence(nb_words=3),
                "start_date": (day_1 + timedelta(64)).strftime("%Y-%m-%d"),
                "end_date": (day_1 + timedelta(128)).strftime("%Y-%m-%d"),
            },
            {
                "organization_id": organization_id,
                "role": role,
                "label": faker.sentence(nb_words=3),
                "start_date": day_1.strftime("%Y-%m-%d"),
                "end_date": (day_1 + timedelta(256)).strftime("%Y-%m-%d"),
            },
        ]
        json_resp[0]["memberships"] = memberships

        # setup mock responses with solr returning the only result
        self.setup_mock(json_resp)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/incarichi_governo.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(context="governo"),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )()

        # total number of persons must be 1
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(
            Person.objects.first().family_name, json_resp[0]["family_name"]
        )

        # n of memberships for the person must be 1, not 2
        self.assertEqual(Person.objects.first().memberships.count(), 1)

    def test_etl_existing_person_new_roles_same_post_different_labels_check_label_true(
        self
    ):
        """Test import of existing person with two new roles for the same post (i.e. Ministro),
           having only different labels,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """

        # change json_resp, so that two memberships in the same role,
        # different labels are included
        json_resp = self.get_person_memberships_json_resp()
        organization_id = json_resp[0]["memberships"][0]["organization_id"]
        role = json_resp[0]["memberships"][0]["role"]
        day_1 = faker.date_time_between("-2y", "-1y")

        memberships = [
            {
                "organization_id": organization_id,
                "role": role,
                "label": faker.sentence(nb_words=3),
                "start_date": (day_1 + timedelta(64)).strftime("%Y-%m-%d"),
                "end_date": (day_1 + timedelta(128)).strftime("%Y-%m-%d"),
            },
            {
                "organization_id": organization_id,
                "role": role,
                "label": faker.sentence(nb_words=3),
                "start_date": day_1.strftime("%Y-%m-%d"),
                "end_date": (day_1 + timedelta(256)).strftime("%Y-%m-%d"),
            },
        ]
        json_resp[0]["memberships"] = memberships

        # setup mock responses with solr returning the only result
        self.setup_mock(json_resp)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/incarichi_governo.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(
                context="governo", check_membership_label=True
            ),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )()

        # total number of persons must be 1
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(
            Person.objects.first().family_name, json_resp[0]["family_name"]
        )

        # n of memberships for the person must be 2, not 1
        self.assertEqual(Person.objects.first().memberships.count(), 2)
        self.assertEqual(
            Person.objects.first().memberships.values("post_id").distinct().count(), 1
        )
        self.assertEqual(
            Person.objects.first()
            .memberships.values("organization_id")
            .distinct()
            .count(),
            1,
        )
        self.assertEqual(
            Person.objects.first().memberships.values("label").distinct().count(), 2
        )

    def test_etl_di_maio(self):
        """Test import of non-existing person with three roles,
           having only different labels,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """

        # change json_resp, so that two memberships in the same role,
        # different labels are included
        classification = ClassificationFactory(
            scheme="FORMA_GIURIDICA_OP", descr="Governo della Repubblica"
        )
        role_type_min = RoleTypeFactory(label="Ministro", classification=classification)
        role_type_vpc = RoleTypeFactory(
            label="Vicepresidente del Consiglio", classification=classification
        )
        election = ElectoralEventFactory(event_type="ELE-POL")
        organization = OrganizationFactory(
            name="Governo Conte I", classification=classification.descr
        )
        organization.add_classification_rel(classification)
        organization.add_key_event_rel(election)

        json_resp = [
            {
                "death_date": None,
                "contact_details": [
                    {
                        "label": "Twitter",
                        "value": "https://twitter.com/luigidimaio",
                        "contact_type": "TWITTER",
                    },
                    {
                        "label": "Facebook",
                        "value": "https://www.facebook.com/luigidimaio",
                        "contact_type": "FACEBOOK",
                    },
                ],
                "birth_location": "AVELLINO",
                "links": [],
                "image": "http://documenti.camera.it/apps/nuovosito/deputato/getFoto.asp?id=305892&legislatura=18",
                "profession": "Giornalista pubblicista",
                "sources": [
                    {
                        "note": "Open Data Camera",
                        "url": "http://dati.camera.it/ocd/persona.rdf/p305892",
                    }
                ],
                "education_level": "Diploma di liceo classico",
                "family_name": "DI MAIO",
                "gender": "M",
                "identifiers": [
                    {
                        "scheme": "OCD-URI",
                        "identifier": "http://dati.camera.it/ocd/persona.rdf/p305892",
                    },
                    {
                        "scheme": "WIKIDATA",
                        "identifier": "http://wikidata.org/entity/Q13581225",
                    },
                    {
                        "scheme": "FREEBASE",
                        "identifier": "http://rdf.freebase.com/ns/m.0_s692h",
                    },
                    {
                        "scheme": "DBPEDIA",
                        "identifier": "http://dbpedia.org/resource/Luigi_Di_Maio",
                    },
                    {
                        "scheme": "DBPEDIA-IT",
                        "identifier": "http://it.dbpedia.org/resource/Luigi_Di_Maio",
                    },
                    {"scheme": "SENATO-ID", "identifier": "29383"},
                ],
                "other_names": [],
                "memberships": [
                    {
                        "organization_id": organization.id,
                        "role": role_type_vpc.label,
                        "links": [
                            {
                                "note": "Pagina sito del Senato",
                                "url": "http://www.senato.it/loc/link.asp?tipodoc=sattsen&leg=18&id=29383",
                            }
                        ],
                        "end_date": None,
                        "sources": [
                            {
                                "note": "Open Data Camera",
                                "url": "http://dati.camera.it/ocd/membroGoverno.rdf/mg305892_2_142_1_20180601",
                            }
                        ],
                        "start_date": "2018-06-01",
                        "label": "Vicepresidente del Consiglio",
                    },
                    {
                        "organization_id": organization.id,
                        "role": role_type_min.label,
                        "links": [
                            {
                                "note": "Pagina sito del Senato",
                                "url": "http://www.senato.it/loc/link.asp?tipodoc=sattsen&leg=18&id=29383",
                            }
                        ],
                        "end_date": None,
                        "sources": [
                            {
                                "note": "Open Data Camera",
                                "url": "http://dati.camera.it/ocd/membroGoverno.rdf/mg305892_3_142_285_20180531",
                            }
                        ],
                        "start_date": "2018-05-31",
                        "label": "Ministro del Lavoro e delle politiche sociali",
                    },
                    {
                        "organization_id": organization.id,
                        "role": role_type_min.label,
                        "links": [
                            {
                                "note": "Pagina sito del Senato",
                                "url": "http://www.senato.it/loc/link.asp?tipodoc=sattsen&leg=18&id=29383",
                            }
                        ],
                        "end_date": None,
                        "sources": [
                            {
                                "note": "Open Data Camera",
                                "url": "http://dati.camera.it/ocd/membroGoverno.rdf/mg305892_3_142_169_20180531",
                            }
                        ],
                        "start_date": "2018-05-31",
                        "label": "Ministro dello Sviluppo economico",
                    },
                ],
                "given_name": "LUIGI",
                "birth_date": "1986-07-06",
            }
        ]

        # setup mock responses with solr returning the only result
        self.setup_mock(json_resp)

        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/incarichi_governo.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(
                context="governo", check_membership_label=True
            ),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )()

        # total number of persons must be 1
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(
            Person.objects.first().family_name, json_resp[0]["family_name"]
        )

        # n of memberships for the person must be 2, not 1
        self.assertEqual(Person.objects.first().memberships.count(), 3)
        self.assertEqual(
            Person.objects.first().memberships.values("post_id").distinct().count(), 2
        )
        self.assertEqual(
            Person.objects.first()
            .memberships.values("organization_id")
            .distinct()
            .count(),
            1,
        )
        self.assertEqual(
            Person.objects.first().memberships.values("label").distinct().count(), 3
        )

    def test_minniti_or_other_names(self):
        """Test import of non-existing person with other_names not null,
           lookup_strategy: anagraphical
           update_strategy: keep_old
        """

        # change json_resp, so that two memberships in the same role,
        # different labels are included
        classification = ClassificationFactory(
            scheme="FORMA_GIURIDICA_OP", descr="Governo della Repubblica"
        )
        role_type_min = RoleTypeFactory(label="Ministro", classification=classification)
        election = ElectoralEventFactory(event_type="ELE-POL")
        organization = OrganizationFactory(
            name="Governo Gentiloni Silveri I", classification=classification.descr
        )
        organization.add_classification_rel(classification)
        organization.add_key_event_rel(election)

        json_resp = [
            {
                "death_date": None,
                "contact_details": [],
                "birth_location": "REGGIO DI CALABRIA",
                "links": [],
                "image": "http://documenti.camera.it/apps/nuovosito/deputato/getFoto.asp?id=300146&legislatura=18",
                "profession": "Dirigente di partito",
                "sources": [
                    {
                        "note": "Open Data Camera",
                        "url": "http://dati.camera.it/ocd/persona.rdf/p300146",
                    }
                ],
                "education_level": "Laurea in filosofia",
                "family_name": "MINNITI",
                "gender": "M",
                "identifiers": [
                    {
                        "scheme": "OCD-URI",
                        "identifier": "http://dati.camera.it/ocd/persona.rdf/p300146",
                    }
                ],
                "other_names": [{"othername_type": "NIC", "name": "MARCO MINNITI"}],
                "memberships": [
                    {
                        "organization_id": organization.id,
                        "role": role_type_min.label,
                        "links": [],
                        "end_date": "2018-05-31",
                        "sources": [
                            {
                                "note": "Open Data Camera",
                                "url": "http://dati.camera.it/ocd/membroGoverno.rdf/mg300146_3_122_15_20161212",
                            }
                        ],
                        "start_date": "2016-12-12",
                        "label": "Ministro dell'Interno del I Governo Gentiloni Silveri",
                    }
                ],
                "given_name": "DOMENICO",
                "birth_date": "1956-06-06",
            }
        ]

        # setup mock responses with solr returning the only result
        self.setup_mock(json_resp)
        # define the instance and invoke the etl() method through __call__()
        ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/incarichi_governo.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(
                context="governo", check_membership_label=True
            ),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )()

        # total number of persons must be 1
        self.assertEqual(Person.objects.count(), 1)
        self.assertEqual(
            Person.objects.first().family_name, json_resp[0]["family_name"]
        )

        # other_names exist
        self.assertEqual(Person.objects.first().other_names.count(), 1)

        # n of memberships for the person is 1 (only 1 membership for Minniti imported in test)
        self.assertEqual(Person.objects.first().memberships.count(), 1)

    """
    Implements complex test case, of an ETL import from json where only family and given name are known.
    Tests how similarities are handled and the role of data_source_url in AKA object.
    See issue #257 (https://gitlab.depp.it/openpolis/opdm/opdm-project/issues/257)
    """
    @staticmethod
    def get_person_names_only_json_resp(org_id, label, role) -> list:

        source_url = faker.url()
        resp = [
            {
                "given_name": "Daniele",
                "family_name": "FRANCO",
                "birth_date": None,
                "death_date": None,
                "birth_location": None,
                "gender": "M",
                "identifiers": [],
                "links": [],
                "sources": [
                    {
                        "url": source_url,
                        "note": faker.sentence()
                    }
                ],
                "contacts": [],
                "memberships": [
                    {
                        "organization_id": org_id,
                        "label": label,
                        "role": role,
                        "start_date": "2018-08-02",
                        "end_date": None,
                        "sources": [
                            {
                                "url": source_url,
                                "note": faker.sentence()
                            }
                        ],
                    },
                    {
                        "organization_id": org_id,
                        "label": label,
                        "role": role,
                        "start_date": "2018-05-20",
                        "end_date": "2018-08-02",
                        "sources": [
                            {
                                "url": source_url,
                                "note": faker.sentence()
                            }
                        ],
                    },
                    {
                        "organization_id": org_id,
                        "label": label,
                        "role": role,
                        "start_date": "2017-03-13",
                        "end_date": "2018-05-20",
                        "sources": [
                            {
                                "url": source_url,
                                "note": faker.sentence()
                            }
                        ],
                    },
                ],
            }
        ]
        return resp

    def test_persons_names_only_generates_similarity(self):
        """Test import of a person with only family and given name known.
        Similar records already exist in the DB
        """
        from datetime import datetime

        classification = ClassificationFactory(
            scheme="FORMA_GIURIDICA_OP", descr="Dipartimento/Direzione/Ufficio"
        )
        org = OrganizationFactory(classification="Dipartimento/Direzione/Ufficio")
        org.add_classification_rel(classification)
        role = RoleTypeFactory(
            label="Capo Dipartimento Dipartimento/Direzione/Ufficio",
            classification=classification
        )
        label = "Ragioniere generale dello Stato"

        # create the person that is in the import snippet (test update)
        json_resp = self.get_person_names_only_json_resp(org.id, label, role.label)

        existing_persons = []
        scores = [69.1, 69.1, 60.77, 12.77]
        for i in range(0, 4):
            birth_parent_area = AreaFactory()
            birth_area = AreaFactory(parent=birth_parent_area)
            existing_persons.append(
                PersonFactory(
                    family_name=json_resp[0]["family_name"],
                    given_name=json_resp[0]["given_name"],
                    birth_date=faker.date_between(
                            start_date="-65y", end_date="-20y"
                        ).strftime("%Y-%m-%d"),
                    birth_location=birth_area.name,
                    birth_location_area=birth_area,
                )
            )

            existing_persons[-1].given_name = faker.first_name()
            existing_persons[-1].save()

        # setup mock responses with solr returning the only result
        self.setup_mock(
            json_resp,
            sqs_select_mock=json.dumps(
                {
                    "responseHeader": {
                        "status": 0,
                        "QTime": 1,
                        "params": {
                            "q": "family_name:{0} given_name:{1}".format(
                                json_resp[0]["family_name"],
                                json_resp[0]["given_name"]
                            ),
                            "df": "text",
                            "spellcheck": "true",
                            "fl": "* score",
                            "start": "0",
                            "spellcheck.count": "1",
                            "fq": "django_ct:(popolo.person)",
                            "rows": "10",
                            "wt": "json",
                            "spellcheck.collate": "true",
                        },
                    },
                    "response": {
                        "numFound": 3,
                        "start": 0,
                        "maxScore": 13.378365,
                        "docs": [
                            {
                                "id": "popolo.person.{0}".format(ep.id),
                                "django_ct": "popolo.person",
                                "django_id": ep.id,
                                "family_name": ep.family_name,
                                "given_name": ep.given_name,
                                "birth_date": datetime.strptime(
                                    ep.birth_date, "%Y-%m-%d"
                                ).strftime("%Y-%m-%dT00:00:00Z"),
                                "birth_date_s": ep.birth_date.replace('-', ''),
                                "birth_location": ep.birth_location,
                                "birth_location_area": ep.birth_location_area.id,
                                "CF_s": "123456789012345",
                                "OP_ID_s": "12345",
                                "score": scores[k]
                            } for k, ep in enumerate(existing_persons)
                        ]
                    }
                }
            )
        )

        # define the instance and invoke the etl() method through __call__()
        etl = ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/test.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(
                context="ministeri", check_membership_label=True
            ),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )

        etl.extract().transform()
        etl.load()

        # total number of persons must be 4 (import was blocked by found similarities)
        self.assertEqual(Person.objects.count(), 4)

        # one AKA record was created, with only 3 similarities (out of the 4 persons)
        self.assertEqual(AKA.objects.count(), 1)
        a = AKA.objects.first()
        self.assertEqual(a.n_similarities, 3)

        # the source has been recorded correctly in the similarity record
        self.assertIsNotNone(a.data_source_url)
        self.assertEqual(a.data_source_url, json_resp[0]['sources'][0]['url'])

    def test_persons_names_only_with_solved_similarity_same_source(self):
        """Test import of a person with only family and given name known.
        Similar records exist in the DB. An AKA was already solved, with the same source.
        No new similarity is created. Memberships are added to the person during the import.
        """
        from datetime import datetime

        classification = ClassificationFactory(
            scheme="FORMA_GIURIDICA_OP", descr="Dipartimento/Direzione/Ufficio"
        )
        org = OrganizationFactory(classification="Dipartimento/Direzione/Ufficio")
        org.add_classification_rel(classification)
        role = RoleTypeFactory(
            label="Capo Dipartimento Dipartimento/Direzione/Ufficio",
            classification=classification
        )
        label = "Ragioniere generale dello Stato"

        # create the person that is in the import snippet (test update)
        json_resp = self.get_person_names_only_json_resp(org.id, label, role.label)

        existing_persons = []
        scores = [69.1, 69.1, 60.77, 12.77]
        for i in range(0, 4):
            birth_parent_area = AreaFactory()
            birth_area = AreaFactory(parent=birth_parent_area)
            existing_persons.append(
                PersonFactory(
                    family_name=json_resp[0]["family_name"],
                    given_name=json_resp[0]["given_name"],
                    birth_date=faker.date_between(
                            start_date="-65y", end_date="-20y"
                        ).strftime("%Y-%m-%d"),
                    birth_location=birth_area.name,
                    birth_location_area=birth_area,
                )
            )

            existing_persons[-1].given_name = faker.first_name()
            existing_persons[-1].save()

        matched_person = PersonFactory(
            family_name=json_resp[0]['family_name'],
            given_name=json_resp[0]['given_name'],
            birth_location=None,
            birth_location_area=None,
            birth_date=None,
            gender=json_resp[0]['gender']

        )
        matched_person.add_sources(json_resp[0]['sources'])

        AKAFactory.create(
            search_params={
                'birth_date': '',
                'given_name': json_resp[0]["given_name"].lower(),
                'family_name': json_resp[0]["family_name"].lower(),
                'birth_location': ''
            },
            n_similarities=3,
            is_resolved=True,
            data_source_url=json_resp[0]['sources'][0]['url'],
            hashed='80e954ee5b7fdb39e602aa5a82c1c6daec795b3c6fd73e279d095e83ba0d8db0',
            loader_context={
                'item': json_resp[0],
                'class': 'PopoloPersonWithRelatedDetailsLoader',
                'module': 'project.tasks.etl.loaders.persons',
                'context': 'ministeri',
                'data_source': 'https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/test.json',
                'check_membership_label': True
            },
            content_object=matched_person,
            # content_type_id=33,
            # object_id=matched_person.id
        )

        # setup mock responses with solr returning the only result
        self.setup_mock(
            json_resp,
            sqs_select_mock=json.dumps(
                {
                    "responseHeader": {
                        "status": 0,
                        "QTime": 1,
                        "params": {
                            "q": "family_name:{0} given_name:{1}".format(
                                json_resp[0]["family_name"],
                                json_resp[0]["given_name"]
                            ),
                            "df": "text",
                            "spellcheck": "true",
                            "fl": "* score",
                            "start": "0",
                            "spellcheck.count": "1",
                            "fq": "django_ct:(popolo.person)",
                            "rows": "10",
                            "wt": "json",
                            "spellcheck.collate": "true",
                        },
                    },
                    "response": {
                        "numFound": 3,
                        "start": 0,
                        "maxScore": 13.378365,
                        "docs": [
                            {
                                "id": "popolo.person.{0}".format(ep.id),
                                "django_ct": "popolo.person",
                                "django_id": ep.id,
                                "family_name": ep.family_name,
                                "given_name": ep.given_name,
                                "birth_date": datetime.strptime(
                                    ep.birth_date, "%Y-%m-%d"
                                ).strftime("%Y-%m-%dT00:00:00Z"),
                                "birth_date_s": ep.birth_date.replace('-', ''),
                                "birth_location": ep.birth_location,
                                "birth_location_area": ep.birth_location_area.id,
                                "CF_s": "123456789012345",
                                "OP_ID_s": "12345",
                                "score": scores[k]
                            } for k, ep in enumerate(existing_persons)
                        ]
                    }
                }
            )
        )

        # define the instance and invoke the etl() method through __call__()
        etl = ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/test.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(
                context="ministeri", check_membership_label=True
            ),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )

        etl.extract().transform()
        etl.load()

        # total number of persons must be 5 (existing plus the one imported after similarity resolve)
        self.assertEqual(Person.objects.count(), 5)

        # one resolved AKA record exists, no new AKA was created
        self.assertEqual(AKA.objects.filter(is_resolved=True).count(), 1)
        self.assertEqual(AKA.objects.filter(is_resolved=False).count(), 0)

        # the person in the similarity has now all roles
        self.assertIsNotNone(matched_person.memberships.count(), len(json_resp[0]['memberships']))

    def test_persons_names_only_with_solved_similarity_different_source(self):
        """Test import of a person with only family and given name known.
        Similar records exist in the DB. An AKA was already solved, with a different source.
        A new similarity is created. No memberships are added to the person during the import.
        """
        from datetime import datetime

        classification = ClassificationFactory(
            scheme="FORMA_GIURIDICA_OP", descr="Dipartimento/Direzione/Ufficio"
        )
        org = OrganizationFactory(classification="Dipartimento/Direzione/Ufficio")
        org.add_classification_rel(classification)
        role = RoleTypeFactory(
            label="Capo Dipartimento Dipartimento/Direzione/Ufficio",
            classification=classification
        )
        label = "Ragioniere generale dello Stato"

        # create the person that is in the import snippet (test update)
        json_resp = self.get_person_names_only_json_resp(org.id, label, role.label)

        existing_persons = []
        scores = [69.1, 69.1, 60.77, 12.77]
        for i in range(0, 4):
            birth_parent_area = AreaFactory()
            birth_area = AreaFactory(parent=birth_parent_area)
            existing_persons.append(
                PersonFactory(
                    family_name=json_resp[0]["family_name"],
                    given_name=json_resp[0]["given_name"],
                    birth_date=faker.date_between(
                            start_date="-65y", end_date="-20y"
                        ).strftime("%Y-%m-%d"),
                    birth_location=birth_area.name,
                    birth_location_area=birth_area,
                )
            )

            existing_persons[-1].given_name = faker.first_name()
            existing_persons[-1].save()

        matched_person = PersonFactory(
            family_name=json_resp[0]['family_name'],
            given_name=json_resp[0]['given_name'],
            birth_location=None,
            birth_location_area=None,
            birth_date=None,
            gender=json_resp[0]['gender']

        )
        matched_person.add_sources(json_resp[0]['sources'])

        AKAFactory.create(
            search_params={
                'birth_date': '',
                'given_name': json_resp[0]["given_name"].lower(),
                'family_name': json_resp[0]["family_name"].lower(),
                'birth_location': ''
            },
            n_similarities=3,
            is_resolved=True,
            data_source_url=faker.url(),
            hashed='80e954ee5b7fdb39e602aa5a82c1c6daec795b3c6fd73e279d095e83ba0d8db0',
            loader_context={
                'item': json_resp[0],
                'class': 'PopoloPersonWithRelatedDetailsLoader',
                'module': 'project.tasks.etl.loaders.persons',
                'context': 'ministeri',
                'data_source': 'https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/test.json',
                'check_membership_label': True
            },
            content_object=matched_person,
            # content_type_id=33,
            # object_id=matched_person.id
        )

        # setup mock responses with solr returning the only result
        self.setup_mock(
            json_resp,
            sqs_select_mock=json.dumps(
                {
                    "responseHeader": {
                        "status": 0,
                        "QTime": 1,
                        "params": {
                            "q": "family_name:{0} given_name:{1}".format(
                                json_resp[0]["family_name"],
                                json_resp[0]["given_name"]
                            ),
                            "df": "text",
                            "spellcheck": "true",
                            "fl": "* score",
                            "start": "0",
                            "spellcheck.count": "1",
                            "fq": "django_ct:(popolo.person)",
                            "rows": "10",
                            "wt": "json",
                            "spellcheck.collate": "true",
                        },
                    },
                    "response": {
                        "numFound": 3,
                        "start": 0,
                        "maxScore": 13.378365,
                        "docs": [
                            {
                                "id": "popolo.person.{0}".format(ep.id),
                                "django_ct": "popolo.person",
                                "django_id": ep.id,
                                "family_name": ep.family_name,
                                "given_name": ep.given_name,
                                "birth_date": datetime.strptime(
                                    ep.birth_date, "%Y-%m-%d"
                                ).strftime("%Y-%m-%dT00:00:00Z"),
                                "birth_date_s": ep.birth_date.replace('-', ''),
                                "birth_location": ep.birth_location,
                                "birth_location_area": ep.birth_location_area.id,
                                "CF_s": "123456789012345",
                                "OP_ID_s": "12345",
                                "score": scores[k]
                            } for k, ep in enumerate(existing_persons)
                        ]
                    }
                }
            )
        )

        # define the instance and invoke the etl() method through __call__()
        etl = ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/test.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(
                context="ministeri", check_membership_label=True
            ),
            transformation=JsonPersonsMemberships2OpdmTransformation(),
            log_level=0,
        )

        etl.extract().transform()
        etl.load()

        # total number of persons must be 5 (existing plus the one imported after similarity resolve)
        self.assertEqual(Person.objects.count(), 5)

        # one resolved AKA record exists, no new AKA was created
        self.assertEqual(AKA.objects.filter(is_resolved=True).count(), 1)
        self.assertEqual(AKA.objects.filter(is_resolved=False).count(), 1)

        # the person in the similarity has now all roles
        self.assertIsNotNone(matched_person.memberships.count(), 0)

    def test_persons_with_classifications(self):
        """Test import of a person with classifications.
        """

        from project.api_v1.tests.test_person import PersonTestCase
        from popolo.models import Classification

        classification_a = ClassificationFactory()
        classification_b = ClassificationFactory()
        classifications = [
            {"classification": classification_a.id},
            {"classification": classification_b.id},
        ]
        n_classifications = len(classifications)

        # create a person from the PersonTestCase get_basic_data method
        # then add classifications and no memberships
        p = PersonTestCase.get_basic_data()
        p['classifications'] = classifications
        p['memberships'] = []
        self.setup_mock([p])  # setup_mock asks for a list of items

        # define the instance and invoke the etl() method through __call__()
        etl = ETL(
            extractor=JsonArrayExtractor(
                "https://s3.eu-central-1.amazonaws.com/opdm-service-data/parsers/test.json"
            ),
            loader=PopoloPersonWithRelatedDetailsLoader(
                context="ministeri", check_membership_label=True
            ),
            log_level=0,
        )

        etl.extract().transform()
        etl.load()

        # total number of persons must be 1
        self.assertEqual(Person.objects.count(), 1)

        # the right number of classifications were imported
        self.assertEqual(Classification.objects.count(), n_classifications)
        self.assertEqual(Person.objects.first().classifications.count(), n_classifications)
