# -*- coding: utf-8 -*-

"""
Implements complex tests, using more than one imports
"""
import json
import logging

from ooetl import ETL
import pandas as pd
from popolo.models import Person, Membership
from popolo.tests.factories import (
    OrganizationFactory,
    RoleTypeFactory,
    AreaFactory,
    ClassificationFactory,
)

from project.tasks.etl.extractors import UACSVExtractor, OPAPIPagedExtractor
from project.tasks.etl.loaders import DummyLoader
from project.tasks.etl.loaders.persons import (
    PopoloPersonMembershipLoader,
    PopoloPersonLoader,
    PopoloMembershipLoader,
)
from project.tasks.etl.transformations import Op2OpdmETL
from project.tasks.etl.transformations.minint2opdm import (
    HistMinintReg2OpdmTransformation,
)
from project.tasks.tests.etl import SolrETLTest, MockResponse

yearly_reg_csv = (
    b"CODICE_REGIONE;DESCRIZIONE_REGIONE;ISTAT_CODICE_REGIONE;ANNO_CENSIMENTO;POPOLAZIONE_CENSITA;"
    b"DESCRIZIONE_TEMPO_GESTIONE;DATA_ELEZIONE;DATA_BALLOTTAGGIO;CONSIGLIERI_SPETTANTI;"
    b"ASSESSORI_ASSEGNATI;SIGLA_TITOLO_ACCADEMICO;COGNOME;NOME;SESSO;DATA_NASCITA;SEDE_NASCITA;"
    b"LIVELLO_CARICA;DESCRIZIONE_CARICA;DATA_NOMINA;PARTITO_LISTA_COALIZIONE;DATA_CESSAZIONE;"
    b"TITOLO_DI_STUDIO;PROFESSIONE\r\n"
    b"01;PIEMONTE;01;2011;4363916;ORDINARIA;25/05/2014;;51;11;ON;CHIAMPARINO;SERGIO;M;01/09/1948;"
    b"MONCALIERI (TO);10;Presidente della regione;25/05/2014;CHIAMPARINO PRESIDENTE;;LAUREA;"
    b"Pensionati e persone ritirate dal lavoro\r\n"
)

json_resp = {
    "count": 1,
    "next": None,
    "previous": None,
    "results": [
        {
            "date_start": "2014-05-26",
            "date_end": None,
            "politician": {
                "content": {
                    "id": 160621,
                    "created_at": "2007-02-27T04:02:56",
                    "updated_at": "2017-07-11T16:51:05",
                },
                "first_name": "SERGIO",
                "last_name": "CHIAMPARINO",
                "birth_date": "1948-09-01T00:00:00",
                "death_date": None,
                "birth_location": "Moncalieri (TO)",
                "sex": "M",
                "self_uri": "http://api3.openpolis.it/politici/politicians/160621",
                "image_uri": "http://politici.openpolis.it/politician/picture?content_id=160621",
                "openparlamento_uri": "http://api3.openpolis.it/parlamento/18/parliamentarians/160621",
                "last_charge_update": "2017-07-11T16:51:05",
                "last_resource_update": "2014-07-25T11:33:56",
                "profession": {"description": "Impiegato"},
                "education_levels": [
                    {
                        "description": "Scienze Politiche",
                        "education_level": {"description": "Laurea"},
                    }
                ],
                "resources": [
                    {
                        "resource_type": "Altra Email",
                        "value": "infochiampa@gmail.com",
                        "description": "",
                        "updated_at": "2014-07-25T11:33:56",
                    }
                ],
                "institution_charges": [
                    {
                        "charge": "dal 31/05/2006 al 15/05/2011 Sindaco Giunta Comunale Torino (Partito: PD)",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/189282",
                    },
                    {
                        "charge": "dal 09/05/1996 al 29/05/2001 Deputato",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/591326",
                    },
                    {
                        "charge": "dal 12/07/1993 al 02/05/1996 Consigliere Consiglio Comunale Torino (Lista "
                        "elettorale: DS) ",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/656101",
                    },
                    {
                        "charge": "dal 28/05/2001 al 27/05/2006 Sindaco Giunta Comunale Torino (Partito: DS)",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/656103",
                    },
                    {
                        "charge": "dal 26/05/2014 Pres. Giunta Regionale Piemonte (Partito: PD)",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/720138",
                    },
                    {
                        "charge": "dal 30/06/2014 Consigliere Consiglio Regionale Piemonte (Lista elettorale: "
                        "CHIAMPARINO PRESIDENTE) ",
                        "self_uri": "http://api3.openpolis.it/politici/instcharges/781576",
                    },
                ],
            },
            "charge_type_descr": "Presidente",
            "institution_descr": "Giunta Regionale",
            "location_descr": "Regione Piemonte",
            "location": "http://api3.openpolis.it/territori/locations/3",
            "constituency_descr": None,
            "constituency_election_type": None,
            "description": "",
            "party": {
                "name": "Partito Democratico",
                "acronym": "PD",
                "oname": "Partito Democratico",
            },
            "group": {"name": "Non specificato", "acronym": None, "oname": None},
            "content": {
                "content": {
                    "id": 720138,
                    "created_at": "2014-05-28T12:35:41",
                    "updated_at": "2014-05-28T12:35:41",
                },
                "user": 28139,
                "deleted_at": None,
                "verified_at": None,
            },
        }
    ],
}
