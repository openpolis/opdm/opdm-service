# -*- coding: utf-8 -*-

"""
Implements tests specific to the etl classes.
"""
from unittest.mock import patch

from ooetl.tests.django import ETLTest


class MockResponse:
    """class that mocks requests' response (json method)
    """

    def __init__(self, json_data, status_code):
        self.json_data = json_data
        self.status_code = status_code

    def json(self):
        return self.json_data


class SolrETLTest(ETLTest):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.mock_sqs_select_patcher = patch("pysolr.Solr._select")
        cls.mock_sqs_select = cls.mock_sqs_select_patcher.start()
        cls.mock_sqs_update_patcher = patch("pysolr.Solr._update")
        cls.mock_sqs_update = cls.mock_sqs_update_patcher.start()

    @classmethod
    def tearDownClass(cls):
        cls.mock_sqs_select_patcher.stop()
        cls.mock_sqs_update_patcher.stop()
        super().tearDownClass()
