from datetime import datetime
import logging
from math import inf

from django.contrib.sites.models import Site
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.db.models import Model
import pandas as pd
from popolo.models import Area, Organization

local_logger = logging.getLogger(__name__)
domain = Site.objects.get_current().domain
entity_types_to_endpoint = {
    "Organization": "organizations",
    "Person": "persons",
    "Area": "areas",
    "Membership": "memberships",
}


class Check:
    """

    """

    def __init__(self, staging_base_url=False, logger=None, generate_report=False):
        self.staging_base_url = staging_base_url
        if logger is None:
            self.logger = local_logger
        else:
            self.logger = logger

        self.generate_report = generate_report
        if self.generate_report:
            self.report = pd.DataFrame(
                columns=[
                    "entity_type",
                    "entity_name",
                    "entity_id",
                    "entity_link_fe",
                    "message",
                ]
            )
            self.report["entity_type"] = self.report["entity_type"].astype(str)
            self.report["entity_name"] = self.report["entity_name"].astype(str)
            self.report["entity_id"] = self.report["entity_id"].astype(int)
            self.report["message"] = self.report["message"].astype(int)

    def log(self, level: int, entity: Model, message: str) -> None:
        base_url_fe = "https://opdm.openpolis.io"
        if self.staging_base_url:
            base_url_fe = base_url_fe.replace("opdm.", "staging.opdm.")

        endpoint = entity_types_to_endpoint[entity.__class__.__name__]
        entity_path_fe = f"/#/{endpoint}/edit/{entity.id}"
        entity_absolute_url_fe = f"{base_url_fe}{entity_path_fe}"
        data = dict(
            entity_type=entity.__class__.__name__,
            entity_link_fe=entity_absolute_url_fe,
            entity_name=str(entity),
            entity_id=entity.id,
            message=message,
        )
        self.logger.log(
            level=level,
            msg="{entity_type}: {entity_id} "
            "[fe: {entity_link_fe}] - {entity_name}: {message}.".format(**data),
        )
        if self.generate_report:
            self.report = self.report.append(data, ignore_index=True)

    def info(self, entity, message):
        self.log(logging.INFO, entity, message)

    def warning(self, entity, message):
        self.log(logging.WARNING, entity, message)

    def error(self, entity, message):
        self.log(logging.ERROR, entity, message)

    def critical(self, entity, message):
        self.log(logging.CRITICAL, entity, message)

    def comune(self, area: Area) -> None:
        qs = area.organizations.filter(classification="Comune")
        try:
            comune_org = qs.get()  # should return exactly 1 object
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            self.warning(
                area, f"Dovrebbe avere esattamente 1 Comune, ma ne ha {qs.count()}"
            )
            return
        qs = comune_org.children.filter(classification="Giunta comunale")
        try:
            giunta_comunale = qs.get()  # should return exactly 1 object
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            self.warning(
                area,
                f"Dovrebbe avere esattamente 1 Giunta comunale, ma ne ha {qs.count()}",
            )
            return
        qs = comune_org.children.filter(classification="Consiglio comunale")
        try:
            consiglio_comunale = qs.get()  # should return exactly 1 object
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            self.warning(
                area,
                f"Dovrebbe avere esattamente 1 Consiglio comunale, ma ne ha {qs.count()}",
            )
            return
        del qs

        parent_parent = area.parent.parent
        if not parent_parent:
            self.warning(area, f"`area.parent.parent` is {area.inhabitants}")
            return

        popolazione = area.inhabitants

        if not popolazione:
            self.warning(area, f"`area.inhabitants` is {area.inhabitants}")
            return

        # Conta tutti i membri del consiglio comunale (inclusi presidente e vice)
        n_consiglieri_in_carica = consiglio_comunale.memberships.current().count()

        n_sindaci_in_carica = (
            giunta_comunale.memberships.current().filter(role="Sindaco").count()
        )
        n_commissari_in_carica = (
            giunta_comunale.memberships.current()
            .filter(role__icontains="Commissario")
            .count()
        )
        commissariato = n_commissari_in_carica > 0

        if not commissariato:
            if n_sindaci_in_carica > 0:
                if n_sindaci_in_carica != 1:
                    self.warning(
                        giunta_comunale,
                        f"Dovrebbe avere esattamente 1 Sindaco in carica, ma ne ha {n_sindaci_in_carica}",
                    )
            else:
                self.warning(
                    giunta_comunale,
                    "Il comune non ha né un sindaco in carica, né un commissario",
                )

            def check_n_consiglieri_in_carica(min_threshold, max_threshold, expected):
                if min_threshold <= popolazione <= max_threshold:
                    if n_consiglieri_in_carica != expected:
                        self.warning(
                            consiglio_comunale,
                            f"Numero consiglieri in carica: {n_consiglieri_in_carica}. Dovrebbero essere {expected}",
                        )

            if "Sicilia" in parent_parent.name:
                if "Marsala" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 30)
                elif "Agrigento" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 30)
                elif "Augusta" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 30)
                elif "Carini" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 30)
                elif "Milazzo" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 30)
                elif "Enna" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 30)
                elif "Tremestieri Etneo" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 20)
                elif "Villabate" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 20)
                elif "Ribera" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 20)
                elif "Bronte" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 19)
                elif "Aci Sant'Antonio" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 18)
                elif "Ispica" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 20)
                elif area.is_provincial_capital:
                    check_n_consiglieri_in_carica(0, 100_000, 24)
                    check_n_consiglieri_in_carica(100_001, 250_000, 32)
                    check_n_consiglieri_in_carica(250_001, 500_000, 36)
                    check_n_consiglieri_in_carica(500_001, inf, 40)
                else:
                    check_n_consiglieri_in_carica(0, 3000, 10)
                    check_n_consiglieri_in_carica(3001, 10000, 12)
                    check_n_consiglieri_in_carica(10001, 30000, 16)
                    check_n_consiglieri_in_carica(30001, 100_000, 24)
                    check_n_consiglieri_in_carica(100_001, 250_000, 32)
                    check_n_consiglieri_in_carica(250_001, 500_000, 36)
                    check_n_consiglieri_in_carica(500_001, inf, 40)
            elif "Sardegna" in parent_parent.name:
                check_n_consiglieri_in_carica(0, 1000, 8)
                check_n_consiglieri_in_carica(1001, 5000, 12)
                check_n_consiglieri_in_carica(5001, 15000, 16)
                check_n_consiglieri_in_carica(15001, 25000, 20)
                check_n_consiglieri_in_carica(25001, 50000, 24)
                check_n_consiglieri_in_carica(50001, 100_000, 28)
                check_n_consiglieri_in_carica(100_001, inf, 34)
            elif "Trentino-Alto Adige" in parent_parent.name:
                if "Bozen" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 45)
                elif "Meran" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 36)
                elif "Leifers" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 27)
                elif "Bruneck" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 27)
                elif "Brixen" in area.name:
                    check_n_consiglieri_in_carica(0, inf, 20)
                elif area.is_provincial_capital:
                    check_n_consiglieri_in_carica(0, inf, 40)
                else:
                    check_n_consiglieri_in_carica(0, 1000, 12)
                    check_n_consiglieri_in_carica(1001, 3000, 15)
                    check_n_consiglieri_in_carica(3001, 10000, 18)
                    check_n_consiglieri_in_carica(10001, 30000, 22)
                    check_n_consiglieri_in_carica(30001, inf, 32)
            elif "Valle d'Aosta" in parent_parent.name:
                if area.is_provincial_capital:
                    check_n_consiglieri_in_carica(0, inf, 30)
                else:
                    check_n_consiglieri_in_carica(0, 300, 12)
                    check_n_consiglieri_in_carica(301, 3000, 14)
                    check_n_consiglieri_in_carica(3000, inf, 18)
            elif "Friuli-Venezia Giulia" in parent_parent.name:
                if area.is_provincial_capital:
                    check_n_consiglieri_in_carica(0, inf, 40)
                else:
                    check_n_consiglieri_in_carica(0, 3000, 12)
                    check_n_consiglieri_in_carica(3001, 10000, 16)
                    check_n_consiglieri_in_carica(10001, 15000, 20)
                    check_n_consiglieri_in_carica(15001, inf, 24)
            else:
                if area.is_provincial_capital:
                    if "Roma" == area.name:
                        check_n_consiglieri_in_carica(0, inf, 48)
                    else:
                        check_n_consiglieri_in_carica(0, 250_000, 32)
                        check_n_consiglieri_in_carica(250_001, 500_000, 36)
                        check_n_consiglieri_in_carica(500_001, 1_000_000, 40)
                        check_n_consiglieri_in_carica(1_000_001, inf, 48)
                else:
                    check_n_consiglieri_in_carica(0, 3000, 10)
                    check_n_consiglieri_in_carica(3001, 10000, 12)
                    check_n_consiglieri_in_carica(10001, 30000, 16)
                    check_n_consiglieri_in_carica(30001, 100_000, 24)
                    check_n_consiglieri_in_carica(100_001, 250_000, 32)
                    check_n_consiglieri_in_carica(250_001, 500_000, 36)
                    check_n_consiglieri_in_carica(500_001, 1_000_000, 40)
                    check_n_consiglieri_in_carica(1_000_001, inf, 48)
        else:  # commissariato
            if n_sindaci_in_carica > 0:
                self.warning(
                    giunta_comunale,
                    f"Il comune è commissariato ma ha {n_sindaci_in_carica} Sindaco in carica",
                )

        for org in [consiglio_comunale, giunta_comunale]:
            self.memberships_ongoing_for_more_than_x_years(org, 5)

    def regione(self, area: Area) -> None:
        qs = area.organizations.filter(classification="Regione")
        try:
            regione_org = qs.get()  # should return exactly 1 object
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            self.warning(
                area, f"Dovrebbe avere esattamente 1 Regione, ma ne ha {qs.count()}"
            )
            return
        qs = regione_org.children.filter(classification="Giunta regionale")
        try:
            giunta_regionale = qs.get()  # should return exactly 1 object
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            self.warning(
                area,
                f"Dovrebbe avere esattamente 1 Giunta regionale, ma ne ha {qs.count()}",
            )
            return
        qs = regione_org.children.filter(classification="Consiglio regionale")
        try:
            consiglio_regionale = qs.get()  # should return exactly 1 object
        except (MultipleObjectsReturned, ObjectDoesNotExist):
            self.warning(
                area,
                f"Dovrebbe avere esattamente 1 Consiglio regionale, ma ne ha {qs.count()}",
            )
            return
        del qs

        n_presid_consiglio_regionale_in_carica = consiglio_regionale.memberships.filter(
            end_date=None, role="Presidente di consiglio regionale"
        ).count()
        n_presid_giunta_regionale_in_carica = giunta_regionale.memberships.filter(
            end_date=None, role="Presidente di Regione"
        ).count()
        n_vicepresid_giunta_regionale_in_carica = giunta_regionale.memberships.filter(
            end_date=None, role="Vicepresidente di Regione"
        ).count()

        n_consiglieri_regionali = consiglio_regionale.memberships.current().count()

        if n_presid_consiglio_regionale_in_carica != 1:
            self.warning(
                consiglio_regionale,
                f"Dovrebbe avere esattamente 1 Presidente di consiglio regionale in carica, "
                f"ma ne ha {n_presid_consiglio_regionale_in_carica}",
            )
        if n_presid_giunta_regionale_in_carica != 1:
            self.warning(
                giunta_regionale,
                f"Dovrebbe avere esattamente 1 Presidente di Regione in carica, "
                f"ma ne ha {n_presid_giunta_regionale_in_carica}",
            )
        if n_vicepresid_giunta_regionale_in_carica != 1:
            self.warning(
                giunta_regionale,
                f"Dovrebbe avere esattamente 1 Vicepresidente di Regione in carica, "
                f"ma ne ha {n_vicepresid_giunta_regionale_in_carica}",
            )

        expected_n_consiglieri = _AREA_NAME_TO_N_CONSIGLIERI_REG.get(area.name, None)
        if expected_n_consiglieri:
            if n_consiglieri_regionali != expected_n_consiglieri:
                self.warning(
                    consiglio_regionale,
                    f"Numero consiglieri in carica: {n_consiglieri_regionali}. "
                    f"Dovrebbero essere {expected_n_consiglieri}",
                )

        for org in [consiglio_regionale, giunta_regionale]:
            self.memberships_ongoing_for_more_than_x_years(org, 5)

    def memberships_ongoing_for_more_than_x_years(
        self, org: Organization, max_years: int
    ) -> None:
        for membership in org.memberships.filter(
            end_date=None
        ):  # iterate current memberships
            m_start_date = datetime.strptime(membership.start_date, "%Y-%m-%d")
            delta = datetime.now() - m_start_date
            if delta.days > (max_years * 365):  # time delta greater than X years?
                self.warning(
                    membership,
                    f"{membership.person.name} è {membership.label} da più di {max_years} anni",
                )


_AREA_NAME_TO_N_CONSIGLIERI_REG = {
    "Umbria": 21,
    "Marche": 31,
    "Sicilia": 70,
    "Friuli-Venezia Giulia": 49,
    "Trentino-Alto Adige/Südtirol": 70,
    "Valle d\'Aosta/Vallée d\'Aoste": 35,
    "Lazio": 51,
    "Molise": 21,
    "Veneto": 51,
    "Abruzzo": 31,
    "Basilicata": 21,
    "Calabria": 31,
    "Campania": 51,
    "Emilia-Romagna": 50,
    "Liguria": 31,
    "Lombardia": 80,
    "Puglia": 51,
    "Sardegna": 60,
    "Toscana": 41,
    "Piemonte": 51,
}
