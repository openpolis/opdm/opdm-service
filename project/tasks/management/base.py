from copy import deepcopy
from itertools import groupby
import json
import logging
from pathlib import Path
import roman
from typing import List, Dict, Optional

from django.conf import settings
from django.core.management import BaseCommand

from django.contrib.contenttypes.models import ContentType
from popolo.models import Classification, Organization

from project.tasks.parsing.common import identifiers_map, parse_date
from project.tasks.parsing.sparql.camera import get_person_from_binding
from project.tasks.parsing.utils import carve_date_intervals, carve_date_interval


class SparqlToJsonCommand(BaseCommand):
    """
    A management command...

    Rather than implementing `handle()`, subclasses must implement `query`,
    to retrieve the SPARQL bindings, and `handle_bindings()` to process the
    bindings into JSON array (list of dictionaries).
    """

    logger = logging.getLogger(f"project.{__name__}")
    json_filename = "sparql"

    def add_arguments(self, parser):
        parser.add_argument(
            "legislature",
            metavar="N",
            action="store",
            type=int,
            default=17,
            help="The legislature to parse as integer number.",
        )
        parser.add_argument(
            "--output-dir",
            "-o",
            action="store",
            default=f"{settings.RESOURCES_PATH}/data/parsers",
            dest="output_dir",
            help="The path of the directory where the JSON will be dumped.",
        )

    def handle(self, *args, **options):
        self.logger.setLevel(
            {
                0: logging.ERROR,
                1: logging.WARNING,
                2: logging.INFO,
                3: logging.DEBUG,
            }.get(options["verbosity"])
        )
        self.logger.info("Querying SPARQL...")
        bindings = self.query(**options)
        self.logger.info(f"Got {len(bindings)} bindings. Processing...")
        output = self.handle_bindings(bindings, **options)
        if output:
            path = Path(options["output_dir"]) / self.get_json_name(**options)
            path.parent.mkdir(parents=True, exist_ok=True)
            with path.open("w") as f:
                self.logger.info(f"Dumping JSON as {path.as_uri()}...")
                json.dump(output, f, indent=True)
                self.logger.info("Done.")
        else:
            self.logger.info(f"Handling of bindings returned {len(output)}.")

    def handle_camera_commissioni_organizations_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        """A common method to be shared among
        camera_commissioni_organizations and camera_commissioni_bicamerali_organizations command,
        in order to avoid code duplications.

        :param bindings:
        :return:
        """
        n_leg = options["legislature"]
        org_classification_map = {
            "ORGANO DELLA PRESIDENZA": "Organo di presidenza parlamentare",
            "GIUNTA PER IL REGOLAMENTO": "Giunta parlamentare",
            "GIUNTA PER LE AUTORIZZAZIONI": "Giunta parlamentare",
            "GIUNTA PER LE ELEZIONI": "Giunta parlamentare",
            "COMMISSIONE PERMANENTE": "Commissione permanente parlamentare",
            "COMMISSIONE BICAMERALE CONSULTIVA": "Commissione/comitato bicamerale parlamentare",
            "COMMISSIONE BICAMERALE D'INCHIESTA": "Commissione/comitato bicamerale parlamentare",
            "COMMISSIONE BICAMERALE DI INDIRIZZO, VIGILANZA E CONTROLLO": (
                "Commissione/comitato bicamerale parlamentare"
            ),
            "COMMISSIONE BICAMERALE DI RANGO COSTITUZIONALE": "Commissione/comitato bicamerale parlamentare",
            "COMMISSIONE MISTA": "Commissione/comitato bicamerale parlamentare",
            "COMITATO PER I PROCEDIMENTI DI ACCUSA": "Commissione/comitato bicamerale parlamentare",
            "COMMISSIONE MONOCAMERALE D'INCHIESTA": "Commissione d'inchiesta parlamentare monocamerale/bicamerale",
            "DELEGAZIONE PARLAMENTARE PRESSO ASSEMBLEA INTERNAZIONALE": (
                "Delegazione parlamentare presso assemblea internazionale"
            ),
            "COMMISSIONE SPECIALE": "Commissione speciale/straordinaria parlamentare",
            "COMITATO PER LA LEGISLAZIONE": "Commissione speciale/straordinaria parlamentare",
            "EX ARTICOLO 58": "Commissione speciale/straordinaria parlamentare",
        }
        classification_descr_to_id_map = dict(
            Classification.objects.filter(scheme="FORMA_GIURIDICA_OP").values_list(
                "descr", "id"
            )
        )

        try:
            parent = Organization.objects.filter(identifier=f"ASS-CAMERA-{n_leg}").get()
        except Organization.DoesNotExist:
            self.logger.error(f"Organization ASS-CAMERA-{n_leg} does not exist.")
            return

        organizations_buf = []

        for binding in bindings:
            classification = org_classification_map.get(binding["classification"].value)
            if not classification:
                self.logger.debug(
                    f"{binding['classification'].value} not mapped... Skipping."
                )
                continue
            # Compose org name
            if classification in [
                "Commissione/comitato bicamerale parlamentare",
                "Delegazione parlamentare presso assemblea internazionale",
            ]:
                name = f'{binding["name"].value} ({roman.toRoman(n_leg)} legislatura)'
                parent_id = None
            else:
                name = f'{binding["name"].value} Camera ({roman.toRoman(n_leg)} legislatura)'
                parent_id = parent.id
            tmp = {
                "name": name,
                "classification": classification,
                "founding_date": parse_date(binding["founding_date"].value),
                "dissolution_date": (
                    parse_date(binding["dissolution_date"].value)
                    if "dissolution_date" in binding
                    else None
                ),
                "identifier": binding["identifier"].value,
                "identifiers": [
                    {"scheme": "OCD-URI", "identifier": binding["identifier"].value}
                ],
                "sources": [
                    {
                        "note": "Open Data Camera",
                        "url": "https://dati.camera.it/",
                    }
                ],
                "parent": parent_id,
            }
            # Attempt to retrieve the classification id
            if classification in classification_descr_to_id_map:
                tmp["classifications"] = [
                    {
                        "classification": classification_descr_to_id_map.get(
                            classification
                        )
                    }
                ]
            else:
                self.logger.debug(
                    f'Classification "{classification}" with schema "FORMA_GIURIDICA_OP" not found. '
                    f"Classification id is not added."
                )
            organizations_buf.append(tmp)

        return organizations_buf

    def handle_camera_commissioni_memberships_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        """A common method to be shared among
        camera_commissioni_memberships and camera_commissioni_bicamerali_memberships command,
        in order to avoid code duplications.

        :param bindings:
        :return:
        """
        ocd_uri_to_org_id = identifiers_map(
            scheme="OCD-URI",
            content_type=ContentType.objects.get_for_model(Organization),
        )
        org_id_to_name = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OCD-URI"
            ).values_list("id", "name")
        }
        org_id_to_classification = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OCD-URI"
            ).values_list("id", "classification")
        }

        leg = options['legislature']

        # build senatori_dict to integrate info about birth date and location for senatori
        senato = Organization.objects.filter(
            name__icontains='assemblea senato', key_events__key_event__identifier=f'ITL_{leg}'
        ).first()
        senatori_dict = {}
        senatori_orig_names = {}
        for m in senato.members:
            senatori_dict[m.name.lower()] = {
                'birth_date': m.birth_date,
                'birth_location': m.birth_location
            }

            # explore other names and reference original names
            for o in m.other_names.values_list('name', flat=True):
                if not senatori_orig_names.get(o.lower(), None):
                    senatori_orig_names[o.lower()] = {
                        'family_name': m.family_name,
                        'given_name': m.given_name
                    }

        buffer = {}
        for binding in bindings:

            tmp = buffer.get(
                binding["person__id"].value,
                {
                    **get_person_from_binding(binding),
                },
            )

            tmp["uri_parlamentare"] = binding["rif_parlamentare"].value

            if 'senatore' in tmp["uri_parlamentare"]:
                name = f"{tmp['given_name']} {tmp['family_name']}".lower()

                # point to original name's data, to avoid duplications
                if name in senatori_orig_names:
                    tmp['given_name'] = senatori_orig_names[name]['given_name']
                    tmp['family_name'] = senatori_orig_names[name]['family_name']
                    name = f"{tmp['given_name']} {tmp['family_name']}".lower()

                if name in senatori_dict:
                    if "birth_date" in senatori_dict[name]:
                        tmp["birth_date"] = senatori_dict[name]['birth_date']
                    else:
                        self.logger.warning(f"Could not find birth_date for senatore {name}. Skipping")
                        continue

                    if "birth_location" in senatori_dict[name]:
                        tmp["birth_location"] = senatori_dict[name]['birth_location']
                    else:
                        self.logger.warning(f"Could not find birth_location for senatore {name}. Skipping")
                        continue
                else:
                    self.logger.warning(f"Could not find {name} among senatori for leg. {leg}.  Skipping")
                    continue

            org_id = ocd_uri_to_org_id.get(binding["membership__organization_id"].value)
            if not org_id:
                self.logger.debug(
                    f"\"{binding['membership__organization_id'].value}\" does not exist, skipping.."
                )
                continue

            if "membership__role" in binding and binding["membership__role"].value:
                carica = binding["membership__role"].value.title()
                if carica in [
                    "Coordinatore Ineleggibilita' E Decadenze",
                    "Coordinatore Incompatibilita'",
                ]:
                    carica = "Componente"
            else:
                carica = "Componente"

            tmp_start_date = parse_date(binding["membership__start_date"].value)
            if "membership__end_date" in binding:
                tmp_end_date = parse_date(binding["membership__end_date"].value)
            else:
                tmp_end_date = None

            if (
                "membership__type" in binding and
                binding["membership__type"].value.lower() == "sostituto" and
                "membership__sostituisce" in binding and
                binding["membership__sostituisce"].value != ""
            ):
                sostituisce = binding["membership__sostituisce"].value
            else:
                sostituisce = None

            org_name = org_id_to_name[org_id]
            org_class = org_id_to_classification[org_id]

            tmp["memberships"].append(
                {
                    "organization_id": org_id,
                    "role": f"{carica} {org_class}",
                    "label": f"{carica} {org_name} ",
                    "start_date": tmp_start_date,
                    "end_date": tmp_end_date,
                    "sostituisce": sostituisce,
                    "sources": [
                        {
                            "note": "Open Data Camera", "url": ""
                            "https://dati.camera.it/"
                        }
                    ],
                }
            )

            buffer[binding["person__id"].value] = tmp

        #
        # re-arrange memberships, in order to avoid duplication of
        # simple memberships and memberships with roles (presidency, vice-p, ...)
        #

        # main loop over buffer (data dict by person_id)
        for person_id, values in buffer.items():
            memberships = values.pop("memberships")

            # only valid in cases of multi-memberships
            if len(memberships) > 1:

                # will contain the final transformed membership
                transformed_memberships = []

                # memberships are grouped by organization_id
                groups = groupby(memberships, lambda x: x["organization_id"])
                for org_id, org_memberships_group in groups:

                    # copy memberships of the group, in order to avoid poisoning references
                    org_memberships = deepcopy(list(org_memberships_group))

                    # filter role memberships
                    role_memberships = list(filter(lambda x: 'componente' not in x['label'].lower(), org_memberships))
                    role_memberships.sort(key=lambda x: x['start_date'] or '-999999')
                    role_memberships_intervals = list(map(lambda x: (x['start_date'], x['end_date']), role_memberships))

                    # filter simple memberships
                    comp_memberships = list(filter(lambda x: 'componente' in x['label'].lower(), org_memberships))
                    comp_memberships.sort(key=lambda x: x['start_date'] or '-999999')
                    comp_memberships_intervals = list(map(lambda x: (x['start_date'], x['end_date']), comp_memberships))

                    # carve role memberships intervals out of simple memberships ones
                    comp_transformed_intervals = carve_date_intervals(
                        comp_memberships_intervals, role_memberships_intervals
                    )

                    # re-build transformed simple memberships, using the
                    # first memebrship as model, and copying the dates from the intervals
                    # this is possible, since having grouped by organization_id (commission)
                    # and being simple memberships, all other data are identical
                    transformed_comp_memberships = []
                    for interval in comp_transformed_intervals:
                        _m = deepcopy(comp_memberships[0])
                        _m['start_date'] = interval[0]
                        _m['end_date'] = interval[1]
                        transformed_comp_memberships.append(_m)

                    # re-assemble simple and role memberships for this organization_id
                    # sorting by start_date
                    transformed_org_memberships = role_memberships + transformed_comp_memberships
                    transformed_org_memberships.sort(key=lambda x: x['start_date'] or '-999999')

                    # extend the transformed memberships list with the result
                    transformed_memberships.extend(transformed_org_memberships)
            else:
                transformed_memberships = memberships

            # sort the transformed membership, by start_date
            transformed_memberships.sort(key=lambda x: x['start_date'] or '-999999')

            # re-integrate the memberships into values
            values["memberships"] = transformed_memberships

        #
        # carve substitutions intervals from original memberships
        #

        # build map from uri_parlamentare to person_id
        deputato_to_person_map = {
            v['uri_parlamentare']: pid for pid, v in buffer.items()
        }

        # loop over all persons' data
        for person_id, values in buffer.items():

            # extract substitutions from current person's memberships
            substitution_memberships = list(filter(
                lambda x: 'sostituisce' in x and x['sostituisce'] is not None,
                values["memberships"]
            ))

            # loop over all substitutions
            for m in substitution_memberships:
                pid = deputato_to_person_map[m["sostituisce"]]

                # build new memberships and mark for membership removals after carving
                additions = []
                for om in buffer[pid]["memberships"]:
                    if om["organization_id"] == m["organization_id"]:
                        oi = (om["start_date"], om["end_date"])
                        i = (m["start_date"], m["end_date"])
                        res_i = carve_date_interval(oi, i)

                        om["to_remove"] = True
                        for (start_date, end_date) in res_i:
                            additions.append({
                                "organization_id": m["organization_id"],
                                "role": m["role"],
                                "label": m["label"],
                                "start_date": start_date,
                                "end_date": end_date,
                                "sources": [
                                    {
                                        "note": "Open Data Camera",
                                        "url": "https://dati.camera.it/"
                                    }
                                ],
                            })
                # remove marked memebrships
                buffer[pid]["memberships"] = [
                    m for m in deepcopy(buffer[pid]["memberships"])
                    if "to_remove" not in m or not m["to_remove"]
                ]

                # add resulting new memberships
                buffer[pid]["memberships"].extend(additions)

            # clean temporary variables
            for m in values["memberships"]:
                m.pop("sostituisce", None)
            values.pop("uri_parlamentare", None)

        # emit output
        return [values for person_id, values in buffer.items()]

    @classmethod
    def get_json_name(cls, **options) -> str:
        return f'{cls.json_filename}_{options["legislature"]:02d}.json'

    def query(self, **options) -> Optional[List[Dict]]:
        raise NotImplementedError

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        raise NotImplementedError
