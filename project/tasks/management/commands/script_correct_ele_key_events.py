import logging

from django.core.management import BaseCommand
from popolo.models import KeyEvent


class Command(BaseCommand):
    help = "Corrections for KeyEvents data of type ELE: missing information are added"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start")

        self.logger.info("Add identifier, and modify names to electoral KeyEvents")
        for ke in KeyEvent.objects.filter(event_type="ELE", identifier__isnull=True):
            election_name = ke.name

            if election_name != "":
                election_name = (
                    election_name.replace("comunal ", "comunali ")
                    .replace("provincial ", "provinciali ")
                    .replace("metropolitan ", "metropolitane ")
                    .replace("regional ", "regionali ")
                )

                if "comunali" in election_name:
                    context = "COM"
                elif "metropolitane" in election_name:
                    context = "METRO"
                elif "provinciali" in election_name:
                    context = "PROV"
                elif "regionali" in election_name:
                    context = "REG"
                else:
                    context = ""
                if context:
                    identifier = "ELE-{0}_{1}".format(context, ke.start_date)
                else:
                    identifier = None

                self.logger.info(
                    "Modifying {0} => {1}. Adding identifier {2}.".format(
                        ke.name, election_name, identifier
                    )
                )
                ke.name = election_name
                ke.identifier = identifier
                ke.save()

        self.logger.info("Modify generic ELE event_type for electoral KeyEvents")
        for ke in KeyEvent.objects.filter(event_type="ELE", identifier__contains="ELE"):
            election_name = ke.name
            if "comunali" in election_name:
                context = "COM"
            elif "metropolitane" in election_name:
                context = "METRO"
            elif "provinciali" in election_name:
                context = "PROV"
            elif "regionali" in election_name:
                context = "REG"
            else:
                context = ""
            if context:
                event_type = "ELE-{0}".format(context)
                self.logger.info(
                    "Modifying {0} => {1} for {2}".format(
                        ke.event_type, event_type, election_name
                    )
                )
                ke.event_type = event_type
                ke.save()

        self.logger.info("End")
