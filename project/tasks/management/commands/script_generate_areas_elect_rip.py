# -*- coding: utf-8 -*-
from django.contrib.gis.db.models import Union
from popolo.models import Area
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """This command will generate the electoral ripartitions for residents in foreign countries.
    """
    help = "Generate electoral ripartitions abroad"

    def handle(self, *args, **options):

        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Starting import process.")

        # generate electoral area 'ESTERO'
        estero, created = Area.objects.get_or_create(
            identifier="ELECT_COLL_ESTERO",
            defaults={
                'name': 'ESTERO',
                'classification': 'ELECT_COLL_ESTERO',
            }
        )
        estero.geometry = Area.objects.filter(
            classification='PCL',
        ).exclude(identifier='NAZ_IT').aggregate(Union('geometry'))['geometry__union']
        estero.save()

        # generate electoral ripartizioni (estero)
        eu, created = Area.objects.get_or_create(
            identifier="ELECT_RIP_EU",
            defaults={
                'name': 'Europa',
                'classification': 'ELECT_RIP',
            }
        )
        eu.geometry = Area.objects.filter(
            classification='PCL',
            parent__identifier='CONT_EU',
        ).exclude(identifier='NAZ_IT').aggregate(Union('geometry'))['geometry__union']
        eu.add_identifier('RI1', 'MININT_CODE')
        eu.save()

        ammer, created = Area.objects.get_or_create(
            identifier="ELECT_RIP_AMMER",
            defaults={
                'name': 'America meridionale',
                'classification': 'ELECT_RIP',
            }
        )
        ammer.geometry = Area.objects.filter(
            classification='CONT',
            identifier='CONT_SA',
        ).aggregate(Union('geometry'))['geometry__union']
        ammer.add_identifier('RI2', 'MININT_CODE')
        ammer.save()

        amcs, created = Area.objects.get_or_create(
            identifier="ELECT_RIP_AMCS",
            defaults={
                'name': 'America settentrionale e centrale',
                'classification': 'ELECT_RIP',
            }
        )
        amcs.geometry = Area.objects.filter(
            classification='CONT',
            identifier='CONT_NA',
        ).aggregate(Union('geometry'))['geometry__union']
        amcs.add_identifier('RI3', 'MININT_CODE')
        amcs.save()

        aaoa, created = Area.objects.get_or_create(
            identifier="ELECT_RIP_AAOA",
            defaults={
                'name': 'Africa Asia Oceania Antartide',
                'classification': 'ELECT_RIP',
            }
        )
        aaoa.geometry = Area.objects.filter(
            classification='CONT',
            identifier__in=['CONT_AF', 'CONT_OC', 'CONT_AN'],
        ).aggregate(Union('geometry'))['geometry__union']
        aaoa.add_identifier('RI4', 'MININT_CODE')
        aaoa.save()

        eu.parent = estero
        self.logger.info("End of import process")
