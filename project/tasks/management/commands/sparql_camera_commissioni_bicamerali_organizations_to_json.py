import re
from typing import Dict, List, Optional

from project.tasks.management.base import SparqlToJsonCommand
from project.tasks.parsing.sparql.utils import get_bindings, read_raw_rq


class Command(SparqlToJsonCommand):
    json_filename = "camera_commissioni_bicamerali_organizations"

    def query(self, **options) -> Optional[List[Dict]]:

        sparql_query = "camera_commissioni_bicamerali_organizations_17.rq"
        q = re.sub(
            r"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_\d+>",
            f"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_{options['legislature']}>",
            read_raw_rq(sparql_query),
        )
        return get_bindings(
            "http://dati.camera.it/sparql",
            q, method="POST"
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        return self.handle_camera_commissioni_organizations_bindings(bindings, **options)
