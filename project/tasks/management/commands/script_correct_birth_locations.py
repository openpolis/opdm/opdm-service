import pandas as pd
from django.core.exceptions import ObjectDoesNotExist

from popolo.models import Area, Person
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Corrections for persons birth locations"

    def add_arguments(self, parser):
        parser.add_argument(
            "--source-url",
            dest="source_url",
            help="Source of the CSV file (http[s]://, file:///, or a relative path). Use ';' as separator.",
            default="data/birth_locations_mismatches.csv"
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        source_url = options["source_url"]
        self.logger.info("Start")

        # load your data
        dataframe = pd.read_csv(source_url, delimiter=";")

        # loop over your data
        for index, row in dataframe.iterrows():
            try:
                # fetch the area with id equal to second column
                area = Area.objects.get(id=row['birth_location_area_id'])
                # fetch the person with birth_location equal to first column
                persons = Person.objects.filter(birth_location__iexact=row['birth_location'])
                # set the birth_location_area_id to the second column
                n_updates = persons.update(birth_location_area=area)
                if n_updates == 0:
                    self.logger.info(f"No persons updated for {row['birth_location']}")
                else:
                    self.logger.info(f"Updated {n_updates} persons for {row['birth_location']}")

            # handle case of missing area or person
            except ObjectDoesNotExist:
                self.logger.warning(
                    f'Person with birth_location: {row["birth_location"]} or '
                    f'Area with id: {row["birth_location_area_id"]} not found, skipping'
                )

        self.logger.info("End")
