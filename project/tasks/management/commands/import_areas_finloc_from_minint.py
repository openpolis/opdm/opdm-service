# -*- coding: utf-8 -*-
from ooetl.extractors import CSVExtractor
from ooetl.transformations import Transformation
from popolo.models import Area
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import CSVDiffCompositeETL
from project.tasks.etl.loaders import PopoloLoader
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class FinlocCodesTransformation(Transformation):
    """Filter and adapt data in CSV preparing it for the loader
    """

    ente_type = None

    def __init__(self, ente_type: str):
        self.ente_type = ente_type

        super(FinlocCodesTransformation, self).__init__()

    @staticmethod
    def filter_rows(od):
        # return filter(lambda x:x['family_name'] == 'Forteleoni', od)
        return od

    def transform(self):
        """Transform the dataframe read from the CSV file, renaming and filtering columns,
        filtering rows, according to ente_type value;
        rewrite location and province names, replacing the combinations <vowel + apostrophe>,
        with the right unicode character (ie: I' => ì);
        """
        self.logger.info("start of transform")
        od = self.etl.original_data

        od.rename(
            columns={
                "codice ente": "cod_ente",
                "codice catastale": "cod_catasto",
                "tipo ente": "tipo_ente",
                "anno inizio": "anno_inizio",
                "anno fine": "anno_fine",
                "descrizione": "den_it"
            }, inplace=True
        )

        # apply tipo_ente filtering
        od = od[(od.tipo_ente == self.ente_type)]

        od = od.filter(
            items=[
                "den_it",
                "cod_catasto",
                "cod_ente",
                "provincia",
                "regione",
                "tipo_ente",
                "anno_inizio",
                "anno_fine"
            ]
        )
        od = od.dropna(how="any", subset=["cod_catasto", "den_it"])

        def lower_and_replace_accents(x):
            return x.lower().\
                replace("a'", "à").replace("e'", "è").\
                replace("i'", "ì").replace("o'", "ò").replace("u'", "ù")

        od["den_it"] = od["den_it"].apply(lower_and_replace_accents)
        od["provincia"] = od["provincia"].apply(lower_and_replace_accents)

        od["anno_inizio"] = od["anno_inizio"].astype(int)
        od["anno_fine"] = od["anno_fine"].astype(int)

        # store processed data into the Transformation instance
        self.etl.processed_data = od


class PopoloAreaFinlocCodesLoader(PopoloLoader):
    """Fetch the correct area and load the MININT_FINLOC identifier.
    No start_date and end_date fields are provided, as this identifiers usually do not change.
    """
    def load_item(self, area, **kwargs):
        self.logger.debug(area['den_it'])

        # get the Area
        a = None
        if area["cod_catasto"]:
            try:
                a = Area.objects.get(identifier=area["cod_catasto"])
            except Area.DoesNotExist:
                pass

        if a is None:
            try:
                a = Area.objects.get(
                    name__iexact=area['den_it'],
                    parent__name__iexact=area['provincia'],
                )
            except Area.DoesNotExist:
                self.logger.error(
                    "Could not find area for {0}, {1}, {2}".format(
                        area['den_it'], area['provincia'], area['cod_catasto']
                    )
                )

        try:
            a.add_identifier(
                scheme='MININT_FINLOC',
                identifier=area['cod_ente'],
                source=self.etl.source,
            )
            self.logger.info("{0} added to {1} ({2})".format(area['cod_ente'], a.name, a.parent.identifier))
        except Exception as e:
            self.logger.error(
                "While importing MININT_FINLOC identifiers for {0}, "
                "got the following exceptions: {1}".format(a, e)
            )


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Import Areas FINLOC codes from minint"

    csv_url = (
        "https://s3.eu-central-1.amazonaws.com/finanzalocale/anagrafica_enti.csv"
    )
    # csv_url = (
    #     "file:///home/gu/Workspace/opdm-service/resources/data"
    #     "/anagrafica_enti.csv"
    # )
    filename = "anagrafica_enti.csv"
    encoding = "latin1"
    sep = ";"
    unique_idx_cols = (3,)

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        # execute handle in mixin and parent,

        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.handle_cache(*args, **options)

        self.logger.info("Starting composite ETL process")

        CSVDiffCompositeETL(
            extractor=CSVExtractor(
                self.csv_url,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=FinlocCodesTransformation(ente_type='CO'),
            loader=PopoloAreaFinlocCodesLoader(),
            clear_cache=self.clear_cache,
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename=self.filename,
            unique_idx_cols=self.unique_idx_cols,
            log_level=self.logger.level,
            logger=self.logger,
        )()

        self.logger.info("End of ETL process")
