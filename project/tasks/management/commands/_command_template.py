# coding=utf-8

from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Custom django-admin command"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action="store_true",
            help="Do not save in the DB"
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        try:
            dry_run = options["dry_run"]
            self.logger.info("Start loading procedure")
            self.logger.warning(f"dry-run variable set to {dry_run}")
            self.logger.info("End loading procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes
