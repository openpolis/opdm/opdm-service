# -*- coding: utf-8 -*-
import os
import zipfile

import requests
from django.contrib.gis.db.models import Union
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import Polygon, MultiPolygon
from popolo.models import Area, AreaRelationship
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """This command will import the countries geometries from a given shp file, compressed as zip file.

    The shapefile is in ./data/shp/world_countries_shp.zip;
    it is a WGS 84 projection and has been downloaded from
    https://hub.arcgis.com/datasets/a21fdb46d23e4ef896f31475217cbb08_1/data,
    simplified to 50%, and repaired through Mapshaper.com

    Other details are taken from geoname.org's API.
    """
    help = "Import Areas geographic limits for world countries from an archived shp file"
    shp_file = None
    overwrite = False

    def add_arguments(self, parser):
        parser.add_argument(
            dest='shp_file',
            help="Absolute path to the compressed shp file"
        )
        parser.add_argument(
            '--overwrite',
            dest='overwrite', action='store_true',
            help="Whether to overwrite all geometries or only write missing ones"
        )

    def handle(self, *args, **options):

        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.shp_file = options['shp_file']
        self.overwrite = options['overwrite']

        self.logger.info("Starting import process.")

        features = geonames_it = None
        try:
            geonames_it = self.fetch_geonames_dict()
            features = self.fetch_shp_dict()
        except Exception as e:
            self.logger.error(e)
            exit(-1)

        if features and geonames_it:

            # create earth and continents
            continents = {(f['continentName'], f['continent']) for f in geonames_it.values()}
            earth, created = Area.objects.get_or_create(
                identifier="EARTH",
                defaults={
                    'name': 'Terra',
                }
            )
            for c_name, c_code in continents:
                Area.objects.get_or_create(
                    identifier=f"CONT_{c_code}",
                    defaults={
                        'name': c_name,
                        'classification': 'CONT',
                        'parent': earth
                    }
                )

            independent_features = {code: feat for code, feat in features.items() if feat.get('AFF_ISO') == code}
            dependent_features = {code: feat for code, feat in features.items() if feat.get('AFF_ISO') != code}

            n = 0
            for code, feature in independent_features.items():
                self.update_or_create_area(code, feature, 'PCL', geonames_it)
                n = n + 1
            self.logger.info(f"{n} independent geometries updated or created")

            n = 0
            for code, feature in dependent_features.items():
                self.update_or_create_area(code, feature, 'PCLD', geonames_it)
                n = n + 1
            self.logger.info(f"{n} dependent geometries updated or created")

            # generate continents' boundaires
            for c_name, c_code in continents:
                continent = Area.objects.get(
                    identifier=f"CONT_{c_code}",
                )

                continent.geometry = Area.objects.filter(
                    classification__in=['PLC', 'PLCD'], parent=continent
                ).aggregate(Union('geometry'))['geometry__union']
                continent.save()

            # generate electoral area 'ESTERO'
            estero, created = Area.objects.get_or_create(
                identifier="ELECT_COLL_ESTERO",
                defaults={
                    'name': 'ESTERO',
                    'classification': 'ELECT_COLL_ESTERO',
                }
            )

            # generate electoral ripartizioni (estero)
            eu, created = Area.objects.get_or_create(
                identifier="ELECT_RIP_EU",
                defaults={
                    'name': 'Europa',
                    'classification': 'ELECT_RIP',
                }
            )
            eu.geometry = Area.objects.filter(
                classification='PLC',
                parent__identifier='CONT_EU',
            ).exclude(identifier='NAZ_IT').aggregate(Union('geometry'))['geometry__union']
            eu.save()

            ammer, created = Area.objects.get_or_create(
                identifier="ELECT_RIP_AMMER",
                defaults={
                    'name': 'America meridionale',
                    'classification': 'ELECT_RIP',
                }
            )
            ammer.geometry = Area.objects.filter(
                classification='CONT',
                identifier='CONT_SA',
            ).aggregate(Union('geometry'))['geometry__union']
            ammer.save()

            amcs, created = Area.objects.get_or_create(
                identifier="ELECT_RIP_AMCS",
                defaults={
                    'name': 'America settentrionale e centrale',
                    'classification': 'ELECT_RIP',
                }
            )
            amcs.geometry = Area.objects.filter(
                classification='CONT',
                identifier='CONT_NA',
            ).aggregate(Union('geometry'))['geometry__union']
            amcs.save()

            aaoa, created = Area.objects.get_or_create(
                identifier="ELECT_RIP_AAOA",
                defaults={
                    'name': 'Africa Asia Oceania Antartide',
                    'classification': 'ELECT_RIP',
                }
            )
            aaoa.geometry = Area.objects.filter(
                classification='CONT',
                identifier__in=['CONT_AF', 'CONT_OC', 'CONT_AN'],
            ).aggregate(Union('geometry'))['geometry__union']
            aaoa.save()

            estero.geometry = Area.objects.filter(
                classification='PLC',
            ).exclude(identifier='NAZ_IT').aggregate(Union('geometry'))['geometry__union']
            estero.save()

        self.logger.info("End of import process")

    def update_or_create_area(self, feat_iso_code, feat, classification, geonames):
        """Insert or update an Area instance, with geometry, identifiers and sources

        :param feat_iso_code:  the 2-letters 3166 ISO code
        :param feat:           the complete feature, from the layer
        :param classification: the geonames classification (PCL, PCLD)
        :param geonames:       a dict containing geonames data for the country
        :return:
        """
        geo_data = geonames[feat_iso_code]
        area, created = Area.objects.update_or_create(
            identifier=f"NAZ_{feat.get('ISO')}",
            istat_classification='NAZ',
            defaults={
                'name': geo_data['countryName'],
                'classification': classification,
                'inhabitants': int(geo_data['population'])
            }
        )
        geom = feat.geom.transform(4326, clone=True)
        geos = geom.geos

        if isinstance(geos, Polygon):
            geos = MultiPolygon(geos)
        area.geometry = geos

        area.parent = Area.objects.get(identifier=f"CONT_{geo_data['continent']}")

        area.add_identifier(geo_data['countryCode'], 'COUNTRY_ISO_ALPHA_2')
        area.add_identifier(geo_data['isoAlpha3'], 'COUNTRY_ISO_ALPHA_3')
        area.add_identifier(geo_data['fipsCode'], 'COUNTRY_FIPS')
        area.add_identifier(geo_data['isoNumeric'], 'COUNTRY_ISO_NUMERIC')
        area.add_identifier(geo_data['geonameId'], 'COUNTRY_GEONAMES_ID')

        area.add_source("https://hub.arcgis.com/datasets/esri::world-countries-generalized")
        area.add_source("http://api.geonames.org/")

        if classification == 'PCLD':
            mother_area = Area.objects.get(identifier=f"NAZ_{feat.get('AFF_ISO')}")
            area.add_relationship(mother_area, AreaRelationship.CLASSIFICATION_TYPES.depends_on)

        area.save()
        if created:
            self.logger.debug(
                f"area and geometry created for area {area.name}, with identifier {area.identifier}"
            )
        else:
            self.logger.debug(
                f"geometry created for area {area.name}, with identifier {area.identifier}"
            )

        return area

    def fetch_shp_dict(self):
        """Return geo layer from zipped shp file

        :return: a DataSource layer (iterable for features)
        """
        if not os.path.exists(self.shp_file):
            self.logger.error(f"File {self.shp_file} was not found. Terminating.")
            return None

        # unzip all files in /tmp (needed), to create a datasource
        with zipfile.ZipFile(self.shp_file) as iz:
            shp_filename = [n for n in iz.namelist() if '.shp' in n][0]
            iz.extractall(path="/tmp")
            ds = DataSource(f"/tmp/{shp_filename}")
            self.logger.info(f"geographic features layer read from {self.shp_file}")

        # extract layer
        layer = ds[0]
        if layer is None:
            raise Exception("No layer found in shp file. Terminating.")
        elif 'ISO' not in layer.fields:
            raise Exception("Wrong fields in shp file. Terminating.")

        # transform the layer into a dict
        features_list = list(layer)
        features_dict = {feat.get('ISO'): feat for feat in features_list}
        return features_dict

    def fetch_geonames_dict(self, language='it'):
        """Read a few country details from geonames.org and return them as a dictionary by 3166 2-digit codes

        :param language:
        :return: a dictionary, containing details, with ISO 3166 2-digit code as key
        """
        r = requests.get(
            f"http://api.geonames.org/countryInfoJSON?formatted=true&lang={language}&username=openpolis&style=full"
        )
        if r.status_code != 200:
            raise Exception(f"Could not access geonames API: {r.reason}")

        geonames = r.json()
        self.logger.info(f"country details read from geonames'API; language: {language}")
        return {country['countryCode']: country for country in geonames['geonames']}
