# -*- coding: utf-8 -*-
import locale
import logging
import os
import re

from django.core.management import BaseCommand
from ooetl import ETL
from ooetl.extractors import LocalCSVExtractor
from ooetl.loaders import CSVLoader
import requests


class Istat2CatETL(ETL):
    """Instance of ``ooetl.ETL`` class that handles
    importing historical istat 2 cadastre codes
    """

    def transform(self):
        # get a copy of the original dataframe
        od = self.original_data.copy()

        storiacomuni_url_fmt = (
            "http://www.elesh.it/storiacomuni" "/storia_comune.asp?istat={0}"
        )

        def get_cat_code(r):
            """fetch Catasto code from remote URL in r.istat_code

            :param r:
            :return:
            """

            istat_code = r.ISTAT_CODE_COM
            storiacomuni_url = storiacomuni_url_fmt.format(istat_code)

            self.logger.info("Request sent to: {0}".format(storiacomuni_url))
            resp = requests.get(storiacomuni_url)
            if resp.status_code != 200:
                self.logger.error(
                    "Could not send GET request to {0}. Skipping.".format(
                        storiacomuni_url
                    )
                )
            else:
                m = re.match(
                    r'.*codice catastale.*"(\w\d\d\d)".*',
                    str(resp.content),
                    re.MULTILINE + re.I,
                )
                if m is None:
                    self.logger.error(
                        "Could not find Codice Catastale in {0}. "
                        "Skipping.".format(storiacomuni_url)
                    )
                else:
                    cat_code = m.group(1)
                    self.logger.info(
                        "Istat {0} => Cat {1}".format(istat_code, cat_code)
                    )
                    return cat_code

        od["CAT_CODE"] = od.apply(lambda r: get_cat_code(r), axis=1)

        # store processed data into the ETL instance
        self.processed_data = od

        # return ETL instance
        return self


class Command(BaseCommand):
    help = "Fetch old catasto codes from "

    logger = logging.getLogger(__name__)

    # change locale ti correctly pare data
    locale.setlocale(locale.LC_ALL, "it_IT.UTF-8")

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("### Start")

        etl = Istat2CatETL(
            extractor=LocalCSVExtractor(
                abs_path=os.path.abspath("../resources/data/old_istat_codes.csv"),
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=CSVLoader(
                label="istat2cat", csv_path=os.path.abspath("../resources/data")
            ),
            log_level=self.logger.level,
        )

        self.logger.info("Starting ETL process")
        etl.etl()

        self.logger.info("### End")
