import re
from typing import Optional, List, Dict

from project.tasks.management.base import SparqlToJsonCommand
from project.tasks.parsing.sparql.utils import get_bindings, read_raw_rq


class Command(SparqlToJsonCommand):
    json_filename = "camera_commissioni_memberships"

    def query(self, **options) -> Optional[List[Dict]]:
        q = re.sub(
            r"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_\d+>",
            f"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_{options['legislature']}>",
            read_raw_rq("camera_commissioni_memberships_17.rq"),
        )
        bindings = get_bindings(
            "http://dati.camera.it/sparql",
            q, method="POST"
        )
        return bindings

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        return self.handle_camera_commissioni_memberships_bindings(bindings, **options)
