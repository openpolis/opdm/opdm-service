# -*- coding: utf-8 -*-
import logging

from django.core.management import BaseCommand
from django.db.models import Sum, Q
from popolo.models import Area


class Command(BaseCommand):
    help = (
        "Update inhabitants for province, "
        "metropolitan areas and regions,"
        "as sum of lower entities inhabitants."
    )

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Update started")
        if Area.objects.count() == 0:
            raise Exception("No Area objects found!")

        self.logger.info("PROVINCE e METROPOLI")
        annotated_province = Area.objects.filter(
            Q(istat_classification=Area.ISTAT_CLASSIFICATIONS.provincia)
            | Q(istat_classification=Area.ISTAT_CLASSIFICATIONS.metro)
        ).annotate(ab=Sum("children__inhabitants"))
        for a in annotated_province.filter(end_date__isnull=True):
            self.logger.info("{0}: {1}".format(a.name, a.ab))
            a.inhabitants = a.ab
            a.save()

        self.logger.info("REGIONI")
        annotated_regioni = Area.objects.filter(
            istat_classification=Area.ISTAT_CLASSIFICATIONS.regione
        ).annotate(ab=Sum("children__inhabitants"))
        for a in annotated_regioni.filter(end_date__isnull=True):
            self.logger.info("{0}: {1}".format(a.name, a.ab))
            a.inhabitants = a.ab
            a.save()

        self.logger.info("RIPARTIZIONI")
        annotated_ripartizioni = Area.objects.filter(
            istat_classification=Area.ISTAT_CLASSIFICATIONS.ripartizione
        ).annotate(ab=Sum("children__inhabitants"))
        for a in annotated_ripartizioni.filter(end_date__isnull=True):
            self.logger.info("{0}: {1}".format(a.name, a.ab))
            a.inhabitants = a.ab
            a.save()

        self.logger.info("Update finished")
