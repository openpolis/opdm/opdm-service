import re
from collections import defaultdict
from typing import Optional, List, Dict, Tuple, Any

from django.contrib.contenttypes.models import ContentType
from popolo.models import Organization, KeyEvent

from project.tasks.management.base import SparqlToJsonCommand
from project.tasks.parsing.common import identifiers_map, parse_date
from project.tasks.parsing.sparql.utils import get_bindings, read_raw_rq, get_person_from_binding


class Command(SparqlToJsonCommand):
    json_filename = "senato_commissioni_memberships"

    @staticmethod
    def get_legislature_dates(n_leg: int) -> Tuple:
        legislature_qs = KeyEvent.objects.filter(event_type='ITL')
        start_date = dict(
            legislature_qs.values_list('identifier', 'start_date')
        ).get(f"ITL_{n_leg:02d}")
        end_date = dict(
            legislature_qs.values_list('identifier', 'end_date')
        ).get(f"ITL_{n_leg:02d}")
        return start_date, end_date

    def query(self, **options) -> Optional[List[Dict]]:
        n_leg = options['legislature']
        start_date, end_date = self.get_legislature_dates(n_leg)

        # adapt raw_rq to all legislatures
        raw_rq = read_raw_rq("senato_commissioni_memberships_17.rq")
        raw_rq = re.sub(
            r"2013-03-15",
            f"{start_date}",
            raw_rq
        )
        if (end_date or '') != '':
            raw_rq = re.sub(
                r"2018-03-22",
                f"{end_date}",
                raw_rq
            )
        else:
            raw_rq = re.sub(
                r'FILTER\(\?membership__start_date < \"2018-03-22T12:00:00\+01:00\"\^\^xsd:dateTime\)',
                "",
                raw_rq
            )
        raw_rq = re.sub(
            r"osr:legislatura \d+",
            f"osr:legislatura {options['legislature']:02d}",
            raw_rq
        )

        return get_bindings(
            "https://dati.senato.it/sparql",
            raw_rq
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:

        n_leg = options["legislature"]

        osr_uri_to_org_id = identifiers_map(
            scheme="OSR-URI+",
            content_type=ContentType.objects.get_for_model(Organization)
        )
        org_id_to_name = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OSR-URI+"
            ).values_list("id", "name")
        }
        org_id_to_classification = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OSR-URI+"
            ).values_list("id", "classification")
        }

        stacked_dates = defaultdict(list)
        for binding in bindings:
            key = (
                binding["person__id"].value,
                binding["membership__organization_id"].value,
                binding["membership__start_date"].value
            )
            if "membership__end_date" in binding:
                stacked_dates[key].append(binding["membership__end_date"].value)

        buffer = {}
        for binding in bindings:

            tmp: Dict[str, Any] = buffer.get(
                binding["person__id"].value,
                {
                    **get_person_from_binding(binding, "OSR-URI"),
                },
            )

            _id = f"{binding['membership__organization_id'].value}"
            if _id and "presidenza" not in _id:
                _id += f"-{n_leg}"
            org_id = osr_uri_to_org_id.get(_id)

            if not org_id:
                self.logger.debug(
                    f"\"{binding['membership__organization_id'].value}\" does not exist, skipping.."
                )
                continue

            if (
                "membership__role" in binding and
                binding["membership__role"].value and
                binding["membership__role"].value != "Membro"
            ):
                carica = binding["membership__role"].value.title()
            else:
                carica = "Componente"

            tmp_start_date = parse_date(binding["membership__start_date"].value)
            key = (
                binding["person__id"].value,
                binding["membership__organization_id"].value,
                binding["membership__start_date"].value
            )
            if "membership__end_date" in binding:
                try:
                    tmp_end_date = sorted(filter(
                        lambda x: x > binding['membership__start_date'].value,
                        stacked_dates[key]
                    ))[0]
                except IndexError:
                    tmp_end_date = None
            else:
                tmp_end_date = None

            # skip wrong dates
            if tmp_end_date and tmp_end_date <= tmp_start_date:
                tmp_end_date = None

            # skip repeating memberships
            if sum((x['start_date'], x['organization_id']) == (tmp_start_date, org_id) for x in tmp['memberships']):
                continue

            org_name = org_id_to_name[org_id]
            org_class = org_id_to_classification[org_id]

            if org_class == 'Organo di presidenza parlamentare':
                carica = carica\
                    .replace("Del Senato", "")\
                    .replace("Della Presidenza", "")\
                    .replace("Vice Presidente", "Vicepresidente")\
                    .replace("Vice presidente", "Vicepresidente")\
                    .strip()

            tmp["memberships"].append(
                {
                    "organization_id": org_id,
                    "role": f"{carica} {org_class}",
                    "label": f"{carica} {org_name}",
                    "start_date": tmp_start_date,
                    "end_date": tmp_end_date,
                    "sources": [
                        {
                            "note": "Open Data Senato",
                            "url": "https://dati.senato.it/"
                        }
                    ],
                }
            )

            buffer[binding["person__id"].value] = tmp

        output = [
            {
                **values,
                "memberships": values["memberships"],
            }
            for persona, values in buffer.items()
        ]

        return output
