# coding=utf-8

from django.db.models import F
from popolo.models import Area
from taskmanager.management.base import LoggingBaseCommand

from config.settings import RESOURCES_PATH
from project.tasks.checks import Check


class Command(LoggingBaseCommand):
    """
    Implements https://gitlab.depp.it/openpolis/opdm/opdm-project/issues/105
    """

    help = "Custom django-admin command"
    context = None
    staging_base_url = None
    com_filter = None
    min_inhabitants = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--staging-base-url",
            dest="staging_base_url",
            action="store_true",
            help="Use staging urls (https://staging.service.opdm.openpolis.io/, ...) instead of production",
        )
        parser.add_argument(
            "--context",
            dest="context",
            default="all",
            help="Context to verify (reg|com|all)",
        )
        parser.add_argument(
            "--com-filter",
            dest="com_filter",
            default=None,
            help="Filter for comuni areas: <parent__identifier='RM',end_date__isnull=False>",
        )
        parser.add_argument(
            "--min-inhabitants",
            dest="min_inhabitants",
            type=int,
            default=0,
            help="Inhabitants threshold for com context. Only cities with more than this are considered",
        )
        parser.add_argument(
            "--generate-report",
            dest="generate_report",
            action="store_true",
            help="Produce a CSV report as output. Default to False.",
        )

        super(Command, self).add_arguments(parser)

    # noinspection PyTypeChecker
    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.context = options["context"]
        self.staging_base_url = options["staging_base_url"]
        self.com_filter = options["com_filter"]
        self.min_inhabitants = options["min_inhabitants"]
        self.logger.info("Start of ETL process")

        try:

            ck = Check(
                self.staging_base_url,
                logger=self.logger,
                generate_report=options["generate_report"],
            )

            if self.context.lower()[:3] in ["reg", "all"]:
                qs = (
                    Area.objects.filter(
                        istat_classification=Area.ISTAT_CLASSIFICATIONS.regione
                    )
                    .select_related("parent", "parent__parent")
                    .prefetch_related("organizations", "children")
                )
                for area in qs:
                    ck.regione(area)

            if self.context.lower()[:3] in ["com", "all"]:
                qs = Area.objects.filter(
                    istat_classification=Area.ISTAT_CLASSIFICATIONS.comune
                )
                if self.com_filter:
                    filters_dict = dict(
                        map(lambda x: x.strip(), i.split("="))
                        for i in self.com_filter.split(",")
                    )
                    for k, v in filters_dict.items():
                        self.logger.debug(f"Filter {k}: {v}")
                        if v == "False":
                            filters_dict[k] = False
                        elif v == "True":
                            filters_dict[k] = True
                        else:
                            try:
                                filters_dict[k] = int(v)
                            except ValueError:
                                pass
                    qs = qs.filter(**filters_dict)
                qs = (
                    qs.select_related("parent", "parent__parent")
                    .prefetch_related("organizations", "children")
                    .filter(inhabitants__gte=self.min_inhabitants)
                    .order_by(F("inhabitants").desc(nulls_last=True))
                )

                for area in qs:
                    ck.comune(area)

            if options["generate_report"]:
                ck.report.to_csv(
                    path_or_buf=RESOURCES_PATH.path("data", "discrepancies.csv"),
                    encoding="utf-8",
                    index=False,
                )

        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."

        self.logger.info("End of ETL process")
