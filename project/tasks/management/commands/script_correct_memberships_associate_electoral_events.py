from popolo.models import Membership
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Associate electoral events to non-apical memberships not yet having it"

    def add_arguments(self, parser):  # noqa: D102
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="dry_run",
            help="Only show modificationsm do not save.",
        )
        parser.add_argument(
            "--context",
            dest="context",
            help="The context to apply correctons [com|prov|reg].",
        )

        parser.add_argument(
            "--start-year",
            dest="start_year",
            type=int,
            help="Years to start considering memberships for corrections."
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        dry_run = options["dry_run"]

        start_year = options["start_year"]
        if start_year < 1990 or start_year > 2018:
            raise Exception("Need to use a start_year in the 1990-2018 interval")

        context = options["context"]
        if context.lower() not in ["com", "reg", "prov"]:
            raise Exception("Need to use a context among 'com', 'prov', 'reg'")
        if context.lower() == 'reg':
            classifications = ["Giunta regionale", "Consiglio regionale"]
            excluded_roles = ["Presidente di Regione"]
        elif context.lower() == 'prov':
            classifications = ["Giunta provinciale", "Consiglio provinciale"]
            excluded_roles = ["Presidente di Provincia"]
        else:  # com is the only possibility left at this point
            classifications = ["Giunta comunale", "Consiglio comunale"]
            excluded_roles = ["Sindaco", "Commissario di giunta comunale"]

        self.logger.info("Start")

        ms = Membership.objects.filter(
            electoral_event__isnull=True,
            start_date__gte='2012',
            organization__classification__in=classifications
        ).exclude(role__in=excluded_roles)

        nms = ms.count()
        for i, m in enumerate(ms, start=1):
            if i % 100 == 0:
                self.logger.info(f"{i}/{nms}")

            electoral_event = m.get_electoral_event(logger=self.logger)
            if not dry_run:
                m.electoral_event = electoral_event
                try:
                    m.save()
                except Exception as e:
                    self.logger.warning(f" {m.id} {e}")

        self.logger.info("Finished")
