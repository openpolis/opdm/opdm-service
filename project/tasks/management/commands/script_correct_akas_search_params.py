import logging
import re

from django.core.management import BaseCommand
from django.db import transaction

from project.akas.models import AKA


class Command(BaseCommand):
    help = "Adjust birth_location, stripping content within parenthesis."

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start")

        akas = AKA.objects.filter(search_params__birth_location__icontains="(")
        n_akas = akas.count()

        with transaction.atomic():

            for n, aka in enumerate(akas):
                if n % 1000 == 0:
                    print("{0}/{1}".format(n, n_akas))
                aka.search_params["birth_location"] = re.sub(
                    r"(.*) \(.*\)", r"\1", aka.search_params["birth_location"].lower()
                )
                aka.save()

        self.logger.info("End")
