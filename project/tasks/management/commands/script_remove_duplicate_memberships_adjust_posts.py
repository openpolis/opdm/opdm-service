import logging

from django.core.management import BaseCommand
from django.db.models import Count
from popolo.models import RoleType, Person


class Command(BaseCommand):
    """
    This solves the issue https://gitlab.depp.it/openpolis/opdm/opdm-project/issues/77,
    """

    help = "Remove duplicate memberships arising from wrong Posts, due to wrong RoleType labels"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start")

        # correct a RoleType label
        RoleType.objects.filter(label="Membro della conferenza metropolitana").update(
            label="Membro di conferenza metropolitana"
        )

        # find persons with double memberships
        for d in (
            Person.objects.values("pk", "memberships__organization__pk")
            .annotate(n_memberships=Count("memberships"))
            .filter(n_memberships__gt=1)
        ):
            for rt in [
                "presidente di consiglio regionale",
                "presidente di consiglio comunale",
                "vicepresidente di consiglio regionale",
                "vicepresidente di consiglio comunale",
            ]:
                p = Person.objects.get(pk=d["pk"])
                d_memberships = p.memberships.filter(role__iexact=rt)
                if d_memberships.count() == 2:
                    print(
                        d_memberships.filter(role__contains="Consiglio").first(),
                        d_memberships.filter(role__contains="Consiglio").first().role,
                    )
                    d_memberships.filter(role__contains="Consiglio").first().delete()
        self.logger.info("End")
