from django.db import transaction
from django.db.models.signals import post_save
from popolo.models import Person
from taskmanager.management.base import LoggingBaseCommand

from project.api_v1.signals import add_cf_to_person
from project.core.person_utils import compute_cf, CFException


class Command(LoggingBaseCommand):
    help = "Find persons with full anagraphical data, but no CF identifier and compute it."
    dry_run = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--lookup-strategy",
            dest="lookup_strategy",
            default="identifier",
            help="Strategy to find duplicates: identifier, manual.",
        )
        parser.add_argument(
            "--lookup-manual-ids",
            dest="lookup_manual_ids",
            nargs="*",
            help="List of duplicates ids, each of the form: id_src:id_dest_1:id_dest_2 ..."
        )
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action="store_true",
            help="Do not save in the DB"
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Start of procedure")

        lookup_strategy = options["lookup_strategy"]
        lookup_manual_ids = options.get("lookup_manual_ids") or []

        dry_run = options["dry_run"]

        # disconnect add_cf_to_person callback from post_save signal
        post_save.disconnect(add_cf_to_person, sender=Person)

        persons_ids = self.find_missing_cfs(
            lookup_strategy,
            lookup_manual_ids=lookup_manual_ids,
            logger=self.logger
        )
        persons = Person.objects.filter(
            id__in=persons_ids, birth_date__isnull=False, birth_location__isnull=False
        )
        n_persons = len(persons)
        self.logger.info(f"{n_persons} persons with missing CF found")
        unmapped_locations = set()
        for n, p in enumerate(persons, start=1):
            self.logger.debug(f"processing {p}")
            try:
                with transaction.atomic():
                    if (
                        p.family_name and p.family_name.strip() != '' and
                        p.given_name and p.given_name.strip() != '' and
                        p.birth_date and p.birth_date.strip() != '' and
                        p.birth_location and p.birth_location.strip() != '' and
                        p.gender
                    ):
                        cf = compute_cf(
                            {
                                'family_name': p.family_name,
                                'given_name': p.given_name,
                                'birth_date': p.birth_date,
                                'gender': p.gender,
                                'birth_location': p.birth_location,
                            }
                        )
                        if cf:
                            if not dry_run:
                                p.add_identifier(scheme='CF', identifier=cf)
                                p.save()
                            self.logger.debug(f"{cf} computed for {p}")
                        else:
                            self.logger.warning(f"empty CF for {p}")
            except CFException as e:
                if "ND" not in str(e):
                    if "\"birthplace\" argument cant be None" in str(e):
                        unmapped_locations.add(p.birth_location)
                    self.logger.warning(f"{e} while computing CF for {p} ({p.birth_date}, {p.birth_location})")

            if n % 25 == 0:
                self.logger.info(f"{n}/{n_persons}")

        # extract distinct bith locations and print them
        self.logger.info("Showing birth locations not mapped to CF codes:")
        for loc in unmapped_locations:
            self.logger.info(loc)

        self.logger.info("End of procedure")

    @staticmethod
    def find_missing_cfs(lookup_strategy: str, **kwargs) -> set:
        """Use lookup_strategy to look for duplicates in Persons
        :param lookup_strategy: the strategy used to look for duplicates (identifier, manual)
        :param kwargs: other parameters used in strategies: lookup_manual_ids

        :return: list of tuples
        """
        lookup_manual_ids = kwargs.get('lookup_manual_ids', [])

        if lookup_strategy == 'identifier':
            persons_with_cfs = set(
                Person.objects.filter(identifiers__scheme='CF').values_list('id', flat=True)
            )
            persons = set(Person.objects.values_list('id', flat=True))
            persons_with_no_cfs = persons - persons_with_cfs
        else:
            persons_with_no_cfs = lookup_manual_ids

        return persons_with_no_cfs
