from datetime import datetime
import re
from typing import Optional, List, Dict, Any

from popolo.models import Organization

from tasks.management.base import SparqlToJsonCommand
from tasks.parsing.common import identifiers_map, parse_date
from tasks.parsing.sparql.camera import get_person_from_binding
from tasks.parsing.sparql.utils import get_bindings, read_raw_rq


class Command(SparqlToJsonCommand):
    json_filename = "camera_governo_memberships"

    def query(self, **options) -> Optional[List[Dict]]:
        q = re.sub(
            r"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_\d+>",
            f"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_{options['legislature']}>",
            read_raw_rq("camera_governo_memberships_17.rq"),
        )
        return get_bindings("http://dati.camera.it/sparql", q, method="POST")

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:

        ocd_uri_to_org_id = identifiers_map(scheme="OCD-URI")
        org_id_to_name = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OCD-URI"
            ).values_list("id", "name")
        }
        org_id_to_dissolution_date = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OCD-URI"
            ).values_list("id", "dissolution_date")
        }
        role_label_map = {
            "SOTTOSEGRETARIO DI STATO": "Sottosegretario di Stato",
            "MINISTRO SENZA PORTAFOGLIO": "Ministro senza Portafoglio",
            "MINISTRO": "Ministro",
            "VICE MINISTRO": "Vice Ministro",
            "PRESIDENTE DEL CONSIGLIO": "Presidente del Consiglio",
            "VICEPRESIDENTE DEL CONSIGLIO": "Vicepresidente del Consiglio",
            "ALTO COMMISSARIO": "Alto Commissario",
            "VICE ALTO COMMISSARIO": "Vice Alto Commissario",
            "ALTO COMMISSARIO AGGIUNTO": "Alto Commissario aggiunto",
            "SOTTOSEGRETARIO DI STATO ALLA PRESIDENZA DEL CONSIGLIO": (
                "Sottosegretario di Stato alla Presidenza del Consiglio"
            ),
        }

        buffer = {}

        for binding in bindings:
            tmp: Dict[str, Any] = buffer.get(
                binding["person__id"].value,
                {
                    **get_person_from_binding(binding),
                },
            )

            org_id = ocd_uri_to_org_id.get(binding["membership__organization_id"].value)
            if not org_id:
                self.logger.debug(
                    f"\"{binding['membership__organization_id'].value}\" does not exist, skipping.."
                )
                continue

            tmp_role = role_label_map.get(binding["membership__role"].value)
            if not tmp_role:
                self.logger.info(
                    f'Unexpected role: {binding["membership__role"].value}'
                )

            # Parse start/end dates
            tmp_start_date = parse_date(binding["membership__start_date"].value)
            if "membership__end_date" in binding:
                tmp_end_date = parse_date(binding["membership__end_date"].value)
            else:
                # Sometimes the binding has no end_date even though the org is closed
                tmp_end_date = org_id_to_dissolution_date.get(org_id)

            # Assemble membership label
            _post = binding["membership__label"].value.split("(")[0].strip()
            _gov_name = org_id_to_name.get(org_id)
            if "membership__label_delega" in binding:
                _delega = f' {binding["membership__label_delega"].value.split("(")[0].strip()} '
            else:
                _delega = " "
            if (
                "membership__label_interim" in binding
                and binding["membership__label_interim"].value == "1"
            ):
                _interim = " (interim)"
            else:
                _interim = ""
            tmp_label = f"{_post}{_delega}del {_gov_name}{_interim}"

            tmp["memberships"].append(
                {
                    "organization_id": org_id,
                    "role": tmp_role,
                    "label": tmp_label,
                    "start_date": tmp_start_date,
                    "end_date": tmp_end_date,
                    "sources": [
                        {"note": "Open Data Camera", "url": "http://dati.camera.it/"}
                    ],
                    "identifiers": [
                        {
                            "scheme": "OCD-URI",
                            "identifier": binding["membership__id"].value,
                        }
                    ],
                }
            )

            buffer[binding["person__id"].value] = tmp

        # Sort out output...
        output = [
            {
                **values,
                "memberships": _refine_memberships(values["memberships"])
            }
            for persona, values in buffer.items()
        ]

        return output


def _refine_memberships(memberships: List[Dict]) -> List[Dict]:
    """
    Apply "ettore" refinements to an array of memberships.

    See: https://gitlab.depp.it/openpolis/opdm/opdm-project/-/issues/284

    :param memberships: a list of "raw" memberships to refine.
    :return: the refined list of dict.
    """

    def days_delta(d1, d2):
        d1 = datetime.strptime(d1, "%Y-%m-%d")
        d2 = datetime.strptime(d2, "%Y-%m-%d")
        return abs((d1 - d2).days)

    tmp = None
    for m in memberships:
        if (
            "ministro" in m["label"].casefold()
            and "ministro senza portafoglio" in m["role"].casefold()
            and "end_date" in m
            and m["end_date"]
            and "start_date" in m
            and m["start_date"]
            and days_delta(m["start_date"], m["end_date"]) <= 10
        ):
            other_memberships = [n for n in memberships if m != n]
            for j, o in enumerate(other_memberships):
                if (
                    "ministro" in o["label"].casefold()
                    and days_delta(o["start_date"], m["end_date"]) <= 1
                ):
                    other_memberships[j]["start_date"] = m["start_date"]
                    tmp = other_memberships

    return tmp or memberships
