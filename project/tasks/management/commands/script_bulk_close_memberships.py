# coding=utf-8
import logging
from urllib.request import urlretrieve

from django.db import transaction
from popolo.models import Organization
from taskmanager.management.base import LoggingBaseCommand

from project.core.dicts_functions import get_organs_dict_by_istat_code

logger = logging.getLogger(__name__)


class Command(LoggingBaseCommand):

    help = (
        "A command which, given a file containing a list of Istat codes (one per line), looks up for the corresponding"
        "administrative units and closes all the current memberships held in local legislative bodies. "
        '(e.g. "Giunta Comunale", "Consiglio Comunale", ecc.)'
    )

    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument("url", metavar="URL", type=str, help="The URL of the file")
        optional = parser._action_groups.pop()
        required = parser.add_argument_group('required arguments')
        parser._action_groups.append(optional)

        required.add_argument(
            "--administrative-level",
            action="store",
            dest="administrative_level",
            choices=["com", "prov", "cm", "reg"],
            required=True,
            help="",
        )
        required.add_argument(
            "--end-date",
            action="store",
            dest="end_date",
            required=True,
            help="The end date to be set on the memberships. Must be in the format YYYY-MM-DD.",
        )
        required.add_argument(
            "--end-reason",
            action="store",
            dest="end_reason",
            required=True,
            help="The end reason to be set on the memberships. ",
        )
        optional.add_argument(
            "--dry-run",
            action="store_true",
            dest="dry_run",
            default=False,
            help="Perform a dry run of this script.",
        )

    @transaction.atomic
    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)

        if options["dry_run"]:
            logger.info("This is a dry run.")

        istat_map = get_organs_dict_by_istat_code(options["administrative_level"])

        file_path, _ = urlretrieve(options["url"])

        with open(file_path) as file:

            for line in file.readlines():
                istat_code = line.strip().zfill(6)
                d = istat_map.get(istat_code)
                if d:
                    try:
                        giunta = Organization.objects.get(id=d["giunta"]["pk"])
                        consiglio = Organization.objects.get(id=d["consiglio"]["pk"])
                    except KeyError as e:
                        self.logger.error(f"{e} for istat code: {istat_code}. Skipping.")
                        continue
                    memberships = (
                        giunta.memberships.current() | consiglio.memberships.current()
                    )
                    n_updated = memberships.update(
                        end_date=options["end_date"], end_reason=options["end_reason"]
                    )
                    logger.info(
                        f"Successfully closed {n_updated} Memberships ({d['name']})"
                    )
                else:
                    logger.error(
                        f"Couldn't find administrative unit with Istat code {istat_code}"
                    )

        if options["dry_run"]:
            transaction.set_rollback(True)
