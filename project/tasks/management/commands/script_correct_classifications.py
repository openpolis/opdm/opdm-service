import csv
from io import StringIO
import logging

from popolo.models import Classification, Organization
import requests
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Corrections for Classifications of Organizations"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.setup_logger(__name__, formatter_key="simple", **options)

        self.logger.info("Start")

        # add unknown classifications
        Classification.objects.get_or_create(
            scheme='FORMA_GIURIDICA_OP',
            code='3710',
            descr='Forza armata o di ordine pubblico e sicurezza'
        )
        Classification.objects.filter(
            scheme="FORMA_GIURIDICA_OP", code="2830"
        ).update(descr="Agenzie, enti e consorzi")

        definizioni = "fonte;classificazione;forma_giuridica_op;occorrenze\r\n" \
            "TIPOLOGIA_SIOPE_BDAP;CAMERE DI COMMERCIO;Camera di commercio\r\n" \
            "CATEGORIA_IPA_BDAP;ISTITUZIONI PER L'ALTA FORMAZIONE ARTISTICA, MUSICALE E COREUTICA - AFAM;" \
            "Istituto e scuola pubblica di ogni ordine e grado\r\n" \
            "CATEGORIA_IPA_BDAP;UNIVERSITA' E ISTITUTI DI ISTRUZIONE UNIVERSITARIA PUBBLICI;Università pubblica\r\n" \
            "TIPOLOGIA_DT_BDAP;SOCIETA' CONSORTILE;Società consortile\r\n" \
            "TIPOLOGIA_DT_BDAP;SOCIETA' COOPERATIVA;Società cooperativa diversa\r\n" \
            "TIPOLOGIA_DT_BDAP;ENTE PUBBLICO ECONOMICO;Ente pubblico economico\r\n" \
            "TIPOLOGIA_DT_BDAP;ENTE PUBBLICO NON ECONOMICO;Altro ente pubblico non economico\r\n" \
            "TIPOLOGIA_DT_BDAP;AZIENDA SPECIALE E DI ENTE LOCALE;Azienda speciale ai sensi del t.u. 267/2000\r\n" \
            "CATEGORIA_IPA_BDAP;ENTI DI REGOLAZIONE DEI SERVIZI IDRICI E O DEI RIFIUTI;" \
            "Altro ente pubblico non economico\r\n" \
            "TIPOLOGIA_DT_BDAP;CONSORZIO;Consorzio di diritto pubblico\r\n" \
            "CATEGORIA_IPA_BDAP;ENTI PUBBLICI PRODUTTORI DI SERVIZI ASSISTENZIALI, RICREATIVI E CULTURALI;" \
            "Altro ente pubblico non economico\r\n" \
            "CATEGORIA_IPA_BDAP;AZIENDE E CONSORZI PUBBLICI TERRITORIALI PER L'EDILIZIA RESIDENZIALE;" \
            "Consorzio di diritto pubblico\r\n" \
            "CATEGORIA_IPA_BDAP;ALTRI ENTI LOCALI;Ente di sviluppo agricolo regionale o di altro ente locale\r\n" \
            "CATEGORIA_IPA_BDAP;COMUNI E LORO CONSORZI E ASSOCIAZIONI;Agenzie, enti e consorzi\r\n" \
            "CATEGORIA_IPA_BDAP;UNIONI DI COMUNI E LORO CONSORZI E ASSOCIAZIONI;Agenzie, enti e consorzi\r\n" \
            "CATEGORIA_IPA_BDAP;ALTRI ENTI LOCALI;Agenzie, enti e consorzi\r\n" \
            "CATEGORIA_IPA_BDAP;ENTI E ISTITUZIONI DI RICERCA PUBBLICI;Istituto o ente pubblico di ricerca\r\n" \
            "CATEGORIA_IPA_BDAP;AGENZIE ED ENTI REGIONALI PER LA FORMAZIONE, LA RICERCA E L'AMBIENTE;" \
            "Ente ambientale regionale\r\n" \
            "CATEGORIA_IPA_BDAP;CONSORZI DI BACINO IMBRIFERO MONTANO;Consorzio di diritto pubblico\r\n" \
            "CATEGORIA_IPA_BDAP;CONSORZI INTERUNIVERSITARI DI RICERCA;Consorzio di diritto pubblico\r\n" \
            "CATEGORIA_IPA_BDAP;PRESIDENZA DEL CONSIGLIO DEI MINISTRI, MINISTERI E AVVOCATURA DELLO STATO;" \
            "Organo costituzionale o a rilevanza costituzionale\r\n" \
            "CATEGORIA_IPA_BDAP;PROVINCE E LORO CONSORZI E ASSOCIAZIONI;Agenzie, enti e consorzi\r\n" \
            "CATEGORIA_IPA_BDAP;COMUNITA' MONTANE E LORO CONSORZI E ASSOCIAZIONI;Agenzie, enti e consorzi\r\n" \
            "CATEGORIA_IPA_BDAP;FORZE DI POLIZIA AD ORDINAMENTO CIVILE E MILITARE PER LA " \
            "TUTELA DELL'ORDINE E DELLA SICUREZZA PUBBLICA;Forza armata o di ordine pubblico e sicurezza\r\n" \
            "TIPOLOGIA_DT_BDAP;ENTE DI DIRITTO PUBBLICO;Ente pubblico economico\r\n" \
            "TIPOLOGIA_DT_BDAP;ASSOCIAZIONI E FONDAZIONI;Associazione riconosciuta\r\n" \
            "CATEGORIA_IPA_BDAP;REGIONI, PROVINCE AUTONOME E LORO CONSORZI E ASSOCIAZIONI;" \
            "Agenzie, enti e consorzi\r\n" \
            "CATEGORIA_IPA_BDAP;ENTI NAZIONALI DI PREVIDENZA ED ASSISTENZA SOCIALE IN CONTO ECONOMICO CONSOLIDATO;" \
            "Altro ente pubblico non economico\r\n" \
            "TIPOLOGIA_DT_BDAP;ALTRO;Altra forma giuridica\r\n"

        reader = csv.DictReader(StringIO(definizioni), delimiter=';')
        for row in reader:
            self.logger.info("{0} => {1}".format(row["classificazione"], row["forma_giuridica_op"]))
            c = Classification.objects.get(scheme="FORMA_GIURIDICA_OP", descr=row["forma_giuridica_op"])
            for o in Organization.objects.filter(
                classifications__classification__scheme=row["fonte"],
                classifications__classification__descr__iexact=row["classificazione"],
                classification__isnull=True
            ):
                o.add_classification_rel(c)
                o.classification = c.descr
                o.save()

        r = requests.get(
            "https://s3.eu-central-1.amazonaws.com/opdm-service-data/unclassified_classifications.csv"
        )
        reader = csv.DictReader(StringIO(r.content.decode("utf8")), delimiter=';')
        for row in reader:
            self.logger.info("{0} => {1}".format(row["name"], row["classification"]))
            try:
                c = Classification.objects.get(
                    scheme="FORMA_GIURIDICA_OP", descr__iexact=row["classification"]
                )
            except Classification.DoesNotExist:
                self.logger.error("Could not find classification for {0}".format(row["classification"]))
                continue
            o_id = int(row["id"].split("/")[-1])
            try:
                o = Organization.objects.get(id=o_id)
                o.add_classification_rel(c)
                o.classification = c.descr
                o.save()
            except Organization.DoesNotExist:
                self.logger.error("Could not find organization for {0}".format(o_id))
                continue

        self.logger.info("End")
