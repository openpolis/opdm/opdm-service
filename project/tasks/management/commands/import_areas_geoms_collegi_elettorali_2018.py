# -*- coding: utf-8 -*-
import os

from django.contrib.gis.db.models import Union
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import Polygon, MultiPolygon
from popolo.models import Area
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """This command will import the 2018 electoral constituencies geometries from given shp files.

    Data come from https://www.istat.it/it/archivio/208278
    and have been transformed into separated, simplified shp files with mapshaper, using:

    mapshaper -i shp/COLLEGI_ELETTORALI_2017.shp \
        -proj wgs84 -simplify 10% visvalingam weighted -clean \
        -o /tmp/collegi_tot.shp

    mapshaper -i /tmp/collegi_tot.shp \
        -filter-fields CIRCO17_C,CIRCO17_D,COD_REG \
        -dissolve CIRCO17_C copy-fields CIRCO17_D,COD_REG \
        -rename-layers circoscrizioni target=1 \
        -o shp/circoscrizioni_camera_2017.shp

    mapshaper -i /tmp/collegi_tot.shp \
        -filter-fields CAM17P_COD,CAM17P_DEN,CIRCO17_C \
        -dissolve CAM17P_COD copy-fields CAM17P_DEN,CIRCO17_C \
        -rename-layers collegi_pluri target=1 \
        -o shp/collegi_pluri_camera_2017.shp

    mapshaper -i /tmp/collegi_tot.shp \
        -filter-fields CAM17U_COD,CAM17U_DEN,CAM17U_NOM,CAM17P_COD \
        -dissolve CAM17U_COD copy-fields CAM17U_DEN,CAM17U_NOM,CAM17P_COD \
        -rename-layers collegi_uni target=1 \
        -o shp/collegi_uni_camera_2017.shp

    mapshaper -i /tmp/collegi_tot.shp \
        -filter-fields SEN17P_COD,SEN17P_DEN,COD_REG \
        -dissolve SEN17P_COD copy-fields SEN17P_DEN,COD_REG \
        -rename-layers collegi_pluri target=1 \
        -o shp/collegi_pluri_senato_2017.shp

    mapshaper -i /tmp/collegi_tot.shp \
        -filter-fields SEN17U_COD,SEN17U_DEN,SEN17U_NOM,SEN17P_COD \
        -dissolve SEN17U_COD copy-fields SEN17U_DEN,SEN17U_NOM,SEN17P_COD \
        -rename-layers collegi_uni target=1 \
        -o shp/collegi_uni_senato_2017.shp
    """
    help = "Import Areas geographic limits for electoral constituencies in 2018 political elections"
    shp_files_path = None
    branch = None

    def add_arguments(self, parser):
        parser.add_argument(
            dest='shp_files_path',
            help="Absolute path to the compressed shp files directory"
        )
        parser.add_argument(
            dest='branch',
            help="The branch to process: camera|senato"
        )

    def handle(self, *args, **options):

        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.shp_files_path = options['shp_files_path']
        self.branch = options['branch'].lower()

        self.logger.info("Starting import process.")

        try:
            if self.branch == 'camera':
                circoscrizioni = self.fetch_features(
                    f"{self.shp_files_path}/circoscrizioni_{self.branch}_2017.shp"
                )
                for n, feature in enumerate(circoscrizioni, start=1):
                    self.update_or_create_area(
                        feature, 'CIRCO17_C',
                        lambda x: x.get('CIRCO17_D'),
                        'ELECT_CIRC',
                        parent_field='COD_REG'
                    )
                self.logger.info(f"{len(circoscrizioni)} geometries for circoscrizioni updated or created")

            branch3 = self.branch[:3].upper()
            branch1 = self.branch[0].upper()
            if branch1 == 'C':
                parent_field = 'CIRCO17_C'
            else:
                parent_field = 'COD_REG'

            collegi_pluri = self.fetch_features(
                f"{self.shp_files_path}/collegi_pluri_{self.branch}_2017.shp"
            )
            for n, feature in enumerate(collegi_pluri, start=1):
                self.update_or_create_area(
                    feature, f'{branch3}17P_COD',
                    lambda x: x.get(f'{branch3}17P_DEN'),
                    'ELECT_COLL',
                    parent_field=parent_field
                )

            collegi_uni = self.fetch_features(
                f"{self.shp_files_path}/collegi_uni_{self.branch}_2017.shp"
            )
            for n, feature in enumerate(collegi_uni, start=1):
                self.update_or_create_area(
                    feature, f'{branch3}17U_COD',
                    lambda x: x.get(f'{branch3}17U_DEN') + " " + x.get(f'{branch3}17U_NOM'),
                    'ELECT_COLL',
                    parent_field=f"{branch3}17P_COD"
                )
        except Exception as e:
            self.logger.error(e)
            exit(-1)

        self.logger.info("End of import process")

    def update_or_create_area(self, feat, identifier_field, name_field, classification, parent_field=None) -> None:
        """Update or create an Area from a feature,

        identifier and name are extracted from the given feature fields
        the classification is used also as a prefix for the identifier

        :param feat: the geometric feature, extracted from the shape file
        :param identifier_field: field to use building the identifier (with classification as prefix)
        :param name_field: field used for the area name
        :param classification: classification and prefix for the identifier
        :param parent_field: field of the parent's identifier
        :return:
        """
        area, created = Area.objects.update_or_create(
            identifier=f"{classification}_{feat.get(identifier_field)}",
            defaults={
                'name': name_field(feat),
                'classification': classification,
                'start_date': '2018-03-01'
            }
        )
        geom = feat.geom.transform(4326, clone=True)
        geos = geom.geos

        if isinstance(geos, Polygon):
            geos = MultiPolygon(geos)
        area.geometry = geos

        try:
            if parent_field == 'COD_REG':
                area.parent = Area.objects.get(
                    identifier=f"{int(feat.get(parent_field)):02}",
                    classification='ADM1'
                )
            elif 'CIRCO' in parent_field:
                parent_classification = 'ELECT_CIRC'
                area.parent = Area.objects.get(
                    identifier=f"{parent_classification}_{feat.get(parent_field)}",
                    classification=parent_classification
                )
            elif '_COD' in parent_field:
                parent_classification = 'ELECT_COLL'
                area.parent = Area.objects.get(
                    identifier=f"{parent_classification}_{feat.get(parent_field)}",
                    classification=parent_classification
                )

        except Exception as e:
            self.logger.error(
                f"parent area could not be found: {e}"
            )
        area.save()
        if created:
            self.logger.debug(
                f"area and geometry created for area {area.name}, "
                f"with identifier {area.identifier}, parent: {area.parent.identifier}"
            )
        else:
            self.logger.debug(
                f"geometry created for area {area.name}, with identifier {area.identifier}, "
                f"parent: {area.parent.identifier}"
            )

        return area

    def fetch_features(self, shp_file) -> Union(list, None):
        """Return list of geometric features from shp file

        :returns
        """
        if not os.path.exists(shp_file):
            self.logger.error(f"File {shp_file} was not found. Terminating.")
            return None

        # create DataSource instance from shp file
        ds = DataSource(shp_file)
        self.logger.info(f"geographic features layer read from {shp_file}")

        # extract layer
        layer = ds[0]
        if layer is None:
            raise Exception("No layer found in shp file. Terminating.")

        # transform the layer into a dict
        features_list = list(layer)
        return features_list
