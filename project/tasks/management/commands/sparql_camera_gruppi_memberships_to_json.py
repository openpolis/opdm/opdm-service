import re
from copy import deepcopy
from itertools import groupby
from typing import Dict, Any, Optional, List

from popolo.models import Organization

from tasks.management.base import SparqlToJsonCommand
from tasks.parsing.common import identifiers_map, parse_date
from tasks.parsing.sparql.camera import get_person_from_binding
from tasks.parsing.sparql.utils import get_bindings, read_raw_rq
from tasks.parsing.utils import carve_date_intervals


class Command(SparqlToJsonCommand):
    json_filename = "camera_gruppi_memberships"

    def query(self, **options) -> Optional[List[Dict]]:
        q = re.sub(
            r"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_\d+>",
            f"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_{options['legislature']}>",
            read_raw_rq("camera_gruppi_memberships_17.rq"),
        )
        return get_bindings(
            "http://dati.camera.it/sparql",
            q, method="POST"
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:

        role_to_op_role = {
            "DELEGATO D'AULA": None,
            "PORTAVOCE": None,
            "PRESIDENTE": "Presidente gruppo politico in assemblea elettiva",
            "PRESIDENTE - PORTAVOCE": "Presidente gruppo politico in assemblea elettiva",
            "RAPPRESENTANTE COMPONENTE GRUPPO MISTO": None,
            "SEGRETARIO": "Segretario gruppo politico in assemblea elettiva",
            "SEGRETARIO D'AULA": "Segretario d'aula gruppo politico in assemblea elettiva",
            "TESORIERE": "Tesoriere gruppo politico in assemblea elettiva",
            "VICEPRES. VICARIO": "Vicepresidente vicario gruppo politico in assemblea elettiva",
            "VICEPRESIDENTE": "Vicepresidente gruppo politico in assemblea elettiva",
        }

        role_to_op_label = {
            "PRESIDENTE": "Presidente",
            "PRESIDENTE - PORTAVOCE": "Presidente",
            "SEGRETARIO": "Segretario",
            "SEGRETARIO D'AULA": "Segretario d'aula",
            "TESORIERE": "Tesoriere",
            "VICEPRES. VICARIO": "Vicepresidente vicario",
            "VICEPRESIDENTE": "Vicepresidente",
        }

        ocd_uri_to_org_id = identifiers_map(scheme="OCD-URI")
        org_id_to_name = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OCD-URI"
            ).values_list("id", "name")
        }
        org_id_to_dissolution_date = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OCD-URI"
            ).values_list("id", "dissolution_date")
        }

        buffer = {}

        for binding in bindings:
            tmp: Dict[str, Any] = buffer.get(
                binding["person__id"].value,
                {
                    **get_person_from_binding(binding),
                },
            )
            org_id = ocd_uri_to_org_id.get(binding["membership__organization_id"].value)
            if not org_id:
                self.logger.debug(
                    f"\"{binding['membership__organization_id'].value}\" does not exist, skipping.."
                )
                continue

            if "membership__role" not in binding:
                role_str = "Membro gruppo politico in assemblea elettiva"
                label_str = f"Membro {org_id_to_name.get(org_id)}"

            else:
                role_str = role_to_op_role.get(binding["membership__role"].value)
                label_str = f"{role_to_op_label.get(binding['membership__role'].value)} {org_id_to_name.get(org_id)}"
            if not role_str:
                continue
            tmp_start_date = parse_date(binding["membership__start_date"].value)
            if "membership__end_date" in binding:
                tmp_end_date = parse_date(binding["membership__end_date"].value)
            else:
                tmp_end_date = None

            if org_id_to_dissolution_date.get(org_id) and not tmp_end_date:
                tmp_end_date = org_id_to_dissolution_date.get(org_id)

            tmp["memberships"].append(
                {
                    "organization_id": org_id,
                    "role": role_str,
                    "label": label_str,
                    "start_date": tmp_start_date,
                    "end_date": tmp_end_date,
                    "sources": [
                        {
                            "note": "Open Data Camera",
                            "url": "http://dati.camera.it/",
                        }
                    ],
                },
            )

            buffer[binding["person__id"].value] = tmp

        #
        # re-arrange memberships, in order to avoid duplication of
        # simple memberships and memberships with roles (presidency, vice-p, ...)
        #

        # main loop over buffer (data dict by person_id)
        for person_id, values in buffer.items():
            memberships = values.pop("memberships")

            # only valid in cases of multi-memberships
            if len(memberships) > 1:

                # will contain the final transformed membership
                transformed_memberships = []

                # memberships are grouped by organization_id
                groups = groupby(memberships, lambda x: x["organization_id"])
                for org_id, org_memberships_group in groups:

                    # copy memberships of the group, in order to avoid poisoning references
                    org_memberships = deepcopy(list(org_memberships_group))

                    # filter role memberships
                    role_memberships = list(filter(lambda x: 'membro' not in x['label'].lower(), org_memberships))
                    role_memberships.sort(key=lambda x: x['start_date'] or '-999999')
                    role_memberships_intervals = list(map(lambda x: (x['start_date'], x['end_date']), role_memberships))

                    # filter simple memberships
                    comp_memberships = list(filter(lambda x: 'membro' in x['label'].lower(), org_memberships))
                    comp_memberships.sort(key=lambda x: x['start_date'] or '-999999')
                    comp_memberships_intervals = list(map(lambda x: (x['start_date'], x['end_date']), comp_memberships))

                    # carve role memberships intervals out of simple memberships ones
                    comp_transformed_intervals = carve_date_intervals(
                        comp_memberships_intervals, role_memberships_intervals
                    )

                    # re-build transformed simple memberships, using the
                    # first memebrship as model, and copying the dates from the intervals
                    # this is possible, since having grouped by organization_id (commission)
                    # and being simple memberships, all other data are identical
                    transformed_comp_memberships = []
                    for interval in comp_transformed_intervals:
                        _m = deepcopy(comp_memberships[0])
                        _m['start_date'] = interval[0]
                        _m['end_date'] = interval[1]
                        transformed_comp_memberships.append(_m)

                    # re-assemble simple and role memberships for this organization_id
                    # sorting by start_date
                    transformed_org_memberships = role_memberships + transformed_comp_memberships
                    transformed_org_memberships.sort(key=lambda x: x['start_date'] or '-999999')

                    # extend the transformed memberships list with the result
                    transformed_memberships.extend(transformed_org_memberships)
            else:
                transformed_memberships = memberships

            # sort the transformed membership, by start_date
            transformed_memberships.sort(key=lambda x: x['start_date'] or '-999999')

            # re-integrate the memberships into values
            values["memberships"] = transformed_memberships

        output = [values for person_id, values in buffer.items()]

        return output
