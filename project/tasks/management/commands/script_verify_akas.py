import logging

from django.core.management import BaseCommand
from popolo.models import Person

from project.akas.models import AKA


class Command(BaseCommand):
    help = (
        "Verify that solved AKAS point to Person objects near searched ones."
        "Useful whenever criterion for computing AKAs change."
    )

    logger = logging.getLogger(__name__)
    persons_update_strategy = None
    memberships_update_strategy = None

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start")
        self.logger.info("Verify solved AKAs point to correct object")
        akas = AKA.objects.filter(is_resolved=True)
        for aka in akas:
            pointed_persons = Person.objects.filter(id=aka.object_id)
            if pointed_persons.count() == 0:
                self.logger.warning(
                    "AKA {0} points to {1}, which corresponds to no Person on this DB".format(
                        aka.id, aka.object_id
                    )
                )

            for p in pointed_persons.values(
                "birth_date", "given_name", "family_name", "birth_location"
            ):
                if (
                    aka.search_params["family_name"].lower().strip()
                    != p["family_name"].lower().strip()
                    and aka.search_params["given_name"].lower().strip()
                    != p["given_name"].lower().strip()
                    and aka.search_params.get("birth_date", "")
                    != p.get("birth_date", "")
                ):
                    self.logger.info("s: " + str(aka.search_params))
                    self.logger.info("p: " + str(p))
                    self.logger.info("--------------------------")

        self.logger.info("End")
