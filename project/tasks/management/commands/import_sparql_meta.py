from importlib import import_module
import logging
from pathlib import Path
import time
from typing import Type

from django.conf import settings
from django.core.management import BaseCommand, call_command

from project.tasks.management.base import SparqlToJsonCommand


class Command(BaseCommand):
    logger = logging.getLogger(f"project.{__name__}")

    def add_arguments(self, parser):
        parser.add_argument(
            "operation",
            metavar="OP",
            nargs=1,
            type=str,
            choices=[
                "camera",
                "camera_governo",
                "camera_gruppi",
                "camera_commissioni",
                "camera_commissioni_bicamerali",
                "senato",
                "senato_gruppi",
                "senato_commissioni",
            ],
            help="""
                The operation to perform.
                Choices:
                    "camera",
                    "camera_governo",
                    "camera_gruppi",
                    "camera_commissioni",
                    "camera_commissioni_bicamerali",
                    "senato",
                    "senato_gruppi",
                    "senato_commissioni".
                """,
        )
        parser.add_argument(
            "legislature",
            metavar="N",
            action="store",
            nargs=1,
            type=int,
            help="The legislature to parse as integer number.",
        )
        parser.add_argument(
            "--output-dir",
            "-o",
            action="store",
            default=f"{settings.RESOURCES_PATH}/data/parsers",
            dest="output_dir",
            help="The path of the directory where the JSON will be dumped.",
        )
        parser.add_argument(
            "--clear-loader-cache",
            dest="clear_loader_cache",
            action="store_true",
            help="Clear loader cache (diff), load will process all records, not just new ones.",
        )

    def handle(self, *args, **options):
        self.logger.setLevel(
            {
                0: logging.ERROR,
                1: logging.WARNING,
                2: logging.INFO,
                3: logging.DEBUG,
            }.get(options["verbosity"])
        )
        operation = options["operation"][0]
        legislature = options["legislature"][0]
        identifier_scheme = {
            "camera": "OCD-URI",
            "senato": "OSR-URI",
            "camera_gruppi": "OCD-URI",
            "camera_commissioni": "OCD-URI",
            "senato_gruppi": "OSR-URI+",
            "senato_commissioni": "OSR-URI+",
            "camera_commissioni_bicamerali": "OCD-URI",

        }.get(operation)

        output_dir_path = Path(options["output_dir"])

        # Import organizations
        if operation not in ["camera", "senato"]:
            self.logger.info(
                "Fetching organizations from Sparql with "
                f"sparql_{operation}_organizations_to_json {legislature} "
                f"--outpur-dir={options['output_dir']} --verbosity={options['verbosity']}"
            )
            self.call_command(
                f"sparql_{operation}_organizations_to_json",
                legislature,
                verbosity=options["verbosity"],
                output_dir=options["output_dir"]
            )
            organizations_json_name = self._get_json_name(
                f"sparql_{operation}_organizations_to_json", legislature=legislature
            )
            organizations_json_path = output_dir_path / organizations_json_name
            self.logger.info(
                "Loading organizations into OPDM, from json file with "
                f"import_orgs_from_json {organizations_json_path} "
                f"--clear-cache={options['clear_loader_cache']} "
                f"--identifier-scheme={identifier_scheme} --verbosity={options['verbosity']}"
            )
            self.call_command(
                "import_orgs_from_json",
                f"{organizations_json_path}",
                verbosity=options["verbosity"],
                clear_cache=options["clear_loader_cache"],
                identifier_scheme=identifier_scheme,
            )

        # Import people/memberships
        self.logger.info(
            "Fetching memberships from Sparql with "
            f"sparql_{operation}_memberships_to_json {legislature} "
            f"--outpur-dir={options['output_dir']} --verbosity={options['verbosity']}"
        )
        self.call_command(
            f"sparql_{operation}_memberships_to_json",
            legislature,
            verbosity=options["verbosity"],
            output_dir=options["output_dir"],
        )
        persons_json_name = self._get_json_name(
            f"sparql_{operation}_memberships_to_json", legislature=legislature
        )
        persons_json_path = output_dir_path / persons_json_name
        self.logger.info(
            "Loading organizations into OPDM, from json file with "
            f"import_orgs_from_json {persons_json_path} "
            f"--clear-cache={options['clear_loader_cache']} "
            "--skip-near-start-dates --log-step=10 "
            f"--context=parser:{operation} --use-dummy-transformation=True "
            f"--verbosity={options['verbosity']}"
        )
        self.call_command(
            "import_persons_from_json",
            f"{persons_json_path}",
            verbosity=options["verbosity"],
            clear_cache=options["clear_loader_cache"],
            check_membership_label=False,
            skip_near_start_dates=True,
            log_step=10,
            context=f"parsers:{operation}",
            use_dummy_transformation=True,
        )

    @staticmethod
    def _get_json_name(module_name: str, **kwargs) -> str:
        """
        Wrapper to ``SparqlToJsonCommand.get_json_name()`` class method.
        :return:
        """

        command_mod = import_module(f".{module_name}", __package__)
        command_class: Type[SparqlToJsonCommand] = command_mod.Command
        return command_class.get_json_name(**kwargs)

    def call_command(self, command_name: str, *args, **options):
        """
        Wrapper to Django ``call_command`` function.

        :param command_name: the name of the command.
        :param args: positional args to be passed to the command.
        :param options: keyword args to be passed to the command.
        """
        self.logger.info(f"----- Starting {command_name} -----")
        start_time = time.time()
        call_command(command_name, *args, **options, stdout=self.stdout)
        elapsed = time.time() - start_time
        self.logger.info(f"----- Completed in {elapsed:.2f}s -----")
