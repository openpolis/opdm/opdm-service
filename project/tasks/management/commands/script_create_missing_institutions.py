import logging

from django.core.management import BaseCommand
from popolo.models import Organization, Classification

from project.core import labels_utils


class Command(BaseCommand):
    """
    This script loops over all Areas, and generates the missing institutions.

    An Area must be linked to an Organization of the right FORMA_GIURIDICA_OP (Es: Lazio => Regione Lazio)
    An institution has 2 organs:
    - the assembly (named council)
    - the government (named giunta or conferenza)
    """

    help = "Create missing institution for local administrations"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start")

        loc_types = [
            {
                "area": "REG",
                "org": "Regione",
                "gov": "Giunta regionale",
                "gov_code": "3110",
                "ass": "Consiglio regionale",
                "ass_code": "3210",
            },
            {
                "area": "CM",
                "org": "Città metropolitana",
                "gov": "Conferenza metropolitana",
                "gov_code": "3111",
                "ass": "Consiglio metropolitano",
                "ass_code": "3211",
            },
            {
                "area": "PROV",
                "org": "Provincia",
                "gov": "Giunta provinciale",
                "gov_code": "3112",
                "ass": "Consiglio provinciale",
                "ass_code": "3212",
            },
            {
                "area": "COM",
                "org": "Comune",
                "gov": "Giunta comunale",
                "gov_code": "3113",
                "ass": "Consiglio comunale",
                "ass_code": "3213",
            },
        ]

        for loc_type in loc_types:
            self.logger.info("")
            self.logger.info(loc_type["area"])
            for org in Organization.objects.filter(classification=loc_type["org"]):
                self.logger.debug(" - {0}".format(org.name))

                # create assemblea
                try:
                    name = "{0} {1} {2}".format(
                        loc_type["ass"],
                        labels_utils.area_prep(org.area.name, loc_type["area"][0]),
                        org.area.name,
                    )
                    a, created = org.children.update_or_create(
                        classification=loc_type["ass"],
                        defaults={
                            "name": name,
                            "founding_date": org.founding_date,
                            "dissolution_date": org.dissolution_date,
                            "end_reason": org.end_reason,
                        },
                    )
                    if created:
                        self.logger.info("Assemblea created: {0}".format(a))
                        c, c_created = Classification.objects.get_or_create(
                            scheme="FORMA_GIURIDICA_OP",
                            descr=loc_type["ass"],
                            code=loc_type["ass_code"],
                        )
                        a.classifications.get_or_create(classification=c)
                    else:
                        self.logger.info("Assemblea updated: {0}".format(a))
                    a.add_classification("CONTESTO_OP", descr="OPDM")

                except Exception as e:
                    self.logger.error("Error {0} while processing {1}".format(e, org))

                # create governo
                try:
                    name = "{0} {1} {2}".format(
                        loc_type["gov"],
                        labels_utils.area_prep(org.area.name, loc_type["area"][0]),
                        org.area.name,
                    )
                    g, created = org.children.update_or_create(
                        classification=loc_type["gov"],
                        defaults={
                            "name": name,
                            "founding_date": org.founding_date,
                            "dissolution_date": org.dissolution_date,
                            "end_reason": org.end_reason,
                        },
                    )
                    if created:
                        self.logger.info("Governo locale created: {0}".format(g))
                        c, c_created = Classification.objects.get_or_create(
                            scheme="FORMA_GIURIDICA_OP",
                            descr=loc_type["gov"],
                            code=loc_type["gov_code"],
                        )
                        g.classifications.get_or_create(classification=c)
                    else:
                        self.logger.info("Consiglio updated: {0}".format(g))
                    g.add_classification("CONTESTO_OP", descr="OPDM")

                except Exception as e:
                    self.logger.error("Error {0} while processing {1}".format(e, org))

        self.logger.info("End")
