# -*- coding: utf-8 -*-
import itertools
import json
import operator

from django.core.serializers import serialize
from popolo.models import Area, Language
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = 'Extract geojson from Areas in opdm'
    filename = None

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--file-name',
            dest='file_name',
            default='comuni.geojson',
            help='Name of the geojson file'
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.filename = options['file_name']

        # get languages, so that languages[1]['iso639_1_code'] == 'it'
        languages = dict(
            (
                (g[0], next(i for i in g[1]))
                for g in itertools.groupby(Language.objects.values(), operator.itemgetter('id'))
            )
        )

        self.logger.info("Serializing comuni ...")
        comuni_areas = Area.objects.comuni().filter(
            geometry__isnull=False, end_date__isnull=True, identifiers__end_date__isnull=True
        ).select_related("parent").prefetch_related("identifiers", "i18n_names").distinct()
        comuni_json = serialize(
            'geojson', comuni_areas,
            geometry_field='geometry',
            fields=(
                'pk',
                'name', 'identifier',
            )
        )

        # open up serialised json in a python data structure
        comuni = json.loads(comuni_json)

        # fetch identifiers and i18n names
        comuni_identifiers = comuni_areas.values(
            'pk',
            'identifiers__scheme', 'identifiers__identifier',
            'i18n_names__language', 'i18n_names__name',
        )

        # assign identifiers and i18n names to comuni properties
        for n, comune_identifiers in enumerate(comuni_identifiers, start=1):
            pk = str(comune_identifiers.pop('pk'))
            comune = next(f for f in comuni['features'] if f['properties']['pk'] == pk)

            identifier_scheme = comune_identifiers['identifiers__scheme']
            identifier_value = comune_identifiers['identifiers__identifier']
            comune['properties'][identifier_scheme.lower()] = identifier_value

            i18n_language = comune_identifiers['i18n_names__language']
            if i18n_language:
                i18n_name = comune_identifiers['i18n_names__name']
                comune['properties'][f"name_{languages[i18n_language]['iso639_1_code']}"] = i18n_name

            if n % 1000 == 0:
                self.logger.info(f"{n}/{len(comuni_identifiers)}")
        self.logger.info(
            f"{len(comuni_identifiers)} identifiers and names assigned to {len(comuni['features'])} comuni"
        )

        comuni_parents = comuni_areas.filter(
            parent__identifiers__scheme='ISTAT_CODE_PROV',
            parent__parent__identifiers__scheme='ISTAT_CODE_REG'
        ).values(
            'pk',
            'parent__name', 'parent__identifier', 'parent__identifiers__identifier',
            'parent__parent__name', 'parent__parent__identifier',
        )
        for n, comune_parents in enumerate(comuni_parents, start=1):
            pk = str(comune_parents.pop('pk'))
            comune = next(f for f in comuni['features'] if f['properties']['pk'] == pk)
            comune['properties']['prov_name'] = comune_parents['parent__name']
            comune['properties']['prov_istat_code'] = comune_parents['parent__identifiers__identifier']
            comune['properties']['prov_istat_code_num'] = int(comune_parents['parent__identifiers__identifier'])
            comune['properties']['prov_acr'] = comune_parents['parent__identifier']
            comune['properties']['reg_name'] = comune_parents['parent__parent__name']
            comune['properties']['reg_istat_code'] = comune_parents['parent__parent__identifier']
            comune['properties']['reg_istat_code_num'] = int(comune_parents['parent__parent__identifier'])

        def transform_fields(f):
            # rename
            field_renames = (
                ('pk', 'opdm_id'),
                ('identifier', 'com_catasto_code'),
                ('istat_code_com', 'com_istat_code')
            )
            for old_name, new_name in field_renames:
                if old_name in f['properties']:
                    f['properties'][new_name] = f['properties'].pop(old_name)

            # copy fields
            f['properties']['com_istat_code_num'] = int(f['properties']['com_istat_code'])

            return f
        comuni['features'] = list(map(transform_fields, comuni['features']))

        # serialise again into json
        comuni_json = json.dumps(comuni)

        # write to file
        self.logger.info(f"Writing {self.filename} ...")
        with open(self.filename, 'w') as fp:
            fp.write(comuni_json)

        self.logger.info("Finished.")
