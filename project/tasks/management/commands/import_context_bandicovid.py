# -*- coding: utf-8 -*-
from graphqlclient import GraphQLClient
import json
import pandas as pd
import requests
from django.core import management
from django.conf import settings
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """This management tasks executes the logic and subtasks, needed to extract information
    from the https://graphql.bandicovid19.openpolis.io/v1/graphql graphql endpoint,
    lookup corresponding data in the ATOKA API, and add them back into OPDM

    python manage.py import_context_bandicovid --batch-size 100 --data-path ./data/atoka
    """

    help = "Extracts data from bandicovid.io, lookup related info in ATOKA, then load into OPDM"
    verbosity = None
    batch_size = None
    data_path = None
    use_atoka_cache = None
    clear_diff_cache = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batch_size", type=int,
            default=50,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--data-path",
            dest="data_path",
            default="./data/atoka",
            help="Complete path to json files"
        )
        parser.add_argument(
            "--use-atoka-cache",
            dest="use_atoka_cache",
            action='store_true',
            help="Use extract_organizations[SUFFIX].json, without fetching it anew from ATOKA."
        )
        parser.add_argument(
            "--clear-diff-cache",
            dest="clear_diff_cache",
            action='store_true',
            help="Clear diff cache, load will process all records, not just new ones."
        )
        parser.add_argument(
            "--skip-graphql-import",
            dest="skip_graphql_import",
            action='store_true',
            help="Skip graphql import, uses ids file stored in data path."
        )

    def section_log(self, title):
        self.logger.info(" ")
        self.logger.info(title)
        self.logger.info("*" * len(title))

    @staticmethod
    def extract_operatori():
        """extract operatori from https://graphql.bandicovid19.openpolis.io/v1/graphql

        :return:
        """

        # jwt authentication
        r = requests.post(
            "https://service.opdm.openpolis.io/api-token-auth/",
            data={'username': settings.BANDICOVID_USERNAME, 'password': settings.BANDICOVID_PASSWORD}
        )
        token = r.json()['token']

        query = """
            query operatori {
              core_operatoreeconomico(where: {stato: {_eq: "ITA"}}) {
                id
                ragione_sociale
                codice_fiscale
              }
            }
        """
        client = GraphQLClient('https://graphql.bandicovid19.openpolis.io/v1/graphql')
        client.inject_token(f'Bearer {token}', 'Authorization')
        result = json.loads(client.execute(query))
        operatori = result['data']['core_operatoreeconomico']

        return operatori

    @staticmethod
    def get_operatori_ids(operatori):
        return [o['codice_fiscale'] for o in operatori]

    @staticmethod
    def transform_operatori(operatori):
        """Transform a list of operatori into a json list, compatible with
        the import_orgs_from_json management task

        :param operatori:
        :return:
        """
        json_list = []
        for o in operatori:
            json_list.append({
                "name": o['ragione_sociale'],
                "identifiers": [
                  {
                    "scheme": "CF",
                    "identifier": o['codice_fiscale']
                  }
                ],
                "sources": [
                  {
                    "url": "https://bandicovid.openpolis.io",
                    "note": "Bandi Covid"
                  }
                ],
              }
            )
        return json_list

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.batch_size = options['batch_size']
        self.data_path = options['data_path']
        self.use_atoka_cache = options['use_atoka_cache']
        self.clear_diff_cache = options['clear_diff_cache']

        skip_graphql_import = options['skip_graphql_import']

        self.logger.info("Starting procedure")

        self.verbosity = options.get("verbosity", 1)

        operatori = self.extract_operatori()

        ids_file = f"{self.data_path}/bandicovid_ids.csv"
        self.logger.info(f"Emitting ids into {ids_file}")
        df = pd.DataFrame(self.get_operatori_ids(operatori), columns=['id'])
        df.to_csv(ids_file, index=False)

        if not skip_graphql_import:
            operatori = self.extract_operatori()
            operatori = self.transform_operatori(operatori)
            with open(f"{self.data_path}/operatori.json", 'w') as jf:
                json.dump(operatori, jf)

            self.section_log(
                f"import_orgs_from_json {self.data_path}/operatori.json --emit-ids={self.data_path}/bandicovid_ids.csv"
            )
            management.call_command(
                'import_orgs_from_json',
                f"{self.data_path}/operatori.json",
                clear_cache=True,
                opdm_context="Bandi Covid Winner",
                verbosity=self.verbosity,
                stdout=self.stdout
            )

        self.section_log(
            f"import_atoka_organizations --ids-source={self.data_path}/bandicovid_ids.csv --clear-diff-cache"
        )
        management.call_command(
            'import_atoka_organizations',
            ids_source=f"{self.data_path}/bandicovid_ids.csv",
            clear_diff_cache=self.clear_diff_cache,
            use_atoka_cache=self.use_atoka_cache,
            data_path=self.data_path,
            batch_size=self.batch_size,
            verbosity=self.verbosity,
            opdm_context="Bandi Covid",
            stdout=self.stdout
        )

        self.section_log("script_process_akas --aka_lo=90 --context-filter=atoka")
        management.call_command(
            'script_process_akas',
            aka_lo=90,
            context_filter="atoka",
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("import_atoka_organizations_economics --clear-diff-cache")
        management.call_command(
            'import_atoka_organizations_economics',
            ids_source=f"{self.data_path}/bandicovid_ids.csv",
            clear_diff_cache=True,
            data_path=self.data_path,
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("script_compute_missing_cfs")
        management.call_command(
            'script_compute_missing_cfs',
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        self.section_log("script_consolidate_persons")
        management.call_command(
            'script_consolidate_persons',
            verbosity=self.verbosity,
            stdout=self.stdout
        )

        # merge duplicate organization, merging data
        self.section_log("script_merge_duplicate_organizations")
        management.call_command(
            'script_merge_duplicate_organizations',
            verbosity=self.verbosity,
            stdout=self.stdout
        )
