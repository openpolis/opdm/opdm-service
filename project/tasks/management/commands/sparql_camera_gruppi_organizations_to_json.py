import re
from typing import Dict, Any, Optional, List

from popolo.models import Classification, Organization
import roman

from project.tasks.parsing.common import (
    SPARQLBinding,
    parse_date,
)
from tasks.management.base import SparqlToJsonCommand
from tasks.parsing.sparql.utils import get_bindings, read_raw_rq
from tasks.template_strings import ORG_GRUPPO_POLITICO_CAMERA_NAME


class Command(SparqlToJsonCommand):
    json_filename = "camera_gruppi_organizations"

    def query(self, **options) -> Optional[List[Dict]]:
        q = re.sub(
            r"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_\d+>",
            f"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_{options['legislature']}>",
            read_raw_rq("camera_gruppi_organizations_17.rq"),
        )
        return get_bindings(
            "http://dati.camera.it/sparql",
            q,
            legacy=True,
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        org_end_dates_map = dict(
            Organization.objects.filter(identifiers__scheme="OCD-URI").values_list(
                "identifiers__identifier", "dissolution_date"
            )
        )
        n_leg = options["legislature"]

        try:
            parent = Organization.objects.filter(identifier=f"ASS-CAMERA-{n_leg}").get()
            classification_3314_id = Classification.objects.get(
                scheme="FORMA_GIURIDICA_OP", code="3314"
            ).id
        except Organization.DoesNotExist:
            self.logger.error(f"Organization ASS-CAMERA-{n_leg} does not exist.")
            return
        except Classification.DoesNotExist:
            self.logger.error("Classification FORMA_GIURIDICA_OP=3314 does not exist.")
            return
        if not parent.founding_date:
            self.logger.warning(
                f"Parent organization ASS-CAMERA-{n_leg} does not have a founding_date."
            )
            return

        organizations = {}

        for binding in bindings:
            organization = organizations.get(
                binding["identifier"]["value"],
                {
                    **self._parse_gruppo(binding),
                    "parent": parent.id,
                    "classifications": [{"classification": classification_3314_id}],
                },
            )

            if not organization["dissolution_date"]:
                organization["dissolution_date"] = org_end_dates_map.get(
                    binding["assemblea"]["value"]
                )

            # Parse former names; infer start dates
            if "pastName" in binding:
                for_other_names = [
                    o
                    for o in organization["other_names"]
                    if o["othername_type"] == "FOR"
                ]

                if for_other_names:
                    for_past_name_start_date = for_other_names[-1]["end_date"]
                else:
                    for_past_name_start_date = organization["founding_date"]

                for_past_name_end_date = parse_date(binding["endDatePastName"]["value"])

                organization["other_names"].append(
                    {
                        "othername_type": "FOR",
                        "name": binding["pastName"]["value"],
                        "start_date": for_past_name_start_date,
                        "end_date": for_past_name_end_date,
                    },
                )
                organization["other_names"].append(
                    {
                        "othername_type": "ACR",
                        "name": binding["pastAcr"]["value"],
                        "start_date": for_past_name_start_date,
                        "end_date": for_past_name_end_date,
                    }
                )

            # assign start and end date to acronym
            if len(organization["other_names"]) > 1:
                organization["other_names"][0]["start_date"] = organization["other_names"][-1]["end_date"]
            else:
                organization["other_names"][0]["start_date"] = organization["founding_date"]

            organization["other_names"][0]["end_date"] = organization["dissolution_date"]

            organizations[binding["identifier"]["value"]] = {
                k: v for k, v in sorted(organization.items())
            }

        output = [values for persona, values in organizations.items()]
        return output

    @staticmethod
    def _parse_gruppo(binding: SPARQLBinding) -> Dict[str, Any]:
        name = binding["name"]["value"].split("(")[0].strip()
        n = int(binding["rif_leg"]["value"].split("_")[1])
        ret = {
            "name": ORG_GRUPPO_POLITICO_CAMERA_NAME.format(
                name=name, roman=roman.toRoman(n)
            ),
            "classification": "Gruppo politico in assemblea elettiva",
            "founding_date": None,
            "dissolution_date": None,
            "identifier": binding["identifier"]["value"],
            "identifiers": [
                {"scheme": "OCD-URI", "identifier": binding["identifier"]["value"]}
            ],
            "other_names": [],
        }

        if "othername__ACR" in binding:
            ret["other_names"].append(
                {"othername_type": "ACR", "name": binding["othername__ACR"]["value"]}
            )
        if "founding_date" in binding:
            ret["founding_date"] = parse_date(binding["founding_date"]["value"])
        if "dissolution_date" in binding:
            ret["dissolution_date"] = parse_date(binding["dissolution_date"]["value"])

        return ret
