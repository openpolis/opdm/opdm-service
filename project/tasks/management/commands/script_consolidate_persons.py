import re

from django.contrib.contenttypes.models import ContentType
from django.db import transaction
from django.db.models import Count
from django.db.models.functions import Lower
from popolo.models import Person, Post, Organization
from taskmanager.management.base import LoggingBaseCommand

from project.akas.models import AKA


class AKAException(Exception):
    pass


class Command(LoggingBaseCommand):
    help = "Find duplicate persons, merge the information and remove the duplicates."
    dry_run = None
    person_ct = ContentType.objects.get(app_label='popolo', model='person')

    def add_arguments(self, parser):
        parser.add_argument(
            "--lookup-strategy",
            dest="lookup_strategy",
            default="identifier",
            help="Strategy to find duplicates: anagraphical, identifier, manual."
            "Defaults to identifier.",
        )
        parser.add_argument(
            "--lookup-identifier-scheme",
            dest="lookup_identifier_scheme",
            default="CF",
            help="Which identifier scheme to use if lookup_strategy is set to identifier."
            "Defaults to CF.",
        )
        parser.add_argument(
            "--lookup-manual-ids",
            dest="lookup_manual_ids",
            default="",
            help="List of duplicates ids, each of the form: id_src:id_dest_1:id_dest_2 ..."
        )
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action="store_true",
            help="Do not save in the DB"
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Start of procedure")

        lookup_strategy = options["lookup_strategy"]
        lookup_identifier_scheme = options["lookup_identifier_scheme"]
        lookup_manual_ids = options.get("lookup_manual_ids").split(" ")

        self.dry_run = options["dry_run"]

        duplicates = self.find_duplicates(
            lookup_strategy,
            lookup_identifier_scheme=lookup_identifier_scheme,
            lookup_manual_ids=lookup_manual_ids,
            logger=self.logger
        )
        n_duplicates = len(duplicates)
        self.logger.info(f"{n_duplicates} duplicates found")
        for n, d in enumerate(duplicates, start=1):
            self.logger.debug(f"processing {d}")
            p_dest = Person.objects.get(id=d[0])
            p_src_list = Person.objects.filter(id__in=d[1:])

            try:
                with transaction.atomic():
                    for p_src in p_src_list:
                        if not self.dry_run:
                            # merge persons details
                            self.merge_persons(source_person=p_src, dest_person=p_dest)

                            # generate AKA to shortcut p_src to p_dest, and avoid creating another duplication
                            self.generate_aka(source_person=p_src, dest_person=p_dest)

                            # redirect solved AKAs from p_src to p_dest
                            self.redirect_akas(source_person=p_src, dest_person=p_dest)

                    if not self.dry_run:
                        # remove duplicates after info have been copied
                        p_src_list.delete()
            except AKAException as e:
                self.logger.warning(
                    f"{e} encountered during processing of {p_src} => {p_dest} - skipping"
                )

            if n % 25 == 0:
                self.logger.info(f"{n}/{n_duplicates}")

        self.logger.info("End of procedure")

    @staticmethod
    def find_duplicates(lookup_strategy: str, logger: None, **kwargs) -> list:
        """Use lookup_strategy to look for duplicates in Persons
        :param lookup_strategy: the strategy used to look for duplicates (anagraphical, identifier, manual)
        :param logger: the logger
        :param kwargs: other parameters used in strategies: lookup_identifier_scheme, lookup_manual_ids

        :return: list of tuples
        """
        lookup_identifier_scheme = kwargs.get('lookup_identifier_scheme', None)
        lookup_manual_ids = kwargs.get('lookup_manual_ids', [])
        duplicates = []
        if logger:
            logger.info(f"Looking for duplicates with {lookup_strategy} strategy")
        if lookup_strategy == 'anagraphical':
            ann_persons = Person.objects.annotate(
                l_given_name=Lower("given_name"),
                l_family_name=Lower("family_name"),
                l_birth_location=Lower("birth_location"),
                l_birth_date=Lower("birth_date")
            )
            dupl_values = list(
                ann_persons.
                values("l_given_name", "l_family_name", "l_birth_location", "l_birth_date").
                distinct().annotate(
                    n=Count(("l_given_name", "l_family_name", "l_birth_date", "l_birth_location"))
                ).filter(n__gt=1).order_by("-n")
            )
            for d in dupl_values:
                del d['n']
                duplicates.append(tuple(ann_persons.filter(**d).values_list('id', flat=True)))
        elif lookup_strategy == 'identifier':
            if lookup_identifier_scheme not in ["CF", ]:
                raise Exception("Only CF scheme has been implemented.")
            ann_persons = Person.objects\
                .filter(identifiers__scheme=lookup_identifier_scheme)\
                .values('identifiers__identifier')\
                .annotate(n=Count('identifiers__identifier'))
            dupl_identifiers = list(
                ann_persons.filter(n__gt=1).order_by("-n")
            )
            for d in dupl_identifiers:
                del d['n']
                duplicates.append(
                    tuple(
                        ann_persons.filter(identifiers__scheme=lookup_identifier_scheme).
                        filter(**d).values_list('id', flat=True)
                    )
                )
        elif lookup_strategy == 'manual':
            if not lookup_manual_ids:
                raise Exception("lookup_manual_ids must contain some values")

            for d in lookup_manual_ids:
                duplicates.append(tuple(map(int, d.split(":"))))
        else:
            raise Exception("lookup_strategy can be: anagraphical, identifier, manual")

        return duplicates

    def merge_persons(self, source_person, dest_person):
        """Merge information from a person into another

        :param source_person: person to copy information from
        :param dest_person:   person to receive information
        :return:
        """
        for m in source_person.memberships.values():
            post = Post.objects.get(pk=m.pop("post_id"))
            m.pop("id")
            m.pop("organization_id")
            m.pop("person_id")
            m.pop("slug")
            try:
                r = dest_person.add_role(post, **m)
                if r:
                    self.logger.info("{0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))

        for c in source_person.contact_details.values():
            c.pop("id")
            c.pop("person", None)
            try:
                r = dest_person.add_contact_detail(**c)
                if r:
                    self.logger.info("{0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))

        if source_person.name != dest_person.name:
            dest_person.add_other_name(source_person.name)

        for o in source_person.other_names.values():
            o.pop("id")
            o.pop("person", None)
            try:
                r = dest_person.add_other_name(**o)
                if r:
                    self.logger.info("{0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))

        for i in source_person.links.values("link__url", "link__note"):
            try:
                r = dest_person.add_link(
                    url=i.get("link__url"), note=i.get("link__note", "")
                )
                if r:
                    self.logger.info("{0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))

        for i in source_person.sources.values("source__url", "source__note"):
            try:
                r = dest_person.add_source(
                    url=i.get("source__url"), note=i.get("source__note", "")
                )
                if r:
                    self.logger.info("{0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))

        for o in source_person.ownerships.values():
            o.pop('id')
            o.pop('owner_person_id')
            o.pop('slug')
            org_id = o.pop('owned_organization_id')
            org = Organization.objects.get(id=org_id)
            try:
                r = dest_person.add_ownership(org, **o)
                if r:
                    self.logger.info("{0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))

    def generate_aka(self, source_person, dest_person):
        """Generate AKA record to shortcut from source_person into dest_person

        :param source_person: person to copy information from
        :param dest_person:   person to receive information
        :return:
        """
        source_url = "script_consolidate_persons"
        search_params = {}
        search_fields = ["given_name", "family_name", "birth_date", "birth_location"]
        for f in search_fields:
            v = getattr(source_person, f)
            if v is not None:
                if f == "birth_location":
                    search_params[f] = re.sub(r"(.*) \(.*\)", r"\1", v.lower())
                else:
                    search_params[f] = v.lower()
            else:
                search_params[f] = ""

        try:
            aka, created = AKA.objects.get_or_create(
                search_params=search_params,
                n_similarities=0,
                loader_context={},
                data_source_url=source_url,
                is_resolved=True,
                content_type=self.person_ct,
                object_id=dest_person.id,
            )
        except AKA.MultipleObjectsReturned:
            raise AKAException(f"Multiple AKAs encountered for {search_params} ")

        if created:
            if self.logger:
                self.logger.info(
                    f"AKA record {aka.id} created for search {search_params} pointing to P-{dest_person.id}"
                )

    def redirect_akas(self, source_person, dest_person):
        """Reditrect solved AKA shortcuts from source_person into dest_person

        :param source_person: person to copy information from
        :param dest_person:   person to receive information
        :return:
        """
        akas = AKA.objects.filter(
            is_resolved=True,
            content_type=self.person_ct,
            object_id=source_person.id
        )

        if self.logger:
            self.logger.info(
                f"{akas.count()} AKA shortcut updated from P-{source_person.id} to P-{dest_person.id}"
            )
        akas.update(object_id=dest_person.id)
