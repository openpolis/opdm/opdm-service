# coding=utf-8
from imghdr import what
from io import BytesIO
import logging
import mimetypes

import boto3
from popolo.models import Person, Organization
import requests
from taskmanager.management.base import LoggingBaseCommand

from project.elections.models import ElectoralListResult


class Command(LoggingBaseCommand):
    help = (
        "Transfers existing Person images to an S3 bucket. "
        "The script requires AWS credentials to be set in the environment variables AWS_ACCESS_KEY_ID and "
        "AWS_SECRET_ACCESS_KEY. The images will be stored under the key `headshots/*.jpeg`."
    )
    requires_migrations_checks = True
    requires_system_checks = True

    client = boto3.client("s3")
    bucket = "opdm-service-data"
    folders = {
        'persons': "images/persons",
        'orgs': "images/organizations",
        'list_elections': "images/list_elections"
    }

    def copy_images(self, model, folder):
        instances = model.objects.filter(
            image__isnull=False
        ).exclude(
            image__icontains="opdm-service-data"
        ).values("id", "image")
        self.logger.info(f"Found {len(instances)} persons to copy.")
        for i, instance in enumerate(instances, start=1):
            self.logger.info(f"Downloading the image from {instance['image']}...")
            try:
                response = requests.get(instance["image"], timeout=5)
                if response.status_code != 200:
                    self.logger.warning(f"An error occurred while downloading {instance['image']}. Moving on... ")
                    continue
            except Exception as e:  # Image does not exist
                self.logger.warning(f"{e} while downloading {instance['image']}. Moving on.")
                continue
            raw = BytesIO(response.content)
            key = f"{folder}/{instance['id']}.{what(raw)}"
            content_type = mimetypes.guess_type(key)[0]
            extra_args = {"ACL": "public-read"}
            if content_type is not None:
                extra_args["ContentType"] = content_type
            self.client.upload_fileobj(raw, self.bucket, key, ExtraArgs=extra_args)
            try:
                s3_response = self.client.head_object(Bucket=self.bucket, Key=key)
                if s3_response:
                    url = f"https://{self.bucket}.s3.amazonaws.com/{key}"
                    if hasattr(model, 'dati_specifici'):
                        model_instance = model.objects.get(id=instance["id"])
                        new_dict = (model_instance.dati_specifici or {})
                        new_dict.update({"old_image": instance["image"]})
                        model_instance.dati_specifici= new_dict
                        model_instance.save()
                    model.objects.filter(id=instance["id"]).update(image=url)

            except Exception as e:  # Image does not exist
                self.logger.warning(f"{e} while uploading {instance['image']} to {key}. Moving on.")

            if i % 100 == 0:
                self.logger.info(f"{i}/{len(instances)}")

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        try:

            self.logger.info(f"Copying persons")
            self.copy_images(Person, self.folders['persons'])

            self.logger.info(f"Copying persons")
            self.copy_images(Organization, self.folders['orgs'])

            self.logger.info(f"Copying list elections")
            self.copy_images(ElectoralListResult, self.folders['list_elections'])

        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes
