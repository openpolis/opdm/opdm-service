from django.contrib.contenttypes.models import ContentType
from taskmanager.management.base import LoggingBaseCommand

from project.akas.models import AKA
from project.core import person_utils
from project.core.exceptions import AKAException


class Command(LoggingBaseCommand):
    help = (
        "Populate all other_names of type AKA for Persons, looping over all AKAs."
    )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)

        self.logger.info("Start")

        # fix content type
        # for future uses, app_label and model may be passed aas an argument
        content_type_id = ContentType.objects.get(app_label='popolo', model='person').id

        akas_qs = AKA.objects.filter(content_type_id=content_type_id, is_resolved=True)

        n_akas = akas_qs.count()
        for n, aka in enumerate(akas_qs):
            if n % 1000 == 0:
                print("{0}/{1}".format(n, n_akas))

            try:
                aka_name = person_utils.populate_other_names_from_aka(aka)
                if aka_name:
                    self.logger.info("{0} AKA added to other_names for {1}".format(
                        aka_name, aka.content_object.name
                    ))
            except AKAException as e:
                self.logger.error(e)
                continue

        self.logger.info("End")
