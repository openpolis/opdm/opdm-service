import re
from typing import Dict, Any, Optional, List

from django.contrib.contenttypes.models import ContentType
from popolo.models import Organization

from tasks.management.base import SparqlToJsonCommand
from tasks.parsing.common import identifiers_map, parse_date
from tasks.parsing.sparql.utils import get_bindings, read_raw_rq, get_person_from_binding


class Command(SparqlToJsonCommand):
    json_filename = "senato_gruppi_memberships"

    def query(self, **options) -> Optional[List[Dict]]:
        q = re.sub(
            r"osr:legislatura \d+",
            f"osr:legislatura {options['legislature']:02d}",
            read_raw_rq("senato_gruppi_memberships_17.rq"),
        )
        return get_bindings(
            "https://dati.senato.it/sparql",
            q
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        n_leg = options["legislature"]
        osr_uri_to_org_id = identifiers_map(
            scheme="OSR-URI+",
            content_type_id=ContentType.objects.get_for_model(Organization)
        )
        org_id_to_name = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OSR-URI+"
            ).values_list("id", "name")
        }
        org_id_to_dissolution_date = {
            id_: name
            for id_, name in Organization.objects.filter(
                identifiers__scheme="OSR-URI+"
            ).values_list("id", "dissolution_date")
        }

        buffer = {}
        for binding in bindings:
            tmp: Dict[str, Any] = buffer.get(
                binding["person__id"].value,
                {
                    **get_person_from_binding(binding, "OSR-URI"),
                },
            )
            org_id = osr_uri_to_org_id.get(f"{binding['membership__organization_id'].value}-{n_leg}")
            if not org_id:
                self.logger.debug(
                    f"Org with identifiers OSR-URI+ \"{binding['membership__organization_id'].value}-{n_leg}\" "
                    "does not exist, skipping.."
                )
                continue

            role_str = f"{binding['membership__role'].value} gruppo politico in assemblea elettiva"
            label_str = f"{binding['membership__role'].value} {org_id_to_name.get(org_id)}"

            tmp_start_date = parse_date(binding["membership__start_date"].value)
            if "membership__end_date" in binding:
                tmp_end_date = parse_date(binding["membership__end_date"].value)
            else:
                tmp_end_date = None

            if org_id_to_dissolution_date.get(org_id) and not tmp_end_date:
                tmp_end_date = org_id_to_dissolution_date.get(org_id)

            tmp["memberships"].append(
                {
                    "organization_id": org_id,
                    "role": role_str,
                    "label": label_str,
                    "start_date": tmp_start_date,
                    "end_date": tmp_end_date,
                    "sources": [
                        {
                            "note": "Open Data Senato",
                            "url": "https://dati.senato.it/",
                        }
                    ],
                },
            )

            buffer[binding["person__id"].value] = tmp

        # Sort out output...
        output = [
            {
                **values,
                "memberships": values["memberships"],
            }
            for persona, values in buffer.items()
        ]

        return output
