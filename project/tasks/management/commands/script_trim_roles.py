import logging

from django.core.management import BaseCommand
from popolo.models import Post, Membership


class Command(BaseCommand):
    help = "Add ISTAT_CODE_REG identifier to regions"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start")
        Post.objects.filter(role="Sindaco ").update(role="Sindaco")
        Membership.objects.filter(role="Sindaco ").update(role="Sindaco")
        self.logger.info("Sindaco trimmed")
        Post.objects.filter(role="Vicesindaco ").update(role="Vicesindaco")
        Membership.objects.filter(role="Vicesindaco ").update(role="Vicesindaco")
        self.logger.info("Vicesindaco trimmed")
        Post.objects.filter(role="Commissario straordinario ").update(
            role="Commissario straordinario"
        )
        Membership.objects.filter(role="Commissario straordinario ").update(
            role="Commissario straordinario"
        )
        self.logger.info("Commissario straordinario trimmed")
        Post.objects.filter(role="Vicesindaco facente funzione sindaco ").update(
            role="Vicesindaco facente funzione sindaco"
        )
        Membership.objects.filter(role="Vicesindaco facente funzione sindaco ").update(
            role="Vicesindaco facente funzione sindaco"
        )
        self.logger.info("Vicesindaco facente funzione sindaco")
        self.logger.info("End")
