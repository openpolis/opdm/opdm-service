from django.conf import settings


class CacheArgumentsCommandMixin(object):
    """Mixin that adds cache defining arguments to a Command class."""

    clear_cache = False
    local_cache_path = None
    local_out_path = None

    def add_arguments_cache(self, parser):
        parser.add_argument(
            "--clear-cache",
            dest="clear_cache",
            action="store_true",
            help="Clear cache if found",
        )
        parser.add_argument(
            "--local-cache-path",
            dest="local_cache_path",
            default=settings.IMPORT_CACHE_PATH,
            help="Where downloaded file should be stored for caching and diffing",
        )
        parser.add_argument(
            "--local-out-path",
            dest="local_out_path",
            default=settings.IMPORT_OUT_PATH,
            help="Where produced file should be stored for successive manual processing",
        )

    def handle_cache(self, *args, **options):
        self.clear_cache = options["clear_cache"]
        self.local_cache_path = options["local_cache_path"]
        self.local_out_path = options["local_out_path"]


class JSONSchemaArgumentsMixin:
    """
    Mixin that adds JSON URL argument to a Command class.
    """

    json_schema_path: str

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_uri",
            type=str,
            help="Source of the JSON file (http[s]:// or local path ./...)",
        ),
        parser.add_argument(
            "-S",
            "--schema",
            dest="schema_uri",
            default=self.json_schema_path,
            type=str,
            help="Source of the JSON schema file (http[s]:// or local path ./...) "
                 "used to validate the source JSON",
        )

        if hasattr(super(JSONSchemaArgumentsMixin, self), "add_arguments"):
            getattr(super(JSONSchemaArgumentsMixin, self), "add_arguments")(parser)
