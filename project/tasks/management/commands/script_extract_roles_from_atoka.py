import csv
from itertools import groupby
import logging
import os
from os.path import abspath

from django.core.management import BaseCommand
from popolo.models import Organization

from atokaconn import AtokaConn, AtokaObjectDoesNotExist
from project.core import batch_generator


class Command(BaseCommand):
    help = (
        "Extract all roles from ATOKA, for companies owned by PA institutions"
    )

    logger = logging.getLogger(__name__)
    persons_update_strategy = None
    memberships_update_strategy = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--batch-size",
            dest="batchsize", type=int,
            default=25,
            help="Size of the batch of organizations processed at once",
        )
        parser.add_argument(
            "--offset",
            dest="offset", type=int,
            default=0,
            help="Start processing",
        )
        parser.add_argument(
            "--out-path",
            dest="outpath",
            default="./resources/data/out"
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        batchsize = options['batchsize']
        offset = options['offset']
        outpath = options['outpath']

        self.logger.info("Start")

        # start filtering current organizations with a tax_id,
        # excluding those classified as private
        organizations_qs = Organization.objects.filter(
            classifications__classification__scheme='FORMA_GIURIDICA_OP'
        ).current().exclude(
            classifications__classification_id__in=[
                11, 20, 24, 29, 48, 69, 83, 295, 321, 346, 403, 621, 941, 730, 1182, 1183, 1184, 1185, 1186, 1187,
                1188, 1190, 1189, 1191, 1192, 1193, 1194, 1195, 1196, 1197, 1198, 1199, 1200, 1201, 1202
            ]
        ).filter(identifier__isnull=False).exclude(
            classifications__classification__descr__in=[]
        ).distinct()

        # group organizations by classification counting occurrences
        organizations_groups = [
            {
                'descr': x['classifications__classification__descr'],
                'id': x['classifications__classification_id'],
                'n': 0
            }
            for x in list(organizations_qs.values(
                'classifications__classification__descr',
                'classifications__classification_id'
            ).distinct())
        ]
        for group in organizations_groups:
            group['n'] = organizations_qs.filter(classifications__classification_id=group['id']).count()
        organizations_groups = sorted(organizations_groups, key=lambda x: x['n'] * -1)

        atoka_conn = AtokaConn()
        atoka_roles = atoka_conn.allowed_roles

        counter = 0
        owned_ids = []
        owned_orgs_dict = {}
        people_ids = []
        people = []
        res_doubles = {}
        atoka_companies_requests = 0
        atoka_people_requests = 0

        for organizations_group in organizations_groups:
            self.logger.info('processing {0} institutions classified as {1}'.format(
                organizations_group['n'], organizations_group['descr']
            ))
            # generate batches of batchsize, to query atoka's endpoint
            batches = batch_generator(
                batchsize, organizations_qs.filter(
                    classifications__classification_id=organizations_group['id']
                ).values_list('identifier', flat=True).distinct().iterator()
            )
            group_owned_ids = []
            group_people_ids = []
            group_owned_orgs_dict = {}
            group_counter = 0

            for tax_ids in batches:
                # extract atoka_ownerships into a list in memory,
                # in order to use it in the two following ETL procedures

                batch_owned_ids = []
                batch_counter = 0

                # implement offset
                if counter >= offset:

                    # fetch all companies among the list having govType values set
                    try:
                        res_tot = atoka_conn.get_companies_from_tax_ids(
                            tax_ids, packages='base,shares', active='true', batch_size=1,
                        )
                        self.logger.info(
                            "- from {0} tax_ids, fetched info and shares for {1} institutions".format(
                                len(tax_ids), len(res_tot)
                            )
                        )
                        atoka_companies_requests += len(res_tot)
                    except AtokaObjectDoesNotExist:
                        res_tot = []

                    # build list of atoka_ids per tax_id
                    for r in res_tot:
                        if r['base']['taxId'] not in res_doubles:
                            res_doubles[r['base']['taxId']] = []
                        res_doubles[r['base']['taxId']].append(r['id'])

                    # remove owners with no shares
                    res_tot = list(filter(lambda x: 'shares' in x and 'sharesOwned' in x['shares'], res_tot))
                    for r in res_tot:
                        r_owned_ids = [
                            sho['id']
                            for sho in filter(
                                lambda x: x['active'] is True and x['typeOfRight'] == 'proprietà' and 'ratio' in x,
                                r['shares']['sharesOwned']
                            )
                        ]
                        if len(r_owned_ids) == 0:
                            continue

                        batch_owned_ids.extend(r_owned_ids)
                        group_owned_ids.extend(r_owned_ids)
                        owned_ids.extend(r_owned_ids)

                        batch_counter += 1
                        group_counter += 1
                        counter += 1

                    owned_orgs = atoka_conn.get_companies_from_atoka_ids(
                        batch_owned_ids, packages='base', active='true', batch_size=1
                    )
                    self.logger.debug("- fetched details for {0} owned companies".format(len(owned_orgs)))
                    atoka_companies_requests += len(owned_orgs)

                    for o in owned_orgs:
                        d = {
                            'legal_form': [x['name'] for x in o['base']['legalForms'] if x['level'] == 2][0]
                        }
                        group_owned_orgs_dict[o['id']] = d
                        owned_orgs_dict[o['id']] = d

                    batch_people = atoka_conn.get_roles_from_atoka_ids(
                        batch_owned_ids, packages='base,companies',
                        companiesRolesOfficial='true', companiesRoles=atoka_roles
                    )
                    self.logger.info("- fetched details and roles for {0} people".format(len(batch_people)))
                    atoka_people_requests += len(batch_people)

                    # append people, if not already there
                    batch_people_ids = []
                    for person in batch_people:
                        if person['id'] not in people_ids:
                            people.append(person)
                            batch_people_ids.append(person['id'])
                    group_people_ids.extend(batch_people_ids)
                    people_ids.extend(batch_people_ids)

                self.logger.info("Group {0}: {1} institutions, {2} owned_orgs, {3} companies, {4} people.".format(
                    organizations_group['descr'], group_counter, len(group_owned_ids),
                    atoka_companies_requests, atoka_people_requests
                ))

            group_owned_ids = list(set(group_owned_ids))
            group_people_ids = list(set(group_people_ids))

            self.logger.info("")
            msg = "{0} statistics".format(organizations_group['descr'])
            self.logger.info(msg)
            self.logger.info("="*len(msg))
            self.logger.info("{0} institutions searched".format(organizations_group['n']))
            self.logger.info("{0} institutions found with shares and processed".format(group_counter))
            self.logger.info("{0} active owned organizations".format(len(group_owned_ids)))
            self.logger.info("{0} official people in roles".format(len(group_people_ids)))
            self.logger.info("")

        owned_ids = list(set(owned_ids))
        people_ids = list(set(people_ids))

        self.logger.info("")
        self.logger.info("Total statistics")
        self.logger.info("================")
        self.logger.info("{0} institutions searched".format(organizations_qs.values('identifier').distinct().count()))
        self.logger.info("{0} institutions found with shares and processed".format(counter))
        self.logger.info("{0} active owned organizations".format(len(owned_ids)))
        self.logger.info("{0} official people in roles".format(len(people_ids)))

        # extract roles from people, filtering only allowed roles in owned companiew
        roles = []
        for person in filter(lambda y: 'base' in y and 'gender' in y['base'], people):
            for company in filter(lambda y: 'id' in y and y['id'] in owned_ids, person['companies']['items']):
                for role in filter(lambda y: 'official' in y, company['roles']):
                    roles.append(
                        (role.get('name', '-').lower(), owned_orgs_dict[company['id']]['legal_form'].lower())
                    )
        roles_frequency = {
            key: len(list(group)) for key, group in groupby(sorted(roles))
        }
        roles = sorted(roles_frequency.items(), key=lambda x: x[1] * -1)

        with open(os.path.join(outpath, "roles.csv"), "w") as f_roles:
            roles_writer = csv.DictWriter(
                f_roles, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL,
                fieldnames=['name', 'legal_form', 'frequency']
            )
            roles_writer.writeheader()
            for role in roles:
                roles_writer.writerow({
                    'name': role[0][0],
                    'legal_form': role[0][1],
                    'frequency': role[1]
                })
        self.logger.info("Roles written to {0}".format(abspath(f_roles.name)))

        res_doubles = {
            k: v
            for k, v in res_doubles.items() if len(v) > 1
        }
        with open(os.path.join(outpath, "doubles.csv"), "w") as f_doubles:
            doubles_writer = csv.DictWriter(
                f_doubles, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL,
                fieldnames=['tax_id', 'n', 'atoka_ids']
            )
            doubles_writer.writeheader()
            for k, v in res_doubles.items():
                doubles_writer.writerow({
                    'tax_id': k,
                    'n': len(v),
                    'atoka_ids': ",".join(v),
                })
        self.logger.info("Doubles written to {0}".format(abspath(f_doubles.name)))

        self.logger.info("End")
