# -*- coding: utf-8 -*-
import re

from ooetl import ETL
from popolo.models import Area
from taskmanager.management.base import LoggingBaseCommand
from ooetl.transformations import Transformation
from ooetl.loaders import JsonLoader

from project.tasks.etl.extractors import JsonArrayExtractor


class Candidates2022Transformation(Transformation):
    """Instance of ``ooetl.transformations.Transformation`` class that handles
    transformation of candidates information for 2022 elections
    """

    def transform(self):
        """
        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original dataframe
        od = self.etl.original_data.copy()

        def rename_roles(r):
            """Correct role types in memberships"""
            for m in r.get('memberships', []):
                if 'Candidato plurinominale' in m['role']:
                    m['role'] = 'Candidato plurinominale'
                if 'Candidato uninominale' in m['role']:
                    m['role'] = 'Candidato uninominale'

            return r

        def map_electoral_constituencies(r):
            """Map electoral constituencies within memberships,
            from Minint identifier to OPDM areas"""
            for m in r.get('memberships', []):
                constituency_original_id = m['area_id']
                if 'RI' in m['area_id']:
                    constituency_area = Area.objects.filter(
                        classification='ELECT_RIP',
                        identifiers__identifier=m['area_id']
                    ).first()
                    m['area_id'] = constituency_area.id
                    self.logger.debug(
                        f"{constituency_original_id} => {constituency_area.classification} {constituency_area.name}"
                        f" ({constituency_area.id})"
                    )
                else:
                    rm = re.match(r'Candidato .*?, (.*) \(.*\)$', m['label'])
                    constituency_area = Area.objects.filter(
                        classification='ELECT_COLL',
                        name__icontains=rm.group(1)
                    ).first()
                    if constituency_area:
                        constituency_original_id = m['area_id']
                        m['area_id'] = constituency_area.id
                        self.logger.debug(
                            f"{constituency_original_id} => {constituency_area.classification} {constituency_area.name}"
                            f" ({constituency_area.id})"
                        )
                    else:
                        self.logger.error(f"Could not find constituency for {rm.group(1)}")

            return r

        def change_electoral_list(r):
            """Change the electoral list, using the electoral_list_descr_tmp field,
            in order for the information to be imported into the membership"""
            for m in r.get('memberships', []):
                # caso plurinominale (electoral_list con possibile array multiplo o coalizione)
                # e caso uninominale (electoral_lists con array di singolo valore)
                if 'electoral_list' in m and len(m['electoral_list']):
                    if len(m['electoral_list']) == 1:
                        m['electoral_list_descr_tmp'] = m['electoral_list'][0]['desc_lista']
                    else:
                        lists = list(map(lambda x: x['desc_lista'].lower(), m['electoral_list']))
                        if '+europa' in lists:
                            m['electoral_list_descr_tmp'] = 'CENTROSINISTRA'
                        else:
                            m['electoral_list_descr_tmp'] = 'CENTRODESTRA'
                elif 'electoral_lists' in m and len(m['electoral_lists']):
                    m['electoral_list_descr_tmp'] = m['electoral_lists'][0]['desc_lista']

            return r

        od = list(map(map_electoral_constituencies, od))
        od = list(map(rename_roles, od))
        od = list(map(change_electoral_list, od))

        # store processed data into the ETL instance
        self.etl.processed_data = od

        # return ETL instance
        return self


class Command(LoggingBaseCommand):
    help = "Transform election candidates to be fed into the import_persons task"

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_url", help="Source of the JSON file (http[s]:// or file:///)"
        )
        parser.add_argument(
            dest="filepath", help="Destination JSON file, complete PATH"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        source_url = options["source_url"]
        filepath = options["filepath"]

        self.logger.info("Start records import")
        self.logger.info(f"Reading from url: {source_url}")

        self.logger.info("Starting ETL process")
        ETL(
            extractor=JsonArrayExtractor(source_url),
            loader=JsonLoader(filepath),
            transformation=Candidates2022Transformation(),
            log_level=self.logger.level,
            logger=self.logger
        )()

        self.logger.info(f"Transformed file written into {filepath}.")
        self.logger.info("End of ETL process")
