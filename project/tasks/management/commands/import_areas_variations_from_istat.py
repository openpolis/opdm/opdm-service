# -*- coding: utf-8 -*-
from datetime import datetime
import locale
import os
import re

from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
import numpy as np
from ooetl.extractors import ZIPCSVExtractor, CSVExtractor
from ooetl.transformations import Transformation
from popolo.exceptions import OverlappingDateIntervalException
from popolo.models import Area, AreaRelationship, OtherName
from slugify import slugify
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import CSVDiffCompositeETL
from project.tasks.etl.loaders.areas import PopoloAreaLoader
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class IstatVariations2PopoloTransformation(Transformation):
    """Instance of ``ooetl.ETL`` class that handles
    importing locations data from ISTAT variations into Django-Popolo model
    """

    def transform(self):
        # get a copy of the original dataframe
        od = self.etl.original_data.copy()

        # rename columns in dataframe
        od.rename(
            columns={
                "Anno": "anno",
                "Tipo variazione": "tipo_variazione",
                "Codice Regione": "regional_code",
                "Codice Comune formato alfanumerico": "city_code_1",
                "Denominazione Comune": "city_desc_1",
                "Codice del Comune associato alla variazione o nuovo codice Istat del Comune": "city_code_2",
                "Denominazione Comune associata alla variazione o nuova denominazione": "city_desc_2",
                "Provvedimento e Documento": "doc_provv",
                "Contenuto del provvedimento": "descr_provv",
                "Data decorrenza validità amministrativa": "validity_date",
            },
            inplace=True,
        )

        # remove empty rows
        od = od.dropna(how="any", subset=["tipo_variazione"])

        # strip spaces from tipo_variazione
        od["tipo_variazione"] = od.tipo_variazione.str.strip()

        # filter only values that are interesting to this task
        od = od[
            (od.tipo_variazione == "CD")
            | (od.tipo_variazione == "AP")
            | (od.tipo_variazione == "CS")
            | (od.tipo_variazione == "ES")
        ]

        date_parsing_regexp_del = re.compile(r".* [nN]\..*? del[l\' ]*(.*)")
        date_parsing_regexp_dal = re.compile(r".* [Ii][nN] vigore dal[l\' ]*(.*)")

        # clean values
        od["city_desc_1"] = od["city_desc_1"].str.strip()
        od["city_desc_2"] = od["city_desc_2"].str.strip()
        od["validity_date"] = od["validity_date"].str.strip()

        def get_data_provv(r):
            if r.validity_date is not np.nan:
                return datetime.strptime(r.validity_date, "%d/%m/%Y").strftime(
                    "%Y-%m-%d"
                )
            elif r.doc_provv is not np.nan:
                m = date_parsing_regexp_dal.match(r.doc_provv)
                if not m:
                    m = date_parsing_regexp_del.match(r.doc_provv)
                    if not m:
                        return np.nan

                # strip extra spaces
                ds = m.group(1).strip()
                # handles dates like: 6-15 settembre 2011
                ds = ds.split("-")[-1]
                # handles dates like: 24 maggio 2004, 1° S.O.
                ds = ds.split(",")[0]
                # remove non alphanumeric chars
                ds = re.sub(r"[^0-9a-zA-Z ]+", "", ds)
                # multiple spaces count as one
                ds = re.sub(r"\s+", " ", ds)

                # convert to standard format and return
                try:
                    formatted_ds = datetime.strptime(str(ds), "%d %B %Y").strftime(
                        "%Y-%m-%d"
                    )
                    return formatted_ds
                except ValueError:
                    try:
                        formatted_ds = datetime.strptime(str(ds), "%d %B%Y").strftime(
                            "%Y-%m-%d"
                        )
                        return formatted_ds
                    except ValueError:
                        self.logger.error("Could not parse {0}".format(ds))
                        return np.nan
            else:
                return np.nan

        # create `event_date` column
        od["event_date"] = od.apply(lambda r: get_data_provv(r), axis=1)

        def get_old_prov_code(r):
            if r.tipo_variazione != "AP":
                return np.nan
            return r.city_code_1[:3]

        od["old_prov_code"] = od.apply(lambda r: get_old_prov_code(r), axis=1)

        def get_new_prov_code(r):
            if r.tipo_variazione != "AP":
                return np.nan
            return r.city_code_2[:3]

        od["new_prov_code"] = od.apply(lambda r: get_new_prov_code(r), axis=1)

        prov_names_regexp = re.compile(
            r"comune della.*provincia di (.*?)"
            r"( andato| confluito| passato| aggregato|, che assume).*"
            r"provincia (di |del |dell\')(.*).*",
            re.IGNORECASE,
        )

        def get_old_prov_desc(r):
            if r.tipo_variazione != "AP":
                return np.nan
            if r.descr_provv is not np.nan:
                m = prov_names_regexp.match(r.descr_provv)
                if not m:
                    return np.nan
                return m.group(1).strip()

        od["old_prov_desc"] = od.apply(lambda r: get_old_prov_desc(r), axis=1)

        def get_new_prov_desc(r):
            if r.tipo_variazione != "AP":
                return np.nan
            if r.descr_provv is not np.nan:
                m = prov_names_regexp.match(r.descr_provv)
                if not m:
                    return np.nan
                return m.group(4).strip()

        od["new_prov_desc"] = od.apply(lambda r: get_new_prov_desc(r), axis=1)

        # sort by `event_date`, last events first
        od.sort_values(["event_date"], ascending=[0], inplace=True)

        # remove Baranzate changes annihilated by the Constitutional Court
        od = od[~((od.city_desc_1 == "Baranzate") & (od.anno != "2004"))]

        # change name to Telti (remove accent)
        od.at[od.city_desc_1 == "Teltì", "city_desc_1"] = "Telti"

        # test with few records, in order to debug
        # od = od[
        #     (od.city_desc_1 == 'Pettinengo') |
        #     (od.city_desc_2 == 'Pettinengo') |
        #     (od.city_desc_1 == 'Lessona') |
        #     (od.city_desc_1 == 'Crosa') |
        #     (od.city_desc_1 == 'Arborio') |
        #     (od.city_desc_1 == 'Lanusei') |
        #     (od.city_desc_1 == 'Cardedu') |
        #     (od.city_desc_1 == 'Padru') |
        #     (od.city_desc_2 == 'Buddusò') |
        #     (od.city_desc_1 == 'Monte Colombo') |
        #     (od.city_desc_2 == 'Monte Colombo') |
        #     (od.city_desc_1 == 'Telti') |
        #     (od.city_desc_1 == 'Teltì') |
        #     (od.city_desc_1 == 'Baranzate') |
        #     (od.city_code_1 == '078156') | (od.city_code_2 == '078156') |
        #     (od.city_desc_1 == 'Fiumicino') |
        #     (od.city_desc_1 == 'Felonica')
        #     False
        # ]

        # remove 'duplicates' events
        cs_df = od[od.tipo_variazione == "CS"]
        es_df = od[od.tipo_variazione == "ES"]

        cses_df = cs_df.merge(
            es_df,
            left_on=["city_code_1", "city_code_2"],
            right_on=["city_code_2", "city_code_1"],
            suffixes=["_cs", "_es"],
        )

        od = od[
            ~(
                od.tipo_variazione.isin(cses_df.tipo_variazione_cs)
                & od.city_code_1.isin(cses_df.city_code_1_cs)
                & od.city_code_2.isin(cses_df.city_code_2_cs)
                & od.event_date.isin(cses_df.event_date_cs)
            )
        ]

        # mark change events not part of ES/CS couples
        od["single_es"] = (od.tipo_variazione == "ES") & ~(
            od.city_code_1.isin(cses_df.city_code_1_es)
            & od.city_code_2.isin(cses_df.city_code_2_es)
            & od.event_date.isin(cses_df.event_date_es)
        )

        # store processed data into the ETL instance
        self.etl.processed_data = od

        # return ETL instance
        return self


class PopoloAreaVariationsLoader(PopoloAreaLoader):
    """:class:`Loader` updates data in ``popolo.Area`` instances
    """

    def __init__(self):
        super(PopoloAreaVariationsLoader, self).__init__()

        # sigle automobilistiche province soppresse
        self.sigle_auto = {"104": "OT", "105": "OG", "106": "VS", "107": "CI"}

    def load(self, **kwargs):
        """Stores data to ``popolo.Area`` instances,
        """

        # transform processed data into a list of dicts
        areas_dicts = self.etl.processed_data.to_dict("records")

        # main loop over already existing areas
        # and hierarchy creation
        for n, area in enumerate(areas_dicts):
            self.logger.debug(
                "{tipo_variazione}, {city_desc_1} ({city_code_1}), "
                "{city_desc_2} ({city_code_2})".format(**area)
            )

            # dynamically call specialized method with getattr
            variation_type = area["tipo_variazione"]

            def unknown_variation_type():
                self.logger.warning("Unknown variation type: " + variation_type)

            getattr(self, "load_" + variation_type.lower(), unknown_variation_type)(
                area
            )

            if n % 50 == 0:
                self.logger.info("{0}/{1}".format(n, len(areas_dicts)))

    def load_cd(self, area):
        """Implements operations for a CD change event

        CD = Cambio Denominazione (name change)

        The municipality changed name at the given date.
        The old name is added as a former name,
        with validity up to the event date.

        :param area: record containing event details
        :return:
        """
        try:
            a = Area.objects.get(
                identifiers__scheme="ISTAT_CODE_COM",
                identifiers__identifier=area["city_code_1"],
            )
        except ObjectDoesNotExist:
            self.logger.warning(
                "Could not fine Area with ISTAT_CODE_COM identifier equal to "
                + area["city_code_1"]
            )
        else:
            end_date = (
                area["event_date"]
                if area["event_date"] is not np.NaN
                else "{0}-01-01".format(area["anno"])
            )
            try:
                a.add_other_name(
                    name=area["city_desc_1"],
                    othername_type=OtherName.NAME_TYPES.former,
                    start_date=None,
                    end_date=end_date,
                    source=self.csv_source,
                )
            except OverlappingDateIntervalException as e:
                a.add_other_name(
                    name=area["city_desc_1"],
                    othername_type=OtherName.NAME_TYPES.former,
                    start_date=e.overlapping.end_date,
                    end_date=end_date,
                    source=self.csv_source,
                )
            except Exception:
                pass
            else:
                a.name = area['city_desc_2']
                a.save()

    def load_ap(self, area):
        """Implements all operations required for an AP change event

        AP = APpartenenza (belonging) change

        The municipality moved from a parent province to another,
        at the specified date

        :param area: record containing event details
        :return:
        """

        # first part: adjust ISTAT_CODE_COM identifiers

        # find new area, by parent (province)
        a = Area.objects.filter(
            parent__identifiers__scheme="ISTAT_CODE_PROV",
            parent__identifiers__identifier=area["new_prov_code"],
            name=area["city_desc_1"],
        ).filter(Q(start_date__lte=area["event_date"]) | Q(start_date__isnull=True))
        if a.count() == 0:
            # try finding new area by ISTAT code and name
            a = Area.objects.filter(
                identifiers__scheme="ISTAT_CODE_COM",
                identifiers__identifier=area["city_code_2"],
                name=area["city_desc_1"],
            ).filter(Q(start_date__lte=area["event_date"]) | Q(start_date__isnull=True))
            if a.count() == 0:
                self.logger.warning(
                    "Could not find Area {0} with "
                    "ISTAT_CODE_COM identifier {1}".format(
                        area["city_desc_1"], area["city_code_2"]
                    )
                )
                return

        if a.count() > 1:
            self.logger.error(
                "Found more than 1 area: {0} "
                "with ISTAT_CODE_COM {1}".format(
                    area["city_desc_1"], area["city_code_2"]
                )
            )
            return
        else:
            a = a.last()

        # add old identifier
        a.add_identifier(
            scheme="ISTAT_CODE_COM",
            identifier=area["city_code_1"],
            start_date=None,
            end_date=area["event_date"],
            source=self.csv_source,
            same_scheme_values_criterion=True,
            extend_overlapping=False,
            merge_overlapping=True,
        )
        # add new identifier
        a.add_identifier(
            scheme="ISTAT_CODE_COM",
            identifier=area["city_code_2"],
            start_date=area["event_date"],
            end_date=None,
            source=self.csv_source,
            same_scheme_values_criterion=True,
            extend_overlapping=False,
            merge_overlapping=True,
        )

        # second part:
        # get or create old province and
        # adjust related_areas of type former_istat_parent

        # build note, with extended description and legislation reference
        note = area["descr_provv"].strip() + ". " + area["doc_provv"].strip() + "."

        old_prov = Area.objects.filter(
            identifiers__scheme="ISTAT_CODE_PROV",
            identifiers__identifier=area["old_prov_code"],
        )

        if old_prov.count() == 0:

            # fetch sigla auto from dict
            sigla_auto = self.sigle_auto[area["old_prov_code"]]

            # create old province, with end_date
            old_prov = Area.objects.create(
                identifier=sigla_auto,
                name=area["old_prov_desc"],
                classification="ADM2",
                istat_classification=Area.ISTAT_CLASSIFICATIONS.provincia,
                slug=slugify(Area.ISTAT_CLASSIFICATIONS.provincia + " " + sigla_auto),
                end_date=area["event_date"],
            )
            old_prov.add_identifier(
                scheme="ISTAT_CODE_PROV",
                identifier=area["old_prov_code"],
                end_date=area["event_date"],
            )
            self.logger.info(
                "Could not find province {0}, corresponding to {1}. "
                "Created it.".format(area["old_prov_desc"], area["old_prov_code"])
            )
        else:
            # fetch existing province
            old_prov = old_prov.first()

        # add FIP (former ISTAT parent) relationship if not already existing
        rel, created = AreaRelationship.objects.get_or_create(
            source_area=a,
            dest_area=old_prov,
            classification=AreaRelationship.CLASSIFICATION_TYPES.former_istat_parent,
            end_date=area["event_date"],
            defaults={"start_date": None, "note": note},
        )

        # look for last FIP relationship's end_date,
        # for newly created relationships,
        # to chain end and start event
        if created:
            next_rel = (
                a.from_relationships.filter(
                    classification="FIP",
                    start_date__isnull=True,
                    end_date__isnull=False,
                )
                .exclude(end_date=area["event_date"])
                .order_by("-end_date")
                .first()
            )
            if next_rel:
                next_rel.start_date = rel.end_date
                next_rel.save()

    def load_es(self, area):
        """Implements all operations required for an ES change event

        ES = Estinzione (extintion) change

        The municipality ceased to exist, at the specified date, and was
        enclosed or merged to form a new municipality.

        :param area:
        :return:
        """
        try:
            new_area = Area.objects.get(
                identifiers__scheme="ISTAT_CODE_COM",
                identifiers__identifier=area["city_code_2"],
            )
        except ObjectDoesNotExist:
            self.logger.error(
                "Could not find area {0} with ISTAT_CODE_COM: {1}".format(
                    area["city_desc_2"], area["city_code_2"]
                )
            )
            new_area = None
        else:
            # mark start date only for those area in ES/CS events couples
            if not area["single_es"]:
                new_area.start_date = area["event_date"]
                new_area.save()

        try:
            old_area = Area.objects.get(
                identifiers__scheme="ISTAT_CODE_COM",
                identifiers__identifier=area["city_code_1"],
            )
        except ObjectDoesNotExist:
            try:
                cat_code = self.etl.istat2cat_df[
                    self.etl.istat2cat_df.ISTAT_CODE_COM == area["city_code_1"]
                ].iloc[0, 1]
            except IndexError:
                self.logger.error(
                    "Could not find CAT_CODE for ISTAT_CODE_COM: {0}. "
                    "Skipping.".format(area["city_code_1"])
                )
                return
            except Exception as e:
                self.logger.error(
                    "Error while processing load_es for ISTAT_CODE_COM: {0}. {1}"
                    "Skipping.".format(area["city_code_1"], e)
                )
            else:
                old_area, created = Area.objects.get_or_create(
                    identifier=cat_code,
                    defaults={
                        "name": area["city_desc_1"],
                        "classification": "ADM3",
                        "istat_classification": Area.ISTAT_CLASSIFICATIONS.comune,
                        "is_provincial_capital": False,
                        "slug": slugify(
                            Area.ISTAT_CLASSIFICATIONS.comune + " " + str(cat_code)
                        ),
                    },
                )
                if created:
                    self.logger.info("Created old area: {0}".format(old_area))
        else:
            try:
                area_id = old_area.identifiers.get(
                    scheme="ISTAT_CODE_COM", identifier=area["city_code_1"]
                )
            except ObjectDoesNotExist:
                try:
                    old_area.add_identifier(
                        scheme="ISTAT_CODE_COM",
                        identifier=area["city_code_1"],
                        source=self.csv_source,
                        end_date=area["event_date"],
                    )
                except Exception as e:
                    self.logger.error(
                        "Could not add identifier {0} to area {1}. "
                        "Exception {2} trapped.".format(
                            area["city_code_1"], old_area, e
                        )
                    )
            else:
                area_id.end_date = area["event_date"]
                area_id.save()

            try:
                old_area.end_date = area["event_date"]
                old_area.save()
            except Exception as e:
                self.logger.error(
                    "Could not change end_date {0} to area {1}. "
                    "Exception {2} trapped.".format(area["event_date"], old_area, e)
                )

            if new_area:
                old_area.parent = new_area.parent
                old_area.new_places.add(new_area)
                old_area.save()

    def load_cs(self, area):
        """Implements all operations required for a CS change event

        CS = CoStituzione (constitution) change

        The municipality starts to exist at the given date.

        The municipality comes from another existing municipality.

        The only thing that changes are the start date

        :param area:
        :return:
        """
        old_area = (
            Area.objects.filter(
                identifiers__scheme="ISTAT_CODE_COM",
                identifiers__identifier=area["city_code_2"],
            )
            .distinct()
            .last()
        )
        if old_area is None:
            self.logger.error(
                "Could not find area with ISTAT_CODE_COM: {0}".format(
                    area["city_code_2"]
                )
            )

        new_area = (
            Area.objects.filter(
                identifiers__scheme="ISTAT_CODE_COM",
                identifiers__identifier=area["city_code_1"],
            )
            .distinct()
            .last()
        )
        if new_area is None:
            self.logger.error(
                "Could not find area with ISTAT_CODE_COM: {0}".format(
                    area["city_code_1"]
                )
            )
        else:
            # add old_area to old_places
            if old_area:
                new_area.old_places.add(old_area)

            # add or change ISTAT_CODE_COM, with start_date
            try:
                # define start_date for new_area
                new_area.start_date = area["event_date"]
                new_area.save()
            except Exception as e:
                self.logger.error(
                    "Could not modify start_date {0} to area {1}. "
                    "Exception {2} trapped.".format(area["event_date"], new_area, e)
                )

            try:
                new_area.add_identifier(
                    scheme="ISTAT_CODE_COM",
                    identifier=area["city_code_1"],
                    source=self.csv_source,
                    start_date=area["event_date"],
                    extend_overlapping=False,
                )
            except OverlappingDateIntervalException:
                try:
                    i = new_area.identifiers.filter(
                        identifier=area["city_code_1"],
                        source=self.csv_source,
                        start_date__isnull=True,
                    ).filter(
                        Q(end_date__gt=area["event_date"]) | Q(end_date__isnull=True)
                    )
                    i.update(start_date=area["event_date"])
                except Exception as e:
                    self.logger.error(
                        "Could not add identifier {0} to area {1}. "
                        "Exception {2} trapped.".format(
                            area["city_code_1"], new_area, e
                        )
                    )


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Import Areas from ISTAT list of italian comuni"

    # change locale to correctly parse dates
    locale.setlocale(locale.LC_ALL, "it_IT.UTF-8")

    istat2cat_source_csv_url = (
        "https://docs.google.com/spreadsheets/u/2/d/"
        "1pQDEBgOsQM5IvKu_GgfU22ZTvFtVUk5pZsFRB-NSbgk/export?format=csv&"
        "id=1pQDEBgOsQM5IvKu_GgfU22ZTvFtVUk5pZsFRB-NSbgk&gid=73307488"
    )
    istat2cat_local_csv_url = os.path.abspath(
        os.path.join(str(settings.RESOURCES_PATH), "data/istat2cat.csv")
    )
    istat2cat_csv_url = istat2cat_source_csv_url
    istat2cat_df = CSVExtractor(
        istat2cat_csv_url,
        sep=",",
        encoding="utf8",
        na_values=["", "-"],
        keep_default_na=False,
    ).extract()

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, **options)
        self.handle_cache(*args, **options)

        self.setup_logger(__name__, formatter_key="simple", **options)
        self.logger.info("Starting composite ETL process")

        CSVDiffCompositeETL(
            extractor=ZIPCSVExtractor(
                source="https://www.istat.it/storage/codici-unita-amministrative"
                "/Variazioni-amministrative-e-territoriali-dal-1991.zip",
                verify_tls=False,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=IstatVariations2PopoloTransformation(),
            loader=PopoloAreaVariationsLoader(),
            extended_attributes={"istat2cat_df": self.istat2cat_df},
            clear_cache=self.clear_cache,
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename="areas_variations_from_istat.csv",
            unique_idx_cols=(0,),
            log_level=self.logger.level,
            logger=self.logger,
        )()

        self.logger.info("End of ETL process")
