# coding=utf-8
from datetime import datetime
import logging

from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.management.base import BaseCommand
from django.core.validators import URLValidator
from ooetl import ETL
from ooetl.loaders import Loader
import pandas as pd
from popolo.models import Area

from project.tasks.etl.extractors import UACSVExtractor

logger = logging.getLogger(__name__)

TABELLA_CONVERSIONE_DAIT_URL = "http://dait.interno.gov.it/" \
    "territorio-e-autonomie-locali/sut/elenco_codici_comuni_csv.php"


class Command(BaseCommand):
    help = (
        "Importazione dei codici elettorali dalla tabella (CSV) di conversione codici dei comuni italiani, "
        "aggiornata giornalmente sul sito del Dipartimento per gli Affari Interni e Territoriali, sezione Open Data. "
        "È possibile specificare un URL differente "
    )
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument(
            "--url",
            "-u",
            type=str,
            dest="url",
            help="URL of dataset.",
            required=False,
            default=TABELLA_CONVERSIONE_DAIT_URL,
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            logger.setLevel(logging.INFO)
        elif verbosity == 3:
            logger.setLevel(logging.DEBUG)
        try:
            validate = URLValidator()
            validate(options["url"])

            etl = MinIntElectoralIdentifierETL(
                extractor=UACSVExtractor(options["url"]),
                loader=MinIntElectoralIdentifierLoader(),
            )

            etl.extract().transform().load()
        except ValidationError:
            return "Invalid URL"
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes


class MinIntElectoralIdentifierETL(ETL):
    def transform(self, **kwargs):
        self.processed_data = pd.DataFrame(
            data={
                "codice_elettorale": self.original_data["CODICE ELETTORALE"],
                "codice_catastale": self.original_data["CODICE BELFIORE"],
            }
        )
        return self


class MinIntElectoralIdentifierLoader(Loader):
    def load(self, **kwargs):
        for _, row in self.etl.processed_data.iterrows():
            comune = Area.objects.comuni().filter(identifier=row.codice_catastale).get()
            today = datetime.strftime(datetime.now(), "%Y-%m-%d")

            for (area, identifier) in [
                (comune, row.codice_elettorale),
                (comune.parent, row.codice_elettorale[3:-4]),
                (comune.parent.parent, row.codice_elettorale[1:-7]),
            ]:

                try:
                    current_codice_elettorale = area.identifiers.filter(
                        scheme="MININT_ELETTORALE", end_date=None
                    ).get()

                    if (
                        current_codice_elettorale.identifier != identifier
                    ):  # Il codice è cambiato ?
                        current_codice_elettorale.update(end_date=today)
                        area.add_identifier(
                            identifier=identifier,
                            scheme="MININT_ELETTORALE",
                            start_date=today,
                            end_date=None,
                        )
                        logger.info(
                            "{}: current Identifier with scheme=MININT_ELETTORALE has changed, "
                            "added identifier={}".format(area, row.codice_elettorale)
                        )

                except ObjectDoesNotExist:
                    area.add_identifier(
                        identifier=identifier,
                        scheme="MININT_ELETTORALE",
                        start_date=None,
                        end_date=None,
                    )

                    logger.info(
                        "{}: added Identifier with scheme=MININT_ELETTORALE "
                        "and identifier={}".format(area, row.codice_elettorale)
                    )
