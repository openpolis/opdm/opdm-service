import re
from collections import defaultdict

import roman
from typing import Dict, Any, Optional, List

from popolo.models import Organization

from project.tasks.management.base import SparqlToJsonCommand
from project.tasks.parsing.common import parse_date
from project.tasks.parsing.sparql.utils import get_bindings, read_raw_rq, get_person_from_binding


class Command(SparqlToJsonCommand):
    json_filename = "senato_memberships"

    def query(self, **options) -> Optional[List[Dict]]:
        return get_bindings(
            "https://dati.senato.it/sparql",
            re.sub(
                r"osr:legislatura \d+",
                f"osr:legislatura {options['legislature']:02d}",
                read_raw_rq("senato_memberships_17.rq"),
            )
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        n_leg = options["legislature"]
        roman_leg = roman.toRoman(n_leg)

        org_senato = (
            Organization.objects.filter(identifier=f"ASS-SENATO-{n_leg}")
            .get()
        )
        if not org_senato:
            self.logger.error(
                f"Organization ASS-SENATO-{n_leg} does not exist, skipping..."
            )
            return

        # check redundant records due to different professions
        stacked_professions = defaultdict(list)
        for binding in bindings:
            key = binding["person__id"].value
            if "person__profession" in binding:
                stacked_professions[key].append(binding["person__profession"].value)

        buffer = {}
        for binding in bindings:
            tmp: Dict[str, Any] = buffer.get(
                binding["person__id"].value,
                {
                    **get_person_from_binding(binding, "OSR-URI"),
                },
            )
            if binding["person__id"].value in stacked_professions:
                tmp['profession'] = stacked_professions[binding["person__id"].value][0]

            # skip repeating memberships
            if sum(x['start_date'] == parse_date(binding["membership__start_date"].value) for x in tmp['memberships']):
                continue

            m = {
                "organization_id": org_senato.id
            }

            if "a vita" in binding["membership__role"].value:
                role_str = "Senatore a vita"
                label_str = f"Senatore {binding['membership__role'].value} - {roman_leg} Legislatura"
            else:
                role_str = "Senatore"
                label_str = f"Senatore nella {roman_leg} Legislatura"
            m['role'] = role_str
            m['label'] = label_str

            m['start_date'] = parse_date(binding["membership__start_date"].value)

            if "membership__end_date" in binding:
                tmp_end_date = parse_date(binding["membership__end_date"].value)
            else:
                tmp_end_date = None
            if not tmp_end_date:
                tmp_end_date = org_senato.dissolution_date
            m['end_date'] = tmp_end_date

            if "membership__id" in binding:
                m['identifiers'] = [
                    {"scheme": "OSR-URI", "identifier": binding["membership__id"].value}
                ]

            if "collegio" in binding:
                m['constituency_descr_tmp'] = binding["collegio"].value

            if "lista" in binding:
                m['electoral_list_descr_tmp'] = binding["lista"].value

            m['sources'] = [
                {
                    "note": "Open Data Senato",
                    "url": "https://dati.senato.it/",
                }
            ]
            tmp["memberships"].append(m)

            buffer[binding["person__id"].value] = tmp

        output = [
            {
                **values,
                "memberships": values["memberships"],
            }
            for persona, values in buffer.items()
        ]

        return output
