# -*- coding: utf-8 -*-
import logging

from django.core.management import BaseCommand
from popolo.models import Area
import requests


class Command(BaseCommand):
    help = (
        "Add or update Openpolis location_id to existing areas, "
        "among the `other_identifiers` relation."
    )

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            "--page-offset",
            dest="page_offset",
            type=int,
            default=1,
            help="The OP API page to start from.",
        )
        parser.add_argument(
            "--api-filters",
            dest="api_filters",
            default="updated_after=1970-01-01",
            help="Filters for the request to the /territori/locations endpoint, in URL querystring format:"
            " namestartswith=Signano",
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Operation started")
        if Area.objects.count() == 0:
            raise Exception("No Area objects found!")

        base_op_api_url = "http://api3.openpolis.it/territori/locations"
        page_size = 100
        op_api_url = "{0}?page_size={1}&page={2}".format(
            base_op_api_url, page_size, options["page_offset"]
        )
        if "api_filters" in options:
            op_api_url += "&{0}".format(options["api_filters"])

        while True:
            self.logger.info("fetching results from {0}".format(op_api_url))
            response = requests.get(op_api_url)
            if response.status_code != 200:
                self.logger.error(
                    "Error {0}. {1}.".format(response.status_code, response.reason)
                )
                break

            json_response = response.json()

            for res in json_response["results"]:
                op_id = res["id"].split("/")[-1]
                location_type = res["location_type"]

                try:
                    if location_type == 7:
                        areas = Area.objects.filter(
                            classification="RGNE",
                            identifier="{0:1d}".format(res["macroregional_id"]),
                        )
                    elif location_type == 4:
                        areas = Area.objects.filter(
                            classification="ADM1",
                            identifier="{0:02d}".format(res["regional_id"]),
                        )
                    elif location_type == 5:
                        areas = Area.objects.filter(
                            classification="ADM2",
                            identifiers__scheme="ISTAT_CODE_PROV",
                            identifiers__identifier="{0:03d}".format(
                                res["provincial_id"]
                            ),
                        )
                    elif location_type == 6:
                        areas = Area.objects.filter(
                            classification="ADM3",
                            identifiers__scheme="ISTAT_CODE_COM",
                            identifiers__identifier="{0:06d}".format(res["city_id"]),
                        )
                    else:
                        self.logger.warning(
                            "Could not find area while adding {0}."
                            "location_type: {1}.".format(op_id, location_type)
                        )
                        continue

                    if areas.count() == 1:
                        area = areas.first()
                    elif areas.count() == 2:
                        area = (
                            areas.filter(end_date__isnull=True)
                            .order_by("start_date")
                            .last()
                        )
                    else:
                        self.logger.warning(
                            "Could not find Area corresponding to {0}.".format(res)
                        )
                        continue

                    area.add_identifier(
                        identifier=op_id,
                        scheme="OP_ID",
                        source="http://api3.openpolis.it",
                    )
                    area.save()
                    self.logger.debug("{0} added to {1}".format(op_id, area))

                except Exception as e:
                    self.logger.warning(
                        "Error {0} caught while adding {1}.".format(e, res)
                    )
                    continue

            op_api_url = json_response["next"]
            if op_api_url is None:
                break

        self.logger.info("Operation finished")
