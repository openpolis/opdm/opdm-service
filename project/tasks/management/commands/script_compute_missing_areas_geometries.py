# -*- coding: utf-8 -*-
from datetime import datetime

from django.contrib.gis import geos
from popolo.models import Area
from taskmanager.management.base import LoggingBaseCommand

CURRENT_YEAR = datetime.now().year


class Command(LoggingBaseCommand):
    help = 'Compute geometry fields of Areas with null geometries'
    start_date = None

    def add_arguments(self, parser):
        # Named (optional) arguments
        parser.add_argument(
            '--start-date',
            dest='start_date',
            default=f"{CURRENT_YEAR}-01-01",
            help='Date to start with checking for missing geometries in Areas. Format YYYY-MM-DD.'
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.start_date = options['start_date']
        self.logger.info("Starting process")

        areas = Area.objects.comuni().filter(
            start_date__gt=self.start_date,
            end_date__isnull=True,
            geometry__isnull=True,
        ).distinct()

        if areas.count() > 0:
            self.logger.info("New areas coming from fusion of old ones")
        for area in areas:
            self.logger.info(f"Processing {area.name}")
            old_areas = Area.objects.filter(new_places=area)
            geom_union = old_areas[0].geometry
            self.logger.info(f"- adding {old_areas[0].name} geometry")
            for old_area in old_areas[1:]:
                geom_union |= old_area.geometry
                self.logger.info(f"- adding {old_area.name} geometry")
            if isinstance(geom_union, geos.MultiPolygon):
                area.geometry = geom_union
            else:
                area.geometry = geos.MultiPolygon(geom_union)
            area.save()

        self.logger.info("Finished.")
