from datetime import datetime
import pandas as pd

from taskmanager.management.base import LoggingBaseCommand

from popolo.models import Person  # , Membership


class Command(LoggingBaseCommand):
    help = "Read current vicesindaci potential memberships from a csv file (minint), and remove them."
    dry_run = None

    def add_arguments(self, parser):  # noqa: D102
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="dry_run",
            help="Only show modifications do not save.",
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)
        # dry_run = options['dry_run']

        self.logger.info("Start")

        df = pd.read_csv(
            './data/vicesindaci_che_non_lo_erano.csv',
            index_col=False,
            sep=";",
            header=None,
            encoding="latin1",
        )[[7, 8, 10, 11, 14]]
        df.columns = ['cognome', 'nome', 'data_nascita', 'luogo_nascita', 'data_inizio']
        for index, row in df.iterrows():
            data_nascita = datetime.strptime(row.data_nascita, "%d/%m/%Y").strftime("%Y-%m-%d")
            try:
                p = Person.objects.get(
                    family_name__iexact=row.cognome,
                    given_name__iexact=row.nome,
                    birth_date=data_nascita
                )
            except Person.DoesNotExist:
                self.logger.warning(f"{row.cognome} {row.nome} {data_nascita} not found in OPDM")
                continue
            except Person.MultipleObjectsReturned:
                n_results = Person.objects.filter(
                    family_name__iexact=row.cognome,
                    given_name__iexact=row.nome,
                    birth_date=data_nascita
                ).count()
                self.logger.warning(f"{row.cognome} {row.nome} {data_nascita} returns {n_results} records in OPDM")
                continue

            m = p.memberships.filter(role='Vicesindaco', end_date__isnull=True)
            if m.count() == 1:
                if not self.dry_run:
                    m.delete()
                self.logger.info(f"{index} - {row.cognome} {row.nome} - {data_nascita} - DELETED")
            elif m.count() == 0:
                self.logger.debug(f"{index} - {row.cognome} {row.nome} - {data_nascita} - NO MEMBERSHIP")
            elif m.count() > 1:
                self.logger.warning(f"{index} - {row.cognome} {row.nome} - {data_nascita} - MORE THAN 1 MEMBERSHIP (?)")

        self.logger.info("Finished")
