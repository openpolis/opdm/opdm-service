import logging

from django.core.management import BaseCommand

from project.tasks.etl.extractors import OPAPIPagedExtractor
from project.tasks.etl.loaders import DummyLoader
from project.tasks.etl.loaders.persons import (
    PopoloPersonLoader,
    PopoloMembershipLoader,
)
from project.tasks.etl.transformations import Op2OpdmETL


class Command(BaseCommand):
    help = "Import Persons from Openpolis API"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            "institutions",
            type=int,
            nargs="+",
            help="The institutions IDs to import (see: http://api3.openpolis.it/politici/institutions)",
        )
        parser.add_argument(
            "--starting-page",
            dest="starting_page",
            type=int,
            default=1,
            help="The page to start importing from, defaults to 1",
        )
        parser.add_argument(
            "--page-size",
            dest="page_size",
            type=int,
            default=250,
            help="Number of results per page to fetch, defaults to 250",
        )
        parser.add_argument(
            "--n-pages",
            dest="n_pages",
            type=int,
            default=0,
            help="Number of pages to fetch (defaults to 0: all pages)",
        )
        parser.add_argument(
            "--persons-update-strategy",
            dest="persons_update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to keep_old",
        )
        parser.add_argument(
            "--api-filters",
            dest="api_filters",
            default="updated_after=1970-01-01",
            help="Filters for the request to the /politici/instcharges endpoint, in URL querystring format:"
            " charge_type_id=13&location_id=4&updated_after=1970-01-01",
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start ETL process")

        institutions_ids = options["institutions"]
        starting_page = options["starting_page"]
        page_size = options["page_size"]
        n_pages = options["n_pages"]
        persons_update_strategy = options["persons_update_strategy"]
        api_filters = options["api_filters"]

        for institution_id in institutions_ids:
            self.logger.info("Starting ETL process")

            self.logger.info("Extraction from OPAPI")
            api_filters += "&institution_id={0}".format(institution_id)
            etl = Op2OpdmETL(
                extractor=OPAPIPagedExtractor(
                    "http://api3.openpolis.it/politici/instcharges",
                    starting_page=starting_page,
                    page_size=page_size,
                    n_pages=n_pages,
                    api_filters=api_filters,
                ),
                loader=DummyLoader(),
                log_level=self.logger.level,
            )
            etl = etl.extract()

            self.logger.info("Persons ETL transformation and loading")
            etl.set_loader(
                PopoloPersonLoader(
                    context="OPAPI import",
                    lookup_strategy="identifier",
                    update_strategy=persons_update_strategy,
                )
            )
            etl.transform_persons().load()

            self.logger.info("Memberships ETL transformation and loading")
            etl.set_loader(PopoloMembershipLoader())
            etl.transform_memberships().load()

            self.logger.info("ETL process finished")

        self.logger.info("End")
