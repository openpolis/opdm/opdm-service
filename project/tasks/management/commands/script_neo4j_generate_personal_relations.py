from django.conf import settings
from neo4j import GraphDatabase

from taskmanager.management.base import LoggingBaseCommand


classifications = [
    'Associazione riconosciuta',
    'Istituto o ente pubblico di ricerca',
    'Società di mutuo soccorso',
    'Altra forma di ente privato con personalità giuridica',
    'Società di mutua assicurazione',
    'Ufficio di diretta collaborazione',
    'Dipartimento/Direzione/Ufficio',
    'Ente ambientale regionale',
    'Società cooperativa a mutualità prevalente',
    'Gruppo europeo di interesse economico',
    'Giunta regionale',
    'Partito/Movimento politico',
    'Altro ente pubblico non economico',
    'Agenzia dello Stato',
    'Società a responsabilità limitata con un unico socio',
    'Società per azioni',
    'Organo costituzionale o a rilevanza costituzionale',
    'Azienda pubblica di servizi alle persone ai sensi del dlgs n 207/2001',
    'Associazione non riconosciuta',
    'Società consortile',
    'Autorità indipendente',
    'Giunta provinciale',
    'Società cooperativa sociale',
    'Ente parco',
    'Azienda o ente del servizio sanitario nazionale',
    'Università pubblica',
    'Ente pubblico economico',
    'Agenzie, enti e consorzi',
    'Società semplice',
    'Fondazione (esclusa fondazione bancaria)',
    'Società in nome collettivo',
    'Forza armata o di ordine pubblico e sicurezza',
    'Impresa individuale non agricola',
    'Società a responsabilità limitata',
    'Giunta comunale',
    'Azienda pubblica di servizi alle persone ai sensi del d.lgs n 207/2001',
    'Società in accomandita per azioni',
    'Azienda speciale ai sensi del t.u. 267/2000',
    'Società cooperativa diversa',
    'Governo della Repubblica',
    'Società di fatto o irregolare, comunione ereditaria',
    'Società in accomandita semplice',
    'Impresa o ente privato costituito all’estero non altrimenti classificabile '
    'che svolge una attività economica in Italia'
]


class Command(LoggingBaseCommand):
    help = (
        "Invoke queries in Neo4j to generate the personal relations from the memberships graph."
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "--show-classifications",
            dest="show_classifications",
            action="store_true",
            help="List classifications with n. of relations each, without generating relations."
        )
        parser.add_argument(
            "--selected-classification",
            dest="selected_classification",
            default="all",
            help="Only memberships in organizations of this classification (as a string), are processed."
        )
        parser.add_argument(
            "--neo4j-uri",
            dest="neo4j_uri",
            default=settings.NEO4J_URI,
            help="Overrides neo4j connection uri."
        )
        parser.add_argument(
            "--neo4j-username",
            dest="neo4j_username",
            default=settings.NEO4J_USERNAME,
            help="Overrides neo4j connection user."
        )
        parser.add_argument(
            "--neo4j-password",
            dest="neo4j_password",
            default=settings.NEO4J_PASSWORD,
            help="Overrides neo4j connection password."
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Start")

        show_classifications = options["show_classifications"]
        selected_classification = options["selected_classification"]
        uri = options["neo4j_uri"]
        username = options["neo4j_username"]
        password = options["neo4j_password"]

        driver = GraphDatabase.driver(
           uri, auth=(username, password)
        )

        allowed_classifications = classifications
        if show_classifications:
            query = \
                "MATCH (p1:Persona)-[r1:INCARICO_IN]->(o:Organizzazione)<-[r2:INCARICO_IN]-(p2:Persona) " \
                "WHERE p1.id < p2.id AND o.classification IN $allowed_classifications " \
                "WITH p1, p2, o " \
                "RETURN o.classification as classification, COUNT(*) as n_relations;"
            with driver.session() as neo4j_session:
                res = neo4j_session.run(query, allowed_classifications=allowed_classifications)
                for r in res:
                    self.logger.info(f"{r['classification']} - {r['n_relations']}")

        else:
            if selected_classification != 'all':
                allowed_classifications = [selected_classification, ]

            def build_query(rel_label, pers_rel_label, shared_label):
                query = \
                    "WITH apoc.date.currentTimestamp()/1000 AS now_ts " \
                    f"MATCH (p1:Persona)-[r1:{rel_label}]->(o:Organizzazione)<-[r2:{rel_label}]-(p2:Persona) " \
                    "WHERE p1.id < p2.id AND o.classification =~ $allowed_classification " \
                    "WITH " \
                    "  now_ts, p1, p2, " \
                    "  apoc.coll.min([" \
                    "   coalesce(r1.end_date_ts, now_ts), " \
                    "   coalesce(r2.end_date_ts, now_ts)" \
                    "  ]) AS min_ts," \
                    "  apoc.coll.max([" \
                    "    coalesce(r1.start_date_ts, now_ts), " \
                    "    coalesce(r2.start_date_ts, now_ts)" \
                    "  ]) AS max_ts," \
                    "  r1, r2, o " \
                    "WITH " \
                    "  now_ts, p1, p2, r1, r2, o, " \
                    "  apoc.date.convert(min_ts - max_ts, 's', 'd') AS n_days, " \
                    "  apoc.date.convert(now_ts - min_ts, 's', 'd') AS n_days_from_end, " \
                    "  apoc.date.format(max_ts, 's', 'YYYY') AS start_year " \
                    f"MERGE (p1)-[r:{pers_rel_label}]-(p2) " \
                    "SET " \
                    "  r.organization = o.name, "\
                    "  r.org_classification = o.classification, " \
                    "  r.n_days = n_days, " \
                    "  r.n_days_from_end = n_days_from_end, " \
                    "  r.start_year = start_year, " \
                    "  r.complete_label = " \
                    "    ( CASE " \
                    "        WHEN n_days_from_end > 0 " \
                    f"        THEN 'Ha condiviso {shared_label} in ' + o.name + " \
                    "             ' a partire dal ' + start_year + ', per ' + n_days + ' giorni ' " \
                    f"        ELSE 'Condivide {shared_label} in ' + o.name + ' a partire dal ' + start_year " \
                    "      END );"
                return query

            for classification in allowed_classifications:
                with driver.session() as neo4j_session:
                    m_query = build_query(
                        rel_label='INCARICO_IN', pers_rel_label='CONDIVIDE_INCARICHI', shared_label="incarichi"
                    )
                    o_query = build_query(
                        rel_label='DETIENE_QUOTA_DI', pers_rel_label='CONDIVIDE_QUOTE', shared_label="quote"
                    )

                    self.logger.debug(f" {classification} - START")
                    neo4j_session.run(m_query, allowed_classification=classification)
                    res = neo4j_session.run(
                        "MATCH p=()-[r:CONDIVIDE_INCARICHI {org_classification: $classification}]->() "
                        "RETURN count(p) as n",
                        classification=classification
                    ).value()
                    self.logger.info(f" {classification} -   {res[0]} relations from memberships merged")

                    neo4j_session.run(o_query, allowed_classification=classification)
                    res = neo4j_session.run(
                        "MATCH p=()-[r:CONDIVIDE_QUOTE {org_classification: $classification}]->() "
                        "RETURN count(p) as n",
                        classification=classification
                    ).value()
                    self.logger.info(f" {classification} -   {res[0]} relations from ownerships merged")
                    self.logger.debug(f" {classification} - END")

            with driver.session() as neo4j_session:
                res = neo4j_session.run(
                    "MATCH p=()-[r:CONDIVIDE_INCARICHI]->() "
                    "RETURN count(p) as n"
                ).value()
                self.logger.info(f"TOTAL - {res[0]} relations from memberships in the graph")
                res = neo4j_session.run(
                    "MATCH p=()-[r:CONDIVIDE_QUOTE]->() "
                    "RETURN count(p) as n"
                ).value()
                self.logger.info(f"TOTAL - {res[0]} relations from memberships in the graph")

        self.logger.info("End")
