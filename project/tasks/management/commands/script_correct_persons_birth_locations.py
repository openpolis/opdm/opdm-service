from popolo.models import Person, Area
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Correct all Persons birth_location, filling it up whenever possible"

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action='store_true',
            help="Show logs, do not modify data in DB.",
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)

        dry_run = options['dry_run']

        self.logger.info("Start")

        persons = Person.objects.filter(birth_location_area__isnull=False, birth_location__isnull=True)
        n_persons = persons.count()
        self.logger.info(
            f"Add birth_location string when missing and birth_location_area is known ({n_persons})"
        )
        for n, p in enumerate(persons, start=1):
            old_birth_location = p.birth_location
            p.birth_location = "{0} ({1})".format(
                p.birth_location_area.name, p.birth_location_area.parent.identifier
            )
            self.logger.debug(f"{old_birth_location} => {p.birth_location}")
            if not dry_run:
                p.save()
            if n % 100 == 0:
                self.logger.info(f"{n}/{n_persons}")

        persons = Person.objects.filter(birth_location_area__isnull=False).exclude(
            birth_location__contains="("
        )
        n_persons = persons.count()
        self.logger.info(
            f"Add province to birth_location string when missing and birth_location_area is known ({n_persons})"
        )
        for n, p in enumerate(persons):
            old_birth_location = p.birth_location
            p.birth_location = "{0} ({1})".format(
                p.birth_location_area.name, p.birth_location_area.parent.identifier
            )
            self.logger.debug(f"{old_birth_location} => {p.birth_location}")
            if not dry_run:
                p.save()
            if n % 100 == 0:
                self.logger.info(f"{n}/{n_persons}")

        # extract areas dictionary
        persons = Person.objects.exclude(birth_location__contains="(")
        n_persons = persons.count()
        areas_dict = {
            a["name"].lower(): "{0} ({1})".format(a["name"], a["parent__identifier"])
            for a in Area.objects.filter(istat_classification="COM").values(
                "name", "parent__identifier"
            )
        }
        self.logger.info(
            f"Update birth_location still without province, with names found in areas ({n_persons})"
        )
        for n, p in enumerate(persons, start=1):
            if p.birth_location and p.birth_location.lower() in areas_dict:
                old_birth_location = p.birth_location
                p.birth_location = areas_dict[p.birth_location.lower()]
                self.logger.debug(f"{old_birth_location} => {p.birth_location}")
                if not dry_run:
                    p.save()
            if n % 100 == 0:
                self.logger.info(f"{n}/{n_persons}")

        self.logger.info("End")
