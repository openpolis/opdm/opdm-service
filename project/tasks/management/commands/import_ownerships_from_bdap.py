# -*- coding: utf-8 -*-
from django.conf import settings
from ooetl.extractors import CSVExtractor
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import CSVDiffCompositeETL
from project.tasks.etl.loaders.organizations import PopoloOrgOwnershipLoader
from project.tasks.etl.transformations import BdapOwnership2OPDMTransformation


class Command(LoggingBaseCommand):
    help = "Import ownerships among organizations from BDAP lists of active entities"

    source_url = "https://bdap-opendata.mef.gov.it/export/csv/Anagrafe-Enti---Partecipazioni-Ente.csv"
    filename = "ownerships_from_bdap.csv"
    encoding = "latin1"
    sep = ";"
    unique_idx_cols = (0, 1, 2)

    def add_arguments(self, parser):
        parser.add_argument(
            "--local-cache-path",
            dest="local_cache_path",
            default=settings.IMPORT_CACHE_PATH,
            help="Where downloaded file should be stored for caching and diffing",
        )
        parser.add_argument(
            "--local-out-path",
            dest="local_out_path",
            default=settings.IMPORT_OUT_PATH,
            help="Where produced file should be stored for successive manual processing",
        )
        parser.add_argument(
            "--update-strategy",
            dest="update_strategy",
            default="overwrite",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to overwrite",
        )
        parser.add_argument(
            "--lookup-strategy",
            dest="lookup_strategy",
            default="mixed",
            help="Whether to lookup using anagraphical (name and founding date), "
            "identifier (see --identifier-scheme) or mixed (identifier first, then cascade to anagraphical)",
        )
        parser.add_argument(
            "--identifier-scheme",
            dest="identifier_scheme",
            default="CODICE_ENTE_BDAP",
            help="Which scheme to use with identifier/mixed lookup strategy",
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.logger.info("Start records import")
        self.logger.info("Reading CSV from url: {0}".format(self.source_url))

        update_strategy = options["update_strategy"]
        lookup_strategy = options["lookup_strategy"]
        identifier_scheme = options["identifier_scheme"]
        local_cache_path = options["local_cache_path"]
        local_out_path = options["local_out_path"]

        CSVDiffCompositeETL(
            extractor=CSVExtractor(
                self.source_url,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=BdapOwnership2OPDMTransformation(),
            loader=PopoloOrgOwnershipLoader(
                update_strategy=update_strategy,
                lookup_strategy=lookup_strategy,
                identifier_scheme=identifier_scheme,
                log_step=250
            ),
            local_cache_path=local_cache_path,
            local_out_path=local_out_path,
            filename=self.filename,
            unique_idx_cols=self.unique_idx_cols,
            log_level=self.logger.level,
        )()
