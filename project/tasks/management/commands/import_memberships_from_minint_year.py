# -*- coding: utf-8 -*-
from datetime import datetime, timedelta

from ooetl import ETL
import requests
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.extractors import UACSVExtractor
from project.tasks.etl.loaders.persons import PopoloPersonMembershipLoader
from project.tasks.etl.transformations.minint2opdm import (
    HistMinintReg2OpdmTransformation,
    HistMinintProv2OpdmTransformation,
    HistMinintMetro2OpdmTransformation,
    HistMinintCom2OpdmTransformation,
)


class Command(LoggingBaseCommand):
    help = "Import Persons and Memberships from historical MinInt source"

    def add_arguments(self, parser):
        parser.add_argument(
            "--context",
            dest="context",
            default="reg",
            help="The context (reg|regione|regioni|prov|provincia|cm|metropoli|com|comune|comuni).",
        )
        parser.add_argument(
            "--year",
            dest="year",
            default=(datetime.now() - timedelta(days=490)).strftime("%Y"),
            help="The year. Defaults to previous year plus 4 months.",
        )
        parser.add_argument(
            "--csv-url",
            dest="csv_url",
            help="Alternative to --year, forces CSV reading from local or remote URL. Still need --context.",
        )
        parser.add_argument(
            "--loc-code",
            dest="loc_code",
            help="Limits import to those records matching this. May use the '%%' as jolly (as in --loc-code=03%%).",
        )
        parser.add_argument(
            "--loc-desc",
            dest="loc_desc",
            help="Limits import to those records matching this. May use the '%%' as jolly (as in --loc-desc=Fubine%%).",
        )
        parser.add_argument(
            "--persons-update-strategy",
            dest="persons_update_strategy",
            default="overwrite_minint_opdm",
            help="Whether to keep old values or to overwrite them "
            "for Person updates (keep_old | overwrite | overwrite_minint_opdm), defaults to overwrite_minint_opdm",
        )
        parser.add_argument(
            "--memberships-update-strategy",
            dest="memberships_update_strategy",
            default="overwrite_minint_opdm",
            help="Whether to keep old values or to overwrite them "
            "for Membership updates (keep_old | overwrite | overwrite_minint_opdm), defaults to overwrite_minint_opdm",
        )

    @staticmethod
    def get_csv_url(year, context):
        """Return correct URL for context and year, probing the source (as different patterns exist)

        :param year:
        :param context:
        :return:
        """
        base_url = "https://dait.interno.gov.it/documenti/storico_amministratori_"
        for csv_url_tpl in [
            "{0}{1}3112{2}.csv",
            "{0}{1}3112{2}_0.csv",
            "{0}{1}_3112{2}.csv",
        ]:
            csv_url = csv_url_tpl.format(base_url, context, year)
            res = requests.head(
                csv_url,
                headers={
                    "User-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) "
                    "AppleWebKit/537.36 (KHTML, like Gecko) "
                    "Chrome/67.0.3396.99 Safari/537.36"
                },
            )
            if res.status_code == 200:
                return csv_url

        return None

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        context = options["context"]
        year = options["year"]
        persons_update_strategy = options["persons_update_strategy"]
        memberships_update_strategy = options["memberships_update_strategy"]

        if context in ["reg", "regione", "regioni"]:
            transformation_class = HistMinintReg2OpdmTransformation
            url_context = "regioni"
        elif context in ["prov", "provincia", "province"]:
            transformation_class = HistMinintProv2OpdmTransformation
            url_context = "province"
        elif context in ["cm", "metro", "metropoli"]:
            if int(year) < 2015:
                raise Exception("No metropolitan areas before 2015")
            transformation_class = HistMinintMetro2OpdmTransformation
            url_context = "province"
        elif context in ["com", "comune", "comuni"]:
            transformation_class = HistMinintCom2OpdmTransformation
            url_context = "comuni"
        else:
            raise Exception("wrong context")

        csv_url = options.get("csv_url", None)

        if csv_url is None:
            csv_url = self.get_csv_url(year, url_context)

        if csv_url is None:
            raise Exception("could not find correct csv_url")

        loc_code = options.get("loc_code", None)
        loc_desc = options.get("loc_desc", None)

        self.logger.info("Reading CSV from url: {0}".format(csv_url))

        etl = ETL(
            extractor=UACSVExtractor(
                url=csv_url,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            loader=PopoloPersonMembershipLoader(
                context=context,
                year=year,
                persons_update_strategy=persons_update_strategy,
                memberships_update_strategy=memberships_update_strategy,
            ),
            transformation=transformation_class(
                loc_code_filter=loc_code, loc_desc_filter=loc_desc, year=year
            ),
            log_level=self.logger.level,
            logger=self.logger
        )

        self.logger.info("Starting ETL process")
        try:
            etl.etl()
        except Exception as e:
            self.logger.error(e)
        self.logger.info("End of ETL process")
