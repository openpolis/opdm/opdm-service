from popolo.models import Membership
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Corrections for Memberships end dates and existing electoral events"

    def add_arguments(self, parser):  # noqa: D102
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="dry_run",
            help="Only show modificationsm do not save.",
        )
        parser.add_argument(
            "--context",
            dest="context",
            help="The context to apply correctons [com|prov|reg].",
        )

        parser.add_argument(
            "--start-year",
            dest="start_year",
            type=int,
            help="Years to start considering memberships for corrections."
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        dry_run = options["dry_run"]

        start_year = options["start_year"]
        if start_year < 1990 or start_year > 2018:
            raise Exception("Need to use a start_year in the 1990-2018 interval")

        context = options["context"]
        if context.lower() not in ["com", "reg", "prov"]:
            raise Exception("Need to use a context among 'com', 'prov', 'reg'")
        if context.lower() == 'reg':
            classifications = ["Giunta regionale", "Consiglio regionale"]
        elif context.lower() == 'prov':
            classifications = ["Giunta provinciale", "Consiglio provinciale"]
        else:  # com is the only possibility left at this point
            classifications = ["Giunta comunale", "Consiglio comunale"]

        self.logger.info("Start")

        ms = Membership.objects.filter(
            electoral_event__isnull=False,
            start_date__gte=str(start_year),
            end_date__isnull=False,
            organization__classification__in=classifications
        )

        nms = ms.count()
        for i, m in enumerate(ms, start=1):
            if i % 100 == 0:
                self.logger.info(f"{i}/{nms}")

            # compute the current and next events for this membership
            # out of the apical positions events
            event, next_event = m.this_and_next_electoral_events(logger=self.logger)

            # verify & correct wrong electoral_event
            if event and m.electoral_event != event:
                self.logger.info(f"Event: {m.id} - {m.electoral_event} => {event}")
                m.electoral_event = event
                if not dry_run:
                    try:
                        m.save()
                    except Exception as e:
                        self.logger.warning(f" {m.id} {e}")

            # verify & correct end_date if it's greater than the next electoral event's date
            if next_event and m.end_date > next_event.start_date:
                self.logger.info(f"End date: {m.id} - {m.end_date} => {next_event.start_date}")
                m.end_date = next_event.start_date
                if not dry_run:
                    try:
                        m.save()
                    except Exception as e:
                        self.logger.warning(f" {m.id} {e}")

        self.logger.info("Finished")
