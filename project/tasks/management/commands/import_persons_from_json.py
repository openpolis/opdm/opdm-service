from datetime import datetime
import logging

from django.conf import settings
from ooetl import DummyTransformation
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import JsonDiffCompositeETL
from project.tasks.etl.composites.diff_logics import (
    person_key_from_dict, JsonOutRemovesDiffLogic,
    CloseInnerMembershipsOwnershipsRemovesDiffLogic, CheckInnerDetailsUpdatesDiffLogic,
)
from project.tasks.etl.extractors import JsonArrayExtractor
from project.tasks.etl.loaders.persons import PopoloPersonWithRelatedDetailsLoader
from project.tasks.etl.transformations.json2opdm import (
    JsonPersonsMemberships2OpdmTransformation,
)
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Import Persons with related Memberships and Ownerships from a JSON source"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_url", help="Source of the JSON file (http[s]:// or file:///)"
        )
        parser.add_argument(
            "--original-source",
            dest="original_source",
            help="Original source of the data, to add to sources attributes (ex: https://api.atoka.io)"
        )
        parser.add_argument(
            "--persons-update-strategy",
            dest="persons_update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to keep_old",
        )
        parser.add_argument(
            "--persons-lookup-strategy",
            dest="persons_lookup_strategy",
            default="mixed",
            help="How to lookup a person: anagraphical, identifier or mixed. "
            "See --persons-lookup-identifiers-scheme, below to select the scheme for identifier or mixed.",
        )
        parser.add_argument(
            "--persons-lookup-identifier-scheme",
            dest="persons_lookup_identifier_scheme",
            default="CF",
            help="Which identifier scheme to use if lookup_strategy is set to identifier or mixed",
        )
        parser.add_argument(
            "--memberships-update-strategy",
            dest="memberships_update_strategy",
            default="overwrite_minint_opdm",
            help="Whether to keep old values or to overwrite them "
            "for Membership updates (keep_old | overwrite | overwrite_minint_opdm), defaults to overwrite_minint_opdm",
        )
        parser.add_argument(
            "--ownerships-update-strategy",
            dest="ownerships_update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to keep_old",
        )
        parser.add_argument(
            "--use-dummy-transformation",
            dest="use_dummy_transformation",
            action="store_true",
            help="Whether to use the DummyTransformation instead of JsonPersonsMemberships2OpdmTransformation",
        )
        parser.add_argument(
            "--check-membership-label",
            dest="check_membership_label",
            action="store_true",
            help="Whether to check membership labels when updating",
        )
        parser.add_argument(
            "--skip-near-start-dates",
            dest="skip_near_start_dates",
            action="store_true",
            help="Whether to skip near start_dates merging when updating or creating new memberships",
        )
        parser.add_argument(
            "--near-start-date-lo",
            dest="near_start_date_lo",
            type=int, default=60,
            help="Neighbouring for the start date in the past (n. of days)",
        )
        parser.add_argument(
            "--near-start-date-hi",
            dest="near_start_date_hi",
            type=int, default=180,
            help="Neighbouring for the start date in the future (n. of days)",
        )
        parser.add_argument(
            "--close-missing",
            dest="close_missing",
            action="store_true",
            help="Whether to close memberships that were in the cache, but not in the live records.",
        )
        parser.add_argument(
            "--log-step",
            dest="log_step",
            type=int,
            default=50,
            help="Number of steps to log process completion to stdout. Defaults to 500.",
        )
        parser.add_argument(
            "--aka-lo",
            dest="aka_lo",
            type=int,
            default=settings.AKA_LO_THRESHOLD,
            help="Threshold below which a similarity is discarded",
        )
        parser.add_argument(
            "--aka-hi",
            dest="aka_hi",
            type=int,
            default=settings.AKA_HI_THRESHOLD,
            help="Threshold above which a similarity is considered identical",
        )
        parser.add_argument(
            "--context",
            dest="context",
            default='parlamento_it',
            help="Context string used in loader_context",
        )

        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.handle_cache(*args, **options)

        self.logger.info("Start loading procedure")

        persons_update_strategy = options["persons_update_strategy"]
        persons_lookup_strategy = options["persons_lookup_strategy"]
        persons_lookup_identifier_scheme = options["persons_lookup_identifier_scheme"]
        memberships_update_strategy = options["memberships_update_strategy"]
        ownerships_update_strategy = options["ownerships_update_strategy"]
        check_membership_label = options["check_membership_label"]
        check_near_start_dates = not options["skip_near_start_dates"]
        near_start_date_lo = options["near_start_date_lo"]
        near_start_date_hi = options["near_start_date_hi"]
        original_source = options["original_source"]
        close_missing = options["close_missing"]
        source_url = options["source_url"]
        log_step = options["log_step"]
        context = options["context"]
        aka_lo = options["aka_lo"]
        aka_hi = options["aka_hi"]
        use_dummy_transformation = options["use_dummy_transformation"]
        filename = source_url[source_url.rfind("/")+1:]

        if use_dummy_transformation:
            transformation = DummyTransformation()
        else:
            transformation = JsonPersonsMemberships2OpdmTransformation()

        if close_missing:
            removes_diff_logic = CloseInnerMembershipsOwnershipsRemovesDiffLogic(
                end_field='end_date',
                end_date=datetime.strftime(datetime.now(), "%Y-%m-%d"),
                end_reason='record missing from source',
            )
        else:
            removes_diff_logic = JsonOutRemovesDiffLogic(local_out_path=self.local_out_path)

        if check_membership_label:
            updates_diff_logic = CheckInnerDetailsUpdatesDiffLogic(
                person_key_getter=person_key_from_dict,
                membership_key_getter=lambda membership: (
                    membership['organization_id'], membership['role'], membership['label'], membership['start_date']
                )
            )
        else:
            updates_diff_logic = CheckInnerDetailsUpdatesDiffLogic(
                person_key_getter=person_key_from_dict
            )

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(source_url),
            loader=PopoloPersonWithRelatedDetailsLoader(
                context=context,
                update_strategy=persons_update_strategy,
                lookup_strategy=persons_lookup_strategy,
                identifier_scheme=persons_lookup_identifier_scheme,
                memberships_update_strategy=memberships_update_strategy,
                ownerships_update_strategy=ownerships_update_strategy,
                check_membership_label=check_membership_label,
                check_near_start_dates=check_near_start_dates,
                near_start_date_lo=near_start_date_lo,
                near_start_date_hi=near_start_date_hi,
                aka_lo=aka_lo, aka_hi=aka_hi,
                log_step=log_step
            ),
            key_getter=person_key_from_dict,
            items_getter=lambda x: x['memberships'],
            updates_diff_logic=updates_diff_logic,
            removes_diff_logic=removes_diff_logic,
            transformation=transformation,
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename=filename,
            clear_cache=self.clear_cache,
            logger=self.logger,
            log_level=self.logger.level,
            end_date=datetime.strftime(datetime.now(), '%Y-%m-%d'),
            end_reason='record missing from source',
            source=original_source
        )()
        self.logger.info("End loading procedure")
