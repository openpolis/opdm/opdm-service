import itertools

from popolo.models import Organization
from taskmanager.management.base import LoggingBaseCommand
from django.db.models import CharField, Value as V, Count
from django.db.models.functions import Concat


class Command(LoggingBaseCommand):
    help = "Detect double memberships, for manual intervention, remove one of them if not in dry-run mode"
    dry_run = None

    def add_arguments(self, parser):  # noqa: D102
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="dry_run",
            help="Only show modifications do not save.",
        )

        parser.add_argument(
            "--context",
            dest="context",
            help="The organization classification to limit the lookup to",
        )

        parser.add_argument(
            "--base-url",
            dest="base_url",
            default="https://opdm.openpolis.io",
            help="The base URL of the site where data can be managed (OPDM)"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)
        context = options.get('context', None)
        dry_run = options['dry_run']
        opdm_base_url = options['base_url']

        self.logger.info("Start")

        orgs = Organization.objects.all()
        if context:
            orgs = orgs.filter(classification=context)

        # a unique slug (u) is build through concatenation of some fields, and
        # annotated into dhe resulting queryset
        org = None
        affected = []
        doubles = orgs.annotate(
            u=Concat(
                'memberships__person_id', V('-'),
                'memberships__label', V('-'),
                'memberships__start_date', V('-'),
                'memberships__end_date',
                output_field=CharField()
            )
        ).values(
            'id', 'memberships__person_id', 'memberships__label',
            'memberships__start_date', 'memberships__end_date',
            'u'
        ).annotate(n=Count('u')).filter(n__gt=1)
        n_doubles = doubles.count()
        self.logger.info(f"{n_doubles} doubles detected")
        for i, o in enumerate(doubles, start=1):
            if i % 100 == 0:
                self.logger.info(f"{i}/{n_doubles}")
            self.logger.debug(f"{o['id']} {o['u']}")

            # only read og from DB if not already there
            if org is None or org.id != o['id']:
                org = Organization.objects.get(id=o['id'])

            # extract the identical memberships
            ms = org.memberships.filter(
                person_id=o['memberships__person_id'],
                label=o['memberships__label'],
                start_date=o['memberships__start_date'],
                end_date=o['memberships__end_date']
            )

            if ms.count() < 2:
                continue

            m_dest = ms[0]
            m_src = ms[1]

            # merge or change dates for memberships tied to electoral events
            try:
                if m_src.electoral_event and m_dest.electoral_event:
                    if m_src.electoral_event == m_dest.electoral_event:
                        self.logger.info(f"removing {m_src} after merging {m_dest} into it")
                        if not dry_run:
                            # merge memberships details
                            self.merge_memberships(source=m_src, dest=m_dest)
                    else:
                        # swap dest and src, so that src refers to latest electoral event
                        if m_dest.electoral_event.start_date >= m_src.electoral_event.start_date:
                            m_dest, m_src = m_src, m_dest
                        next_electoral_event = m_src.this_and_next_electoral_events()[1]
                        if m_src.electoral_event and next_electoral_event:
                            p = m_src.person
                            self.logger.info(
                                f"Trying recreation of {m_src}, "
                                f"using event: {m_src.electoral_event} and according dates"
                            )
                            if not dry_run:
                                m = p.add_role(
                                    m_src.post,
                                    label=m_src.label,
                                    role=m_src.role,
                                    start_date=m_src.electoral_event.start_date,
                                    end_date=next_electoral_event.start_date,
                                    electoral_event=m_src.electoral_event,
                                )
                                if m:
                                    self.merge_memberships(source=m_src, dest=m)
                                    affected.append({'org': org, 'm': m})
                else:
                    self.logger.info(f"removing {m_src} after merging {m_dest} into it")
                    if not dry_run:
                        # merge memberships details and remove one of them
                        self.merge_memberships(source=m_src, dest=m_dest)

                if not dry_run:
                    m_src.delete()

            except Exception as e:
                self.logger.error(
                    f"{e} encountered during processing of {m_src} => {m_dest} - skipping"
                )

        grouped_affected = list(itertools.groupby(affected, lambda x: x['org']))

        self.logger.info(
            f"{len(affected)} memberships had their dates changed "
            f"in {len(grouped_affected)} organizations:"
        )
        grouped_affected = itertools.groupby(affected, lambda x: x['org'])
        for o, g in grouped_affected:
            self.logger.info(
                f"{o} - {opdm_base_url}/#/memberships?organization__id={o.id}&page_size=15&page=1&active_status="
            )
            for item in list(g):
                m = item['m']
                self.logger.info(
                    f"  {m.person} {m.label} dal {m.start_date} - {opdm_base_url}/#/memberships/edit/{m.id}"
                )

        self.logger.info("Finished")

    def merge_memberships(self, source, dest):
        for i in source.sources.values("source__url", "source__note"):
            try:
                r = dest.add_source(
                    url=i.get("source__url"), note=i.get("source__note", "")
                )
                if r:
                    self.logger.debug("  {0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))

        for i in source.links.values("link__url", "link__note"):
            try:
                r = dest.add_link(
                    url=i.get("link__url"), note=i.get("link__note", "")
                )
                if r:
                    self.logger.debug("  {0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))
