import logging

from django.core.management import BaseCommand
from django.db.models import Q
from popolo.models import Organization, Classification


class Command(BaseCommand):
    help = "Assign Consigli regionali to their correct parents"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start")

        # set FORMA_GIURIDICA_OP classification right for Consigli regionali misclassified
        crc = Classification.objects.get(
            scheme="FORMA_GIURIDICA_OP", descr="Consiglio regionale"
        )
        for o in Organization.objects.filter(
            name__icontains="consiglio regionale"
        ).exclude(
            Q(name__icontains="psicologi")
            | Q(name__icontains="giornalisti")
            | Q(classifications__classification__descr="Consiglio regionale")
        ):
            o.classification = "Consiglio regionale"
            c = o.classifications.filter(
                classification__scheme="FORMA_GIURIDICA_OP"
            ).first()
            if c:
                c.classification = crc
                c.save()
            else:
                o.classifications.get_or_create(classification=crc)
            o.save()

        parents_names = {
            "CONSIGLIO REGIONALE DELLA REGIONE AUTONOMA TRENTINO ALTO - ADIGE SUDTIROL":
                "REGIONE AUTONOMA TRENTINO ALTO ADIGE",
            "CONSIGLIO DELLA PROVINCIA AUTONOMA DI TRENTO": "PROVINCIA AUTONOMA DI TRENTO",
            "CONSIGLIO DELLA PROVINCIA AUTONOMA DI BOLZANO": "PROVINCIA AUTONOMA DI BOLZANO/AUTONOME PROVINZ BOZEN",
            "CONSIGLIO REGIONALE DELLA VALLE D'AOSTA": "REGIONE AUTONOMA VALLE D'AOSTA",
            "CONSIGLIO REGIONALE DEL FRIULI VENEZIA GIULIA": "REGIONE AUTONOMA FRIULI VENEZIA GIULIA",
            "ASSEMBLEA LEGISLATIVA - REGIONE UMBRIA": "REGIONE DELL'UMBRIA",
            "ASSEMBLEA LEGISLATIVA DELLE MARCHE": "REGIONE MARCHE",
            "ASSEMBLEA REGIONALE SICILIANA": "REGIONE SICILIANA",
            "CONSIGLIO REGIONALE DEL MOLISE": "REGIONE MOLISE",
            "CONSIGLIO REGIONALE DEL PIEMONTE": "REGIONE PIEMONTE",
            "CONSIGLIO REGIONALE DEL VENETO": "REGIONE DEL VENETO",
            "CONSIGLIO REGIONALE DELL' ABRUZZO": "REGIONE ABRUZZO",
            "CONSIGLIO REGIONALE DELLA BASILICATA": "REGIONE BASILICATA",
            "CONSIGLIO REGIONALE DELLA CALABRIA": "REGIONE CALABRIA",
            "CONSIGLIO REGIONALE DELLA CAMPANIA": "REGIONE CAMPANIA",
            "CONSIGLIO REGIONALE DELLA LIGURIA": "REGIONE LIGURIA",
            "CONSIGLIO REGIONALE DELLA LOMBARDIA": "REGIONE LOMBARDIA",
            "CONSIGLIO REGIONALE DELLA PUGLIA": "REGIONE PUGLIA",
            "CONSIGLIO REGIONALE DELLA SARDEGNA": "REGIONE AUTONOMA DELLA SARDEGNA",
        }

        for o in Organization.objects.filter(
            classification__iexact="Consiglio regionale"
        ):
            self.logger.debug(
                "{0} | {1} | {2} | {3}".format(
                    o.name,
                    o.parent,
                    o.parent.area if o.parent else "-",
                    o.memberships.count(),
                )
            )
            if o.parent is None:
                try:
                    parent = Organization.objects.get(name=parents_names[o.name])
                except Organization.DoesNotExist:
                    self.logger.warning(
                        "Could not find parent {0} for {1}".format(
                            parents_names[o.name], o.name
                        )
                    )
                except ValueError:
                    self.logger.warning(
                        "Could not find {0} in parent_names.".format(o.name)
                    )
                else:
                    o.parent = parent
                    self.logger.info("Parent {0} assigned to {1}".format(parent, o))
                    o.save()
        self.logger.info("End")
