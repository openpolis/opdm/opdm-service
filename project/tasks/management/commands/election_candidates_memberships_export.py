# -*- coding: utf-8 -*-
import csv
from datetime import datetime
import os
import re

from ooetl import ETL
from ooetl.loaders import CSVLoader
from ooetl.transformations import Transformation
import pandas as pd
from requests import HTTPError
from taskmanager.management.base import LoggingBaseCommand
import tortilla
from tortilla.utils import bunchify

from project.tasks.etl.extractors import JsonArrayExtractor


class ElectionCandidatesTransformation(Transformation):
    """Instance of ``ooetl.transformations.Transformation`` class that handles
    transforming election candidates fields
    """

    def transform(self):
        """ Transform data,
        renaming columns, filtering non-used columns,
        removing empty lines, building useful columns.

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original list into a pandas dataframe
        od = pd.DataFrame(
            self.etl.original_data.copy()
        )

        # select only useful columns, and sort them
        od = od.filter(
            items=[
                "nome_c",
                "cogn_c",
                "dt_nasc",
                "l_nasc",
                "altro_1",
                "altro_2",
                "desc_lista",
                "desc_ente",
                "num_c"
            ]
        )

        od.rename(
            columns={
                "altro_1": "aka_1",
                "altro_2": "aka_2",
                "nome_c": "given_name",
                "cogn_c": "family_name",
                "dt_nasc": "birth_date",
                "l_nasc": "birth_location",
                "desc_lista": "list",
                "desc_ente": "constituency",
                "num_c": "pos_in_list"
            },
            inplace=True,
        )

        od["given_name"] = od["given_name"].map(str.title)
        od["family_name"] = od["family_name"].map(str.title)

        if "birth_location" in list(od.columns):

            def split_birth_location(loc):
                if not loc:
                    return ""
                m = re.search(r"(.+) \((\w+)\)", loc)
                if m:
                    return "{0} ({1})".format(m.group(1).title(), m.group(2).upper())
                else:
                    return loc.title()

            od["birth_location"] = od["birth_location"].apply(split_birth_location)
        else:
            od["birth_location"] = None

        # dates conversion
        def convert_date(d):
            if d:
                return datetime.strptime(d, "%d/%m/%Y").strftime("%Y-%m-%d")
            else:
                return None
        od["birth_date"] = od["birth_date"].apply(
            lambda d: convert_date(d)
        )

        self.etl.processed_data = od

        # return ETL instance
        return self.etl


class Command(LoggingBaseCommand):
    help = "Extract candidates from source and find which of them have memebrships in OPDM"

    def add_arguments(self, parser):
        parser.add_argument(
            "--source-url",
            dest="source_url",
            help="The url of the resource containing the list of candidates",
        )
        parser.add_argument(
            "--scsv-file",
            dest="scsv_file",
            default="./data/elections/candidates_eu_2019.csv",
            help="Complete path of candidates (as CSV file)",
        )
        parser.add_argument(
            "--out-file",
            dest="out_file",
            default="./data/elections/candidates_eu_2019_memberships.csv",
            help="Complete path to the CSV file with the memberships"
        )
        parser.add_argument(
            "--sim-file",
            dest="sim_file",
            default="./data/elections/similarities.csv",
            help="Complete path to the CSV holding solved similarities map"
        )
        parser.add_argument(
            "--api-url",
            dest="api_url",
            default="http://op:op_pass@localhost:8001",
            help="Url to access opdm API, complete with username and password: "
            "ie: https://user:pass@service.opdm.openpolis.io"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        source_url = options['source_url']
        out_file = options['out_file']

        self.logger.info("Start procedure")

        # api authentication and connection
        api_url = options['api_url']
        m = re.match(r"https?://(.*?):(.*?)@(.*)", api_url)
        if len(m.groups()) == 3:
            username = m.groups()[0]
            password = m.groups()[1]
            api_url = api_url.replace("{0}:{1}@".format(username, password), "")
        else:
            raise Exception("API url does not contain username and password: {0}".format(api_url))

        api_auth = tortilla.wrap(api_url)
        try:
            resp = api_auth.post(
                "api-token-auth",
                data={"username": username, "password": password},
                suffix="/"
            )
            token = resp.token
        except HTTPError:
            raise Exception("Username and password are not correct")

        api = tortilla.wrap("{0}/v1".format(api_url.strip("/")))
        api.config.headers.token = token

        self.logger.info("Start procedure")

        # fetch data from source url if not existing as CSV file (scsv)
        scsv_complete_file = options['scsv_file']
        scsv_path, scsv_file = os.path.split(scsv_complete_file)
        scsv_filename, scsv_ext = os.path.splitext(scsv_file)
        if not os.path.exists(scsv_complete_file):
            ETL(
                extractor=JsonArrayExtractor(url=source_url, dpath="/candidati"),
                transformation=ElectionCandidatesTransformation(),
                loader=CSVLoader(scsv_path, label=scsv_filename, encoding="utf8", sep=",")
            )()

        # build solved similarities map out of the sim_file
        sim_complete_file = options['sim_file']
        sim_map = {}
        if os.path.exists(sim_complete_file):
            with open(sim_complete_file, mode='r') as sim_fp:
                sim_reader = csv.DictReader(sim_fp)
                for item in sim_reader:
                    key = (
                        item['family_name'], item['given_name'],
                        item['birth_date'], item['birth_location']
                    )
                    sim_map[key] = item['opdm_id']

        # read scsv file, search for persons in opdm, extract memberships
        with open(scsv_complete_file, mode='r') as scsv_fp:
            fieldnames = scsv_fp.readline().split(",")
            w_fieldnames = fieldnames + ['label', 'start_date', 'end_date']
            scsv_fp.seek(0)
            with open(out_file, mode='w') as out_fp:
                reader = csv.DictReader(scsv_fp, fieldnames=fieldnames)
                writer = csv.DictWriter(
                    out_fp, fieldnames=w_fieldnames
                )
                writer.writeheader()
                n = 1
                for item in reader:
                    w_item = item.copy()
                    key = (
                        item['family_name'], item['given_name'],
                        item['birth_date'], item['birth_location']
                    )
                    matching_id = sim_map.get(key, None)
                    if matching_id:
                        search_results = bunchify(
                            {
                                'count': 1,
                                'results': [
                                    {
                                        "family_name": item['family_name'],
                                        "given_name": item["given_name"],
                                        "birth_date": item.get("birth_date", ''),
                                        "birth_location": item.get("birth_location", ''),
                                        "score": 0.0,
                                        "id": matching_id,
                                    }
                                ]
                            }
                        )
                    else:
                        try:
                            search_results = api.persons.search.get(
                                params={
                                    "family_name": item['family_name'],
                                    "given_name": item["given_name"],
                                    "birth_date": item.get("birth_date", ''),
                                    "birth_location": item.get("birth_location", '')
                                }
                            )
                        except HTTPError:
                            search_results = bunchify({
                                'count': 0,
                                'results': []
                            })
                    n_results = search_results.count
                    self.logger.info("{0}: {1},{2},{3},{4} - {5}".format(
                        n,
                        item['given_name'], item['family_name'], item['birth_date'], item['birth_location'],
                        n_results
                    ))

                    if (
                        n_results == 1 and
                        (search_results.results[0].score == 0 or search_results.results[0].score >= 150)
                    ):
                        res = search_results.results[0]
                        icon = "=="
                        self.logger.info("{0}({1}),{2},{3},{4},{5} -> {6}".format(
                            icon, res.score,
                            res.given_name, res.family_name, res.birth_date, res.birth_location,
                            res.id
                        ))
                        person = api.persons.get(res.id)
                        for m in person.memberships:
                            w_item.update({
                                'label': m.label,
                                'start_date': m.start_date,
                                'end_date': m.end_date
                            })
                            writer.writerow(w_item)
                    elif n_results > 0:
                        icon = "~~"
                        for res in search_results.results:
                            self.logger.info("{0}({1}),{2},{3},{4},{5} -> {6}".format(
                                icon, res.score,
                                res.given_name, res.family_name, res.birth_date, res.birth_location,
                                res.id
                            ))
                    n += 1

        self.logger.info("End procedure")
