# -*- coding: utf-8 -*-
import numpy as np
from ooetl.extractors import CSVExtractor
from ooetl.transformations import Transformation
import pandas as pd
from popolo.models import Area, ContactDetail, AreaRelationship
from slugify import slugify
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import CSVDiffCompositeETL
from project.tasks.etl.loaders.organizations import (
    PopoloOrgLoader,
    descr_stato_partecipazione,
)
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class Bdap2PopoloTransformation(Transformation):
    """Instance of ``ooetl.ETL`` class that handles
    importing locations data from BDAP into Django-Popolo model
    """

    def transform(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns.

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original dataframe
        od = self.etl.original_data.copy()

        # columns renaming (uppercase)
        od = od.rename(columns={col: col.upper() for col in list(od.columns)})

        # select only useful columns
        od = od.filter(
            items=[
                "ID_ENTE",
                "DENOMINAZIONE",
                "CF",
                "PIVA",
                "DATA_ISTITUZIONE",
                "DATA_CESSAZIONE",
                "CODICE_ATECO",
                "DESCR_CODICE_ATECO",
                "CODICE_FORMA_GIURIDICA",
                "DESCR_FORMA_GIURIDICA",
                "CODICE_CATEGORIA_IPA",
                "DESCR_CATEGORIA_IPA",
                "CODICE_TIPOLOGIA_IPA",
                "DESCR_TIPOLOGIA_IPA",
                "CODICE_TIPOLOGIA_SIOPE",
                "DESCR_TIPOLOGIA_SIOPE",
                "CODICE_TIPOLOGIA_MIUR",
                "DESCR_TIPOLOGIA_MIUR",
                "CODICE_TIPOLOGIA_DT",
                "DESCR_TIPOLOGIA_DT",
                "CODICE_TIPOLOGIA_ISTAT_S13",
                "DESCR_TIPOLOGIA_ISTAT_S13",
                "CODICE_TIPOLOGIA_DLGS_118_2011",
                "DESCR_TIPOLOGIA_DLGS_118_2011",
                "TELEFONO",
                "FAX",
                "URL",
                "INDIRIZZO",
                "CAP",
                "CODICE_CATASTALE",
                "SIGLA_PROVINCIA",
                "STATO_PARTECIPAZIONE",
                "CODICE_ENTE_SIOPE",
                "CODICE_ENTE_IPA",
                "CODICE_ENTE_MIUR",
                "CODICE_ENTE_SSN",
                "CODICE_ENTE_ISTAT_S13",
            ]
        )

        # rename ID_ENTE into CODICE_ENTE_BDAP
        od = od.rename(columns={"ID_ENTE": "CODICE_ENTE_BDAP"})

        # remove empty rows
        od = od.dropna(
            how="any",
            subset=["CF", "CODICE_ENTE_BDAP", "DENOMINAZIONE", "DATA_ISTITUZIONE"],
        )

        # do some more filtering here (test and debug)
        # od = od[
        #   (od.DESCR_FORMA_GIURIDICA == 'Regione') |
        #   (od.DESCR_TIPOLOGIA_SIOPE == 'CONSIGLI REGIONALI')
        # ]

        def get_slug(r):
            return slugify(
                r["DENOMINAZIONE"] + " " + r["CF"] + " " + r["DATA_ISTITUZIONE"]
            )

        #
        # Implements rules described at
        # https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/vocabolari/org-classificatione-forma-giuridica-op
        # to populate the FORMA_GIURIDICA_OP classification
        #
        od["CODICE_FORMA_GIURIDICA_OP"] = od["CODICE_FORMA_GIURIDICA"]
        od["DESCR_FORMA_GIURIDICA_OP"] = od["DESCR_FORMA_GIURIDICA"]

        od.loc[
            (
                (od.CODICE_FORMA_GIURIDICA_OP.isnull())
                & (od.CODICE_TIPOLOGIA_MIUR.notnull())
            ),
            "CODICE_FORMA_GIURIDICA_OP",
        ] = "2610"
        od.loc[
            (
                (od.DESCR_FORMA_GIURIDICA_OP.isnull())
                & (od.CODICE_TIPOLOGIA_MIUR.notnull())
            ),
            "DESCR_FORMA_GIURIDICA_OP",
        ] = "Istituto e scuola pubblica di ogni ordine e grado"

        od.loc[
            (
                (od.CODICE_FORMA_GIURIDICA_OP.isnull())
                & (od.DENOMINAZIONE.str.contains("s.r.l.|srl|S.R.L.|SRL"))
            ),
            "CODICE_FORMA_GIURIDICA_OP",
        ] = "1320"
        od.loc[
            (
                (od.DESCR_FORMA_GIURIDICA_OP.isnull())
                & (od.DENOMINAZIONE.str.contains("s.r.l.|srl|S.R.L.|SRL"))
            ),
            "DESCR_FORMA_GIURIDICA_OP",
        ] = "Società a responsabilità limitata"

        od.loc[
            (
                (od.CODICE_FORMA_GIURIDICA_OP.isnull())
                & (od.DENOMINAZIONE.str.contains("s.p.a.|spa|S.P.A.|SPA"))
            ),
            "CODICE_FORMA_GIURIDICA_OP",
        ] = "1310"
        od.loc[
            (
                (od.DESCR_FORMA_GIURIDICA_OP.isnull())
                & (od.DENOMINAZIONE.str.contains("s.p.a.|spa|S.P.A.|SPA"))
            ),
            "DESCR_FORMA_GIURIDICA_OP",
        ] = "Società per azioni"

        for map_item in self.etl.forma_giuridica_op_df.to_dict("records"):
            if map_item["altre_condizioni"] is np.nan:
                od.loc[
                    (
                        (od.CODICE_FORMA_GIURIDICA_OP.isnull())
                        & (
                            od[map_item["codelist_riferimento"]]
                            == map_item["codice_riferimento"]
                        )
                    ),
                    "CODICE_FORMA_GIURIDICA_OP",
                ] = map_item["codice_forma_giuridica_op"]
                od.loc[
                    (
                        (od.DESCR_FORMA_GIURIDICA_OP.isnull())
                        & (
                            od[map_item["codelist_riferimento"]]
                            == map_item["codice_riferimento"]
                        )
                    ),
                    "DESCR_FORMA_GIURIDICA_OP",
                ] = map_item["descr_forma_giuridica_op"]
            else:
                field, value = map_item["altre_condizioni"].split(",")
                od.loc[
                    (
                        (od.CODICE_FORMA_GIURIDICA_OP.isnull())
                        & (
                            od[map_item["codelist_riferimento"]]
                            == map_item["codice_riferimento"]
                        )
                        & (od[field].str.contains(value.upper()))
                    ),
                    "CODICE_FORMA_GIURIDICA_OP",
                ] = map_item["codice_forma_giuridica_op"]
                od.loc[
                    (
                        (od.DESCR_FORMA_GIURIDICA_OP.isnull())
                        & (
                            od[map_item["codelist_riferimento"]]
                            == map_item["codice_riferimento"]
                        )
                        & (od[field].str.contains(value.upper()))
                    ),
                    "DESCR_FORMA_GIURIDICA_OP",
                ] = map_item["descr_forma_giuridica_op"]

        od["SLUG"] = od.apply(lambda r: get_slug(r), axis=1)

        od["DENOMINAZIONE"] = od["DENOMINAZIONE"].apply(
            lambda x: x.replace("E'", "È")
            .replace("A'", "À")
            .replace("O'", "Ò")
            .replace("I'", "Ì")
            .replace("U'", "Ù")
            .replace("BASTIDA DÈ DOSSI", "BASTIDA DE' DOSSI")
            .replace("PIAN DI SCO", "PIAN DI SCÒ")
            .replace("SANT OMOBONO TERME", "SANT'OMOBONO TERME")
        )

        od["DATA_ISTITUZIONE"] = od["DATA_ISTITUZIONE"].apply(
            lambda x: np.nan if x == "1900-01-01" else x
        )

        def transform_item(item):
            """Enrich each item with identifiers, contacts, classifications, area, sources, links, ...

            :param item:
            :return:
            """
            processed_item = {
                "name": item["DENOMINAZIONE"],
                "identifier": item["CF"],
                "founding_date": item["DATA_ISTITUZIONE"],
                "dissolution_date": item["DATA_CESSAZIONE"],
            }
            # add alternate identifiers
            identifiers = []
            for id_key in [
                "CF",
                "PIVA",
                "CODICE_ENTE_BDAP",
                "CODICE_ENTE_SIOPE",
                "CODICE_ENTE_IPA",
                "CODICE_ENTE_MIUR",
                "CODICE_ENTE_SSN",
                "CODICE_ENTE_ISTAT_S13",
            ]:
                if item[id_key] is not None:
                    identifiers.append({"identifier": item[id_key], "scheme": id_key})
            processed_item["identifiers"] = identifiers

            # get area for institutions bound to areas (only comuni)
            area = None
            if item["DATA_ISTITUZIONE"]:
                sd = str(int(item["DATA_ISTITUZIONE"].split("-")[0]) + 1) + "-01-01"
            else:
                sd = "1800-01-01"
            try:
                area = (
                    Area.objects.current(sd)
                    .comuni()
                    .get(identifier=item["CODICE_CATASTALE"])
                )
            except Area.DoesNotExist:
                if "comune di " in item["DENOMINAZIONE"].lower():
                    name = item["DENOMINAZIONE"].replace("COMUNE DI ", "").lower()
                    try:
                        area = (
                            Area.objects.current(sd)
                            .comuni()
                            .get(
                                name__iexact=name,
                                parent__identifier=item["SIGLA_PROVINCIA"],
                            )
                        )
                    except Area.DoesNotExist:
                        self.logger.error(
                            "Could not bind comune to an existing area. No area found with name {0}".format(
                                name
                            )
                        )
                    except Area.MultipleObjectsReturned:
                        self.logger.error(
                            "Could not bind comune to an existing area. "
                            "Multiple areas found with name {0}".format(name)
                        )

            # add contact details
            contact_details = []
            for (c_key, c_label, c_type) in [
                (
                    "TELEFONO",
                    "Telefono della sede legale",
                    ContactDetail.CONTACT_TYPES.phone,
                ),
                ("FAX", "Fax della sede legale", ContactDetail.CONTACT_TYPES.fax),
                ("URL", "Indirizzo del sito web", ContactDetail.CONTACT_TYPES.url),
                (
                    "INDIRIZZO",
                    "Indirizzo della sede legale",
                    ContactDetail.CONTACT_TYPES.mail,
                ),
            ]:
                if item[c_key] is not None:
                    v = item[c_key]
                    # handles CAP for sede legale address
                    if c_key == "INDIRIZZO" and item["CAP"] is not None:
                        v += " - {0}".format(item["CAP"])

                    # handles city name for sede legale address
                    if c_key == "INDIRIZZO" and area is not None:
                        v += " - {0}".format(area.name)

                    contact_details.append(
                        {"label": c_label, "contact_type": c_type, "value": v}
                    )
            processed_item["contact_details"] = contact_details

            # classifications
            classifications = []
            classifications_schemes = [
                ("CODICE_ATECO", "ATECO_BDAP"),
                ("CODICE_FORMA_GIURIDICA", "FORMA_GIURIDICA_ISTAT_BDAP"),
                ("CODICE_FORMA_GIURIDICA_OP", "FORMA_GIURIDICA_OP"),
                ("CODICE_CATEGORIA_IPA", "CATEGORIA_IPA_BDAP"),
                ("CODICE_TIPOLOGIA_IPA", "TIPOLOGIA_IPA_BDAP"),
                ("CODICE_TIPOLOGIA_SIOPE", "TIPOLOGIA_SIOPE_BDAP"),
                ("CODICE_TIPOLOGIA_MIUR", "TIPOLOGIA_MIUR_BDAP"),
                ("CODICE_TIPOLOGIA_DT", "TIPOLOGIA_DT_BDAP"),
                ("CODICE_TIPOLOGIA_ISTAT_S13", "TIPOLOGIA_ISTAT_S13_BDAP"),
                ("CODICE_TIPOLOGIA_DLGS_118_2011", "TIPOLOGIA_DLGS_118_2011_BDAP"),
                ("STATO_PARTECIPAZIONE", "STATO_PARTECIPAZIONE_BDAP"),
            ]
            for (csv_field, scheme) in classifications_schemes:
                if item[csv_field] is not None:
                    code = item[csv_field]
                    if csv_field == "CODICE_ATECO":
                        descr = item["DESCR_CODICE_ATECO"]
                    elif csv_field == "STATO_PARTECIPAZIONE":
                        descr = descr_stato_partecipazione[code]
                    else:
                        descr = item[csv_field.replace("CODICE", "DESCR")]

                    if descr is not None:
                        classifications.append(
                            {"scheme": scheme, "code": code, "descr": descr}
                        )

            # add default OPDM context
            classifications.append({"scheme": "CONTESTO_OP", "descr": "OPDM"})

            processed_item["classifications"] = classifications

            if item["DESCR_FORMA_GIURIDICA_OP"] is not None:
                processed_item["classification"] = item["DESCR_FORMA_GIURIDICA_OP"]

            # area foreign key for organizations bound to areas
            if area:
                if processed_item["founding_date"]:
                    try:
                        area_parent = area.from_relationships.get(
                            classification="FIP",
                            start_date__lt=processed_item["founding_date"],
                            end_date__gt=processed_item["founding_date"],
                        ).dest_area
                    except AreaRelationship.DoesNotExist:
                        area_parent = area.parent

                    try:
                        area_grandparent = area_parent.from_relationships.get(
                            classification="FIP",
                            start_date__lt=processed_item["founding_date"],
                            end_date__gt=processed_item["founding_date"],
                        ).dest_area
                    except AreaRelationship.DoesNotExist:
                        area_grandparent = area_parent.parent
                else:
                    try:
                        area_parent = area.from_relationships.get(
                            classification="FIP", start_date__isnull=True
                        ).dest_area
                    except AreaRelationship.DoesNotExist:
                        area_parent = area.parent

                    try:
                        area_grandparent = area_parent.from_relationships.get(
                            classification="FIP", start_date__isnull=True
                        ).dest_area
                    except AreaRelationship.DoesNotExist:
                        area_grandparent = area_parent.parent

                if item["DESCR_FORMA_GIURIDICA_OP"] == "Regione":
                    area = area_grandparent
                elif item["DESCR_FORMA_GIURIDICA_OP"] == "Provincia":
                    area = area_parent
                elif item["DESCR_FORMA_GIURIDICA_OP"] == "Città metropolitana":
                    area = area_parent

            if area:
                processed_item["area_id"] = area.id
            else:
                processed_item["area_id"] = None

            processed_item["sources"] = [
                {"url": self.etl.source, "note": "BDAP open data URI"}
            ]
            return processed_item

        # convert nulls into None
        od = od.where((pd.notnull(od)), None)

        self.etl.logger.info("Starting transform_item")

        # define transformation for each item as a generator
        # to be passed along to the loader, without resolution
        od = (transform_item(item) for item in [i[1] for i in od.iterrows()])

        self.etl.logger.info("transform_item finished")

        # store processed data into the ETL instance
        self.etl.processed_data = od

        # return ETL instance
        return self.etl


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Import Organizations from BDAP lists of active entities"

    source_url = "https://bdap-opendata.mef.gov.it/export/csv/Anagrafe-Enti---Ente.csv"
    filename = "orgs_from_bdap.csv"
    encoding = "latin1"
    sep = ";"
    unique_idx_cols = (0,)

    forma_giuridica_op_source_csv_url = (
        "https://docs.google.com/spreadsheets/u/2/d/"
        "1zmLhqJ7R2BdE5f3OlWye0dgbf0UGDRLzsypPnIr_7Ns/export?format=csv&"
        "id=1zmLhqJ7R2BdE5f3OlWye0dgbf0UGDRLzsypPnIr_7Ns&gid=0"
    )
    forma_giuridica_op_local_csv_url = (
        "file:///home/gu/Workspace/opdm-service/"
        "resources/data/forma_giuridica_op.csv"
    )
    forma_giuridica_op_csv_url = forma_giuridica_op_source_csv_url
    forma_giuridica_op_df = CSVExtractor(
        forma_giuridica_op_csv_url,
        sep=",",
        encoding="utf8",
        na_values=["", "-"],
        keep_default_na=False,
    ).extract()

    def add_arguments(self, parser):
        parser.add_argument(
            "--update-strategy",
            dest="update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to keep_old",
        )
        parser.add_argument(
            "--lookup-strategy",
            dest="lookup_strategy",
            default="mixed",
            help="Whether to lookup using anagraphical (name and founding date), "
            "identifier (see --identifier-scheme) or mixed (identifier first, then cascade to anagraphical)",
        )
        parser.add_argument(
            "--identifier-scheme",
            dest="identifier_scheme",
            default="CODICE_ENTE_BDAP",
            help="Which scheme to use with identifier/mixed lookup strategy",
        )
        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="verbose", **options)
        self.handle_cache(*args, **options)

        self.logger.info("Start records import")
        self.logger.info("Reading CSV from url: {0}".format(self.source_url))

        update_strategy = options["update_strategy"]
        lookup_strategy = options["lookup_strategy"]
        identifier_scheme = options["identifier_scheme"]

        CSVDiffCompositeETL(
            extractor=CSVExtractor(
                source=self.source_url,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=Bdap2PopoloTransformation(),
            loader=PopoloOrgLoader(
                update_strategy=update_strategy,
                lookup_strategy=lookup_strategy,
                identifier_scheme=identifier_scheme,
                log_step=100
            ),
            extended_attributes={"forma_giuridica_op_df": self.forma_giuridica_op_df},
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            clear_cache=self.clear_cache,
            filename=self.filename,
            unique_idx_cols=(0,),
            log_level=self.logger.level,
            logger=self.logger
        )()
