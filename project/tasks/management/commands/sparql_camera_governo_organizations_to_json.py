import re
from typing import Dict, List, Optional

from popolo.models import Classification

from project.tasks.parsing.common import parse_date
from tasks.management.base import SparqlToJsonCommand
from tasks.parsing.sparql.utils import get_bindings, read_raw_rq


class Command(SparqlToJsonCommand):
    json_filename = "camera_governo_organizations"

    def query(self, **options) -> Optional[List[Dict]]:
        q = re.sub(
            r"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_\d+>",
            f"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_{options['legislature']:02d}>",
            read_raw_rq("camera_governo_organizations_17.rq"),
        )
        return get_bindings("http://dati.camera.it/sparql", q, method="POST")

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        classification_descr = "Governo della Repubblica"
        classification_forma_giuridica_op_id = (
            Classification.objects.filter(
                scheme="FORMA_GIURIDICA_OP", descr="Governo della Repubblica"
            )
            .get()
            .id
        )

        organizations_buf = []

        for binding in bindings:
            name = binding["nomeGoverno"].value.split("(")[0].strip()
            founding_date = parse_date(binding["date"].value.split("-")[0])
            try:
                dissolution_date = parse_date(binding["date"].value.split("-")[1])
            except IndexError:
                dissolution_date = None
            organizations_buf.append({
                "name": name,
                "classification": classification_descr,
                "founding_date": founding_date,
                "dissolution_date": dissolution_date,
                "identifier": binding["governoURL"].value,
                "identifiers": [
                    {"scheme": "OCD-URI", "identifier": binding["governoURL"].value}
                ],
                "classifications": [
                    {"classification": classification_forma_giuridica_op_id}
                ],
                "sources": [
                    {
                        "note": "Open Data Camera",
                        "url": "http://dati.camera.it/",
                    }
                ],
            })

        return organizations_buf
