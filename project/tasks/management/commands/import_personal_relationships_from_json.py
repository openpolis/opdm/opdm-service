import logging

from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import JsonDiffCompositeETL
from project.tasks.etl.composites.diff_logics import JsonOutRemovesDiffLogic
from project.tasks.etl.extractors import JsonArrayExtractor
from project.tasks.etl.loaders.persons import PopoloPersonalRelationshipLoader
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Import Personal relationships from a JSON source"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_uri",
            help="Source of the JSON file (http[s]:// or local path ./...)",
        )
        parser.add_argument(
            dest="original_source",
            help="Original source of the data, to add to sources attributes (ex: http://api.atoka.io)",
        )
        parser.add_argument(
            "--update-strategy",
            dest="update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to keep_old",
        )
        parser.add_argument(
            "--log-step",
            dest="log_step",
            type=int,
            default=500,
            help="Number of steps to log process completion to stdout. Defaults to 500.",
        )

        super().add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        super().handle(__name__, *args, formatter_key="simple", **options)
        self.handle_cache(*args, **options)

        self.logger.info("Start loading procedure")

        update_strategy = options["update_strategy"]
        source_uri = options["source_uri"]
        original_source = options["original_source"]
        log_step = options["log_step"]
        filename = source_uri[source_uri.rfind("/") + 1:]

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(source_uri),
            loader=PopoloPersonalRelationshipLoader(
                update_strategy=update_strategy,
                log_step=log_step,
            ),
            source=original_source,
            key_getter=lambda x: x["person1_id"],
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename=filename,
            clear_cache=self.clear_cache,
            logger=self.logger,
            log_level=self.logger.level,
            removes_diff_logic=JsonOutRemovesDiffLogic(
                local_out_path=self.local_out_path
            ),
        )()
        self.logger.info("End loading procedure")
