# coding=utf-8
import itertools
import datetime as dt

from django.db.models import F
from popolo.models import Organization
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """
    Loop over all municipalities or regions and show those with overlapping unique roles:
    - presidenti, sindaci, presidenti del consiglio
    """

    help = "Check overlapping unique roles"
    epoch = dt.datetime(1970, 1, 1)

    def add_arguments(self, parser):
        parser.add_argument(
            "--context",
            dest="context",
            help="The context to check [com|prov|reg].",
        )

        parser.add_argument(
            "--max-ab",
            dest="max_ab", type=int,
            help="The maximum inhabitants limit.",
        )

        parser.add_argument(
            "--min-ab",
            dest="min_ab", type=int,
            help="The minimum inhabitants limit.",
        )

    @classmethod
    def check_overlapping_roles_in_org(cls, org, classification, role):
        """

        :param org:
        :param classification:
        :param role:
        :return:
        """
        roles = []
        org = org.children.get(classification=classification)
        ms = org.memberships.filter(
            end_date__isnull=False,
            role=role
        )
        _l = list(itertools.chain(*ms.values_list('start_date', 'end_date').order_by('start_date')))
        if sorted(_l) != _l:
            overlapping_roles = list(
                ms.values('person__name', 'label', 'start_date', 'end_date').order_by('start_date')
            )
            for k in range(0, len(overlapping_roles)-1):
                role = overlapping_roles[k]
                role_next = overlapping_roles[k+1]
                if role_next['start_date'] < role['end_date']:
                    roles.append((role, role_next))

        return roles

    @classmethod
    def check_incompatible_roles(cls, org, classification, roles):
        """Check overlapping roles for single person with roles as
        Presidente del consiglio and Consigliere or
        Vicesindaco and Assessore

        :param org:
        :param classification:
        :param roles:
        :return:
        """
        _roles = []
        org = org.children.get(classification=classification)

        ms = org.memberships.filter(
            end_date__isnull=False,
            role__in=roles
        )
        for k, g in itertools.groupby(
            ms.values_list(
                'person_id', 'role', 'start_date', 'end_date'
            ).order_by('person_id', 'start_date'),
            lambda x: x[0]
        ):
            g1 = list(g)
            if roles[0] not in {i[1] for i in g1}:
                continue
            _l = list(itertools.chain(*[[i[2], i[3]] for i in g1]))
            if _l != sorted(_l):
                overlapping_roles = list(
                    ms.filter(person_id=k).values(
                        'person__name', 'label', 'start_date', 'end_date'
                    ).order_by('start_date')
                )
                for i in range(0, len(overlapping_roles)-1):
                    role = overlapping_roles[i]
                    role_next = overlapping_roles[i+1]
                    if role_next['start_date'] < role['end_date']:
                        _roles.append((role, role_next))

        return _roles

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        context = options["context"]
        if context.lower() not in ["com", "reg", "prov"]:
            raise Exception("Need to use a context among 'com', 'prov', 'reg'")
        if context.lower() == 'reg':
            orgs = Organization.objects.regioni()
            cons_classification = "Consiglio regionale"
            cons_role = "Presidente di consiglio regionale"
            cons_incompatible_roles = [cons_role, "Consigliere regionale"]
            cons_incompatible_roles_vice = [f"Vice{cons_role.lower()}", "Consigliere regionale"]
            giunta_classification = "Giunta regionale"
            giunta_role = "Presidente di Regione"
            giunta_incompatible_roles = ["Vicepresidente di Regione", "Assessore regionale"]
        elif context.lower() == 'prov':
            orgs = Organization.objects.province()
            cons_classification = "Consiglio provinciale"
            cons_role = "Presidente di consiglio provinciale"
            cons_incompatible_roles = [cons_role, "Consigliere provinciale"]
            cons_incompatible_roles_vice = [f"Vice{cons_role.lower()}", "Consigliere provinciale"]
            giunta_classification = "Giunta provinciale"
            giunta_role = "Presidente di Provincia"
            giunta_incompatible_roles = ["Vicepresidente di Provincia", "Assessore provinciale"]
        else:  # com is the only possibility left at this point
            orgs = Organization.objects.comuni()
            cons_classification = "Consiglio comunale"
            cons_role = "Presidente di consiglio comunale"
            cons_incompatible_roles = [cons_role, "Consigliere comunale"]
            cons_incompatible_roles_vice = [f"Vice{cons_role.lower()}", "Consigliere comunale"]
            giunta_classification = "Giunta comunale"
            giunta_role = "Sindaco"
            giunta_incompatible_roles = ["Vicesindaco", "Assessore comunale"]

        min_ab = options.get('min_ab', None)
        max_ab = options.get('max_ab', None)

        self.logger.info("Start of process")

        if min_ab:
            orgs = orgs.filter(area__inhabitants__gte=min_ab, area__inhabitants__isnull=False)

        if max_ab:
            orgs = orgs.filter(area__inhabitants__lte=max_ab, area__inhabitants__isnull=False)

        orgs = orgs.order_by(F('area__inhabitants').desc(nulls_last=True))

        n_orgs = orgs.count()
        self.logger.info(f"Processing {n_orgs} organizations")
        for k, org in enumerate(orgs, start=1):

            # overlap presidenti consiglio
            cons_ov = self.check_overlapping_roles_in_org(org, cons_classification, cons_role)

            # overlap apicali giunta
            giunta_ov = self.check_overlapping_roles_in_org(org, giunta_classification, giunta_role)

            # incompatibilità consiglio
            cons_inc = self.check_incompatible_roles(org, cons_classification, cons_incompatible_roles)
            cons_inc_vice = self.check_incompatible_roles(org, cons_classification, cons_incompatible_roles_vice)

            # incompatibilità giunta
            giunta_inc = self.check_incompatible_roles(org, giunta_classification, giunta_incompatible_roles)

            # loop su tutti i casi: mostra etichetta comune di X solo se ci sono segnalazioni
            _types = {
                'cons_ov': {'label': "Consiglio", 'value': cons_ov},
                'giunta_ov': {'label': "Giunta", 'value': giunta_ov},
                'cons_inc': {'label': "Presidente del consiglio/Consigliere", 'value': cons_inc},
                'cons_inc_vice': {'label': "Vicepresidente del consiglio/Consigliere", 'value': cons_inc_vice},
                'giunta_inc': {'label': "Vicepresidente - Vicesindaco/Assessore", 'value': giunta_inc},
            }
            if any([i['value'] for i in _types.values()]):
                self.logger.info(f"{k} - {org}")

                for _, v in _types.items():
                    if v['value']:
                        self.logger.info(v['label'])
                        for r in v['value']:
                            self.logger.info(f"  {r[0]['person__name']} {r[0]['label']}")
                            self.logger.info(f"    dal {r[0]['start_date']} al {r[0]['end_date']}")
                            self.logger.info(f"    dal {r[1]['start_date']} al {r[1]['end_date']}")

        self.logger.info("End of process")
