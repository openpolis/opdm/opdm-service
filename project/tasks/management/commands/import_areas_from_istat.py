# -*- coding: utf-8 -*-
import ssl

from ooetl.extractors import CSVExtractor
from ooetl.transformations import Transformation
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import CSVDiffCompositeETL
from project.tasks.etl.loaders.areas import PopoloAreaLoader
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class Istat2PopoloTransformation(Transformation):
    """Instance of ``ooetl.transformations.Transformation`` class that handles
    transforming areas data from ISTAT into Django-Popolo model
    """

    def transform(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns.

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original dataframe
        od = self.etl.original_data.copy()

        # rename columns in dataframe
        od.rename(
            columns={
                "Codice Regione": "code_reg",
                "Codice dell'Unità territoriale sovracomunale (valida a fini statistici)": "code_cm",
                "Codice Provincia (Storico)(1)": "code_prov",
                "Progressivo del Comune (2)": "code_com_prog",
                "Codice Comune formato alfanumerico": "code_com",
                "Denominazione (Italiana e straniera)": "den_full",
                "Denominazione in italiano": "den_it",
                "Denominazione altra lingua": "den_xx",
                "Codice Ripartizione Geografica": "code_rip",
                "Ripartizione geografica": "den_rip",
                "Denominazione Regione": "den_reg",
                "Denominazione dell'Unità territoriale sovracomunale (valida a fini statistici)": "den_prov",
                "Flag Comune capoluogo di provincia/città metropolitana/libero consorzio": "is_cap",
                "Sigla automobilistica": "sigla_auto",
                "Codice Comune formato numerico": "code_com_num",
                "Codice Comune numerico con 110 province (dal 2010 al 2016)": "code_com_110",
                "Codice Comune numerico con 107 province (dal 2006 al 2009)": "code_com_107",
                "Codice Comune numerico con 103 province (dal 1995 al 2005)": "code_com_103",
                "Codice Catastale del comune": "code_catasto",
                "Popolazione legale 2011 (09/10/2011)": "pop_2011",
                # "Codice NUTS1 2010": "code_nuts1_2010",
                # "Codice NUTS2 2010 (3)": "code_nuts2_2010",
                # "Codice NUTS3 2010": "code_nuts3_2010",
                "Codice NUTS1 2021": "code_nuts1_2021",
                "Codice NUTS2 2021 (3)": "code_nuts2_2021",
                "Codice NUTS3 2021": "code_nuts3_2021",
                "Codice NUTS1 2024": "code_nuts1_2024",
                "Codice NUTS2 2024 (3)": "code_nuts2_2024",
                "Codice NUTS3 2024": "code_nuts3_2024",
            },
            inplace=True,
        )

        # select only useful columns
        od = od.filter(
            items=[
                "code_rip",
                "code_reg",
                "code_cm",
                "code_prov",
                "code_com",
                "den_rip",
                "den_reg",
                "den_cm",
                "den_prov",
                "den_full",
                "den_it",
                "den_xx",
                "is_cap",
                "sigla_auto",
                "code_catasto",
                "code_com_110",
                "code_com_107",
                "code_com_103",
                # "code_nuts1_2010",
                # "code_nuts2_2010",
                # "code_nuts3_2010",
                "code_nuts1_2021",
                "code_nuts2_2021",
                "code_nuts3_2021",
                "code_nuts1_2024",
                "code_nuts2_2024",
                "code_nuts3_2024",
            ]
        )

        # remove empty rows
        od = od.dropna(how="any", subset=["code_catasto", "den_it"])

        #
        # superseeded: den_full is already in the data
        #
        # # build full denominations out of den_it and den_de
        # def get_full_den(r):
        #     if r["den_xx"] is not np.nan:
        #         return r["den_it"] + "/" + r["den_xx"]
        #     else:
        #         return r["den_it"]
        #
        # od["den_full"] = od.apply(lambda r: get_full_den(r), axis=1)

        # population is converted into integer
        # population column was removed from the source
        # od["pop_2011"] = od["pop_2011"].apply(lambda x: x.replace(".", ""))

        # convert numerical code_com into alphanumeric, 0-padded string
        code_com_columns = ["code_com_110", "code_com_107", "code_com_103"]
        for col in code_com_columns:
            od[col] = od[col].apply(lambda x: format(int(x), "06d"))

        # assign SU to nan sigla_auto columns
        od.loc[od.sigla_auto.isnull(), "sigla_auto"] = "SU"

        # convert inhabitants field to integer
        # od["pop_2011"].astype(int)

        # od = od[
        #     (od.den_it == 'Benna')
        #     (od.den_it == 'Arborio') |
        #     (od.den_it == 'Lanusei') |
        #     (od.den_it == 'Cardedu') |
        #     (od.den_it == 'Padru') |
        #     (od.den_it == 'Montescudo-Monte Colombo') |
        #     (od.den_it == 'Bertinoro') |
        #     (od.den_it == 'Telti') |
        #     (od.den_it == 'Teltì')
        # ]
        # od = od[
        #     (od.den_reg == 'Sardegna')
        # ]
        # store processed data into the ETL instance
        self.etl.processed_data = od

        # return ETL instance
        return self


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    """
    usage: manage.py import_areas_from_istat [-h] [--version] [-v {0,1,2,3}]
                                             [--settings SETTINGS]
                                             [--pythonpath PYTHONPATH]
                                             [--traceback] [--no-color]
                                             [--clear-cache]
                                             [--local-cache-path LOCAL_CACHE_PATH]
                                             [--local-out-path LOCAL_OUT_PATH]
                                             [--disable-notifications]

    Import Areas from ISTAT list of italian comuni

    optional arguments:
      -h, --help            show this help message and exit
      --version             show program's version number and exit
      -v {0,1,2,3}, --verbosity {0,1,2,3}
                            Verbosity level; 0=minimal output, 1=normal output,
                            2=verbose output, 3=very verbose output
      --settings SETTINGS   The Python path to a settings module, e.g.
                            "myproject.settings.main". If this isn't provided, the
                            DJANGO_SETTINGS_MODULE environment variable will be
                            used.
      --pythonpath PYTHONPATH
                            A directory to add to the Python path, e.g.
                            "/home/djangoprojects/myproject".
      --traceback           Raise on CommandError exceptions
      --no-color            Don't colorize the command output.
      --clear-cache         Clear cache if found
      --local-cache-path LOCAL_CACHE_PATH
                            Where downloaded file should be stored for caching and
                            diffing
      --local-out-path LOCAL_OUT_PATH
                            Where produced file should be stored for successive
                            manual processing
      --disable-notifications
    """
    help = "Import Areas from ISTAT list of italian comuni"

    csv_url = (
        "https://www.istat.it/storage/"
        "codici-unita-amministrative/Elenco-comuni-italiani.csv"
    )
    # csv_url = (
    #     "file:///Users/gu/Downloads"
    #     "/Elenco-comuni-italiani.csv"
    # )
    filename = "areas_from_istat.csv"
    encoding = "latin1"
    sep = ";"
    unique_idx_cols = (18,)

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        # execute handle in mixin and parent,

        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.handle_cache(*args, **options)

        self.logger.info("Cleaning up some data")

        self.logger.info("Starting composite ETL process")

        # bad hack to avoid SSL verification errors
        ssl._create_default_https_context = ssl._create_unverified_context

        CSVDiffCompositeETL(
            extractor=CSVExtractor(
                self.csv_url,
                sep=";",
                encoding="latin1",
                na_values=["", "-"],
                keep_default_na=False,
            ),
            transformation=Istat2PopoloTransformation(),
            loader=PopoloAreaLoader(),
            clear_cache=self.clear_cache,
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename=self.filename,
            unique_idx_cols=(18,),
            log_level=self.logger.level,
            logger=self.logger,
        )()

        self.logger.info("End of ETL process")
