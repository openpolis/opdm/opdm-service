from datetime import timedelta, datetime
from typing import Iterable

from django.db import transaction
from django.db.models import Count, signals
from django.db.models.functions import Concat
from popolo.models import Ownership
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Find duplicate ownerships, resolve them, updating dates, or removing duplicated records."
    dry_run = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action="store_true",
            help="Do not save in the DB"
        )

    @staticmethod
    def prev_day(str_date: str, fmt: str = "%Y-%m-%d") -> str:
        """A function that, given a date in a given format,
        returns the previos date, using the same format.

        :param str_date: - the given date
        :param fmt:      - the given date format

        :return: the previous day, using ``fmt``.
        """
        d = timedelta(days=1)
        return datetime.strftime(
            datetime.strptime(str_date, fmt) - d,
            fmt
        )

    def process_duplicate_ownerships(self, duplicate_ownerships: Iterable, dry_run: bool = False):
        """A generic function that may be used both for persons and organizations ownerships.

        The algorithm in the function uses a loop over all duplicated ownerships,
        and an inner loop over each duplicated record for an ownership.

        :param duplicate_ownerships: the ownerhips having more than one record
        :param dry_run: whether changes should be written to the DB or not
        :return:
        """

        # ensure easy_audit post_delete signals is disconnected, as it generates errors,
        # probably due to transactions issues
        signals.post_delete.disconnect(dispatch_uid='easy_audit_signals_post_delete')

        for i, o in enumerate(duplicate_ownerships, start=1):
            n_duplicates = o.pop('n')
            ows = [ow for ow in Ownership.objects.filter(**o).order_by('-created_at')]
            self.logger.info("")
            self.logger.info(f"{i}: {ows[0].owner} -[owns]-> {ows[0].owned_organization} ({n_duplicates} records)")
            if any(
                filter(
                    lambda x: x.start_date is None,
                    ows[:-1]
                )
            ):
                self.logger.warning("|- some null dates were found, removing all records but the latest.")
                for ow in ows[1:]:
                    self.logger.warning(f"|  |- {ow.percentage}% from: {ow.start_date} to: {ow.end_date}")
                    if not dry_run:
                        ow.delete()
            else:
                with transaction.atomic():
                    for n, ow in enumerate(ows):
                        creation_date = datetime.strftime(ow.created_at, '%Y-%m-%d')

                        # store values to allow for update and deletion
                        percentage = ow.percentage
                        start_date = ow.start_date
                        end_date = ow.end_date

                        end_date_msg = f"{end_date}"
                        if n > 0:
                            delta = abs(ows[n-1].percentage - percentage)

                            if end_date != self.prev_day(ows[n-1].start_date):
                                # self.logger.info(f"  d: {delta} ({delta > 10e-5})")
                                if delta > 10e-5:

                                    if start_date and start_date >= ows[n-1].start_date:
                                        end_date_msg += " =REMOVED= (duplicated starting date)"
                                        if not dry_run:
                                            ows[n].delete()
                                    else:
                                        ow.end_date = self.prev_day(ows[n-1].start_date)
                                        if (
                                            ow.start_date is None or
                                            (ow.start_date is not None and ow.end_date > ow.start_date)
                                        ):
                                            if not dry_run:
                                                ow.save()
                                        else:
                                            self.logger.error(
                                                f"Wrong dates found: end_date {ow.end_date} "
                                                f"can not be before {ow.start_date}."
                                            )
                                else:
                                    end_date_msg += " =REMOVED= (duplicated percentage)"
                                    if not dry_run:
                                        ow.delete()

                        self.logger.info(
                            f"|-{n} (@{creation_date}) - {percentage}% from: {start_date} to: {end_date_msg}"
                        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Start of procedure")

        self.dry_run = options["dry_run"]

        self.logger.info("Organization ownerships")
        duplicate_org_ownerships = Ownership.objects.\
            filter(owner_organization__isnull=False).\
            order_by('owned_organization', 'owner_organization', ).\
            values('owned_organization_id', 'owner_organization_id', ).\
            annotate(k=Concat('owned_organization_id', 'owner_organization_id', )).\
            annotate(n=Count('k')).order_by('-n').\
            filter(n__gt=1).values('owned_organization_id', 'owner_organization_id', 'n')
        self.process_duplicate_ownerships(duplicate_org_ownerships, dry_run=self.dry_run)

        self.logger.info("Personal ownerships")
        duplicate_pers_ownerships = Ownership.objects.\
            filter(owner_person__isnull=False).\
            order_by('owned_organization', 'owner_person', ).\
            values('owned_organization_id', 'owner_person_id', ).\
            annotate(k=Concat('owned_organization_id', 'owner_person_id', )).\
            annotate(n=Count('k')).order_by('-n').\
            filter(n__gt=1).values('owned_organization_id', 'owner_person_id', 'n')
        self.process_duplicate_ownerships(duplicate_pers_ownerships, dry_run=self.dry_run)

        self.logger.info("End of procedure")
