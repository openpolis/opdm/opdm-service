import logging

from django.core.management import BaseCommand
from django.db.models import Count, F
from popolo.models import Organization


class Command(BaseCommand):
    help = "Remove organs (Organizations) pointing to wrong areas and extra organs"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start")

        # loop over all interesting organization types
        for context in ["Regione", "Provincia", "Città metropolitana", "Comune"]:
            res = (
                Organization.objects.filter(parent__classification=context)
                .exclude(name__icontains=F("parent__area__name"))
                .delete()
            )
            if res[0] > 1:
                self.logger.info(
                    "{0} organizations of type {1} removed".format(
                        res[1]["popolo.Organization"], context
                    )
                )

        # # transfer memberships for these
        # for cr_old_name, cr_new_name in [
        #     ("Consiglio regionale della Valle d'Aosta/Vallée d'Aoste", "CONSIGLIO REGIONALE DELLA VALLE D'AOSTA"),
        #     ("Consiglio regionale della Friuli-Venezia Giulia", "CONSIGLIO REGIONALE DEL FRIULI VENEZIA GIULIA"),
        #     ("Consiglio regionale del Trentino-Alto Adige/Südtirol", "CONSIGLIO REGIONALE DELLA REGIONE AUTONOMA
        # TRENTINO ALTO - ADIGE SUDTIROL")
        # ]:
        #     try:
        #         cr_old = Organization.objects.get(name=cr_old_name)
        #     except Organization.DoesNotExist:
        #         self.logger.warning("Could not find old CR {0}. Skipping.".format(cr_old_name))
        #         continue
        #
        #     try:
        #         cr_new = Organization.objects.get(name=cr_new_name)
        #     except Organization.DoesNotExist:
        #         self.logger.warning("Could not find new CR {0}. Skipping.".format(cr_new_name))
        #         continue
        #
        #     Membership.objects.filter(organization=cr_old).update(organization=cr_new)
        #     Organization.objects.get(name=cr_old_name).delete()
        #

        # rename wrong consigli regionali
        for cr_o in Organization.objects.filter(name__startswith="Consiglio regionale"):
            cr_o.name = cr_o.name.upper()
            cr_o.save()

        orgs = Organization.objects.annotate(n=Count("children")).filter(n__gt=2)
        for org in orgs:
            self.logger.info(
                "{0} organs found for Organization {1}".format(org.n, org.name)
            )
            res = org.children.annotate(n=Count("memberships")).filter(n=0).delete()
            if res[0] > 1:
                self.logger.info(
                    "  {0} {1} removed".format(res[1]["popolo.Organization"], context)
                )

        self.logger.info("End")
