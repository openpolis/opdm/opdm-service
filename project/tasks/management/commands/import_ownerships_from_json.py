import logging

from taskmanager.management.base import LoggingBaseCommand

from project.atoka.etl.transformations import get_cf
from project.tasks.etl.composites import JsonDiffCompositeETL
from project.tasks.etl.composites.diff_logics import JsonOutRemovesDiffLogic
from project.tasks.etl.extractors import JsonArrayExtractor
from project.tasks.etl.loaders.organizations import PopoloOrgOwnershipLoader
from project.tasks.etl.loaders.persons import PopoloPersonOwnershipLoader
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Import Ownerships relationships from a JSON source"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_uri", help="Source of the JSON file (http[s]:// or local path ./...)"
        )
        parser.add_argument(
            "--original-source",
            dest="original_source",
            help="Original source of the data, to add to sources attributes (ex: http://api.atoka.io)"
        )
        parser.add_argument(
            "--update-strategy",
            dest="update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to keep_old",
        )
        parser.add_argument(
            "--lookup-strategy",
            dest="lookup_strategy",
            default="identifier",
            help="Whether to lookup using mixed (name and start date), "
            "identifier (see --identifier-scheme) or mixed (identifier first, then cascade to anagraphical)",
        )
        parser.add_argument(
            "--identifier-scheme",
            dest="identifier_scheme",
            default='CF',
            help="Which scheme to use with identifier/mixed lookup strategy. None to use main identifier.",
        )
        parser.add_argument(
            "--private-company-verification",
            dest="private_company_verification",
            action='store_true',
            help="Enable verificaion of private orgs before anagraphical search in mixed strategy.",
        )
        parser.add_argument(
            "--log-step",
            dest="log_step",
            type=int,
            default=500,
            help="Number of steps to log process completion to stdout. Defaults to 500.",
        )

        parser.add_argument(
            "--owner-type",
            dest="owner_type",
            default="organization",
            help="Type of owner, organization or person"
        )

        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.handle_cache(*args, **options)

        self.logger.info("Start loading procedure")

        update_strategy = options["update_strategy"]
        lookup_strategy = options["lookup_strategy"]
        identifier_scheme = options["identifier_scheme"]
        private_company_verification = options['private_company_verification']
        source_uri = options["source_uri"]
        original_source = options["original_source"]
        log_step = options["log_step"]
        owner_type = options["owner_type"].lower()
        filename = source_uri[source_uri.rfind("/")+1:]

        if owner_type == "person":
            loader_class = PopoloPersonOwnershipLoader
        else:
            loader_class = PopoloOrgOwnershipLoader

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(source_uri),
            loader=loader_class(
                update_strategy=update_strategy,
                lookup_strategy=lookup_strategy,
                identifier_scheme=identifier_scheme,
                private_company_verification=private_company_verification,
                log_step=log_step
            ),
            source=original_source,
            key_getter=lambda x: (
                get_cf(x['owning_{0}'.format(owner_type.replace('organization', 'org'))]),
                get_cf(x['owned_org'])
            ),
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename=filename,
            clear_cache=self.clear_cache,
            logger=self.logger,
            log_level=self.logger.level,
            removes_diff_logic=JsonOutRemovesDiffLogic(local_out_path=self.local_out_path)
        )()
        self.logger.info("End loading procedure")
