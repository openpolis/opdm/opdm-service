import logging

from django.core.management import BaseCommand
import numpy as np
import pandas as pd
from popolo.models import Classification, RoleType


class Command(BaseCommand):
    help = (
        "Generate opdm RoleTypes, starting from a CSV file with atoka->opdm mapping"
    )

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            "--csv-file",
            dest="csvfile",
            default="./resources/data/role_types_atoka_opdm.csv",
            help="Complete csv filename",
        )
        parser.add_argument(
            "--delete",
            dest="delete",
            action='store_true',
            help="Reset by deleting all roletypes that would be created (in development phase)"
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        csvfile = options['csvfile']

        self.logger.info("Start")
        df = pd.read_csv(csvfile, delimiter=";", dtype=object)
        classifications_mapping = None
        for n, row in enumerate(df.to_dict('records')):
            if n == 0:
                classifications_mapping = row
            else:
                for atoka_label, opdm_id in classifications_mapping.items():
                    if opdm_id is np.nan:
                        continue
                    opdm_id = int(opdm_id)
                    rt_label = "{0} {1}".format(
                        row['role_type_opdm'],
                        Classification.objects.get(id=opdm_id).descr.lower()
                    )
                    if options['delete']:
                        rt = RoleType.objects.filter(
                            label=rt_label,
                            classification_id=opdm_id
                        )
                        if rt:
                            rt.delete()
                            print(rt_label + " deleted")
                    else:
                        if int(row[atoka_label]) > 0:
                            rt, created = RoleType.objects.get_or_create(
                                label=rt_label,
                                classification_id=opdm_id
                            )
                            if created:
                                print(rt_label + " created")
        self.logger.info("End")
