# -*- coding: utf-8 -*-
import os

from django.contrib.gis.db.models import Union
from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import Polygon, MultiPolygon
from popolo.models import Area
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """This command will import the 2020 electoral constituencies geometries from given shp files.

    Data come from https://www.istat.it/it/archivio/273443
    and have been transformed into separated, simplified shp files with mapshaper, using:

    mapshaper -i shp/COLLEGI_ELETTORALI_2017.shp \
        -proj wgs84 -simplify 10% visvalingam weighted -clean \
        -o /tmp/collegi_tot.shp

    mapshaper -i tmp_collegi_tot.shp encoding=utf8\
        -filter-fields CIRC_COD,CIRC_DEN,COD_REG \
        -dissolve CIRC_COD copy-fields CIRC_DEN,COD_REG \
        -rename-layers circoscrizioni target=1 \
        -o circoscrizioni_camera_2020.shp

    mapshaper -i tmp_collegi_tot.shp encoding=utf8 \
        -filter-fields CP20_COD,CP20_DEN,CIRC_COD \
        -dissolve CP20_COD copy-fields CP20_DEN,CIRC_COD \
        -rename-layers collegi_pluri target=1 \
        -o collegi_pluri_camera_2020.shp

    mapshaper -i tmp_collegi_tot.shp encoding=utf8 \
        -filter-fields CU20_COD,CU20_DEN,CP20_COD \
        -dissolve CU20_COD copy-fields CU20_DEN,CP20_COD \
        -rename-layers collegi_uni target=1 \
        -o collegi_uni_camera_2020.shp

    mapshaper -i tmp_collegi_tot.shp encoding=utf8 \
        -filter-fields SP20_COD,SP20_DEN,COD_REG \
        -dissolve SP20_COD copy-fields SP20_DEN,COD_REG \
        -rename-layers collegi_pluri target=1 \
        -o collegi_pluri_senato_2020.shp

    mapshaper -i tmp_collegi_tot.shp encoding=utf8 \
        -filter-fields SU20_COD,SU20_DEN,SP20_COD \
        -dissolve SU20_COD copy-fields SU20_DEN,SP20_COD \
        -rename-layers collegi_uni target=1 \
        -o collegi_uni_senato_2020.shp
    """
    help = "Import Areas geographic limits for electoral constituencies in 2022 political elections"
    shp_files_path = None
    branch = None

    def add_arguments(self, parser):
        parser.add_argument(
            dest='shp_files_path',
            help="Absolute path to the compressed shp files directory"
        )
        parser.add_argument(
            dest='branch',
            help="The branch to process: camera|senato"
        )

    def handle(self, *args, **options):

        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.shp_files_path = options['shp_files_path']
        self.branch = options['branch'].lower()

        self.logger.info("Starting import process.")

        try:
            branch1 = self.branch[0].upper()
            if self.branch == 'camera':
                circoscrizioni = self.fetch_features(
                    f"{self.shp_files_path}/circoscrizioni_{self.branch}_2020.shp"
                )
                for n, feature in enumerate(circoscrizioni, start=1):
                    self.update_or_create_area(
                        feature,
                        identifier_fn=lambda x: f"{branch1}_{x.get('CIRC_COD')}",
                        name_fn=lambda x: x.get('CIRC_DEN'),
                        classification='ELECT_CIRC',
                        parent_field='COD_REG',
                        parent_fn=lambda x: f"{x.get('COD_REG'):02}"
                    )
                self.logger.info(f"{len(circoscrizioni)} geometries for circoscrizioni updated or created")

            if branch1 == 'C':
                parent_field = 'CIRC_COD'

                def parent_fn(x):
                    return f"{branch1}_{x.get('CIRC_COD')}"
            else:
                parent_field = 'COD_REG'

                def parent_fn(x):
                    return f"{x.get('COD_REG'):02}"

            collegi_pluri = self.fetch_features(
                f"{self.shp_files_path}/collegi_pluri_{self.branch}_2020.shp"
            )
            indicator_field = f'{branch1}P20_COD'
            for n, feature in enumerate(collegi_pluri, start=1):
                self.update_or_create_area(
                    feature,
                    identifier_fn=lambda x: f"{branch1}P_{x.get(indicator_field)}",
                    name_fn=lambda x: x.get(f'{branch1}P20_DEN'),
                    classification='ELECT_COLL',
                    parent_field=parent_field,
                    parent_fn=parent_fn
                )

            collegi_uni = self.fetch_features(
                f"{self.shp_files_path}/collegi_uni_{self.branch}_2020.shp"
            )
            indicator_field = f'{branch1}U20_COD'
            parent_field = f'{branch1}P20_COD'
            for n, feature in enumerate(collegi_uni, start=1):
                self.update_or_create_area(
                    feature,
                    identifier_fn=lambda x: f"{branch1}U_{x.get(indicator_field)}",
                    name_fn=lambda x: x.get(f'{branch1}U20_DEN'),
                    classification='ELECT_COLL',
                    parent_field=parent_field,
                    parent_fn=lambda x: f"{branch1}P_{x.get(parent_field)}",
                )
        except Exception as e:
            self.logger.error(e)
            exit(-1)

        self.logger.info("End of import process")

    def update_or_create_area(self, feat, identifier_fn, name_fn, classification, parent_field, parent_fn) -> None:
        """Update or create an Area from a feature,

        identifier and name are extracted from the given feature fields
        the classification is used also as a prefix for the identifier

        :param feat: the geometric feature, extracted from the shape file
        :param identifier_fn: callback used to build the identifier (with classification as prefix)
        :param name_fn: callback used to build the area name
        :param classification: classification and prefix for the identifier
        :param parent_field: field to be used to retrieve the parent
        :param parent_fn: callback used to build the parent's identifier
        :return:
        """
        area, created = Area.objects.update_or_create(
            identifier=identifier_fn(feat),
            defaults={
                'name': name_fn(feat) or "-",
                'classification': classification,
                'start_date': '2020-12-23'
            }
        )
        geom = feat.geom.transform(4326, clone=True)
        geos = geom.geos

        if isinstance(geos, Polygon):
            geos = MultiPolygon(geos)
        area.geometry = geos

        try:
            if parent_field == 'COD_REG':
                area.parent = Area.objects.get(
                    identifier=parent_fn(feat),
                    classification='ADM1'
                )
            elif 'CIRC_' in parent_field:
                parent_classification = 'ELECT_CIRC'
                area.parent = Area.objects.get(
                    identifier=parent_fn(feat),
                    classification=parent_classification
                )
            elif '_COD' in parent_field:
                parent_classification = 'ELECT_COLL'
                area.parent = Area.objects.get(
                    identifier=parent_fn(feat),
                    classification=parent_classification
                )

        except Exception as e:
            self.logger.error(
                f"parent area could not be found: {e}"
            )

        area.save()
        if created:
            self.logger.debug(
                f"area and geometry created for area {area.name}, "
                f"with identifier {area.identifier}, parent: {area.parent.identifier}"
            )
        else:
            self.logger.debug(
                f"geometry created for area {area.name}, with identifier {area.identifier}, "
                f"parent: {area.parent.identifier}"
            )

        return area

    def fetch_features(self, shp_file) -> Union(list, None):
        """Return list of geometric features from shp file

        :returns
        """
        if not os.path.exists(shp_file):
            self.logger.error(f"File {shp_file} was not found. Terminating.")
            return None

        # create DataSource instance from shp file
        ds = DataSource(shp_file)
        self.logger.info(f"geographic features layer read from {shp_file}")

        # extract layer
        layer = ds[0]
        if layer is None:
            raise Exception("No layer found in shp file. Terminating.")

        # transform the layer into a dict
        features_list = list(layer)
        return features_list
