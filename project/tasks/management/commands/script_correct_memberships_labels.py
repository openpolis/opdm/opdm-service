import logging
import re

from django.db.models import Q, F
from popolo.models import Membership
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Corrections for Memberships labels"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.logger.info("Start")

        ms = Membership.objects.exclude(label=F("post__label")).filter(
            label__in=[
                "Consigliere regionale",
                "Consigliere metropolitano",
                "Consigliere provinciale",
                "Consigliere comunale",
            ]
        )
        n_ms = ms.count()
        self.logger.info(
            "Find un-specific labels and set them to post.label. {0} memberships concerned.".format(
                n_ms
            )
        )
        for n, m in enumerate(ms, 1):
            if n % 100 == 0:
                self.logger.info("{0}/{1} processed.".format(n, n_ms))
            m.label = m.post.label
            m.save()

        ms = Membership.objects.exclude(label=F("post__label")).filter(
            Q(label__contains=": None") | Q(label="Assessore con deleghe: ")
        )
        n_ms = ms.count()
        self.logger.info(
            "Find mis-imported labels. {0} memberships concerned.".format(ms.count())
        )
        for n, m in enumerate(ms, 1):
            if n % 100 == 0:
                self.logger.info("{0}/{1} processed.".format(n, n_ms))
            m.label = m.post.label
            m.save()

        ms = Membership.objects.exclude(label=F("post__label")).filter(
            label__contains="Assessore con deleghe: "
        )
        n_ms = ms.count()
        self.logger.info(
            "Find assessori with deleghe and specify role and location in label. "
            "{0} memberships concerned.".format(ms.count())
        )
        for n, m in enumerate(ms, 1):
            if n % 100 == 0:
                self.logger.info("{0}/{1} processed.".format(n, n_ms))
            m.label = re.sub(r"Assessore", m.post.label, m.label)
            m.save()

        ms = (
            Membership.objects.exclude(label=F("post__label"))
            .exclude(label__icontains="deleghe")
            .exclude(label__icontains="ministro")
            .exclude(label__icontains="sottosegretario")
        )
        n_ms = ms.count()
        self.logger.info(
            "Fix all other labels. " "{0} memberships concerned.".format(ms.count())
        )
        for n, m in enumerate(ms, 1):
            if n % 100 == 0:
                self.logger.info("{0}/{1} processed.".format(n, n_ms))
            m.label = m.post.label
            m.save()

        self.logger.info("Finished")
