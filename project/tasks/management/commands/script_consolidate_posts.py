from django.db import transaction
from django.db.models import Count
from popolo.models import Post
from taskmanager.management.base import LoggingBaseCommand
from popolo.utils import PartialDatesInterval, PartialDate


class AKAException(Exception):
    pass


class Command(LoggingBaseCommand):
    help = "Find duplicate posts, merge the information and remove the duplicates."
    dry_run = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action="store_true",
            help="Do not save in the DB"
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Start of procedure")

        self.dry_run = options["dry_run"]

        duplicates = self.find_duplicates(
            logger=self.logger
        )
        n_duplicates = len(duplicates)
        self.logger.info(f"{n_duplicates} duplicates found")
        for n, d in enumerate(duplicates, start=1):
            self.logger.debug(f"processing {d}")
            p_dest = Post.objects.get(id=d[0])
            p_src_list = Post.objects.filter(id__in=d[1:])

            try:
                with transaction.atomic():
                    for p_src in p_src_list:
                        if not self.dry_run:
                            # merge persons details
                            self.merge_posts(source_post=p_src, dest_post=p_dest)

                    if not self.dry_run:
                        # remove duplicates after info have been copied
                        p_src_list.delete()
            except AKAException as e:
                self.logger.warning(
                    f"{e} encountered during processing of {p_src} => {p_dest} - skipping"
                )

            if n % 25 == 0:
                self.logger.info(f"{n}/{n_duplicates}")

        self.logger.info("End of procedure")

    @staticmethod
    def find_duplicates(logger: None) -> list:
        """Look for duplicates in Posts
        :param logger: the logger

        :return: list of tuples
        """
        duplicates = []
        if logger:
            logger.info("Looking for duplicates")

        dupl_values = list(
            Post.objects.values(
                'role_type', 'organization'
            ).annotate(
                n=Count(('role_type', 'organization'))
            ).filter(n__gt=1)
        )
        for d in dupl_values:
            del d['n']
            duplicates.append(tuple(Post.objects.filter(**d).values_list('id', flat=True)))

        return duplicates

    def merge_posts(self, source_post, dest_post):
        """Merge information from a post into another

        :param source_post: post to copy information from
        :param dest_post: post to receive information
        :return:
        """
        for m in source_post.memberships.values():
            m.pop('id')
            m.pop('post_id')
            m.pop('slug')
            person_id = m.pop('person_id')
            organization_id = m.pop('organization_id')
            m['label'] = dest_post.label
            m['role'] = dest_post.role

            same_org_person_memberships = dest_post.memberships.filter(
                person_id=person_id,
                organization_id=organization_id,
            )

            # new  dates interval as PartialDatesInterval instance
            new_int = PartialDatesInterval(start=m.get("start_date", None), end=m.get("end_date", None))

            is_overlapping = False
            for i in same_org_person_memberships:

                # existing identifier interval as PartialDatesInterval instance
                i_int = PartialDatesInterval(start=i.start_date, end=i.end_date)

                # compute overlap days
                #  > 0 means crossing
                # == 0 means touching (end date == start date)
                #  < 0 means not touching
                # dates only overlap if crossing
                overlap = PartialDate.intervals_overlap(new_int, i_int)

                if overlap > 0:
                    is_overlapping = True

            if not is_overlapping:
                dest_post.memberships.create(
                    person_id=person_id,
                    organization_id=organization_id,
                    **m
                )
                self.logger.info(f"Membership {m['label']} merged into post {dest_post}")
            else:
                self.logger.debug(f"Membership {m['label']} already existing in post {dest_post}")

        for i in source_post.links.values("link__url", "link__note"):
            try:
                r = dest_post.add_link(
                    url=i.get("link__url"), note=i.get("link__note", "")
                )
                if r:
                    self.logger.info("{0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))

        for i in source_post.sources.values("source__url", "source__note"):
            try:
                r = dest_post.add_source(
                    url=i.get("source__url"), note=i.get("source__note", "")
                )
                if r:
                    self.logger.info("{0} added".format(r))
            except Exception as e:
                self.logger.error("Errore: {0}".format(e))
