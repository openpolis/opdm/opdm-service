import logging

from django.contrib.contenttypes.models import ContentType
from django.core.management import BaseCommand
from ooetl import ETL
from ooetl.extractors import CSVExtractor
from popolo.models import Person

from project.akas.models import AKA
from project.tasks.etl.loaders import PopoloLoader


class MinintAka2OpdmETL(ETL):
    """Instance of ``ooetl.ETL`` class that handles
    importing minintaka csv data into opdm
    """

    def transform(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns.

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original dataframe
        od = self.original_data.copy()

        # store processed data into the ETL instance
        self.processed_data = od

        # return ETL instance
        return self


class PopoloPersonLoader(PopoloLoader):
    def load_item(self, item, **kwargs):
        """
        """
        # get_or_create the Organization
        ct = ContentType.objects.get(model="person")

        try:
            p = Person.objects.get(
                identifiers__scheme="OP_ID", identifiers__identifier=item["op_id"]
            )
        except Person.DoesNotExist:
            self.logger.warn(
                "Person with op_id: {0} not found in OPDM".format(item["op_id"])
            )
        else:
            parts = item["minint_aka"].split("+")
            search_params = {}
            if len(parts) < 2:
                self.logger.warning(
                    "{0} too short, less than 2 parts".format(item["minint_aka"])
                )
                return
            if len(parts) >= 2:
                search_params["given_name"] = parts[0].lower()
                search_params["family_name"] = parts[1].lower()
            if len(parts) >= 3:
                search_params["birth_date"] = parts[2]
            if len(parts) >= 4:
                search_params["birth_location"] = parts[3]
            if len(parts) >= 5:
                self.logger.warning(
                    "{0} too long, less than 2 parts".format(item["minint_aka"])
                )
                return

            loader_context = {
                "item": item["minint_aka"],
                "context": "import_op_minintakas",
                "source": self.etl.extractor.remote_url,
            }

            aka, created = AKA.objects.get_or_create(
                n_similarities=0,
                search_params=search_params,
                loader_context=loader_context,
                is_resolved=True,
                content_type=ct,
                object_id=p.pk,
            )

            if created:
                self.logger.debug(
                    "minint_aka: {0} added to Person {1}".format(item["minint_aka"], p)
                )


class Command(BaseCommand):
    help = "Import Posts and Persons from Openpolis API"

    logger = logging.getLogger(__name__)
    csv_url = "https://s3.eu-central-1.amazonaws.com/opdm-service-data/minintakas.csv"
    csv_extractor = CSVExtractor(
        csv_url,
        sep=",",
        encoding="utf8",
        converters={"op_id": int},
        na_values=["", "-"],
        keep_default_na=False,
    )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start records creation")

        etl = MinintAka2OpdmETL(
            extractor=self.csv_extractor,
            loader=PopoloPersonLoader(),
            log_level=self.logger.level,
        )

        self.logger.info("Starting ETL process")
        etl.etl()
        self.logger.info("End")
