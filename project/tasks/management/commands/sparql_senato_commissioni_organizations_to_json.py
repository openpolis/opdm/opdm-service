import re
from typing import Dict, List, Optional, Tuple

from popolo.models import Classification, Organization, KeyEvent
import roman

from project.tasks.parsing.common import parse_date
from project.tasks.management.base import SparqlToJsonCommand
from project.tasks.parsing.sparql.utils import get_bindings, read_raw_rq


class Command(SparqlToJsonCommand):
    json_filename = "senato_commissioni_organizations"

    @staticmethod
    def get_legislature_dates(n_leg: int) -> Tuple:
        legislature_qs = KeyEvent.objects.filter(event_type='ITL')
        start_date = dict(
            legislature_qs.values_list('identifier', 'start_date')
        ).get(f"ITL_{n_leg:02d}")
        end_date = dict(
            legislature_qs.values_list('identifier', 'end_date')
        ).get(f"ITL_{n_leg:02d}")
        return start_date, end_date

    def query(self, **options) -> Optional[List[Dict]]:
        n_leg = options['legislature']
        start_date, end_date = self.get_legislature_dates(n_leg)

        # adapt raw_rq to all legislatures
        raw_rq = read_raw_rq("senato_commissioni_organizations_17.rq")
        raw_rq = re.sub(
            r"2013-03-15",
            f"{start_date}",
            raw_rq
        )
        if (end_date or '') != '':
            raw_rq = re.sub(
                r"2018-03-22",
                f"{end_date}",
                raw_rq
            )
        else:
            raw_rq = re.sub(
                r'FILTER\(\?dataInizio < \"2018-03-22T12:00:00\+01:00\"\^\^xsd:dateTime\)',
                "",
                raw_rq
            )
        raw_rq = re.sub(
            r"osr:legislatura \d+",
            f"osr:legislatura {options['legislature']:02d}",
            raw_rq
        )

        return get_bindings(
            "https://dati.senato.it/sparql",
            raw_rq
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        n_leg = options["legislature"]
        start_date, end_date = self.get_legislature_dates(n_leg)

        org_classification_map = {
            "consiglio di presidenza": "Organo di presidenza parlamentare",
            "giunta": "Giunta parlamentare",
            "commissione per la biblioteca": "Giunta parlamentare",
            "commissione permanente": "Commissione permanente parlamentare",
            "commissione parlamentare d'inchiesta": "Commissione d'inchiesta parlamentare monocamerale/bicamerale",
            "commissione parlamentare di inchiesta": "Commissione d'inchiesta parlamentare monocamerale/bicamerale",
            "commissione straordinaria": "Commissione speciale/straordinaria parlamentare",
            "commissione speciale": "Commissione speciale/straordinaria parlamentare",
            "commissione per": "Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza",
            "commissione parlamentare per": "Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza",
            "comitato per la legislazione": "Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza",
            "comitato parlamentare": "Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza",
            "delegazione": "Delegazione parlamentare presso assemblea internazionale",
            "parlamentari membri": "Commissioni e comitati parlamentari di indirizzo, controllo e vigilanza",
        }
        classification_descr_to_id_map = dict(
            Classification.objects.filter(scheme="FORMA_GIURIDICA_OP").values_list(
                "descr", "id"
            )
        )

        commissioni_bicamerali = [
            "Parlamentari membri della Commissione di vigilanza sulla Cassa Depositi e Prestiti",
            "Parlamentari membri della Commissione per l'accesso ai documenti amministrativi",
            "Commissione parlamentare per l'infanzia e l'adolescenza",
            "Commissione parlamentare per l'indirizzo generale e la vigilanza dei servizi radiotelevisivi",

            "Delegazione italiana all'Assemblea parlamentare della "
            "Organizzazione per la sicurezza e la cooperazione in Europa (OSCE)",

            "Delegazione parlamentare italiana presso l'Assemblea del Consiglio d'Europa",
            "Delegazione parlamentare italiana presso l'Assemblea parlamentare della NATO",
            "Delegazione parlamentare italiana presso l'Assemblea parlamentare dell'Iniziativa Centro Europea",
            "Delegazione parlamentare italiana presso l'Assemblea parlamentare dell'Unione per il Mediterraneo",
            "Commissione parlamentare di inchiesta sul rapimento e sulla morte di Aldo Moro",
            "Commissione parlamentare di inchiesta sulle cause del disastro del traghetto Moby Prince",

            "Commissione parlamentare di inchiesta sulla ricostruzione della città dell'Aquila e "
            "degli altri comuni interessati dal sisma del 6 aprile 2009",

            "Commissione straordinaria per la verifica dell'andamento generale dei prezzi al consumo "
            "e per il controllo della trasparenza dei mercati",

            "Sottocommissione permanente per l'accesso Senato - Sottocommissione permanente RAI per l' accesso",

            "Commissione parlamentare per le questioni regionali",
            "Comitato parlamentare per i procedimenti di accusa",
            "Commissione parlamentare di vigilanza sull'anagrafe tributaria",
            "Commissione di vigilanza servizi radiotelevisivi",
            "Commissione di vigilanza sull'anagrafe tributaria",
            "Comitato parlamentare per la sicurezza della Repubblica",
            "Commissione di controllo enti gestori previdenza assistenza",
            "Comitato parlamentare Schengen, Europol e immigrazione",
            "Commissione parlamentare per l'infanzia e l'adolescenza",
            "Commissione parlamentare per la semplificazione",
            "Commissione parlamentare per l'attuazione del federalismo fiscale",

            "Commissione parlamentare per il controllo sull'attività "
            "degli enti gestori di forme obbligatorie di previdenza e assistenza sociale",

            "Commissione parlamentare per il controllo sull'attivita' "
            "degli enti gestori di forme obbligatorie di previdenza e assistenza",

            "Comitato parlamentare di controllo sull'attuazione dell'Accordo di Schengen, "
            "di vigilanza sull'attività di Europol, di controllo e vigilanza in materia di immigrazione",

            "Commissione di inchiesta sul fenomeno delle mafie",
            "Commissione di inchiesta sul ciclo dei rifiuti",
            "Commissione di inchiesta sul rapimento e sulla morte di Aldo Moro",
            "Commissione di inchiesta sul sistema bancario e finanziario",

            "Commissione parlamentare di inchiesta sul fenomeno delle mafie e "
            "sulle altre associazioni criminali, anche straniere",

            "Commissione parlamentare di inchiesta sulle attività illecite connesse al ciclo dei rifiuti e su altri "
            "illeciti ambientali e agroalimentari",

            "Commissione parlamentare di inchiesta sul sistema bancario e finanziario",
            "Commissione parlamentare di inchiesta sui fatti accaduti presso la comunità \"Il Forteto\"",

            "Commissione parlamentare di inchiesta sulle attività "
            "connesse alle comunità di tipo familiare che accolgono minori",

            "Commissione di vigilanza Cassa Depositi e Prestiti",
            "Commissione per l'accesso ai documenti amministrativi",
            "Commissione consultiva ricompense al merito civile",
            "Commissione parlamentare di inchiesta sul femminicidio",
            "Commissione parlamentare per il contrasto degli svantaggi derivanti dall'insularità",

        ]

        def get_classification_from_titolo(
            classification_map: dict,
            titolo: str
        ) -> str:
            for k, descr in classification_map.items():
                if k.lower() in titolo.lower():
                    return descr

            return ''

        try:
            parent = Organization.objects.filter(identifier=f"ASS-SENATO-{n_leg}").get()
        except Organization.DoesNotExist:
            self.logger.error(f"Organization ASS-CAMERA-{n_leg} does not exist.")
            return

        organizations_buf = []

        for binding in bindings:
            # skip commissioni bicamerali, che vengono prese dalla Camera
            skip = False
            for commissione_bicamerale_partial in commissioni_bicamerali:
                if commissione_bicamerale_partial.lower() in binding["titolo"].value.lower():
                    self.logger.debug(
                        f"{binding['titolo'].value} is Bicamerale, will get it from Camera ... Skipping."
                    )
                    skip = True
                    break
            if skip:
                continue

            classification = get_classification_from_titolo(
                org_classification_map, binding["titolo"].value
            )
            if classification == '':
                self.logger.debug(
                    f"{binding['titolo'].value} not mapped to known classification ... Skipping."
                )
                continue

            # compose org name
            name = binding['titolo'].value
            if classification == "Commissione permanente parlamentare":
                name = f"{name} Senato - {binding['titoloBreve'].value} ({roman.toRoman(n_leg)} legislatura)"
            else:
                name = f"{name} Senato ({roman.toRoman(n_leg)} legislatura)"

            parent_id = parent.id

            identifier = f"{binding['organo'].value}"
            tmp = {
                "name": name,
                "classification": classification,
                "founding_date": (
                    parse_date(binding["dataInizio"].value)
                    if "dataInizio" in binding and binding["dataInizio"].value >= start_date
                    else start_date
                ),
                "dissolution_date": (
                    parse_date(binding["dataFine"].value)
                    if "dataFine" in binding
                    else end_date
                ),
                "identifier": identifier,
                "identifiers": [
                    {
                        "scheme": "OSR-URI",
                        "identifier": identifier
                    }
                ],
                "sources": [
                    {
                        "note": "Open Data Senato",
                        "url": "https://dati.senato.it/",
                    }
                ],
                "parent": parent_id,
            }
            if classification != "Organo di presidenza parlamentare":
                tmp["identifier"] = f"{identifier}-{n_leg:02d}"
                tmp["identifiers"].append({
                    "scheme": "OSR-URI+",
                    "identifier": f"{identifier}-{n_leg:02d}"
                })
            else:
                tmp["identifiers"].append({
                    "scheme": "OSR-URI+",
                    "identifier": f"{identifier}"
                })

            # Attempt to retrieve the classification id
            if classification in classification_descr_to_id_map:
                tmp["classifications"] = [
                    {
                        "classification": classification_descr_to_id_map.get(
                            classification
                        )
                    }
                ]
            else:
                self.logger.debug(
                    f'Classification "{classification}" with schema "FORMA_GIURIDICA_OP" not found. '
                    f"Classification id is not added."
                )
            organizations_buf.append(tmp)

        return organizations_buf
