from django.db import transaction
from taskmanager.management.base import LoggingBaseCommand

from project.akas.models import AKA
from project.core import person_utils
from project.core.exceptions import UnprocessableEntitytAPIException


class Command(LoggingBaseCommand):
    help = "Find all solved similarities pointing ro non-existing persons, and re-create them"
    context = None
    run_dry = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--context",
            dest="context",
            default='atoka',
            help="Context string used in loader_context",
        )
        parser.add_argument(
            "--run-dry",
            dest="run_dry",
            action="store_true",
            help="Do not write in the DB",
        )

    def handle(self, *args, **options):

        self.setup_logger(__name__, formatter_key='simple', **options)

        context = options["context"]
        run_dry = options["run_dry"]

        self.logger.info("Start")

        akas = AKA.objects.filter(is_resolved=True, loader_context__context__icontains=context)
        n_akas = akas.count()

        with transaction.atomic():
            c = 0
            for n, aka in enumerate(akas, start=1):
                if n % 1000 == 0:
                    self.logger.info(f"{n}/{n_akas}.")

                if aka.content_object is None:
                    c += 1
                    if not run_dry:
                        item = aka.loader_context['item']
                        try:
                            p, created = person_utils.update_or_create_person_from_item(
                                item, 0
                            )
                        except Exception as e:
                            raise UnprocessableEntitytAPIException(detail=str(e))
                        else:
                            person_utils.sync_solr_index(p)

                        aka.object_id = p.id
                        aka.save()
                        self.logger.info(
                            f"{c}/{n_akas}: aka points to non-existing {aka.object_id}; "
                            f"person {p.id} created; aka re-saved."
                        )
                    else:
                        self.logger.info(f"{c}/{n_akas}: aka points to non-existing {aka.object_id}.")

        self.logger.info("End")
