# -*- coding: utf-8 -*-
from datetime import datetime
from io import BytesIO
import os
import zipfile

from django.contrib.gis.gdal import DataSource
from django.contrib.gis.geos import Polygon, MultiPolygon
from django.db.models import Q
from popolo.models import Area
import requests
from taskmanager.management.base import LoggingBaseCommand

CURRENT_YEAR = datetime.now().year


class Command(LoggingBaseCommand):
    """This command will fetch shp files from ISTAT website,
    uncompress them and keep then in a cache path.

    From shp files, geographic limits are extracted and imported into Areas in OPDM.

    Geometries are imported from the last valid shp file for a given area.
    This can be computed looking the `end_date` field of an Area:
    - Areas where end_date__isnull=True OR end_date__gte='LASTYEAR-01-01', get their geometries from LASTYEAR.shp
      or {LASTYEAR-1}.shp if the file is not found
    - Areas where end_date__gte='YEAR-01-01', get their geometries from YEAR.shp

    """
    help = "Import Areas geographic limits from ISTAT shp files"
    context = None
    last_year = None
    first_year = None
    shp_cache_path = None
    overwrite = False

    def add_arguments(self, parser):
        parser.add_argument(
            '--last-year',
            dest='last_year', type=int,
            default=CURRENT_YEAR,
            help="Last year published by ISTAT, format YYYY. Defaults to current."
        )
        parser.add_argument(
            '--first-year',
            dest='first_year', type=int,
            default=2001,
            help="First year published by ISTAT, format YYYY. Defaults to 2001"
        )
        parser.add_argument(
            '--shp-cache-path',
            dest='shp_cache_path',
            default='./data/shp',
            help="Path were exploded shp files are kept."
        )
        parser.add_argument(
            '--context',
            dest='context',
            default='com',
            help="Context to import. [com|prov|reg|rip]"
        )
        parser.add_argument(
            '--overwrite',
            dest='overwrite', action='store_true',
            help="Whether to overwrite all geometries or only write missing ones"
        )

    def handle(self, *args, **options):

        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.context = options['context']
        self.last_year = options['last_year']
        self.first_year = options['first_year']
        self.shp_cache_path = options['shp_cache_path']
        self.overwrite = options['overwrite']

        context_filter = {
            'com': {
                'identifiers__scheme': 'ISTAT_CODE_COM',
                'classification': 'ADM3'
            },
            'prov': {
                'identifiers__scheme': 'ISTAT_CODE_PROV',
                'classification': 'ADM2'
            },
            'cm': {
                'identifiers__scheme': 'ISTAT_CODE_CM',
                'classification': 'ADM2'
            },
            'reg': {
                'identifiers__scheme': 'ISTAT_CODE_REG',
                'classification': 'ADM1'
            },
            'rip': {
                'classification': 'RGNE'
            }
        }[self.context]

        self.logger.info(f"Starting import process for context {self.context}.")

        if self.context == 'rip':
            id_field = 'identifier'
        else:
            id_field = 'identifiers__identifier'

        interesting_areas = Area.objects.filter(
            Q(end_date__isnull=True) |
            Q(end_date__gte=f"{self.last_year}-01-01")
        ).filter(**context_filter).values('id', id_field)

        if not self.overwrite:
            interesting_areas = interesting_areas.filter(geometry__isnull=True)

        # fetch geometries for areas still valid or valid until current years' 01-01
        if interesting_areas.count():
            res = self.check_and_update_geoms(interesting_areas, self.last_year, id_field=id_field)
            if res == -1:
                self.check_and_update_geoms(interesting_areas, self.last_year-1, id_field=id_field)

        # fetch geometries for areas closed during previous years
        for year in range(self.last_year - 1, self.first_year, -1):

            interesting_areas = Area.objects.filter(
                Q(end_date__gte=f"{year}-01-01") &
                Q(end_date__lte=f"{year}-12-31")
            ).filter(**context_filter).values('id', id_field)

            if not self.overwrite:
                interesting_areas = interesting_areas.filter(geometry__isnull=True)

            if interesting_areas.count() == 0:
                self.logger.info(f"No interesting areas for year {year}. Skipping.")
                continue
            self.check_and_update_geoms(interesting_areas, year, id_field=id_field)

        self.logger.info("End of import process")

    def check_and_update_geoms(self, interesting_areas, year, id_field='identifiers__identifier'):
        """Loop over all features in shp layer, matching interesting_areas,
            extract the corresponding Area and change its geometry.

        :param interesting_areas: areas deemed interesting
        :param year: year of concern
        :param id_field: field used to fetch identifiers
        :return: 0 on success; -1 if no layer could be found
        """
        self.logger.info(f"checking geoms for {len(interesting_areas)} areas in {year}")

        interesting_areas_identifiers = [a[id_field] for a in interesting_areas]
        context_layer_filter = {
            'com': lambda f: f.get('PRO_COM_T').zfill(6),
            'prov': lambda f: str(f.get('COD_PROV')).zfill(3),
            'cm': lambda f: str(f.get('COD_CM')),
            'reg': lambda f: str(f.get('COD_REG')).zfill(2),
            'rip': lambda f: str(f.get('COD_RIP')),
        }[self.context]

        layer = self.fetch_shp_layer(year)
        if layer is None:
            return -1

        interesting_features = list(filter(
            lambda f: context_layer_filter(f) in interesting_areas_identifiers,
            layer
        ))
        for n, feat in enumerate(interesting_features, start=1):
            istat_id = context_layer_filter(feat)
            area_id = next(filter(
                lambda a: a[id_field] == istat_id,
                interesting_areas
            ))['id']
            area = Area.objects.get(id=area_id)

            # shp features contain coordinates with srid = 32632
            # EPSG:32632 WGS 84 / UTM zone 32N
            # that need to be transformed into lat/lon (srid 4326),
            # EPSG:4326 WGS 84
            # before being used in a geometry field
            geom = feat.geom.transform(4326, clone=True).geos
            if isinstance(geom, Polygon):
                geom = MultiPolygon(geom)

            area.geometry = geom

            area.save()
            self.logger.debug(f"geometry updated for area {area.name}, with ISTAT_CODE_COM: {istat_id}")
            if n % 100 == 0:
                self.logger.info(f"{n}/{len(interesting_features)}")

        self.logger.info(f"{len(interesting_features)} geometries updated for year {year}")

        return 0

    def fetch_shp_layer(self, year):
        """Fetch shp file from ISTAT or from cache, return the corresponding layer,
        according to context and year.

        :param year: The year we're interested in
        :return: a DataSource layer (iterable for features)
        """
        shp_base_url = "http://www.istat.it/storage/cartografia/confini_amministrativi/generalizzati"
        shp_cache_path = os.path.abspath(self.shp_cache_path)
        if year in [2011, 2001, 1991]:
            shp_zipname = f"Limiti{year}_g"
        else:
            shp_zipname = f"Limiti0101{year}_g"

        if not os.path.exists(os.path.join(shp_cache_path, shp_zipname)):
            self.logger.info(f"downloading {shp_base_url}/{year}/{shp_zipname}.zip into {shp_cache_path}")
            r = requests.get(f"{shp_base_url}/{year}/{shp_zipname}.zip")
            if r.status_code == 404:
                self.logger.warning(f"could not find {shp_base_url}/{year}/{shp_zipname}.zip")
                return None
            with zipfile.ZipFile(BytesIO(r.content)) as z:
                z.extractall(shp_cache_path)
        else:
            self.logger.info(f"using cached shp files in {shp_cache_path}/{shp_zipname}")

        froot, fdirs, ffiles = next(
                (root, dirs, files)
                for (root, dirs, files) in os.walk(os.path.join(shp_cache_path, shp_zipname))
                if self.context in root.lower()
        )
        fname = next(
            i for i in ffiles if 'shp' in i
        )
        shp_file_path = os.path.join(shp_cache_path, froot, fname)
        ds = DataSource(shp_file_path)
        self.logger.info(f"geographic features layer read from {shp_cache_path}{froot}/{fname}")

        return ds[0]
