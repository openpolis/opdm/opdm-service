import logging

from django.db import transaction
from popolo.models import Identifier, Organization
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """Update all organizations' identifiers differing from identifies with scheme: CF"""
    help = "Corrections of identifier field for Organizations"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            "--dry-run",
            dest="dry_run", action='store_true',
            help="Show logs, do not modify data in DB.",
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.setup_logger(__name__, formatter_key="simple", **options)

        dry_run = options['dry_run']

        self.logger.info("Start")

        orgs_ids = Organization.objects.\
            filter(identifiers__scheme='CF').\
            values('identifiers__identifier', 'identifier', 'id').\
            distinct()
        orgs_ids = [d for d in orgs_ids if d['identifiers__identifier'] != d['identifier']]

        n_orgs = len(orgs_ids)
        self.logger.info(f"Updating {n_orgs} organizations")

        transaction.set_autocommit(False)

        for n, org_dict in enumerate(orgs_ids, start=1):
            try:
                org = Organization.objects.get(id=org_dict['id'])
                org.identifier = org_dict['identifiers__identifier']
                if not dry_run:
                    org.save()
            except Identifier.DoesNotExist:
                self.logger.warning(f"Could not find org for {org_dict}")

            if n % 100 == 0:
                self.logger.info(f"{n}/{n_orgs}")
                transaction.commit()

        transaction.commit()
        transaction.set_autocommit(True)

        self.logger.info("End")
