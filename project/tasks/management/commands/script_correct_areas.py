from django.db.models import Count, F
from popolo.models import Area, Identifier
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Corrections for Area data (can be safely launched more than once)"

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Start")

        self.logger.info("Add ISTAT_CODE_REG identifier to regions")
        for reg in Area.objects.filter(istat_classification="REG"):
            if reg.identifiers.filter(scheme="ISTAT_CODE_REG").all().count() == 0:
                reg.add_identifier(reg.identifier, "ISTAT_CODE_REG")

        self.logger.info("Add parent to some provinces in Sardegna")
        sardegna = Area.objects.get(name="Sardegna")
        Area.objects.province().filter(
            istat_classification="PROV", parent__isnull=True
        ).update(parent=sardegna)

        self.logger.info("Adjust city with 2 ISTAT_CODE_COM having null start_dates")
        doubles = Area.objects.filter(
            identifiers__scheme='ISTAT_CODE_COM', identifiers__start_date__isnull=True
        ).annotate(n=Count('identifiers__identifier')).values('id', 'n').filter(n=2)
        n_doubles = doubles.count()
        for n, d in enumerate(doubles, start=1):
            a = Area.objects.get(id=d['id'])
            identifiers = a.identifiers.filter(
                scheme='ISTAT_CODE_COM'
            ).values(
                'identifier', 'start_date', 'end_date'
            ).order_by(
                F('end_date').desc(nulls_last=True)
            )

            id1 = identifiers.first()
            id2 = identifiers.last()
            if n % 100 == 0:
                self.logger.info(f"{n}/{n_doubles}")
            if id2['end_date'] is None and id1['end_date'] is not None:
                try:
                    i = a.identifiers.get(identifier=id2['identifier'], end_date__isnull=True)
                except Identifier.MultipleObjectsReturned:
                    self.logger.warning(
                        f"{a} ({a.id}) has multiple ISTAT_CODE_COM:{id2['identifier']} instances, check it out manually"
                    )
                else:
                    i.start_date = id1['end_date']
                    i.save()
                    self.logger.info(f"{a}: {i} start_date set to {i.start_date}")

        multiples = Area.objects.filter(
            identifiers__scheme='ISTAT_CODE_COM', identifiers__start_date__isnull=True
        ).annotate(n=Count('identifiers__identifier')).values('id', 'n').filter(n__gt=2)
        for n, d in enumerate(multiples, start=1):
            a = Area.objects.get(id=d['id'])
            self.logger.info(
                f"{a} ({a.id}) has more than 2 ISTAT_CODE_COM identifiers with null start_dates, check it out manually"
            )

        self.logger.info("End")
