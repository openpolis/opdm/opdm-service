# -*- coding: utf-8 -*-
from ooetl import ETL
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.extractors import JsonArrayExtractor
from project.tasks.etl.loaders.keyevents import PopoloKeyEventLoader


class Command(LoggingBaseCommand):
    help = "Import KeyEvents from a remote or local json source"

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_url", help="Source of the JSON file (http[s]:// or file:///)"
        )
        parser.add_argument(
            "--update-strategy",
            dest="update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them (keep_old | overwrite), defaults to keep_old",
        )
        parser.add_argument(
            "--lookup-strategy",
            dest="lookup_strategy",
            default="mixed",
            help="Whether to lookup using mixed (name and start date), "
            "identifier (see --identifier-scheme) or mixed (identifier first, then cascade to anagraphical)",
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        update_strategy = options["update_strategy"]
        lookup_strategy = options["lookup_strategy"]
        source_url = options["source_url"]

        self.logger.info("Start records import")
        self.logger.info("Reading CSV from url: {0}".format(source_url))

        # define the instance and invoke the etl() method through __call__()
        self.logger.info("Starting ETL process")
        ETL(
            extractor=JsonArrayExtractor(source_url),
            loader=PopoloKeyEventLoader(
                lookup_strategy=lookup_strategy, update_strategy=update_strategy
            ),
            log_level=self.logger.level,
            logger=self.logger
        )()
        self.logger.info("End")
