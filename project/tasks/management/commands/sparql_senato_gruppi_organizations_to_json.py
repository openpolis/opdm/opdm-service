import re
from collections import defaultdict
from typing import Dict, Optional, List

from popolo.models import Classification, Organization
import roman

from tasks.management.base import SparqlToJsonCommand
from tasks.parsing.sparql.utils import get_bindings, read_raw_rq
from tasks.template_strings import ORG_GRUPPO_POLITICO_SENATO_NAME


class Command(SparqlToJsonCommand):
    json_filename = "senato_gruppi_organizations"

    def query(self, **options) -> Optional[List[Dict]]:
        q = re.sub(
            r"osr:legislatura \d+",
            f"osr:legislatura {options['legislature']:02d}",
            read_raw_rq("senato_gruppi_organizations_17.rq"),
        )
        return get_bindings(
            "https://dati.senato.it/sparql",
            q,
            legacy=True,
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        n_leg = options["legislature"]

        try:
            parent = Organization.objects.filter(identifier=f"ASS-SENATO-{n_leg:02d}").get()
            classification_gruppo_id = Classification.objects.get(
                scheme="FORMA_GIURIDICA_OP", code="3314"
            ).id
        except Organization.DoesNotExist:
            self.logger.error(f"Organization ASS-CAMERA-{n_leg:02} does not exist.")
            return
        except Classification.DoesNotExist:
            self.logger.error("Classification FORMA_GIURIDICA_OP=3314 does not exist.")
            return
        if not parent.founding_date:
            self.logger.warning(
                f"Parent organization ASS-SENATO-{n_leg:02} does not have a founding_date."
            )
            return

        # group organizations by identifier (URL)
        # limit fetch to records of this legislature
        organizations = defaultdict(list)
        for binding in bindings:
            identifier = binding["identifier"]["value"]

            if (
                    (
                        "dissolution_date" not in binding or
                        binding["dissolution_date"]["value"] >= parent.founding_date or
                        binding["dissolution_date"]["value"] is None
                    ) and
                    (
                        parent.dissolution_date is None or
                        binding["founding_date"]["value"] <= parent.dissolution_date

                    )
            ):

                founding_date = binding["founding_date"]["value"]
                if "dissolution_date" in binding:
                    dissolution_date = binding["dissolution_date"]["value"]
                else:
                    dissolution_date = parent.dissolution_date

                if founding_date < parent.founding_date:
                    founding_date = parent.founding_date

                organizations[identifier].append(
                    {
                        "name": ORG_GRUPPO_POLITICO_SENATO_NAME.format(
                            name=binding["name"]["value"].replace(f"{roman.toRoman(n_leg)} Legislatura", "").strip(),
                            roman=roman.toRoman(n_leg)
                        ),
                        "founding_date": founding_date,
                        "dissolution_date": dissolution_date,
                        "othername__ACR": binding["othername__ACR"]["value"],
                    }
                )

        # create groups, using latest one for the name and acronym,
        groups = []
        for identifier, gruppi in organizations.items():
            first_group = gruppi[0]
            latest_group = gruppi.pop()
            group = {
                "identifier": f"{identifier}-{n_leg:02d}",
                "identifiers": [
                    {
                        "scheme": "OSR-URI+",
                        "identifier": f"{identifier}-{n_leg:02d}",
                        "start_date": first_group["founding_date"],
                        "end_date": latest_group["dissolution_date"]
                    },
                    {
                        "scheme": "OSR-URI",
                        "identifier": identifier,
                        "start_date": first_group["founding_date"],
                        "end_date": latest_group["dissolution_date"]
                    }
                ],
                "name": latest_group["name"],
                "founding_date": first_group["founding_date"],
                "dissolution_date": latest_group["dissolution_date"],
                "parent": parent.id,
                "sources": [{
                    "note": "Open Data Senato",
                    "url": "http://dati.senato.it"
                }],
                "classification": "Gruppo politico in assemblea elettiva",
                "classifications": [{
                    "classification": classification_gruppo_id
                }],
                "other_names": []
            }
            # append all names
            if "othername__ACR" in latest_group:
                group["other_names"].append({
                    "othername_type": "ACR",
                    "name": latest_group["othername__ACR"],
                    "start_date": latest_group["founding_date"],
                    "end_date": latest_group["dissolution_date"]
                })
            for g in gruppi:
                group["other_names"].append({
                    "othername_type": "FOR",
                    "name": g["name"],
                    "start_date": g["founding_date"],
                    "end_date": g["dissolution_date"]
                })
                if "othername__ACR" in g:
                    group["other_names"].append({
                        "othername_type": "ACR",
                        "name": g["othername__ACR"],
                        "start_date": g["founding_date"],
                        "end_date": g["dissolution_date"]
                    })

            groups.append(group)

        return groups
