import logging

from popolo.models import Organization, Area, Classification
from taskmanager.management.base import LoggingBaseCommand

from project.core import labels_utils


class Command(LoggingBaseCommand):
    help = "Correct comuni organizations with no links to an Area or with no Giunta or Consiglio children"

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Start")

        comuni = Organization.objects.filter(classification='Comune', area__isnull=True)
        self.logger.info(f"{comuni.count()} comuni found with no link to an Area.")

        for c in comuni:
            name = c.name.replace("COMUNE DI ", "").replace("COMUNE", "").strip()
            a = None
            try:
                a = Area.objects.comuni().get(name__iexact=name)
            except Area.MultipleObjectsReturned:
                a = Area.objects.comuni().current().get(name__iexact=name)
            except Area.DoesNotExist:
                try:
                    a = Area.objects.comuni().get(other_names__name__iexact=name)

                except Area.DoesNotExist:
                    try:
                        a = Area.objects.comuni().get(i18n_names__name__iexact=name)
                    except Area.DoesNotExist:
                        self.logger.error(f" - {name}")
            finally:
                if a:
                    c.area = a
                    c.save()
                    self.logger.info(f"{c.name} => {a.identifier}")

        comuni = Organization.objects.filter(classification='Comune', children__isnull=True)
        self.logger.info(f"{comuni.count()} comuni found with no Giunta and/or Consiglio.")
        labels = {
            "area": "COM",
            "org": "Comune",
            "gov": "Giunta comunale",
            "gov_code": "3113",
            "ass": "Consiglio comunale",
            "ass_code": "3213",
        }
        for org in comuni:
            if org.area:
                # create assemblea
                try:
                    name = "{0} {1} {2}".format(
                        labels["ass"],
                        labels_utils.area_prep(org.area.name, labels["area"][0]),
                        org.area.name,
                    )
                    a, created = org.children.update_or_create(
                        classification=labels["ass"],
                        defaults={
                            "name": name,
                            "founding_date": org.founding_date,
                            "dissolution_date": org.dissolution_date,
                            "end_reason": org.end_reason,
                        },
                    )
                    if created:
                        self.logger.info("Assemblea created: {0}".format(a))
                        c, c_created = Classification.objects.get_or_create(
                            scheme="FORMA_GIURIDICA_OP",
                            descr=labels["ass"],
                            code=labels["ass_code"],
                        )
                        a.classifications.get_or_create(classification=c)
                    else:
                        self.logger.info("Assemblea updated: {0}".format(a))
                    a.add_classification("CONTESTO_OP", descr="OPDM")

                except Exception as e:
                    self.logger.error("Error {0} while processing {1}".format(e, org))

                # create giunta
                try:
                    name = "{0} {1} {2}".format(
                        labels["gov"],
                        labels_utils.area_prep(org.area.name, labels["area"][0]),
                        org.area.name,
                    )
                    g, created = org.children.update_or_create(
                        classification=labels["gov"],
                        defaults={
                            "name": name,
                            "founding_date": org.founding_date,
                            "dissolution_date": org.dissolution_date,
                            "end_reason": org.end_reason,
                        },
                    )
                    if created:
                        self.logger.info("Giunta created: {0}".format(g))
                        c, c_created = Classification.objects.get_or_create(
                            scheme="FORMA_GIURIDICA_OP",
                            descr=labels["gov"],
                            code=labels["gov_code"],
                        )
                        g.classifications.get_or_create(classification=c)
                    else:
                        self.logger.info("Giunta updated: {0}".format(g))
                    g.add_classification("CONTESTO_OP", descr="OPDM")
                except Exception as e:
                    self.logger.error("Error {0} while processing {1}".format(e, org))
            else:
                self.logger.error(f"Area not associated to {org}")

        self.logger.info("End")
