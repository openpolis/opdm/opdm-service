# -*- coding: utf-8 -*-
from django.conf import settings
import requests
from taskmanager.management.base import LoggingBaseCommand

from project.tasks.etl.composites import CSVDiffCompositeETL
from project.tasks.etl.extractors import UAProxyCSVExtractor
from project.tasks.etl.loaders.persons import PopoloPersonMembershipLoader
from project.tasks.etl.transformations.minint2opdm import (
    CurrentMinintReg2OpdmTransformation,
    CurrentMinintProv2OpdmTransformation,
    CurrentMinintMetro2OpdmTransformation,
    CurrentMinintCom2OpdmTransformation,
    CurrentMinintComm2OpdmTransformation,
)

USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) " \
             "AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36"

HTTP_PROXIES = {
    'http': f'http://{settings.HTTP_PROXY_USERNAME}:{settings.HTTP_PROXY_PASSWORD}@'
            f'{settings.HTTP_PROXY_HOST}:{settings.HTTP_PROXY_HOST_PORT}',
    'https': f'http://{settings.HTTP_PROXY_USERNAME}:{settings.HTTP_PROXY_PASSWORD}@'
             f'{settings.HTTP_PROXY_HOST}:{settings.HTTP_PROXY_HOST_PORT}',
}


class Command(LoggingBaseCommand):
    help = "Import Persons and Memberships from current MinInt source"

    def add_arguments(self, parser):
        parser.add_argument(
            "--context",
            dest="context",
            default="reg",
            help="The context (reg|regione|regioni|prov|provincia|cm|metropoli|com|comune|comuni|comm|"
                 "commissario|commissari).",
        )
        parser.add_argument(
            "--loc-code",
            dest="loc_code",
            help="Limits import to those records matching this. May use the '%%' as jolly (as in --loc-code=03%%).",
        )
        parser.add_argument(
            "--loc-desc",
            dest="loc_desc",
            help="Limits import to those records matching this. May use the '%%' as jolly (as in --loc-desc=Fubine%%).",
        )
        parser.add_argument(
            "--persons-update-strategy",
            dest="persons_update_strategy",
            default="overwrite_minint_opdm",
            help="Whether to keep old values or to overwrite them "
                 "for Person updates (keep_old | overwrite | overwrite_minint_opdm), defaults to overwrite_minint_opdm",
        )
        parser.add_argument(
            "--memberships-update-strategy",
            dest="memberships_update_strategy",
            default="overwrite_minint_opdm",
            help="Whether to keep old values or to overwrite them "
                 "for Membership updates (keep_old | overwrite | overwrite_minint_opdm), defaults to "
                 "overwrite_minint_opdm",
        )
        parser.add_argument(
            "--local-cache-path",
            dest="local_cache_path",
            default=settings.IMPORT_CACHE_PATH,
            help="Where downloaded file should be stored for caching and diffing",
        )
        parser.add_argument(
            "--local-out-path",
            dest="local_out_path",
            default=settings.IMPORT_OUT_PATH,
            help="Where produced file should be stored for successive manual processing",
        )
        parser.add_argument(
            "--clear-cache",
            dest="clear_cache",
            action="store_true",
            help="Whether to clear the cached file, and re-import from scratch",
        )

    @staticmethod
    def get_csv_url(context):
        """Return correct URL for context

        :param context:
        :return:
        """

        csv_url = "https://dait.interno.gov.it/documenti/amm{0}.csv".format(context)

        res = requests.head(
            csv_url,
            proxies=HTTP_PROXIES,
            headers={
                "User-agent": USER_AGENT
            },
        )
        if res.status_code == 200:
            return csv_url
        else:
            raise Exception("Error while fetching data from {0}: {1}".format(csv_url, res.reason))

    def handle(self, *args, **options):

        self.setup_logger(__name__, formatter_key="simple", **options)

        context = options["context"]
        persons_update_strategy = options["persons_update_strategy"]
        memberships_update_strategy = options["memberships_update_strategy"]

        if context in ["reg", "regione", "regioni"]:
            transformation_class = CurrentMinintReg2OpdmTransformation
            url_context = "reg"
            unique_idx_cols = (3, 4, 6, 7, 8, 9)
        elif context in ["prov", "provincia", "province"]:
            transformation_class = CurrentMinintProv2OpdmTransformation
            url_context = "prov"
            unique_idx_cols = (5, 6, 8, 9, 10, 11)
        elif context in ["cm", "metro", "metropoli"]:
            transformation_class = CurrentMinintMetro2OpdmTransformation
            url_context = "metropolitani"
            unique_idx_cols = (5, 6, 8, 9, 10, 11)
        elif context in ["com", "comune", "comuni"]:
            transformation_class = CurrentMinintCom2OpdmTransformation
            url_context = "com"
            unique_idx_cols = (7, 8, 10, 11, 12, 13)
        elif context in ["comm", "commissario", "commissari"]:
            transformation_class = CurrentMinintComm2OpdmTransformation
            url_context = "com"
            unique_idx_cols = (7, 8, 10, 11, 12, 13)
        else:
            raise Exception("wrong context")

        csv_url = self.get_csv_url(url_context)

        loc_code = options.get("loc_code", None)
        loc_desc = options.get("loc_desc", None)
        local_cache_path = options["local_cache_path"]
        local_out_path = options["local_out_path"]
        clear_cache = options["clear_cache"]

        if loc_code or loc_desc:
            import hashlib
            m = hashlib.sha256()
            if loc_desc:
                m.update(bytes(str(loc_desc), 'utf8'))
            if loc_code:
                m.update(bytes(str(loc_code), 'utf8'))
            suffix = f"_{m.hexdigest()[:8]}"
        else:
            suffix = ""

        self.logger.info("Reading CSV from url: {0}".format(csv_url))

        self.logger.info("Starting ETL process")

        CSVDiffCompositeETL(
            extractor=UAProxyCSVExtractor(
                proxies=HTTP_PROXIES,
                headers={'User-agent': USER_AGENT},
                url=csv_url, sep=";", encoding="latin1", skiprows=2, na_values=["", "-"], keep_default_na=False
            ),
            transformation=transformation_class(loc_code_filter=loc_code, loc_desc_filter=loc_desc),
            loader=PopoloPersonMembershipLoader(
                context=context,
                persons_update_strategy=persons_update_strategy,
                memberships_update_strategy=memberships_update_strategy,
            ),
            local_cache_path=local_cache_path,
            local_out_path=local_out_path,
            filename=f"minint_{url_context}_current_{suffix}.csv",
            clear_cache=clear_cache,
            unique_idx_cols=unique_idx_cols,
            log_level=self.logger.level,
            logger=self.logger,
        )()

        self.logger.info("End of ETL process")
