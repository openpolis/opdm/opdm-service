import logging

from django.core.management import BaseCommand
from popolo.models import Area


class Command(BaseCommand):
    """This script shows how to extract a list of comuni active at a given date,
    """
    help = "Export areas with prov and istat id at given date"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            'date',
            nargs="?",
            help='The date of extraction, format YYYY-MM-DD.',
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        d = options['date']

        self.logger.info("Start")

        for com in Area.historic_objects.comuni_with_prov_and_istat_identifiers(d):
            self.logger.info("{0}: {1} ({2}), ISTAT_ID: {3}".format(
                com.id, com.name, com.prov_identifier, com.istat_identifier
            ))
        self.logger.info("End")
