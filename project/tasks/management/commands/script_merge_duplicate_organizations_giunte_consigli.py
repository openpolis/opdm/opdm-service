import logging

from popolo.models import Organization, Post, Classification, Person
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Merge duplicate giunte and consigli"

    def add_arguments(self, parser):
        parser.add_argument(
            "--context",
            dest="context",
            default="reg",
            help="The context (reg|regione|regioni|prov|pro|province|provincie|com|comune|comuni).",
        )

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.setup_logger(__name__, formatter_key="simple", **options)

        context = options['context'][:3]
        self.logger.info("Start")
        getattr(self, 'handle_{0}'.format(context))()
        self.logger.info("End")

    def handle_com(self):

        parameters_list = [
            {'ok_name': 'Comune di Brescia', 'ko_name': 'Associazione comuni bresciani',
             'parent_cl': 'Altro ente pubblico non economico'},
            {'ok_name': 'Comune di Bozzolo', 'ko_name': 'Unione di comuni lombarda terra dei Gonzaga',
             'parent_cl': 'Altro ente pubblico non economico'},
            {'ok_name': 'Comune di Perfugas', 'ko_name': 'Comune Perfugas'},
            {'ok_name': 'Comune di Laces', 'ko_name': 'Comune di Laces gemeinde Latsch'},
            {'ok_name': 'Comune di Rifiano', 'ko_name': 'Comune di Caines'},
        ]

        for parameters in parameters_list:
            self.logger.info("Processing params: ok_name: {0}, ko_name: {1}".format(
                parameters['ok_name'], parameters['ko_name']
            ))
            for it in ['Consiglio comunale', 'Giunta comunale']:
                org_ko = Organization.objects.get(
                    classification=it,
                    parent__name__iexact=parameters['ko_name']
                )

                org_ok = Organization.objects.get(
                    classification=it,
                    parent__name__iexact=parameters['ok_name']
                )

                posts_ok = Post.objects.filter(organization=org_ok)
                posts_ko = Post.objects.filter(organization=org_ko)

                # find correct Post instance and copy roles (avoid duplication)
                for post_ko in posts_ko:
                    self.logger.info("Copying memberships for: {0}".format(post_ko))
                    for m in post_ko.memberships.all():
                        p = m.person
                        p.add_role(
                            post=posts_ok.get(role=post_ko.role),
                            label=m.label, role=m.role,
                            start_date=m.start_date, end_date=m.end_date
                        )

                # remove wrong organization and post
                posts_ko.delete()

                if 'parent_cl' in parameters:
                    ko_org_parent = org_ko.parent
                    ko_org_parent.classification = parameters['parent_cl']
                    ko_org_parent.save()

                    org_cl_ko = ko_org_parent.classifications.get(classification__scheme='FORMA_GIURIDICA_OP')
                    org_cl_ko.classification = Classification.objects.get(
                        scheme='FORMA_GIURIDICA_OP',
                        descr=parameters['parent_cl']
                    )
                    org_cl_ko.save()
                    org_ko.delete()

        for ko_name in ['Comune Perfugas', 'Comune di Laces gemeinde Latsch']:
            Organization.objects.get(classification='Comune', name__iexact=ko_name).delete()

        for o in Organization.objects.get(classification="Comune", name__iexact="Comune di caines").children.all():
            o.name = o.name.replace("Rifiano/Riffian", "Caines")
            o.save()

    def handle_pro(self):

        def merge_organizations(o0, o1):
            """Merge organization o1 into organization o0,
            then remove organization o1.

            :param o0: organization to keep
            :param o1: organization to remove
            :return:
            """
            self.logger.info("Merging {1} => {0}".format(o0, o1))

            if o1.name.lower() != o0.name.lower():
                o0.add_other_name(o1.name)

            for c in o1.contact_details.values():
                c.pop("id")
                try:
                    r = o0.add_contact_detail(**c)
                    if r:
                        self.logger.info("  {0} added".format(r))
                except Exception as e:
                    self.logger.error("  Errore: {0}".format(e))

            for i in o1.identifiers.values():
                i.pop("id")
                try:
                    r = o0.add_identifier(**i)
                    if r:
                        self.logger.info("  {0} added".format(r))
                except Exception as e:
                    self.logger.error("  Errore: {0}".format(e))

            for c in o1.classifications.values():
                try:
                    r = o0.add_classification_rel(c["classification_id"])
                    if r:
                        self.logger.info("  {0} added".format(r))
                except Exception as e:
                    self.logger.error("  Errore: {0}".format(e))

            for i in o1.links.values("link__url", "link__note"):
                try:
                    r = o0.add_link(
                        url=i.get("link__url"), note=i.get("link__note", "")
                    )
                    if r:
                        self.logger.info("{0} added".format(r))
                except Exception as e:
                    self.logger.error("Errore: {0}".format(e))

            for i in o1.sources.values("source__url", "source__note"):
                try:
                    r = o0.add_source(
                        url=i.get("source__url"), note=i.get("source__note", "")
                    )
                    if r:
                        self.logger.info("{0} added".format(r))
                except Exception as e:
                    self.logger.error("Errore: {0}".format(e))

            # merge memberships, by adding roles to persons, in order to avoid duplications
            for m in o1.memberships.values():
                try:
                    post = Post.objects.get(role=m['role'], organization=o0)
                except Post.DoesNotExist:
                    self.logger.warning(
                        "Could not find post for role {0} in organization {1}".format(m['role'], o0)
                    )
                    continue
                person = Person.objects.get(id=m['person_id'])
                try:
                    m.pop("id")
                    m.pop("organization_id")
                    m.pop("person_id")
                    m.pop("slug")
                    r = person.add_role(post, **m)
                    if r:
                        self.logger.info("{0} added".format(r))
                except Exception as e:
                    self.logger.error("Errore: {0}".format(e))

            # remove duplicates after info were copied
            o1.delete()

        # merge duplicate organizations, no identifiers available for public institutions
        c_bz = Organization.objects.get(id=28589)

        try:
            c_bz_2 = Organization.objects.get(id=35058)
            merge_organizations(c_bz, c_bz_2)
        except Organization.DoesNotExist:
            self.logger.error("Could not find Consiglio provinciale BZ to merge (id=35058). Probably already merged.")

        c_tn = Organization.objects.get(id=31699)
        try:
            c_tn_2 = Organization.objects.get(id=35004)
            merge_organizations(c_tn, c_tn_2)
        except Organization.DoesNotExist:
            self.logger.error("Could not find Consiglio provinciale TN to merge (id=35004). Probably already merged.")

    def handle_reg(self):

        # assign region as parent to consiglio currently missing them
        for reg in ['friuli', 'trentino', 'aosta']:
            self.logger.info("Assigning region's parent to consiglio of {0}".format(reg))
            r = Organization.objects.get(name__icontains=reg, classification="Regione")
            c = Organization.objects.get(name__icontains=reg, classification="Consiglio regionale")
            c.parent = r
            c.save()
