from datetime import datetime

from django.db.models import F, Q
from popolo.models import Organization
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Correct overlapping Sindaci roles in giunte comunali, by modifying crossing end_dates"
    dry_run = None

    def add_arguments(self, parser):  # noqa: D102
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="dry_run",
            help="Only show modifications do not save.",
        )

        parser.add_argument(
            "--start-year",
            dest="start_year",
            type=int, default=1990,
            help="Years to start considering memberships for corrections."
        )

        parser.add_argument(
            "--context",
            dest="context",
            help="The context to apply correctons [com|prov|reg].",
        )

        parser.add_argument(
            "--days-threshold",
            dest="days_threshold",
            type=int, default=30,
            help="Days above which end dates are not corrected and a warning is signaled."
        )

    def check_dates(self, giunta, role, days_threshold=30):

        date_apicals = list(
                giunta.memberships.filter(
                    role=role
                ).order_by(
                    'start_date', F('end_date').asc(nulls_last=True)
                )
        )

        date_fmt = '%Y-%m-%d'

        for i in range(1, len(date_apicals)):
            curr = date_apicals[i]
            prev = date_apicals[i-1]
            if prev.end_date is None:
                self.logger.error(f"{prev} is not the last result")
                return
            if prev and curr and prev.end_date > curr.start_date:
                self.logger.debug(f"{giunta}")

                ddays = (
                    datetime.strptime(prev.end_date, date_fmt) - datetime.strptime(curr.start_date, date_fmt)
                ).days
                if ddays < days_threshold:
                    if not self.dry_run:
                        prev.end_date = curr.start_date
                        prev.save()
                        self.logger.debug(f"OK {prev.end_date} => {curr.start_date}")
                    else:
                        self.logger.debug(
                            f"  {giunta}/{prev.id} dal {prev.start_date} al {prev.end_date} overlaps"
                            f" {curr.id}/{curr.role} dal {curr.start_date} al {curr.end_date}"
                            f" of {ddays} days"
                            f" - next election date: {curr.electoral_event.start_date if curr.electoral_event else '-'}"
                        )

                else:
                    self.logger.warning(
                        f"  {giunta}/{prev.id} dal {prev.start_date} al {prev.end_date} overlaps"
                        f" {curr.id}/{curr.role} dal {curr.start_date} al {curr.end_date}"
                        f" of more than {days_threshold} days ({ddays})"
                        f" - next election date: {curr.electoral_event.start_date if curr.electoral_event else '-'}"
                    )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key='simple', **options)

        self.dry_run = options["dry_run"]

        start_year = options["start_year"]
        if start_year < 1990 or start_year > 2018:
            raise Exception("Need to use a start_year in the 1990-2018 interval")

        context = options["context"]
        if not context or context.lower() not in ["com", "reg", "prov"]:
            raise Exception("Need to use a context, among 'com', 'prov', 'reg'")
        if context.lower() == 'reg':
            classification = "Giunta regionale"
            role = "Presidente di Regione"
        elif context.lower() == 'prov':
            classification = "Giunta provinciale"
            role = "Presidente di Provincia"
        else:  # com is the only possibility left at this point
            classification = "Giunta comunale"
            role = "Sindaco"

        days_threshold = options["days_threshold"]

        self.logger.info("Start")

        giunte = Organization.objects.filter(
            classification=classification
        ).filter(
            Q(end_date__gte=start_year) | Q(end_date__isnull=True)
        )
        n_giunte = giunte.count()
        for k, giunta in enumerate(giunte, start=1):
            if k % 500 == 0:
                self.logger.info(f"{k}/{n_giunte}")
            self.check_dates(giunta, role, days_threshold)

        self.logger.info("Finished")
