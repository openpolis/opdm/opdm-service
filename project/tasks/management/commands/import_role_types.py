# -*- coding: utf-8 -*-
import logging

from django.core.management.base import BaseCommand
from ooetl import ETL
from ooetl.extractors import CSVExtractor

from project.tasks.etl.loaders.persons import PopoloRoleTypeLoader


class RoleTypes2PopoloETL(ETL):
    """Instance of ``ooetl.ETL`` class that handles
    importing role_types from csv into Popolo
    """

    def transform(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns.

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original dataframe
        od = self.original_data.copy()

        # convert priority into an int
        od["priority"] = od["priority"].astype(int)

        # store processed data into the ETL instance
        self.processed_data = od

        # return ETL instance
        return self


class Command(BaseCommand):
    help = "Import role types from google sheet"

    source_csv_url = (
        "https://docs.google.com/spreadsheets/u/2/d/"
        "1kfhF3cpWOv4FdTq5krtvnFSskX10_6D-vwNLJaUDL3M/export"
        "?format=csv&id=1kfhF3cpWOv4FdTq5krtvnFSskX10_6D-vwNLJaUDL3M&gid=0"
    )
    local_csv_url = (
        "file:///home/gu/Workspace/opdm-service/resources/data/role_types.csv"
    )
    csv_url = source_csv_url

    logger = logging.getLogger(__name__)

    csv_extractor = CSVExtractor(
        csv_url,
        sep=",",
        encoding="utf8",
        na_values=["", "-"],
        keep_default_na=False,
    )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.logger.info("Start records creation")

        etl = RoleTypes2PopoloETL(
            extractor=self.csv_extractor,
            loader=PopoloRoleTypeLoader(),
            log_level=self.logger.level,
        )
        self.logger.info("Starting ETL process")
        etl.etl()
        self.logger.info("End")
