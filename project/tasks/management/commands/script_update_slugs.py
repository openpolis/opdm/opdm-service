# -*- coding: utf-8 -*-
from autoslug import AutoSlugField
from django.utils.text import slugify
from taskmanager.management.base import LoggingBaseCommand
from autoslug import utils
from popolo.behaviors.models import get_slug_source
from popolo.models import Person, Organization, Membership

slug_field = AutoSlugField(
    name='slug', populate_from=get_slug_source,
    max_length=255, unique=True, slugify=slugify
)


class Command(LoggingBaseCommand):
    help = "Update slugs values for Permalinkable objects"

    def add_arguments(self, parser):
        parser.add_argument(
            'object_types', metavar='T', type=str, nargs='*',
            help='The type of objects [person, organisation, membership]'
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.logger.info("Start")

        object_types = [o.lower() for o in options['object_types']]

        if 'person' in object_types or not len(object_types):
            self._correct_missing_slugs(
                Person.objects.filter(slug__istartswith='none-'),
                label='persons', manager=Person.objects
            )
        if 'organisation' in object_types or not len(object_types):
            self._correct_missing_slugs(
                missing_slugs_qs=Organization.objects.filter(slug__istartswith='none-'),
                label='organizations', manager=Organization.objects
            )
        if 'membership' in object_types or not len(object_types):
            self._correct_missing_slugs(
                missing_slugs_qs=Membership.objects.filter(slug__istartswith='none-'),
                label='organizations', manager=Organization.objects
            )
        self.logger.info("End")

    def _correct_missing_slugs(self, missing_slugs_qs, label, manager):
        self.logger.info(f"Processing {missing_slugs_qs.count()} {label}")
        for instance in missing_slugs_qs:
            value = get_slug_source(instance)
            slug = slugify(value)
            instance.slug = utils.generate_unique_slug(slug_field, instance, slug, manager)
            instance.save()
            self.logger.info(f"{value:40s} => {instance.slug}")
