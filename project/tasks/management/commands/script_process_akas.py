from django.conf import settings
from django.db import transaction
from taskmanager.management.base import LoggingBaseCommand

from project.akas.models import AKA
from project.core import person_utils


class Command(LoggingBaseCommand):
    help = (
        "Process all AKAs, as if it was a new import operation. Remove AKAs, then re-create it if needed."
        "Useful whenever criterion for computing AKAs change."
    )

    persons_update_strategy = None
    memberships_update_strategy = None
    check_membership_label = None
    aka_lo = None
    aka_hi = None
    context_filter = None

    def add_arguments(self, parser):
        parser.add_argument(
            "--persons-update-strategy",
            dest="persons_update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them "
            "for Person updates (keep_old | overwrite | overwrite_minint_opdm), defaults to overwrite_minint_opdm",
        )
        parser.add_argument(
            "--memberships-update-strategy",
            dest="memberships_update_strategy",
            default="overwrite_minint_opdm",
            help="Whether to keep old values or to overwrite them "
            "for Membership updates (keep_old | overwrite | overwrite_minint_opdm), defaults to overwrite_minint_opdm",
        )
        parser.add_argument(
            "--check-membership-label",
            dest="check_membership_label",
            action="store_true",
            help="Whether to check membership labels when updating",
        )
        parser.add_argument(
            "--aka-lo",
            dest="aka_lo",
            type=int,
            default=settings.AKA_LO_THRESHOLD,
            help="Threshold below which a similarity is discarded",
        )
        parser.add_argument(
            "--aka-hi",
            dest="aka_hi",
            type=int,
            default=settings.AKA_HI_THRESHOLD,
            help="Threshold above which a similarity is considered identical",
        )
        parser.add_argument(
            "--context-filter",
            dest="context_filter",
            help="Only process similarities for specified context (ie: atoka)",
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.logger.info("Start")

        self.persons_update_strategy = options["persons_update_strategy"]
        self.memberships_update_strategy = options["memberships_update_strategy"]
        self.check_membership_label = options["check_membership_label"]
        self.aka_lo = options["aka_lo"]
        self.aka_hi = options["aka_hi"]
        self.context_filter = options.get("context_filter", None)

        akas_qs = AKA.objects.filter(is_resolved=False)
        if self.context_filter:
            akas_qs = akas_qs.filter(
                loader_context__context__icontains=self.context_filter
            )
        n_akas = akas_qs.count()
        for n, aka in enumerate(akas_qs.all(), start=1):
            if n % 10 == 0:
                self.logger.info("{0}/{1}".format(n, n_akas))
            self.reload_aka(aka)

        self.logger.info("End")

    @transaction.atomic()
    def reload_aka(self, aka: AKA) -> None:
        """remove existing aka and re-try lookup + update/insert of person's information

        The operation is transactional.

        :param aka: aka to be removed, where contextual info are taken for a new look-upsert
        :return: None

        """
        aka_id = aka.id
        aka_loader_context = aka.loader_context
        item = aka_loader_context["item"]

        try:
            with transaction.atomic():

                # remove aka (it will be recreated by the instructions below)
                aka.delete()

                # lookup person and return id (0 if not found, < 0 if similarities were found)
                # re-create AKA in the process
                person_id = person_utils.person_lookup(
                    item, "anagraphical", aka_loader_context,
                    aka_lo=self.aka_lo, aka_hi=self.aka_hi,
                    logger=self.logger
                )

                # add person and membership only if no similarities were found
                if person_id >= 0:
                    p, created = person_utils.update_or_create_person_and_details_from_item(
                        person_id,
                        item,
                        loader_context=aka_loader_context,
                        persons_update_strategy=None,
                        memberships_update_strategy=self.memberships_update_strategy,
                        check_membership_label=self.check_membership_label,
                        logger=self.logger,
                    )

                    if created:
                        self.logger.debug("    {0} created".format(p))
                    else:
                        self.logger.debug("    {0} updated".format(p))
        except Exception as e:
            self.logger.error("Error {0} during reloading of aka {1}".format(e, aka_id))
