# coding=utf-8

import logging
import pathlib

from django.apps import apps
from django.core.management.base import BaseCommand
from django.db import models
from django.utils.translation import ugettext_lazy as _


class Command(BaseCommand):
    help = "Custom django-admin command"
    requires_migrations_checks = True
    requires_system_checks = True
    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            "django_apps",
            metavar="DJANGO_APPS",
            type=str,
            nargs="+",
            help="List of Django apps to be compiled.",
        )
        parser.add_argument(
            "--sort-fields",
            "-s",
            action="store_true",
            dest="sort_fields",
            default=False,
            help="Sort class fields alphabetically.",
        )
        parser.add_argument(
            "--no-semicolons",
            "-ns",
            action="store_false",
            dest="add_semicolons",
            default=True,
            help="Don't add semicolons.",
        )
        parser.add_argument(
            "--no-comments",
            "-nc",
            action="store_false",
            dest="add_comments",
            default=True,
            help="Don't generate comments.",
        )
        parser.add_argument(
            "--dry-run",
            action="store_true",
            dest="dry_run",
            default=False,
            help=" Don't write files. Just print result to stdout.",
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)
        try:
            django_models = list()
            for app in options["django_apps"]:
                django_models.extend(
                    [model for model in apps.get_app_config(app).get_models()]
                )

            results = DjangoToTypescriptCompiler(
                django_models=django_models,
                add_semicolons=options["add_semicolons"],
                add_comments=options["add_comments"],
                sort_fields=options["sort_fields"],
            ).build()

            if options["dry_run"]:
                separator_ln = "-" * 80 + "\n"
                print(separator_ln.join([r[1] for r in results]))
            else:
                out_path = "resources/models"
                pathlib.Path(out_path).mkdir(parents=True, exist_ok=True)
                for (model_name, model_buffer) in results:
                    with open(
                        "{path}/{file}".format(path=out_path, file=model_name), "w+"
                    ) as f:
                        f.write(model_buffer)

        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return


class DjangoToTypescriptCompiler:
    type_mapping = {
        "AutoField": "number",
        "BooleanField": "boolean",
        "CharField": "string",
        "URLField": "string",
        "TextField": "string",
        "DecimalField": "number",
        "IntegerField": "number",
        "PositiveIntegerField": "number",
        "DateTimeField": "string",
        "SlugField": "string",
    }

    def __init__(
        self,
        django_models: list,
        indentation: int = 4,
        add_comments: bool = True,
        add_semicolons: bool = True,
        sort_fields: bool = True,
    ):
        self.indentation = indentation
        self.add_comments = add_comments
        self.add_semicolons = add_semicolons
        self.sort_fields = sort_fields
        self._sc = ";" if self.add_semicolons else ""
        self._li = " " * self.indentation
        self.django_models = django_models

    def build(self):
        result = []
        for model in self.django_models:
            buffer = []
            model_fields = self._get_model_fields(model)
            if self.sort_fields:
                model_fields = sorted(model_fields, key=lambda f: f.name)

            # BEGIN BLOCK: Import statements
            # ---------------------------------------------------------------

            for field in model_fields:
                if (
                    hasattr(field, "related_model")
                    and field.related_model in self.django_models
                    and model is not field.related_model
                ):
                    buffer.append(
                        "import {{ {0} }} from './{1}'{sc}\n".format(
                            field.related_model.__name__,
                            field.related_model.__name__.lower(),
                            sc=self._sc,
                        )
                    )
                    buffer = list(set(buffer))  # Remove duplicate import statements
            if buffer:
                buffer.append("\n")
            # END BLOCK: Import statements
            # ---------------------------------------------------------------

            # BEGIN BLOCK: Class definition
            # ---------------------------------------------------------------
            buffer.append("export class {0} {{\n".format(model.__name__))

            primary_key = primary_key_type = None
            constructor_stmts = []

            # Generate class attributes declarations
            for field in model_fields:
                ts_type = self._get_ts_type(field)
                if ts_type is None:
                    continue

                var_declaration = "{field_name}: {ts_type}".format(
                    field_name=field.name, ts_type=ts_type
                )

                # Generate class attributes comments
                comment = (
                    self._get_comment_ln(
                        field=field,
                        internal_type=field.get_internal_type(),
                        var_declaration=var_declaration,
                    )
                    if self.add_comments
                    else ""
                )

                buffer.append(
                    "{0}{var_declaration}{1}{comment}\n".format(
                        self._li,
                        self._sc,
                        var_declaration=var_declaration,
                        comment=comment,
                    )
                )
                if hasattr(field, "primary_key") and field.primary_key:
                    primary_key = field.name
                    primary_key_type = ts_type
                else:
                    if (
                        hasattr(field, "default")
                        and field.default is not models.fields.NOT_PROVIDED
                        and not callable(field.default)
                    ):
                        if ts_type == "string":
                            default_value = "'" + field.default + "'"
                        elif ts_type == "number":
                            default_value = field.default
                        else:
                            default_value = "null"
                    else:
                        if ts_type == "string":
                            default_value = "''"
                        elif ts_type == "number":
                            default_value = "null"
                        elif "[]" in ts_type:
                            default_value = "[]"
                        else:  # Perhaps it's an object...
                            default_value = "<{object_name}>{{}}".format(
                                object_name=ts_type
                            )
                    constructor_stmts.append(
                        "this.{field_name} = {value}".format(
                            field_name=field.name, value=default_value
                        )
                    )

            # BEGIN BLOCK: Class constructor
            # ---------------------------------------------------------------
            buffer.append(
                "\n"
                "{0}constructor({pk}?: {pk_type}) {{\n"
                "{0}{0}this.{pk} = {pk}{1}\n".format(
                    self._li, self._sc, pk=primary_key, pk_type=primary_key_type
                )
            )
            for a in constructor_stmts:
                buffer.append(
                    "{0}{0}{assignment}{1}\n".format(self._li, self._sc, assignment=a)
                )

            buffer.append("{0}}}\n".format(self._li))
            # END BLOCK: Class constructor
            # ---------------------------------------------------------------

            buffer.append("}\n\n")
            # END BLOCK: Class definition
            # ---------------------------------------------------------------

            result.append((self._get_model_name(model) + ".ts", "".join(buffer)))
        return result

    def _get_ts_type(self, field):
        ts_type = self.type_mapping.get(field.get_internal_type(), None)
        if hasattr(field, "is_relation") and field.is_relation:
            if (
                hasattr(field, "related_model")
                and field.related_model in self.django_models
            ):
                if hasattr(field, "many_to_many") and field.many_to_many:
                    ts_type = field.related_model.__name__ + "[]"
                elif hasattr(field, "one_to_many") and field.one_to_many:
                    ts_type = field.related_model.__name__ + "[]"
                elif hasattr(field, "many_to_one") and field.many_to_one:
                    ts_type = field.related_model.__name__
                elif hasattr(field, "one_to_one") and field.one_to_one:
                    ts_type = field.related_model.__name__
                else:
                    ts_type = None
            else:
                field.name += "_id"
        return ts_type

    def _get_model_fields(self, model: models.Model):
        """
        Get the fields of the specified Django model.
        Returns a list of fields exluding fields not explicitly declared.
        E.g: if the a field is an explicitly declared ForeignKey, defining a many-to-one relation,
        then include the field. If the field is automatically generated by Django, for example a ManyToOneRel,
        being the "-one part" of a many-to-one relation defined in another model, then exclude the field.

        :param model: The Django model we want to obtain the fields
        :type model: django.db.models.Model
        :return: The list of fields of the specified Django model
        :rtype: list
        """
        return [
            f
            for f in model._meta.get_fields()
            if (
                (f.name != self._get_model_name(model))
                and ("GenericForeignKey" not in str(type(f)))
                and ("ManyToManyRel" not in str(type(f)))
                and ("ManyToOneRel" not in str(type(f)))
                and ("OneToManyRel" not in str(type(f)))
                and ("OneToOneRel" not in str(type(f)))
                and ("reverse_related" not in f.get_internal_type())
            )
        ]

    @staticmethod
    def _get_model_name(model):
        return model._meta.model_name

    def _get_comment_ln(self, field, internal_type, var_declaration):
        comment = []
        if hasattr(field, "help_text") and field.help_text != "":
            comment.append("{help_text}".format(help_text=_(field.help_text)))
        if hasattr(field, "primary_key") and field.primary_key:
            comment.append("Primary key")
        if hasattr(field, "blank") and field.blank is False:
            comment.append("required")
        if "CharField" in internal_type:
            comment.append("max_length: {0}".format(field.max_length))
        if hasattr(field, "default") and field.default != models.fields.NOT_PROVIDED:
            comment.append("default: {0}".format(field.default))
        if internal_type == "ManyToManyField":
            comment.append("related model: {0}".format(field.related_model.__name__))
        if internal_type == "DateField":
            comment.append("DateField".format())

        def get_tab_length(length):
            if length > 0:
                if (length + len(var_declaration)) % self.indentation == 0:
                    return length
            length = length + 1
            return get_tab_length(length)

        comment_tab = get_tab_length(28 - len(var_declaration)) * " "
        return comment_tab + "// " + ", ".join(comment) if comment else ""
