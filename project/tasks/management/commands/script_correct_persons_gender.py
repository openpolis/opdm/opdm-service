import logging
import pandas as pd
import pathlib

from taskmanager.management.base import LoggingBaseCommand
from popolo.models import Person


class Command(LoggingBaseCommand):
    help = "Lookup correct genders in a file, and update gender information when wrong"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            "--duplicates-path",
            dest="duplicates_path",
            help="Duplicate names filepath",
            default="./data/nomi_duplicati.csv"
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Starting procedure")
        changed = 0

        gdn = pd.read_csv(pathlib.PurePath(options["duplicates_path"]))
        for dn in gdn.to_records():
            nome = dn["nome"]
            genere = dn["genere"]
            if nome != "ANDREA":
                rows = Person.objects.filter(given_name__iexact=nome).exclude(gender=genere).count()
                if rows:
                    # loop to save, in order to change CF through a post_save signal
                    for p in Person.objects.filter(given_name__iexact=nome).exclude(gender=genere):
                        p.gender = genere
                        p.save()
                    self.logger.info(f"{nome}: {rows} => {genere}")

                    changed += rows
        self.logger.info(f"Procedure finished. {changed} rows were changed.")
