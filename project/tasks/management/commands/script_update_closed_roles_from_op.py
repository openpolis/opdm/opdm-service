# -*- coding: utf-8 -*-
import logging
import re

from django.core.management import BaseCommand
from munch import munchify
from popolo.models import Person
import requests


class Command(BaseCommand):
    help = "Update closed roles from Openpolis API"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            "--updated-after",
            dest="updated_after",
            help="Only import memberships changed after this date",
        )

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.updated_after = options.get("updated_after", None)

        page_size = 50
        instcharges_url = "{0}/politici/instcharges.json".format(
            "http://api3.openpolis.it"
        )

        url_filters = ""
        if self.updated_after:
            url_filters += "&updated_after={0}".format(self.updated_after)

        url = "{0}?page_size={1}{2}".format(instcharges_url, page_size, url_filters)

        while True:
            self.logger.info("Fetching data from {0}".format(url))
            resp = requests.get(url)

            if resp.status_code == 200:
                data = munchify(resp.json())
                for op_post in data.results:
                    op_politician = op_post.politician
                    self.logger.info(
                        "PROCESSING: {0} {1} - {2} {3} - "
                        "{4} (from {5} to {6}) [updated@{7}]".format(
                            op_politician.first_name,
                            op_politician.last_name,
                            op_post.charge_type_descr,
                            op_post.institution_descr,
                            op_post.location_descr,
                            op_post.date_start,
                            op_post.date_end or "-",
                            op_post.content.content.updated_at,
                        )
                    )

                    try:
                        person = Person.objects.get(
                            identifiers__scheme="OP_ID",
                            identifiers__identifier=op_politician.content.id,
                        )

                    except Exception as e:
                        self.logger.error("Could not import person. {0}".format(e))
                        continue

                    op_location = re.sub(
                        r"^(.*) \(.*\)$", r"\1", op_post.location_descr
                    )

                    try:
                        m = person.memberships.get(
                            organization__parent__name__iexact=op_location,
                            organization__name__icontains=op_post.institution_descr,
                            start_date=op_post.date_start,
                        )
                        if m.end_date != op_post.date_end:
                            self.logger.info(
                                "... OPDM end_date: {0}, OPAPI end_date: {1}".format(
                                    m.end_date, op_post.date_end
                                )
                            )
                            m.end_date = op_post.date_end
                            m.save()
                    except Exception as e:
                        self.logger.error("... {0}".format(e))

            else:
                self.logger.error(
                    "Error {0}: {1} while fetching from url".format(
                        resp.status_code, resp.reason
                    )
                )
                break

            if data.next is None:
                break

            url = data.next

        self.logger.info("End COM")
