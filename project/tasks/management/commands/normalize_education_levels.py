from ooetl.extractors import CSVExtractor
from popolo.models import OriginalEducationLevel

from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Read normalization map from CSV and apply to model in DB"

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_url", help="Source of the CSV file (http[s]:// or file:///)"
        )

    def handle(self, *args, **options):
        self.setup_logger(__name__, formatter_key="simple", **options)

        self.logger.info("Start process")

        source_url = options["source_url"]

        self.logger.info("Extraction from JSON")
        mapping_df = CSVExtractor(
            source_url, sep=",",
            na_values=["", "-"],
            keep_default_na=False,
        ).extract()
        for mapping in mapping_df.to_dict('records'):
            oe = OriginalEducationLevel.objects.get(id=mapping['originaleducationlevel_id'])
            oe.normalized_education_level_id = mapping['educationlevel_id']
            oe.save()
            self.logger.info(
                "Original: {0} => normalized: {1}".format(
                    mapping['originaleducationlevel_id'], mapping['educationlevel_id']
                )
            )
        self.logger.info("End")
