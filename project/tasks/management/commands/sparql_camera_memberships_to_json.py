import re
import roman
from typing import Dict, Any, Optional, List

from popolo.models import Organization

from tasks.management.base import SparqlToJsonCommand
from tasks.parsing.common import parse_date, parse_identifier
from tasks.parsing.sparql.camera import get_person_from_binding
from tasks.parsing.sparql.utils import get_bindings, read_raw_rq


class Command(SparqlToJsonCommand):
    json_filename = "camera_memberships"

    def query(self, **options) -> Optional[List[Dict]]:
        return get_bindings(
            "http://dati.camera.it/sparql",
            re.sub(
                r"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_\d+>",
                f"<http://dati.camera.it/ocd/legislatura.rdf/repubblica_{options['legislature']}>",
                read_raw_rq("camera_memberships_17.rq"),
            ),
            method="POST"
        )

    def handle_bindings(self, bindings, **options) -> Optional[List[Dict]]:
        n_leg = options["legislature"]
        roman_leg = roman.toRoman(n_leg)

        org_camera = (
            Organization.objects.filter(identifier=f"ASS-CAMERA-{n_leg}")
            .get()
        )
        if not org_camera:
            self.logger.error(
                f"Organization ASS-CAMERA-{n_leg} does not exist, skipping..."
            )
            return

        buffer = {}

        for binding in bindings:
            tmp: Dict[str, Any] = buffer.get(
                binding["person__id"].value,
                {
                    **get_person_from_binding(binding),
                },
            )
            m = {
                "organization_id": org_camera.id
            }

            if "identifier" in binding:
                identifier = parse_identifier(binding["identifier"].value)
                if identifier:
                    tmp["identifiers"].append(identifier)

            # skip repeating memberships
            if sum(
                {'scheme': 'OCD-URI', 'identifier': binding["membership__id"].value} in x['identifiers']
                for x in tmp['memberships']
            ):
                continue

            m['role'] = "Deputato"
            m['label'] = f"Deputato nella {roman_leg} Legislatura"

            m['start_date'] = parse_date(binding["membership__start_date"].value)

            if "membership__end_date" in binding:
                tmp_end_date = parse_date(binding["membership__end_date"].value)
            else:
                tmp_end_date = None
            if not tmp_end_date:
                tmp_end_date = org_camera.dissolution_date
            m['end_date'] = tmp_end_date

            if "membership__end_reason" in binding:
                m["end_reason"] = binding["membership__end_reason"].value

            if "membership__id" in binding:
                m["identifiers"] = [
                    {"scheme": "OCD-URI", "identifier": binding["membership__id"].value}
                ]

            if "collegio" in binding:
                m['constituency_descr_tmp'] = binding["collegio"].value

            if "lista" in binding:
                m['electoral_list_descr_tmp'] = binding["lista"].value

            m['sources'] = [
                {
                    "note": "Open Data Camera",
                    "url": "http://dati.camera.it/",
                }
            ]
            tmp["memberships"].append(m)

            buffer[binding["person__id"].value] = tmp

        output = [
            {
                **values,
                "memberships": values["memberships"]
            }
            for persona, values in buffer.items()
        ]

        return output
