# -*- coding: utf-8 -*-
import copy
from datetime import datetime
import logging

from taskmanager.management.base import LoggingBaseCommand
from ooetl.transformations import Transformation

from project.tasks.etl.composites import JsonDiffCompositeETL
from project.tasks.etl.composites.diff_logics import (
    JsonOutRemovesDiffLogic, CloseItemRemovesDiffLogic,
    organization_key_from_dict,
)
from project.tasks.etl.extractors import JsonArrayExtractor
from project.tasks.etl.loaders.organizations import PopoloOrgLoader
from project.tasks.management.commands.mixins import CacheArgumentsCommandMixin


class ContextTransformation(Transformation):
    """Add context to data, as classification, before loading.
    """
    def __init__(self, context):
        self.context = context
        super(Transformation, self).__init__()

    def transform(self):
        """ Transform dataframe parsed from CSV,
        renaming columns, filtering non-used columns,
        removing empty lines, builnding usefule columns.

        :return: the ETL instance (to chain methods)
        """

        # get a copy of the original dataframe
        od = self.etl.original_data.copy()

        def transform_item(item, context):
            """Enrich each item with identifiers, contacts, classifications, area, sources, links, ...

            :param item:
            :param context: the opdm context to append to items to load
            :return:
            """
            processed_item = copy.deepcopy(item)

            # add context
            if "classifications" not in processed_item:
                processed_item["classifications"] = []
            processed_item["classifications"].append({"scheme": "CONTESTO_OP", "descr": context})

            return processed_item

        # define transformation for each item as a generator
        # to be passed along to the loader, without resolution
        # can safely assume od is a list, since it comes from JsonArrayExtractor
        od = (transform_item(item, self.context) for item in od)

        # store processed data into the ETL instance
        self.etl.processed_data = od

        # return ETL instance
        return self.etl


class Command(CacheArgumentsCommandMixin, LoggingBaseCommand):
    help = "Import Organizations from a remote or local json source"

    logger = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument(
            dest="source_url", help="Source of the JSON file (http[s]:// or file:///)"
        )
        parser.add_argument(
            "--update-strategy",
            dest="update_strategy",
            default="keep_old",
            help="Whether to keep old values or to overwrite them (keep_old | keep_old_overwrite_cf | overwrite), "
                 "defaults to keep_old",
        )
        parser.add_argument(
            "--lookup-strategy",
            dest="lookup_strategy",
            default="identifier",
            help="Whether to lookup using anagraphical (name and start date), "
            "identifier (see --identifier-scheme) or mixed (identifier first, then cascade to anagraphical). "
            "Add _current suffix to only lookup into currently active organizations (ie: mixed_current)",
        )
        parser.add_argument(
            "--close-missing",
            dest="close_missing",
            action="store_true",
            help="Whether to close organizations that were in the cache, but not in the live records.",
        )
        parser.add_argument(
            "--identifier-scheme",
            dest="identifier_scheme",
            default='CF',
            help="Which scheme to use with identifier/mixed lookup strategy",
        )
        parser.add_argument(
            "--private-company-verification",
            dest="private_company_verification",
            action='store_true',
            help="Enable verificaion of private orgs before anagraphical search in mixed strategy.",
        )
        parser.add_argument(
            "--log-step",
            dest="log_step",
            type=int,
            default=500,
            help="Number of steps to log process completion to stdout. Defaults to 500.",
        )
        parser.add_argument(
            "--opdm-context",
            dest="opdm_context",
            default="OPDM",
            help="OP_CONTESTO classification value to add to all loaded items"
        )

        super(Command, self).add_arguments(parser)
        self.add_arguments_cache(parser)

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)
        self.handle_cache(*args, **options)

        self.logger.info("Start loading procedure")

        update_strategy = options["update_strategy"]
        lookup_strategy = options["lookup_strategy"]
        close_missing = options["close_missing"]
        identifier_scheme = options["identifier_scheme"]
        private_company_verification = options['private_company_verification']
        source_url = options["source_url"]
        log_step = options["log_step"]
        opdm_context = options["opdm_context"]
        filename = source_url[source_url.rfind("/")+1:]

        if close_missing:
            removes_diff_logic = CloseItemRemovesDiffLogic(
                end_field='dissolution_date',
                end_date=datetime.strftime(datetime.now(), "%Y-%m-%d"),
                end_reason='record missing from source',
            )
        else:
            removes_diff_logic = JsonOutRemovesDiffLogic(local_out_path=self.local_out_path)

        JsonDiffCompositeETL(
            extractor=JsonArrayExtractor(source_url),
            transformation=ContextTransformation(opdm_context),
            loader=PopoloOrgLoader(
                update_strategy=update_strategy,
                lookup_strategy=lookup_strategy,
                identifier_scheme=identifier_scheme,
                private_company_verification=private_company_verification,
                log_step=log_step
            ),
            key_getter=organization_key_from_dict,
            local_cache_path=self.local_cache_path,
            local_out_path=self.local_out_path,
            filename=filename,
            clear_cache=self.clear_cache,
            logger=self.logger,
            log_level=self.logger.level,
            removes_diff_logic=removes_diff_logic
        )()
        self.logger.info("End loading procedure")
