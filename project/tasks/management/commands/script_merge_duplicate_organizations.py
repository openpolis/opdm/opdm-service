import logging

from django.db import transaction
from django.db.models import Count
from popolo.models import Organization
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Merge duplicate organizations, removing those created sooner."

    logger = logging.getLogger(__name__)

    def handle(self, *args, **options):
        verbosity = options["verbosity"]
        if verbosity == 0:
            self.logger.setLevel(logging.ERROR)
        elif verbosity == 1:
            self.logger.setLevel(logging.WARNING)
        elif verbosity == 2:
            self.logger.setLevel(logging.INFO)
        elif verbosity == 3:
            self.logger.setLevel(logging.DEBUG)

        self.setup_logger(__name__, formatter_key="simple", **options)

        self.logger.info("Start")

        double_identifiers = Organization.objects.filter(dissolution_date__isnull=True).values('identifier').annotate(
            n=Count('identifier')).order_by('-n').filter(n__gt=1).values_list('identifier', flat=True)

        for identifier in double_identifiers:
            orgs = Organization.objects.filter(
                identifier=identifier, dissolution_date__isnull=True
            ).order_by("created_at")
            self.logger.info(
                "{0} => {1}".format(identifier, ", ".join(["{0} ({1})".format(org.id, org.created_at) for org in orgs]))
            )

            try:
                with transaction.atomic():

                    o0 = orgs[0]  # Organization to remove
                    o1 = orgs[1]  # Organization to merge

                    if o1.name != o0.name:
                        o1.add_other_name(o0.name)

                    for c in o0.contact_details.values():
                        c.pop("id")
                        r = o1.add_contact_detail(**c)
                        if r:
                            self.logger.info("  {0} added".format(r))

                    for i in o0.identifiers.values():
                        i.pop("id")
                        r = o1.add_identifier(**i)
                        if r:
                            self.logger.info("  {0} added".format(r))

                    for c in o0.classifications.values():
                        r = o1.add_classification_rel(c["classification_id"])
                        if r:
                            self.logger.info("  {0} added".format(r))

                    for i in o0.links.values("link__url", "link__note"):
                        r = o1.add_link(
                            url=i.get("link__url"), note=i.get("link__note", "")
                        )
                        if r:
                            self.logger.info("{0} added".format(r))

                    for i in o0.sources.values("source__url", "source__note"):
                        r = o1.add_source(
                            url=i.get("source__url"), note=i.get("source__note", "")
                        )
                        if r:
                            self.logger.info("{0} added".format(r))

                    # merge memberships, by adding roles to persons, in order to avoid duplications
                    for m in o0.memberships.all():
                        post = m.post
                        post.organization = o1
                        post.save()
                        self.logger.info(f"Post {post} merged")

                        m.organization = o1
                        m.save()
                        self.logger.info(f"Membership {m} merged")

                    # merge ownerships
                    for ow in o0.ownerships.all():
                        ow.owner_organization = o1
                        ow.save()
                        self.logger.info(f"Ownership {ow} merged")

                    # merge ownerships (as in owned)
                    for ow in o0.ownerships_as_owned.all():
                        ow.owned = o1
                        ow.save()
                        self.logger.info(f"Ownership as in owned {ow} merged")

                    # remove duplicates after info were copied
                    self.logger.info(f"Organization {o0} removed (was merged into {o1})")
                    o0.delete()

            except Exception as e:
                self.logger.error(f"{e} while merging {o0} into {o1}")

        self.logger.info("End")
