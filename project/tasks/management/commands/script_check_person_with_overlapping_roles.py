# coding=utf-8
import pandas as pd
import numpy as np
import datetime as dt
import math

from django.db.models import F
from popolo.models import Person, Membership
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    """
    Loop over all personal roles to see if some overlapping ones are found
    """

    help = "Check overlapping roles for persons"
    epoch = dt.datetime(1970, 1, 1)

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)

    @classmethod
    def detect_overlap(cls, a, b):
        if a is not np.nan and b is not np.nan:
            _max = max(a[0], b[0])
            _min = min(a[1], b[1])
            if _max < _min:
                return (_max, _min)
        return ()

    @classmethod
    def detect_overlaps(cls, g):
        s = (pd.to_datetime(g['m_start_date']) - cls.epoch).dt.days
        e = (pd.to_datetime(g['m_end_date']) - cls.epoch).dt.days
        g = g.assign(start_days=s, end_days=e)

        def generate_range(row):
            if not math.isnan(row['start_days']):
                r_start = int(row['start_days'])
            else:
                r_start = -999999
            if not math.isnan(row['end_days']):
                r_end = int(row['end_days'])
            else:
                r_end = 999999
            return (r_start, r_end)

        r = g.apply(lambda x: generate_range(x), axis=1)
        g = g.assign(range=r, shiftrange=r.shift())

        overlap = g.apply(
            lambda x: cls.detect_overlap(x['range'], x['shiftrange']),
            axis=1
        )
        g = g.assign(overlap=overlap)
        del g['range'], g['shiftrange'], g['start_days'], g['end_days']

        return g[g.overlap != ()]

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        self.logger.info("Start of ETL process")

        v = list(
            Person.objects.filter(memberships__isnull=False).annotate(
                p_id=F('id'),
                o_id=F('memberships__organization_id'),
                m_start_date=F('memberships__start_date'), m_end_date=F('memberships__end_date'),
                m_role=F('memberships__role'),
                m_label=F('memberships__label')
            ).order_by(
                'p_id', 'o_id', 'm_role', 'm_start_date', 'm_end_date'
            ).values(
                'p_id', 'o_id', 'm_start_date', 'm_end_date', 'm_role', 'm_label'
            )
        )

        df = pd.DataFrame(v)

        self.logger.info("Grouping rows by person_id, organization_id, role")
        gdf = df.groupby(['p_id', 'o_id', 'm_role', 'm_label'])  # ~ 589K rows

        self.logger.info("Removing groups with just one row ...")
        mdf = gdf.filter(lambda x: len(x) > 1)  # ~ 250K rows (takes few minutes)
        mgdf = mdf.groupby(['p_id', 'o_id', 'm_role', 'm_label'])  # ~ 110K groups

        c = 1
        for n, g in mgdf:
            if c % 1000 == 0:
                self.logger.info(f"{c}/{len(mgdf.size())}")
            overlaps = self.detect_overlaps(g)
            if len(overlaps):
                for k, v in overlaps['overlap'].iteritems():
                    og = g.loc[k]
                    om_dict = {
                        'person_id': og.p_id,
                        'organization_id': og.o_id,
                        'role': og.m_role,
                        'label': og.m_label,
                        'start_date': og.m_start_date,
                        'end_date': og.m_end_date
                    }
                    try:
                        om = Membership.objects.get(**om_dict)
                    except Membership.MultipleObjectsReturned:
                        self.logger.info(f"multiple memberships detected for {om_dict}")
                        continue

                    if v[1] - v[0] > 30:
                        self.logger.info(
                            f"overlap of {v[1]-v[0]} days detected for "
                            f"membership: {om.id}, person: {om.person_id} ({om})"
                        )
                    else:
                        self.logger.info(
                            f"possible stop_and_go of {v[1]-v[0]} days detected for "
                            f"membership: {om.id}, person: {om.person_id} ({om})"
                        )
            c += 1

        self.logger.info("End of ETL process")
