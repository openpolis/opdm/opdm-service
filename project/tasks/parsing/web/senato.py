from typing import Iterable, Tuple, Optional

from bs4 import BeautifulSoup
import dateparser

from project.tasks.parsing.utils import requests_retry_session


def get_governo_urls(
    slug: str, url: str = "http://www.senato.it/leg/ElencoMembriGoverno/Governi.html"
) -> Optional[Tuple[str, str]]:
    page = requests_retry_session().get(url)
    soup = BeautifulSoup(page.text, features="lxml")
    div = soup.find(id="content")
    s = div.find(string=slug)
    if not s:
        slug = slug.split("-")[0]
        s = div.find(string=slug)
        if not s:
            return None
    a = s.find_parent("a")
    if a and "href" in a.attrs:
        ministri_url = f"http://www.senato.it{a['href']}"
        sottosegretari_url = ministri_url.replace("_M.htm", "_S.htm")
        return ministri_url, sottosegretari_url
    else:
        return None


def get_members(url: str) -> Iterable:
    page = requests_retry_session().get(url)
    soup = BeautifulSoup(page.text, features="lxml")
    tmp = soup.find("div", class_="composizione")
    results = tmp.find_all("a")
    return results


def get_person_dict(url: str):
    page = requests_retry_session().get(url)
    text = page.text.replace(
        "<!--- <SENATO.IT:SEN.ANAGRAFICA> -->", "<span id='anagrafica'>"
    )
    text = text.replace("<!--- </SENATO.IT:SEN.ANAGRAFICA> -->", "</span>")
    soup = BeautifulSoup(text, features="lxml")
    tag = soup.find(id="anagrafica")
    gen = tag.strings

    person = {}

    try:

        tmp = next(gen)  # process 1st item: determine gender
        if "Nato" in tmp:
            person["gender"] = "M"
        elif "Nata" in tmp:
            person["gender"] = "F"

        tmp = next(gen)  # precess 2nd item: birth date
        tmp = tmp.replace(" l'", "").replace(" il", "")
        parsed = dateparser.parse(tmp)
        if parsed:
            person["birth_date"] = parsed.strftime("%Y-%m-%d")

        # TODO: parse birth location (?)
        # tmp = next(gen)
    except StopIteration:
        pass

    return person
