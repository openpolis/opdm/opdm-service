from datetime import datetime
from functools import lru_cache
from typing import Dict, Tuple, Optional, Union, List, Any

from popolo.models import KeyEvent, Identifier
import roman

role_label_map = {
    "SOTTOSEGRETARIO DI STATO": "Sottosegretario di Stato",
    "MINISTRO SENZA PORTAFOGLIO": "Ministro senza Portafoglio",
    "MINISTRO": "Ministro",
    "VICE MINISTRO": "Vice Ministro",
    "PRESIDENTE DEL CONSIGLIO": "Presidente del Consiglio",
    "VICEPRESIDENTE DEL CONSIGLIO": "Vicepresidente del Consiglio",
    "ALTO COMMISSARIO": "Alto Commissario",
    "VICE ALTO COMMISSARIO": "Vice Alto Commissario",
    "ALTO COMMISSARIO AGGIUNTO": "Alto Commissario aggiunto",
    "SOTTOSEGRETARIO DI STATO ALLA PRESIDENZA DEL CONSIGLIO": "Sottosegretario di Stato alla Presidenza del Consiglio",
}
"""Map role labels to normalized values."""

SPARQLBinding = Dict[str, Dict[str, Any]]


def parse_membership(
    binding: SPARQLBinding, **kwargs: Any
) -> Dict[str, Union[str, None]]:
    ret = {
        "label": None,
        "role": None,
        "start_date": None,
        "end_date": None,
        "end_reason": None,
        "constituency_descr_tmp": None,
        "electoral_list_descr_tmp": None,
        "sources": [],
    }

    if "titolo" in binding and "nomeGoverno" in binding:
        titolo = binding["titolo"]["value"].split("(")[0].strip()
        nome_governo = binding["nomeGoverno"]["value"].split("(")[0].strip()

        if "delega" in binding:
            delega = binding["delega"]["value"].split("(")[0].strip()
            ret["label"] = f"{titolo} {delega} del {nome_governo}"
        else:
            ret["label"] = f"{titolo} del {nome_governo}"
        if "interim" in binding and binding["interim"]["value"] == "1":
            ret["label"] = f"{ret['label']} (interim)"

    if "carica" in binding:
        ret["role"] = role_label_map.get(
            binding["carica"]["value"], binding["carica"]["value"]
        )
    if "deputato" in binding:
        if "assemblea" in binding:
            n_leg = binding["assemblea"]["value"].split(
                "http://dati.camera.it/ocd/assemblea.rdf/a"
            )[1]
        else:
            n_leg = binding["leg"]["value"].split("_")[1]

        roman_leg = roman.toRoman(int(n_leg))
        ret["role"] = "Deputato"
        ret["label"] = f"Deputato nella {roman_leg} Legislatura"
        ret["sources"] = [{"note": "Open Data Camera", "url": "http://dati.camera.it/"}]

    if "senatore" in binding and "tipoMandato" in binding:
        roman_leg = roman.toRoman(int(kwargs["n_leg"]))
        if binding["tipoMandato"]["value"] == "elettivo":
            ret["role"] = "Senatore"
            ret["label"] = f"Senatore nella {roman_leg} Legislatura"
        elif "a vita" in binding["tipoMandato"]["value"]:
            ret["role"] = "Senatore a vita"
            ret[
                "label"
            ] = f"Senatore {binding['tipoMandato']['value']} - XVIII Legislatura"
        if "mandato" in binding:
            ret["links"] = [
                {"note": "Open Data Senato", "url": binding["mandato"]["value"]}
            ]
        ret["sources"] = [{"note": "Open Data Senato", "url": "http://dati.senato.it/"}]

    if "collegio" in binding:
        ret["constituency_descr_tmp"] = binding["collegio"]["value"]
    if "lista" in binding:
        ret["electoral_list_descr_tmp"] = binding["lista"]["value"]

    if "dataInizio" in binding:
        date_format = "%Y-%m-%d" if "-" in binding["dataInizio"]["value"] else "%Y%m%d"
        ret["start_date"] = datetime.strptime(
            binding["dataInizio"]["value"], date_format
        ).strftime("%Y-%m-%d")
    if "dataFine" in binding:
        date_format = "%Y-%m-%d" if "-" in binding["dataFine"]["value"] else "%Y%m%d"
        ret["end_date"] = datetime.strptime(
            binding["dataFine"]["value"], date_format
        ).strftime("%Y-%m-%d")
    if "inizioMandato" in binding:
        date_format = (
            "%Y-%m-%d" if "-" in binding["inizioMandato"]["value"] else "%Y%m%d"
        )
        ret["start_date"] = datetime.strptime(
            binding["inizioMandato"]["value"], date_format
        ).strftime("%Y-%m-%d")
    if "fineMandato" in binding:
        date_format = "%Y-%m-%d" if "-" in binding["fineMandato"]["value"] else "%Y%m%d"
        ret["end_date"] = datetime.strptime(
            binding["fineMandato"]["value"], date_format
        ).strftime("%Y-%m-%d")

        if "motivoFine" in binding:
            ret["end_reason"] = binding["motivoFine"]["value"]

    # sources key
    if "membroGoverno" in binding:
        ret["links"] = [
            {"note": "Open Data Camera", "url": binding["membroGoverno"]["value"]}
        ]

    if "dataElezione" in binding:
        try:
            ee_start_date = datetime.strptime(
                binding["dataElezione"]["value"], "%Y%m%d"
            ).strftime("%Y-%m-%d")
            electoral_event_id = KeyEvent.objects.values_list("id", flat=True).get(
                event_type="ELE-POL", start_date=ee_start_date
            )
            ret["electoral_event_id"] = electoral_event_id
        except (KeyEvent.MultipleObjectsReturned, KeyEvent.DoesNotExist):
            pass

    return ret


def parse_person(binding: SPARQLBinding) -> Dict[str, Union[str, List, None]]:
    ret = {
        "given_name": None,
        "family_name": None,
        "gender": None,
        "birth_location": None,
        "birth_date": None,
        "death_date": None,
        "image": None,
        "profession": None,
        "education_level": None,
        "other_names": [],
        "memberships": [],
        "contact_details": [],
        "identifiers": [],
    }

    if "given_name" in binding:
        ret["given_name"] = binding["given_name"]["value"].title()
    if "family_name" in binding:
        ret["family_name"] = binding["family_name"]["value"].title()
    if "birth_location" in binding:
        ret["birth_location"] = binding["birth_location"]["value"].title()
    if "birth_date" in binding:
        date_format = "%Y-%m-%d" if "-" in binding["birth_date"]["value"] else "%Y%m%d"
        ret["birth_date"] = datetime.strptime(
            binding["birth_date"]["value"], date_format
        ).strftime("%Y-%m-%d")
    if "death_date" in binding:
        date_format = "%Y-%m-%d" if "-" in binding["death_date"]["value"] else "%Y%m%d"
        ret["death_date"] = datetime.strptime(
            binding["death_date"]["value"], date_format
        ).strftime("%Y-%m-%d")
    if "gender" in binding:
        ret["gender"] = "M" if binding["gender"]["value"][0].casefold() == "m" else "F"
    if "image_url" in binding:
        ret["image"] = binding["image_url"]["value"]
    if "studioProf" in binding:
        ret["education_level"], ret["profession"] = _split_studioprof(
            binding["studioProf"]["value"]
        )
    elif "profession" in binding:
        ret["profession"] = binding["profession"]["value"]

    if "persona" in binding:
        ret["identifiers"] = [
            {"scheme": "OCD-URI", "identifier": binding["persona"]["value"]}
        ]
        ret["sources"] = [
            {"note": "Linked Open Data Camera", "url": binding["persona"]["value"]}
        ]
    elif "senatore" in binding:
        ret["identifiers"] = [
            {"scheme": "OSR-URI", "identifier": binding["senatore"]["value"]}
        ]
        ret["sources"] = [
            {"note": "Open Data Senato", "url": binding["senatore"]["value"]}
        ]
    return ret


def parse_contact_detail(x: str):
    ret = None
    x = (
        x.replace("twitter.com@", "twitter.com/@")
        .replace("http://.", "http://")
        .replace("http://http://", "http://")
        .replace("http:///", "http://")
        .replace("http://httpps://", "http://")
        .replace("http://https://", "http://")
        .replace("//www.facebook/", "//www.facebook.com/")
        .replace("http://", "https://")
        .strip("/")
        .lower()
    )
    if "twitter.com/" in x:
        handle = x.split("/")[-1]
        handle = handle.strip("@")
        ret = {
            "label": "Twitter",
            "value": f"https://twitter.com/{handle}",
            "contact_type": "TWITTER",
        }
    elif "facebook.com" in x:
        handle = x.split("/")[-1]
        ret = {
            "label": "Facebook",
            "value": f"https://www.facebook.com/{handle}",
            "contact_type": "FACEBOOK",
        }
    elif "youtube.com" in x:
        if "/user/" in x:
            splitter = "/user/"
        elif "/channel/" in x:
            splitter = "/channel/"
        else:
            splitter = "/"
        handle = x.split(splitter)[-1]
        ret = {
            "label": "YouTube",
            "value": f"https://www.youtube.com{splitter}{handle}",
            "contact_type": "YOUTUBE",
        }
    # elif "instagram" in x:
    #     handle = x.split("/")[-1]
    #     ret = {
    #         "label": "Instagram",
    #         "value": f"https://www.instagram.com/{handle}",
    #         "contact_type": "INSTAGRAM",
    #     }

    return ret


IdentifierT = Dict[str, str]


def parse_identifier(x: str) -> Optional[IdentifierT]:

    x = x.replace("ie.dbpedia.org", "it.dbpedia.org")

    if "it.dbpedia.org" in x:
        return {"scheme": "DBPEDIA-IT", "identifier": x}
    elif "wikidata.org" in x:
        return {"scheme": "WIKIDATA", "identifier": x}
    elif "freebase.com" in x:
        return {"scheme": "FREEBASE", "identifier": x}
    elif "dbpedia.org" in x:
        return {"scheme": "DBPEDIA", "identifier": x}
    elif "yago-knowledge-org" in x:
        return {"scheme": "YAGO", "identifier": x}
    else:
        return None


studio = [
    "triennio",
    "laureanda",
    "laureando",
    "corso di",
    "management",
    "accademia",
    "maturita'",
    "abilitazione",
    "b.a.",
    "m.b.a",
    "baccalaureata",
    "biennio/triennio",
    "corso di perfezionamento",
    "diploma",
    "dottorato",
    "dottore di ricerca",
    "economia aziendale",
    "istituto",
    "laurea",
    "laureato",
    "licenza",
    "liceo",
    "magistero",
    "master",
    "maturità",
    "perito",
    "phd",
    "ragioniera",
    "ragioniere",
    "scienze",
    "specializzazione",
    "studente",
    "studentessa",
    "studi",
]


def _split_studioprof(x: str) -> Tuple[Optional[str], Optional[str]]:
    x = (
        x.replace("Diplona", "Diploma")
        .replace("Larea", "Laurea")
        .replace("Lauea", "Laurea")
    )
    splitted = x.split(";")
    education_level = None
    profession = None
    if len(splitted) < 2:
        if any([s in splitted[0].casefold() for s in studio]):
            education_level = splitted[0]
        else:
            profession = splitted[0]
    else:
        education_level = splitted[0]
        profession = splitted[1]

    return education_level, profession


def parse_date(date: str):
    date_format = "%Y-%m-%d" if "-" in date else "%Y%m%d"
    return datetime.strptime(date, date_format).strftime("%Y-%m-%d")


@lru_cache(maxsize=None)
def keyevents_map():
    return {}


@lru_cache(maxsize=None)
def identifiers_map(**filter_args) -> Dict[str, Any]:
    return dict(
        Identifier.objects.filter(
            **filter_args
        ).values_list("identifier", "object_id")
    )
