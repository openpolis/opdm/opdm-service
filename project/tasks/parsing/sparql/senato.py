from SPARQLWrapper import JSON, SPARQLWrapper

ENDPOINT_SPARQL = "https://dati.senato.it/sparql"


def get_senatori(n_leg):
    sparql = SPARQLWrapper(ENDPOINT_SPARQL)
    sparql.setQuery(
        f"""
        PREFIX osr: <http://dati.senato.it/osr/>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        PREFIX bio: <http://purl.org/vocab/bio/0.1/>

        SELECT DISTINCT ?senatore ?image_url ?family_name ?given_name ?legislatura
        ?inizioMandato ?fineMandato ?tipoMandato
        ?tipoFineMandato ?birth_date ?birth_location
        ?nazioneNascita ?mandato ?collegioElezione ?gender ?death_date ?profession
        WHERE {{
            ?senatore a osr:Senatore.
            ?senatore foaf:firstName ?given_name.
            ?senatore foaf:lastName ?family_name.
            OPTIONAL {{?senatore foaf:depiction ?image_url.}}
            OPTIONAL {{?senatore osr:cittaNascita ?birth_location.}}
            OPTIONAL {{?senatore foaf:gender ?gender.}}
            OPTIONAL {{?senatore osr:professione ?professione. }}
            OPTIONAL {{?professione rdfs:label ?profession. }}
            OPTIONAL {{?senatore osr:nazioneNascita ?nazioneNascita.}}
            OPTIONAL {{?senatore osr:dataNascita ?birth_date.}}
            OPTIONAL {{?senatore bio:death ?death_date.}}
            ?senatore osr:mandato ?mandato.
            ?mandato osr:tipoMandato ?tipoMandato.
            ?mandato osr:inizio ?inizioMandato .
            OPTIONAL {{ ?mandato osr:fine ?fineMandato. }}
            OPTIONAL {{ ?mandato osr:tipoFineMandato ?tipoFineMandato .}}
            OPTIONAL {{ ?mandato osr:collegioElezione ?collegioElezione. }}
            ?mandato osr:legislatura ?legislatura.
            FILTER(?legislatura={n_leg})
        }} ORDER BY ?family_name ?given_name

        """
    )
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results["results"]["bindings"]


def get_gruppi_members(n_leg: int):
    sparql = SPARQLWrapper(ENDPOINT_SPARQL)
    sparql.setQuery(
        """
            PREFIX ocd: <http://dati.camera.it/ocd/>
            PREFIX osr: <http://dati.senato.it/osr/>
            PREFIX foaf: <http://xmlns.com/foaf/0.1/>

            SELECT DISTINCT ?gruppo ?nomeGruppo ?acrGruppo ?senatore ?nome
                ?cognome ?cittaNascita ?dataNascita ?carica ?inizioAdesione ?fineAdesione
            WHERE
            {
                ?gruppo a ocd:gruppoParlamentare .
                ?gruppo osr:denominazione ?denominazione .
                ?denominazione osr:titolo ?nomeGruppo .
                OPTIONAL {?denominazione osr:titoloBreve ?acrGruppo .}
                ?adesioneGruppo a ocd:adesioneGruppo .
                ?adesioneGruppo osr:carica ?carica .
                ?adesioneGruppo osr:legislatura ?legislatura .
                ?adesioneGruppo osr:inizio ?inizioAdesione.
                ?adesioneGruppo osr:gruppo ?gruppo.
                ?senatore ocd:aderisce ?adesioneGruppo.
                ?senatore a osr:Senatore.
                ?senatore foaf:firstName ?nome.
                ?senatore foaf:lastName ?cognome.
                OPTIONAL {?senatore osr:cittaNascita ?cittaNascita.}
                OPTIONAL {?senatore osr:dataNascita ?dataNascita.}
                OPTIONAL { ?adesioneGruppo osr:fine ?fineAdesione }
                OPTIONAL { ?denominazione osr:fine ?fineDenominazione }
                FILTER(!bound(?fineDenominazione) && ?legislatura=18)
            }
            GROUP BY ?gruppo
            ORDER BY ?nomeGruppo


            """.replace(
            "?legislatura=18", f"?legislatura={n_leg}"
        )
    )
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results["results"]["bindings"]
