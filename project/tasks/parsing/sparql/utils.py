"""
SPARQL utils.

The workflow:
    1. Create/edit the query and store it as file
    2. Retrieve the query as string using "read_raw_rq"
    3. Manipulate the query (if needed).
    4. Execute the query (with `get_bindings` func)
"""
import time
import urllib.error
from datetime import datetime
import pathlib
from typing import List, Dict, Any

from SPARQLWrapper import SPARQLWrapper2
from SPARQLWrapper.SmartWrapper import Bindings
from SPARQLWrapper.SmartWrapper import Value

RQ_RAW_PATH: pathlib.Path = pathlib.Path(__file__).parent.absolute() / "raw"
"""
Path where the .rq files are located.
"""


def read_raw_rq(rq_name: str) -> str:
    """
    Read a file representing a SPARQL query.

    :param rq_name: the name of the file.
    :return: return the query as string.
    """
    rq_path = RQ_RAW_PATH / rq_name

    with rq_path.open() as rq_file:
        return rq_file.read()


def get_bindings(
    endpoint: str, query: str, legacy: bool = False, method: str = "GET"
) -> List[Dict[str, Any]]:
    """
    Execute SPARQL query and returns a list of bindings.

    A wrapper to `SPARQLWrapper`.

    :param endpoint: A string representing the SPARQL endpoint's URI.
    :param query: The text of the query to be executed.
    :param legacy:
    :param method: GET or POST
    :return: The result of the query, as a list of bindings.
    """
    sparql_wrapper = SPARQLWrapper2(endpoint)
    sparql_wrapper.setQuery(query)
    sparql_wrapper.setMethod(method)

    max_retries = 10
    while max_retries > 0:
        time.sleep(3)
        """
        Ciclo while per gestione fallimento endpoint Sparql camere del Parlamento.
        Tale anomalia si riscontra all'endpoint Sparql della Camera dei deputati
        """
        try:
            result: Bindings = sparql_wrapper.query()
            if legacy:
                return result.fullResult["results"]["bindings"]
            else:
                return result.bindings
        except urllib.error.HTTPError:
            max_retries -= 1
    raise Exception(f"Endpoint {endpoint} failure.")


def get_person_from_binding(binding, identifier_scheme) -> Dict[str, Value]:
    ret = {
        "given_name": None,
        "family_name": None,
        "gender": None,
        "birth_date": None,
        "birth_location": None,
        "memberships": [],
    }

    if "person__given_name" in binding:
        ret["given_name"] = str(binding["person__given_name"].value).strip().title()
    if "person__family_name" in binding:
        ret["family_name"] = str(binding["person__family_name"].value).strip().title()
    if "person__birth_location" in binding:
        ret["birth_location"] = (
            str(binding["person__birth_location"].value).strip().title()
        )
    if "person__birth_date" in binding:
        raw_birth_date = str(binding["person__birth_date"].value)
        date_format = "%Y-%m-%d" if "-" in raw_birth_date else "%Y%m%d"
        ret["birth_date"] = datetime.strptime(raw_birth_date, date_format).strftime(
            "%Y-%m-%d"
        )
    if "person__death_date" in binding:
        raw_death_date = str(binding["person__death_date"].value)
        date_format = "%Y-%m-%d" if "-" in raw_death_date else "%Y%m%d"
        ret["death_date"] = datetime.strptime(raw_death_date, date_format).strftime(
            "%Y-%m-%d"
        )
    if "person__gender" in binding:
        ret["gender"] = (
            "M" if str(binding["person__gender"].value)[0].casefold() == "m" else "F"
        )
    if "person__image" in binding:
        ret["image"] = binding["person__image"].value
    if "person__contact_types" in binding:
        ret["contact_details"] = []
        for ct in binding["person__contact_types"].value.split(";"):
            ct = parse_contact_detail(ct)
            if ct:
                ret["contact_details"].append(ct)
    if "person__id":
        ret["identifiers"] = []
        ret["identifiers"].append(
            {"scheme": identifier_scheme, "identifier": binding["person__id"].value}
        )
    return ret


def parse_contact_detail(x: str):
    ret = None
    x = (
        x.replace("twitter.com@", "twitter.com/@")
        .replace("http://.", "http://")
        .replace("http://http://", "http://")
        .replace("http:///", "http://")
        .replace("http://httpps://", "http://")
        .replace("http://https://", "http://")
        .replace("//www.facebook/", "//www.facebook.com/")
        .replace("http://", "https://")
        .strip("/")
        .strip()
        .lower()
    )
    if "twitter.com/" in x:
        handle = x.split("/")[-1]
        handle = handle.strip("@")
        ret = {
            "label": "Twitter",
            "value": f"https://twitter.com/{handle}",
            "contact_type": "TWITTER",
        }
    elif "facebook.com" in x:
        handle = x.split("/")[-1]
        ret = {
            "label": "Facebook",
            "value": f"https://www.facebook.com/{handle}",
            "contact_type": "FACEBOOK",
        }
    elif "youtube.com" in x:
        if "/user/" in x:
            splitter = "/user/"
        elif "/channel/" in x:
            splitter = "/channel/"
        else:
            splitter = "/"
        handle = x.split(splitter)[-1]
        ret = {
            "label": "YouTube",
            "value": f"https://www.youtube.com{splitter}{handle}",
            "contact_type": "YOUTUBE",
        }
    elif "instagram" in x:
        handle = x.split("/")[-1]
        ret = {
            "label": "Instagram",
            "value": f"https://www.instagram.com/{handle}",
            "contact_type": "INSTAGRAM",
        }

    return ret
