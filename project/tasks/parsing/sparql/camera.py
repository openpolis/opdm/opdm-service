"""Collection of SPARQL queries used in data import tasks."""

from datetime import datetime
from typing import Dict, Union, List

from project.tasks.parsing.common import _split_studioprof


def get_person_from_binding(binding) -> Dict[str, Union[str, List]]:
    ret = {
        "given_name": None,
        "family_name": None,
        "gender": None,
        "birth_location": None,
        "birth_date": None,
        "death_date": None,
        "image": None,
        "profession": None,
        "education_level": None,
        "other_names": [],
        "memberships": [],
        "contact_details": [],
        "identifiers": [],
    }

    if "person__given_name" in binding:
        ret["given_name"] = str(binding["person__given_name"].value).strip().title()
    if "person__family_name" in binding:
        ret["family_name"] = str(binding["person__family_name"].value).strip().title()
    if "person__birth_location" in binding:
        ret["birth_location"] = (
            str(binding["person__birth_location"].value).strip().title()
        )
    if "person__birth_date" in binding:
        raw_birth_date = str(binding["person__birth_date"].value)
        date_format = "%Y-%m-%d" if "-" in raw_birth_date else "%Y%m%d"
        ret["birth_date"] = datetime.strptime(raw_birth_date, date_format).strftime(
            "%Y-%m-%d"
        )
    if "person__death_date" in binding:
        raw_death_date = str(binding["person__death_date"].value)
        date_format = "%Y-%m-%d" if "-" in raw_death_date else "%Y%m%d"
        ret["death_date"] = datetime.strptime(raw_death_date, date_format).strftime(
            "%Y-%m-%d"
        )
    if "person__gender" in binding:
        ret["gender"] = (
            "M" if str(binding["person__gender"].value)[0].casefold() == "m" else "F"
        )
    if "person__image" in binding:
        ret["image"] = binding["person__image"].value
    if "person__contact_types" in binding:
        for ct in binding["person__contact_types"].value.split(";"):
            ct = parse_contact_detail(ct)
            if ct:
                ret["contact_details"].append(ct)
    if "person__id" in binding:
        ret["identifiers"].append(
            {"scheme": "OCD-URI", "identifier": binding["person__id"].value}
        )

    if "nickFamilyName" in binding or "nickGivenName" in binding:
        given = family = ""
        if "nickFamilyName" in binding:
            family = binding["nickFamilyName"].value
        if "nickGivenName" in binding:
            given = binding["nickGivenName"].value
        ret["other_names"].append(
            {
                "othername_type": "NIC",
                "name": f"{given} {family}".strip().title()
            }
        )

    if "studioProf" in binding:
        ret["education_level"], ret["profession"] = _split_studioprof(
            binding["studioProf"].value
        )

    return ret


def parse_contact_detail(x: str):
    ret = None
    x = (
        x.replace("twitter.com@", "twitter.com/@")
        .replace("http://.", "http://")
        .replace("http://http://", "http://")
        .replace("http:///", "http://")
        .replace("http://httpps://", "http://")
        .replace("http://https://", "http://")
        .replace("//www.facebook/", "//www.facebook.com/")
        .replace("http://", "https://")
        .strip("/")
        .strip()
        .lower()
    )
    if "twitter.com/" in x:
        handle = x.split("/")[-1]
        handle = handle.strip("@")
        ret = {
            "label": "Twitter",
            "value": f"https://twitter.com/{handle}",
            "contact_type": "TWITTER",
        }
    elif "facebook.com" in x:
        handle = x.split("/")[-1]
        ret = {
            "label": "Facebook",
            "value": f"https://www.facebook.com/{handle}",
            "contact_type": "FACEBOOK",
        }
    elif "youtube.com" in x:
        if "/user/" in x:
            splitter = "/user/"
        elif "/channel/" in x:
            splitter = "/channel/"
        else:
            splitter = "/"
        handle = x.split(splitter)[-1]
        ret = {
            "label": "YouTube",
            "value": f"https://www.youtube.com{splitter}{handle}",
            "contact_type": "YOUTUBE",
        }
    # elif "instagram" in x:
    #     handle = x.split("/")[-1]
    #     ret = {
    #         "label": "Instagram",
    #         "value": f"https://www.instagram.com/{handle}",
    #         "contact_type": "INSTAGRAM",
    #     }

    return ret
