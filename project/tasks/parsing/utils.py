from typing import Tuple, List, Union


# def requests_retry_session(
#     retries: int = 3,
#     backoff_factor: float = 0.3,
#     status_forcelist: Tuple[int] = (500, 502, 504),
#     session: requests.Session = None,
# ):
#     """
#     Wrap a request Session with retries.
#
#     Credits: Peter Bengtsson
#
#     Original post: https://www.peterbe.com/plog/best-practice-with-retries-with-requests
#
#     :param retries: maximum number of retries.
#     :param backoff_factor: A backoff factor to apply between attempts after the second try
#     :param status_forcelist: A set of integer HTTP status codes that we should force a retry on.
#     :param session:
#     :return:
#     """
#     session = session or requests.Session()
#     retry = Retry(
#         total=retries,
#         read=retries,
#         connect=retries,
#         backoff_factor=backoff_factor,
#         status_forcelist=status_forcelist,
#     )
#     adapter = HTTPAdapter(max_retries=retry)
#     session.mount("http://", adapter)
#     session.mount("https://", adapter)
#     return session
#
#
# def merge_intervals(
#     a: Dict[str, str],
#     b: Dict[str, str],
#     start_date_key: str = "start_date",
#     end_date_key: str = "end_date",
# ) -> List[Dict[str, str]]:
#     """
#     Merge a couple of overlapping "intervals".
#
#     A valid "interval" is defined as a dictionary having a `start_date`, and a `end_date` key.
#     Start date of both intervals must be defined (non-null).
#     A null end date implies an open interval.
#
#     Start date and end date keys name can be customized.
#
#     :param a: an interval.
#     :param b: another interval.
#     :param start_date_key: the key of the dictionary to consider as start date.
#     :param end_date_key: the key of the dictionary to consider as end date.
#     :return:
#     """
#     a, b = sorted([a, b], key=lambda x: (x[start_date_key]))
#     tmp = []
#     # Since we sorted by start date:
#     # assert a[start_date_key] < b[start_date_key]
#     # a |---?
#     # b     |---?
#     # Now, check if overlapping.
#     if a[start_date_key] > b[start_date_key]:
#         # Not overlapping...
#         return list({a, b})
#     if a[start_date_key] < b[start_date_key]:
#         # a         |-------|
#         # b             |---?
#         # ~         |-a-|
#         tmp.append(
#             {
#                 **a,
#                 start_date_key: a[start_date_key],
#                 end_date_key: b[start_date_key],
#             },
#         )
#
#     tmp.append(b)
#
#     if b[end_date_key] is not None and (
#         a[end_date_key] is None or a[end_date_key] > b[end_date_key]
#     ):
#         # a     |-----------|
#         # b         |---|
#         # =     |-a-|-b-|-a-|
#         tmp.append(
#             {**a, start_date_key: b[end_date_key], end_date_key: a[end_date_key]}
#         )
#
#     return tmp
#
#
# def sort_overlapping_memberships(
#     memberships: List[Dict[str, Any]],
#     group_by_key: str = None,
#     start_date_key: str = "start_date",
#     end_date_key: str = "end_date",
# ) -> List[Dict[str, str]]:
#     """
#     Sort memberships and handle overlaps.
#
#     It just works.
#
#     :param memberships: a list of "memberships".
#     :param group_by_key: the key to be used to group the dictionaries.
#         Each group will be sorted separately. Defaults to none (no grouping).
#     :param start_date_key: the key of the dictionary to consider as start date.
#     :param end_date_key: the key of the dictionary to consider as end date.
#     :return: the sorted list of "memberships".
#     """
#
#     # If list empty, do nothing
#     if not memberships:
#         return memberships
#
#     def sort(memberships_):
#         # First, sort by start date
#         memberships_ = sorted(memberships_, key=lambda x: x[start_date_key])
#         tmp = [memberships_.pop(0)]
#         # Now, fix overlapping date intervals
#         for m in memberships_:
#             tmp = tmp + merge_intervals(tmp.pop(), m, start_date_key, end_date_key)
#
#         return tmp
#
#     if group_by_key:
#         groups = groupby(memberships, lambda x: x[group_by_key])
#         iter_map = map(sort, [[*group] for _, group in groups])
#         return list(chain.from_iterable(iter_map))
#
#     return sort(memberships)


OPDMDate = Union[str, None]
DateInterval = Tuple[OPDMDate, OPDMDate]


class DateIntervalException(Exception):
    pass


def carve_date_interval(original_interval: DateInterval, interval: DateInterval) -> List[DateInterval]:
    """A function that "carve" a date interval from an original interval,
    returning the resulting intervals a a list.
    Handles None value correctly for starting and end dates.

    |-------------------------------------------------|
                        |------------------|
                                  |
                                  v
    |-------------------|                  |----------|


          |--------------------------------------------|
    |-------------------|
                                  |
                                  v
                        |------------------------------|

    |--------------------------------------------|
                                       |-------------------|
                                  |
                                  v
    |----------------------------------|


                        |------------------|
    |-------------------------------------------------|
                                  |
                                  v
                                 [ ]
   </pre>

    :param original_interval: interval to be carved
    :param interval: interval to use for carving
    :return: list of intervals (0, 1 or 2)
    """

    # minimal and maximal values for dates comparison
    min_limit = "-999999"
    max_limit = "999999"

    def nones_to_limits(_interval: DateInterval) -> DateInterval:
        """Transform None values in a DateInterval into a string,
        so that dates can be compared more easily

        :param _interval: the interval to be transformed (with Nones)
        :return: the transformed interval (with limits)
        """
        start, end = _interval
        if start is None:
            start = min_limit
        if end is None:
            end = max_limit
        return start, end

    def limits_to_nones(_interval: DateInterval) -> DateInterval:
        """Transform limit string values in a DateInterval back into None values,
        so that date values are kept correctly.

        :param _interval: interval to be transformed (with limits)
        :return: the transformed interval (with Nones)
        """
        start, end = _interval
        if start == min_limit:
            start = None
        if end == max_limit:
            end = None
        return start, end

    original_interval_start, original_interval_end = nones_to_limits(original_interval)
    interval_start, interval_end = nones_to_limits(interval)

    if original_interval_start > original_interval_end:
        raise DateIntervalException("Start date is successive to end date")

    if interval_start > interval_end:
        raise DateIntervalException("Start date is successive to end date")

    if original_interval_start < interval_start <= interval_end < original_interval_end:
        return [
            limits_to_nones((original_interval_start, interval_start)),
            limits_to_nones((interval_end, original_interval_end))
        ]
    elif interval_start <= original_interval_start <= interval_end < original_interval_end:
        return [
            limits_to_nones((interval_end, original_interval_end))
        ]
    elif original_interval_start < interval_start <= original_interval_end <= interval_end:
        return [
            limits_to_nones((original_interval_start, interval_start))
        ]
    elif interval_start <= original_interval_start <= original_interval_end <= interval_end:
        return []

    elif (
        interval_end < original_interval_start or
        interval_start > original_interval_end
    ):
        return [limits_to_nones((original_interval_start, original_interval_end))]
    else:
        raise Exception("Unexpected exception")


def carve_date_intervals(original_intervals: List[DateInterval], intervals: List[DateInterval]) -> List[DateInterval]:
    """Carve multiple date time intervals from an original one

    Apply multiple times the carve_date_interval, after sorting the carving
    :param original_intervals: intervals to be carved
    :param intervals: intervals used for carving
    :return: the resulting interval, an empty list if all is removed
    """
    for interval in intervals:
        resulting_intervals = []
        for original_interval in original_intervals:
            resulting_intervals.extend(carve_date_interval(original_interval, interval))
        original_intervals = list(set(resulting_intervals))

    original_intervals.sort(key=lambda x: x[0])
    return original_intervals
