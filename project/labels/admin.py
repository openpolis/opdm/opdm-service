from django.contrib import admin

from project.labels.models import PersonMapping, OrgMapping


class PersonMappingAdmin(admin.ModelAdmin):
    model = PersonMapping
    list_display = ('role_type', 'label')
    raw_id_fields = ('role_type', )
    list_filter = ('label_classification', )
    search_fields = ('role_type__label', )

    def label(self, obj):
        return f"{obj.label_classification.descr}"
    label.short_description = 'Label'


class OrgMappingAdmin(admin.ModelAdmin):
    model = OrgMapping
    list_display = ('original_classification', 'label')
    raw_id_fields = ('classification', )
    list_filter = ('label_classification', )
    search_fields = ('classification__label', )

    def original_classification(self, obj):
        return f"{obj.classification.descr}"
    original_classification.short_description = 'Original classification'

    def label(self, obj):
        return f"{obj.label_classification.descr}"
    label.short_description = 'Label'


def register():
    """Register all the admin classes. """
    for model_class, admin_class in {
        (PersonMapping, PersonMappingAdmin),
        (OrgMapping, OrgMappingAdmin),
    }:
        admin.site.register(model_class, admin_class)
