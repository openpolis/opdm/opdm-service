from django.apps import AppConfig
from django.conf import settings
from django.utils.translation import ugettext_lazy as _


class LabelsConfig(AppConfig):
    """
    Default "labels" app configuration.

    Only connect signals when specified in settings (can be disabled in tests).
    """

    name = 'project.labels'
    verbose_name = _("Labels")

    def ready(self):
        from . import admin
        from . import signals

        admin.register()  # Register models in admin site
        if settings.LABELS_CONNECT_SIGNALS:
            signals.connect()  # Connect the signals
