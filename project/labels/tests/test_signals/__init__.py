import logging
import django.test
from popolo.tests.factories import OrganizationFactory, ClassificationFactory, RoleTypeFactory, PostFactory, \
    PersonFactory

from project.labels.models import OrgMapping, PersonMapping
from project.labels import signals


class OrgSignalsTest(django.test.TestCase):

    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.labels"
        )
        logger.handlers = []
        signals.connect()

    @classmethod
    def tearDownClass(cls):
        signals.disconnect()
        super().tearDownClass()

    def test_add_label_to_org(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        lcl_1 = ClassificationFactory(scheme='OPDM_ORGANIZATION_LABEL', descr='Label 1')
        OrgMapping.objects.create(classification=cl_a, label_classification=lcl_1)

        org_a.add_classification_rel(cl_a)
        self.assertTrue(
            org_a.classifications.filter(classification=lcl_1).count() == 1
        )

    def test_remove_label_from_org(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        lcl_1 = ClassificationFactory(scheme='OPDM_ORGANIZATION_LABEL', descr='Label 1')
        OrgMapping.objects.create(classification=cl_a, label_classification=lcl_1)
        org_a.add_classification_rel(cl_a)
        org_a.classifications.filter(classification=cl_a).delete()
        self.assertTrue(
            org_a.classifications.filter(classification=lcl_1).count() == 0
        )

    def test_add_classification_for_missing_label(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        org_a.add_classification_rel(cl_a)
        self.assertTrue(
            org_a.classifications.count() == 1
        )

    def test_remove_classification_for_missing_label(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        org_a.add_classification_rel(cl_a)
        org_a.classifications.filter(classification=cl_a).delete()
        self.assertTrue(
            org_a.classifications.count() == 0
        )


class PersonSignalsTest(django.test.TestCase):

    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.labels"
        )
        logger.handlers = []
        signals.connect()

    @classmethod
    def tearDownClass(cls):
        signals.disconnect()
        super().tearDownClass()

    def test_add_label_to_person(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        pers_a = PersonFactory.create()

        lcl_1 = ClassificationFactory(scheme='OPDM_PERSON_LABEL', descr='Label 1')
        PersonMapping.objects.create(role_type=rt_a, label_classification=lcl_1)

        pers_a.add_role(post_a)
        self.assertTrue(
            pers_a.classifications.filter(classification=lcl_1).count() == 1
        )

    def test_remove_label_from_person(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        pers_a = PersonFactory.create()

        lcl_1 = ClassificationFactory(scheme='OPDM_PERSON_LABEL', descr='Label 1')
        PersonMapping.objects.create(role_type=rt_a, label_classification=lcl_1)

        pers_a.add_role(post_a)
        pers_a.memberships.filter(post__role_type=rt_a).delete()

        self.assertTrue(
            pers_a.classifications.filter(classification=lcl_1).count() == 0
        )

    def test_add_membership_for_missing_label(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        pers_a = PersonFactory.create()
        pers_a.add_role(post_a)
        self.assertTrue(
            pers_a.classifications.count() == 0
        )

    def test_remove_membership_for_missing_label(self):
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        pers_a = PersonFactory.create()
        pers_a.add_role(post_a)
        pers_a.memberships.filter(post__role_type=rt_a).delete()
        self.assertTrue(
            pers_a.classifications.count() == 0
        )
