import logging

import django.test
from io import StringIO
from django.core.management import call_command
from popolo.models import Classification
from popolo.tests.factories import OrganizationFactory, PersonFactory, ClassificationFactory, RoleTypeFactory, \
    PostFactory


class ImportOrgMappingTest(django.test.TestCase):
    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.labels.management.commands.test_logging_command"
        )
        logger.handlers = []

    def test_criteriafile_required(self):
        out = StringIO()
        call_command(
            "import_organization_mappings", verbosity=2, stdout=out
        )
        self.assertTrue("ERROR" in out.getvalue() and "criteria-file must be given" in out.getvalue())

    def test_labels_assigned_correctly(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        org_a.add_classification(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        org_b.add_classification(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')
        org_c.add_classification(scheme='FORMA_GIURIDICA_OP', descr='Classificazione C')

        out = StringIO()
        call_command(
            "import_organization_mappings",
            verbosity=2,
            criteria_file='project/labels/tests/data/organization_labels_A.csv',
            stdout=out
        )
        self.assertTrue("ERROR" not in out.getvalue())
        self.assertTrue(
            all([
                'Label 1' in Classification.objects.filter(
                    scheme='OPDM_ORGANIZATION_LABEL'
                ).values_list('descr', flat=True),
                'Label 2' in Classification.objects.filter(
                    scheme='OPDM_ORGANIZATION_LABEL'
                ).values_list('descr', flat=True)
            ])
        )

    def test_missing_classification(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        org_a.add_classification(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        org_b.add_classification(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')
        org_c.add_classification(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')

        out = StringIO()
        call_command(
            "import_organization_mappings",
            verbosity=2,
            criteria_file='project/labels/tests/data/organization_labels_A.csv',
            stdout=out
        )
        self.assertTrue("ERROR" in out.getvalue())
        self.assertTrue("Could not find Classification" in out.getvalue())

    def test_handle_double_mapping(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()

        org_a.add_classification(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        org_b.add_classification(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')

        out = StringIO()
        call_command(
            "import_organization_mappings",
            verbosity=2,
            criteria_file='project/labels/tests/data/organization_labels_B.csv',
            stdout=out
        )
        self.assertTrue("ERROR" not in out.getvalue())
        self.assertTrue("already existing" in out.getvalue())


class ImportPersonMappingTest(django.test.TestCase):
    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.labels.management.commands.test_logging_command"
        )
        logger.handlers = []

    def test_criteriafile_required(self):
        out = StringIO()
        call_command(
            "import_person_mappings", verbosity=2, stdout=out
        )
        self.assertTrue("ERROR" in out.getvalue() and "criteria-file must be given" in out.getvalue())

    def test_labels_assigned_correctly(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        cl_a = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        cl_b = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')
        cl_c = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione C')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)
        org_c.add_classification_rel(cl_c)

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        rt_b = RoleTypeFactory.create(label='Roletype B', classification=cl_b)
        rt_c = RoleTypeFactory.create(label='Roletype C', classification=cl_c)

        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        post_b = PostFactory.create(organization=org_b, role_type=rt_b, role=rt_b.label)
        post_c = PostFactory.create(organization=org_c, role_type=rt_c, role=rt_c.label)

        pers_a = PersonFactory.create()
        pers_b = PersonFactory.create()
        pers_c = PersonFactory.create()

        pers_a.add_role(post_a)
        pers_b.add_role(post_b)
        pers_c.add_role(post_c)

        out = StringIO()
        call_command(
            "import_person_mappings",
            verbosity=2,
            criteria_file='project/labels/tests/data/person_labels_A.csv',
            stdout=out
        )
        self.assertTrue("ERROR" not in out.getvalue())
        self.assertTrue(
            all([
                'Label 1' in Classification.objects.filter(
                    scheme='OPDM_PERSON_LABEL'
                ).values_list('descr', flat=True),
                'Label 2' in Classification.objects.filter(
                    scheme='OPDM_PERSON_LABEL'
                ).values_list('descr', flat=True)
            ])
        )

    def test_missing_roletype(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        cl_a = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        cl_b = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')
        cl_c = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione C')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)
        org_c.add_classification_rel(cl_c)

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        rt_b = RoleTypeFactory.create(label='Roletype B', classification=cl_b)

        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        post_b = PostFactory.create(organization=org_b, role_type=rt_b, role=rt_b.label)
        post_c = PostFactory.create(organization=org_c, role_type=rt_b, role=rt_b.label)

        pers_a = PersonFactory.create()
        pers_b = PersonFactory.create()
        pers_c = PersonFactory.create()

        pers_a.add_role(post_a)
        pers_b.add_role(post_b)
        pers_c.add_role(post_c)

        out = StringIO()
        call_command(
            "import_person_mappings",
            verbosity=2,
            criteria_file='project/labels/tests/data/person_labels_A.csv',
            stdout=out
        )
        self.assertTrue("ERROR" in out.getvalue())
        self.assertTrue("Could not find role type" in out.getvalue())

    def test_handle_double_mapping(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()

        cl_a = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        cl_b = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        rt_b = RoleTypeFactory.create(label='Roletype B', classification=cl_b)

        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        post_b = PostFactory.create(organization=org_b, role_type=rt_b, role=rt_b.label)

        pers_a = PersonFactory.create()
        pers_b = PersonFactory.create()

        pers_a.add_role(post_a)
        pers_b.add_role(post_b)

        out = StringIO()
        call_command(
            "import_person_mappings",
            verbosity=2,
            criteria_file='project/labels/tests/data/person_labels_B.csv',
            stdout=out
        )
        self.assertTrue("ERROR" not in out.getvalue())
        self.assertTrue("already existing" in out.getvalue())
