import logging

import django.test
from io import StringIO
from django.core.management import call_command
from popolo.models import Ownership
from popolo.tests.factories import OrganizationFactory, ClassificationFactory, RoleTypeFactory, PostFactory, \
    PersonFactory

from project.labels.models import OrgMapping, PersonMapping


class SyncOrgMappingTest(django.test.TestCase):
    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.labels.management.commands.test_logging_command"
        )
        logger.handlers = []

    def test_labels_synched_correctly(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        cl_b = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')
        cl_c = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione C')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)
        org_c.add_classification_rel(cl_c)

        lcl_1 = ClassificationFactory(scheme='OPDM_ORGANIZATION_LABEL', descr='Label 1')
        lcl_2 = ClassificationFactory(scheme='OPDM_ORGANIZATION_LABEL', descr='Label 2')

        org_c.add_classification_rel(lcl_1)

        OrgMapping.objects.create(classification=cl_a, label_classification=lcl_1)
        OrgMapping.objects.create(classification=cl_b, label_classification=lcl_2)
        OrgMapping.objects.create(classification=cl_c, label_classification=lcl_2)

        out = StringIO()
        call_command(
            "script_sync_organization_labels", verbosity=2, stdout=out
        )
        self.assertTrue(
            all([
                org_a.classifications.filter(classification=lcl_1).count() == 1,
                org_b.classifications.filter(classification=lcl_2).count() == 1,
                org_c.classifications.filter(classification=lcl_2).count() == 1,
            ])
        )

    def test_labels_sync_does_not_remove(self):
        """Organization already labeled are not reset when mapping changes, if --reset is not used
        """
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()

        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        cl_b = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)

        lcl_1 = ClassificationFactory(scheme='OPDM_ORGANIZATION_LABEL', descr='Label 1')
        lcl_2 = ClassificationFactory(scheme='OPDM_ORGANIZATION_LABEL', descr='Label 2')

        org_a.add_classification_rel(lcl_1)
        org_b.add_classification_rel(lcl_2)

        OrgMapping.objects.create(classification=cl_a, label_classification=lcl_1)

        out = StringIO()
        call_command(
            "script_sync_organization_labels", verbosity=2, stdout=out
        )
        self.assertTrue(
            all([
                org_a.classifications.filter(classification=lcl_1).count() == 1,
                org_b.classifications.filter(classification=lcl_2).count() == 1,
            ])
        )

    def test_labels_sync_with_reset_does_remove(self):
        """Organization already labeled are reset when mapping changes, if --reset is being used
        """
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()

        cl_a = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        cl_b = ClassificationFactory(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)

        lcl_1 = ClassificationFactory(scheme='OPDM_ORGANIZATION_LABEL', descr='Label 1')
        lcl_2 = ClassificationFactory(scheme='OPDM_ORGANIZATION_LABEL', descr='Label 2')

        org_a.add_classification_rel(lcl_1)
        org_b.add_classification_rel(lcl_2)

        OrgMapping.objects.create(classification=cl_a, label_classification=lcl_1)

        out = StringIO()
        call_command(
            "script_sync_organization_labels", verbosity=2, reset=True, stdout=out
        )
        self.assertTrue(
            all([
                org_a.classifications.filter(classification=lcl_1).count() == 1,
                org_b.classifications.filter(classification=lcl_2).count() == 0,
            ])
        )

    def test_missing_classification_mapping(self):
        """A FORMA_GIURIDICA_OP classification has no mapping to a label"""
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        org_a.add_classification_rel(cl_a)

        out = StringIO()
        call_command(
            "script_sync_organization_labels", verbosity=2, stdout=out
        )
        self.assertTrue('WARNING' in out.getvalue() and 'No label assigned')


class SyncPersonMappingTest(django.test.TestCase):
    def setUp(self):
        """Logger handlers need to be reset before each tests or they mangle up.

        :return:
        """
        logger = logging.getLogger(
            "project.labels.management.commands.test_logging_command"
        )
        logger.handlers = []

    def test_labels_synched_correctly(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        p = PersonFactory.create()
        o = OrganizationFactory.create()
        Ownership.objects.create(owner_person=p, owned_organization=o, percentage=0.25)

        cl_a = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        cl_b = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')
        cl_c = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione C')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)
        org_c.add_classification_rel(cl_c)

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        rt_b = RoleTypeFactory.create(label='Roletype B', classification=cl_b)
        rt_c = RoleTypeFactory.create(label='Roletype C', classification=cl_c)

        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        post_b = PostFactory.create(organization=org_b, role_type=rt_b, role=rt_b.label)
        post_c = PostFactory.create(organization=org_c, role_type=rt_c, role=rt_c.label)

        pers_a = PersonFactory.create()
        pers_b = PersonFactory.create()
        pers_c = PersonFactory.create()

        pers_a.add_role(post_a)
        pers_b.add_role(post_b)
        pers_c.add_role(post_c)

        lcl_1 = ClassificationFactory(scheme='OPDM_PERSON_LABEL', descr='Label 1')
        lcl_2 = ClassificationFactory(scheme='OPDM_PERSON_LABEL', descr='Label 2')
        lcl_tit = ClassificationFactory.create(scheme='OPDM_PERSON_LABEL', descr='Titolare quote')

        # person c is already labeled with label 1
        pers_c.add_classification_rel(lcl_1)

        PersonMapping.objects.create(role_type=rt_a, label_classification=lcl_1)
        PersonMapping.objects.create(role_type=rt_b, label_classification=lcl_2)
        PersonMapping.objects.create(role_type=rt_c, label_classification=lcl_2)

        out = StringIO()
        call_command(
            "script_sync_person_labels", verbosity=2, stdout=out
        )
        self.assertTrue(
            all([
                pers_a.classifications.filter(classification=lcl_1).count() == 1,
                pers_b.classifications.filter(classification=lcl_2).count() == 1,
                p.classifications.filter(classification=lcl_tit)
            ])
        )

        # multiple labels are possible with Persons, so person c has label 2 and keeps label 1
        self.assertTrue(pers_c.classifications.filter(classification=lcl_1).count() == 1)
        self.assertTrue(pers_c.classifications.filter(classification=lcl_2).count() == 1)

    def test_labels_reset_correctly(self):
        org_a = OrganizationFactory.create()
        org_b = OrganizationFactory.create()
        org_c = OrganizationFactory.create()

        cl_a = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        cl_b = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione B')
        cl_c = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione C')

        org_a.add_classification_rel(cl_a)
        org_b.add_classification_rel(cl_b)
        org_c.add_classification_rel(cl_c)

        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        rt_b = RoleTypeFactory.create(label='Roletype B', classification=cl_b)
        rt_c = RoleTypeFactory.create(label='Roletype C', classification=cl_c)

        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        post_b = PostFactory.create(organization=org_b, role_type=rt_b, role=rt_b.label)
        post_c = PostFactory.create(organization=org_c, role_type=rt_c, role=rt_c.label)

        pers_a = PersonFactory.create()
        pers_b = PersonFactory.create()
        pers_c = PersonFactory.create()

        pers_a.add_role(post_a)
        pers_b.add_role(post_b)
        pers_c.add_role(post_c)

        lcl_1 = ClassificationFactory(scheme='OPDM_PERSON_LABEL', descr='Label 1')
        lcl_2 = ClassificationFactory(scheme='OPDM_PERSON_LABEL', descr='Label 2')
        ClassificationFactory.create(scheme='OPDM_PERSON_LABEL', descr='Titolare quote')

        # person c is already labeled witn label 1
        pers_c.add_classification_rel(lcl_1)

        PersonMapping.objects.create(role_type=rt_a, label_classification=lcl_1)
        PersonMapping.objects.create(role_type=rt_b, label_classification=lcl_2)
        PersonMapping.objects.create(role_type=rt_c, label_classification=lcl_2)

        out = StringIO()
        call_command(
            "script_sync_person_labels", verbosity=2, reset=True, stdout=out
        )
        self.assertTrue(
            all([
                pers_a.classifications.filter(classification=lcl_1).count() == 1,
                pers_b.classifications.filter(classification=lcl_2).count() == 1,
            ])
        )

        # labels were reset, so person c has only label 2
        self.assertTrue(pers_c.classifications.filter(classification=lcl_1).count() == 0)
        self.assertTrue(pers_c.classifications.filter(classification=lcl_2).count() == 1)

    def test_missing_role_type_mapping(self):
        """A RoleType has no mapping to a label"""
        org_a = OrganizationFactory.create()
        cl_a = ClassificationFactory.create(scheme='FORMA_GIURIDICA_OP', descr='Classificazione A')
        ClassificationFactory.create(scheme='OPDM_PERSON_LABEL', descr='Titolare quote')

        org_a.add_classification_rel(cl_a)
        rt_a = RoleTypeFactory.create(label='Roletype A', classification=cl_a)
        post_a = PostFactory.create(organization=org_a, role_type=rt_a, role=rt_a.label)
        pers_a = PersonFactory.create()
        pers_a.add_role(post_a)

        out = StringIO()
        call_command(
            "script_sync_person_labels", verbosity=2, stdout=out
        )
        self.assertTrue('WARNING' in out.getvalue() and 'No label assigned')
