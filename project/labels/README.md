In order to run tests and measure coverage for this app:

```
DJANGO_SETTINGS_MODULE=config.settings.test coverage run \
  --source="project.labels" --omit="*/tests/*"  manage.py test labels
coverage report
```
