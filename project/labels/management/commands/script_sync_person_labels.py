# coding=utf-8
from datetime import datetime
from datetime import timedelta

from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from popolo.models import Person, ClassificationRel, RoleType, Ownership, Classification

from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Sync automatic person labels, from the models"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument(
            "--reset",
            dest="reset",
            action='store_true',
            help="Reset all personal labels, by deleting all of them before re-creating them.",
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        try:
            chunk_size = 1000
            batch_size = 100
            reset = options['reset']

            person_content_type = ContentType.objects.filter(model='person').first()
            self.logger.info("Starting procedure")

            if reset:
                labels = ClassificationRel.objects.filter(
                    content_type=person_content_type,
                    classification__scheme='OPDM_PERSON_LABEL',
                )
                self.logger.info(f"Removing all {labels.count()} labels, to perform a reset, as requested.")
                labels._raw_delete(labels.db)

            # labels generating from RoleTypes
            for rt in RoleType.objects.all():
                if rt.labels_mapping.count():
                    label_cl = rt.labels_mapping.first().label_classification

                    # compute ids of all persons with given role types,
                    # not classified with corresponding OPDM_PERSON_LABEL
                    diff_ids = list(set(
                        Person.objects.filter(memberships__post__role_type=rt).values_list('id', flat=True)
                    ) - set(
                        Person.objects.filter(classifications__classification=label_cl).values_list('id', flat=True)
                    ))
                    self.logger.info(f"{rt.label} <= {label_cl.descr} ({len(diff_ids)})")
                    for n, c in enumerate(range(0, len(diff_ids), chunk_size)):
                        chunk_ids = diff_ids[c:c + chunk_size]
                        to_add_ids = list(set(chunk_ids))
                        items_objects = [
                            ClassificationRel(
                                content_type=person_content_type,
                                object_id=pid,
                                classification=label_cl)
                            for pid in to_add_ids
                        ]
                        ClassificationRel.objects.bulk_create(items_objects, batch_size=batch_size)

                        if n > 0:
                            self.logger.info(
                                "{0}/{1}".format(
                                    n * chunk_size, len(diff_ids)
                                )
                            )

                else:
                    self.logger.warning(f"{rt.label} => No label assigned")

            # labels generating from Ownerships

            five_years_ago = datetime.strftime(
                datetime.now() - timedelta(days=-5*365),
                '%Y-%d-%m'
            )
            owners_ids = Ownership.objects.filter(
                owner_person__isnull=False
            ).filter(
                Q(end_date__isnull=True) | Q(end_date__gt=five_years_ago)
            ).values_list('owner_person_id', flat=True).distinct()

            titolare_cl = Classification.objects.get(
                scheme='OPDM_PERSON_LABEL', descr='Titolare quote'
            )
            labeled_ids = Person.objects.filter(
                classifications__classification=titolare_cl
            ).values_list('id', flat=True).distinct()
            diff_ids = list(set(owners_ids) - set(labeled_ids))
            self.logger.info(f"Owners <= {titolare_cl.descr} = ({len(diff_ids)})")
            for n, c in enumerate(range(0, len(diff_ids), chunk_size)):
                chunk_ids = diff_ids[c:c + chunk_size]
                to_add_ids = list(set(chunk_ids))
                items_objects = [
                    ClassificationRel(
                        content_type=person_content_type,
                        object_id=pid,
                        classification=titolare_cl)
                    for pid in to_add_ids
                ]
                ClassificationRel.objects.bulk_create(items_objects, batch_size=batch_size)

                if n > 0:
                    self.logger.info(
                        "{0}/{1}".format(
                            n * chunk_size, len(diff_ids)
                        )
                    )

            self.logger.info("End of procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes
