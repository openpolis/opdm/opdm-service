# coding=utf-8
from ooetl.extractors import CSVExtractor
from popolo.models import Classification, RoleType
from numpy import int32, int64

from project.labels.models import PersonMapping
from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Import role types to labels mappings, from a csv source"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument(
            "--criteria-file",
            dest="criteria_file",
            help="Criteria to assign labels to persons with roles in organizations of given types"
            "(path or URL to file)",
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        try:
            criteria_file = options['criteria_file']

            self.logger.info("Starting procedure")

            # extract criteria list from file
            if criteria_file is None:
                self.logger.error("criteria-file must be given: use a local path or a remote url to a CSV file")
                return "Done!"
            criteria_df = CSVExtractor(
                criteria_file,
                sep=",",
                dtype={
                    'occorrenze': int32,
                    'roletype_id': int64,
                    'classification_id': int64
                }
            ).extract()

            for crit in criteria_df.to_dict(orient="records"):
                label = crit['label person']

                try:
                    rt = RoleType.objects.get(
                        label=crit['roletype_label']
                    )
                except RoleType.DoesNotExist:
                    self.logger.error(f"Could not find role type for {crit['roletype_label']}.")
                    continue

                label_cl, created = Classification.objects.get_or_create(
                    scheme="OPDM_PERSON_LABEL",
                    descr=label
                )
                if created:
                    self.logger.debug(f"Label {label} added to OPDM_PERSON_LABEL Classifications")

                m, created = PersonMapping.objects.get_or_create(
                    role_type=rt,
                    label_classification=label_cl
                )
                if created:
                    self.logger.info(f"{m} created.")
                else:
                    self.logger.info(f"{m} already existing.")

            self.logger.info("End of procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes
