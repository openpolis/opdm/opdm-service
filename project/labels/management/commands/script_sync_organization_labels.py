# coding=utf-8
from django.contrib.contenttypes.models import ContentType
from popolo.models import Classification, ClassificationRel, Organization

from taskmanager.management.base import LoggingBaseCommand


class Command(LoggingBaseCommand):
    help = "Synchronize organization labels, from the existing mappings"
    requires_migrations_checks = True
    requires_system_checks = True

    def add_arguments(self, parser):
        parser.add_argument(
            "--reset",
            dest="reset",
            action='store_true',
            help="Reset all organization labels, by deleting all of them before re-creating them.",
        )

    def handle(self, *args, **options):
        super(Command, self).handle(__name__, *args, formatter_key="simple", **options)

        try:
            chunk_size = 1000
            batch_size = 100
            reset = options['reset']

            org_content_type = ContentType.objects.filter(model='organization').first()
            self.logger.info("Start loading procedure")

            if reset:
                labels = ClassificationRel.objects.filter(
                    content_type=org_content_type,
                    classification__scheme='OPDM_ORGANIZATION_LABEL',
                )
                self.logger.info(f"Removing all {labels.count()} labels, to perform a reset, as requested.")
                labels._raw_delete(labels.db)

            for cl in Classification.objects.filter(
                scheme="FORMA_GIURIDICA_OP",
            ):
                if cl.labels_mapping.count():
                    label_cl = cl.labels_mapping.first().label_classification
                    diff_ids = list(set(
                        Organization.objects.filter(classifications__classification=cl).
                        values_list('id', flat=True)
                    ) - set(
                        Organization.objects.filter(classifications__classification=label_cl).
                        values_list('id', flat=True)
                    ))

                    self.logger.info(f"{cl.descr} => {label_cl.descr} = {len(diff_ids)}")
                    for n, c in enumerate(range(0, len(diff_ids), chunk_size)):
                        chunk_ids = diff_ids[c:c + chunk_size]
                        to_add_ids = list(set(chunk_ids))
                        items_objects = [
                            ClassificationRel(
                                content_type=org_content_type,
                                object_id=oid,
                                classification=label_cl)
                            for oid in to_add_ids
                        ]
                        ClassificationRel.objects.filter(
                            content_type=org_content_type,
                            object_id__in=to_add_ids,
                            classification__scheme="OPDM_ORGANIZATION_LABEL"
                        ).delete()
                        ClassificationRel.objects.bulk_create(items_objects, batch_size=batch_size)

                        if n > 0:
                            self.logger.info(
                                "{0}/{1}".format(
                                    n * chunk_size, len(diff_ids)
                                )
                            )
                else:
                    self.logger.warning(f"{cl.descr} => No label assigned")

            self.logger.info("End of procedure")
        except (KeyboardInterrupt, SystemExit):
            return "\nInterrupted by the user."
        return "Done!"  # Will be printed to stdout when command finishes
