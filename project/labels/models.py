from popolo.behaviors.models import Timestampable
from popolo.models import Classification, RoleType

from django.utils.translation import ugettext_lazy as _
from django.db import models


class OrgMapping(Timestampable, models.Model):
    """Maps an OP_FORMA_GIURIDICA Classification instance to a OPDM_ORGANIZAZION_LABEL one
    """
    classification = models.ForeignKey(
        to=Classification,
        related_name="labels_mapping",
        limit_choices_to={"scheme": "OP_FORMA_GIURIDICA"},
        help_text=_("The original classification"),
        on_delete=models.CASCADE,
    )

    label_classification = models.ForeignKey(
        to=Classification,
        related_name="original_classifications_mapping",
        limit_choices_to={"scheme": "OPDM_ORGANIZAZION_LABEL"},
        help_text=_("The label classification"),
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        return f"(FORMA_GIURIDICA_OP:{self.classification.descr}) -> " \
            f"(OPDM_ORGANIZAZION_LABEL:{self.label_classification.descr})"


class PersonMapping(Timestampable, models.Model):
    """Maps a RoleType instance to a OPDM_PERSON_LABEL one
    """
    role_type = models.ForeignKey(
        to=RoleType,
        related_name="labels_mapping",
        help_text=_("The original RoleType"),
        on_delete=models.CASCADE,
    )

    label_classification = models.ForeignKey(
        to=Classification,
        related_name="original_role_types_mapping",
        limit_choices_to={"scheme": "OPDM_PERSON_LABEL"},
        help_text=_("The label classification"),
        on_delete=models.CASCADE,
    )

    def __str__(self) -> str:
        return f"(RoleType:{self.role_type.label}) -> (OPDM_PERSON_LABEL:{self.label_classification.descr})"
