from django.db.models.signals import post_save, pre_delete

from popolo.models import ClassificationRel, Membership, Organization


def _classification_to_label(org):
    mapping = org.classifications.get(classification__scheme='FORMA_GIURIDICA_OP').\
        classification.labels_mapping.first()
    if mapping:
        return mapping.label_classification
    else:
        return None


def _roletype_to_label(m):
    if m.post and m.post.role_type:
        mapping = m.post.role_type.labels_mapping.first()
        if mapping:
            return mapping.label_classification
    return None


def add_label_to_org(sender, instance: ClassificationRel, **kwargs):
    """
    Assign an OPDM_ORGANIZATION_LABEL classification to an Organization that
    just received a FORMA_GIURIDICA_OP classification.

    :param sender: The model class
    :param instance: The actual instance being saved.
    :param kwargs: Other args. See: https://docs.djangoproject.com/en/dev/ref/signals/#post-save
    """
    if instance.classification.scheme == 'FORMA_GIURIDICA_OP':
        try:
            org = Organization.objects.get(id=instance.object_id)
        except Organization.DoesNotExist:
            pass
        else:
            label_classification = _classification_to_label(org)
            if label_classification:
                org.add_classification_rel(label_classification)


def remove_label_from_org(sender, instance: ClassificationRel, **kwargs):
    """
    Remove an OPDM_ORGANIZATION_LABEL classification from an Organization after
    removal of a FORMA_GIURIDICA_OP classification.

    :param sender: The model class
    :param instance: The actual instance being saved.
    :param kwargs: Other args. See: https://docs.djangoproject.com/en/dev/ref/signals/#post-save
    """
    if instance.classification.scheme == 'FORMA_GIURIDICA_OP':
        try:
            org = Organization.objects.get(id=instance.object_id)
        except Organization.DoesNotExist:
            pass
        else:
            label_classification = _classification_to_label(org)
            if label_classification:
                org.classifications.filter(classification=label_classification).delete()


def add_label_to_person(sender, instance: Membership, **kwargs):
    """
    Assign an OPDM_PERSON_LABEL classification to a Person that
    just received a Membership.

    :param sender: The model class
    :param instance: The actual instance being saved.
    :param kwargs: Other args. See: https://docs.djangoproject.com/en/dev/ref/signals/#post-save
    :raises: IntegrityError
    """
    label_classification = _roletype_to_label(instance)
    if label_classification:
        instance.person.add_classification_rel(label_classification, allow_same_scheme=True)


def remove_label_from_person(sender, instance: Membership, **kwargs):
    """
    Remove an OPDM_ORGANIZATION_LABEL classification from a Person after
    removal of a membership.

    :param sender: The model class
    :param instance: The actual instance being saved.
    :param kwargs: Other args. See: https://docs.djangoproject.com/en/dev/ref/signals/#post-save
    """
    label_classification = _roletype_to_label(instance)
    if label_classification:
        instance.person.classifications.filter(classification=label_classification).delete()


def connect():
    """
    Connect all the signals.
    """
    post_save.connect(receiver=add_label_to_org, sender=ClassificationRel)
    pre_delete.connect(receiver=remove_label_from_org, sender=ClassificationRel)

    post_save.connect(receiver=add_label_to_person, sender=Membership)
    pre_delete.connect(receiver=remove_label_from_person, sender=Membership)


def disconnect():
    """
    Connect all the signals.
    """
    post_save.disconnect(receiver=add_label_to_org, sender=ClassificationRel)
    pre_delete.disconnect(receiver=remove_label_from_org, sender=ClassificationRel)

    post_save.disconnect(receiver=add_label_to_person, sender=Membership)
    pre_delete.disconnect(receiver=remove_label_from_person, sender=Membership)
