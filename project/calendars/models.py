from django.db import models
from django.utils.translation import gettext_lazy as _


class CalendarDaily(models.Model):
    """Sitting of the organ where votations occurr."""

    date = models.DateField(
        _("date"),
        help_text=_("Date"),
    )
    weekday_number = models.PositiveSmallIntegerField(
        help_text=_("Weekday Number")
    )
    weekday_description = models.CharField(
        help_text=_("Weekday description"),
        max_length=20
    )
    week_number = models.PositiveSmallIntegerField(
        help_text=_("Weekday Number")
    )
    month = models.PositiveSmallIntegerField(
        help_text=_("Month")
    )
    month_name = models.CharField(
        help_text=_("Month name"),
        max_length=20
    )
    year = models.PositiveSmallIntegerField(
        help_text=_("Year")
    )
    iso_year = models.PositiveSmallIntegerField(
        help_text=_("ISO Year")
    )
    quarter = models.PositiveSmallIntegerField(
        help_text=_("Quarter")
    )
    is_leap_year = models.BooleanField(
        help_text=_("If year is leap")
    )
    day_of_year = models.PositiveSmallIntegerField(
        help_text=_("Day of year")
    )
    days_in_month = models.PositiveSmallIntegerField(
        help_text=_("Days in month")
    )
    is_month_end = models.BooleanField(
        help_text=_("If day is end month")
    )
    is_month_start = models.BooleanField(
        help_text=_("If day is start month")
    )
    is_quarter_end = models.BooleanField(
        help_text=_("If day is end quarter")
    )
    is_quarter_start = models.BooleanField(
        help_text=_("If day is start quarter")
    )
    is_year_end = models.BooleanField(
        help_text=_("If day is end year")
    )
    is_year_start = models.BooleanField(
        help_text=_("If day is start month")
    )

    class Meta:
        verbose_name = _("Calendar")
        verbose_name_plural = _("Calendars")

    def __str__(self):
        return f"{self.date}"
