from taskmanager.management.base import LoggingBaseCommand
from project.calendars.models import CalendarDaily
import pandas as pd
from ooetl.loaders import DjangoUpdateOrCreateLoader
from ooetl import ETL
from ooetl.extractors import DataframeExtractor


def create_calendar_daily(start='1948-01-01', end='2050-12-31'):
    df = pd.DataFrame({"date": pd.date_range(start, end)})
    df["weekday_description"] = df.date.dt.day_name()
    df["week_number"] = df.date.dt.isocalendar().week
    df["weekday_number"] = df.date.dt.day_of_week + 1
    df["year"] = df.date.dt.year
    df["month"] = df.date.dt.month
    df["month_name"] = df.date.dt.month_name()
    df["quarter"] = df.date.dt.quarter
    df["iso_year"] = df.date.dt.isocalendar().year
    df["is_leap_year"] = df.date.dt.is_leap_year
    df["day_of_year"] = df.date.dt.day_of_year
    df["days_in_month"] = df.date.dt.days_in_month
    df["is_month_end"] = df.date.dt.is_month_end
    df["is_month_start"] = df.date.dt.is_month_start
    df["is_quarter_end"] = df.date.dt.is_quarter_end
    df["is_quarter_start"] = df.date.dt.is_quarter_start
    df["is_year_end"] = df.date.dt.is_year_end
    df["is_year_start"] = df.date.dt.is_year_start
    return df


class Command(LoggingBaseCommand):
    """Command to create daily calendar table.
    """

    help = "This command is intented to run one-shot or on-demand to reduce/extend/update date-time range"

    def add_arguments(self, parser):
        """Add arguments to the command."""

        parser.add_argument(
            '--sd',
            dest="start_date",
            help='Start date')

        parser.add_argument(
            '--ed',
            dest="end_date",
            help='End date')

    def handle(self, *args, **options):
        df = create_calendar_daily()
        ETL(
            extractor=DataframeExtractor(df),
            loader=DjangoUpdateOrCreateLoader(django_model=CalendarDaily,
                                              fields_to_update=[]))()
