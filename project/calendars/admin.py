from django.contrib import admin
import datetime

from project.calendars.filters import YearFilter
from project.calendars.models import CalendarDaily

today = datetime.datetime.now()





@admin.register(CalendarDaily)
class CalendarDailyAdmin(admin.ModelAdmin):
    list_filter = ('date', YearFilter, 'month')
    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.filter(date__lte=today)

    def has_change_permission(self, request, obj=None):
        return False
    def has_add_permission(self, request):
        return False
    def has_delete_permission(self, request, obj=None):
        return False
