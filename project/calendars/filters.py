from django.contrib.admin import SimpleListFilter
import datetime

today = datetime.datetime.now()

class YearFilter(SimpleListFilter):
    title = 'year'

    parameter_name = 'year'

    def lookups(self, request, model_admin):
        qs = model_admin.model.objects.all()
        return [(str(i), str(i)) for i in qs.filter(date__lte=today).values_list('year', flat=True) \
                                  .distinct().order_by('-year')]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(year__exact=self.value())
