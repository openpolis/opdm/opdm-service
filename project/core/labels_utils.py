import re


deleghe_re = re.compile(r"^con deleg[ahe]+ a[glieoa\']*(.*)$")


def area_prep(loc, loc_type):
    """Return an articulate proposition for a given area, based on the loc_type

    :param loc: The region name
    :param loc_type: Regione, Comune or Provincia
    :return: strig
    """
    if loc_type != "R":
        return "di"

    if loc in ["Piemonte", "Trentino Alto Adige", "Friuli Venezia Giulia", "Veneto", "Lazio", "Molise"]:
        return "del"
    elif loc in ["Abruzzo", "Umbria", "Emilia Romagna"]:
        return "dell'"
    elif loc == "Marche":
        return "delle"
    else:
        return "della"


def context2adj(context):
    return {
        "reg": "regional",
        "regione": "regional",
        "regioni": "regional",
        "pro": "provincial",
        "prov": "provincial",
        "provincia": "provincial",
        "provincie": "provincial",
        "province": "provincial",
        "cm": "metropolitan",
        "metro": "metropolitan",
        "metropoli": "metropolitan",
        "com": "comunal",
        "comune": "comunal",
        "comuni": "comunal",
    }[context.lower()]


def organ2art(organ):
    organ = (
        organ.replace(" comunale", "")
        .replace(" provinciale", "")
        .replace(" metropolitana", "")
        .replace(" metropolitano", "")
        .replace(" regionale", "")
    )
    return {
        "giunta": "della",
        "conferenza": "della",
        "consiglio": "del",
        "governo della repubblica": "del",
        "assemblea parlamentare": "della",
        "organo legislativo internazionale o europeo": "del"
    }.get(organ, "per")


def build_charge_descr_from_opapi_data(charge_type, institution, description):
    """Return a charge, starting from fields in OPAPI

    :param charge_type: Assessore
    :param institution: Giunta Comunale
    :param description: Mobilità
    :return: Assessore con deleghe: mobilità
    """

    organ, context_adj = institution.split(" ")
    descr = charge_type
    if organ.lower() == "giunta":
        if charge_type == "Assessore" and description:
            descr = "{0} con deleghe: {1}".format(charge_type, description)
        elif charge_type in ["Presidente", "Vicepresidente"]:
            if context_adj.lower() == "regionale":
                inst = " della Regione"
            elif context_adj.lower() == "provinciale":
                inst = " della Provincia"
            else:
                inst = ""
            descr = "{0}{1}".format(charge_type, inst)

    elif organ.lower() == "consiglio":
        if charge_type == "Consigliere":
            descr = charge_type
        elif charge_type in ["Presidente", "Vicepresidente"]:
            descr = "{0} del Consiglio {1}".format(charge_type, context_adj.lower())

    return descr


def normalize_charge(charge_desc):
    """Normalize the charge, to a controlled dictionary of charge

    :param charge_desc:
    :return: string
    """
    norm_charge = charge_desc.lower()

    if "vicepresidente della" in norm_charge:
        return "Vicepresidente della Giunta"

    if "vicepresidente reggente della" in norm_charge:
        return "Vicepresidente della Giunta"

    if "vicepresidente supplente della" in norm_charge:
        return "Vicepresidente della Giunta"

    if "vicepresidente del" in norm_charge:
        return "Vicepresidente del Consiglio"

    if "vicepresidente reggente del" in norm_charge:
        return "Vicepresidente del Consiglio"

    if "delega funzioni da parte del sindaco" in norm_charge:
        return "Consigliere"

    if "vicesindaco metropolitano" in norm_charge:
        return "Vicesindaco metropolitano"

    if "vicesindaco" in norm_charge:
        return "Vicesindaco"

    if "presidente della" in norm_charge:
        return "Presidente della Giunta"

    if "presidente del" in norm_charge:
        return "Presidente del Consiglio"

    if "commissario" in norm_charge:
        return "Commissario"

    if "commissione" in norm_charge:
        return "Commissario"

    if "assessore" in norm_charge:
        return "Assessore"

    if "consigliere" in norm_charge:
        return "Consigliere"

    if "segretario" in norm_charge:
        return "Consigliere"

    return charge_desc


def charge2organ(charge_desc):
    """return the organ, given the charge

    'Assessore' => 'giunta'
    'Consigliere' => 'consiglio'

    :param charge_desc: charge description
    :return: organ where the charg has the post
    """
    translator = {
        "presidente della giunta": "giunta",
        "sindaco": "giunta",
        "sindaco metropolitano": "conferenza",
        "vicesindaco metropolitano": "conferenza",
        "commissario": "giunta",
        "vicepresidente della giunta": "giunta",
        "vicesindaco": "giunta",
        "assessore": "giunta",
        "presidente del consiglio": "consiglio",
        "vicepresidente del consiglio": "consiglio",
        "segretario del consiglio": "consiglio",
        "consigliere": "consiglio",
        "questore": "consiglio",
    }

    # charge normalization
    return translator[normalize_charge(charge_desc).lower()]


def build_role_for_local_adm(charge_desc, context):
    normalized_charge = normalize_charge(charge_desc)
    role = (
        "{0} {1}".format(normalized_charge, context2adj(context))
        .replace("regional", "regionale")
        .replace("provincial", "provinciale")
        .replace("comunal", "comunale")
        .replace("del Consiglio", "di Consiglio")
        .replace("Presidente della Giunta regionale", "Presidente di Regione")
        .replace("Presidente della Giunta provinciale", "Presidente di Provincia")
        .replace("Vicepresidente della Giunta regionale", "Vicepresidente di Regione")
        .replace("Vicepresidente della Giunta provinciale", "Vicepresidente di Provincia")
        .replace("Commissario regionale", "Commissario di giunta regionale")
        .replace("Commissario provinciale", "Commissario di giunta provinciale")
        .replace("Commissario comunale", "Commissario di giunta comunale")
        .replace("metropolitano metropolitan", "metropolitano")
        .replace("Consigliere metropolitan", "Consigliere metropolitano")
        .replace("Sindaco comunale", "Sindaco")
        .replace("Vicesindaco comunale", "Vicesindaco")
    )

    return role


def build_label_for_local_adm(charge_desc, org_name):
    """Build the correct label from partial information,
    avoiding pleonastic repetitions

    :param charge_desc:
    :param org_name:
    :return:
    """
    normalized_charge = normalize_charge(charge_desc)
    org_name = (
        org_name.title()
        .replace("Regionale", "regionale")
        .replace("Provinciale", "provinciale")
        .replace("Comunale", "comunale")
        .replace("Metropolitan", "metropolitan")
        .replace(" Del", " del")
        .replace(" Di ", " di ")
        .replace("Consiglio", "consiglio")
        .replace("Giunta", "giunta")
        .replace("Conferenza", "conferenza")
    )

    return (
        "{0} {1} {2}".format(normalized_charge, organ2art(charge2organ(normalized_charge)), org_name)
        .replace("Assessore della giunta", "Assessore")
        .replace("Consigliere del consiglio", "Consigliere")
        .replace("Sindaco della giunta comunale", "Sindaco")
        .replace("Vicesindaco della giunta comunale", "Vicesindaco")
        .replace("del Consiglio del consiglio", "del consiglio")
        .replace("della Giunta della giunta", "della giunta")
        .replace("Presidente della giunta regionale", "Presidente della Regione")
        .replace("Vicepresidente della giunta provinciale", "Vicepresidente della Provincia")
        .replace("Presidente della giunta provinciale", "Presidente della Provincia")
        .replace("Vicepresidente della giunta regionale", "Vicepresidente della Regione")
        .replace("Regione del ", "Regione ")
        .replace("Regione della ", "Regione ")
        .replace("Regione dell'", "Regione ")
        .replace("Sindaco metropolitano della conferenza metropolitana", "Sindaco metropolitano")
        .replace("Vicesindaco metropolitano della conferenza metropolitana", "Vicesindaco metropolitano")
    )
