import re
from datetime import datetime, timedelta

from codicefiscale import codicefiscale
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q

from project.akas.models import AKA
from project.core.birth_locations_data import remapped_birth_location
from project.core.exceptions import (
    UnprocessableEntitytAPIException,
    AKAException,
    SearchException,
)
from project.core import labels_utils
from project.core import organization_utils
from project.core import search_utils
from project.api_v1.search_indexes import PersonIndex
from popolo.models import (
    Person,
    Organization,
    OriginalProfession,
    OriginalEducationLevel,
    Area,
    Membership,
    KeyEvent,
    RoleType,
    Post,
    Identifier,
    Classification,
    PersonalRelationship,
)
from django.db import transaction

from atokaconn import AtokaConn, AtokaObjectDoesNotExist, AtokaException


def sync_solr_index(p):
    if p:
        index = PersonIndex()
        index.update_object(p)


def update_or_create_person_from_item(item, person_id=0, update_strategy="keep_old"):
    """do the actual person creation or update,
    starting from an item dict and a person_id (0 signals creating)

    the updateing strategy can be chosen, using the `update_strategy` parameter,
    based on the context of the operations
    it defaults to `keep_old`, where old values are kept

    :param item: dict containing details of the person to be created
    :param person_id: the id of the person (to update), 0 to create
    :param update_strategy: how to update Person
      - `keep_old`: only write fields that are empty, keeping old values
      - `overwrite`: overwrite all fields
      - `overwrite_minint_opdm`: partial overwrite
        see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali

    :return: person_instance, created
    """

    profession_name = item.pop("profession", None)
    if profession_name:
        original_profession, created = OriginalProfession.objects.get_or_create(
            name=profession_name
        )
    else:
        original_profession = None

    education_level_name = item.pop("education_level", None)
    if education_level_name:
        original_education_level, created = OriginalEducationLevel.objects.get_or_create(
            name=education_level_name
        )
    else:
        original_education_level = None

    person_fields = [
        f.name for f in Person._meta.fields if f.name not in ["start_date", "end_date"]
    ]
    person_defaults = {}
    for k, v in item.items():
        if k in person_fields:
            person_defaults[k] = v

    if "name" not in person_defaults:
        person_defaults["name"] = "{0} {1}".format(
            person_defaults["given_name"], person_defaults["family_name"]
        )

    if (
        "birth_location_area" not in person_defaults
        and "birth_location" in person_defaults
        and "birth_date" in person_defaults
    ):
        person_defaults["birth_location_area"] = get_area_from_birth_data(
            person_defaults["birth_location"], person_defaults["birth_date"]
        )

    person_defaults["original_profession"] = original_profession
    person_defaults["original_education_level"] = original_education_level
    if original_education_level:
        person_defaults["education_level"] = original_education_level.normalized_education_level

    contact_details = item.get("contact_details", [])
    other_names = item.get("other_names", [])
    identifiers = item.get("identifiers", [])
    sources = item.get("sources", [])
    links = item.get("links", [])
    classifications = item.get("classifications", [])

    if person_id < 0:
        raise ValueError("person_id must be greater than 0")

    with transaction.atomic():
        if person_id > 0:
            try:
                p = Person.objects.get(id=person_id)
            except Person.DoesNotExist:
                return None, False

            # update according to chosen update_strategy
            for k, v in person_defaults.items():
                if (
                    update_strategy == "overwrite"
                    or not getattr(p, k)
                    or update_strategy == "overwrite_minint_opdm"
                    and k in ["original_profession", "original_education_level", "education_level"]
                ):
                    setattr(p, k, v)
            p.save()
            created = False
        else:
            p = Person.objects.create(**person_defaults)
            created = True

        p.add_contact_details(contact_details)
        p.add_other_names(other_names)
        p.add_identifiers(identifiers)
        p.add_sources(sources)
        p.add_links(links)
        p.add_classifications(classifications)

    return p, created


def update_or_create_role(
    item,
    person,
    post,
    electoral_event,
    label=None,
    role_string=None,
    check_label=False,
    check_near_start_dates=True,
    near_start_date_lo=60,
    near_start_date_hi=180,
    logger=None,
    update_strategy="keep_old",
):
    """update or create a role (membership with post), given all needed info

    the updateing strategy can be chosen, using the `update_strategy` parameter,
    based on the context of the operations
    it defaults to `keep_old`, where old values are kept

    Presidents and Vicepresidents of local administrations councils are also consiglieri,
    so, existing memberships lookups need to take this into account
    in order to avoid importing a consigliere role where a council president or vicepresident one
    already exist.

    :param item: the dict containing role's data
    :param person: the Person instance
    :param post: the Post instance
    :param electoral_event: the KeyEvent instance
    :param label: label to be used for the membership (if None, then use post.label)
    :param role_string: role string to be used for the membership (if None, then use post.role)
    :param check_label: boolean, whether to check the label in update_or_create method
    :param check_near_start_dates: boolean, memberships with start_dates in a given neighborhood are updated,
           instead of created
    :param near_start_date_lo: integer, n. of days before the start_date
    :param near_start_date_hi: integer, n. of days after the start_date
    :param logger: logger to use
    :param update_strategy: how to update Membership
      - `keep_old`: only write fields that are empty, keeping old values
      - `overwrite`: overwrite all fields
      - `overwrite_minint_opdm`: partial overwrite
        see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
    :return:
    """

    # look for membership within +-6months interval
    try:
        start_date_dt = datetime.strptime(item["start_date"], "%Y-%m-%d")
    except ValueError:
        try:
            start_date_dt = datetime.strptime(item["start_date"], "%Y-%m")
        except ValueError:
            try:
                start_date_dt = datetime.strptime(item["start_date"], "%Y")
            except ValueError as e:
                raise e

    start_date_lo = (start_date_dt - timedelta(near_start_date_lo)).strftime("%Y-%m-%d")
    start_date_hi = (start_date_dt + timedelta(near_start_date_hi)).strftime("%Y-%m-%d")

    contact_details = item.get("contact_details", [])
    sources = item.get("sources", [])
    links = item.get("links", [])

    area_id = item.get("area_id", None)

    disable_check_label = False

    # avoid multiple duplicated roles (our convention)
    match = re.match(
        r"(consigliere|assessore) (regionale|provinciale|comunale)",
        post.role.lower(),
        re.IGNORECASE,
    )
    if match and match.lastindex == 2:
        typ = match.group(1)
        ctx = match.group(2)

        if typ == "consigliere":
            equivalent_posts_roles = [
                "Consigliere {0}".format(ctx),
                "Presidente di consiglio {0}".format(ctx),
                "Vicepresidente di consiglio {0}".format(ctx),
            ]
        else:
            equivalent_posts_roles = [
                "Assessore {0}".format(ctx),
                "Presidente di giunta {0}".format(ctx),
                "Vicepresidente di giunta {0}".format(ctx),
                "Presidente di regione",
                "Vicepresidente di regione",
                "Sindaco",
                "Vicesindaco",
                "Vicesindaco reggente"
            ]
        m = Membership.objects.filter(
            person=person, post__role__iregex=r'(' + '|'.join(equivalent_posts_roles) + ')'
        )
    else:
        if (
            'componente commissione' in post.role.lower() or
            'componente giunta' in post.role.lower()
        ):
            equivalent_posts_roles = [
                post.role,
                post.role.replace('Componente', 'Capogruppo')
            ]
            filters_dict = {
                'person': person,
                'post__organization': post.organization,
                'post__role__in': equivalent_posts_roles
            }

            m = Membership.objects.filter(**filters_dict)
            disable_check_label = True
        else:
            m = Membership.objects.filter(person=person, post=post)

    if check_near_start_dates:
        m = m.filter(
            Q(start_date__gte=start_date_lo) & Q(start_date__lte=start_date_hi)
            | Q(start_date=item["start_date"][:4])
        )
    else:
        m = m.filter(start_date=item["start_date"])

    if item.get("end_date", None) and item["start_date"] != item["end_date"]:
        m = m.exclude(start_date=item["end_date"])

    if area_id:
        m = m.filter(area_id=area_id)

    if label and check_label and not disable_check_label:
        m = m.filter(label=label)

    created = False
    if m.count() > 1:
        # handle error
        if logger:
            logger.warning(
                " more than one membership found for {0}, {1}".format(person, post)
            )
        membership = None
    else:
        if m.count() == 1:
            # one membership was found and is now updated
            # according to chosen update_strategy
            membership = m.first()
            memberships_defaults = {
                "start_date": item["start_date"],
                "end_date": item.get("end_date", None),
                "end_reason": item.get("end_reason", None),
                "electoral_event": electoral_event,
                "electoral_list_descr_tmp": item.get(
                    "electoral_list_descr_tmp", item.get("party_list_coalition", None)
                ),
                "constituency_descr_tmp": item.get("constituency_descr_tmp", None),
                "label": label if label else post.label,
                "role": role_string if role_string else post.role,
            }
            to_save = False
            for k, v in memberships_defaults.items():
                if (
                    not getattr(membership, k)
                    or update_strategy == "overwrite"
                    or (
                        v is not None
                        and update_strategy == "overwrite_minint_opdm"
                        and k in ["start_date", "electoral_list_descr_tmp"]
                        and not (
                            "non osservat" in v.lower()
                            or "sconosciut" in v.lower()
                            or "non not" in v.lower()
                        )
                    ) or (
                        "overwrite" in update_strategy and
                        k == "end_date" and v is not None
                    ) or (
                        "overwrite" in update_strategy and
                        k == "end_reason" and v is not None
                    )
                ):
                    to_save = True
                    setattr(membership, k, v)
            if to_save:
                membership.save()
        else:
            # no memberships were found, and one is now created
            membership = person.add_role(
                post=post,
                start_date=item["start_date"],
                end_date=item.get("end_date", None),
                electoral_event=electoral_event,
                electoral_list_descr_tmp=item.get(
                    "electoral_list_descr_tmp", item.get("party_list_coalition", None)
                ),
                constituency_descr_tmp=item.get("constituency_descr_tmp", None),
                area_id=item.get("area_id", None),
                label=label if label else post.label,
                role=role_string if role_string else post.role,
                check_label=check_label,
            )
            if membership:
                created = True
            else:
                if logger:
                    logger.warning(
                        " no membership could be created for {0}, {1}, "
                        "start_date:{2}, end_date:{3} (overlapping?)".format(
                            person, post, item["start_date"], item.get("end_date", "-")
                        )
                    )

        # add source if item has it
        if "source" in item and membership:
            membership.add_source(item["source"])

        if membership:
            membership.add_contact_details(contact_details)
            membership.add_sources(sources)
            membership.add_links(links)

    return membership, created


def update_or_create_membership(
    item,
    person,
    organization,
    election,
    label=None,
    role_string=None,
    logger=None,
    update_strategy="keep_old",
    check_near_start_dates=True,
    near_start_date_lo=60,
    near_start_date_hi=180,
):
    """update or create a membership (no Post), given all needed info

    the updateing strategy can be chosen, using the `update_strategy` parameter,
    based on the context of the operations
    it defaults to `keep_old`, where old values are kept

    :param item:
    :param person:
    :param organization:
    :param election:
    :param label: label to be used for the membership (if None, then use post.label)
    :param role_string: role string to be used for the membership (if None, then use post.role)
    :param logger: logger to use
    :param update_strategy: how to update Membership
      - `keep_old`: only write fields that are empty, keeping old values
      - `overwrite`: overwrite all fields
      - `overwrite_minint_opdm`: partial overwrite
        see: https://gitlab.depp.it/openpolis/opdm/opdm-project/wikis/import/update-amministratori-locali
    :param check_near_start_dates: boolean, memberships with start_dates in a given neighborhood are updated,
           instead of created
    :param near_start_date_lo: integer, n. of days before the start_date
    :param near_start_date_hi: integer, n. of days after the start_date
    :return:
    """

    # look for membership within +-6months interval
    try:
        start_date_dt = datetime.strptime(item["start_date"], "%Y-%m-%d")
    except ValueError:
        try:
            start_date_dt = datetime.strptime(item["start_date"], "%Y-%m")
        except ValueError:
            try:
                start_date_dt = datetime.strptime(item["start_date"], "%Y")
            except ValueError as e:
                raise e
    start_date_lo = (start_date_dt - timedelta(near_start_date_lo)).strftime("%Y-%m-%d")
    start_date_hi = (start_date_dt + timedelta(near_start_date_hi)).strftime("%Y-%m-%d")

    m = Membership.objects.filter(person=person, organization=organization)
    if check_near_start_dates:
        m = m.filter(
            Q(start_date__gte=start_date_lo) & Q(start_date__lte=start_date_hi)
        )
    else:
        m = m.filter(start_date=item["start_date"])

    created = False
    if m.count() > 1:
        # handle error
        if logger:
            logger.warning(
                " more than one membership found for {0} in {1}".format(
                    person, organization
                )
            )
        membership = None
    else:
        if m.count() == 1:
            # one membership was found and is now updated
            # according to chosen update_strategy
            membership = m.first()
            membership_defaults = {
                "start_date": item["start_date"],
                "end_date": item.get("end_date", None),
                "electoral_event": election,
                "electoral_list_descr_tmp": item.get("party_list_coalition", None),
                "label": label if label else None,
                "role": role_string if role_string else None,
            }
            to_save = False
            for k, v in membership_defaults.items():
                if (
                    update_strategy == "overwrite"
                    or not getattr(membership, k)
                    or update_strategy == "overwrite_minint_opdm"
                    and k in ["start_date", "electoral_list_descr_tmp"]
                    or "overwrite" in update_strategy
                    and k == "end_date"
                    and item.get("end_date", None) is not None
                ):
                    if v:
                        to_save = True
                        setattr(membership, k, v)
            if to_save:
                membership.save()
        else:
            # no memberships were found, and one is now created
            membership = person.add_membership(
                organization,
                start_date=item["start_date"],
                end_date=item.get("end_date", None),
                electoral_event=election,
                electoral_list_descr_tmp=item.get("party_list_coalition", None),
                label=label if label else None,
                role=role_string if role_string else None,
            )
            if membership:
                created = True
            else:
                if logger:
                    logger.warning(
                        " no membership could be created for {0} in {1}, "
                        "start_date:{2}, end_date:{3} (overlapping?)".format(
                            person,
                            organization,
                            item["start_date"],
                            item.get("end_date", "-"),
                        )
                    )

        # add source if item has it
        if "source" in item and membership:
            membership.add_source(item["source"])

    return membership, created


def update_or_create_person_and_details_from_item(
    person_id,
    item,
    loader_context=None,
    persons_update_strategy=None,
    memberships_update_strategy=None,
    ownerships_update_strategy=None,
    check_membership_label=False,
    check_near_start_dates=True,
    near_start_date_lo=60, near_start_date_hi=180,
    logger=None,
):
    """Updates both Person and related Membership/Ownership instances.

    Used in loaders, both in `PopoloPersonMembershipLoader`, `PopoloPersonWithRelatedDetailsLoader` and
    in `PopoloPersonWithMembershipsLoader`, that have different item structure.

    A single function call will make simpler a refactoring towards a unified Loader.

    :return: tuple (Person, bool), the person instance and whether it was created
    """

    # pop info out of item, in order not to pass unknown or wrong arguments to update_or_create...
    role_type_label = item.pop("role_type_label", None)

    try:
        person, person_created = update_or_create_person_from_item(
            item, person_id, update_strategy=persons_update_strategy,
        )
    except Exception as e:
        raise UnprocessableEntitytAPIException(detail=str(e))
    else:
        sync_solr_index(person)

    # compute CF and silently handle exceptions
    try:
        cf = compute_cf(item)
    except CFException:
        cf = None

    # add CF identifier to person (add_identifier avoids duplicates)
    if cf and person_created:
        person.add_identifier(cf, "CF", overwrite_overlapping=True)

    if "memberships" not in item and "ownerships" not in item:
        if loader_context is None:
            raise UnprocessableEntitytAPIException(
                "loader_context must not be None when loading membership {0}".format(
                    item
                )
            )

        # update or create Membership from post_id, election_id
        election_id = loader_context.get("election_id", None)
        if election_id:
            election = KeyEvent.objects.get(pk=election_id)
        else:
            election = None

        post_id = loader_context.get("post_id", None)
        organization_id = loader_context["item"].get("org_id", None)

        if post_id:
            post = Post.objects.get(pk=post_id)
            try:
                p, created = update_or_create_role(
                    item,
                    person,
                    post,
                    election,
                    label=role_type_label,
                    update_strategy=memberships_update_strategy,
                    check_label=item.get(
                        "check_membership_label", check_membership_label
                    ),
                    check_near_start_dates=check_near_start_dates,
                    near_start_date_lo=near_start_date_lo,
                    near_start_date_hi=near_start_date_hi,
                )
                if logger:
                    if created:
                        logger.info("    {0} created".format(p))
                    else:
                        logger.debug("    {0} updated".format(p))

            except Exception as e:
                raise UnprocessableEntitytAPIException(
                    "{0} when loading membership {1}".format(e, item)
                )
        elif organization_id:
            try:
                org = Organization.objects.get(pk=organization_id)
            except Organization.DoesNotExist:
                if logger:
                    logger.error(
                        "Organization could not be found for {0}".format(
                            organization_id
                        )
                    )
            else:
                try:
                    m, created = update_or_create_membership(
                        item,
                        person,
                        org,
                        election,
                        label=role_type_label,
                        update_strategy=memberships_update_strategy,
                        check_near_start_dates=check_near_start_dates,
                        near_start_date_lo=near_start_date_lo,
                        near_start_date_hi=near_start_date_hi,
                    )
                    if logger:
                        if created:
                            logger.info("    {0} created".format(m))
                        else:
                            logger.debug("    {0} updated".format(m))

                except Exception as e:
                    raise UnprocessableEntitytAPIException(
                        "{0} when loading membership {1}".format(e, item)
                    )
        else:
            raise UnprocessableEntitytAPIException(
                "post_id must be specified in loader_context, or org_id in loader_context['item']"
            )
    else:
        for m in item.get("memberships", []):

            # skip memberhips with no start_date
            if "start_date" not in m or m["start_date"] is None:
                if logger:
                    logger.error("No start date for membership {0}".format(m))
                continue

            # get or create the Post this role refers to, from role and organization_id
            # memberships must have been "annotated" with
            # Electoral KeyEvent, Organization and RoleType instances during transform()
            try:
                organization = Organization.objects.get(pk=m["organization_id"])
            except Organization.DoesNotExist:
                if logger:
                    logger.error(
                        "Organization could not be found for {0}".format(
                            m["organization_id"]
                        )
                    )
                continue
            try:
                role_type = RoleType.objects.get(label__iexact=m["role"])
            except RoleType.DoesNotExist:
                if logger:
                    logger.error(
                        "RoleType could not be found for {0}".format(m["role"])
                    )
                continue

            electoral_event_id = m.get("electoral_event_id", None)
            if electoral_event_id:
                electoral_event = KeyEvent.objects.get(id=electoral_event_id)
            else:
                electoral_event = None
            if organization.classification:
                prep = labels_utils.organ2art(organization.classification.lower())
            else:
                prep = "per"
            try:
                post, post_created = Post.objects.get_or_create(
                    role_type=role_type,
                    organization=organization,
                    defaults={
                        "label": "{0} {1} {2}".format(m["role"], prep, organization.name),
                        "role": m["role"],
                    },
                )
            except Post.MultipleObjectsReturned as e:
                raise UnprocessableEntitytAPIException(
                    "{0} when loading membership {1}".format(e, item)
                )

            try:
                r, created = update_or_create_role(
                    m,
                    person,
                    post,
                    electoral_event,
                    label=m["label"],
                    update_strategy=memberships_update_strategy,
                    check_label=item.get(
                        "check_membership_label", check_membership_label
                    ),
                    check_near_start_dates=check_near_start_dates,
                    near_start_date_lo=near_start_date_lo,
                    near_start_date_hi=near_start_date_hi,
                )
                if logger:
                    if created:
                        logger.info("Membership {0} created".format(r))
                    else:
                        logger.debug("Membership {0} updated".format(r))

            except Exception as e:
                if logger:
                    logger.error("{0} when loading membership {1}".format(e, m))

        for o in item.get("ownerships", []):
            # get or create the person's Ownership of the given organization_id
            try:
                organization_id = o["organization_id"]
            except KeyError:
                if logger:
                    logger.error("Organization id could not be found in ownership")
                continue

            try:
                owned_organization = Organization.objects.get(pk=organization_id)
            except Organization.DoesNotExist:
                if logger:
                    logger.error(
                        "Organization could not be found for {0}".format(
                            organization_id
                        )
                    )
                continue

            try:
                # invoke class method to create or update an ownership out of an item
                ow, created = organization_utils.update_or_create_ownership_from_item(
                    person,
                    owned_organization,
                    o,
                    update_strategy=ownerships_update_strategy,
                )
                if logger:
                    if created:
                        logger.info("Ownership {0} created".format(ow))
                    else:
                        logger.debug("Ownership {0} updated".format(ow))
            except Exception as e:
                if logger:
                    logger.error(
                        "Error {0} when loading ownership for {1} owning {2}% of {3} created".format(
                            e, person_id, round(float(item["percentage"]), 2), organization_id
                        )
                    )

    return person, person_created


class CFException(Exception):
    pass


def compute_cf(item):
    """

    :param item:
    :return:
    """
    if (
        all(
            f in item
            for f in [
                "family_name",
                "given_name",
                "gender",
                "birth_date",
                "birth_location",
            ]
        )
        and item["birth_date"]
        and item["birth_location"]
    ):
        try:
            cf = codicefiscale.encode(
                surname=item["family_name"],
                name=item["given_name"],
                sex=item["gender"],
                birthdate=datetime.strptime(item["birth_date"], "%Y-%m-%d").strftime(
                    "%d/%m/%Y"
                ),
                birthplace=remapped_birth_location(item["birth_location"].strip()),
            )
            return cf
        except ValueError as e:
            raise CFException(e)
        except Exception as e:
            raise CFException(e)

    return None


def get_area_from_birth_data(location, date):
    """return area from birth_location and birth_date

    :param location: birth_location string, as in Roma (RM)
    :param date: birth_date, as in 1968-05-25
    :return: Area instance, or None
    """
    if not location:
        return None

    m = re.search(r"(\w+) \((\w+)\)", location)

    area_qs = Area.objects.filter(istat_classification="COM")
    if date:
        area_qs = area_qs.filter(
            Q(start_date__lte=date) | Q(start_date__isnull=True)
        ).filter(Q(end_date__gt=date) | Q(end_date__isnull=True))

    if m:
        area_qs = area_qs.filter(name__iexact=m.group(1))
        area_qs = area_qs.filter(parent__identifier=m.group(2))
    else:
        area_qs = area_qs.filter(name__iexact=location)

    return area_qs.first()


def search_anagraphical(item, source_url):
    # anagraphical lookup strategy: use solr to lookup existing Persons, before creating new
    # create similarities records to be solved (AKAs), if some are found
    search_params = {}
    search_fields = ["given_name", "family_name", "birth_date", "birth_location"]
    for k, v in item.items():
        if k in search_fields:
            if v is not None:
                if k == "birth_location":
                    search_params[k] = re.sub(r"(.*) \(.*\)", r"\1", v.lower())
                elif k in ["given_name", "family_name"]:
                    search_params[k] = v.lower().replace("(", r"\(").replace(")", r"\)")
                else:
                    search_params[k] = v.lower()
            else:
                search_params[k] = ""

    non_empty_search_params = {k: v for k, v in search_params.items() if v != ""}
    resolved_aka = AKA.objects.filter(search_params=search_params, is_resolved=True)
    # limit search to AKAs generated by the same URL for records with only 2 fields (family_name, given_name)
    if len(non_empty_search_params) <= 2:
        resolved_aka = resolved_aka.filter(data_source_url=source_url)

    if resolved_aka.count() > 0:
        # get person_id from found AKA
        a = resolved_aka.first()
        p = a.content_object

        # if a similarity was already solved, and the attached object was removed,
        # then log the error and return -1
        if p is None:
            raise SearchException(
                f"A solved similarity was found for aka_id: {a.id}, search: {search_params},"
                f" but points to a non-existing object."
                f" Please launch the script_create_persons_from_solved_akas task to correct this."
            )

        queryset = [Person.objects.get(pk=p.id)]
        queryset[0].score = 0.0

    else:
        queryset = search_utils.search_person_by_anagraphics(**search_params)

    return queryset, search_params


def search_identifier(item, identifier_scheme=None):
    # identifier lookup strategy: use given scheme or lookup on first identifier
    search_params = {}
    if identifier_scheme:
        d = {x["scheme"]: x["identifier"] for x in item["identifiers"]}
        scheme = identifier_scheme
        identifier = d[scheme]
    else:
        if "identifiers" in item and len(item["identifiers"]):
            scheme = item["identifiers"][0]["scheme"]
            identifier = item["identifiers"][0]["identifier"]
        elif "identifier" in item:
            scheme = "CF"
            identifier = item["identifier"]
        else:
            return [], search_params
    search_params["%s_s" % scheme] = str(identifier)
    queryset = search_utils.search_person_by_identifier(**search_params)

    return queryset, search_params


def person_lookup(
    item,
    strategy,
    loader_context=None,
    aka_lo=settings.AKA_LO_THRESHOLD,
    aka_hi=settings.AKA_HI_THRESHOLD,
    **kwargs,
):
    """
    :param item:
    :param strategy: lookup strategy (anagraphical, identifier)
    :param loader_context: dict to be stored in AKA instance, if needed
    :param aka_lo: threshold below which a similarity is resolved as a NO
    :param aka_hi: threshold aboce which a similarity is resolved as a YES
    :param kwargs:   other params
      - identifier_scheme
      - logger
    :return: person_id
      0   - Not found => create
      > 0 - found => update
      < 0 - unknown => n similarities were added,
    """

    if loader_context is None:
        loader_context = {}

    identifier_scheme = kwargs.get("identifier_scheme", None)
    logger = kwargs.get("logger", None)

    # check existing AKAs shortcuts before performing any search
    sources = item.get("sources", [])
    if sources:
        source_url = sources[0]["url"]
    else:
        source_url = loader_context.get("data_source", None)

    # assume not found as base value
    person_id = 0

    search_params = {}
    try:
        if strategy == "anagraphical":
            queryset, search_params = search_anagraphical(item, source_url)
        elif strategy in ["identifier", "mixed"]:
            queryset, search_params = search_identifier(item, identifier_scheme=identifier_scheme)
            if len(queryset) == 0:
                queryset, search_params = search_anagraphical(item, source_url)
        else:
            raise Exception("accepted values for lookup_strategy are: akas, identifier")

    except SearchException as e:
        if logger:
            logger.error("{0} for {1}".format(str(e), str(search_params)))
    else:
        # filter out results below threshold,
        # keep first 30 results
        queryset = [
            res for res in queryset if res.score >= aka_lo or res.score == 0.0
        ][:30]

        if len(queryset) >= 1:
            # check if more than one results above thresholds
            hi_res = [
                res for res in queryset if res.score > aka_hi or res.score == 0.0
            ]
            if len(hi_res) == 0 or len(hi_res) > 1:

                # if more than one result above threshold, generate similarities only about them
                if len(hi_res) > 1:
                    queryset = hi_res

                # AKAs generation

                # remove memberships from loader_context, as it's
                # not necessary and contains non-serializable keys
                if "memberships" in loader_context["item"]:
                    for m in loader_context["item"]["memberships"]:
                        m.pop("organization", None)
                        m.pop("role_type", None)

                aka, created = AKA.objects.update_or_create(
                    search_params=search_params,
                    loader_context=loader_context,
                    data_source_url=source_url,
                    defaults={"n_similarities": len(queryset)},
                )
                if logger:
                    logger.info(
                        "  {0} similarities found for search {1}".format(
                            aka.n_similarities, str(aka.search_params)
                        )
                    )

                # return negative value of similarities
                person_id = -1 * aka.n_similarities
            else:
                # if just one result is above hi threshold, then it's him
                person_id = queryset[0].id

    return person_id


def verify_tax_id_with_atoka(p: Person, atoka_conn: AtokaConn):
    """Perform a verification using the ATOKA API,
    raise verbose Exceptions whenever something is wrong.
    """
    tax_id = p.tax_id

    if tax_id is None:
        raise Exception("No tax_id found in OPDM!")
    try:
        atoka_p = atoka_conn.get_person_from_tax_id(tax_id)
    except AtokaObjectDoesNotExist:
        try:
            atoka_p = atoka_conn.search_person(p)
        except AtokaException as e:
            raise Exception(e)
        else:
            # update existing CF or create it
            Identifier.objects.update_or_create(
                content_type_id=ContentType.objects.get(
                    app_label="popolo", model="person"
                ).id,
                object_id=p.id,
                scheme="CF",
                defaults={"identifier": atoka_p["base"]["taxId"]},
            )
            p.is_identity_verified = True
            p.save()
            p.add_identifier(atoka_p["id"], "ATOKA_ID")
    except AtokaException as e:
        raise Exception(e)
    else:
        p.is_identity_verified = True
        p.save()
        p.add_identifier(atoka_p["id"], "ATOKA_ID")


def populate_other_names_from_aka(aka: AKA) -> str:
    """Populate the `other_names` field with name extracted from given AKA

    :param aka:
    :return: the aka_name added or None
    """
    if isinstance(aka.loader_context["item"], dict):
        if "name" in aka.loader_context["item"]:
            aka_name = aka.loader_context["item"]["name"]
        elif "given_name" in aka.loader_context["item"]:
            aka_name = "{0} {1}".format(
                aka.loader_context["item"]["given_name"],
                aka.loader_context["item"]["family_name"],
            )
        else:
            raise AKAException("Could not find proper name for aka {0}".format(aka.id))
    else:
        aka_name = " ".join(aka.loader_context["item"].split("+")[:2])

    if "source" in aka.loader_context["item"]:
        aka_source = aka.loader_context["item"]["source"]
    elif "source" in aka.loader_context:
        aka_source = aka.loader_context["source"]
    else:
        aka_source = aka.loader_context.get("data_source", None)

    if aka.content_object is None:
        raise AKAException("Could not find content_object for aka {0}".format(aka.id))

    if aka.content_object.name.lower() != aka_name.lower():
        aka.content_object.add_other_name(
            aka_name, othername_type="AKA", source=aka_source
        )
        return aka_name


def update_or_create_personal_relationship(
    source_person: Person,
    dest_person: Person,
    item,
    update_strategy="keep_old",
):

    from django.utils.translation import ugettext_lazy

    # filter relationships among persons
    related_persons_qs = PersonalRelationship.objects.filter(
        source_person=source_person, dest_person=dest_person,
        classification__code='KINSHIP',
    )

    created = False

    with transaction.atomic():
        new_classification, _ = Classification.objects.get_or_create(
            scheme="OP_TIPO_RELAZIONE_PERS",
            code='KINSHIP',
            descr=ugettext_lazy('Kinship')
        )
        if related_persons_qs.count() == 1:
            # one membership was found and is now updated
            # according to chosen update_strategy
            personal_relationship = related_persons_qs.first()
            relationship_defaults = {
                "classification": new_classification,
                "descr": item.get("description", None),
                "start_date": item.get("start_date", None),
                "end_date": item.get("end_date", None),
            }
            to_save = False
            for k, v in relationship_defaults.items():
                if update_strategy == "overwrite" or not getattr(
                    personal_relationship, k
                ):
                    if v:
                        to_save = True
                        setattr(personal_relationship, k, v)
            if to_save:
                personal_relationship.save()
        else:
            # no existing relationships were found, and one is now created
            personal_relationship = source_person.add_relationship(
                dest_person,
                classification=new_classification,
                descr=item.get("description", None),
                start_date=item.get("start_date", None),
                end_date=item.get("end_date", None),
            )
            if personal_relationship:
                created = True

        # add related fields
        if personal_relationship:
            personal_relationship.add_sources(item.get("sources", []))

    return personal_relationship, created
