from django.db import transaction
from popolo.models import Organization, Identifier

from project.api_v1.search_indexes import OrganizationIndex


def sync_solr_index(item):
    index = OrganizationIndex()
    index.update_object(item)


def org_anagraphical_lookup(item, logger=None, current=False):
    """anagraphical lookup strategy implementation

    :param item:    the item to lookup in the DB
    :param logger:  the logger
    :param current: only look for current data (dissolution_date None)
    :return: the id if found, 0 if not found, negative number of multiples if multiples found
    """

    org_id = 0
    basic_filters = {"name__iexact": item["name"]}
    discrimination_filter = None
    if current:
        basic_filters.update({"dissolution_date__isnull": True})
    if 'founding_date' in item:
        discrimination_filter = {"founding_date": item.get("founding_date")}

    filters = basic_filters
    try:
        org = Organization.objects.get(**filters)
        org_id = org.id
    except Organization.DoesNotExist:
        pass
    except Organization.MultipleObjectsReturned:
        if 'founding_date' in item:
            filters.update(discrimination_filter)
            try:
                org = Organization.objects.get(**filters)
                org_id = org.id
            except Organization.DoesNotExist:
                if logger:
                    logger.warning("Could not find organization by name, cf and founding date: {0}".format(item))
            except Organization.MultipleObjectsReturned:
                org_id = -1 * Organization.objects.filter(**filters).count()
        else:
            org_id = -1 * Organization.objects.filter(**filters).count()

    return org_id


def org_identifier_lookup(item, identifier_scheme, current=False, logger=None):
    """identifier lookup strategy implementation

    :param item:   the item to lookup in the DB
    :param identifier_scheme: the scheme to use in the lookup
    :param current: whether to fetch only current organizations
    :param logger: the logger, if it had to be used
    :return: the id if found, 0 if not found, negative number of multiples if multiples found
    """

    org_id = 0
    if identifier_scheme:
        d = {x["scheme"]: x["identifier"] for x in item["identifiers"]}
        scheme = identifier_scheme
        identifier = d.get(scheme, None)

        # if scheme is not among used identifiers, then it's a NOT FOUND
        if identifier is None:
            return 0
        filters = {"identifiers__scheme": scheme, "identifiers__identifier": identifier}
    else:
        # DEPRECATED
        identifier = item['identifier']
        filters = {"identifier": identifier}

    if current:
        filters.update({"dissolution_date__isnull": True})

    try:
        org = Organization.objects.get(**filters)
        org_id = org.id
    except Organization.DoesNotExist:
        if not identifier_scheme or identifier_scheme == 'CF':
            if 'identifier' in filters:
                del filters['identifier']
            if 'identifiers__identifier' in filters:
                del filters['identifiers__identifier']
                del filters['identifiers__scheme']
            filters.update({
                'identifiers__scheme': 'ALTRI_CF_ATOKA',
                'identifiers__identifier__icontains': identifier
            })
            try:
                org = Organization.objects.get(**filters)
                org_id = org.id
            except Organization.DoesNotExist:
                pass
            except Organization.MultipleObjectsReturned:
                org_id = -1 * Organization.objects.filter(**filters).count()
    except Organization.MultipleObjectsReturned:
        # if more than one orgs found, then try to specialise the identifier lookup with dates
        orgs = Organization.objects.filter(**filters)

        founding_date = item.get('founding_date', None)
        if founding_date:
            orgs = orgs.filter(
                founding_date__gte=founding_date
            )
        dissolution_date = item.get('dissolution_date', None)
        if dissolution_date:
            orgs = orgs.filter(
                dissolution_date__lte=dissolution_date
            )
        else:
            orgs = orgs.filter(
                dissolution_date__isnull=True
            )

        orgs_count = orgs.count()
        if orgs_count == 0:
            pass
        elif orgs_count == 1:
            org_id = orgs.first().id
        else:
            org_id = -1 * orgs_count

    return org_id


def is_private_org(item):
    """check if item to lookup is classified as private

        item has this structure:
        {
            'name': "AGENZIA NAZIONALE PER L'ATTRAZIONE DEGLI INVESTIMENTI E LO SVILUPPO D'IMPRESA S.P.A.",
            'classifications': [{'classification': 11}],
            ...
        }
    """
    for c in item.get('classifications', []):
        cl = c.get('classification', None)
        if cl in [
            11, 20, 24, 29, 48, 69, 83, 295, 321, 346,
            403, 621, 941, 730, 1182, 1183, 1184, 1185, 1186, 1187,
            1188, 1190, 1189, 1191, 1192, 1193, 1194, 1195, 1196,
            1197, 1198, 1199, 1200, 1201, 1202,
            1620, 1630, 2740
        ]:
            return True

    return False


def org_lookup(item, strategy, **kwargs):
    """
    :param item:
    :param strategy: lookup strategy (anagraphical, identifier, mixed_current, anagraphical_current, ...)
    :param kwargs:   other params
      - identifier_scheme
      - logger
      - private_company_verification (enable verification of private org before anagraphical search in mixed strategy)
    :return: organization_id
      0   - Not found => create
      > 0 - found => update
    """

    logger = kwargs.get("logger", None)
    private_company_verification = kwargs.get("private_company_verification", False)
    identifier_scheme = kwargs.get("identifier_scheme", None)

    try:
        strategy, current = strategy.split("_")
        current = True
    except ValueError:
        strategy, current = strategy, False

    if strategy == "anagraphical":
        org_id = org_anagraphical_lookup(item, logger=logger, current=current)

    elif strategy in ["identifier", "mixed"]:
        # identifier lookup strategy: use given scheme or lookup on natural identifier (CF)
        # if not found a try with the anagraphical lookup strategy is done
        org_id = org_identifier_lookup(item, identifier_scheme, logger=logger)

        # mixed strategy: try the anagraphical lookup strategy if identifier lookup failed
        if org_id == 0 and strategy == "mixed":
            if not (private_company_verification and is_private_org(item)):
                org_id = org_anagraphical_lookup(item, logger=logger, current=current)
                if org_id:
                    org = Organization.objects.get(id=org_id)
                    if item.get('classification', None) != org.classification:
                        logger.warning(f"Classification mismatch for {item}, lookup will fail")
                        org_id = 0
    else:
        raise Exception("accepted values for lookup_strategy are: anagraphical, identifier, mixed")

    return org_id


def update_or_create_org_from_item(item, org_id=0, update_strategy="keep_old"):
    """do the actual organization creation or update,
    starting from an item dict and a org_id (0 signals creating)

    the updating strategy can be chosen, using the `update_strategy` parameter,
    based on the context of the operations
    it defaults to `keep_old`, where old values are kept

    :param item: dict containing details of the organization to be created
    :param org_id: the id of the organization (to update), 0 to create
    :param update_strategy: how to update Person
      - `keep_old`: only write fields that are empty, keeping old values
      - `keep_old_overwrite_cf`: like `keep_old`, but the cf identifiers is overwritten,
        and the old one put into the `ALTRI_ATOKA_CF` identifier
      - `overwrite`: overwrite all fields

    :return: person_instance, created
    """

    org_fields = [f.name for f in Organization._meta.fields if f.name not in ["start_date", "end_date"]]
    org_defaults = {}
    for k, v in item.items():
        if k in org_fields:
            if k in ["parent", "area"]:
                org_defaults["{0}_id".format(k)] = v
            else:
                org_defaults[k] = v

    key_events = item.get("key_events", [])
    contact_details = item.get("contact_details", [])
    identifiers = item.get("identifiers", [])
    classifications = item.get("classifications", [])
    other_names = item.get("other_names", [])
    sources = item.get("sources", [])
    links = item.get("links", [])

    if org_id < 0:
        raise ValueError("org_id must be greater than 0")

    with transaction.atomic():
        if org_id > 0:
            try:
                o = Organization.objects.get(id=org_id)
            except Organization.DoesNotExist:
                return None, False

            # update according to chosen update_strategy
            for k, v in org_defaults.items():
                if update_strategy == "overwrite" or not getattr(o, k):
                    setattr(o, k, v)
            o.save()
            created = False
        else:
            o = Organization.objects.create(**org_defaults)
            created = True

        o.add_key_events(key_events)
        o.add_contact_details(contact_details)

        # if a new CF identifier is present, then check if it must be overwritten and if so, remove it
        # store the old CF into an `ALTRI_CF_ATOKA` identifier
        if update_strategy == 'keep_old_overwrite_cf' and 'CF' in [i['scheme'] for i in identifiers]:
            try:
                cf_identifier = o.identifiers.get(scheme='CF')
                old_cf = cf_identifier.identifier
                new_cf = next(i['identifier'] for i in identifiers if i['scheme'] == 'CF')
                if new_cf != old_cf:
                    cf_identifier.delete()
                    more_cf_identifier = o.identifiers.filter(scheme='ALTRI_CF_ATOKA').first()
                    if more_cf_identifier:
                        more_cfs = more_cf_identifier.identifier.split(',')
                        if old_cf not in more_cfs:
                            more_cfs.append(old_cf)
                        if new_cf in more_cfs:
                            more_cfs.remove(new_cf)
                        more_cf_identifier.identifier = ",".join(more_cfs)
                        more_cf_identifier.save()
                    else:
                        o.add_identifier(scheme='ALTRI_CF_ATOKA', identifier=old_cf)
            except Identifier.DoesNotExist:
                pass

        o.add_identifiers(identifiers)
        o.add_classifications(classifications)
        o.add_other_names(other_names)
        o.add_sources(sources)
        o.add_links(links)

    return o, created


def update_or_create_ownership_from_item(owner, organization, item, logger=None, update_strategy="keep_old"):
    """do the actual organizations' ownership creation or update,
    starting from an item dict, the owner organization and the owned organization

    the updating strategy can be chosen, using the `update_strategy` parameter,
    based on the context of the operations
    it defaults to `keep_old`, where old values are kept

    :param owner: the owner Organization or Person instance
    :param organization: the owned Organization instance
    :param item: dict containing details of the ownership to be created
    :param logger: the logger, set to None not to log
    :param update_strategy: how to update Person
      - `keep_old`: only write fields that are empty, keeping old values
      - `overwrite`: overwrite all fields

    :return: ownership instance, created
    """

    rounded_perc = round(float(item["percentage"]), 2)

    # filter ownerships among owner, using organization_id and percentage as key
    ownerships_qs = owner.ownerships.filter(owned_organization=organization, percentage=rounded_perc)

    # # look for ownesrships within +-6months interval, if there is a start date for the ownership
    # if 'start_date' in item and item['start_date']:
    #     try:
    #         start_date_dt = datetime.strptime(item["start_date"], "%Y-%m-%d")
    #     except ValueError:
    #         try:
    #             start_date_dt = datetime.strptime(item["start_date"], "%Y-%m")
    #         except ValueError:
    #             try:
    #                 start_date_dt = datetime.strptime(item["start_date"], "%Y")
    #             except ValueError as e:
    #                 raise e
    #     start_date_lo = (start_date_dt - timedelta(180)).strftime("%Y-%m-%d")
    #     start_date_hi = (start_date_dt + timedelta(180)).strftime("%Y-%m-%d")
    #     ownerships_qs = ownerships_qs.filter(Q(start_date__gte=start_date_lo) & Q(start_date__lte=start_date_hi))

    created = False
    if ownerships_qs.count() > 1:
        # handle error
        if logger:
            logger.warning(f"more than one ownership found for {owner} having {rounded_perc}% in {organization}")
        ownership = None
    else:
        with transaction.atomic():
            if ownerships_qs.count() == 1:
                # one membership was found and is now updated
                # according to chosen update_strategy
                ownership = ownerships_qs.first()
                ownership_defaults = {
                    "start_date": item.get("start_date", None),
                    "end_date": item.get("end_date", None),
                    "end_reason": item.get("end_reason", None)
                }
                to_save = False
                for k, v in ownership_defaults.items():
                    if (
                        update_strategy == "overwrite"
                        or not getattr(ownership, k)
                    ):
                        if v:
                            to_save = True
                            setattr(ownership, k, v)
                if to_save:
                    ownership.save()
            else:
                # no memberships were found, and one is now created
                ownership = owner.add_ownership(
                    organization,
                    percentage=rounded_perc,
                    start_date=item.get("start_date", None),
                    end_date=item.get("end_date", None),
                )
                if ownership:
                    created = True
                else:
                    if logger:
                        logger.warning(
                            "No ownership could be created for {0} in {1}, "
                            "start_date:{2}, end_date:{3} (overlapping?)".format(
                                owner, organization, item.get("start_date", None), item.get("end_date", "-")
                            )
                        )

            # add related fields
            if ownership:
                ownership.add_sources(item.get("sources", []))

    return ownership, created
