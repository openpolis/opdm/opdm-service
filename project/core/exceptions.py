from rest_framework.exceptions import APIException


class UnprocessableEntitytAPIException(APIException):
    status_code = 422
    default_detail = "The request was formally correct, but not accettable."
    default_code = "unprocessable_entity"

    def __init__(self, detail=None, code=None, available_renderers=None):
        self.available_renderers = available_renderers
        super().__init__(detail, code)


class InternalServerErrorAPIException(APIException):
    status_code = 500
    default_detail = "The request was formally correct, but an error occurred while processing it."
    default_code = "internal_server_error"

    def __init__(self, detail=None, code=None, available_renderers=None):
        self.available_renderers = available_renderers
        super().__init__(detail, code)


class AKAException(Exception):
    pass


class SearchException(Exception):
    pass
