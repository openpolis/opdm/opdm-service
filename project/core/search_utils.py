from haystack.backends import SQ
from haystack.inputs import Exact, Raw
from haystack.query import SearchQuerySet
from popolo.models import Person, Organization

from project.core.exceptions import SearchException


def build_exact_search_query(
    family_name, given_name, birth_date=None, birth_location=None, birth_location_area=None
):
    """extract results using exact values, to check if the record is present

    :param family_name:
    :param given_name:
    :param birth_date:
    :param birth_location_area:
    :param birth_location:
    :return:
    """
    exact_search_query = SQ(family_name=Exact(family_name)) & SQ(given_name=Exact(given_name))
    if birth_date:
        birth_date_s = birth_date.replace("-", "")
        exact_search_query = exact_search_query & SQ(birth_date_s=Exact(birth_date_s))

    # if the ID is given, then use it in the exact query, else try the string
    if birth_location_area:
        exact_search_query = exact_search_query & SQ(birth_location_area=Exact(birth_location_area))
    elif birth_location:
        exact_search_query = exact_search_query & SQ(birth_location=Exact(birth_location))

    return exact_search_query


def build_raw_fuzzy_search_query(
    family_name, given_name, birth_date=None, birth_location=None, birth_location_area=None
):
    """extract results from a raw, fuzzy query, looking for similarities
    the query is built according to these instructions
      (family_name:$FN~ AND given_name:$GN~)^5 OR
      (family_name:$FN~ AND birth_date_s:$BD~)^3 OR
      (family_name:$FN~ AND birth_location:$BL~)^2 OR
      (given_name:$GN~ AND birth_date_s:$BD~) OR
      (given_name:$GN AND birth_location:$BL~) OR
      (family_name:$FN) OR
      (birth_date_s:$BD AND birth_location:$BL)^0.5
    and uses fuzzy queries in order to check for records containing similar
    family and given names, or birth locations and dates

    spaces need to be escaped ("\\ ")

    :param family_name:
    :param given_name:
    :param birth_date:
    :param birth_location_area:
    :param birth_location:
    :return:
    """

    # fuzzy search parameters need to be escaped manually,
    # as raw solr search is being used
    family_name = family_name.replace(" ", "\\ ")
    given_name = given_name.replace(" ", "\\ ")
    if birth_location:
        birth_location = birth_location.replace(" ", "\\ ").replace("(", "\\(").replace(")", "\\)")

    # raw fuzzy search query building
    raw_fuzzy_search_query = ""
    raw_fuzzy_search_query += "(family_name:{0}~ AND given_name:{1}~)^5 OR ".format(family_name, given_name)

    if birth_date:
        birth_date_s = birth_date.replace("-", "")
    else:
        birth_date_s = ""

    if birth_date:
        raw_fuzzy_search_query += "(family_name:{0}~ AND birth_date_s:{1}~)^3 OR ".format(family_name, birth_date_s)
        raw_fuzzy_search_query += "(given_name:{0}~ AND birth_date_s:{1}~) OR ".format(given_name, birth_date_s)
    if birth_location_area:
        raw_fuzzy_search_query += "(family_name:{0}~ AND birth_location_area:{1})^2 OR ".format(
            family_name, birth_location_area
        )
        raw_fuzzy_search_query += "(given_name:{0}~ AND birth_location_area:{1}) OR ".format(
            given_name, birth_location_area
        )
        if birth_date:
            raw_fuzzy_search_query += "(birth_date_s:{0} AND birth_location_area:{1})^0.5 OR ".format(
                birth_date_s, birth_location_area
            )
    elif birth_location:
        raw_fuzzy_search_query += "(family_name:{0}~ AND birth_location:{1}~)^2 OR ".format(
            family_name, birth_location
        )
        raw_fuzzy_search_query += "(given_name:{0}~ AND birth_location:{1}~) OR ".format(given_name, birth_location)
        if birth_date:
            raw_fuzzy_search_query += "(birth_date_s:{0} AND birth_location:{1})^0.5 OR ".format(
                birth_date_s, birth_location
            )

    if birth_date and birth_location:
        raw_fuzzy_search_query += "(birth_date_s:{0} AND birth_location:{1})^0.5 OR ".format(
            birth_date_s, birth_location
        )
    raw_fuzzy_search_query += "(family_name:{0})".format(family_name)

    return raw_fuzzy_search_query


def search_person_by_identifier(**kwargs):
    # prepare exact query
    exact_results = SearchQuerySet().filter(**kwargs)
    exact_count = exact_results.count()
    if exact_count == 1:
        person_id = int(exact_results[0].pk)
        queryset = [Person.objects.get(pk=person_id)]
        queryset[0].score = 0.0
        return queryset
    elif exact_count == 0:
        queryset = []
    else:
        raise SearchException("Multiple objects found for this search")

    return queryset


def search_person_by_anagraphics(
    family_name, given_name, birth_date=None, birth_location=None, birth_location_area=None, n_max_results=30
):
    # prepare exact and fuzzy queries, using api_v1.core.SearchQueryBuilder class methods
    exact_search_query = build_exact_search_query(
        family_name, given_name, birth_date, birth_location, birth_location_area
    )
    raw_fuzzy_search_query = build_raw_fuzzy_search_query(
        family_name, given_name, birth_date, birth_location, birth_location_area
    )

    # check if an exact result is found
    exact_results = SearchQuerySet().filter(exact_search_query)
    exact_count = exact_results.count()
    if birth_date and (birth_location or birth_location_area) and exact_count == 1:
        # given all parameters, a single result is found: it's that.
        person_id = int(exact_results[0].pk)
        try:
            queryset = [Person.objects.get(pk=person_id)]
        except Person.DoesNotExist:
            raise SearchException("No person found for this search. Possible solr mismatch.")
        else:
            scores = {person_id: 0.0}
    elif birth_date and (birth_location or birth_location_area) and exact_count > 1:
        # given all parameters there cannot be more than one results
        raise SearchException(
            "Multiple objects found for this search. "
            "Possible duplicate, but also a Solr mismatch is likely, re-index your data."
        )
    else:
        # check for fuzzy results (similarities)
        fuzzy_results = SearchQuerySet().filter(content=Raw(raw_fuzzy_search_query))

        # scores dictionary, with object id as key and score as value
        # injected later into the queryset
        scores = dict(map(lambda x: (int(x.pk), x.score), fuzzy_results))

        # build the queryset from the returned id,
        # optimizing the number of queries needed to build the response
        queryset = (
            Person.objects.filter(pk__in=scores.keys())
            .prefetch_related("other_names", "identifiers", "memberships", "related_persons")
            .select_related("birth_location_area")
        )

    # inject the scores into the queryset
    for r in queryset:
        r.score = scores[r.pk]
    queryset = sorted(queryset, key=lambda x: x.score, reverse=True)

    # limit the queryset to avoid performance issues
    return queryset[:n_max_results]


def search_organization(name, identifier=None, n_max_results=30):
    search_query = SQ()
    if name:
        search_query = search_query & SQ(name=name)
    if identifier:
        search_query = search_query & SQ(identifier=Exact(identifier))
    # check if an exact result is found
    results = SearchQuerySet().filter(search_query)

    # prepares scores dictionary, with object id as key and score as value
    # injected later into the queryset
    scores = dict(map(lambda x: (int(x.pk), x.score), results))

    # build the queryset from the returned id,
    # optimizing the number of queries needed to build the response
    queryset = Organization.objects.filter(pk__in=scores.keys()).prefetch_related(
        "other_names", "identifiers", "memberships"
    )

    # inject the scores into the queryset
    for r in queryset:
        r.score = scores[r.pk]
    queryset = sorted(queryset, key=lambda x: x.score, reverse=True)

    # limit the queryset to avoid performance issues
    return queryset[:n_max_results]
