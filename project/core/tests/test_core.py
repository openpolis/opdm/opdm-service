from unittest.mock import MagicMock, patch

from codicefiscale import codicefiscale
from django.conf import settings
from django.test.testcases import TestCase
from faker import Factory
from popolo.tests.factories import AreaFactory, PersonFactory, OrganizationFactory

from atokaconn import (
    AtokaConn,
    AtokaObjectDoesNotExist,
    AtokaMultipleObjectsReturned,
    AtokaResponseError,
)
from project.atoka.tests import get_person_ok
from project.core import millis
from project.core import organization_utils
from project.core import person_utils
from project.core.person_utils import CFException

faker = Factory.create("it_IT")  # a factory to create fake data for tests


class PackageMethodsTestCase(TestCase):
    def test_millis(self):
        t = millis()
        self.assertIsInstance(t, int)
        self.assertTrue(t > 0)


class OrganizationUtilsTestCase(TestCase):
    def test_org_anagraphical_lookup(self):
        """Test that an organization is found with anagraphical lookup
        """
        org = OrganizationFactory(
            founding_date=faker.date(pattern="%Y-%m-%d", end_datetime="-27y")
        )

        item = {
            "name": org.name.upper(),
            "identifier": org.identifier,
            "founding_date": org.founding_date,
        }

        org_id = organization_utils.org_anagraphical_lookup(item)
        self.assertNotEqual(org_id, 0)

    def test_org_anagraphical_lookup_multiple(self):
        """Multiple organizations with same name/id, having different founding_date
        """
        name = faker.company()
        identifier = faker.ssn()
        org_a = OrganizationFactory(
            name=name,
            identifier=identifier,
            founding_date=faker.date(pattern="%Y-%m-%d", end_datetime="-25y"),
            dissolution_date="2010-01-01",
        )

        org_b = OrganizationFactory(
            name=name,
            identifier=identifier,
            founding_date=faker.date(pattern="%Y-%m-%d", end_datetime="-10y"),
            dissolution_date=None,
        )

        # multiple organizations found when current is not specified (id < 0)
        item = {"name": org_a.name.upper(), "identifier": org_a.identifier}
        org_id = organization_utils.org_anagraphical_lookup(item)
        self.assertLess(org_id, 0)

        # correct organization found when current specified
        item = {
            "name": org_a.name.upper(),
            "identifier": org_a.identifier,
            "founding_date": org_a.founding_date,
        }
        org_id = organization_utils.org_anagraphical_lookup(item)
        self.assertNotEqual(org_id, org_b.id)
        self.assertEqual(org_id, org_a.id)

    def test_org_anagraphical_lookup_current(self):
        """Test that the current organization is correctly found
        """
        current_org = OrganizationFactory(dissolution_date=None)

        item = {
            "name": current_org.name.upper(),
            "identifier": current_org.identifier,
            "founding_date": current_org.founding_date,
        }

        past_org = OrganizationFactory(
            dissolution_date=faker.date(pattern="%Y-%m-%d", end_datetime="-2y"), **item
        )

        # correct organization found when current specified
        org_id = organization_utils.org_anagraphical_lookup(item, current=True)
        self.assertNotEqual(org_id, past_org.id)
        self.assertEqual(org_id, current_org.id)

        # multiple organizations found when current is not specified (id < 0)
        org_id = organization_utils.org_anagraphical_lookup(item)
        self.assertLess(org_id, 0)

    def test_org_identifier_lookup_with_scheme(self):
        """Test that the current organization is correctly found
        when the lookup stategy uses a known identifier
        """
        org = OrganizationFactory(dissolution_date=None)
        org.add_identifier(scheme="TEST", identifier="123")

        item = {
            "name": org.name,
            "identifiers": [
                {"scheme": "TEST", "identifier": "123"},
                {"scheme": "OP_ID", "identifier": "143098fg"},
            ],
        }
        org_id = organization_utils.org_identifier_lookup(item, "TEST")
        self.assertNotEqual(org_id, 0)

    def test_org_identifier_lookup_with_scheme_fails_not_found(self):
        """Test that the organization is not found (id == 0)
        """
        org = OrganizationFactory(dissolution_date=None)
        org.add_identifier(scheme="TEST", identifier="123")

        item = {
            "name": org.name,
            "identifiers": [
                {"scheme": "TEST", "identifier": "124"},
                {"scheme": "OP_ID", "identifier": "143098fg"},
            ],
        }
        org_id = organization_utils.org_identifier_lookup(item, "TEST")
        self.assertEqual(org_id, 0)

    def test_org_identifier_lookup_with_scheme_fails_multiple(self):
        """Test that the organization is found multiple times (id < 0)
        """
        name = faker.company()
        scheme = "TEST"
        identifier = "123"

        org_a = OrganizationFactory(name=name, dissolution_date=None)
        org_a.add_identifier(scheme=scheme, identifier=identifier)

        org_b = OrganizationFactory(name=name, dissolution_date=None)
        org_b.add_identifier(scheme=scheme, identifier=identifier)

        item = {
            "name": name,
            "identifiers": [
                {"scheme": scheme, "identifier": identifier},
                {"scheme": "OP_ID", "identifier": "143098fg"},
            ],
        }
        org_id = organization_utils.org_identifier_lookup(item, scheme)
        self.assertLess(org_id, 0)

    def test_org_identifier_lookup_no_scheme(self):
        """Test that the  organization is correctly found
        when looked up using the main identifier (CF)
        """
        identifier = faker.ssn()
        org = OrganizationFactory(dissolution_date=None, identifier=identifier)

        # correct organization found when identifier_scheme specified
        item = {"name": org.name, "identifier": identifier}
        org_id = organization_utils.org_identifier_lookup(item, identifier_scheme=None)
        self.assertNotEqual(org_id, 0)

    def test_org_identifier_lookup_multiple_cfs(self):
        """Test that the  organization is correctly found
        when looked up using the main identifier (CF),
        even in the case when the identifier is in the ALTRI_CF_ATOKA identifier
        """
        main_identifier = faker.ssn()
        other_identifiers = [faker.ssn() for _ in range(1, 4)]
        org = OrganizationFactory(dissolution_date=None, identifier=main_identifier)
        org.add_identifier(
            scheme="ALTRI_CF_ATOKA", identifier=",".join(other_identifiers)
        )

        # correct organization found when identifier_scheme specified
        item = {"name": org.name, "identifier": other_identifiers[1]}
        org_id = organization_utils.org_identifier_lookup(item, identifier_scheme=None)
        self.assertNotEqual(org_id, 0)

    def test_orgs_with_same_name_different_cf_found_when_private_but_no_private_flag(self):
        # a private organization having the same name is wrongly found when the
        # private_company_verification is not set (defaults to false)
        main_identifier = faker.ssn()
        org = OrganizationFactory(dissolution_date=None, identifier=main_identifier)
        org.add_classification('FORMA_GIURIDICA_OP', code=1310)
        org.add_identifier(scheme='CF', identifier=main_identifier)

        item_cf = faker.ssn()
        item = {
            "name": org.name,
            "identifier": item_cf,
            "identifiers": [{"scheme": "CF", "identifier": item_cf}],
            "classifications": [{"classification": 11}],
        }
        org_id = organization_utils.org_lookup(item, strategy="mixed_current", identifier_scheme='CF')
        self.assertNotEqual(org_id, 0)

    def test_orgs_with_same_name_different_cf_not_found_when_private_using_private_flag(self):
        # a private organization having the same name is correctly not found when the
        # private_company_verification is set
        main_identifier = faker.ssn()
        org = OrganizationFactory(dissolution_date=None, identifier=main_identifier)
        org.add_classification('FORMA_GIURIDICA_OP', code=1310)
        org.add_identifier(scheme='CF', identifier=main_identifier)

        item_cf = faker.ssn()
        item = {
            "name": org.name,
            "identifier": item_cf,
            "identifiers": [{"scheme": "CF", "identifier": item_cf}],
            "classifications": [{"classification": 11}],
        }
        org_id = organization_utils.org_lookup(
            item, strategy="mixed_current", identifier_scheme='CF',
            private_company_verification=True
        )
        self.assertEqual(org_id, 0)

    def test_orgs_with_same_name_different_cf_found_if_public_same_classification(self):
        # a private organization having the same name is correctly not found when the
        # private_company_verification is set
        main_identifier = faker.ssn()
        org = OrganizationFactory(dissolution_date=None, identifier=main_identifier)
        org.add_classification('FORMA_GIURIDICA_OP', code=2410)  # regione
        org.add_identifier(scheme='CF', identifier=main_identifier)

        item_cf = faker.ssn()
        item = {
            "name": org.name,
            "identifier": item_cf,
            "identifiers": [{"scheme": "CF", "identifier": item_cf}],
            "classifications": [{"classification": 455}],  # regione
        }
        org_id = organization_utils.org_lookup(
            item, strategy="mixed_current", identifier_scheme='CF',
            private_company_verification=True
        )
        self.assertNotEqual(org_id, 0)


class PersonUtilsTestCase(TestCase):
    def test_verify_tax_id_with_atoka(self):
        """Test verify_tax_id_with_atoka method works properly
        """

        parent_area = AreaFactory(name="Lazio")
        area = AreaFactory(name="Roma", parent=parent_area)
        person = PersonFactory.create(
            family_name=faker.last_name_male(),
            given_name=faker.first_name_male(),
            birth_date=faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
            birth_location_area=area,
        )
        person.add_identifier(faker.ssn(), scheme="CF")
        tax_id = person.tax_id

        # mock atoka_conn method
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
        )

        atoka_conn.get_person_from_tax_id = MagicMock(
            return_value=get_person_ok(tax_id=tax_id)["items"][0]
        )

        # do the test
        person_utils.verify_tax_id_with_atoka(person, atoka_conn)
        self.assertEqual(person.is_identity_verified, True)
        self.assertEqual(person.identifiers.filter(scheme="ATOKA_ID").count(), 1)

    def test_verify_tax_id_with_atoka_fallback_to_search(self):
        """Test verify_tax_id_with_atoka method works properly,
        when tax_id is not found and the person is found in ATOKA through search
        """

        parent_area = AreaFactory(name="Lazio")
        area = AreaFactory(name="Roma", parent=parent_area)
        person = PersonFactory.create(
            family_name=faker.last_name_male(),
            given_name=faker.first_name_male(),
            birth_date=faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
            birth_location_area=area,
        )
        person.add_identifier(faker.ssn(), scheme="CF")
        tax_id = person.tax_id

        # mock atoka_conn methods
        # do the test
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
        )
        atoka_conn.get_person_from_tax_id = MagicMock(
            return_value=None,
            side_effect=AtokaObjectDoesNotExist(
                "Could not find person with tax_id {0} in Atoka.".format(tax_id)
            ),
        )
        atoka_conn.search_person = MagicMock(
            return_value=get_person_ok(
                tax_id=tax_id,
                search_params={
                    "family_name": person.family_name,
                    "given_name": person.given_name,
                    "birth_date": person.birth_date,
                },
            )["items"][0]
        )

        # do the test
        person_utils.verify_tax_id_with_atoka(person, atoka_conn)
        self.assertEqual(person.is_identity_verified, True)
        self.assertEqual(person.identifiers.filter(scheme="ATOKA_ID").count(), 1)

    def test_verify_tax_id_with_atoka_no_tax_id_in_opdm(self):
        """Test verify_tax_id_with_atoka method fails with generic Exception when
        person instance has no CF identifier (tax_id)
        """

        parent_area = AreaFactory(name="Lazio")
        area = AreaFactory(name="Roma", parent=parent_area)
        person = PersonFactory.create(
            family_name=faker.last_name_male(),
            given_name=faker.first_name_male(),
            birth_date=faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
            birth_location_area=area,
        )

        # do the test
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
        )
        with self.assertRaises(Exception):
            person_utils.verify_tax_id_with_atoka(person, atoka_conn)

    def test_verify_tax_id_with_atoka_handle_exceptions_from_get_person(self):
        """Test verify_tax_id_with_atoka method properly handles other AtokaExceptions,
        generated by the get_person method
        """

        parent_area = AreaFactory(name="Lazio")
        area = AreaFactory(name="Roma", parent=parent_area)
        person = PersonFactory.create(
            family_name=faker.last_name_male(),
            given_name=faker.first_name_male(),
            birth_date=faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
            birth_location_area=area,
        )
        person.add_identifier(faker.ssn(), scheme="CF")
        tax_id = person.tax_id

        # mock atoka_conn method
        # do the test
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
        )
        atoka_conn.get_person_from_tax_id = MagicMock(
            return_value=None, side_effect=AtokaResponseError("URI not found")
        )

        # do the test, that generates an exception
        with self.assertRaises(Exception):
            person_utils.verify_tax_id_with_atoka(person, atoka_conn)

        # mock atoka_conn method
        # do the test
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
        )
        atoka_conn.get_person_from_tax_id = MagicMock(
            return_value=None,
            side_effect=AtokaMultipleObjectsReturned(
                "Found more than one person with tax_id {0} in Atoka.".format(tax_id)
            ),
        )

        # do the test, that generates an exception
        with self.assertRaises(Exception):
            person_utils.verify_tax_id_with_atoka(person, atoka_conn)

    def test_verify_tax_id_with_atoka_handle_exceptions_from_search_person(self):
        """Test verify_tax_id_with_atoka method properly handles other AtokaExceptions,
        generated by the search_person method
        """

        parent_area = AreaFactory(name="Lazio")
        area = AreaFactory(name="Roma", parent=parent_area)
        person = PersonFactory.create(
            family_name=faker.last_name_male(),
            given_name=faker.first_name_male(),
            birth_date=faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
            birth_location_area=area,
        )
        person.add_identifier(faker.ssn(), scheme="CF")

        # mock atoka_conn method
        # do the test
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
        )
        atoka_conn.search_person = MagicMock(
            return_value=None, side_effect=AtokaResponseError("URI not found")
        )

        # do the test, that generates an exception
        with self.assertRaises(Exception):
            person_utils.verify_tax_id_with_atoka(person, atoka_conn)

        params = {
            "family_name": person.family_name,
            "given_name": person.given_name,
            "birth_date": person.birth_date,
        }

        # mock atoka_conn method
        # do the test
        atoka_conn = AtokaConn(
            service_url=settings.ATOKA_API_ENDPOINT,
            version=settings.ATOKA_API_VERSION,
            key=settings.ATOKA_API_KEY,
        )
        atoka_conn.search_person = MagicMock(
            return_value=None,
            side_effect=AtokaMultipleObjectsReturned(
                "Found more than one person with parameters {0} in Atoka.".format(
                    params
                )
            ),
        )

        # do the test, that generates an exception
        with self.assertRaises(Exception):
            person_utils.verify_tax_id_with_atoka(person, atoka_conn)

    def test_compute_cf(self):

        item = {
            "family_name": faker.last_name_male(),
            "given_name": faker.first_name_male(),
            "gender": "M",
            "birth_date": faker.date(pattern="%Y-%m-%d", end_datetime="-47y"),
            "birth_location": "Roma",
        }
        cf = (
            faker.ssn()
        )  # not necessarily equal to computed from item, but it's a test!

        # mock codicefiscale method into returning cf
        with patch.object(codicefiscale, "encode", return_value=cf):
            # do the test
            computed_cf = person_utils.compute_cf(item)
        self.assertEqual(computed_cf, cf)

    def test_compute_cf_missing_item_key(self):

        item = {
            "family_name": faker.last_name_male(),
            "given_name": faker.first_name_male(),
            "gender": "M",
            "birth_location": "Roma",
        }
        cf = (
            faker.ssn()
        )  # not necessarily equal to computed from item, but it's a test!

        # mock codicefiscale method into returning cf
        with patch.object(codicefiscale, "encode", return_value=cf):
            # do the test
            computed_cf = person_utils.compute_cf(item)
        self.assertEqual(computed_cf, None)

    def test_compute_cf_none_item_key(self):

        item = {
            "family_name": faker.last_name_male(),
            "given_name": faker.first_name_male(),
            "gender": "M",
            "birth_date": None,
            "birth_location": "Roma",
        }
        cf = (
            faker.ssn()
        )  # not necessarily equal to computed from item, but it's a test!

        with patch.object(codicefiscale, "encode", return_value=cf):
            # do the test
            computed_cf = person_utils.compute_cf(item)
        self.assertEqual(computed_cf, None)

    def test_compute_cf_malformed_item_key(self):

        item = {
            "family_name": faker.last_name_male(),
            "given_name": faker.first_name_male(),
            "gender": "M",
            "birth_date": "28/12/2023",
            "birth_location": "Roma",
        }
        cf = (
            faker.ssn()
        )  # not necessarily equal to computed from item, but it's a test!

        # mock codicefiscale method into returning cf
        with patch.object(codicefiscale, "encode", return_value=cf):
            # do the test
            try:
                computed_cf = person_utils.compute_cf(item)
            except CFException:
                computed_cf = None
        self.assertEqual(computed_cf, None)
