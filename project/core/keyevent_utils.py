from typing import Dict, Any, Tuple, Optional

from django.db import transaction
from popolo.models import KeyEvent


# Utility functions used through all the project, related to key_events


def ke_anagraphical_lookup(item):
    """anagraphical lookup strategy implementation,
    the instance is lookoed up by event_type and start_date (which are set as unique index)

    :param item:   the item to lookup in the DB
    :return: the id if found, 0 if not found, negative number of multiples if multiples found
    """

    ke_id = 0
    filters = {
        "event_type__iexact": item["event_type"],
        "start_date": item["start_date"],
    }
    try:
        ke = KeyEvent.objects.get(**filters)
        ke_id = ke.id
    except KeyEvent.DoesNotExist:
        pass
    except KeyEvent.MultipleObjectsReturned:
        ke_id = -1 * KeyEvent.objects.filter(**filters).count()

    return ke_id


def ke_identifier_lookup(item):
    """identifier lookup strategy implementation,
    the instance is found by the identifier field

    :param item:   the item to lookup in the DB
    :return: the id if found, 0 if not found, negative number of multiples if multiples found
    """

    ke_id = 0
    filters = {"identifier": item["identifier"]}
    try:
        ke = KeyEvent.objects.get(**filters)
        ke_id = ke.id
    except KeyEvent.DoesNotExist:
        pass
    except KeyEvent.MultipleObjectsReturned:
        ke_id = -1 * KeyEvent.objects.filter(**filters).count()

    return ke_id


def keyevent_lookup(item, strategy):
    """
    :param item:
    :param strategy: lookup strategy (anagraphical, identifier)
    :return: person_id
      0   - Not found => create
      > 0 - found => update
      < 0 - unknown => n similarities were added,
    """

    if strategy == "anagraphical":
        ke_id = ke_anagraphical_lookup(item)

    elif strategy in ["identifier", "mixed"]:
        # identifier lookup strategy: use given scheme or lookup on natural identifier (CF)
        # if not found a try with the anagraphical lookup strategy is done
        ke_id = ke_identifier_lookup(item)

        # mixed strategy: try the anagraphical lookup strategy if identifier lookup failed
        if ke_id == 0 and strategy == "mixed":
            ke_id = ke_anagraphical_lookup(item)
    else:
        raise Exception(
            "accepted values for lookup_strategy are: anagraphical, identifier, mixed"
        )

    return ke_id


def update_or_create_keyevent_from_item(
    item: Dict, ke_id: Any = 0, update_strategy: str = "keep_old"
) -> Tuple[Optional[KeyEvent], bool]:
    """
    Update or create a `KeyEvent` from a dictionary.

    Do the actual event creation or update,
    starting from an item dict and a ke_id (0 signals creating)

    the updateing strategy can be chosen, using the `update_strategy` parameter,
    based on the context of the operations
    it defaults to `keep_old`, where old values are kept

    :param item: dict containing details of the organization to be created
    :param ke_id: the id of the organization (to update), 0 to create
    :param update_strategy: how to update instance
      - `keep_old`: only write fields that are empty, keeping old values
      - `overwrite`: overwrite all fields

    :return: Return a tuple (object, created), where created is a boolean
        specifying whether an object was created.
    """

    ke_fields = [f.name for f in KeyEvent._meta.fields]
    ke_defaults = {}
    for k, v in item.items():
        if k in ke_fields:
            ke_defaults[k] = v

    if ke_id < 0:
        raise ValueError("ke_id must be greater than 0")

    with transaction.atomic():
        if ke_id > 0:
            try:
                ke = KeyEvent.objects.get(id=ke_id)
            except KeyEvent.DoesNotExist:
                return None, False

            # update according to chosen update_strategy
            for k, v in ke_defaults.items():
                if update_strategy == "overwrite" or not getattr(ke, k):
                    setattr(ke, k, v)
            ke.save()
            created = False
        else:
            ke = KeyEvent.objects.create(**ke_defaults)
            created = True

    return ke, created
