import time as time_  # make sure we don't override time
from collections import OrderedDict

from codicefiscale import codicefiscale
from django.db.models import F
from rest_framework.pagination import PageNumberPagination
from rest_framework.renderers import BrowsableAPIRenderer
from rest_framework.response import Response

from popolo.models import (
    Person, KeyEvent, Organization, Classification)
from django.db import models
from haystack import signals

from project.opp.acts.filtersets import GovDecreeFilterSet, BillFilterSet, \
    ImplementingDecreeFilterSet  # , ImplementingDecreeFilterSet
from project.opp.acts.models import Bill, ImplementingDecree, GovDecree
from project.opp.api_opp_v1.utils import get_governmensts_by_leg
from project.opp.gov.models import Government
from project.opp.metrics.models import PositionalPower
from project.opp.votes import filtersets
from project.opp.parl.models import Assembly
from django.core.cache import cache

from project.opp.votes.models import Voting

codicefiscale._DATA["municipalities"].update(
    {
        "donada": {"code": "D337", "province": "RO", "name": "DONADA"},
        "terzo-d-aquileia": {"code": "L144", "province": "UD", "name": "TERZO D'AQUILEIA"},
        "san-dorligo-della-valle-dolina": {"code": "D324", "province": "TS", "name": "San Dorligo Della Valle-Dolina"},
        "castelnuovo-di-val-di-cecina": {"code": "C244", "province": "PI", "nome": "Castelnuovo Di Val Di Cecina"},
        "riviera-d-adda": {"code": "H351", "province": "BG", "nome": "Riviera d'Adda"},
        "colcavagno": {"code": "C831", "province": "AT", "name": "Colcavagno"},
        "contarina": {"code": "C967", "province": "RO", "name": "Contarina"},
        "fubine": {"code": "D814", "province": "AL", "name": "Fubine"},
        "nicastro": {"code": "F888", "province": "CZ", "name": "Nicastro"},
        "sambiase": {"code": "H742", "province": "CZ", "name": "Sambiase"},
        "san-remo": {"code": "I138", "province": "IM", "name": "San Remo"},
        "sannicandro-garganico": {"code": "I054", "province": "FG", "name": "Sannicandro Garganico"},
        "telese": {"code": "L086", "province": "BN", "name": "Telese"},
        "trodena": {"code": "L444", "province": "BZ", "name": "Trodena"},
    }
)

codicefiscale._DATA["countries"].update(
    {
        "ex-repubblica-jugoslava-di-macedonia": {
            "code": "Z148",
            "province": "EE",
            "name": "Ex Repubblica Jugoslava Di Macedonia",
        },
        "jugoslavia": {"code": "Z118", "province": "EE", "name": "Jugoslavia"},
    }
)


def millis():
    return int(round(time_.time() * 1000))


def batch_generator(batchsize, iterator):
    """Creates a generator containing batches of values, sized `batchsize`, out of iterator

    :param batchsize: size of each batch
    :param iterator:  list of original values
    :return:          a generator yielding a list of values each time
    """
    while True:
        pk_buffer = []
        try:
            # Consume queryset iterator until batch is reached or the
            # iterator has been exhausted.
            while len(pk_buffer) < batchsize:
                pk_buffer.append(next(iterator))
        except StopIteration:
            # Break out of the loop once the queryset has been consumed.
            break
        finally:
            # yield the batch of pks
            yield pk_buffer


def get_list_of_dict(list_of_tuples, keys, string=True):
    """
    This function will accept keys and list_of_tuples as args and return list of dicts
    """
    if string:
        list_of_dict = [dict(zip(keys, map(str, values))) for values in list_of_tuples]
        return list_of_dict
    else:
        list_of_dict = [dict(zip(keys, values)) for values in list_of_tuples]
        return list_of_dict


class OPDMBrowsableAPIRenderer(BrowsableAPIRenderer):
    """Renders the browsable api, with no forms."""

    def get_description(self, view, status_code):
        """Overrides the get_description, in order to infer descriptions for the single action"""
        import re
        docdict = {}
        if getattr(view, '__doc__'):
            for s in list(map(
                lambda x: x.strip(' '),
                view.__doc__.split("\n")
            )):
                m = re.match(r"(.*?):(.*)", s)
                if m is None:
                    continue
                else:
                    docdict[m.group(1)] = m.group(2).strip()
        return docdict.get(view.action, super().get_description(view, status_code))

    def get_context(self, *args, **kwargs):
        ctx = super().get_context(*args, **kwargs)
        ctx['display_edit_forms'] = False
        return ctx


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 25
    page_size_query_param = "page_size"
    max_page_size = 100


class ResultsWithVotationsCodelistPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500

    def get_codelists_dict(self):
        """Compute and return the codelists dictionary"""
        leg = self.request.parser_context['kwargs']['legislature']

        return {
            'years': get_list_of_dict(filtersets.get_years(self.request),keys=['id', 'value']),
            'branch': filtersets.get_branches(self.request),
            'governments': get_governmensts_by_leg(leg),
            'governments_filter': get_governmensts_by_leg(leg),
            'main_vote_type': get_list_of_dict((filtersets.get_vote_types(descriptive=True)), keys=['id', 'value']),
            'sub_vote_type': get_list_of_dict(filtersets.get_sub_vote_types(), keys=['id', 'value']),
            'vm_cohesion_rates': dict(filtersets.get_vm_cohesion_rates(descriptive=True)),
            'orderings': get_list_of_dict(self.request.parser_context['view'].ord_fields, keys=['id', 'value']),
            'outcome': get_list_of_dict(
                Voting.OUTCOME_TYPES,
                keys=['id', 'value'])
        }

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('codelists', self.get_codelists_dict()),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class ResultsWithEventsCodelistPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500

    def get_codelists_dict(self):
        """Compute and return the codelists dictionary"""
        leg = self.request.parser_context['kwargs']['legislature']

        return {
            'years': get_list_of_dict(filtersets.get_years(self.request), keys=['id', 'value']),
            'branch': filtersets.get_branches(self.request),
            'governments': get_governmensts_by_leg(leg),
            'event_category': get_list_of_dict([
                ('law', 'Approvazione legge'),
                ('comm', 'Commissioni'),
                ('dact', 'Decreti attuativi'),
                ('dl', 'Decreti legge'),
                ('dlgs', 'Decreti legislativi'),
                ('ddl', 'Disegni di legge'),
                ('gov', 'Governo'),
                ('groups', 'Gruppi'),
                ('inc_parl', 'Incarichi parlamentari'),
                ('organs', 'Organi'),
                ('voto', 'Votazioni'),

            ], keys=('id', 'value'))}

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('codelists', self.get_codelists_dict()),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class PersonOnlySignalProcessor(signals.BaseSignalProcessor):
    def setup(self):
        # Listen only to the ``Person`` model.
        models.signals.post_save.connect(self.handle_save, sender=Person)
        models.signals.post_delete.connect(self.handle_delete, sender=Person)

    def teardown(self):
        # Disconnect only for the ``Person`` model.
        models.signals.post_save.disconnect(self.handle_save, sender=Person)
        models.signals.post_delete.disconnect(self.handle_delete, sender=Person)


class ResultsWithMembershipsCodelistPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500

    def get_codelists_dict(self):
        """Compute and return the codelists dictionary"""
        keys = ['id', 'value']
        try:
            legislature_identifier = f"ITL_{self.request.parser_context['kwargs']['legislature']}"
        except KeyError:
            return {}
        e = KeyEvent.objects.get(identifier=legislature_identifier)
        governi = Organization.objects.filter(
            classifications__classification__descr__icontains='governo della repubblica',
            start_date__gte=e.start_date
        ).annotate(value=F('name')).values('slug', 'value')
        governi_list = list(governi)
        for gov in governi_list:
            gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()
        return {
            'election_macro_areas': get_list_of_dict((filtersets.get_election_macro_areas(self.request)), keys),
            # 'election_areas': get_list_of_dict(filtersets.get_election_areas(self.request), keys),
            'role_types': dict(filtersets.ROLE_TYPES),
            'latest_group': get_list_of_dict(filtersets.get_election_latest_group(self.request), keys),
            'governments': governi_list,
            'orderings': get_list_of_dict(self.request.parser_context['view'].ord_fields, keys=['id', 'label'])
        }

    def get_pp_total_ordering(self):
        try:
            legislature_identifier = f"ITL_{self.request.parser_context['kwargs']['legislature']}"
        except KeyError:
            return {}

        orgs = Assembly.objects.filter(organization__key_events__key_event__identifier=legislature_identifier)
        role = self.request.query_params.get('role')
        if role and role.lower() == 'senatore':
            return orgs.get(organization__name__icontains='senato').positional_power().count()
        elif role and role.lower() == 'deputato':
            return orgs.get(organization__name__icontains='camera').positional_power().count()
        else:
            return None

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""

        if cache.get(f'membership_list_{self.request._request.get_raw_uri()}'):

            return Response(OrderedDict(cache.get(f'membership_list_{self.request._request.get_raw_uri()}')))
        else:
            res = [
                ('count', self.page.paginator.count),
                ('pp_total_ordering', self.get_pp_total_ordering()),
                ('codelists', self.get_codelists_dict()),
                ('next', self.get_next_link()),
                ('previous', self.get_previous_link()),
                ('results', data)
            ]
        cache.set(f'membership_list_{self.request._request.get_raw_uri()}', res, timeout=60*60)
        return Response(OrderedDict(res))


class ResultsWithGroupVoteCodelistPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class ResultsWithGovDecreCodelistPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500

    def get_codelists_dict(self):

        """Compute and return the codelists dictionary"""
        try:
            legislature_identifier = f"ITL_{self.request.parser_context['kwargs']['legislature']}"
        except KeyError:
            return {}

        e = KeyEvent.objects.get(identifier=legislature_identifier)
        governi = Organization.objects.filter(
            classifications__classification__descr__icontains='governo della repubblica',
            start_date__gte=e.start_date
        ).annotate(value=F('name')).values('slug', 'value')
        keys = ['id', 'value']
        governi_list = list(governi)
        for gov in governi_list:
            gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()

        governi_filter = (Organization.objects.filter(id__in=Government.objects.filter(organization_id__in=GovDecree.objects.by_legislature(self.request.parser_context['kwargs']['legislature'])
                                                   .values('sitting__government__id')
                                                   .distinct()).values('organization_id')).annotate(value=F('name')).values('id', 'value')
                          .order_by('-start_date'))
        # governi_filter = GovDecreeFilterSet().filters['government'].extra['queryset'](self.request) \
        #     .annotate(value=F('name')).values('id', 'value')
        governi_filter_list = list(governi_filter)
        for i in governi_filter_list:
            i['value'] = Government.objects.get(organization_id=i['id']).name

        return {
            'governments': governi_list,
            'governments_filter': governi_filter_list,
            'status': get_list_of_dict(GovDecreeFilterSet().filters['status'].extra['choices'], keys),
            'typology': get_list_of_dict(GovDecreeFilterSet().filters['typology'].extra['choices'], keys),
            'orderings': get_list_of_dict(self.request.parser_context['view'].ord_fields, keys=['id', 'label'])
        }

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('codelists', self.get_codelists_dict()),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class ResultsWithBillCodelistPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500

    def get_codelists_dict(self):
        """Compute and return the codelists dictionary"""
        try:
            legislature_identifier = f"ITL_{self.request.parser_context['kwargs']['legislature']}"
        except KeyError:
            return {}

        e = KeyEvent.objects.get(identifier=legislature_identifier)
        governi = Organization.objects.filter(
            classifications__classification__descr__icontains='governo della repubblica',
            start_date__gte=e.start_date
        ).annotate(value=F('name')).values('slug', 'value').order_by('-start_date')
        keys = ['id', 'value']
        governi_list = list(governi)
        for gov in governi_list:
            gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()
        return {
            'governments': governi_list,
            'governments_filter': get_list_of_dict(BillFilterSet().filters['government'].extra['choices'], keys,
                                                   string=False),
            'status': get_list_of_dict(Bill.STATUS, keys),
            'typology': get_list_of_dict(BillFilterSet().filters['typology'].extra['choices'], keys),
            'initiative': get_list_of_dict(Bill.INITIATIVE_TYPES, keys),
            'branch': filtersets.get_branches(self.request),
            'type': get_list_of_dict(Bill.BILL_TYPES, keys),
            'orderings': get_list_of_dict(self.request.parser_context['view'].ord_fields, keys=['id', 'label'])
        }

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('codelists', self.get_codelists_dict()),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class ResultsWithGovBillCodelistPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500

    def get_codelists_dict(self):
        """Compute and return the codelists dictionary"""
        leg = self.request.parser_context['kwargs']['legislature']
        legislature_identifier = f"ITL_{leg}"

        e = KeyEvent.objects.get(identifier=legislature_identifier)
        governi = Organization.objects.filter(
            classifications__classification__descr__icontains='governo della repubblica',
            start_date__gte=e.start_date
        ).annotate(value=F('name')).values('slug', 'value')
        # keys = ['id', 'value']
        governi_list = list(governi)
        for gov in governi_list:
            gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()
        return {
            'governments': governi_list,
            'governments_filter': get_governmensts_by_leg(leg),
            'orderings': get_list_of_dict(self.request.parser_context['view'].ord_fields, keys=['id', 'label'])
        }

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('codelists', self.get_codelists_dict()),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class ResultsWithImplementingDecreeCodelistPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = "page_size"
    max_page_size = 500

    def get_codelists_dict(self):
        """Compute and return the codelists dictionary"""
        try:
            legislature_identifier = f"ITL_{self.request.parser_context['kwargs']['legislature']}"
        except KeyError:
            return {}

        e = KeyEvent.objects.get(identifier=legislature_identifier)
        governi = Organization.objects.filter(
            classifications__classification__descr__icontains='governo della repubblica',
            start_date__gte=e.start_date
        ).annotate(value=F('name')).values('slug', 'value').order_by('-start_date')
        keys = ['id', 'value']
        governi_list = list(governi)
        for gov in governi_list:
            gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()

        governi = Organization.objects.filter(
            id__in=ImplementingDecree.objects.all().values_list('government_id', flat=True)) \
            .annotate(value=F('name')).values('id', 'slug', 'value').order_by('-start_date')
        governi_list_filter = list(governi)
        for gov in governi_list_filter:
            gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()

        return {
            'governments': governi_list,
            'governments_filter': governi_list_filter,
            'orderings': get_list_of_dict(self.request.parser_context['view'].ord_fields, keys=['id', 'label']),
            'type_law': get_list_of_dict(ImplementingDecreeFilterSet()
                                         .filters['type_law'].extra['choices'], keys),
            'mins_proposer': get_list_of_dict(ImplementingDecreeFilterSet()
                                              .filters['mins_proposer'].extra['choices'], keys),
            'status': [
                {'id': 'app',
                 'value': 'Adottato'},
                {'id': 'napp',
                 'value': 'Non adottato'},
                {'id': 'exp',
                 'value': 'Scaduto'},
            ],
            'policy': get_list_of_dict(
                ImplementingDecree.objects.all().values_list('policy', 'policy').distinct().order_by('policy'),
                keys=['id', 'label'])
        }

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('codelists', self.get_codelists_dict()),
            ('next', self.get_next_link()),
            ('previous', self.get_previous_link()),
            ('results', data)
        ]))


class ResultsWithPersonsCodelistPagination(PageNumberPagination):
    page_size = 25
    page_size_query_param = "page_size"
    max_page_size = 25

    def get_codelists_dict(self):
        """Compute and return the codelists dictionary"""
        try:
            legislature_identifier = f"ITL_{self.request.parser_context['kwargs']['legislature']}"
        except KeyError:
            return {}

        e = KeyEvent.objects.get(identifier=legislature_identifier)
        governi = Organization.objects.filter(
            classifications__classification__descr__icontains='governo della repubblica',
            start_date__gte=e.start_date
        ).annotate(value=F('name')).values('slug', 'value')

        governi_list = list(governi)
        for gov in governi_list:
            gov['value'] = ' '.join(gov['value'].split('Governo')[::-1]).strip()
        return {
            'governments': governi_list,
            'orderings': [{"id": "pp",
                           "label": "Indice di forza"},
                          {"id": "family_name",
                           "label": "Cognome"}
                          ],
        }

    def get_paginated_response(self, data):
        """Override the get_paginated_response method of the base PageNumberPagination,
        adding a 'codelists' key, that contains the codelists dictionary.

        This helps an external API to only make one query to get both the results and the codelists."""

        if cache.get(f'pp_index_{self.request._request.get_raw_uri()}'):

            return Response(OrderedDict(cache.get(f'pp_index_{self.request._request.get_raw_uri()}')))

        else:
            res = [
                ('count', self.page.paginator.count),
                ('pp_total', PositionalPower.positional_power_all(
                    leg=self.request.parser_context['kwargs']['legislature']).count()),
                ('codelists', self.get_codelists_dict()),
                ('next', self.get_next_link()),
                ('previous', self.get_previous_link()),
                ('results', data)
            ]
            cache.set(f'pp_index_{self.request._request.get_raw_uri()}', res, timeout=60*60)
            return Response(OrderedDict(res))
