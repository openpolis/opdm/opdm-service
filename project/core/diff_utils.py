import difflib
import filecmp
import json
import os
import shutil
from io import StringIO

from project.tasks.etl.composites.diff_logics import ShallowUpdatesDiffLogic


class DiffHelper(object):
    """Helper class that extracts differences between new content and locally cached one

     Some utility class methods to compute diffs among data are available.

     The class behaves like a context manager, and it must should be used as such, as
     important cache cleanup operations are performed on exit.

     NOTE:
         This class is currently working for CSV and JSON contents (fmt)
         and need to be modified in order to work with other contents.
    """
    def __init__(
        self, local_cache_path, filename,
        key_getter=None, fmt="csv", clear_cache=False,
        updates_diff_logic=ShallowUpdatesDiffLogic(), logger=None
    ):
        """Constructor

        :param local_cache_path: the path, absolute or relative to manage.py path
        :param filename:         the complete filename (with extension)
        :param fmt:              the format (CSV, JSON, ...), if not clear from extension
        :param logger:           the logger used to output to console and files

        The extension, as extracted from filename, determines the format.
        """
        self.local_cache_path = local_cache_path
        if "." in filename:
            self.filename, self.extension = ".".join(filename.split(".")[:-1]), filename.split(".")[-1]
        else:
            self.filename, self.extension = filename, ''
        self.clear_cache = clear_cache
        self.fmt = fmt
        if self.extension is None or self.extension == '' and self.fmt:
            self.extension = self.fmt
        self.logger = logger
        self.adds_and_updates = {'n': 0, 'records': [], 'io': StringIO("")}
        self.removes = {'n': 0, 'records': [], 'io': StringIO("")}
        self.new = "{0}/{1}_new.{2}".format(self.local_cache_path, self.filename, self.extension)
        self.old = "{0}/{1}.{2}".format(self.local_cache_path, self.filename, self.extension)
        self.ok = False
        self.key_getter = key_getter
        self.updates_diff_logic = updates_diff_logic

        if self.extension.lower() in ('csv', 'json'):
            self.fmt = self.extension.lower()

        if self.key_getter is None and self.fmt == 'json':
            raise DiffHelperException("A key_getter function must be passed to identify JSON records uniquely")

    def __enter__(self):
        """Enter into context.

        :return:
        """

        # create the cache directory if needed
        if not os.path.isdir(self.local_cache_path):
            os.mkdir(self.local_cache_path)

        # clear the file in cache, if requested
        if self.clear_cache and os.path.isfile(self.old):
            os.unlink(self.old)

        # return self so that with DiffHelper(params) as diff_helper, can be used
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        """Exit from context.
        If an error occurred, then remove the new file, else move new into old

        """
        if os.path.isfile(self.new):
            if not self.ok:
                os.remove(self.new)
            else:
                shutil.move(self.new, self.old)

        return False

    def extract_differences(self, **kwargs):
        """Invoke the correct method, according to the format

        :param kwargs: parameters passed to the invoked method
        :return:
        """
        if self.fmt.lower() == 'csv':
            self.extract_differences_csv(**kwargs)
        elif self.fmt.lower() == 'json':
            self.extract_differences_json(**kwargs)
        else:
            raise DiffHelperException("Format {0} is not (yet) supported.".format(self.fmt))

    def extract_differences_json(self, encoding='utf-8'):

        # if new file does not exist, raise an exception
        if not os.path.isfile(self.new):
            raise DiffHelperException("The new file is not present in the cache directory")

        # if cached file does not exist, then one containing an empty list is created
        # this way, all content from the new file will go into the insert section
        if not os.path.isfile(self.old):
            with open(self.old, 'w', encoding=encoding) as f:
                json.dump([], f)

        adds, removes, updates = self.json_files_diff(
            encoding=encoding
        )

        self.adds_and_updates['n'] = len(adds + updates)
        self.adds_and_updates['records'] = adds + updates
        self.adds_and_updates['io'] = StringIO(json.dumps(adds + updates))

        self.removes['n'] = len(removes)
        self.removes['records'] = removes
        self.removes['io'] = StringIO(json.dumps(removes))

        self.ok = True

    def json_files_diff(self, encoding='utf8'):
        old_path = self.old
        new_path = self.new

        with open(old_path, "r", encoding=encoding) as fold:
            with open(new_path, "r", encoding=encoding) as fnew:
                jold = json.load(fold)
                jnew = json.load(fnew)

                if type(jold) != list or type(jnew) != list:
                    raise Exception("Both JSON files to compare must be of type <list>")

                jold_keys_set = {self.key_getter(v) for v in jold}
                jnew_keys_set = {self.key_getter(v) for v in jnew}

                removes_keys_set = jold_keys_set - jnew_keys_set
                removes = filter(lambda x: self.key_getter(x) in removes_keys_set, jold)

                adds_keys_set = jnew_keys_set - jold_keys_set
                adds = filter(lambda x: self.key_getter(x) in adds_keys_set, jnew)

                intersecting_keys_set = jold_keys_set & jnew_keys_set
                intersecting_items_new = {
                    self.key_getter(i): i
                    for i in filter(
                        lambda x: self.key_getter(x) in intersecting_keys_set,
                        jnew
                    )
                }
                intersecting_items_old = {
                    self.key_getter(i): i
                    for i in filter(
                        lambda x: self.key_getter(x) in intersecting_keys_set,
                        jold
                    )
                }

                # call the updates_diff_logic function using getattr,
                # in order to avoid problems with errors in signature
                # as the function pointed by updates_diff_logic may have different ones
                updates = self.updates_diff_logic.compute_diffs(
                    intersecting_items_old, intersecting_items_new
                )

        return list(adds), list(removes), list(updates)

    def extract_differences_csv(self, encoding='utf-8', sep=',', index_cols=(0,)):
        """Compare new and old file in cache and extract differences.
        The difference are set in the `adds_and_updates_io` and `removes_io` `StringIO` stream
        for further consumption.

        The method handles different scenarios in which no cached file exists or files are identical.

        :param encoding:    the encoding used in the files
        :param sep:         the separator character
        :param index_cols:  the index of the columns that generates a unique index
        :return:
        """

        # if new file does not exist, raise an exception
        if not os.path.isfile(self.new):
            raise DiffHelperException("The new file is not present in the cache directory")

        # if cached file does not exist, then an empty one is created
        # using the new file header
        # this way, all content from the new file will go into the adds_and_updates_io
        if not os.path.isfile(self.old):
            with open(self.new, 'r', encoding=encoding) as new_file:
                old_header = new_file.readline().strip('\n')
            with open(self.old, 'w', encoding=encoding) as f:
                f.write(old_header)

        # extract and compare headers
        with open(self.old, 'r', encoding=encoding) as old_file:
            old_header = old_file.readline().strip('\n')
        with open(self.new, 'r', encoding=encoding) as new_file:
            new_header = new_file.readline().strip('\n')

        if old_header != new_header:
            raise DiffHelperException("The header of the file has changed! Process blocked")

        # check if separator is correct
        if len(new_header.split(sep)) < 2:
            raise DiffHelperException(
                "It seems the CSV file separator is not {0}".format(sep)
            )

        adds, removes, updates = self.csv_files_diff(
            self.old, self.new,
            encoding=encoding, sep=sep,
            index_colums=index_cols
        )

        self.adds_and_updates['n'] = len(adds + updates)
        self.adds_and_updates['io'] = StringIO("\n".join([new_header] + adds + updates))

        self.removes['n'] = len(removes)
        self.removes['io'] = StringIO("\n".join([new_header] + removes))

        self.ok = True

    @classmethod
    def csv_files_diff(cls, filepath_old, filepath_new, encoding='utf-8', sep=',', index_colums=(0,)):
        """Given two paths to csv files, returns a triple of lists:
           - rows to be added (adds)
           - rows to be removed (removes)
           - rows to be updates (updates)

        An encoding for the files can be set, if different from utf-8.
        The delimiter and index_cols parameters can be used to identify the unique index in the file.

        More than one columns can be specified as index.

        Returns a triple of empty lists if the files are identical.
        """
        if not filecmp.cmp(filepath_old, filepath_new):

            # open files and create result generator
            with open(filepath_old, 'r', encoding=encoding) as f_new:
                with open(filepath_new, 'r', encoding=encoding) as f_old:
                    return cls.csv_streams_diff(f_old, f_new, sep=sep, index_colums=index_colums)
        else:
            # return three empty lists if files are equal
            return [], [], []

    @classmethod
    def csv_streams_diff(cls, old, new, sep=',', index_colums=(0,), returns_deltas=False):
        """Given two csv streams, returns a triple of lists:
           - rows to be added (adds)
           - rows to be removed (removes)
           - rows to be updates or deltas (updates)

        An encoding for the files can be set, if different from utf-8.
        The delimiter and index_cols parameters can be used to identify the unique index in the file.

        More than one columns can be specified as index.

        Returns a triple of empty lists if the files are identical.

        If the flag `returns_deltas` is found, then the updates are returned as a a
        structured list of changes:
        [
            { "idx": "value1, value2",
              "field_1": ("value1", "value2"),
              ...
            }, ...
        ]
        """
        # reset stream pointers
        new.seek(0)
        old.seek(0)

        result = difflib.unified_diff(new.readlines(), old.readlines())

        # remove non-changed results, in order to shorten the list to process
        changed_results = filter(lambda x: x[0] in '+-' and x[:3] != '---' and x[:3] != '+++', result)

        # build the changes index, as a dictioary, for fast access
        changes_idx = {}
        for change in changed_results:
            values = change.strip('-+\n').split(sep)
            index = sep.join(values[i] for i in index_colums)
            if index not in changes_idx:
                changes_idx[index] = []

            # avoid duplications
            if (
                (change[0], change.strip('-+\n')) in
                [(i['op_type'], i['row']) for i in changes_idx[index]]
            ):
                continue

            changes_idx[index].append({
                'op_type': change[0], 'row': change.strip('-+\n')
            })

            # remove false changes
            if len(changes_idx[index]) == 2 and changes_idx[index][0]['row'] == changes_idx[index][1]['row']:
                del changes_idx[index]

        # identify adds and removes among the index (only one row, plus or minus op_type)
        adds = map(
            lambda x: x[0]['row'],
            filter(lambda y: len(y) == 1 and y[0]['op_type'] == '+', changes_idx.values())
        )
        removes = map(
            lambda x: x[0]['row'],
            filter(lambda y: len(y) == 1 and y[0]['op_type'] == '-', changes_idx.values())
        )

        # build a flattened list of multiple rows per index (changes),
        # then filters the new (plus op_type) to create the updates
        flattened = (
            item for sublist in filter(lambda y: len(y) > 1, changes_idx.values()) for item in sublist
        )

        if returns_deltas:
            updates = []
            for k, v in changes_idx.items():
                if len(v) == 2:
                    change = {'idx': k}
                    fields_0 = v[0]['row'].split(",")
                    fields_1 = v[1]['row'].split(",")

                    for n, f in enumerate(fields_0):
                        if fields_0[n] != fields_1[n]:
                            change[n] = (fields_0[n], fields_1[n])

                    updates.append(change)
        else:
            updates = map(
                lambda x: x['row'], filter(lambda y: y['op_type'] == '+', flattened)
            )

        # return the three generators (maps)
        return list(adds), list(removes), list(updates)

    @classmethod
    def dataframes_diff(cls, old_df, new_df, index_columns):
        """Given an old and a new dataframe, and a set of columns constituting a unique index,
           generates a triple of dataframes, containing:
           - rows to be added (adds_df)
           - rows to be removed (removes_df)
           - rows to be updates (updates_df)

        """
        # setup dataframes
        old_df = old_df.set_index(index_columns).sort_index()
        new_df = new_df.set_index(index_columns).sort_index()

        # all new rows NOT among old ones are to be added
        adds_df = new_df[~new_df.index.isin(old_df.index)]

        # all old rows NOT new ones are to be removed
        removes_df = old_df[~old_df.index.isin(new_df.index)]

        # changed rows can be detected among the new ones, through the `eq` dataframe method
        diff_df = new_df.eq(old_df)
        equals = diff_df[(~diff_df.index.isin(adds_df.index)) & (~diff_df.index.isin(removes_df.index))].all(1)
        updates = equals[~equals.values].index
        updates_df = new_df.loc[updates, :]

        # return a triple of DataFrames (adds, removes, updates)
        return adds_df, removes_df, updates_df


class DiffHelperException(Exception):
    pass
