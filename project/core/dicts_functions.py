from typing import Dict

from django.contrib.contenttypes.models import ContentType
from popolo.models import Person, Organization, RoleType, Identifier


#
# this module contains a series of functions that build maps connecting identifiers to objects, or among themselves
#


def get_people_by_tax_id_dict(tax_ids: list) -> dict:
    """Build a mapping: tax_id => pk

    :param tax_ids:
    :return:
    """
    people = Person.objects.filter(
        identifiers__scheme="CF", identifiers__identifier__in=tax_ids,
    ).values("id", "identifiers__identifier", "name")

    ret = {}
    for person in people:
        identifier = person.pop("identifiers__identifier")
        ret[identifier] = person
    return ret


def get_orgs_dict_by_org_id(classifications_labels):
    """returns a dict of organizations (national and european levels only), related to their ID

    :return: a dict of the form
    {
      ID: {
        'organization': O,
        'election': KE,
      }, ...
    }
    """
    orgs = Organization.objects.filter(
        classifications__classification__scheme="FORMA_GIURIDICA_OP",
        classifications__classification__descr__in=classifications_labels,
    )

    ret = {}
    for org in orgs:
        ret[org.id] = {"organization": org, "election": None}
        if org.key_events.filter(key_event__event_type__icontains="ele"):
            ret[org.id]["election"] = (
                org.key_events.filter(key_event__event_type__icontains="ele")
                .first()
                .key_event
            )

    return ret


def get_org_by_tax_id_dict(tax_ids: list) -> dict:
    """Build a mapping: tax_id => pk

    :param tax_ids:
    :return:
    """
    orgs = Organization.objects.filter(
        identifiers__scheme="CF", identifiers__identifier__in=tax_ids,
    ).values("id", "identifiers__identifier", "name", "classification")

    ret = {}
    for org in orgs:
        ret[org["identifiers__identifier"]] = {
            "id": org["id"],
            "classification": org["classification"],
            "name": org["name"],
        }
    return ret


def get_roletypes_dict_by_role(classifications_labels):
    """

    :return: a dict of the form
    {
        'Ministro': RT,
        ...
    }
    """
    rts = RoleType.objects.filter(
        classification__scheme="FORMA_GIURIDICA_OP",
        classification__descr__in=classifications_labels,
    )
    ret = {}
    for rt in rts:
        ret[rt.label.lower()] = rt
    return ret


def get_organs_dict_by_op_id():
    """returns a dict of organs related to an area OP_ID code

    :return: a dict of the form
    {
      '01': {
        'giunta': { 'name': 'Giunta regionale del Piemonte', 'pk': 1234 },
        'consiglio': { 'name': 'Consiglio regionale del Piemonte': 'pk': 3456 },
      },
      '02': {
        ...
      }
    }
    """
    orgs = Organization.objects.filter(area__identifiers__scheme="OP_ID").values(
        "area__identifiers__identifier", "name", "children__name", "children__pk"
    )

    ret = {}
    for org in orgs:
        area_code = org["area__identifiers__identifier"]
        org_name = org["name"]
        organ_name = org["children__name"]
        organ_pk = org["children__pk"]
        if organ_name is None:
            continue
        if area_code not in ret:
            ret[area_code] = {"name": org_name}
        if "giunta" in organ_name.lower():
            ret[area_code]["giunta"] = {"name": organ_name, "pk": organ_pk}
        elif "conferenza" in organ_name.lower():
            ret[area_code]["conferenza"] = {"name": organ_name, "pk": organ_pk}
        elif "consiglio metropolitano" in organ_name.lower():
            ret[area_code]["consiglio_metropolitano"] = {
                "name": organ_name,
                "pk": organ_pk,
            }
        elif "consiglio" in organ_name.lower() or "assemblea" in organ_name.lower():
            ret[area_code]["consiglio"] = {"name": organ_name, "pk": organ_pk}
    return ret


def get_organs_dict_by_minint_code(context):
    """returns a dict of organs related to an area minint electoral code

    :return: a dict of the form
    {
      '01': {
        'giunta': { 'name': 'Giunta regionale del Piemonte', 'pk': 1234 },
        'consiglio': { 'name': 'Consiglio regionale del Piemonte': 'pk': 3456 },
      },
      '02': {
        ...
      }
    }
    """
    if context == "reg":
        ctx = "Regione"
    elif context == "prov":
        ctx = "Provincia"
    elif context == "com":
        ctx = "Comune"
    elif context == "cm":
        ctx = "Città metropolitana"
    else:
        raise Exception("context has not been defined")

    orgs = Organization.objects.filter(
        classification=ctx, area__identifiers__scheme__startswith="MININT_ELETTORALE"
    ).values("area__identifiers__identifier", "name", "children__name", "children__pk")

    ret = {}
    for org in orgs:
        area_code = org["area__identifiers__identifier"]
        if ctx == "Comune":
            # strip first character as it's not in csv file for Comune context
            area_code = area_code[1:]
        org_name = org["name"]
        organ_name = org["children__name"]
        organ_pk = org["children__pk"]
        if organ_name is None:
            continue
        if area_code not in ret:
            ret[area_code] = {"name": org_name}
        if "giunta" in organ_name.lower():
            ret[area_code]["giunta"] = {"name": organ_name, "pk": organ_pk}
        elif "conferenza" in organ_name.lower():
            ret[area_code]["conferenza"] = {"name": organ_name, "pk": organ_pk}
        elif "consiglio metropolitano" in organ_name.lower():
            ret[area_code]["consiglio_metropolitano"] = {
                "name": organ_name,
                "pk": organ_pk,
            }
        elif "consiglio" in organ_name.lower() or "assemblea" in organ_name.lower():
            ret[area_code]["consiglio"] = {"name": organ_name, "pk": organ_pk}
    return ret


def get_organs_dict_by_istat_code(context):
    """returns a dict of organs related to an area istat code

    :return: a dict of the form
    {
      '01': {
        'giunta': { 'name': 'Giunta regionale del Piemonte', 'pk': 1234 },
        'consiglio': { 'name': 'Consiglio regionale del Piemonte': 'pk': 3456 },
      },
      '02': {
        ...
      }
    }
    """
    if context == "reg":
        ctx = "Regione"
    elif context == "prov":
        ctx = "Provincia"
    elif context == "com":
        ctx = "Comune"
    elif context == "cm":
        ctx = "Città metropolitana"
    else:
        raise Exception("context has not been defined")

    orgs = Organization.objects.filter(
        classification=ctx, area__identifiers__scheme__startswith="ISTAT_CODE"
    ).values("area__identifiers__identifier", "name", "children__name", "children__pk")

    ret = {}
    for org in orgs:
        area_code = org["area__identifiers__identifier"]
        org_name = org["name"]
        organ_name = org["children__name"]
        organ_pk = org["children__pk"]
        if organ_name is None:
            continue
        if area_code not in ret:
            ret[area_code] = {"name": org_name}
        if "giunta" in organ_name.lower():
            ret[area_code]["giunta"] = {"name": organ_name, "pk": organ_pk}
        elif "conferenza" in organ_name.lower():
            ret[area_code]["conferenza"] = {"name": organ_name, "pk": organ_pk}
        elif "consiglio metropolitano" in organ_name.lower():
            ret[area_code]["consiglio_metropolitano"] = {
                "name": organ_name,
                "pk": organ_pk,
            }
        elif "consiglio" in organ_name.lower() or "assemblea" in organ_name.lower():
            ret[area_code]["consiglio"] = {"name": organ_name, "pk": organ_pk}
    return ret


def get_organs_dict_by_sigla_prov(context):
    """returns a dict of organs related to the province identifier

    :return: a dict of the form
    {
      'TO': {
        'giunta': { 'name': 'Giunta provinciale di Torino', 'pk': 1234 },
        'consiglio': { 'name': 'Consiglio provinciale di Torino': 'pk': 3456 },
      },
      'AL': {
        ...
      }
    }
    """
    if context == "prov":
        ctx = "PROV"
    elif context == "metro":
        ctx = "CM"
    else:
        raise Exception("context must be defined (prov | metro)")

    orgs = Organization.objects.filter(area__istat_classification=ctx).values(
        "area__identifier", "name", "children__name", "children__pk"
    )

    ret = {}
    for org in orgs:
        area_code = org["area__identifier"].lower()
        org_name = org["name"]
        organ_name = org["children__name"]
        organ_pk = org["children__pk"]
        if organ_name is None:
            continue
        if area_code not in ret:
            ret[area_code] = {"name": org_name}
        if "giunta" in organ_name.lower():
            ret[area_code]["giunta"] = {"name": organ_name, "pk": organ_pk}
        elif "conferenza" in organ_name.lower():
            ret[area_code]["conferenza"] = {"name": organ_name, "pk": organ_pk}
        elif "consiglio metropolitano" in organ_name.lower():
            ret[area_code]["consiglio_metropolitano"] = {
                "name": organ_name,
                "pk": organ_pk,
            }
        elif "consiglio" in organ_name.lower() or "assemblea" in organ_name.lower():
            ret[area_code]["consiglio"] = {"name": organ_name, "pk": organ_pk}
    return ret


def get_organs_dict_by_city_prov():
    """returns a dict of organs related to the city name and province identifier

    Multilingual or multiple names, when separated by "/" are all added as key, leading to the same values

    :return: a dict of the form
    {
      'Agliè (TO)': {
        'giunta': { 'name': 'Giunta comunale di Agliè', 'pk': 1234 },
        'consiglio': { 'name': 'Consiglio comunale di Agliè': 'pk': 3456 },
      },
      'Aldino/Aldein (BZ)': {
        'giunta': { 'name': 'Giunta comunale di Aldino/Aldein', 'pk': 4321 },
        'consiglio': { 'name': 'Consiglio comunale di Aldino/Aldein': 'pk': 6543 },
      },
      'Aldino (BZ)': {
        'giunta': { 'name': 'Giunta comunale di Aldino/Aldein', 'pk': 4321 },
        'consiglio': { 'name': 'Consiglio comunale di Aldino/Aldein': 'pk': 6543 },
      },
      'Aldein (BZ)': {
        'giunta': { 'name': 'Giunta comunale di Aldino/Aldein', 'pk': 4321 },
        'consiglio': { 'name': 'Consiglio comunale di Aldino/Aldein': 'pk': 6543 },
      },
      ...
    }
    """

    orgs = Organization.objects.filter(
        area__istat_classification="COM",
        classification__in=['Comune', 'Provincia', 'Regione', 'Città Metropolitana']
    ).values(
        "area__name",
        "area__other_names__name",
        "area__parent__identifier",
        "name",
        "children__name",
        "children__pk",
    )

    ret = {}
    for org in orgs:

        #
        names = [org["area__name"]]
        if "/" in org["area__name"]:
            names.extend(org["area__name"].split("/"))

        if org["area__other_names__name"]:
            names.append(org["area__other_names__name"])

        for area_name in names:
            area_code = "{0} ({1})".format(
                area_name, org["area__parent__identifier"]
            ).lower()
            org_name = org["name"]
            organ_name = org["children__name"]
            organ_pk = org["children__pk"]
            if organ_name is None:
                continue
            if area_code not in ret:
                ret[area_code] = {"name": org_name}
            if "giunta" in organ_name.lower():
                ret[area_code]["giunta"] = {"name": organ_name, "pk": organ_pk}
            elif "conferenza" in organ_name.lower():
                ret[area_code]["conferenza"] = {"name": organ_name, "pk": organ_pk}
            elif "consiglio metropolitano" in organ_name.lower():
                ret[area_code]["consiglio_metropolitano"] = {
                    "name": organ_name,
                    "pk": organ_pk,
                }
            elif "consiglio" in organ_name.lower() or "assemblea" in organ_name.lower():
                ret[area_code]["consiglio"] = {"name": organ_name, "pk": organ_pk}

    return ret


def identifier_to_object_id_dict(
    content_type: ContentType, scheme: str, current: str = None, **lookup_params,
) -> Dict:
    """
    Get a dictionary which maps an identifier to an object id.

    :param content_type: the content type of the object.
    :param scheme: the scheme of the identifier.
    :param current: if the object is `Dataframeable` and it has a custom manager,
        you may want to pass this value to the "current" method of the custom manager.
        Optional, will fail silently if the model class of the object doesn't have
        the `current` method in its manager class.
    :param lookup_params: additional (and optional) lookup parameters to filter the identifiers queryset.
    :return: a dictionary which maps an identifier to an object id.
    """

    lookup_params = {"content_type": content_type, "scheme": scheme, **lookup_params}

    qs = Identifier.objects.current(current).filter(**lookup_params)

    if current and hasattr(qs, "current"):
        qs.current(current)

    return {
        identifier: object_id
        for identifier, object_id in qs.values_list("identifier", "object_id")
    }
