.. Openpolis Data Manager documentation master file, created by
   sphinx-quickstart on Mon Dec 31 10:59:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Openpolis Data Manager's documentation!
==================================================

.. toctree::
    :maxdepth: 2
    :caption: Contents:

    about.rst
    install.rst
    tests.rst
    deploy.rst
    debug.rst
    data_ingestion.rst
    operations.rst
    neo4j_data_sync.rst
    modules/models.rst
    modules/classes.rst
    commands.rst
