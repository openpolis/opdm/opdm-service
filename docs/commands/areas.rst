Areas
=====


.. _import_areas_from_istat:

import_areas_from_istat
-----------------------

.. djcommand:: project.tasks.management.commands.import_areas_from_istat

Classes being used internally
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. autoclass:: project.tasks.management.commands.import_areas_from_istat.Istat2PopoloTransformation



.. _import_areas_variations_from_istat:

import_areas_variations_from_istat
----------------------------------

.. djcommand:: project.tasks.management.commands.import_areas_variations_from_istat



.. _import_areas_geoms_from_istat:

import_areas_geoms_from_istat
-----------------------------

.. djcommand:: project.tasks.management.commands.import_areas_geoms_from_istat



.. _import_areas_finloc_from_minint:

import_areas_finloc_from_minint
-------------------------------

.. djcommand:: project.tasks.management.commands.import_areas_finloc_from_minint



.. _script_update_inhabitants_cache:

script_update_inhabitants_cache
-------------------------------

.. djcommand:: project.tasks.management.commands.script_update_inhabitants_cache