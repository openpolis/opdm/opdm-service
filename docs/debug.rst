Debugging
=========

Since the uwsgi uses the spooler processes, debugging the task execution in these process requires
a hack through remote debugging.

1. pip install pydevd-pycharm==191.6605.12 (versions must be upgraded, see preferences/about)
2. open a shell in the environment and launch uwsgi with a set of arguments:

    .. code-block::

        uwsgi --check-static=./resources --http=:8000 --master \
          --module=wsgi --callable=application \
          --pythonpath=/Users/gu/Workspace/opdm-service \
          --processes=2 --spooler=./resources/uwsgi-spooler --spooler-processes=1

3. define a python remote debug configuration on pycharm, using localhost:4444 as host:port, and
   following the instructions on how to install pydevd
4. add this snippet to code

    .. code-block::

        import pydevd
        pydevd.settrace('localhost', port=4444, stdoutToServer=True, stderrToServer=True)

   use `wsgi.py` to debug the request/response processes
   and `taskmanager/models.py` or `taskmanager/tasks.py`, to debug the command execution

5. add breakpoints

6. launch the uwsgi command in terminal
7. launch the debugger in pycharm
8. navigate, create and launch the task
9. debug!

When no debugger is activated, this can be used to test the uwsgi-spooler in a local development environment.
Just remove the code snippets and launch the uwsgi command from the terminal.

You'll be able to manage tasks and execute the commands using the uwsgi spooler processes.
