taskmanager.models
==================

.. automodule:: taskmanager.models

   
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
   
      AppCommand
      Report
      ReportManager
      Task
      TaskObjectsManager
   
   

   
   
   