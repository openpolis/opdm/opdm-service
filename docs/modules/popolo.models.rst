popolo.models
=============

.. automodule:: popolo.models
   :undoc-members:
   
   .. rubric:: Functions

   .. autosummary::
      :nosignatures:
   
      copy_organization_date_fields
      copy_person_date_fields
      update_education_levels
      validate_fields
      verify_membership_has_org_and_member
      verify_ownership_has_org_and_owner
      verify_start_end_dates_order
   
   

   
   
   .. rubric:: Classes

   .. autosummary::
      :nosignatures:

      Area
      AreaI18Name
      AreaRelationship
      Classification
      ClassificationRel
      ClassificationShortcutsMixin
      ContactDetail
      ContactDetailsShortcutsMixin
      EducationLevel
      Event
      HistoricAreaManager
      Identifier
      IdentifierShortcutsMixin
      KeyEvent
      KeyEventRel
      Language
      Link
      LinkRel
      LinkShortcutsMixin
      Membership
      Organization
      OriginalEducationLevel
      OriginalProfession
      OtherName
      OtherNamesShortcutsMixin
      Ownership
      Person
      PersonalRelationship
      Post
      Profession
      RoleType
      Source
      SourceRel
      SourceShortcutsMixin
   
   

   
   
   .. rubric:: Exceptions

   .. autosummary::
   
      Error
      OverlappingIntervalError
   
