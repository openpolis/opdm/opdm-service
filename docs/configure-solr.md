# Apache Solr


## 1. Install and configure Solr on a development machine

Install Solr and run it in standalone mode.

###### Create a collection

To create a Solr collection, run:

```bash

solr create_core -c opdm

```

Alternatively, you can visit Solr admin interface at http://<solr_host>:<solr_port>/solr
and create a new core from there.

## 2. Configure Haystack + pysolr

###### Set SOLR_URL in `config/.env`

```
# URL to a running Solr instance, changing host and port if necessary.
# In this case, "opdm" is the name of a previously created solr collection.
# Set the collection name accordingly. It must be a pre-existing collection.
SOLR_URL = http://localhost:8983/solr/opdm

```

###### Generate solr `schema.xml`

When changes are made to `search_indexes.py`, it is necessary to rebuild the 
`schema.xml` file, and to place it in the Solr configuration directory. 
To do that, we can simpy use the following Haystack management task:

```bash

python manage.py build_solr_schema > config/solr/schema.xml

```

###### Build indexes

To put your data in from your database into the search index. 
Haystack ships with a management task to make this process easy.
Simply run:

```bash

python manage.py rebuild_index

```

### Expose solr
By setting the SOLR_DOMAINS environment variable, before running the 
solr container, it is possible to use the nginx-proxy mechanism, 
in order to expose the solr server.

NOTE: this is dangerous, as the server is not protected against 
write/delete operarions, once exposed, but it can be helpful
in identifying problems.

Using docker-machine on a laptop, set the `SOLR_DOMAINS` variable in
``.dcenv``
```bash
SOLR_DOMAINS=staging.solr.openpolis.io
``` 

Using .gitlab-ci, the `SOLR_DOMAINS_STAGING` variable can be set to the
same value.

Set `SOLR_DOMAINS` to a non-existing domain, to turn this feature off.

Use the `SOLR_DOMAINS_STAGING` and `SOLR_DOMAINS_PRODUCTION` variables 
in the gitlab CI-CD settings section.
