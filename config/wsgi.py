# coding=utf-8
"""
WSGI config for opdm_service project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings")

# uncomment to debug request/response uwsgi process
# this is the process that adds the file to the spooler
# import pydevd
# pydevd.settrace('localhost', port=4444, stdoutToServer=True, stderrToServer=True)

application = get_wsgi_application()
