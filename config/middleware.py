# coding=utf-8
from django.conf import settings


class JsonAsHtml:
    """
    View a JSON response in your browser as HTML
    Useful for viewing stats using Django Debug Toolbar

    This middleware should be placed AFTER Django Debug Toolbar middleware
    ---
    Based on Mark Harris' answer on https://stackoverflow.com/a/19778011
    """

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        # not for production or production like environment
        if not settings.DEBUG or not settings.DEBUG_JSON_AS_HTML:
            return response

        # do nothing for actual ajax requests
        if request.is_ajax():
            return response

        # only do something if this is a json response
        if "application/json" in response['Content-Type'].lower():
            response.content = "<html><head><title>{title}</title></head><body>{content}</body></html>".format(
                title="JSON as HTML Middleware for: {0}".format(request.get_full_path()),
                content=response.content
            )
            response['Content-Type'] = 'text/html'
        return response
