"""Settings for local development."""

from .base import *  # noqa

# CACHES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#caches
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.locmem.LocMemCache",
        "LOCATION": "",
    }
}

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = ["django.contrib.auth.hashers.MD5PasswordHasher"]

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = "django.core.mail.backends.locmem.EmailBackend"

# Audit log settings
# -----------------------------------------------------------------------------
# Turn off audit log during local development
DJANGO_EASY_AUDIT_WATCH_MODEL_EVENTS = False
DJANGO_EASY_AUDIT_WATCH_AUTH_EVENTS = False
DJANGO_EASY_AUDIT_WATCH_REQUEST_EVENTS = True

# Do not connect labels signals during local development
LABELS_CONNECT_SIGNALS = False
TOPICS_CONNECT_SIGNALS = False

# Turn off normal logging during local development
LOGGING['loggers']['project']['handlers'] = []  # noqa

# Django Extensions
# https://django-extensions.readthedocs.io/en/stable/graph_models.html
SHELL_PLUS_PRINT_SQL = True
GRAPH_MODELS = {
    "all_applications": False,
    "arrow_shape": "diamond",
    "disable_abstract_fields": False,
    "disable_fields": False,
    "exclude_columns": [
        "created_at",
        "updated_at",
    ],
    "exclude_models": ",".join(
        (
            "Timestampable",
        )
    ),
    "group_models": True,
    "hide_edge_labels": False,
    "inheritance": False,
    "language": "it",
    "layout": "dot",
    "relations_as_fields": True,
    "sort_fields": False,
    "theme": "django2018",
    "verbose_names": True,
}

REST_FRAMEWORK["DEFAULT_PERMISSION_CLASSES"] = ['rest_framework.permissions.AllowAny']  # noqa
