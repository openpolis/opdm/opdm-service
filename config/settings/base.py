"""
Django settings for opdm_service project.

For more information on this file, see
https://docs.djangoproject.com/en/dev/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/dev/ref/settings/
"""
import datetime
import os
from pathlib import Path
import sys

import environ


# PATH CONFIGURATION
# -----------------------------------------------------------------------------
ROOT_PATH = Path(__file__).parents[2]
PROJECT_NAME = "Openpolis Data Manager"
PROJECT_PACKAGE = "project"
PROJECT_PATH = ROOT_PATH / PROJECT_PACKAGE
APPS_PATH = PROJECT_PATH
CONFIG_PATH = ROOT_PATH / "config"
RESOURCES_PATH = ROOT_PATH / "resources"

project_pkg = __import__(PROJECT_PACKAGE)
sys.path.append(str(PROJECT_PATH))

# Parse .env file
# -----------------------------------------------------------------------------
env = environ.Env()
# .env file, should load only in development environment
# Operating System Environment variables have precedence over variables defined
# in the .env file, that is to say variables from the .env files will only be
# used if not defined as environment variables.
if env.str("DJANGO_ENV_FILE", default=True):
    environ.Env.read_env(str(CONFIG_PATH / ".env"))

# GENERAL
# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#debug
DEBUG = env.bool("DEBUG", True)
# https://docs.djangoproject.com/en/dev/ref/settings/#use-thousand-separator
SECRET_KEY = env("SECRET_KEY", default="not-so-secret")
# Local time zone. Choices are
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# though not all of them may be available with every OS.
# In Windows, this must be set to your system time zone.
TIME_ZONE = "Europe/Rome"
# https://docs.djangoproject.com/en/dev/ref/settings/#language-code
LANGUAGE_CODE = "it"
# https://docs.djangoproject.com/en/dev/ref/settings/#site-id
SITE_ID = 1
# https://docs.djangoproject.com/en/dev/ref/settings/#use-i18n
USE_I18N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-l10n
USE_L10N = True
# https://docs.djangoproject.com/en/dev/ref/settings/#use-tz
USE_TZ = True
# https://docs.djangoproject.com/en/dev/ref/settings/#locale-paths
LOCALE_PATHS = [(ROOT_PATH / "locale")]
# https://docs.djangoproject.com/en/dev/ref/settings/#use-thousand-separator
USE_THOUSAND_SEPARATOR = True

DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

# See https://docs.djangoproject.com/en/1.11/ref/settings/#allowed-hosts
ALLOWED_HOSTS = env.list("ALLOWED_HOSTS", default="*")

# DOMAIN
DOMAIN = env.str("DOMAIN", default='local')

# DATABASES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#databases
DATABASES = {
    "default": env.db("DATABASE_URL")
}
DATABASES["default"]["ATOMIC_REQUESTS"] = True

# URLS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#root-urlconf
ROOT_URLCONF = "config.urls"
# https://docs.djangoproject.com/en/dev/ref/settings/#wsgi-application
WSGI_APPLICATION = "config.wsgi.application"

# APPS
# ------------------------------------------------------------------------------
DJANGO_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.humanize",
    "django.contrib.admin",
    "django.contrib.gis",
]
THIRD_PARTY_APPS = [
    "rest_framework",  # REST APIs framework
    "rest_framework_gis",
    "drf_yasg",  # OpenAPI generator
    "django_extensions",
    "django_filters",
    "crispy_forms",
    "haystack",  # solr search and indexing engine
    "popolo",  # Popolo models - http://www.popoloproject.com/
    "easyaudit",
    "taskmanager",  # uWSGI task manager
    "colorfield",
    "django_rq",
    "ckeditor",
]

LOCAL_APPS = [
    "project.api_v1",  # REST APIs v1
    "project.opp.api_opp_v1",  # Applicative API for Openparlamento App v1
    "project.atoka",
    "project.atoka.align",
    "project.akas",
    "project.scrapy",
    "project.tasks",  # Import and maintenance scripts
    "project.appointments",
    "project.labels",
    "project.topics",
    # "project.audit.apps.AuditConfig",  # Custom app config for easyaudit
    "project.elections",
    "project.opp.votes",
    "project.opp.acts",
    "project.calendars",
    "project.opp.metrics",
    "project.opp.gov",
    "project.opp.parl",
    "project.opp.texts",
    "project.opp.home",

]

# https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

# AUTHENTICATION
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#authentication-backends
AUTHENTICATION_BACKENDS = ["django.contrib.auth.backends.ModelBackend"]

# PASSWORDS
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#password-hashers
PASSWORD_HASHERS = [
    # https://docs.djangoproject.com/en/dev/topics/auth/passwords/#using-argon2-with-django
    "django.contrib.auth.hashers.Argon2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2PasswordHasher",
    "django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher",
    "django.contrib.auth.hashers.BCryptSHA256PasswordHasher",
]
# https://docs.djangoproject.com/en/dev/ref/settings/#auth-password-validators
AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

# MIDDLEWARE
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#middleware
MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.locale.LocaleMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "easyaudit.middleware.easyaudit.EasyAuditMiddleware",
    "django.middleware.gzip.GZipMiddleware"
]

# STATIC
# ------------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-root
STATIC_ROOT = env("STATIC_ROOT", default=str(RESOURCES_PATH / "static"))
# See: https://docs.djangoproject.com/en/dev/ref/settings/#static-url
STATIC_URL = "/static/"
# See: https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-STATICFILES_DIRS
STATICFILES_DIRS = [str(PROJECT_PATH / "static")]
# See: https://docs.djangoproject.com/en/dev/ref/contrib/staticfiles/#staticfiles-finders
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.FileSystemFinder",
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
)

# MEDIA
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#media-root
MEDIA_ROOT = env("MEDIA_ROOT", default=str(RESOURCES_PATH / "media"))
# https://docs.djangoproject.com/en/dev/ref/settings/#media-url
MEDIA_URL = "/media/"
# https://docs.djangoproject.com/en/dev/ref/settings/#file-upload-permissions
FILE_UPLOAD_PERMISSIONS = 0o644
# https://docs.djangoproject.com/en/dev/ref/settings/#file-upload-max-memory-size
FILE_UPLOAD_MAX_MEMORY_SIZE = 52428800

# TEMPLATES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#templates
TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [(str(PROJECT_PATH / "templates"))],
        "APP_DIRS": True,
        "OPTIONS": {
            "debug": DEBUG,
            "context_processors": [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven't customized them:
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "django.template.context_processors.request",
                "project.context_processors.main_settings",
            ],
        },
    }
]

# http://django-crispy-forms.readthedocs.io/en/latest/install.html#template-packs
CRISPY_TEMPLATE_PACK = "bootstrap4"

# FIXTURES
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#fixture-dirs
FIXTURE_DIRS = [str(PROJECT_PATH / "fixtures")]

# SECURITY
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#session-cookie-httponly
SESSION_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#csrf-cookie-httponly
CSRF_COOKIE_HTTPONLY = True
# https://docs.djangoproject.com/en/dev/ref/settings/#secure-browser-xss-filter
SECURE_BROWSER_XSS_FILTER = True
# https://docs.djangoproject.com/en/dev/ref/settings/#x-frame-options
X_FRAME_OPTIONS = "DENY"

# EMAIL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#email-backend
EMAIL_BACKEND = env(
    "DJANGO_EMAIL_BACKEND", default="django.core.mail.backends.smtp.EmailBackend"
)
# https://docs.djangoproject.com/en/dev/ref/settings/#email-timeout
EMAIL_TIMEOUT = 5

# ADMIN
# ------------------------------------------------------------------------------
# Django Admin URL.
ADMIN_URL = "admin/"
ADMIN_EMAIL = env("ADMIN_EMAIL", default="admin.opdm@openpolis.it")
ADMIN_NAME = env("ADMIN_NAME", default=ADMIN_EMAIL.split("@")[0])
ADMINS = [(ADMIN_EMAIL, ADMIN_NAME)]
# https://docs.djangoproject.com/en/dev/ref/settings/#managers
MANAGERS = ADMINS

# LOGGING CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://docs.djangoproject.com/en/dev/ref/settings/#logging
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGS_PATH = env("LOGS_PATH", default=str(RESOURCES_PATH / "logs"))

LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "verbose": {
            "format": "[%(asctime)s] %(levelname)s [%(pathname)s:%(funcName)s:%(lineno)s] %(message)s",
            "datefmt": "%d/%b/%Y %H:%M:%S",
        },
        "simple": {
            "format": "[%(asctime)s] %(levelname)s %(message)s",
            "datefmt": "%d/%b/%Y %H:%M:%S",
        },
    },
    "handlers": {
        "file": {
            "level": "INFO",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": os.path.normpath(os.path.join(LOGS_PATH, "opdm-service.log")),
            "maxBytes": 1024 * 1024 * 10,  # 10 MB
            "backupCount": 7,
            "formatter": "verbose",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "stream": sys.stdout,
            "formatter": "verbose",
        },
    },
    "loggers": {
        "django": {
            "handlers": [],
            # 'handlers': ['file', 'console'],
            "level": "DEBUG",
            "propagate": True,
        },
        "project": {"handlers": ["file", "console"], "level": "INFO"},
        "ooetl": {"handlers": ["file", "console"], "level": "DEBUG"},
        "taskmanager": {"handlers": ["file", "console"], "level": "INFO"},
    },
}
# TOOLBAR CONFIGURATION
# -----------------------------------------------------------------------------
# See: http://django-debug-toolbar.readthedocs.org/en/latest/installation.html
DEBUG_TOOLBAR = env.bool("DEBUG_TOOLBAR", DEBUG)
if DEBUG_TOOLBAR:
    INSTALLED_APPS += ("debug_toolbar",)
    MIDDLEWARE += ("debug_toolbar.middleware.DebugToolbarMiddleware",)
    DEBUG_TOOLBAR_PATCH_SETTINGS = False

    # The Debug Toolbar is shown only if your IP is listed in the INTERNAL_IPS setting.
    INTERNAL_IPS = env("DEBUG_TOOLBAR_INTERNAL_IPS", default="127.0.0.1").split(",")

# def show_toolbar(request):
#     return False

DEBUG_JSON_AS_HTML = env("DEBUG_JSON_AS_HTML", default=False)
if DEBUG_JSON_AS_HTML:
    MIDDLEWARE += ("config.middleware.JsonAsHtml",)

# DEBUG_TOOLBAR_CONFIG = {
#     "SHOW_TOOLBAR_CALLBACK": show_toolbar,
# }

# REST FRAMEWORK CONFIGURATION
# -----------------------------------------------------------------------------
REST_FRAMEWORK = {
    # Only authenticated users can access the API
    # Authentication may be passed through:
    # - a cookie,
    # - a basic_auth header
    # - a jwt header (json web token)
    "DEFAULT_PERMISSION_CLASSES": ["api_v1.permissions.OPDMPermission",
                                   "opp.api_opp_v1.permissions.OPPPermission"],
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework_simplejwt.authentication.JWTAuthentication",
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework.authentication.BasicAuthentication",
    ),
    "DEFAULT_PAGINATION_CLASS": "project.core.LargeResultsSetPagination",
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
    "DEFAULT_RENDERER_CLASSES": (
        "rest_framework.renderers.JSONRenderer",
        # 'rest_framework.renderers.BrowsableAPIRenderer',
        "project.core.OPDMBrowsableAPIRenderer",
        "rest_framework.renderers.AdminRenderer",
    ),
    "DEFAULT_VERSIONING_CLASS": "rest_framework.versioning.NamespaceVersioning",
    "DEFAULT_VERSION": "v1",
    "DEFAULT_THROTTLE_CLASSES": ("rest_framework.throttling.AnonRateThrottle",),
    "DEFAULT_THROTTLE_RATES": {"anon": "10000/day"},
    "DEFAULT_SCHEMA_CLASS": "rest_framework.schemas.coreapi.AutoSchema",
}

# OPENAPI CONFIGURATION
# -----------------------------------------------------------------------------
SWAGGER_SETTINGS = {
    "DEFAULT_INFO": "config.urls.api_info",
    "USE_SESSION_AUTH": False,
    "DEFAULT_MODEL_DEPTH": 1,
}

# JWT AUTH CONFIGURATION
# -----------------------------------------------------------------------------
# See: https://github.com/davesque/django-rest-framework-simplejwt
SIMPLE_JWT = {
    "ALGORITHM": "HS256",
    "SIGNING_KEY": SECRET_KEY,
    "VERIFYING_KEY": None,
    "AUDIENCE": None,
    "ISSUER": "https://service.opdm.openpolis.io",
    "AUTH_HEADER_TYPES": ("Bearer", "JWT"),
    "USER_ID_FIELD": "id",
    "USER_ID_CLAIM": "user_id",
    "AUTH_TOKEN_CLASSES": ("rest_framework_simplejwt.tokens.SlidingToken",),
    "TOKEN_TYPE_CLAIM": "token_type",
    "JTI_CLAIM": "jti",
    "SLIDING_TOKEN_REFRESH_EXP_CLAIM": "refresh_exp",
    "SLIDING_TOKEN_LIFETIME": datetime.timedelta(days=1),
    "SLIDING_TOKEN_REFRESH_LIFETIME": datetime.timedelta(days=7),
    # These settings are irrelevant when using SlidingToken:
    # "ACCESS_TOKEN_LIFETIME": datetime.timedelta(days=1),
    # "REFRESH_TOKEN_LIFETIME": datetime.timedelta(days=7),
    # "ROTATE_REFRESH_TOKENS": False,
    # "BLACKLIST_AFTER_ROTATION": True,
}

# CORS HEADERS CONFIGURATION
# -----------------------------------------------------------------------------
if env.bool("DJANGO_CORS_HEADERS", default=False):
    MIDDLEWARE = ["corsheaders.middleware.CorsMiddleware"] + MIDDLEWARE
    DJANGO_APPS = DJANGO_APPS + ["corsheaders"]
    CORS_ORIGIN_WHITELIST = env.list("CORS_ORIGIN_WHITELIST", default=[])
    CORS_ALLOW_CREDENTIALS = env.bool("CORS_ALLOW_CREDENTIALS", default=False)

# HAYSTACK CONFIGURATION
# -----------------------------------------------------------------------------
HAYSTACK_CONNECTIONS = {
    "default": {
        "ENGINE": "haystack.backends.solr_backend.SolrEngine",
        "URL": env("SOLR_URL", default="http://localhost:8983/solr/opdm"),
        "TIMEOUT": 60 * 5,
        "INCLUDE_SPELLING": True,
        "BATCH_SIZE": env("SOLR_BATCH_SIZE", default=100),
    }
}
HAYSTACK_SEARCH_RESULTS_PER_PAGE = env("HAYSTACK_SEARCH_RESULTS_PER_PAGE", default=50)
HAYSTACK_SIGNAL_PROCESSOR = env(
    "HAYSTACK_SIGNAL_PROCESSOR", default="haystack.signals.BaseSignalProcessor"
)

# AKA generation thresholds
# if results found by solr query have maximum score below AKA_LO_THRESHOLD, consider the record NOT FOUND
# if results found by solr query have maximum score above AKA_HI_THRESHOLD, consider the record POSITIVELY IDENTIFIED
# only generates AKA records (similarities) for maximum scores falling in between
# -----------------------------------------------------------------------------
AKA_LO_THRESHOLD = env("AKA_LO_THRESHOLD", default=50)
AKA_HI_THRESHOLD = env("AKA_HI_THRESHOLD", default=150)

# Import cache and output folders
IMPORT_CACHE_PATH = env("IMPORT_CACHE_PATH", default=str(RESOURCES_PATH / "data/cache"))
IMPORT_OUT_PATH = env("IMPORT_OUT_PATH", default=str(RESOURCES_PATH / "data/out"))

# Atoka API connection settings
# -----------------------------------------------------------------------------
# https://developers.atoka.io/v2/
ATOKA_API_ENDPOINT = env("ATOKA_API_ENDPOINT", default="https://api.atoka.io")
ATOKA_API_VERSION = env("ATOKA_API_VERSION", default="v2")
ATOKA_API_KEY = env("ATOKA_API_KEY", default=None)

# Proxy to connect as if from Italy
HTTP_PROXY_HOST = env("HTTP_PROXY_HOST", default=None)
HTTP_PROXY_HOST_PORT = env("HTTP_PROXY_HOST_PORT", default="8080")
HTTP_PROXY_USERNAME = env("HTTP_PROXY_USERNAME", default=None)
HTTP_PROXY_PASSWORD = env("HTTP_PROXY_PASSWORD", default=None)

# Task manager settings (django-uwsgi-taskmanager)
# -----------------------------------------------------------------------------
UWSGI_TASKMANAGER_N_REPORTS_INLINE = 5
UWSGI_TASKMANAGER_N_LINES_IN_REPORT_LOG = 5
UWSGI_TASKMANAGER_N_LINES_IN_REPORT_INLINE = 10
UWSGI_TASKMANAGER_SHOW_LOGVIEWER_LINK = True
UWSGI_TASKMANAGER_USE_FILTER_COLLAPSE = True

UWSGI_TASKMANAGER_NOTIFICATION_HANDLERS = {
    "slack": {
        "class": "taskmanager.notifications.SlackNotificationHandler",
        "level": "warnings",
        "token": env("UWSGI_TASKMANAGER_NOTIFICATIONS_SLACK_TOKEN", default=""),
        "channel": env("UWSGI_TASKMANAGER_NOTIFICATIONS_SLACK_CHANNELS", default=""),
    },
}

# Audit log settings
# -----------------------------------------------------------------------------
DJANGO_EASY_AUDIT_WATCH_MODEL_EVENTS = env.bool("DJANGO_EASY_AUDIT_WATCH_MODEL_EVENTS", default=True)
DJANGO_EASY_AUDIT_WATCH_AUTH_EVENTS = env.bool("DJANGO_EASY_AUDIT_WATCH_AUTH_EVENTS", default=True)
DJANGO_EASY_AUDIT_WATCH_REQUEST_EVENTS = env.bool("DJANGO_EASY_AUDIT_WATCH_REQUEST_EVENTS", default=True)
DJANGO_EASY_AUDIT_UNREGISTERED_CLASSES_EXTRA = []
DJANGO_EASY_AUDIT_REGISTERED_CLASSES = [
    "popolo.Area",
    "popolo.Person",
    "popolo.Organization",
    "popolo.Membership",
    "popolo.Ownership",
    "popolo.Post",
    "popolo.KeyEvent",
]
# This is a custom setting, not included in easyaudit upstream project.
# See: `settings` module inside `audit` app.
DJANGO_EASY_AUDIT_AUDIT_HTTP_METHODS = ["GET", "POST", "PUT", "DELETE"]
DJANGO_EASY_AUDIT_UNREGISTERED_URLS_EXTRA = [r'^/api-openparlamento/', '^/taskmanager/read_loglines/']

LABELS_CONNECT_SIGNALS = True
TOPICS_CONNECT_SIGNALS = True

# Neo4j connection variables
NEO4J_URI = env("NEO4J_URI", default="bolt://localhost:7687")
NEO4J_USERNAME = env("NEO4J_USERNAME", default="neo4j")
NEO4J_PASSWORD = env("NEO4J_PASSWORD", default="neo4j")

# Bandicovid authentication (cor contextual import)
BANDICOVID_USERNAME = env("BANDICOVID_USERNAME", default="")
BANDICOVID_PASSWORD = env("BANDICOVID_PASSWORD", default="")

# OPP19 settings
RELATION_TYPE_CLASSIFICATION = {
    'scheme': "OP_TIPO_RELAZIONE_ATTI",
    'nextprev_value': "Successivo",
    'split_value': "Suddivisione",
    'joined_value': "Unione"
}

# -----------------------------------------------------------------------------
CONTEXT = env("CONTEXT", default="production")
VERSION = project_pkg.get_version_str()
# Usually set by the Gitlab runner, to set it manually:
# CI_COMMIT_SHA=$(git rev-parse HEAD)
GIT_REVISION = env.str("CI_COMMIT_SHA", default=project_pkg.parse_revision())

# ENDPOINTS SPARQL assemblee parlamentari

ENDPOINTS_ASSEMBLEE_PARLAMENTARI = {
    'camera': 'http://dati.camera.it/sparql',
    'senato': 'https://dati.senato.it/sparql'
}
REDIS_HOST = env("REDIS_HOST", default="redis")
REDIS_PORT = env("REDIS_PORT", default=6379)
REDIS_DB = env("REDIS_DB", default=0)
REDIS_URL = f"redis://{REDIS_HOST}:{REDIS_PORT}/{REDIS_DB}"
RQ_QUEUES = {
    'default': {
        'URL': env('REDIS_URL', default=REDIS_URL),
        'DEFAULT_TIMEOUT': 360,
    },
    'high': {
        'URL': env('REDIS_URL', default=REDIS_URL),
        'DEFAULT_TIMEOUT': 500,
    },
    'low': {
        'URL': env('REDIS_URL', default=REDIS_URL),
    }
}

# CACHES CONFIGURATION
# -----------------------------------------------------------------------------

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": env("REDIS_URL", default=f"{REDIS_URL}"),
        "OPTIONS": {"CLIENT_CLASS": "django_redis.client.DefaultClient"},
        "IGNORE_EXCEPTIONS": True,
    },
    "db": {
        "BACKEND": "django.core.cache.backends.db.DatabaseCache",
        "LOCATION": "opdm_cache_table",
        "OPTIONS": {"MAX_ENTRIES": 999999999},
    }
}
APPEND_SLASH = True
# Neo4j connection variables
CACHE_INVALIDATE_TOKEN = env("CACHE_INVALIDATE_TOKEN")
