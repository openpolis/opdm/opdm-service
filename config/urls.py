from django.conf import settings
from django.contrib import admin
from django.urls import re_path, include, path
from django.views.generic import TemplateView, RedirectView
from rest_framework.documentation import include_docs_urls
from rest_framework_simplejwt.views import (
    TokenRefreshSlidingView,
    TokenVerifyView,
)

from project.jwt_auth.views import CustomTokenObtainSlidingView
from project.opp.home.views import get_instances_for_content_type
from project.views import AboutView, DatalogView, ChangelogView, api_list, V1RedirectView

admin.autodiscover()

urlpatterns = [
    re_path(r"^$", RedirectView.as_view(url="about"), name="home"),
    re_path(r"^about/$", AboutView.as_view(template_name="about.html"), name="about"),
    re_path(
        r"^about/it/$",
        AboutView.as_view(template_name="about_it.html"),
        name="about_it",
    ),
    re_path(
        r"^data-changelog/$",
        DatalogView.as_view(template_name="changelog.html"),
        name="datalog",
    ),
    re_path(
        r"^changelog/$",
        ChangelogView.as_view(template_name="changelog.html"),
        name="changelog",
    ),
    re_path(
        r"^docs/", include_docs_urls(
            title=f"{settings.PROJECT_NAME} API",
            description="All APIs in OPDM."
        )
    ),
    re_path(r"^admin/", admin.site.urls),
    re_path(
        r"^403$", TemplateView.as_view(template_name="403.html"), name="tampering-403"
    ),

    # External apps
    re_path(r"^popolo/", include("popolo.urls")),
    re_path(r"^taskmanager/", include("taskmanager.urls")),

    # REST API authentication endpoints
    re_path(
        r"^api-token-auth/",
        CustomTokenObtainSlidingView.as_view(),
        name="simplejwt_obtain_token",
    ),
    re_path(
        r"^api-token-refresh/",
        TokenRefreshSlidingView.as_view(),
        name="simplejwt_refresh_token",
    ),
    re_path(
        r"^api-token-verify/", TokenVerifyView.as_view(), name="simplejwt_verify_token"
    ),
    re_path(r"^api-auth/", include("rest_framework.urls", namespace="rest_framework")),

    # REST APIs
    path("api-list/", api_list, name='api-list'),
    path("api-mappepotere/", include("project.api_v1.urls")),

    # REDIRECT old API
    re_path('^v1/(?P<path>.*)$', V1RedirectView.as_view())

]
if settings.DEBUG_TOOLBAR:
    import debug_toolbar

    urlpatterns.append(path('__debug__/', include(debug_toolbar.urls)))

# electoral institutions and organizations redirections
urlpatterns += [
    path(
        r'v1/electoral_results/constituencies/<int:pk>/',
        RedirectView.as_view(url="/v1/areas/%(pk)s", permanent=True),
        name="redirect_electoral_constituencies"
    ),
    path(
        r'v1/electoral_results/institutions/<int:pk>/',
        RedirectView.as_view(url="/v1/organizations/%(pk)s", permanent=True),
        name="redirect_electoral_institutions"
    ),
    path(
        r'v1/electoral_results/events/<int:pk>/',
        RedirectView.as_view(url="/v1/keyevents/%(pk)s", permanent=True),
        name="redirect_electoral_institutions"
    ),
]

# opp application API urls
urlpatterns += [
    path("api-openparlamento/", include("project.opp.api_opp_v1.urls")),
]

urlpatterns += [
    path('django-rq/', include('django_rq.urls'))
]

urlpatterns += [
    path('get-instances/', get_instances_for_content_type, name='get_instances'),
]
