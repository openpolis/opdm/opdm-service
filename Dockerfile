FROM python:3.9-slim

# Update, upgrade and install useful tools and set aliases
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get -qy update \
    && apt-get install -qqy apt-utils \
    && apt-get -qqy upgrade \
    && apt-get install -y --no-install-recommends binutils libproj-dev gdal-bin \
    && apt-get install -qqqy --no-install-recommends \
        gcc \
        git \
        locales gettext \
        python3-dev \
        libxml2-dev libxslt-dev \
        tmux \
        jq \
        less \
        vim \
    && rm -rf /var/lib/apt/lists/*
RUN echo 'alias ll="ls -l"' >> ~/.bashrc \
    && echo 'alias la="ls -la"' >> ~/.bashrc

# add it locale
COPY locales.txt /etc/locale.gen
RUN locale-gen

# Create `/app` directory
RUN mkdir -p /app
WORKDIR /app

# Install projects requirements
COPY requirements.txt /app/
RUN pip3 install --upgrade pip pip-tools "setuptools>=61.0.0,<66.0.0"  && pip-sync

# check for new changes in django-popolo and reinstall it if necessary
ADD "https://gitlab.depp.it/openpolis/django-popolo/commits/master?format=atom" /dev/null
RUN pip3 install --exists-action=w -e git+https://gitlab.depp.it/openpolis/django-popolo@master#egg=django-popolo

# check for new changes in opdm-etl
ADD "https://gitlab.depp.it/openpolis/opdm/opdm-etl/commits/master?format=atom" /dev/null
RUN pip3 install --exists-action=w -e git+https://gitlab.depp.it/openpolis/opdm/opdm-etl.git@master#egg=opdm-etl

# remove gcc and build dependencies to keep image small
RUN apt-get purge -y --auto-remove gcc python3-dev

# copy the project in app
COPY . /app/

COPY ./compose/django/start-rqworker /start-rqworker
RUN sed -i 's/\r$//g' /start-rqworker
RUN chmod +x /start-rqworker

# remove locales, as not needed any longer
RUN rm /app/locales.txt

# create directory for the  uwsgi spooler
RUN mkdir -p /var/lib/uwsgi

